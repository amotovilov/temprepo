package ru.olekstra.common.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dozer.Mapper;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.common.helper.DozerHelper;
import ru.olekstra.domain.TransactionExportInfo;
import ru.olekstra.domain.TransactionTotalInfo;
import ru.olekstra.domain.dto.ProcessingTransactionMessage;
import ru.olekstra.domain.dto.TransactionTotalInfoDto;

@Service
public class TransactionTotalInfoService {

    @Autowired
    private DynamodbService dynamodbService;

    @Autowired
    private Mapper mapper;

    public void updateTransactionTotalInfo(ProcessingTransactionMessage mes) {
        String period = LimitPeriod.DAY.getCurrentPeriod(DateTime.now(DateTimeZone.UTC));
        String lastThreeDigits = mes.getCardNumber().substring(mes.getCardNumber().length() - 3);
        String lastTwoDigits = mes.getCardNumber().substring(mes.getCardNumber().length() - 2);

        TransactionTotalInfo info3 = dynamodbService.getObject(TransactionTotalInfo.class,
                period, lastThreeDigits);

        Map<String, Object> valuesToAdd = new HashMap<String, Object>();
        valuesToAdd.put(TransactionTotalInfo.FLD_COUNT, mes.getCount());
        valuesToAdd.put(TransactionTotalInfo.FLD_TOTAL_SUM_TO_PAY, mes.getSumToPay());

        if (info3 == null) {
            TransactionTotalInfo newInfo3 = new TransactionTotalInfo(period, lastThreeDigits, mes.getSumToPay(),
                    mes.getCount());
            dynamodbService.putObjectOrDie(newInfo3);
        } else {
            boolean success = dynamodbService.addToAttributes(TransactionTotalInfo.TABLE_NAME, period,
                    lastThreeDigits, valuesToAdd);

            if (!success) {
                throw new RuntimeException(String.format(
                        "Failed to update TransactionTotalInfo entity (period: %s, lastDigits: %s)", period,
                        lastThreeDigits));
            }
        }

        TransactionTotalInfo info2 = dynamodbService.getObject(TransactionTotalInfo.class,
                period, lastTwoDigits);

        if (info2 == null) {
            TransactionTotalInfo newInfo = new TransactionTotalInfo(period, lastTwoDigits, mes.getSumToPay(),
                    mes.getCount());
            dynamodbService.putObjectOrDie(newInfo);
        } else {
            boolean success = dynamodbService.addToAttributes(TransactionTotalInfo.TABLE_NAME, period,
                    lastTwoDigits, valuesToAdd);

            if (!success) {
                throw new RuntimeException(String.format(
                        "Failed to update TransactionTotalInfo entity (period: %s, lastDigits: %s)", period,
                        lastTwoDigits));
            }
        }
    }

    public void saveTransactionExportInfo(ProcessingTransactionMessage mes) {
        String period = LimitPeriod.DAY.getCurrentPeriod(DateTime.now(DateTimeZone.UTC));
        String digits = mes.getCardNumber().substring(mes.getCardNumber().length() - 3);
        String transactionId = mes.getTrasactionRangeKey();
        String drugstoreId = mes.getDrugstoreId();
        TransactionExportInfo exportInfo = new TransactionExportInfo(period, digits, transactionId, drugstoreId);
        dynamodbService.putObjectOrDie(exportInfo);
    }

    public List<TransactionTotalInfoDto> getDailyTransactionInfo(DateTime date) throws InstantiationException,
            IllegalAccessException, IOException {
        List<TransactionTotalInfo> items = dynamodbService.queryObjects(TransactionTotalInfo.class,
                LimitPeriod.DAY.getCurrentPeriod(date));
        List<TransactionTotalInfoDto> result = DozerHelper.map(mapper, items, TransactionTotalInfoDto.class);
        return result;
    }

}
