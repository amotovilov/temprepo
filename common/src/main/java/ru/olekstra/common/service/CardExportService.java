package ru.olekstra.common.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.dozer.Mapper;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.olekstra.awsutils.BatchOperationResult;
import ru.olekstra.awsutils.DynamodbEntity;
import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.DynamodbService.LimitObjectsResult;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.common.helper.DozerHelper;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.CardChange;
import ru.olekstra.domain.dto.CardChangeDto;
import ru.olekstra.domain.dto.CardExportResponse;

import com.amazonaws.services.dynamodb.model.AttributeValue;
import com.amazonaws.services.dynamodb.model.Key;

@Service
public class CardExportService {
    @Autowired
    Mapper mapper;
    @Autowired
    DynamodbService service;
    @Autowired
    DateTimeService dateService;

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat
            .forPattern(DynamodbEntity.DB_DATE_FORMAT);

    private List<CardChangeDto> cardChange2Dto(List<CardChange> value) throws RuntimeException, InterruptedException,
            IOException {
        String[] hashKeys = new String[value.size()];
        int i = 0;
        for (CardChange c : value) {
            hashKeys[i] = c.getNumber();
            i++;
        }
        BatchOperationResult<Card> cards = service.getObjects(Card.class, hashKeys);
        return DozerHelper.map(mapper, cards.getSuccessList(), CardChangeDto.class);
    }

    private class KeyContainer {
        Key key;
    }

    private List<CardChangeDto> exportData(DateTime date, KeyContainer lastEvaluatedKey) throws InstantiationException,
            IllegalAccessException, RuntimeException, InterruptedException, IOException {
        LimitObjectsResult<CardChange> values = service.getLimitObjects(CardChange.class,
                AppSettingsService.EXPORT_BATCH_SIZE, DATE_FORMATTER.print(date), lastEvaluatedKey.key);
        lastEvaluatedKey.key = values.getLastEvaluatedKey();
        return cardChange2Dto(values.getResult());
    }

    private List<CardChangeDto> exportList(DateTime date, StringBuilder lastEvaluatedCardNumber)
            throws InstantiationException, IllegalAccessException, RuntimeException, InterruptedException, IOException {
        Key lastKey = null;
        if (lastEvaluatedCardNumber != null)
            if (lastEvaluatedCardNumber.length() > 0)
                lastKey = new Key(new AttributeValue(DATE_FORMATTER.print(date)), new AttributeValue(
                        lastEvaluatedCardNumber.toString()));
        KeyContainer keyContainer = new KeyContainer();
        keyContainer.key = lastKey;
        List<CardChangeDto> result = exportData(date, keyContainer);
        lastEvaluatedCardNumber.setLength(0);
        if (keyContainer.key != null)
            lastEvaluatedCardNumber.append(keyContainer.key.getRangeKeyElement().getS());
        return result;
    }

    public CardExportResponse exportData(DateTime date, String lastEvaluatedCardNumber) throws InstantiationException,
            IllegalAccessException, RuntimeException, InterruptedException, IOException {
        CardExportResponse response = new CardExportResponse();
        response.setDate(DATE_FORMATTER.print(date));
        StringBuilder lk = new StringBuilder(lastEvaluatedCardNumber == null ? "" : lastEvaluatedCardNumber);
        response.setValues(exportList(date, lk));
        response.setLastEvaluatedKey(lk.length() == 0 ? null : lk.toString());
        return response;
    }

    public CardChangeDto exportCard(String cardNumber) {
        Card card = service.getObject(Card.class, cardNumber);
        return mapper.map(card, CardChangeDto.class);
    }

    public List<CardChangeDto> cardsChangeEvent(Set<String> cardNumbers) throws ItemSizeLimitExceededException,
            InterruptedException, RuntimeException, IOException {
        DateTime date = dateService.createUTCDate();
        String hashKey = DATE_FORMATTER.print(date);
        String[] hashKeys = new String[cardNumbers.size()];
        Arrays.fill(hashKeys, hashKey);

        String[] rangeKeys = cardNumbers.toArray(new String[cardNumbers.size()]);
        BatchOperationResult<CardChange> exists = service.getObjects(CardChange.class, hashKeys, rangeKeys);

        if (exists != null) {
            Iterator<String> cardNumsIterator = cardNumbers.iterator();
            while (cardNumsIterator.hasNext()) {
                String num = cardNumsIterator.next();
                for (CardChange found : exists.getSuccessList()) {
                    if (found.getNumber().equals(num)) {
                        cardNumsIterator.remove();
                    }
                }
            }
        }

        List<CardChange> values = new ArrayList<CardChange>();
        for (String cardNumber : cardNumbers) {
            CardChange value = new CardChange();
            value.setChangeDate(date);
            value.setNumber(cardNumber);
            values.add(value);
        }
        if (values.size() == 0)
            return null;

        int batchSize = service.getMaxBatchSize();
        int counter = 0;
        List<CardChange> batch = new ArrayList<CardChange>();
        List<CardChange> result = new ArrayList<CardChange>();
        for (CardChange value : values) {
            counter++;
            batch.add(value);
            if ((batch.size() == batchSize) || (counter == values.size())) {
                result.addAll(
                        service.putObjects(
                                batch.toArray(new CardChange[batch.size()]))
                                .getSuccessList());
                batch.clear();
            }

        }

        if (result.size() == 0)
            return null;

        return DozerHelper.map(mapper, result, CardChangeDto.class);
    }

    public DateTime stringToDate(String value) {
        return DATE_FORMATTER.parseDateTime(value);
    }

    public String dateToString(DateTime value) {
        return DATE_FORMATTER.print(value);
    }

}
