package ru.olekstra.common.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import ru.olekstra.common.dao.AppSettingsDao;
import ru.olekstra.domain.AppSettings;
import ru.olekstra.domain.Formular;
import ru.olekstra.domain.Pack;
import ru.olekstra.domain.PackA;
import ru.olekstra.domain.PackB;

@Service
public class AppSettingsService {
    private AppSettingsDao dao;
    private AppSettings appSettings;

    public static final String DEFAULT_BACKOFFICE_REPORT_BUCKET = "reports.backoffice.olekstra.ru";
    public static final String DEFAULT_MANUFACTURER_REPORT_BUCKET = "manufacturer.reports.backoffice.olekstra.ru";
    public static final String DEFAULT_RECIPE_REPORT_BUCKET = "recipe.backoffice.olekstra.ru";
    public static final String DEFAULT_RECIPE_BUCKET = "recipe.backoffice.olekstra.ru";
    public static final String DEFAULT_MASTER_TABLE = PackA.TABLE_NAME;
    public static final String DEFAULT_SLAVE_TABLE = PackB.TABLE_NAME;
    public static final Long DEFAULT_READ_CAPACITY = 5L;
    public static final Long DEFAULT_WRITE_CAPACITY = 10L;

    public static final String DEFAULT_STRING_VALUE = "";
    public static final List<String> DEFAULT_LIST_VALUE = new ArrayList<String>();

    public static final Integer DEFAULT_SMS_MAX_SEND_THRESHOLD = 1000;
    public static final Integer DEFAULT_SMS_NO_MAX_THRESHOLD = 0;
    public static final Integer DEFAULT_NON_REFUND_PERCENT = 5;
    public static final String DEFAULT_PACK_EKT = "0000000000000";
    public static final Integer EXPORT_BATCH_SIZE = 100;

    public static final String DEFAULT_TIME_ZONE = "Europe/Moscow";

    @Autowired
    public AppSettingsService(AppSettingsDao dao) {
        this.dao = dao;
        loadAppSettings();
    }

    public void loadAppSettings() {
        appSettings = dao.loadSettings();
        if (appSettings == null) {
            appSettings = new AppSettings();
        }
    }

    public void setPackMasterTableName(String tableName) {
        appSettings.setPackMasterTableName(tableName);
    }

    public String getPackMasterTableName() {
        String masterTable = appSettings.getPackMasterTableName();
        return masterTable != null ? masterTable : DEFAULT_MASTER_TABLE;
    }

    public void setPackSlaveTableName(String tableName) {
        appSettings.setPackSlaveTableName(tableName);
    }

    public String getPackSlaveTableName() {
        String slaveTable = appSettings.getPackSlaveTableName();
        return slaveTable != null ? slaveTable : DEFAULT_SLAVE_TABLE;
    }

    @SuppressWarnings("unchecked")
    public <T extends Pack> Class<T> getMasterTablePackEntity() {
        String masterTable = appSettings.getPackMasterTableName();
        if (masterTable.equals(PackA.TABLE_NAME)) {
            return (Class<T>) PackA.class;
        } else if (masterTable.equals(PackB.TABLE_NAME)) {
            return (Class<T>) PackB.class;
        } else
            return (Class<T>) PackA.class;
    }

    @SuppressWarnings("unchecked")
    public <T extends Pack> Class<T> getSlaveTablePackEntity() {
        String slaveTable = appSettings.getPackSlaveTableName();
        if (slaveTable.equals(PackA.TABLE_NAME)) {
            return (Class<T>) PackA.class;
        } else if (slaveTable.equals(PackB.TABLE_NAME)) {
            return (Class<T>) PackB.class;
        } else
            return (Class<T>) PackB.class;
    }

    public String getBackofficeReportBucketName() {
        String bucketName = appSettings.getBackofficeReportBucketName();
        return bucketName != null ? bucketName
                : DEFAULT_BACKOFFICE_REPORT_BUCKET;
    }

    public String getManufacturerReportBucketName() {
        String manufacturerName = appSettings.getManufacturerReportBucketName();
        return manufacturerName != null ? manufacturerName
                : DEFAULT_MANUFACTURER_REPORT_BUCKET;
    }

    public String getRecipeBucketName() {
        String recipeName = appSettings.getRecipeBucketName();
        return recipeName != null ? recipeName : DEFAULT_RECIPE_BUCKET;
    }

    public String getSmsGateSettings() {
        String smsGate = appSettings.getSmsGateSettings();
        return smsGate != null ? smsGate : DEFAULT_STRING_VALUE;
    }

    public Integer getSmsMaxSendThreshold() {
        Integer maxSendThreshold = appSettings.getSmsMaxSendThreshold();
        return maxSendThreshold != null ? maxSendThreshold
                : DEFAULT_SMS_MAX_SEND_THRESHOLD;
    }

    public List<String> getTicketStatusAll() {
        List<String> ticketStatusAll = appSettings.getTicketStatusAll();
        return ticketStatusAll != null ? ticketStatusAll : DEFAULT_LIST_VALUE;
    }

    public List<String> getTicketStatusNew() {
        List<String> ticketStatusNew = appSettings.getTicketStatusNew();
        return ticketStatusNew != null ? ticketStatusNew : DEFAULT_LIST_VALUE;
    }

    public List<String> getTicketStatusProcessing() {
        List<String> ticketStatusProcessing = appSettings
                .getTicketStatusProcessing();
        return ticketStatusProcessing != null ? ticketStatusProcessing
                : DEFAULT_LIST_VALUE;
    }

    public List<String> getTicketStatusClosed() {
        List<String> ticketStatusClosed = appSettings.getTicketStatusClosed();
        return ticketStatusClosed != null ? ticketStatusClosed
                : DEFAULT_LIST_VALUE;
    }

    public List<String> getTicketTheme() {
        List<String> ticketThemes = appSettings.getTicketTheme();
        return ticketThemes != null ? ticketThemes : DEFAULT_LIST_VALUE;
    }

    public String getTicketStatusReopen() {
        String ticketStatusReopen = appSettings.getTicketStatusReopen();
        return ticketStatusReopen != null ? ticketStatusReopen
                : DEFAULT_STRING_VALUE;
    }

    public String getOlekstraSupportPhone() {
        String olxSupportPhone = appSettings.getOlekstraSupportPhone();
        return olxSupportPhone != null ? olxSupportPhone : DEFAULT_STRING_VALUE;
    }

    public List<String> getTicketStatusNewTemp() {
        List<String> ticketStatusNewTemp = appSettings.getTicketStatusNewTemp();
        return ticketStatusNewTemp != null ? ticketStatusNewTemp
                : DEFAULT_LIST_VALUE;
    }

    public Long getReadCapacity() {
        Long readCapacity = appSettings.getReadCapactiy();
        return readCapacity != null ? readCapacity : DEFAULT_READ_CAPACITY;
    }

    public Long getWriteCapacity() {
        Long writeCapacity = appSettings.getWriteCapactiy();
        return writeCapacity != null ? writeCapacity : DEFAULT_WRITE_CAPACITY;
    }

    public String getTimeZoneDefault() {
        String timeZoneDefault = appSettings.getTimeZoneDefault();
        return timeZoneDefault != null ? timeZoneDefault : DEFAULT_TIME_ZONE;
    }

    public List<String> getTimeZones() {
        List<String> timeZones = appSettings.getTimeZones();
        return timeZones != null ? timeZones : DEFAULT_LIST_VALUE;
    }

    public Integer getNonRefundPercent() {
        Integer nonRefund = appSettings.getNonRefundPercent();
        return nonRefund != null ? nonRefund : DEFAULT_NON_REFUND_PERCENT;
    }

    public String getDefaultPackEkt() {
        String ekt = appSettings.getDefaultPackEkt();
        return ekt != null ? ekt : DEFAULT_PACK_EKT;
    }

    public String getApiTransactionsToken() {
        return appSettings.getApiTransactionsToken();
    }

    public List<String> getFavouriteCards() {
        return appSettings.getFavouriteCards();
    }
    
    public List<Formular> getFormularList() throws JsonParseException, JsonMappingException, IOException {
        return appSettings.getFormularList();
    }    

    /**
     * This method will be called automatically every 15 minutes to refresh app
     * settings.
     */
    // cron expression: invoke every 15 minutes at :00, e.g. :15 and so on
    @Scheduled(cron = "0 */15 * * * *")
    public void refreshAppSettings() {
        loadAppSettings();
    }
}
