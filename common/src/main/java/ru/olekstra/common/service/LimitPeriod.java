package ru.olekstra.common.service;

import java.util.Locale;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public enum LimitPeriod {
	

    DAY("day") {
        public String getCurrentPeriod(DateTime dateTime) {
            DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMdd");
            return formatter.print(dateTime);
        }
    },
    WEEK("week") {
        public String getCurrentPeriod(DateTime dateTime) {
            DateTimeFormatter yearFormatter = DateTimeFormat.forPattern("yyyy");
            DateTimeFormatter weekFormatter = DateTimeFormat.forPattern("ww")
                    .withLocale(new Locale("RU"));
            return yearFormatter.print(dateTime) + "w" + weekFormatter.print(dateTime);
        }
    },
    MONTH("month") {
        public String getCurrentPeriod(DateTime dateTime) {
            DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMM");
            return formatter.print(dateTime);
        }
    },
    QUARTER("quarter") {
        public String getCurrentPeriod(DateTime dateTime) {
            String currentQuarter;
            DateTimeFormatter monthFormatter = DateTimeFormat.forPattern("MM");
            DateTimeFormatter yearFormatter = DateTimeFormat.forPattern("yyyy");
            String year = yearFormatter.print(dateTime);
            Integer month = Integer.parseInt(monthFormatter.print(dateTime));
            Integer m = (month - 1) / 3 + 1;
            currentQuarter = year + "q" + m.toString();
            return currentQuarter;
        }
    },
    HALFYEAR("halfyear") {
        public String getCurrentPeriod(DateTime dateTime) {
            String currentHalfyear;
            DateTimeFormatter monthFormatter = DateTimeFormat.forPattern("MM");
            DateTimeFormatter yearFormatter = DateTimeFormat.forPattern("yyyy");
            String year = yearFormatter.print(dateTime);
            Integer month = Integer.parseInt(monthFormatter.print(dateTime));
            Integer m = (month - 1) / 6 + 1;
            currentHalfyear = year + "h" + m.toString();
            return currentHalfyear;
        }
    },
    YEAR("year") {
        public String getCurrentPeriod(DateTime dateTime) {
            DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy");
            return formatter.print(dateTime);
        }
    };

    String period;

    LimitPeriod(String period) {
        this.period = period;
    }

    public String getPeriod() {
        return this.period;
    }

    public abstract String getCurrentPeriod(DateTime dateTime);
    
	public static String parsePeriod(String usedPeriod){
        if (usedPeriod.matches("\\d{4}[0-1]\\d{1}[0-3]\\d{1}")) {
            return LimitPeriod.DAY.getPeriod();
        }
        if (usedPeriod.matches("\\d{4}[0-1]\\d{1}")) {
            return LimitPeriod.MONTH.getPeriod();
        }
        if (usedPeriod.matches("\\d{4}w\\d{1,2}")) {
            return LimitPeriod.WEEK.getPeriod();
        }        
        if (usedPeriod.matches("\\d{4}q[1-4]")) {
            return LimitPeriod.QUARTER.getPeriod();
        }
        if (usedPeriod.matches("\\d{4}h[1|2]")) {
            return LimitPeriod.HALFYEAR.getPeriod();
        }
        if (usedPeriod.matches("\\d{4}")) {
            return LimitPeriod.YEAR.getPeriod();
        }        
        throw new IllegalArgumentException("Cant parse period " + usedPeriod);		
	}    

}