package ru.olekstra.common.service;

import java.io.IOException;
import java.util.Iterator;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.olekstra.common.dao.DrugstoreDao;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.Drugstore;

@Service
public class DateTimeService {
    @Autowired
    AppSettingsService settingService;
    @Autowired
    private DrugstoreDao drugstoreCashe;

    private boolean isTimeZoneValid(String timeZoneName) {
        if (timeZoneName != null
                && DateTimeZone.getAvailableIDs().contains(timeZoneName))
            return true;
        else
            return false;
    }

    private DateTimeZone getDateTimeZone(String timeZoneName) {
        if (isTimeZoneValid(timeZoneName))
            return DateTimeZone.forID(timeZoneName);
        else
            return DateTimeZone.forID(AppSettingsService.DEFAULT_TIME_ZONE);
    }

    public DateTime createDefaultDateTime() {
        return DateTime.now().withZone(getDateTimeZone(settingService.getTimeZoneDefault()));
    }

    public DateTime createDateTimeWithZone(String zone) {
        return DateTime.now().withZone(getDateTimeZone(zone));
    }

    public DateTime parseDefaultTimeZomeDate(String pattern, String value) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
        return formatter.parseDateTime(value).withZone(this.getDateTimeZone(settingService.getTimeZoneDefault()));
    }

    public DateTime createDrugstoreDate(String drugstoreId) throws IOException {
        Drugstore drugstore = null;
        if (drugstoreId != null) {
            Iterator<Drugstore> i = drugstoreCashe.getDrugstoreNames().iterator();
            while (i.hasNext()) {
                Drugstore d = i.next();
                if (drugstoreId.equals(d.getId())) {
                    drugstore = d;
                    break;
                }
            }
        }

        if (drugstore == null) {
            throw new IllegalArgumentException("Drugstore not found!");
        }

        return this.createDateTimeWithZone(drugstore.getTimeZone());
    }

    public DateTime createCardDate(Card card) {
        if (card == null) {
            throw new IllegalArgumentException("Card not found.");
        }

        /*
         * TODO добавить обработку временной зоны карты (ее пока нет в самой
         * карте)
         */
        return this.createDefaultDateTime();
    }

    public DateTime createUTCDate() {
        return DateTime.now().withZone(DateTimeZone.UTC);
    }
}
