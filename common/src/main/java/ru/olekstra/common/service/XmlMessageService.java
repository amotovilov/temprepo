package ru.olekstra.common.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ru.olekstra.awsutils.exception.OlekstraException;
import ru.olekstra.azure.model.IMessage;
import ru.olekstra.azure.model.Message;
import ru.olekstra.common.helper.XmlMessageHelper;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.DrugstorePackIndex;
import ru.olekstra.domain.Pack;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.dto.CardDatesSetMessage;
import ru.olekstra.domain.dto.CardPlanChangeMessage;
import ru.olekstra.domain.dto.DiscountDataCleanMessage;
import ru.olekstra.domain.dto.DrugstoreUpdateMessage;
import ru.olekstra.domain.dto.PackDiscountData;
import ru.olekstra.domain.dto.ProcessingTransactionMessage;
import ru.olekstra.domain.dto.TransactionTotalInfoDto;
import ru.olekstra.domain.dto.VoucherResponseRow;

@Service
public class XmlMessageService {

    public static final String CARD_UPDATE = "card-update";
    public static final String TRANSACTION = "transaction-limits-recalc";
    public static final String TRANSACTION_COMPLETE = "transaction-complete";
    public static final String DISCOUNT_CLEANUP = "discount-cleanup";
    public static final String CATALOG_ITEM_UPDATE = "catalog-item-update";
    public static final String DRUGSTORE_PACK_UPDATE = "drugstore-pack-mapping-update";
    public static final String DRUGSTORE_UPDATE = "drugstore-update";
    public static final String CARD_LOCK = "card-lock";
    public static final String CARD_UNLOCK = "card-unlock";
    public static final String CARD_DATES_SET = "card-dates-set";
    public static final String CARD_CREATE = "card-create";
    public static final String CARD_DISCOUNT_PLAN_SET = "card-discount-plan-set";
    public static final String DAILY_TRANSACTION_TOTALS = "daily-transaction-totals";
    public static final String RUN_DAILY_TASKS = "run-daily-tasks";

    @Autowired
    private AppSettingsService settingsService;

    @Autowired
    private XmlMessageHelper helper;

    private static final Logger LOGGER = Logger.getLogger(XmlMessageService.class);

    private Pack makePack(Class<Pack> pack, String id, String ekt, String name,
            String barcode, String declNumber, String manufacturer,
            String synonym, String tradeName, String inn) {
        try {
            return pack.getConstructor(
                    new Class[] { String.class, String.class, String.class,
                            String.class, String.class, String.class,
                            String.class, String.class, String.class })
                    .newInstance(id, ekt, barcode, declNumber, name,
                            manufacturer, synonym, tradeName, inn);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public IMessage createCardUpdateMessage(Card card)
            throws ParserConfigurationException, TransformerException {
        Document doc = helper.createNewDocument(XmlMessageHelper.MESSAGING_NAMESPACE, CARD_UPDATE);
        Element cardElement = helper.createElement(doc, XmlMessageHelper.OBJECTS_NAMESPACE, "card");

        if (card.getNumber() != null) {
            cardElement.setAttribute("number", card.getNumber());
        }

        cardElement.appendChild(helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE, "disabled",
                DatatypeConverter.printBoolean(card.getDisabled())));

        if (card.getDiscountPlanId() != null) {
            cardElement.appendChild(helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE,
                    "discount-plan-id", card.getDiscountPlanId()));
        }
        if (card.getHolderName() != null) {
            cardElement.appendChild(helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE, "holder",
                    card.getHolderName()));
        }
        if (card.getTelephoneNumber() != null) {
            cardElement.appendChild(helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE, "phone",
                    card.getTelephoneNumber()));
        }
        if (card.getLimitRemain() != null) {
            cardElement.appendChild(helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE, "balance",
                    DatatypeConverter.printDecimal(card.getLimitRemain())));
        }
        if (card.getStartDate() != null) {
            cardElement.appendChild(helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE, "start-date",
                    card.getStartDate().toString()));

        }
        if (card.getEndDate() != null) {
            cardElement.appendChild(helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE, "end-date",
                    card.getEndDate().toString()));
        }
        if (card.getCvv() != null) {
            cardElement.appendChild(helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE, "cvv",
                    card.getCvv()));
        }
        if (card.getCardBatches() != null) {
            Element batches = helper.createElement(doc, XmlMessageHelper.OBJECTS_NAMESPACE, "batches");
            for (String batch : card.getCardBatches()) {
                batches.appendChild(helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE, "batch",
                        batch));
            }
            cardElement.appendChild(batches);
        }
        doc.getDocumentElement().appendChild(cardElement);
        Message msg = new Message();
        msg.setBody(helper.documentToString(doc));
        msg.setLabel(CARD_UPDATE);
        LOGGER.info(msg.getBody());
        return msg;
    }

    public IMessage createTransactionCompleteMessage(ProcessingTransaction transaction)
            throws ParserConfigurationException, JsonParseException, JsonMappingException, DOMException, IOException,
            OlekstraException, TransformerException {

        if (!transaction.isComplete())
            throw new OlekstraException("Transaction should be completed to perform message send operation");

        Document doc = helper.createNewDocument(XmlMessageHelper.MESSAGING_NAMESPACE, "transaction-complete");
        Element transactionElement = helper.createElement(doc, XmlMessageHelper.OBJECTS_NAMESPACE, "transaction");

        // атрибуты - из ключей, считаем, что null-ами быть не могут
        transactionElement.setAttribute("id", transaction.getVoucherId().toString());
        DateTimeFormatter periodFormatter = DateTimeFormat.forPattern("yyyyMM");
        transactionElement.setAttribute("period", periodFormatter.print(transaction.getDate()));

        Element dateElement = helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE, "date", transaction
                .getCompleteTime().toString());
        transactionElement.appendChild(dateElement);

        // хэш-ключ таблицы, считаем, что null-ом быть не может
        Element drugstoreElement = helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE,
                "drugstore-id", transaction.getDrugstoreId());
        transactionElement.appendChild(drugstoreElement);

        if (transaction.getCard().getNumber() != null) {
            Element cardElement = helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE, "card-number",
                    transaction.getCard().getNumber());
            transactionElement.appendChild(cardElement);
        }

        if (transaction.getDiscountPlanId() != null) {
            Element discountElement = helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE,
                    "discount-plan-id", transaction.getDiscountPlanId());
            transactionElement.appendChild(discountElement);
        }

        // Rows element
        List<VoucherResponseRow> rows = transaction.getRows();
        if (rows != null && !rows.isEmpty()) {
            Element rowsElement = helper.createElement(doc, XmlMessageHelper.OBJECTS_NAMESPACE, "rows");

            for (VoucherResponseRow row : rows) {
                Element rowElement = helper.createElement(doc, XmlMessageHelper.OBJECTS_NAMESPACE, "row");
                rowElement.setAttribute("number", DatatypeConverter.printInt(row.getNumber()));

                if (row.getId() != null) {
                    Element packIdElement = helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE,
                            "item-id", row.getId());
                    rowElement.appendChild(packIdElement);
                }

                if (row.getName() != null) {
                    Element packNameElement = helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE,
                            "item-name", row.getName());
                    rowElement.appendChild(packNameElement);
                }

                if (row.getClientPackId() != null) {
                    Element clientPackIdElement = helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE,
                            "client-item-id", row.getClientPackId());
                    rowElement.appendChild(clientPackIdElement);
                }

                if (row.getClientPackName() != null) {
                    Element clientPackNameElement = helper.createElementWithText(doc,
                            XmlMessageHelper.OBJECTS_NAMESPACE,
                            "client-item-name", row.getClientPackName());
                    rowElement.appendChild(clientPackNameElement);
                }

                Element quantityElement = helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE,
                        "count", DatatypeConverter.printInt(row.getQuantity()));
                rowElement.appendChild(quantityElement);

                if (row.getPrice() != null) {
                    Element priceElement = helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE,
                            "price", DatatypeConverter.printDecimal(row.getPrice()));
                    rowElement.appendChild(priceElement);
                }

                if (row.getSumWithoutDiscount() != null) {
                    Element sumElement = helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE,
                            "sum-without-discount", DatatypeConverter.printDecimal(row
                                    .getSumWithoutDiscount()));
                    rowElement.appendChild(sumElement);
                }

                if (row.getDiscountPercent() != null) {
                    Element percentElement = helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE,
                            "discount-value", DatatypeConverter.printDecimal(row
                                    .getDiscountPercent()));
                    rowElement.appendChild(percentElement);
                }

                if (row.getDiscountSum() != null) {
                    Element discountSumElement = helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE,
                            "discount-sum", DatatypeConverter.printDecimal(row
                                    .getDiscountSum()));
                    rowElement.appendChild(discountSumElement);
                }

                if (row.getSumToPay() != null) {
                    Element sumToPayElement = helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE,
                            "sum-to-pay", DatatypeConverter.printDecimal(row.getSumToPay()));
                    rowElement.appendChild(sumToPayElement);
                }

                if (row.getRefundSum() != null) {
                    Element refundSumElement = helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE,
                            "refund-sum", DatatypeConverter.printDecimal(row.getRefundSum()));
                    rowElement.appendChild(refundSumElement);
                }

                // TODO исправить на проверку специального поля, обозначающего,
                // что в объекте есть exact-поля
                // (они должны присутствовать либо все три, либо ни одного)

                if (row.getExactNonRefundSum() != null) {
                    BigDecimal exactSumToPay = BigDecimal.ZERO;
                    if (row.getExactSumToPay() != null) {
                        exactSumToPay = row.getExactSumToPay();
                    }
                    Element exactSumEToPayElement = helper.createElementWithText(doc,
                            XmlMessageHelper.OBJECTS_NAMESPACE,
                            "exact-sum-to-pay", DatatypeConverter.printDecimal(exactSumToPay));
                    rowElement.appendChild(exactSumEToPayElement);

                    BigDecimal exactRefundSum = BigDecimal.ZERO;
                    if (row.getExactRefundSum() != null) {
                        exactRefundSum = row.getExactRefundSum();
                    }
                    Element exactRefundSumElement = helper.createElementWithText(doc,
                            XmlMessageHelper.OBJECTS_NAMESPACE,
                            "exact-refund-sum", DatatypeConverter.printDecimal(exactRefundSum));
                    rowElement.appendChild(exactRefundSumElement);

                    BigDecimal exactNonRefundSum = BigDecimal.ZERO;
                    if (row.getExactNonRefundSum() != null) {
                        exactNonRefundSum = row.getExactNonRefundSum();
                    }
                    Element exactNonRefundSumElement = helper.createElementWithText(doc,
                            XmlMessageHelper.OBJECTS_NAMESPACE,
                            "exact-non-refund-sum", DatatypeConverter.printDecimal(exactNonRefundSum));
                    rowElement.appendChild(exactNonRefundSumElement);

                } // exact

                if (row.getPackDiscount() != null && row.getPackDiscount().length > 0) {
                    Element discountDetailsElement = helper.createElement(doc, XmlMessageHelper.OBJECTS_NAMESPACE,
                            "discount-details");

                    for (PackDiscountData packDiscountData : row.getPackDiscount()) {
                        Element discountDetailElement = helper.createElement(doc, XmlMessageHelper.OBJECTS_NAMESPACE,
                                "discount-detail");

                        if (packDiscountData.getDiscountId() != null) {
                            Element discountIdElement = helper.createElementWithText(doc,
                                    XmlMessageHelper.OBJECTS_NAMESPACE,
                                    "discount-id", packDiscountData.getDiscountId());
                            discountDetailElement.appendChild(discountIdElement);
                        }

                        if (packDiscountData.getUsedPercent() != null) {
                            Element usedPercentElement = helper.createElementWithText(doc,
                                    XmlMessageHelper.OBJECTS_NAMESPACE,
                                    "percent", DatatypeConverter.printDecimal(packDiscountData.getUsedPercent()));
                            discountDetailElement.appendChild(usedPercentElement);
                        }

                        if (packDiscountData.getUsedSum() != null) {
                            Element usedSumElement = helper.createElementWithText(doc,
                                    XmlMessageHelper.OBJECTS_NAMESPACE, "sum", DatatypeConverter
                                            .printDecimal(packDiscountData.getUsedSum()));
                            discountDetailElement.appendChild(usedSumElement);
                        }

                        discountDetailsElement.appendChild(discountDetailElement);
                    }
                    rowElement.appendChild(discountDetailsElement);
                }
                rowsElement.appendChild(rowElement);
                // reversed
                if (row.isReversed()) {
                    Element rowStornElement = (Element) rowElement.cloneNode(true);

                    Element count = (Element) helper.findNodeInList("count",
                            rowStornElement.getChildNodes());
                    if (count != null) {
                        count.getChildNodes().item(0)
                                .setNodeValue(DatatypeConverter.printInt(-row.getQuantity()));
                    }
                    if (row.getSumToPay() != null) {
                        Element sumToPay = (Element) helper.findNodeInList("sum-to-pay",
                                rowStornElement.getChildNodes());
                        if (sumToPay != null) {
                            sumToPay.getChildNodes().item(0)
                                    .setNodeValue(DatatypeConverter.printDecimal(row.getSumToPay().negate()));
                        }
                    }
                    if (row.getDiscountSum() != null) {
                        Element discountSum = (Element) helper.findNodeInList("discount-sum",
                                rowStornElement.getChildNodes());
                        if (discountSum != null) {
                            discountSum.getChildNodes().item(0)
                                    .setNodeValue(DatatypeConverter.printDecimal(row.getDiscountSum().negate()));
                        }
                    }
                    if (row.getRefundSum() != null) {
                        Element refundSum = (Element) helper.findNodeInList("refund-sum",
                                rowStornElement.getChildNodes());
                        if (refundSum != null) {
                            refundSum.getChildNodes().item(0)
                                    .setNodeValue(DatatypeConverter.printDecimal(row.getRefundSum().negate()));
                        }
                    }
                    if (row.getSumWithoutDiscount() != null) {
                        Element sumWithoutDiscount = (Element) helper.findNodeInList("sum-without-discount",
                                rowStornElement.getChildNodes());
                        if (sumWithoutDiscount != null) {
                            sumWithoutDiscount.getChildNodes().item(0)
                                    .setNodeValue(DatatypeConverter.printDecimal(row.getSumWithoutDiscount().negate()));
                        }
                    }
                    if (row.getCardLimitUsedSum() != null) {
                        Element cardLimitUsed = (Element) helper.findNodeInList("card-limit-used",
                                rowStornElement.getChildNodes());
                        if (cardLimitUsed != null) {
                            cardLimitUsed.getChildNodes().item(0)
                                    .setNodeValue(DatatypeConverter.printDecimal(row.getCardLimitUsedSum().negate()));
                        }
                    }
                    if (row.getExactSumToPay() != null) {
                        Element exactSumToPay = (Element) helper.findNodeInList("exact-sum-to-pay",
                                rowStornElement.getChildNodes());
                        if (exactSumToPay != null) {
                            exactSumToPay.getChildNodes().item(0)
                                    .setNodeValue(DatatypeConverter.printDecimal(row.getExactSumToPay().negate()));
                        }
                    }
                    if (row.getExactRefundSum() != null) {
                        Element exactRefundSum = (Element) helper.findNodeInList("exact-refund-sum",
                                rowStornElement.getChildNodes());
                        if (exactRefundSum != null) {
                            exactRefundSum.getChildNodes().item(0)
                                    .setNodeValue(DatatypeConverter.printDecimal(row.getExactRefundSum().negate()));
                        }
                    }
                    if (row.getExactNonRefundSum() != null) {
                        Element exactNonRefundSum = (Element) helper.findNodeInList("exact-non-refund-sum",
                                rowStornElement.getChildNodes());
                        if (exactNonRefundSum != null) {
                            exactNonRefundSum.getChildNodes().item(0)
                                    .setNodeValue(DatatypeConverter.printDecimal(row.getExactNonRefundSum().negate()));
                        }
                    }
                    if (row.getPackDiscount() != null && row.getPackDiscount().length > 0) {
                        NodeList details = (NodeList) helper.findNodeInList("discount-details",
                                rowStornElement.getChildNodes());
                        for (int i = 0; i < details.getLength(); i++) {
                            Node node = details.item(i);
                            if (!"discount-detail".equals(node.getLocalName())) {
                                continue;
                            }
                            Node sum = helper.findNodeInList("sum", node.getChildNodes()).getFirstChild();
                            sum.setNodeValue(DatatypeConverter.printDecimal(DatatypeConverter.parseDecimal(
                                    sum.getNodeValue()).negate()));
                        }
                    }
                    rowStornElement.setAttribute("number",
                            Integer.toString(-DatatypeConverter.parseInt(rowStornElement.getAttribute("number"))));
                    rowsElement.appendChild(rowStornElement);
                }
            }
            transactionElement.appendChild(rowsElement);
        } // end Rows element

        if (transaction.getSumWithoutDiscount() != null) {
            Element sumWithoutDiscountElement = helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE,
                    "sum-without-discount", DatatypeConverter.printDecimal(transaction.getSumWithoutDiscount()));
            transactionElement.appendChild(sumWithoutDiscountElement);
        }

        if (transaction.getDiscountSum() != null) {
            Element discountSumElement = helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE,
                    "discount-sum", DatatypeConverter.printDecimal(transaction.getDiscountSum()));
            transactionElement.appendChild(discountSumElement);
        }

        if (transaction.getSumToPay() != null) {
            Element sumToPayElement = helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE,
                    "sum-to-pay", DatatypeConverter
                            .printDecimal(transaction.getSumToPay()));
            transactionElement.appendChild(sumToPayElement);
        }

        if (transaction.getRefundSum() != null) {
            Element refundSumElement = helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE,
                    "refund-sum", DatatypeConverter
                            .printDecimal(transaction.getRefundSum()));
            transactionElement.appendChild(refundSumElement);
        }

        // TODO исправить на проверку специального поля, обозначающего, что в
        // объекте есть exact-поля
        // (они должны присутствовать либо все три, либо ни одного)

        if (transaction.getExactNonRefundSum() != null) {
            Element exactSumToPayElement = helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE,
                    "exact-sum-to-pay", DatatypeConverter
                            .printDecimal(transaction.getExactSumToPay()));
            transactionElement.appendChild(exactSumToPayElement);

            Element exactRefundSumElement = helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE,
                    "exact-refund-sum", DatatypeConverter
                            .printDecimal(transaction.getExactRefundSum()));
            transactionElement.appendChild(exactRefundSumElement);

            Element exactNonRefundSumElement = helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE,
                    "exact-non-refund-sum", DatatypeConverter
                            .printDecimal(transaction.getExactNonRefundSum()));
            transactionElement.appendChild(exactNonRefundSumElement);
        }

        if (transaction.getAuthCode() != null) {
            Element authCodeElement = helper.createElementWithText(doc, XmlMessageHelper.OBJECTS_NAMESPACE,
                    "auth-code", transaction.getAuthCode());
            transactionElement.appendChild(authCodeElement);
        }

        doc.getDocumentElement().appendChild(transactionElement);
        Message msg = new Message();
        msg.setBody(helper.documentToString(doc));
        LOGGER.info(msg.getBody());
        return msg;
    }

    public IMessage createDiscountDataCleanMessage(DiscountDataCleanMessage message)
            throws ParserConfigurationException, TransformerException {
        Document doc = helper.createNewDocument(XmlMessageHelper.MESSAGING_NAMESPACE, DISCOUNT_CLEANUP);
        doc.getDocumentElement().appendChild(
                helper.createElementWithText(doc, XmlMessageHelper.MESSAGING_NAMESPACE, "discount-id",
                        message.getDiscountId()));

        Message msg = new Message();
        msg.setBody(helper.documentToString(doc));
        msg.setLabel(DISCOUNT_CLEANUP);
        LOGGER.info(msg.getBody());
        return msg;
    }

    public String getDayString(DateTime dateTime) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        return formatter.print(dateTime);
    }
    
    public IMessage createDailyTransactionTotalsMessage(List<TransactionTotalInfoDto> items, DateTime day)
            throws ParserConfigurationException, TransformerException {
        Document doc = helper.createNewDocument(XmlMessageHelper.MESSAGING_NAMESPACE, DAILY_TRANSACTION_TOTALS);

        for (TransactionTotalInfoDto item : items) {
            Element totals = helper.createElement(doc, XmlMessageHelper.MESSAGING_NAMESPACE, "totals");
            totals.setAttribute("key", item.getLastDigits());
            totals.appendChild(helper.createElementWithText(doc, XmlMessageHelper.MESSAGING_NAMESPACE, "count", item
                    .getCount().toString()));
            totals.appendChild(helper.createElementWithText(doc, XmlMessageHelper.MESSAGING_NAMESPACE,
                    "total-sum-to-pay", item.getTotalSumToPay().toString()));
            doc.getDocumentElement().appendChild(totals);
        }
        doc.getDocumentElement().setAttribute("day", getDayString(day));

        Message msg = new Message();
        msg.setBody(helper.documentToString(doc));
        msg.setLabel(DAILY_TRANSACTION_TOTALS);
        LOGGER.info(msg.getBody());
        return msg;
    }

    public Pack parseCatalogItem(Document document)
            throws XPathExpressionException, IllegalArgumentException, IOException, ParserConfigurationException,
            SAXException {

        XPath xpath = helper.prepareXPath(document);

        String id = xpath.compile("/mes:catalog-item-update/obj:catalog-item/@id").evaluate(document);
        String ekt = id;
        String name = xpath.compile(
                "/mes:catalog-item-update/obj:catalog-item/obj:name").evaluate(document);
        String manufacturer = xpath.compile(
                "/mes:catalog-item-update/obj:catalog-item/obj:properties/obj:property[obj:name='manufacturer']/"
                        + "obj:value").evaluate(document);
        String inn = xpath.compile(
                "/mes:catalog-item-update/obj:catalog-item/obj:properties/obj:property[obj:name='inn']/"
                        + "obj:value")
                .evaluate(document);
        String tradeName = xpath.compile(
                "/mes:catalog-item-update/obj:catalog-item/obj:properties/obj:property[obj:name='tradeName']/"
                        + "obj:value")
                .evaluate(document);
        String barcode = xpath.compile(
                "/mes:catalog-item-update/obj:catalog-item/obj:properties/obj:property[obj:name='barcode']/"
                        + "obj:value")
                .evaluate(document);
        String synonym = xpath.compile(
                "/mes:catalog-item-update/obj:catalog-item/obj:properties/obj:property[obj:name='synonym']/"
                        + "obj:value")
                .evaluate(document);
        String declNumber = "";

        Pack pack = makePack(settingsService.getMasterTablePackEntity(), id, ekt, name, barcode, declNumber,
                manufacturer, synonym,
                tradeName, inn);

        return pack;
    }

    public DiscountDataCleanMessage parseDiscountDataCleanMessage(Document document)
            throws XPathExpressionException {
        XPath xpath = helper.prepareXPath(document);

        String discountId = xpath.compile("/mes:discount-cleanup/mes:discount-id").evaluate(document);
        DiscountDataCleanMessage result = new DiscountDataCleanMessage();
        result.setDiscountId(discountId);
        return result;
    }

    public ProcessingTransactionMessage parseProcessingTransactionMessage(Document document)
            throws XPathExpressionException {
        XPath xpath = helper.prepareXPath(document);

        String transactionId = xpath.compile("/mes:transaction-complete/obj:transaction/@id").evaluate(document);
        DateTime transactionDate = new DateTime(xpath.compile("/mes:transaction-complete/obj:transaction/obj:date")
                .evaluate(document));
        String drugstoreId = xpath.compile("/mes:transaction-complete/obj:transaction/obj:drugstore-id").evaluate(
                document);
        BigDecimal sumToPay = new BigDecimal(xpath.compile("/mes:transaction-complete/obj:transaction/obj:sum-to-pay")
                .evaluate(
                        document));
        String cardNumber = xpath.compile("/mes:transaction-complete/obj:transaction/obj:card-number").evaluate(
                document);
        Integer count = Integer.parseInt(xpath.compile(
                "sum(/mes:transaction-complete/obj:transaction/obj:rows/obj:row/obj:count)").evaluate(
                document));
        String transactionKey = ProcessingTransaction.buildRangeKey(transactionDate, UUID.fromString(transactionId));
        ProcessingTransactionMessage result = new ProcessingTransactionMessage();
        result.setTrasactionRangeKey(transactionKey);
        result.setDrugstoreId(drugstoreId);
        result.setSumToPay(sumToPay);
        result.setCardNumber(cardNumber);
        result.setCount(count);
        result.setDate(transactionDate);
        return result;
    }

    public List<DrugstorePackIndex> parseDrugstorePackMessage(Document document) throws XPathExpressionException {
        XPath xpath = helper.prepareXPath(document);

        String drugstoreId = xpath.compile("/mes:drugstore-pack-mapping-update/mes:drugstore-id").evaluate(document);
        String drugstorePackId = xpath.compile("/mes:drugstore-pack-mapping-update/mes:drugstore-pack-id").evaluate(
                document);
        String drugstorePackName = xpath.compile("/mes:drugstore-pack-mapping-update/mes:drugstore-pack-name")
                .evaluate(document);
        String packId = xpath.compile("/mes:drugstore-pack-mapping-update/obj:catalog-item-ref").evaluate(document);
        String formularId = xpath.compile("/mes:drugstore-pack-mapping-update/obj:catalog-item-ref/@catalog-id")
                .evaluate(document);

        DrugstorePackIndex packIdIndex = new DrugstorePackIndex(drugstoreId, drugstorePackId, formularId, packId);
        DrugstorePackIndex packNameIndex = new DrugstorePackIndex(drugstoreId, drugstorePackName, formularId, packId);

        return Arrays.asList(new DrugstorePackIndex[] { packIdIndex, packNameIndex });
    }

    public String parseCardCreateMessage(Document document) throws XPathExpressionException {
        XPath xpath = helper.prepareXPath(document);

        String cardCode = xpath.compile("/mes:card-create/mes:number").evaluate(document);
        return cardCode;
    }

    public CardPlanChangeMessage parseCardPlanChange(Document document) throws XPathExpressionException {
        XPath xpath = helper.prepareXPath(document);

        String cardCode = xpath.compile("/mes:card-discount-plan-set/@number").evaluate(document);
        String planId = xpath.compile("/mes:card-discount-plan-set/mes:discount-plan-id").evaluate(document);

        CardPlanChangeMessage result = new CardPlanChangeMessage(cardCode, planId);

        return result;
    }

    public DrugstoreUpdateMessage parseDrugstoreUpdate(Document document) throws XPathExpressionException {
    	DrugstoreUpdateMessage result = new DrugstoreUpdateMessage();
        XPath xpath = helper.prepareXPath(document);
        result.setId(xpath.compile("/mes:drugstore-update/obj:drugstore/@id").evaluate(document));
        result.setName(xpath.compile("/mes:drugstore-update/obj:drugstore/obj:name").evaluate(document));
        String disable = xpath.compile("/mes:drugstore-update/obj:drugstore/obj:disabled").evaluate(document);
        result.setDisabled(DatatypeConverter.parseBoolean(disable));
        result.setTimezone(xpath.compile("/mes:drugstore-update/obj:drugstore/obj:timezone").evaluate(document));
        return result;
    }

    /**
     * 
     * @param document
     * @return String cardNumber
     * @throws XPathExpressionException
     */
    public String parseCardLock(Document document) throws XPathExpressionException {
        XPath xpath = helper.prepareXPath(document);

        String cardNumber = xpath.compile("/mes:card-lock/@number").evaluate(document);

        return cardNumber;
    }

    /**
     * 
     * @param document
     * @return String cardNumber
     * @throws XPathExpressionException
     */
    public String parseCardUnlock(Document document) throws XPathExpressionException {
        XPath xpath = helper.prepareXPath(document);

        String cardNumber = xpath.compile("/mes:card-unlock/@number").evaluate(document);

        return cardNumber;
    }

    public CardDatesSetMessage parseCardDatesSet(Document document) throws XPathExpressionException {
        XPath xpath = helper.prepareXPath(document);

        String cardNumber = xpath.compile("/mes:card-dates-set/@number").evaluate(document);
        DateTime startDate = null;
        DateTime endDate = null;
        String startDateString = xpath.compile("/mes:card-dates-set/mes:start-date").evaluate(document);
        if (StringUtils.isNotEmpty(startDateString)) {
            startDate = new DateTime(startDateString);
        }
        String endDateString = xpath.compile("/mes:card-dates-set/mes:end-date").evaluate(document);
        if (StringUtils.isNotEmpty(endDateString)) {
            endDate = new DateTime(endDateString);
        }

        return new CardDatesSetMessage(cardNumber, startDate, endDate);
    }

    public DateTime parseRunDailyTasks(Document document) throws XPathExpressionException {
        XPath xpath = helper.prepareXPath(document);

        String day = xpath.compile("/mes:run-daily-tasks/mes:day").evaluate(document);

        return new DateTime(day);
    }
}
