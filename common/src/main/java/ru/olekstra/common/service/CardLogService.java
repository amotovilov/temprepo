package ru.olekstra.common.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import ru.olekstra.awsutils.ComplexKey;
import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.common.util.MessagesUtil;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.CardLog;
import ru.olekstra.domain.DiscountLimit;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.dto.CardLogType;
import ru.olekstra.domain.dto.DiscountLimitRemaining;
import ru.olekstra.domain.dto.VoucherResponseRow;

import com.amazonaws.AmazonServiceException;

@Service
public class CardLogService {

    private DynamodbService dynamodbService;
    private MessageSource messageSource;
    @Autowired
    DateTimeService dateService;

    @Autowired
    public CardLogService(DynamodbService dynamodbService,
            MessageSource messageSource) {
        this.dynamodbService = dynamodbService;
        this.messageSource = messageSource;
    }

    public CardLog createNewTransactionTypeCardLog(
            ProcessingTransaction transaction, Card card) {
        String note = messageSource.getMessage("msg.drugstorecardchecknote",
                new Object[] {transaction.getDrugstoreName()}, null);
        CardLog cardLog = new CardLog(card.getNumber(),
                transaction.getStartTime(),
                note,
                CardLogType.CARD_CHECK,
                false);
        cardLog.setInternalUse(false);
        cardLog.setVoucherId(transaction.getVoucherId());
        cardLog.setDrugstoreId(transaction.getDrugstoreId());
        
        return cardLog;
    }

    public CardLog createCompletedTransactionCardLog(
            ProcessingTransaction transaction)
            throws JsonParseException, JsonMappingException, IOException {
        String note = messageSource.getMessage("msg.transactioncompletenote",
                new Object[] {
                        transaction.getDrugstoreName(),
                        transaction.getSumWithoutDiscount().toString(),
                        transaction.getDiscountSum().toString(),
                        transaction.getSumToPay().toString()
                }, null);
        CardLog cardLog = new CardLog(transaction.getCard().getNumber(),
                transaction.getStartTime(),
                note,
                CardLogType.TRANSACTION_COMPLETE,
                false);
        cardLog.setInternalUse(false);
        cardLog.setVoucherId(transaction.getVoucherId());
        cardLog.setDrugstoreId(transaction.getDrugstoreId());
        cardLog.setCardLogId(transaction.getCardLogId());
        return cardLog;
    }

    public List<CardLog> createTransactionRowTypeCardLog(
            ProcessingTransaction transaction, Card card)
            throws JsonParseException, JsonMappingException, IOException {
        List<CardLog> cardLogList = null;
        List<VoucherResponseRow> rows = transaction.getRows();
        if (rows != null && rows.size() > 0) {
            cardLogList = new ArrayList<CardLog>();
            for (VoucherResponseRow row : rows) {
                // TODO создаем лог-записи для каждой VoucherResponseRow
            }
        }
        return cardLogList;
    }

    public CardLog createDisableCardTypeCardLog(Card card, String lockReason, String jiraTask, boolean disable)
            throws JsonGenerationException, JsonMappingException, IOException,
            ItemSizeLimitExceededException {

        String cardNote = messageSource.getMessage(
                disable ? "msg.cardDisabled" : "msg.cardEnabled",
                new Object[] {lockReason, jiraTask}, null);

        CardLog cardLog = new CardLog(card.getNumber(), dateService.createCardDate(card), cardNote,
                disable ? CardLogType.DISABLED : CardLogType.ENABLED,
                false);

        return cardLog;
    }

    public CardLog createLimitRemainCardLog(String cardNumber, DiscountLimitRemaining limit, DateTime date) {
        String currentLimitGroup = DiscountLimit.isAllPurposeLimitGroupId(limit.getLimitGroupId()) ?
                messageSource.getMessage("msg.defaultLimitGroupName",
                        new Object[] {}, null) : limit.getLimitGroupId();
        String cardNote = messageSource.getMessage("msg.limitRemaining",
                new Object[] {
                        limit.getPeriod(),
                        currentLimitGroup,
                        limit.getDiscountId(),
                        limit.getRemainingAveragePackCount() == null ? 0l : limit.getRemainingAveragePackCount(),
                        limit.getRemainingDiscountSum() == null ? 0l : MessagesUtil.bigDecimalToString(limit.getRemainingDiscountSum()),
                        limit.getRemainingPackCount() == null ? 0l : limit.getRemainingPackCount(),
                        limit.getRemainingSumBeforeDiscount() == null ? 0l : MessagesUtil.bigDecimalToString(limit.getRemainingSumBeforeDiscount())
                }, null);
        
        CardLog cardLog = new CardLog(cardNumber, date, cardNote, CardLogType.LIMIT_REMAINING, true);
        return cardLog;
    }

    public CardLog createCardLog(String cardNumber, String drugstoreId,
            DateTime date, CardLogType type, boolean internal) {
        return new CardLog(cardNumber, date, drugstoreId, type, internal);

    }

    public CardLog createCardLog(String cardNumber, String note, String drugstoreId,
            DateTime date, CardLogType type, boolean internal) {
        CardLog cardLog = createCardLog(cardNumber, drugstoreId, date, type, internal);
        cardLog.setNote(note);

        return cardLog;
    }

    public CardLog createTransactionCardLog(ProcessingTransaction transaction, String note, int rowNumber)
            throws JsonParseException, JsonMappingException, IOException {
        CardLog cardLog = new CardLog(transaction.getCard().getNumber(),
                transaction.getStartTime(),
                note,
                CardLogType.TRANSACTION,
                false);
        cardLog.setInternalUse(false);
        cardLog.setVoucherId(transaction.getVoucherId());
        cardLog.setDrugstoreId(transaction.getDrugstoreId());
        cardLog.setRowNum(rowNumber);
        return cardLog;
    }

    public CardLog createLimitCardLog(String cardNumber, String note,
            String user, boolean notificationSent, boolean internalUse) throws JsonParseException,
            JsonMappingException, IOException {
        CardLog cardLog = createCardLog(cardNumber, null, dateService.createDefaultDateTime(), CardLogType.LIMIT,
                internalUse);
        cardLog.setNote(note);
        cardLog.setNotificationSend(notificationSent);
        cardLog.setUser(user);

        return cardLog;
    }

    public CardLog createLimitCardLog(String cardNumber, String drugstoreId, String note, DateTime date) {
        CardLog cardLog = createCardLog(cardNumber, drugstoreId, date, CardLogType.LIMIT, false);
        cardLog.setNote(note);

        return cardLog;
    }

    public CardLog createChangePlanCardLog(String cardNumber,
            String note, boolean internal, String user) {
        CardLog cardLog = createCardLog(cardNumber, note, null,
                dateService.createDefaultDateTime(), CardLogType.CHANGE_PLAN, true);
        cardLog.setUser(user);

        return cardLog;
    }

    public CardLog createTransactionStornoCardLog(String cardNumber, String drugstoreId, String note,
            Integer rowNumber, String user, boolean notificationSent,
            DateTime date) throws JsonParseException, JsonMappingException, IOException {
        CardLog cardLog = createCardLog(cardNumber, drugstoreId, date, CardLogType.TRANSACTION_STORNO, false);
        cardLog.setUser(user);
        cardLog.setNotificationSend(notificationSent);
        cardLog.setNote(note);
        cardLog.setRowNum(rowNumber);
        cardLog.setDrugstoreId(drugstoreId);

        return cardLog;
    }

    public boolean saveCardLog(CardLog cardLog)
            throws ItemSizeLimitExceededException {
        try {
            dynamodbService.putObjectOrDie(cardLog);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean saveCardLog(List<CardLog> cardLogList)
            throws AmazonServiceException, IllegalArgumentException,
            InterruptedException, ItemSizeLimitExceededException {
        List<ComplexKey> unprocessed = new ArrayList<ComplexKey>();

        int batchSize = dynamodbService.getMaxBatchSize();
        int counter = 0;
        List<CardLog> batch = new ArrayList<CardLog>();
        for (CardLog value : cardLogList) {
            counter++;
            batch.add(value);
            if ((batch.size() == batchSize) || (counter == cardLogList.size())) {
                unprocessed.addAll(
                        dynamodbService.putObjects(
                                batch.toArray(new CardLog[batch.size()]))
                                .getUnSuccessList());
                batch.clear();
            }

        }

        if (unprocessed.size() > 0)
            return false;
        else
            return true;
    }

}
