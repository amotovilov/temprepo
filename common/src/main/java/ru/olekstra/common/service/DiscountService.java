package ru.olekstra.common.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.dozer.Mapper;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.BatchGetException;
import ru.olekstra.awsutils.exception.NotUpdatedException;
import ru.olekstra.awsutils.exception.OlekstraException;
import ru.olekstra.common.dao.DiscountDao;
import ru.olekstra.common.helper.DozerHelper;
import ru.olekstra.common.util.OperationUtil;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.Discount;
import ru.olekstra.domain.DiscountData;
import ru.olekstra.domain.DiscountLimit;
import ru.olekstra.domain.DiscountLimitUsed;
import ru.olekstra.domain.DiscountPlan;
import ru.olekstra.domain.DiscountReplace;
import ru.olekstra.domain.Drugstore;
import ru.olekstra.domain.EntityProxy;
import ru.olekstra.domain.Pack;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.dto.DiscountLimitRemaining;
import ru.olekstra.domain.dto.DiscountLimitUsedDetail;
import ru.olekstra.domain.dto.DiscountLimitUsedDto;
import ru.olekstra.domain.dto.DiscountLimitUsedTotal;
import ru.olekstra.domain.dto.PackDiscountData;
import ru.olekstra.domain.dto.PackDiscountLimitData;
import ru.olekstra.domain.dto.VoucherResponsePackReplace;
import ru.olekstra.domain.dto.VoucherResponseRow;

import com.amazonaws.services.cloudfront_2012_03_15.model.InvalidArgumentException;

@Service
public class DiscountService {

    private DiscountDao discountDao;
    private DynamodbService dynamodbService;
    private PlanService planService;
    private AppSettingsService settingsService;
    private Mapper mapper;
    private MessageSource messageSource;
    private DateTimeService dateService;

    @Autowired
    public DiscountService(DiscountDao dao, DynamodbService service,
            PlanService planService, AppSettingsService settingsService,
            Mapper mapper, MessageSource messageSource,
            DateTimeService dateService) {
        this.discountDao = dao;
        this.dynamodbService = service;
        this.planService = planService;
        this.settingsService = settingsService;
        this.mapper = mapper;
        this.messageSource = messageSource;
        this.dateService = dateService;
    }

    /**
     * Метод возвращает дисконты по заданным id Метод работает через кеш, где
     * хранит все дисконты, что позволяет запрашивать БД только для обновления
     * кеша. Важно то, что метод возвращает дисконты в том же порядке, что и
     * поступившие id дисконтов в метод.
     * 
     * @param discountsId список id дисконтов
     * @return
     * @throws RuntimeException
     * @throws InterruptedException
     * @throws IOException
     * @throws OlekstraException
     */
    public List<Discount> getDiscounts(List<String> discountIdList)
            throws RuntimeException, InterruptedException, IOException {
        List<Discount> discountList = discountDao
                .getAllDiscountsUsingCache();
        List<Discount> resultList = new ArrayList<Discount>();
        for (String discountId : discountIdList) {
            for (Discount discount : discountList) {
                if (discount.getId().equals(discountId)) {
                    resultList.add(discount);
                    break;
                }
            }
        }
        return resultList;
    }

    /**
     * Метод возвращает дисконт по заданному id Метод работает через кеш, где
     * хранит все дисконты, что позволяет запрашивать БД только для обновления
     * кеша.
     * 
     * @param discountId id дисконта
     * @return
     * @throws RuntimeException
     * @throws InterruptedException
     * @throws IOException
     */
    public Discount getDiscount(String discountId)
            throws RuntimeException, InterruptedException, IOException {
        List<Discount> discountList = discountDao.getAllDiscountsUsingCache();
        for (Discount discount : discountList) {
            if (discount.getId().equals(discountId)) {
                return discount;
            }
        }
        return null;
    }

    public ProcessingTransaction getDiscountsAndLimits(
            ProcessingTransaction transaction) throws JsonParseException,
            JsonMappingException, IOException, RuntimeException,
            InterruptedException, OlekstraException {

        // выберем дисконты
        List<Discount> discountList = this.getDiscounts(transaction
                .getDiscounts());

        // выберем данные дисконтов для расчета
        List<DiscountData> discountDataList = this
                .getDiscountDataList(discountList, transaction);

        // выбираем данные по скидкам с подходящей версией, и сортируем по
        // порядку скидок
        discountDataList = this
                .sortDiscountData(discountList, discountDataList);

        // выберем лимиты по дисконтам
        List<DiscountLimit> discountLimitList = this
                .getDiscountLimitList(
                        discountList, discountDataList,
                        transaction.getDiscountLimits());

        // сохраним их в transaction
        transaction.setDiscountLimits(discountLimitList);

        // складываем данные по дисконтам и лимитам в rows
        this.fillRowsDiscountAndLimitsData(transaction, discountDataList,
                discountLimitList);

        return transaction;
    }

    public List<DiscountData> getDiscountDataList(List<Discount> discountList,
            ProcessingTransaction transaction)
            throws JsonParseException, JsonMappingException, IOException,
            BatchGetException {
        // формируем список ключей, для загрузки DiscountData
        // и проверяем скидки на наличие PromoPercent. (при наличии создаем
        // расчетную скидку для всех товаров, без загрузки DiscountData)
        int size = transaction.getRows().size();
        List<String> hashKeyList = new ArrayList<String>(size);
        List<String> rangeKeyList = new ArrayList<String>(size);
        List<DiscountData> discountDataList = new ArrayList<DiscountData>();

        Set<String> preparedKeyPairSet = new HashSet<String>();
        for (Discount discount : discountList) {
            for (VoucherResponseRow row : transaction.getRows()) {
                if (discount.getPromoPercent() != null) {
                    discountDataList.add(new DiscountData(discount.getId(), row
                            .getId(), discount.getCurrentVersion(), discount
                            .getPromoPercent()));
                } else {
                    String keysForCheck = discount.getId() + row.getId();
                    if (!preparedKeyPairSet.contains(keysForCheck)) {
                        preparedKeyPairSet.add(keysForCheck);
                        hashKeyList.add(discount.getId());
                        rangeKeyList.add(row.getId());
                    }
                }
            }
        }

        // проверяем необходимость загрузки данных по скидкам и, если нужно,
        // загружаем
        if (hashKeyList.size() > 0 && rangeKeyList.size() > 0) {
            List<DiscountData> discountDataListFromDb = dynamodbService
                    .getObjectsAllOrDie(
                            DiscountData.class,
                            hashKeyList.toArray(new String[hashKeyList.size()]),
                            rangeKeyList
                                    .toArray(new String[rangeKeyList.size()]));
            discountDataList.addAll(discountDataListFromDb);
        }

        return discountDataList;
    }

    public List<DiscountData> sortDiscountData(List<Discount> discountList,
            List<DiscountData> discountDataList) {
        // выбираем данные по скидкам с подходящей версией, и сортируем по
        // порядку скидок
        List<DiscountData> sortedDiscountsData = new ArrayList<DiscountData>();
        Set<String> alreadyAddedDiscounts = new HashSet<String>();
        for (Discount discount : discountList) {
            for (DiscountData discountData : discountDataList) {
                String alreadyAddedDiscountId = discountData.getDiscount()
                        + discountData.getEkt();
                if (discountData.getCurrentVersion().compareTo(
                        // discount.getCurrentVersion()) >= 0
                        discount.getCurrentVersion()) == 0
                        && discountData.getDiscount().equals(discount.getId())
                        && !alreadyAddedDiscounts
                                .contains(alreadyAddedDiscountId)) {
                    alreadyAddedDiscounts.add(alreadyAddedDiscountId);
                    sortedDiscountsData.add(discountData);
                }
            }
        }
        return sortedDiscountsData;
    }

    public void fillRowsDiscountAndLimitsData(
            ProcessingTransaction transaction,
            List<DiscountData> discountDataList,
            List<DiscountLimit> discountLimitList) throws JsonParseException,
            JsonMappingException, IOException {
        // заполняем для каждой позиции в чеке соответсвующие PackDiscountData
        List<VoucherResponseRow> updatedRows = new ArrayList<VoucherResponseRow>(transaction.getRows().size());
        for (VoucherResponseRow row : transaction.getRows()) {
            List<PackDiscountData> rowPackDiscountData = new ArrayList<PackDiscountData>();
            for (DiscountData discountData : discountDataList) { // was:
                                                                 // sortedDiscountsData
                if (row.getId().equals(discountData.getEkt())) {
                    PackDiscountData packDiscountData = new PackDiscountData();
                    packDiscountData.setDiscountId(discountData.getDiscount());
                    packDiscountData.setOriginalPercent(new BigDecimal(
                            discountData.getPercent() != null ? discountData.getPercent() : 0));
                    // проверяем, есть ли лимиты по дисконту и добавляем
                    List<PackDiscountLimitData> packDiscountLimitDataList = new ArrayList<PackDiscountLimitData>();
                    if (discountData.getLimitGroupId() != null)
                        packDiscountLimitDataList.add(new PackDiscountLimitData(
                                discountData.getLimitGroupId(), discountData.getLimitGroupWeight()));

                    // поищем "общий" лимит для дисконта
                    if (discountLimitList != null) {
                        for (DiscountLimit dl : discountLimitList) {
                            if (dl.getDiscountId().equals(discountData.getDiscount())
                                    && DiscountLimit.isAllPurposeLimitGroupId(dl.getLimitGroupId())
                                    && dl.getCurrentVersion().equals(discountData.getCurrentVersion())) {
                                packDiscountLimitDataList.add(new PackDiscountLimitData(dl.getLimitGroupId(), null));
                            }
                        }
                    }

                    if (packDiscountLimitDataList.size() > 0) {
                        PackDiscountLimitData[] packDiscountLimitData = packDiscountLimitDataList
                                .toArray(new PackDiscountLimitData[packDiscountLimitDataList.size()]);
                        packDiscountData.setPackDiscountLimitData(packDiscountLimitData);
                    }

                    rowPackDiscountData.add(packDiscountData);
                }
            }

            row.setPackDiscount(rowPackDiscountData.toArray(new PackDiscountData[rowPackDiscountData.size()]));
            updatedRows.add(row);
        }

    }

    public List<DiscountLimit> getDiscountLimitList(
            List<Discount> discountList, List<DiscountData> discountDataList,
            List<DiscountLimit> alreadyKnownDiscountLimitList)
            throws JsonParseException,
            JsonMappingException, IOException, BatchGetException {

        // выбрать из БД всё что есть
        List<DiscountLimit> fullDiscountLimitList = this
                .getFullDiscountLimitList(
                        discountDataList,
                        alreadyKnownDiscountLimitList);
        // оставить только те, что имеют нужную версию
        List<DiscountLimit> discountLimitList = this
                .removeUnversionedLimits(
                        fullDiscountLimitList,
                        discountList);
        // объединяем с уже имевшимися в ProcessingTransaction (если они были)
        if (alreadyKnownDiscountLimitList != null)
            discountLimitList.addAll(alreadyKnownDiscountLimitList);

        return discountLimitList;
    }

    public List<DiscountLimit> removeUnversionedLimits(
            List<DiscountLimit> fullDiscountLimitList,
            List<Discount> discountList) {
        List<DiscountLimit> discountLimitList = new ArrayList<DiscountLimit>();
        if (fullDiscountLimitList.size() > 0) {
            for (DiscountLimit limit : fullDiscountLimitList) {
                int limitVersion = limit.getCurrentVersion();
                for (Discount discount : discountList) {
                    int discountVersion = discount.getCurrentVersion();
                    String discountId = discount.getId();
                    if (limit.getDiscountId().equalsIgnoreCase(discountId)
                            && (discountVersion == limitVersion))
                        discountLimitList.add(limit);
                }
            }
        }
        return discountLimitList;
    }

    public List<DiscountLimit> getFullDiscountLimitList(
            List<DiscountData> discountDataList,
            List<DiscountLimit> alreadyKnownDiscountLimitList)
            throws BatchGetException {
        // формируем набор ключей для запроса информации по лимитам
        // (при этом из запроса исключаются те лимиты, которые уже
        // загружены в ProcessingTransaction)
        List<String> hashKeyList = new ArrayList<String>();
        List<String> rangeKeyList = new ArrayList<String>();
        Set<String> keyPairSet = new HashSet<String>(); // для исключения дублей
                                                        // ключей в запросе
        Set<String> dicountIdSet = new HashSet<String>();
        for (DiscountData discount : discountDataList) {
            String keyPair = discount.getDiscount() + EntityProxy.getDelimeter() + discount.getLimitGroupId();
            dicountIdSet.add(discount.getDiscount());
            boolean found = false;
            if (alreadyKnownDiscountLimitList != null)
                for (DiscountLimit d : alreadyKnownDiscountLimitList) {
                    if (d.getDiscountId().equals(discount.getDiscount())
                            && d.getLimitGroupId().equals(
                                    discount.getLimitGroupId())) {
                        found = true;
                        break;
                    }
                }
            if (!found && !keyPairSet.contains(keyPair) && discount.getLimitGroupId() != null) {
                hashKeyList.add(discount.getDiscount());
                rangeKeyList.add(discount.getLimitGroupId());
                keyPairSet.add(keyPair);
            }
        }

        // для каждого discountId может быть "общий" для всех товаров лимит -
        // добавим их
        Set<String> allPurposeLimitId = DiscountLimit.getAllPurposeLimitId();
        for (String discountId : dicountIdSet) {
            boolean found = false;
            if (alreadyKnownDiscountLimitList != null)
                for (DiscountLimit d : alreadyKnownDiscountLimitList) {
                    if (d.getDiscountId().equals(discountId)
                            && DiscountLimit.isAllPurposeLimitGroupId(d.getLimitGroupId())) {
                        found = true;
                        break;
                    }
                }
            // Если был найден хоть один общий лимит, то все они были загружены.
            if (!found) {
                Iterator<String> i = allPurposeLimitId.iterator();
                while (i.hasNext()) {
                    hashKeyList.add(discountId);
                    rangeKeyList.add(i.next());
                }
            }
        }

        int hSize = hashKeyList.size();
        int rSize = rangeKeyList.size();

        System.out.println(" ");
        System.out.println("========================");
        System.out.println("Keys for DiscountLimit query");
        System.out.println("List size: Hash = " + hSize + "  Range = " + rSize);

        for (String k : hashKeyList)
            System.out.println(k);
        System.out.println(" ");
        for (String r : rangeKeyList)
            System.out.println(r);

        List<DiscountLimit> dll = dynamodbService.getObjectsAllOrDie(
                DiscountLimit.class,
                hashKeyList.toArray(new String[hashKeyList.size()]),
                rangeKeyList.toArray(new String[rangeKeyList.size()]));
        System.out.println("Actually returned from DB:");
        for (DiscountLimit dl : dll) {
            System.out.println("DiscountId: " + dl.getDiscountId()
                    + "  LimitId: " + dl.getLimitGroupId() + "  Ver: "
                    + dl.getCurrentVersion() + "  Period: "
                    + dl.getLimitPeriod());
        }

        return dll;
    }

    @Deprecated
    public List<DiscountLimit> getDiscountLimitList(
            List<Discount> discountList,
            List<DiscountLimit> alreadyLoadedDiscountLimitList)
            throws RuntimeException, InterruptedException,
            JsonGenerationException, JsonMappingException, IOException,
            InstantiationException, IllegalAccessException {
        if (discountList == null)
            return null;

        // формируем массив ключей для запроса информации по лимитам
        // (при этом из запроса исключаются те лимиты, которые уже
        // загружены в ProcessingTransaction)
        List<String> discountIdList = new ArrayList<String>();
        for (Discount discount : discountList) {
            boolean found = false;
            if (alreadyLoadedDiscountLimitList != null)
                for (DiscountLimit d : alreadyLoadedDiscountLimitList) {
                    if (d.getDiscountId().equals(discount.getId())) {
                        found = true;
                        break;
                    }
                }
            if (!found)
                discountIdList.add(discount.getId());
        }

        // поскольку из Primary Key таблицы DiscountLimit у нас известны только
        // нужные Hashkey, выбираем лимиты последовательными запросами к Динаме
        List<DiscountLimit> fullDiscountLimitList = new ArrayList<DiscountLimit>();
        for (String discountId : discountIdList)
            fullDiscountLimitList.addAll(dynamodbService.queryObjects(
                    DiscountLimit.class, discountId));

        List<DiscountLimit> discountLimitList = new ArrayList<DiscountLimit>();
        if (fullDiscountLimitList.size() > 0) {
            for (DiscountLimit limit : fullDiscountLimitList) {
                int limitVersion = limit.getCurrentVersion();
                for (Discount discount : discountList) {
                    int discountVersion = discount.getCurrentVersion();
                    String discountId = discount.getId();
                    if (limit.getDiscountId().equalsIgnoreCase(discountId)
                            && (discountVersion == limitVersion))
                        discountLimitList.add(limit);
                }
            }
        }

        // объединяем с уже имевшимися в ProcessingTransaction (если они были)
        if (alreadyLoadedDiscountLimitList != null)
            discountLimitList.addAll(alreadyLoadedDiscountLimitList);

        if (discountLimitList.size() > 0)
            return discountLimitList;
        else
            return null;
    }

    // public List<DiscountLimitUsedRecord> getUsedLimits(

    public List<DiscountLimitUsed> getUsedLimits(
            String cardNumber, DateTime transactionDateTime,
            List<DiscountLimit> discountLimitList,
            // List<DiscountLimitUsedRecord> loadedBefore) {
            List<DiscountLimitUsedTotal> loadedBefore) throws BatchGetException {

        if (discountLimitList == null)
            return null;

        List<String> hashKeyList = new ArrayList<String>();
        List<String> rangeKeyList = new ArrayList<String>();
        for (DiscountLimit discountLimit : discountLimitList) {
            // поднимаем из БД только "итоговые" записи (voucherId - нули,
            // row = 0) и только те, которых ещё нет в ProcessingTransaction
            String limitGroupId = discountLimit.getLimitGroupId();
            String discountId = discountLimit.getDiscountId();
            boolean exists = false;
            if (loadedBefore != null) {
                for (DiscountLimitUsedTotal dlur : loadedBefore) {
                    if (dlur.getLimitGroupId().equalsIgnoreCase(limitGroupId)
                            && dlur.getDiscountId().equalsIgnoreCase(discountId)) {
                        exists = true;
                        break;
                    }
                }
            }
            if (!exists) {
                hashKeyList.add(DiscountLimitUsed.buildTotalHashKey(
                        cardNumber,
                        getCurrentPeriod(discountLimit.getLimitPeriod(), transactionDateTime)));
                rangeKeyList.add(DiscountLimitUsed.buildTotalRangeKey(
                        discountLimit.getDiscountId(),
                        discountLimit.getLimitGroupId()));
            }
        }

        // List<DiscountLimitUsedRecord> found;
        if (hashKeyList.size() > 0)
            return dynamodbService.getObjectsAllOrDie(DiscountLimitUsed.class,
                    hashKeyList.toArray(new String[hashKeyList.size()]),
                    rangeKeyList.toArray(new String[rangeKeyList.size()]));
        else
            return new ArrayList<DiscountLimitUsed>();
    }

    // объединяем с уже имеющимися, добавив только те что нужны
    public List<DiscountLimitUsedTotal> unionPreviousLimitUsedTotal(
            String cardNumber, DateTime transactionDateTime,
            List<DiscountLimit> discountLimitList,
            List<DiscountLimitUsed> discountLimitUsedList,
            List<DiscountLimitUsedTotal> loadedBefore) {
        List<DiscountLimitUsedTotal> found = DozerHelper.map(mapper, discountLimitUsedList,
                DiscountLimitUsedTotal.class);
        if (loadedBefore != null) {
            for (DiscountLimitUsedTotal dr : loadedBefore) {
                for (DiscountLimit d : discountLimitList) {
                    if (dr.getDiscountId().equalsIgnoreCase(d.getDiscountId())
                            && dr.getLimitGroupId().equalsIgnoreCase(d.getLimitGroupId())) {
                        found.add(dr);
                    }
                }
            }
        }

        // если для каких-то лимитов не нашлись данные по их использованию,
        // то инициализируем их с нулями
        this.initEmptyUsedLimits(cardNumber, transactionDateTime, discountLimitList, found);

        return found;
    }

    private void initEmptyUsedLimits(String cardNum, DateTime dateTime, List<DiscountLimit> dlList,
            List<DiscountLimitUsedTotal> dlutList) {

        if (dlList != null) {
            for (DiscountLimit dl : dlList) {
                boolean exists = false;
                if (dlutList != null) {
                    for (DiscountLimitUsedTotal dlut : dlutList) {
                        if (dl.getDiscountId().equalsIgnoreCase(dlut.getDiscountId())
                                && dl.getLimitGroupId().equalsIgnoreCase(dlut.getLimitGroupId())) {
                            exists = true;
                            break;
                        }
                    }
                }
                if (!exists) {
                    dlutList.add(this.newTotal(cardNum,
                            this.getCurrentPeriod(dl.getLimitPeriod(), dateTime),
                            dl.getDiscountId(), dl.getLimitGroupId()));
                }
            }
        }
    }

    private DiscountLimitUsedTotal newTotal(String cardNumber,
            String currentPeriod, String discount, String limitGroup) {
        return new DiscountLimitUsedTotal(
                currentPeriod, cardNumber,
                UUID.fromString(EntityProxy.NULL_UUID),
                DiscountLimitUsed.DEFAULT_ROW_NUMBER, limitGroup,
                discount, 0, 0l, BigDecimal.ZERO,
                BigDecimal.ZERO);
    }

    public String getCurrentPeriod(String limitPeriod, DateTime transactionDateTime) {
        String currentPeriod;
        if (limitPeriod.equalsIgnoreCase(LimitPeriod.DAY.getPeriod())) {
            currentPeriod = LimitPeriod.DAY.getCurrentPeriod(transactionDateTime);
        } else if (limitPeriod.equalsIgnoreCase(LimitPeriod.WEEK.getPeriod())) {
            currentPeriod = LimitPeriod.WEEK.getCurrentPeriod(transactionDateTime);
        } else if (limitPeriod.equalsIgnoreCase(LimitPeriod.MONTH.getPeriod())) {
            currentPeriod = LimitPeriod.MONTH.getCurrentPeriod(transactionDateTime);
        } else if (limitPeriod.equalsIgnoreCase(LimitPeriod.QUARTER.getPeriod())) {
            currentPeriod = LimitPeriod.QUARTER.getCurrentPeriod(transactionDateTime);
        } else if (limitPeriod.equalsIgnoreCase(LimitPeriod.HALFYEAR.getPeriod())) {
            currentPeriod = LimitPeriod.HALFYEAR.getCurrentPeriod(transactionDateTime);
        } else if (limitPeriod.equalsIgnoreCase(LimitPeriod.YEAR.getPeriod())) {
            currentPeriod = LimitPeriod.YEAR.getCurrentPeriod(transactionDateTime);
        } else {
            throw new IllegalArgumentException("Wrong period keyword");
        }
        return currentPeriod;
    }

    public void clearTotal(DiscountLimitUsed parent) {
        if (!parent.getType().equals(DiscountLimitUsed.TYPE_TOTAL)) {
            throw new InvalidArgumentException(
                    "DiscountLimitUsed object not total type");
        }
        parent.setDiscountSum(new BigDecimal(0));
        parent.setSumBeforeDiscount(new BigDecimal(0));
        parent.setPackCount(0);
        parent.setAveragePackCount(0L);
    }

    public void appendTotal(DiscountLimitUsed parent,
            List<DiscountLimitUsed> child) {
        if (!parent.getType().equals(DiscountLimitUsed.TYPE_TOTAL)) {
            throw new InvalidArgumentException(
                    "DiscountLimitUsed object not total type");
        }

        BigDecimal valueDiscountSum = parent.getDiscountSum();
        BigDecimal valueSumBeforeDiscount = parent.getSumBeforeDiscount();
        Integer valuePackCount = parent.getPackCount();
        Long valueAvrgPackCount = parent.getAveragePackCount();
        for (DiscountLimitUsed dataItem : child) {
            if (dataItem.getType().equals(DiscountLimitUsed.TYPE_DETAIL)) {
                if (dataItem.getDiscountId().equals(parent.getDiscountId())
                        && dataItem.getPeriod().equals(parent.getPeriod())
                        && dataItem.getLimitGroupId().equals(
                                parent.getLimitGroupId())
                        && dataItem.getCardNumber().equals(
                                parent.getCardNumber())) {

                    valueDiscountSum = valueDiscountSum.add(dataItem
                            .getDiscountSum());
                    valueSumBeforeDiscount = valueSumBeforeDiscount
                            .add(dataItem.getSumBeforeDiscount());
                    valuePackCount = valuePackCount + dataItem.getPackCount();
                    valueAvrgPackCount = valueAvrgPackCount
                            + dataItem.getAveragePackCount();
                }
            }
        }
        parent.setDiscountSum(valueDiscountSum);
        parent.setSumBeforeDiscount(valueSumBeforeDiscount);
        parent.setPackCount(valuePackCount);
        parent.setAveragePackCount(valueAvrgPackCount);
    }

    // TODO перетащить тесты на этот метод из authcard (VoucherService) один
    // метод разбился на несколько
    public void saveDiscountLimitUsed(List<DiscountLimitUsed> dataToSave)
            throws NotUpdatedException {

        dynamodbService.putObjectsAllOrDie(dataToSave);
    }

    public void updateTotalAndSave(List<DiscountLimitUsedDto> list)
            throws NotUpdatedException {
        if (list != null)
            saveDiscountLimitUsed(DozerHelper.map(mapper, list, DiscountLimitUsed.class));
    }

    // TODO написать тест к методу
    public void stornDiscountData(List<DiscountLimitUsedDetail> list,
            int rowNumber) throws NotUpdatedException {
        List<DiscountLimitUsed> valuestoSave = new ArrayList<DiscountLimitUsed>();
        for (DiscountLimitUsedDetail r : list) {
            if (r.getRow().equals(rowNumber)) {
                DiscountLimitUsed d = mapper.map(r, DiscountLimitUsed.class);
                d.setRow(rowNumber * -1);
                d.setPackCount(d.getPackCount() * -1);
                d.setAveragePackCount(d.getAveragePackCount() * -1);
                d.setDiscountSum(d.getDiscountSum().negate());
                d.setSumBeforeDiscount(d.getSumBeforeDiscount().negate());
                valuestoSave.add(d);
            }
        }
        saveDiscountLimitUsed(valuestoSave);
    }

    public List<DiscountLimitRemaining> calculateRemainingLimits(List<DiscountLimit> limits,
            List<DiscountLimitUsedTotal> limitsUsed, String cardNumber) {

        if (limitsUsed == null) {
            return null;
        }

        List<DiscountLimitRemaining> result = new ArrayList<DiscountLimitRemaining>();

        for (DiscountLimitUsedTotal limitUsed : limitsUsed) {
            for (DiscountLimit limit : limits) {
                String period = LimitPeriod.parsePeriod(limitUsed.getPeriod());
                if (limit.getLimitGroupId().equals(limitUsed.getLimitGroupId())
                        && limit.getDiscountId().equals(limitUsed.getDiscountId())
                        && limit.getLimitPeriod().equals(period)) {

                    DiscountLimitRemaining limitRemaining = new DiscountLimitRemaining();

                    limitRemaining.setCardNum(cardNumber);
                    limitRemaining.setDiscountId(limit.getDiscountId());
                    String currentLimitGroup = DiscountLimit.isAllPurposeLimitGroupId(limit.getLimitGroupId())
                            ? messageSource.getMessage("msg.defaultLimitGroupName", new Object[] {}, null)
                            : limit.getLimitGroupId();
                    limitRemaining.setLimitGroupId(currentLimitGroup);
                    limitRemaining.setPeriod(limitUsed.getPeriod());
                    if (limit.getMaxAveragePackCount() > 0) {
                        limitRemaining.setRemainingAveragePackCount(limit
                                .getMaxAveragePackCount() - limitUsed.getAveragePackCount());
                    }
                    if (limit.getMaxPackCount() > 0) {
                        limitRemaining.setRemainingPackCount(((long) limit
                                .getMaxPackCount()) - limitUsed.getPackCount());
                    }
                    if (limit.getMaxDiscountSum() != null
                            && limit.getMaxDiscountSum().compareTo(
                                    BigDecimal.ZERO) > 0) {
                        limitRemaining.setRemainingDiscountSum(limit
                                .getMaxDiscountSum().add(
                                        limitUsed.getDiscountSum().negate()));
                    }
                    if (limit.getMaxSumBeforeDiscount() != null
                            && limit.getMaxSumBeforeDiscount().compareTo(
                                    BigDecimal.ZERO) > 0) {
                        limitRemaining.setRemainingSumBeforeDiscount(limit
                                .getMaxSumBeforeDiscount().add(
                                        limitUsed.getSumBeforeDiscount()
                                                .negate()));
                    }
                    result.add(limitRemaining);
                }
            }
        }

        Collections.sort(result, new Comparator<DiscountLimitRemaining>() {

            @Override
            public int compare(DiscountLimitRemaining l1,
                    DiscountLimitRemaining l2) {
                return l1.getLimitGroupId().compareTo(l2.getLimitGroupId());
            }

        });

        return result;
    }

    public List<DiscountLimitRemaining> getRemainingLimits(Card card) throws JsonParseException, JsonMappingException,
            IOException, RuntimeException, InterruptedException, InstantiationException, IllegalAccessException,
            OlekstraException {
        if (StringUtils.isEmpty(card.getDiscountPlanId()))
            return null;
        DiscountPlan plan = planService.getDiscountPlan(card.getDiscountPlanId());
        DateTime currentDate = dateService.createUTCDate();
        if (plan == null)
            return null;
        List<String> discountIds = plan.getDiscounts();
        List<Discount> allDiscounts = this.getDiscounts(discountIds);
        List<DiscountLimit> limits = this.getDiscountLimitList(allDiscounts, null);
        List<DiscountLimitUsed> dluList = this.getUsedLimits(card.getNumber(), currentDate, limits, null);
        List<DiscountLimitUsedTotal> dlutList = DozerHelper.map(mapper, dluList, DiscountLimitUsedTotal.class);
        this.initEmptyUsedLimits(card.getNumber(), currentDate, limits, dlutList);
        return this.calculateRemainingLimits(limits, dlutList, card.getNumber());
    }

    public void getDiscountReplaces(ProcessingTransaction transaction)
            throws JsonParseException, JsonMappingException, IOException, BatchGetException, InstantiationException,
            IllegalAccessException {
        // заведём карту, куда будем складывать синонимы на случай повторения
        // строк чека (чтоб два раза не бегать)
        Map<String, List<VoucherResponsePackReplace>> replaceMap =
                new HashMap<String, List<VoucherResponsePackReplace>>();
        for (VoucherResponseRow row : transaction.getRows()) {
            if (row.getPackDiscount() == null || row.getPackDiscount().length == 0) {
                Pack pack = dynamodbService.getObject(settingsService.getMasterTablePackEntity(),
                        row.getId(), row.getId());
                if (StringUtils.isNotEmpty(pack.getSynonym())) {
                    if (!replaceMap.containsKey(row.getId())) {
                        List<DiscountReplace> replaces = new ArrayList<DiscountReplace>();
                        for (String discountId : transaction.getDiscounts()) {
                            String key = discountId + OperationUtil.SEPARATOR + pack.getSynonym();
                            List<DiscountReplace> replaceList = dynamodbService
                                    .queryObjects(DiscountReplace.class, key);
                            replaces.addAll(replaceList);
                        }
                        List<VoucherResponsePackReplace> replaceDtoList = new ArrayList<VoucherResponsePackReplace>();
                        for (DiscountReplace replace : replaces) {
                            // повторы нам не нужны
                            boolean found = false;
                            for (VoucherResponsePackReplace replaceDto : replaceDtoList)
                                if (replaceDto.getPackId().equalsIgnoreCase(replace.getPackId())) {
                                    found = true;
                                    continue;
                                }
                            if (!found)
                                replaceDtoList.add(new VoucherResponsePackReplace(replace.getPackId(),
                                        replace.getBarcode(), replace.getTradeName()));
                        }
                        row.setReplacePacks(replaceDtoList.toArray(new VoucherResponsePackReplace[replaceDtoList.size()]));
                        replaceMap.put(row.getId(), replaceDtoList);
                    } else {
                        List<VoucherResponsePackReplace> replaceDtoListFromMap = replaceMap.get(row.getId());
                        row.setReplacePacks(replaceDtoListFromMap
                                .toArray(new VoucherResponsePackReplace[replaceDtoListFromMap.size()]));
                    }
                }
            }
        }
    }

    public void calculateDiscounts(ProcessingTransaction transaction,
            BigDecimal cardLimit)
            throws JsonParseException, JsonMappingException, IOException {
        DiscountPlan plan = dynamodbService.getObject(DiscountPlan.class,
                transaction.getDiscountPlanId());
        BigDecimal maxPercent = new BigDecimal(plan.getMaxDiscountPercent());
        BigDecimal cardLimitUsedSum = BigDecimal.ZERO;
        BigDecimal cardLimitDeficit = BigDecimal.ZERO;
        for (VoucherResponseRow row : transaction.getRows()) {
            BigDecimal rowPercent = BigDecimal.ZERO;
            BigDecimal rowLimitUsedSum = BigDecimal.ZERO;
            /*
             * BigDecimal rowSumToPay = BigDecimal.ZERO; BigDecimal
             * rowSumWithoutDiscount = BigDecimal.ZERO;
             */
            if (row.getPackDiscount() != null) {
                for (PackDiscountData data : row.getPackDiscount()) {
                    if (rowPercent.add(data.getOriginalPercent()).compareTo(
                            maxPercent) < 0) {
                        data.setUsedPercent(data.getOriginalPercent());
                        rowPercent = rowPercent.add(data.getOriginalPercent());
                    } else {
                        BigDecimal usedPercent =
                                maxPercent.add(rowPercent.negate());
                        if (usedPercent.compareTo(BigDecimal.ZERO) > 0) {
                            data.setUsedPercent(usedPercent);
                        } else {
                            data.setUsedPercent(BigDecimal.ZERO);
                        }
                        rowPercent = maxPercent;
                    }

                    Discount discount = dynamodbService.getObject(
                            Discount.class, data.getDiscountId());
                    data.setUsedSum(data.getUsedPercent()
                            .divide(new BigDecimal(100))
                            .multiply(data.getOriginalSum()));
                    data.setRefundSum(discount.getAlwaysFullRefund() ?
                            data.getOriginalSum() : data.getUsedSum());
                    if (discount.getUseCardLimit()) {
                        if (data.getUsedSum().add(rowLimitUsedSum)
                                .compareTo(cardLimit) > 0) {
                            if (rowLimitUsedSum.compareTo(cardLimit) <= 0) {
                                cardLimitDeficit = cardLimitDeficit.add(
                                        data.getUsedSum()).add(
                                        cardLimit.add(rowLimitUsedSum.negate())
                                                .negate());
                            } else {
                                cardLimitDeficit = cardLimitDeficit.add(data
                                        .getUsedSum());
                            }
                        }
                        rowLimitUsedSum = rowLimitUsedSum
                                .add(data.getUsedSum());
                    }
                }
            }

            row.setDiscountPercent(rowPercent);
            row.setDiscountSum(row.getSumWithoutDiscount().multiply(
                    rowPercent.divide(new BigDecimal(100))));
            row.setSumToPay(row.getSumWithoutDiscount().add(
                    row.getDiscountSum().negate()));
            row.setCardLimitUsedSum(rowLimitUsedSum);
            cardLimitUsedSum = cardLimitUsedSum.add(rowLimitUsedSum);
            if (transaction.getCardLimitDeficit() == null) {
                transaction.setCardLimitDeficit(BigDecimal.ZERO);
            }
            Drugstore drugstore = dynamodbService.getObject(Drugstore.class,
                    transaction.getDrugstoreId());
            BigDecimal nonRefundPercent;
            if (drugstore == null || drugstore.getNonRefundPercent() == null) {
                nonRefundPercent = new BigDecimal(
                        settingsService.getNonRefundPercent());
            } else {
                nonRefundPercent = new BigDecimal(
                        drugstore.getNonRefundPercent());
            }
            row.setRefundSum(row.getSumWithoutDiscount().multiply(
                    rowPercent.add(nonRefundPercent.negate()).divide(
                            new BigDecimal(100))));
        }
        transaction.setCardLimitDeficit(cardLimitDeficit);
        transaction.setCardLimitUsedSum(cardLimitUsedSum);
    }
}
