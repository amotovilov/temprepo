package ru.olekstra.common.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.olekstra.awsutils.DynamodbEntity;
import ru.olekstra.domain.dto.SmsResponseDto;
import ru.olekstra.domain.Card;

@Service
public class SmsService {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(SmsService.class);

    private AppSettingsService appSettings;

    public static final String PHONE_SEPARATOR = ";";
    public static final String RECIPIENT_SEPARATOR = "\n";
    public static final String RECIPIENT_DELIMITER = ":";
    public static final String MULTIPLE_PARAM_SEPARATOR = "=";
    public static final String MULTIPLE_PARAM_DELEMITER = "\\|";
    public static final String FORMAT_CODE = "7";
    public static final String UTF = "UTF-8";

    public static final int CONNECTION_TIMEOUT = 30000;

    private static final Set<Integer> RESOLVED_RESPONSE_CODES = new HashSet<Integer>(
            Arrays.asList(0, 5, 7, 8));

    @Autowired
    public SmsService(AppSettingsService appSettings) {
        this.appSettings = appSettings;
    }

    /**
     * Send SMS message to specified phone number
     * 
     * @param phoneNumber
     * @param message
     * @return true if message was accepted by aggregator, otherwise false
     * @throws ClientProtocolException
     * @throws IOException
     */
    public boolean send(String phoneNumber, String message)
            throws ClientProtocolException, IOException {

        List<Map<String, String>> recipients = new ArrayList<Map<String, String>>();
        Map<String, String> recipinet = new HashMap<String, String>();
        recipinet.put(phoneNumber, message);
        recipients.add(recipinet);

        return processRecipients(recipients);
    }

    /**
     * Sending SMS message to specified list of phone numbers
     * 
     * @param phoneNumbers
     * @param message
     * @throws ClientProtocolException
     * @throws IOException
     */
    public void sendSingleMessage(List<String> phoneNumbers,
            String message)
            throws ClientProtocolException, IOException {

        List<Map<String, String>> recipients = new ArrayList<Map<String, String>>();
        for (String recipientNumber : phoneNumbers) {
            Map<String, String> recipinet = new HashMap<String, String>();
            recipinet.put(recipientNumber, message);
            recipients.add(recipinet);
        }

        processRecipients(recipients);
    }

    /**
     * Sending SMS messages to specified list of recipients
     * 
     * @param recipients (recipient phone number and it's specified message)
     * @throws ClientProtocolException
     * @throws IOException
     */
    public void sendMultpleMessages(List<Map<String, String>> recipients)
            throws ClientProtocolException, IOException {

        processRecipients(recipients);
    }

    public String getCardNumLastFourDigits(String cardNum) {
        return Card.getCuttingNumber(cardNum);
    }

    private boolean processRecipients(List<Map<String, String>> recipients)
            throws ClientProtocolException, IOException {

        boolean result = false;
        int fromIndex = 0;
        int toIndex = 0;
        int sendThreshold = appSettings.getSmsMaxSendThreshold();

        while (fromIndex < recipients.size()) {
            toIndex += sendThreshold;
            if (toIndex > recipients.size()
                    || sendThreshold == AppSettingsService.DEFAULT_SMS_NO_MAX_THRESHOLD) {
                toIndex = recipients.size();
            }
            List<Map<String, String>> recipientsPart = recipients.subList(
                    fromIndex,
                    toIndex);

            String formatedRecipients = formatRecipients(recipientsPart);
            result = processSend(formatedRecipients);
            fromIndex = toIndex;
        }

        return result;
    }

    private boolean processSend(String recipients)
            throws ClientProtocolException, IOException {

        HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams,
                CONNECTION_TIMEOUT);
        HttpClient httpclient = new DefaultHttpClient(httpParams);

        String rowParameters = appSettings.getSmsGateSettings();
        boolean result = false;
        if (rowParameters.isEmpty()) {
            LOGGER.debug("Multple sms send settings is empty");
            System.out.println("Multple sms send settings is empty");
            return false;
        }

        try {

            Map<String, String> parameters = parseSendParams(rowParameters);
            String path = getSendUrl(parameters);
            List<NameValuePair> nameValuePairs = prepareSmsGateSettings(
                    parameters, recipients);

            HttpPost httpPost = new HttpPost(path);
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, UTF));

            LOGGER.debug("executing POST request: " + httpPost.getURI());
            System.out.println("executing POST request " + httpPost.getURI());

            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            String responseBody = httpclient.execute(httpPost,
                    responseHandler);

            System.out.println("Multiple messages send response:");
            System.out.println(responseBody);

            LOGGER.debug("Multiple messages send response: " + responseBody);
            SmsResponseDto response = (SmsResponseDto) DynamodbEntity
                    .fromJson(responseBody, SmsResponseDto.class);
            result = (RESOLVED_RESPONSE_CODES
                    .contains(response.getError_code()));

        } finally {
            httpclient.getConnectionManager().shutdown();
        }
        return result;
    }

    private String formatRecipients(List<Map<String, String>> recipients)
            throws UnsupportedEncodingException {
        String result = "";
        for (Map<String, String> value : recipients) {
            result += FORMAT_CODE;
            result += value.keySet().toArray()[0];
            result += RECIPIENT_DELIMITER;
            result += value.get(value.keySet().toArray()[0]);
            result += RECIPIENT_SEPARATOR;
        }
        return result.substring(0,
                result.length() - RECIPIENT_SEPARATOR.length());
    }

    private Map<String, String> parseSendParams(String encodedParams) {
        Map<String, String> parsedParams = new HashMap<String, String>();

        String[] params = encodedParams.split(MULTIPLE_PARAM_DELEMITER);
        for (String value : params) {
            String[] param = value.split(MULTIPLE_PARAM_SEPARATOR);
            parsedParams.put(param[0], param[1]);
        }

        return parsedParams;
    }

    private String getSendUrl(Map<String, String> parameters) {
        return parameters.get("url");
    }

    private List<NameValuePair> prepareSmsGateSettings(
            Map<String, String> parameters, String recipients) {

        List<NameValuePair> nameValuePairs =
                new ArrayList<NameValuePair>(1);
        nameValuePairs.add(new BasicNameValuePair("login",
                parameters.get("login")));
        nameValuePairs.add(new BasicNameValuePair("psw",
                parameters.get("psw")));
        nameValuePairs.add(new BasicNameValuePair("list",
                recipients));
        nameValuePairs.add(new BasicNameValuePair("fmt",
                parameters.get("fmt")));
        nameValuePairs.add(new BasicNameValuePair("sender",
                parameters.get("sender")));
        nameValuePairs.add(new BasicNameValuePair("charset",
                parameters.get("charset")));

        return nameValuePairs;
    }
}
