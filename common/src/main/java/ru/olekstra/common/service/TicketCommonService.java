package ru.olekstra.common.service;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.SnsService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.EntityProxy;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.Ticket;
import ru.olekstra.domain.TicketPost;
import ru.olekstra.domain.TicketUnassigned;

@Service
public class TicketCommonService {

    protected DynamodbService dynamodbService;
    protected AppSettingsService appSettings;
    private SnsService snsService;
    private MessageSource messageSource;

    private static final DateTimeFormatter dateFormatter = DateTimeFormat
            .forPattern("yyyyMMdd");

    @Autowired
    public TicketCommonService(DynamodbService service,
            AppSettingsService settingsService,
            SnsService snsService, MessageSource messageSource) {
        this.dynamodbService = service;
        this.appSettings = settingsService;
        this.snsService = snsService;
        this.messageSource = messageSource;
    }

    public List<Ticket> getAllUserTickets(String cardNumber) {
        return null;
    }

    /**
     * Возвращает список сообщений тикета или null
     * 
     * @param ticketId - Id тикета
     * @return
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws IOException
     */
    public List<TicketPost> getTicketPosts(String ticketId) throws InstantiationException, IllegalAccessException,
            IOException {
        List<TicketPost> posts = dynamodbService.queryObjects(TicketPost.class, ticketId);
        Collections.sort(posts);
        return posts;
    }

    public Ticket getTicket(String period, String ticketId) {
        return dynamodbService.getObject(Ticket.class, period, ticketId);
    }

    public boolean saveNewTicket(Ticket ticket, TicketPost post) throws IOException {

        boolean result = true;

        try {
            result &= dynamodbService.putObject(ticket);
            result &= dynamodbService.putObject(post);
            TicketUnassigned ticketUnassigned = new TicketUnassigned(ticket);
            result &= dynamodbService.putObject(ticketUnassigned);
            snsService.publishTicketAlertMessage(
                    messageSource.getMessage("msg.createTicketNotificationTopic",
                            new String[] {Card.getCuttingNumber(ticket.getCardNumber())}, null),
                    messageSource.getMessage(
                            "msg.createTicketNotification",
                            new String[] {ticket.getCardNumber(), post.getAuthor(), post.getSubject(),
                                    ticket.getDrugstoreName(), post.getPost()}, null));
        } catch (ItemSizeLimitExceededException e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean addPost(TicketPost post, Ticket ticket) throws IOException {

        boolean result = true;

        try {
            ticket.setLastPost(post.getPost());
            if (appSettings.getTicketStatusClosed().contains(
                    // если тикет был закрыт, то переоткрываем
                    ticket.getLastStatus())) {
                ticket.setLastStatus(appSettings.getTicketStatusReopen());
                post.setStatus(ticket.getLastStatus());
                TicketUnassigned ticketUnassigned = new TicketUnassigned(ticket);
                result &= dynamodbService.putObject(ticketUnassigned);
            }
            result &= dynamodbService.putObject(ticket);
            result &= dynamodbService.putObject(post);
        } catch (ItemSizeLimitExceededException e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean ifTicketAlreadyExists(Ticket ticket) {
        return getTicket(ticket.getPeriod(), ticket.getTicketId().toString()) != null;
    }

    public List<Ticket> getTicketListForPeriod(String period) throws InstantiationException, IllegalAccessException,
            IOException {
        return dynamodbService.queryObjects(Ticket.class, period);
    }

    public boolean isTransactionFound(DateTime date,
            String voucherRowId, String drugstoreId) throws JsonParseException,
            JsonMappingException, IOException {
        String transactionId = voucherRowId.split(EntityProxy.getDelimeter())[0];
        int row;
        try {
            row = Integer.parseInt(voucherRowId.split(EntityProxy
                    .getDelimeter())[1]);
        } catch (NumberFormatException nfe) {
            return false;
        } catch (ArrayIndexOutOfBoundsException aioobe) {
            return false;
        }
        ProcessingTransaction transaction = this.getTransaction(date,
                transactionId, drugstoreId);

        if (transaction != null) {
            if (row <= transaction.getRows().size() && row > 0) {
                return true;
            } else
                return false;
        } else
            return false;
    }

    private ProcessingTransaction getTransaction(DateTime date,
            String voucherId, String drugstoreId) {
        String period = date.toString(dateFormatter);
        return dynamodbService.getObject(ProcessingTransaction.class,
                drugstoreId,
                period + ProcessingTransaction.getDelimeter() + voucherId);
    }

}
