package ru.olekstra.common.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.DateFormat;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Properties;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.olekstra.awsutils.dynamodb.TableManagement;
import ru.olekstra.domain.Card;

@Service
public class AboutService {

    @Autowired
    protected ServletContext servletContext;
    @Autowired
    protected TableManagement tableService;

    private Properties manifestAttributes = null;
    private static final String NO_DATA_MSG = "There is no data";
    private static Logger log = Logger.getLogger(AboutService.class);

    private static final DateTimeFormatter ISO_DATE_TIME_FORMAT = ISODateTimeFormat
            .dateTime();

    private String getManifestAttribute(String attributeName)
            throws IOException {
        if (manifestAttributes == null) {
            InputStream is = servletContext
                    .getResourceAsStream("/META-INF/MANIFEST.MF");
            manifestAttributes = new Properties();
            Reader r = new InputStreamReader(is, "UTF-8");
            manifestAttributes.load(r);
        }
        String property = manifestAttributes.getProperty(attributeName);
        return property == null ? null : property.toString();
    }

    public LinkedHashMap<String, String> getBuildStat(Locale locale)
            throws IOException {

        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM,
                locale);

        // данные билд сервера
        LinkedHashMap<String, String> group = new LinkedHashMap<String, String>();
        String buildKey = getManifestAttribute("buildKey");
        String buildNumber = getManifestAttribute("buildNumber");
        String buildTimeStamp = getManifestAttribute("buildTimeStamp");
        String repositoryRevisionNumber = getManifestAttribute("repositoryRevisionNumber");
        if (buildKey == null || buildKey.trim().equals("")) {
            buildKey = NO_DATA_MSG;
        }
        if (buildNumber == null || buildNumber.trim().equals("")) {
            buildNumber = NO_DATA_MSG;
        }
        if (buildTimeStamp == null || buildTimeStamp.trim().equals("")) {
            buildTimeStamp = NO_DATA_MSG;
        }
        if (repositoryRevisionNumber == null
                || repositoryRevisionNumber.trim().equals("")) {
            repositoryRevisionNumber = NO_DATA_MSG;
        }
        group.put("Build key", buildKey);
        group.put("Build number", buildNumber);
        group.put("Build timestamp", buildTimeStamp);
        group
                .put("Repository revision number",
                        repositoryRevisionNumber);
        group.put("Number of cards", String.valueOf(tableService
                .getItemCount(Card.TABLE_NAME)));

        return group;
    }
}
