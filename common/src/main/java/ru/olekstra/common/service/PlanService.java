package ru.olekstra.common.service;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.common.helper.PlanHelper;
import ru.olekstra.domain.Discount;
import ru.olekstra.domain.DiscountPlan;
import ru.olekstra.domain.dto.DiscountPlanForm;

@Service
public class PlanService {

    public static final String SIGN_TO_REPLACE = ",";
    public static final String REPLACEMENT_SIGN = ".";

    public static final int PLAN_FIELDS_NUMBER = 7;
    public static final int FIELD_PACK_ID = 0;
    public static final int FIELD_BASE_COST = 1;
    public static final int FIELD_DISCOUNT = 2;
    public static final int FIELD_REFOUND = 3;
    public static final int FIELD_BARCODE = 4;
    public static final int FIELD_SYNONYM = 5;
    public static final int FIELD_TRADE_NAME = 6;

    private static final Logger LOGGER = LoggerFactory
            .getLogger(PlanService.class);

    private PlanHelper helper;

    @Autowired
    public PlanService(PlanHelper helper) {
        this.helper = helper;
    }

    public boolean addPlan(DiscountPlan plan) throws ItemSizeLimitExceededException {
        return helper.addPlan(plan);
    }

    public String getDiscountPlanName(String id) {
        return helper.getDiscountPlanName(id);
    }

    public void deletePlan(String id) {
        // Удаляем все возможные записи об этом плане, эксепшены глушим, т.к.
        // при попытке удалить не существущий айтем выкидывается эксепшн
        for (int i = 0; i < 10; i++) {
            try {
                helper.deletePlan(id);
            } catch (Exception ex) {
                LOGGER
                        .debug("Cannt delete item while delete all plan items: id - "
                                + id + ", lastdigitId - " + String.valueOf(i));
            }
        }
    }

    public List<DiscountPlan> getAllPlans() throws IOException {
        List<DiscountPlan> result = helper.getAllDiscountPlans();
        Collections.sort(result, new Comparator<DiscountPlan>() {
            @Override
            public int compare(DiscountPlan plan1, DiscountPlan plan2) {
                return plan1.getPlanName().toLowerCase().compareTo(plan2.getPlanName().toLowerCase());
            }
        });
        return result;
    }

    public DiscountPlan getDiscountPlan(String id) {
        return helper.getDiscountPlan(id);
    }

    public boolean savePlan(DiscountPlanForm form,
            List<String> discounts) throws JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException {

        DiscountPlan plan = new DiscountPlan(form.getId(),
                form.getName(),
                form.getMaxDiscount(),
                form.getDescription(),
                form.getDetailsLink(),
                form.getSelfActivationBonus());
        plan.setDiscounts(discounts);
        plan.setFormularId(form.getFormular());

        return helper.addPlan(plan);
    }

    public boolean hasPlan(String planId) {
        return helper.hasPlan(planId);
    }

    public List<Discount> getAllDiscounts() throws IOException {
        List<Discount> result = helper.getAllDiscounts();
        Collections.sort(result, new Comparator<Discount>() {
            @Override
            public int compare(Discount item1, Discount item2) {
                return item1.getName().toLowerCase().compareTo(item2.getName().toLowerCase());
            }
        });
        return result;
    }

}
