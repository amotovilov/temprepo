package ru.olekstra.common.util;

import java.util.UUID;

import org.dozer.BeanFactory;

public class UuidDozerBeanFactory implements BeanFactory {

    @Override
    public Object createBean(Object arg0, Class<?> arg1, String arg2) {
        if (arg0 == null) {
            return null;
        }
        UUID source = (UUID) arg0;
        UUID destination = new UUID(source.getMostSignificantBits(), source.getLeastSignificantBits());
        return destination;
    }

}
