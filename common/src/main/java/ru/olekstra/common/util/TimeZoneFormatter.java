package ru.olekstra.common.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import ru.olekstra.common.service.AppSettingsService;

public class TimeZoneFormatter {

    private AppSettingsService appSettingsService;
    private static final List<TimeZoneItem> timeZoneItems = new ArrayList<TimeZoneItem>();

    private DateTimeFormatter timeZoneFormatter;

    // private static final DateTimeFormatter timeZoneFormatter =
    // DateTimeFormat.forPattern("HH:mm (dd MMM)").withLocale(new Locale("ru"));

    public TimeZoneFormatter(AppSettingsService appSettingsService,
            Locale currentLocale) {
        this.appSettingsService = appSettingsService;
        this.timeZoneFormatter = DateTimeFormat.forPattern("HH:mm (dd MMM)")
                .withLocale(currentLocale);
    }

    public class TimeZoneItem implements Comparable<TimeZoneItem> {

        private String value;

        public TimeZoneItem(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getFormattedValue() {
            if (isTimeZoneValid(value)) {
                return new DateTime(DateTimeZone.forTimeZone(TimeZone
                        .getTimeZone(value))).toString(timeZoneFormatter) + " "
                        + value;
            } else {
                return new DateTime(DateTimeZone.forTimeZone(TimeZone
                        .getTimeZone(AppSettingsService.DEFAULT_TIME_ZONE)))
                        .toString(timeZoneFormatter) + " "
                        + AppSettingsService.DEFAULT_TIME_ZONE;
            }
        }

        @Override
        public int compareTo(TimeZoneItem timeZoneItem) {
            TimeZone value = TimeZone.getTimeZone(timeZoneItem.getValue());
            return TimeZone.getTimeZone(this.getValue()).getRawOffset()
                    - value.getRawOffset();
        }
    }

    private void reload() {

        List<String> timeZones = appSettingsService.getTimeZones();
        timeZoneItems.clear();

        if (timeZones.isEmpty())
            timeZones.add(AppSettingsService.DEFAULT_TIME_ZONE);

        for (String timeZoneId : timeZones) {
            timeZoneItems.add(new TimeZoneItem(timeZoneId));
        }

        Collections.sort(timeZoneItems);
    }

    public List<TimeZoneItem> getTimeZoneItems() {
        reload();
        return timeZoneItems;
    }

    public static boolean isTimeZoneValid(String timeZoneName) {
        if (timeZoneName != null
                && DateTimeZone.getAvailableIDs().contains(timeZoneName))
            return true;
        else
            return false;
    }
/*
    public static TimeZone getTimeZone(String timeZoneName) {
        if (isTimeZoneValid(timeZoneName))
            return TimeZone.getTimeZone(timeZoneName);
        else
            return TimeZone.getTimeZone(AppSettingsService.DEFAULT_TIME_ZONE);
    }

    public static DateTimeZone getDateTimeZone(String timeZoneName) {
        if (isTimeZoneValid(timeZoneName))
            return DateTimeZone.forID(timeZoneName);
        else
            return DateTimeZone.forID(AppSettingsService.DEFAULT_TIME_ZONE);
    }
*/
}
