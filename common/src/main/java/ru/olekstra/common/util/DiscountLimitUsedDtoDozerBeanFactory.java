package ru.olekstra.common.util;

import org.dozer.BeanFactory;

import ru.olekstra.domain.DiscountLimitUsed;

public class DiscountLimitUsedDtoDozerBeanFactory implements BeanFactory {

    @Override
    public Object createBean(Object arg0, Class<?> arg1, String arg2) {
        if (arg0 == null) {
            return null;
        }
        DiscountLimitUsed source = (DiscountLimitUsed) arg0;        
        return DiscountLimitUsed.createDto(source);
    }

}
