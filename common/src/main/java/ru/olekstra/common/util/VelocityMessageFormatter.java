package ru.olekstra.common.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.context.MessageSource;

public class VelocityMessageFormatter {

    private MessageSource messageSource;

    public VelocityMessageFormatter(MessageSource messageSource) {

        this.messageSource = messageSource;
    }

    public String getMessage(String code, List<Object> values) {

        // Convert object to String to save original big decimal format values
        List<String> convertedData = new ArrayList<String>();
        for (Object value : values.toArray()) {
            convertedData.add(value != null ? value.toString() : "");
        }

        return messageSource.getMessage(code, convertedData.toArray(), null);
    }

    public String getExactSumMessage(String code, List<BigDecimal> exactValueList) {
        List<String> convertedData = new ArrayList<String>();
        for (Object value : exactValueList.toArray()) {
            convertedData.add(value != null ? value.toString() : "0.00");
        }
        return messageSource.getMessage(code, convertedData.toArray(), null);
    }

    /**
     * Получить сообщение с html-escaped параметрами
     * 
     * @param code
     * @param values
     * @return
     */
    public String getMessageWithHtml(String code, List<Object> values) {

        // Convert object to String to save original big decimal format values
        List<String> convertedData = new ArrayList<String>();
        for (Object value : values.toArray()) {
            convertedData.add(value != null ? StringEscapeUtils.escapeHtml(value.toString()) : "");
        }

        return messageSource.getMessage(code, convertedData.toArray(), null);
    }

    public String getMessage(String code, List<Object> values, Locale locale) {

        // Convert object to String to save original big decimal format values
        List<String> convertedData = new ArrayList<String>();
        for (Object value : values.toArray()) {
            convertedData.add(value != null ? value.toString() : "");
        }

        return messageSource.getMessage(code, convertedData.toArray(), locale);
    }

    public String getMessage(String code, Object value) {
        return messageSource.getMessage(code, new Object[] {value}, null);
    }

    public String getMessage(String code, Object value, Locale locale) {
        return messageSource.getMessage(code, new Object[] {value}, locale);
    }
}
