package ru.olekstra.common.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import org.springframework.context.MessageSource;

public class MessagesUtil {

    public static final String PREFIX_MESSAGE = "msg.";
    
	public static class Formatter{
		private static DecimalFormat decimalFormatter = new DecimalFormat("0.00");
		static {
			decimalFormatter.setGroupingUsed(true);
			decimalFormatter.setGroupingSize(3);
			DecimalFormatSymbols dfs = new DecimalFormatSymbols();
			dfs.setGroupingSeparator(' ');
			dfs.setDecimalSeparator(',');
			
			decimalFormatter.setDecimalFormatSymbols(dfs);			
		}
	}
	
	public static String bigDecimalToString(BigDecimal value){
		return Formatter.decimalFormatter.format(value);
	}

    public static String getMessage(MessageSource messageSource,
            String messageCode) {

        return messageSource.getMessage(PREFIX_MESSAGE + messageCode, null,
                null);
    }

    public static String getMessage(MessageSource messageSource,
            String messageCode, Object[] args) {

        return messageSource.getMessage(PREFIX_MESSAGE + messageCode, args,
                null);
    }

    public static String getMessageWithOutPrefix(MessageSource messageSource,
            String messageCode) {

        return messageSource.getMessage(messageCode, null, null);
    }

    public static String getMessageWithOutPrefix(MessageSource messageSource,
            String messageCode, Object[] args) {

        return messageSource.getMessage(messageCode, args, null);
    }

}
