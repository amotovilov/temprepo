package ru.olekstra.common.util;

import java.util.regex.Pattern;

public class OperationUtil {

    public static final String SEPARATOR = "#";
    public static final String NEXT_ID_RESERVE_CHARACTER = "0";

    public static final int NEXT_ID_VALUE_PARAMETER = 0;
    public static final int NEXT_ID_INDEX_PARAMETER = 1;

    public static final int NEXT_ID_START_INDEX = 2;

    public static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private static final Pattern pattern = Pattern.compile(EMAIL_PATTERN);

    public static String generateNextId(String currentId) {

        String nextId = currentId + SEPARATOR + NEXT_ID_START_INDEX;
        if (currentId.contains(SEPARATOR)) {
            try {

                String[] currentParsedId = currentId.split(SEPARATOR);
                int currentIndexValue = Integer
                        .parseInt(currentParsedId[NEXT_ID_INDEX_PARAMETER]);
                currentIndexValue++;
                nextId = currentParsedId[NEXT_ID_VALUE_PARAMETER] + SEPARATOR
                        + currentIndexValue;

            } catch (Exception e) {
                System.err.println("Incorect current id value: " + currentId);
            }
        }
        return nextId;
    }

    public static boolean hasNextRecord(String currentId) {

        if (currentId.contains(SEPARATOR)) {
            try {

                String nextIndex = currentId.split(SEPARATOR)[NEXT_ID_INDEX_PARAMETER];
                if (Integer.parseInt(nextIndex) == 0) {
                    return false;
                } else {
                    return true;
                }
            } catch (Exception e) {
                System.err.println("Incorect current id value: " + currentId);
            }
        }
        return false;
    }

    public static boolean isReadyToStoreNextId(String currentId, String nextId) {

        try {
            if (nextId.contains(SEPARATOR)) {

                String currentIndex = NEXT_ID_RESERVE_CHARACTER;
                if (currentId.split(SEPARATOR).length > NEXT_ID_INDEX_PARAMETER) {
                    currentIndex = currentId.split(SEPARATOR)[NEXT_ID_INDEX_PARAMETER];
                }

                String nextIndexReserve = nextId.split(SEPARATOR)[NEXT_ID_INDEX_PARAMETER];

                int nextIndexValue = Integer.parseInt(currentIndex) + 1;

                if (nextIndexReserve.length() == String.valueOf(nextIndexValue)
                        .length()) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (Exception e) {
            System.err.println("Incorect id values: currentId = " + currentId
                    + ", nextId = " + nextId);
        }
        return false;
    }

    public static String generateNextIdReserve(String currentId) {

        try {
            if (currentId.contains(SEPARATOR)) {

                String[] parsedCurrentId = currentId.split(SEPARATOR);
                String currentValue = parsedCurrentId[NEXT_ID_VALUE_PARAMETER];
                String currentIndex = parsedCurrentId[NEXT_ID_INDEX_PARAMETER];

                int nextIndexValue = Integer.parseInt(currentIndex) + 1;
                return currentValue
                        + SEPARATOR
                        + makeReserveIndex(String.valueOf(nextIndexValue)
                                .length());
            }
        } catch (Exception e) {
            System.err.println("Incorect currentId = " + currentId);
        }
        return currentId + SEPARATOR + makeReserveIndex(1);
    }

    public static String makeReserveIndex(int scale) {

        String result = "";
        for (int i = 0; i < scale; i++) {
            result += NEXT_ID_RESERVE_CHARACTER;
        }
        return result;
    }

    public static String deleteSpaces(String sourceStr) {
        if (sourceStr.contains(" ")) {
            String[] tmp = sourceStr.split(" ");
            sourceStr = "";
            for (String tmpStr : tmp) {
                tmpStr = tmpStr.trim();
                if (!tmpStr.isEmpty())
                    sourceStr += tmpStr;
            }
        }
        return sourceStr;
    }

    public static String deleteSplashes(String sourceStr) {
        if (sourceStr.contains("-")) {
            String[] tmp = sourceStr.split("-");
            sourceStr = "";
            for (String tmpStr : tmp) {
                tmpStr = tmpStr.trim();
                if (!tmpStr.isEmpty())
                    sourceStr += tmpStr;
            }
        }
        return sourceStr;
    }

    /**
     * Метод для очистки номер телефона от возможных "мусорных символов":
     * пробелы, тире
     * 
     * @param phoneNumber номер телефона для удаления лишних символов
     * @return "очищенный" номер телефон
     */
    public static String clearPhoneNumber(String phoneNumber) {
        return deleteSplashes(deleteSpaces(phoneNumber));
    }

    /**
     * Метод для очистки номера карты от возможных "мусорных символов": пробелы
     * 
     * @param cardNumber номер карты для удаления лишних символов
     * @return "очищенный" номер карты
     */
    public static String clearCardNumber(String cardNumber) {
        return deleteSpaces(cardNumber);
    }

    /**
     * Метод для очистки ект от возможных "мусорных символов": пробелы. Заменяет
     * все символы в ekt на upper case
     * 
     * @param cardNumber номер карты для удаления лишних символов
     * @return "очищенный" номер карты
     */
    public static String clearEkt(String ekt) {
        return deleteSpaces(ekt.toUpperCase());
    }

    /**
     * Метод проверят правильность заполнения email адреса
     * 
     * @param value адрес для проверки
     * @return true, если адрес верный, иначе false
     */
    public static boolean validateEmail(final String value) {
        return pattern.matcher(value).matches();
    }
}