package ru.olekstra.common.util;

import org.dozer.BeanFactory;

import ru.olekstra.domain.dto.DiscountLimitUsedDto;

public class DiscountLimitUsedDozerBeanFactory implements BeanFactory {

    @Override
    public Object createBean(Object arg0, Class<?> arg1, String arg2) {
        if (arg0 == null) {
            return null;
        }
        DiscountLimitUsedDto source = (DiscountLimitUsedDto) arg0;        
        return DiscountLimitUsedDto.createEntity(source);
    }

}
