package ru.olekstra.common.helper;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Iterator;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.lang.mutable.MutableInt;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import ru.olekstra.common.exception.ValidationException;

@Repository
public class XmlMessageHelper {

    static final String JAXP_SCHEMA_LANGUAGE = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
    static final String JAXP_SCHEMA_SOURCE = "http://java.sun.com/xml/jaxp/properties/schemaSource";
    public static final String MESSAGING_NAMESPACE = "http://www.olekstra.ru/2014/messaging";
    public static final String OBJECTS_NAMESPACE = "http://www.olekstra.ru/2014/objects";

    static final String W3C_XML_SCHEMA = "http://www.w3.org/2001/XMLSchema";

    private static final Logger LOGGER = Logger.getLogger(XmlMessageHelper.class);

    public XPath prepareXPath(final Document document) {
        XPathFactory xPathfactory = XPathFactory.newInstance();
        XPath xpath = xPathfactory.newXPath();
        xpath.setNamespaceContext(new NamespaceContext() {

            @SuppressWarnings("rawtypes")
            @Override
            public Iterator getPrefixes(String namespaceURI) {
                return null;
            }

            @Override
            public String getPrefix(String namespaceURI) {
                return null;
            }

            @Override
            public String getNamespaceURI(String prefix) {
                if ("obj".equals(prefix)) {
                    return OBJECTS_NAMESPACE;
                } else if ("mes".equals(prefix)) {
                    return MESSAGING_NAMESPACE;
                }

                return null;
            }
        });
        return xpath;
    }

    public Document parseXmlString(String xml)
            throws IllegalArgumentException, IOException, ParserConfigurationException, SAXException,
            ValidationException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(true);
        factory.setAttribute(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA);
        factory.setAttribute(JAXP_SCHEMA_SOURCE, new InputStream[] {
                getClass().getResource("objects.xsd").openStream(),
                getClass().getResource("messages.xsd").openStream()});
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        final MutableInt errorCount = new MutableInt();
        builder.setErrorHandler(new ErrorHandler() {

            @Override
            public void warning(SAXParseException exception) throws SAXException {
                LOGGER.warn(exception.getMessage());
            }

            @Override
            public void fatalError(SAXParseException exception) throws SAXException {
                LOGGER.error(exception.getMessage());
                errorCount.increment();
                throw exception;
            }

            @Override
            public void error(SAXParseException exception) throws SAXException {
                LOGGER.error(exception.getMessage());
                errorCount.increment();
                throw exception;
            }
        });
        InputSource inputSource = new InputSource(new StringReader(xml));
        Document document = builder.parse(inputSource);
        if (errorCount.intValue() > 0) {
            throw new ValidationException("XSD validation of message has been failed");
        }
        return document;
    }

    public String documentToString(Document doc)
            throws TransformerException {
        StringWriter sw = new StringWriter();
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

        transformer.transform(new DOMSource(doc), new StreamResult(sw));
        return sw.toString();
    }

    public Document createNewDocument(String rootElementNamespace, String rootElementName) throws ParserConfigurationException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        DOMImplementation domImpl = docBuilder.getDOMImplementation();

        Document doc = domImpl.createDocument(rootElementNamespace, rootElementName, null);

        doc.getDocumentElement().setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:obj", OBJECTS_NAMESPACE);
        
        return doc;
    }
    
    public Element createElement(Document doc, String namespace, String name) {
        Element element = doc.createElementNS(namespace, name);
        String prefix;
        if (namespace.equals(OBJECTS_NAMESPACE)) {
            prefix = "obj";
        } else {
            prefix = "mes";
        }
        element.setPrefix(prefix);
        
        return element;
    }
    
    public Element createElementWithText(Document doc, String namespace, String name, String text) {
        Element element = createElement(doc, namespace, name);
        element.appendChild(doc.createTextNode(text));
        
        return element;
    }
    
    public Node findNodeInList(String localName, NodeList list) {
        for (int i = 0; i < list.getLength(); i++) {
            Node elm = list.item(i);
            if (elm.getLocalName() != null && elm.getLocalName().equals(localName)) {
                return elm;
            }
        }
        return null;
    }
}
