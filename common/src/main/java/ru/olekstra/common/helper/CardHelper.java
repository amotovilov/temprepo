package ru.olekstra.common.helper;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ru.olekstra.awsutils.BatchOperationResult;
import ru.olekstra.awsutils.ComplexKey;
import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.common.annotation.CardAudit;
import ru.olekstra.common.annotation.CardNumber;
import ru.olekstra.common.annotation.CardObject;
import ru.olekstra.common.annotation.CardObjectList;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.CardBatch;
import ru.olekstra.domain.IndexEntity;
import ru.olekstra.domain.TelCardIndex;

import com.amazonaws.AmazonServiceException;

@Repository
public class CardHelper {

    private DynamodbService service;

    public CardHelper() {

    }

    @Autowired
    public CardHelper(DynamodbService service) {
        this.service = service;
    }

    public boolean addNewCard(Card card) {

        List<String> notExistingAttributes = new ArrayList<String>();
        notExistingAttributes.add(Card.FLD_IS_DISABLED);

        return service.putObjectNotExistingExpected(card,
                notExistingAttributes);
    }

    public Card getCard(String number) {
        return service.getObject(Card.class, number);
    }

    /**
     * Метод который в batch режиме запрашивает карточки batchSize по умолчанию
     * 25
     * 
     * @param numbers номера карточек к записи
     * @return список карточек
     * @throws InterruptedException
     * @throws RuntimeException
     * @throws UnsupportedEncodingException
     * @throws IOException
     */
    public BatchOperationResult<Card> getCardsInBatchMode(List<String> numbers)
            throws RuntimeException, InterruptedException, IOException {

        List<String> batch = new ArrayList<String>();
        int batchSize = service.getMaxBatchSize();
        BatchOperationResult<Card> resultCards = new BatchOperationResult<Card>();
        BatchOperationResult<Card> result = new BatchOperationResult<Card>();

        for (String number : numbers) {
            if (batch.size() == batchSize) {
                result = service.getObjects(Card.class, batch
                        .toArray(new String[batch.size()]));
                if ((result != null) && (result.getSuccessList().size() > 0)) {
                    resultCards.getSuccessList()
                            .addAll(result.getSuccessList());
                    result.getSuccessList().clear();
                }
                batch.clear();
            }
            batch.add(number);
        }

        if (batch.size() > 0) {
            result = service.getObjects(Card.class, batch
                    .toArray(new String[batch.size()]));
            if ((result != null) && (result.getSuccessList().size() > 0)) {
                resultCards.getSuccessList().addAll(result.getSuccessList());
                result.getSuccessList().clear();
            }
            batch.clear();
        }
        return resultCards;
    }

    public String getCardPhone(String number) {

        Map<String, Object> attributes = service.getAttributes(Card.TABLE_NAME,
                number, Arrays.asList(Card.FLD_TEL_NUMBER));

        return (String) attributes.get(Card.FLD_TEL_NUMBER);
    }

    public List<Card> getAllCards() throws IOException {

        List<Card> cardList = service.getAllObjects(Card.class);
        return cardList;
    }

    @CardAudit
    public boolean updateCard(@CardObject Card card) throws ItemSizeLimitExceededException {
        boolean success = service.putObject(card);
        if (success) {
            TelCardIndex cardIndex = new TelCardIndex(
                    card.getTelephoneNumber(), card.getNumber());
            success = service.putObject(cardIndex);
        }
        return success;
    }

    @CardAudit
    public boolean updateCardWithNullConditions(@CardObject Card card,
            List<String> emptyAttributesList) {
        return service.putObjectNotExistingExpected(card,
                emptyAttributesList);
    }

    /**
     * Возвращает номер карты, к которой привязан указанный телефон
     * 
     * @param phone номер телефона
     * @return номер карты, к которой привязан указанный телефон
     */
    public String checkPhone(String phone) {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add(TelCardIndex.FLD_INDEX);
        fieldNames.add(TelCardIndex.FLD_VALUE);

        Map<String, Object> attributes = service
                .getAttributes(TelCardIndex.TABLE_NAME, phone,
                        TelCardIndex.PREFIX, fieldNames);

        return (String) attributes.get(TelCardIndex.FLD_VALUE);
    }

    /**
     * Проверяет не зарегистрирована на указаный телефон карта
     * 
     * @param phone номер телефона
     * @return true елси номер используется, false если не используется
     */
    public boolean isPhoneUsed(String phone) {

        return phone != null && !phone.isEmpty() && checkPhone(phone) != null;
    }

    @CardAudit
    public boolean updateCardLimit(@CardNumber String cardNumber, BigDecimal expectedValue,
            BigDecimal newValue) {

        return service.updateAttributeWithCondition(Card.TABLE_NAME,
                cardNumber, Card.FLD_LIMIT_REMAIN, expectedValue.toString(),
                newValue.toString());
    }

    /**
     * Выполняет пакетное обновление карт
     * 
     * @param cards список карт готовых к обновению
     * @return список номеров карт обновить которые не удалось
     * @throws InterruptedException
     * @throws IllegalArgumentException
     * @throws AmazonServiceException
     * @throws ItemSizeLimitExceededException
     */
    @CardAudit
    public List<String> updateCards(@CardObjectList List<Card> cards)
            throws AmazonServiceException, IllegalArgumentException,
            InterruptedException, ItemSizeLimitExceededException {
        List<Card> batch = new ArrayList<Card>();
        int batchSize = service.getMaxBatchSize();
        List<ComplexKey> unprocessed = new ArrayList<ComplexKey>();
        List<String> resultNums = new ArrayList<String>();

        int counter = 0;
        for (Card card : cards) {
            batch.add(card);
            counter++;
            if ((batch.size() == batchSize) || (counter == cards.size())) {
                unprocessed = service.putObjects(
                        batch.toArray(new Card[batch.size()]))
                        .getUnSuccessList();
                for (ComplexKey key : unprocessed) {
                    resultNums.add(key.getHashKey());
                }
                batch.clear();
            }
        }
        return resultNums;
    }

    /**
     * Выполняет пакетное обновление индексов
     * 
     * @param cards список карт готовых к обновению
     * @return список номеров карт обновить которые не удалось
     * @throws InterruptedException
     * @throws IllegalArgumentException
     * @throws AmazonServiceException
     * @throws ItemSizeLimitExceededException
     */
    @SuppressWarnings("unchecked")
    public <T extends IndexEntity> List<String> updateIndexes(List<T> indexes)
            throws AmazonServiceException, IllegalArgumentException,
            InterruptedException, ItemSizeLimitExceededException {

        List<T> batch = new ArrayList<T>();
        int batchSize = service.getMaxBatchSize();
        List<ComplexKey> unprocessed = new ArrayList<ComplexKey>();
        List<String> resultNums = new ArrayList<String>();

        int counter = 0;
        for (T index : indexes) {
            batch.add(index);
            counter++;
            if ((batch.size() == batchSize) || (counter == indexes.size())) {
                unprocessed = service.putObjects(
                        (T[]) batch.toArray((T[]) Array.newInstance(index
                                .getClass(), batch.size()))).getUnSuccessList();
                for (ComplexKey key : unprocessed) {
                    resultNums.add(key.getHashKey());
                }
                batch.clear();
            }
        }
        return resultNums;
    }

    /**
     * Обновляет серии карт в таблице CardBatch, что нужно для корректной
     * выгрузки отчетов
     * 
     * @param cards список карт
     * @param batchName название серии
     * @param toDelete флаг для индикации операции: true удаление, false
     *            добавление
     * @return true если удалось успешно обновить списки
     */
    public boolean updateCardBatches(List<Card> cards, String batchName,
            boolean toDelete) {

        CardBatch cardBatch = service.getObject(CardBatch.class, batchName);

        if (cardBatch.getBatchList() == null) {
            cardBatch.setBatchList(new ArrayList<String>());
        }

        for (Card card : cards) {
            if (toDelete) {
                cardBatch.getBatchList().remove(card.getNumber());
            } else {
                if (!cardBatch.getBatchList().contains(card.getNumber())) {
                    cardBatch.getBatchList().add(card.getNumber());
                }
            }
        }

        try {
            return service.putObject(cardBatch);
        } catch (ItemSizeLimitExceededException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Удаляет телефон из индексной таблицы
     * 
     * @param telephoneNumber номер телефона
     */
    public void deleteTelephoneIndexes(Set<String> phones) {
        // TODO сделать поддержку массовой записи правильно
        for (String telephoneNumber : phones) {
            service.deleteItem(TelCardIndex.TABLE_NAME, telephoneNumber,
                    TelCardIndex.PREFIX);
        }
    }

    /**
     * Удаляет телефоны из индексной таблицы
     * 
     * @param telephoneNumber номер телефона
     */
    public void deleteTelephoneIndex(String telephoneNumber) {

        service.deleteItem(TelCardIndex.TABLE_NAME, telephoneNumber,
                TelCardIndex.PREFIX);
    }

    @Deprecated
    public boolean updateCardDiscountPlan(String cardNumber, String planId) {
        return service.updateAttribute(Card.TABLE_NAME, cardNumber,
                Card.FLD_DISCOUNT_PLAN_ID, planId);
    }

    public List<Card> getCardsBatch(List<String> cardNumbers)
            throws RuntimeException, InterruptedException, IOException {
        return service.getObjects(Card.class,
                cardNumbers.toArray(new String[cardNumbers.size()]))
                .getSuccessList();
    }

    public boolean saveCardIfNotExists(Card card)
            throws ItemSizeLimitExceededException {
        boolean success = service
                .putObjectNotExistingExpected(card, Arrays.asList(new String[] {Card.FLD_IS_DISABLED}));
        if (success) {
            TelCardIndex cardIndex = new TelCardIndex(
                    card.getTelephoneNumber(), card.getNumber());
            success = service.putObject(cardIndex);
        }
        return success;
    }
}
