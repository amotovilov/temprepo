package ru.olekstra.common.helper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ru.olekstra.awsutils.ComplexKey;
import ru.olekstra.awsutils.DynamodbEntity;
import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.domain.Discount;
import ru.olekstra.domain.DiscountPlan;

import com.amazonaws.AmazonServiceException;

@Repository
public class PlanHelper {

    private DynamodbService service;

    @Autowired
    public PlanHelper(DynamodbService service) {
        this.service = service;
    }

    public String getDiscountPlanName(String id) {

        DiscountPlan dp = service.getObject(DiscountPlan.class, id);
        if (dp == null) {
            return null;
        }
        return dp.getPlanName();
    }

    public List<DiscountPlan> getAllDiscountPlans() throws IOException {

        List<DiscountPlan> plans = new ArrayList<DiscountPlan>();
        List<Map<String, Object>> items = service.getAllItems(
                DiscountPlan.TABLE_NAME, null);

        for (Map<String, Object> attributes : items) {
            DiscountPlan plan = new DiscountPlan(attributes);
            plans.add(plan);
        }

        return plans;
    }

    public DiscountPlan getDiscountPlan(String id) {
        return service.getObject(DiscountPlan.class, id);
    }

    public void deletePlan(String id) {
        service.deleteItem(DiscountPlan.TABLE_NAME, id);
    }

    public boolean addPlan(DiscountPlan plan) throws ItemSizeLimitExceededException {
        return service.putObject(plan);
    }

    public boolean hasPlan(String planId) {
        List<String> fields = new ArrayList<String>();
        fields.add(new DiscountPlan().getHashKey());
        Map<String, Object> attributes = service.getAttributes(
                DiscountPlan.TABLE_NAME, planId, fields);
        return attributes.size() > 0;
    }

    public <T extends DynamodbEntity> List<ComplexKey> saveItems(T[] packArray)
            throws AmazonServiceException, IllegalArgumentException,
            InterruptedException, ItemSizeLimitExceededException {
        List<ComplexKey> unprocessed = new ArrayList<ComplexKey>();
        unprocessed = service.putObjects(packArray).getUnSuccessList();
        return unprocessed;
    }

    /**
     * get Amazon DB max size for batch save
     * 
     * @return size of batch
     */
    public int getMaxBatchSize() {
        return service.getMaxBatchSize();
    }

    /**
     * Return all available discounts
     * 
     * @return list of discounts
     * @throws IOException
     */
    public List<Discount> getAllDiscounts() throws IOException {
        return service.getAllObjects(Discount.class);
    }
}