package ru.olekstra.common.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ru.olekstra.awsutils.ComplexKey;
import ru.olekstra.awsutils.DynamodbEntity;
import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.common.util.OperationUtil;
import ru.olekstra.domain.CardLog;
import ru.olekstra.domain.CardOperation;
import ru.olekstra.domain.Drugstore;
import ru.olekstra.domain.EntityProxy;
import ru.olekstra.domain.dto.CardLogRecord;
import ru.olekstra.domain.dto.CardLogType;
import ru.olekstra.domain.dto.LogRecord;

import com.amazonaws.AmazonServiceException;

@Repository
public class CardOperationDao {

    public static final String VALUE_NULL = "null";

    private static final DateTimeFormatter cardFormatter = DateTimeFormat
            .forPattern("yyyyMM");

    private static final Logger LOGGER = Logger
            .getLogger(CardOperationDao.class);

    private DynamodbService service;
    private DrugstoreDao dao;

    @Autowired
    public CardOperationDao(DynamodbService service, DrugstoreDao dao) {
        this.service = service;
        this.dao = dao;
    }

    private String getActualPeriod() {
        return DateTime.now().withZone(DateTimeZone.UTC)
                .toString(cardFormatter);
    }

    private boolean storeLogRecord(String tableName, String id, String period,
            String value, String valueableField)
            throws ItemSizeLimitExceededException {

        return service.appendAttribute(tableName, id, period, valueableField,
                value);
    }

    private String getNextId(String tableName, String id, String rangeKey) {

        List<String> attributesToGet = new ArrayList<String>();
        attributesToGet.add(CardOperation.FLD_NEXT_ID);

        Map<String, Object> attributes = service.getAttributes(tableName,
                id, rangeKey, attributesToGet);

        String nextId = id;
        if (attributes != null) {
            String value = String.valueOf(attributes
                    .get(CardOperation.FLD_NEXT_ID));

            if (!VALUE_NULL.equals(value)) {
                nextId = value;
            }
        }
        return nextId;
    }

    private void updateNextId(String tableName, String currentId,
            String nextId, String rangeKey) {
        if (!OperationUtil.isReadyToStoreNextId(currentId, nextId)) {
            nextId = OperationUtil.generateNextIdReserve(currentId);
            saveNextId(tableName, currentId, rangeKey, nextId);
        }
    }

    private void saveNextId(String tableName, String currentId,
            String rangeKey,
            String nextId) {

        service.updateAttribute(tableName, currentId, rangeKey,
                CardOperation.FLD_NEXT_ID,
                nextId);
    }

    private String getCurrentId(String tableName, String id, String rangeKey) {

        String currentId = id;
        String nextId = getNextId(tableName, currentId, rangeKey);
        updateNextId(tableName, currentId, nextId, rangeKey);

        while (OperationUtil.hasNextRecord(nextId)) {
            currentId = nextId;
            nextId = getNextId(tableName, nextId, rangeKey);
            updateNextId(tableName, currentId, nextId, rangeKey);
        }

        return currentId;
    }

    /**
     * Сохраняет лог запись в таблицу CardOperation
     * 
     * @param cardId - номер карты
     * @param record - лог запись
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     * @throws ItemSizeLimitExceededException
     */
    public void saveLogRecord(String cardId, LogRecord record)
            throws JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException {

        String rangeKey = getActualPeriod();
        String currentId = getCurrentId(CardOperation.TABLE_NAME, cardId,
                rangeKey);

        try {
            storeLogRecord(CardOperation.TABLE_NAME, currentId, rangeKey,
                    DynamodbEntity
                            .toJson(record), CardOperation.FLD_LOG_RECORD);
        } catch (ItemSizeLimitExceededException islee) {

            String nextId = OperationUtil.generateNextId(currentId);

            saveNextId(CardOperation.TABLE_NAME, currentId, rangeKey, nextId);
            storeLogRecord(CardOperation.TABLE_NAME, nextId, rangeKey,
                    DynamodbEntity
                            .toJson(record), CardOperation.FLD_LOG_RECORD);
        }
    }

    private List<CardOperation> loadCardOperations(Set<String> cardNumbers)
            throws RuntimeException, InterruptedException, IOException {
        String[] ranges = new String[cardNumbers.size()];
        Arrays.fill(ranges, getActualPeriod());
        return service.getObjects(CardOperation.class,
                cardNumbers.toArray(new String[cardNumbers.size()]), ranges)
                .getSuccessList();
    }

    private void rewriteCardOperation(CardOperation operation,
            List<String> records)
            throws ItemSizeLimitExceededException {

        try {
            saveCurrentCardOperation(operation);
        } catch (ItemSizeLimitExceededException islee) {

            String period = operation.getPeriod();
            String currentId = getCurrentId(CardOperation.TABLE_NAME,
                    operation.getId(), period);

            String nextId = OperationUtil.generateNextId(currentId);
            saveNextId(CardOperation.TABLE_NAME, currentId, period,
                    nextId);

            operation = new CardOperation(nextId, period);
            operation.setLogRecord(records);
            saveCurrentCardOperation(operation);
        }
    }

    private List<String> saveCardOperations(List<CardOperation> operations,
            Map<String, LogRecord> records)
            throws AmazonServiceException,
            IllegalArgumentException, InterruptedException,
            JsonGenerationException, JsonMappingException,
            ItemSizeLimitExceededException, IOException {

        int batchSize = service.getMaxBatchSize();
        List<CardOperation> batch = new ArrayList<CardOperation>(batchSize);
        List<ComplexKey> unprocessed = new ArrayList<ComplexKey>();
        List<String> resultNums = new ArrayList<String>();

        int counter = 0;
        for (CardOperation operation : operations) {
            batch.add(operation);
            counter++;
            if ((batch.size() == batchSize) || (counter == operations.size())) {
                try {
                    unprocessed = service.putObjects(
                            batch.toArray(new CardOperation[batch.size()]))
                            .getUnSuccessList();

                    for (ComplexKey key : unprocessed) {
                        resultNums.add(key.getHashKey());
                    }
                } catch (ItemSizeLimitExceededException islee) {
                    for (CardOperation unprocessedOperation : batch) {
                        LOGGER.debug("Manually rewriting operation item for card #"
                                + unprocessedOperation.getId());
                        rewriteCardOperation(unprocessedOperation,
                                Arrays.asList(DynamodbEntity.toJson(records
                                        .get(unprocessedOperation.getId()))));
                    }
                }
                batch.clear();
            }
        }
        return resultNums;
    }

    /**
     * Сохраняет лог записи в таблицу CardOperation
     * 
     * @param records - номера карт и соответствующие им лог записи
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     * @throws ItemSizeLimitExceededException
     * @throws InterruptedException
     * @throws RuntimeException
     */
    public void saveLogRecords(Map<String, LogRecord> records)
            throws JsonGenerationException, JsonMappingException, IOException,
            ItemSizeLimitExceededException, RuntimeException,
            InterruptedException {

        String period = getActualPeriod();
        Set<String> cardNumbers = records.keySet();
        List<CardOperation> currentOperations = loadCardOperations(cardNumbers);

        Map<String, CardOperation> loadedCardOperations = new HashMap<String, CardOperation>(
                currentOperations.size());
        for (CardOperation operation : currentOperations) {
            loadedCardOperations.put(operation.getId(), operation);
        }

        List<CardOperation> operationsToSave = new ArrayList<CardOperation>(
                records.size());
        Set<String> loadedCardOperationNumbers = loadedCardOperations.keySet();
        for (String cardNumber : cardNumbers) {

            CardOperation operation = null;
            if (!loadedCardOperationNumbers.contains(cardNumber)) {
                operation = new CardOperation(cardNumber, period);
            } else {
                operation = loadedCardOperations.get(cardNumber);
            }

            if (operation.getLogRecord() == null) {
                operation.setLogRecord(new ArrayList<String>());
            }

            operation.getLogRecord().add(
                    DynamodbEntity.toJson(records.get(cardNumber)));
            operationsToSave.add(operation);
        }

        saveCardOperations(operationsToSave, records);
    }

    private CardOperation loadCurrentCardOperation(String cardOperationId,
            String actualPeriod) {
        return service.getObject(CardOperation.class, cardOperationId,
                actualPeriod);
    }

    private boolean saveCurrentCardOperation(CardOperation cardOperation)
            throws ItemSizeLimitExceededException {
        return service.putObject(cardOperation);
    }

    /**
     * Добавляет новые записи в логи для указанного номера карты. Если на
     * текущий период лог записей не существует, то создает новый item с
     * записями. Для дальнейшего чтения записи должны соответствовать
     * сериализованному JSON объекту CardRecord
     * 
     * @param cardNumber - номер карты
     * @param records - список JSON-форматированных лог записей
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     * @throws ItemSizeLimitExceededException
     */
    public void appendLogRecords(String cardNumber, List<String> records)
            throws JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException {

        String period = getActualPeriod();
        String currentId = getCurrentId(CardOperation.TABLE_NAME, cardNumber,
                period);

        CardOperation currentOperation = loadCurrentCardOperation(currentId,
                period);

        if (currentOperation == null) {
            LOGGER.debug("New opertion record was created for card #"
                    + cardNumber
                    + " and period " + period);
            currentOperation = new CardOperation(currentId, period);
        }

        if (currentOperation.getLogRecord() != null) {
            currentOperation.getLogRecord().addAll(records);
        } else {
            currentOperation.setLogRecord(records);
        }

        try {
            saveCurrentCardOperation(currentOperation);
        } catch (ItemSizeLimitExceededException islee) {
            String nextId = OperationUtil.generateNextId(currentId);
            saveNextId(CardOperation.TABLE_NAME, currentId, period,
                    nextId);

            currentOperation = new CardOperation(nextId, period);
            currentOperation.setLogRecord(records);
            saveCurrentCardOperation(currentOperation);
        }
    }

    @SuppressWarnings("unchecked")
    private void extractLogRecords(List<String> result,
            Map<String, Object> attributes, String fieldName) {
        Object value = attributes.get(fieldName);
        if (value != null) {
            if (value instanceof String)
                result.add((String) value);
            else
                result.addAll((List<String>) value);
        }
    }

    /**
     * Загружает лог записи по карте за указанный период
     * 
     * @param cardId - номер карты
     * @param period - перод (в формате yyyyMM)
     * @return List<CardRecord> - список лог записей по карте за указанный
     *         период
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    public List<CardLogRecord> getLogRecords(String cardId, String period)
            throws JsonParseException, JsonMappingException, IOException {

        List<String> records = new ArrayList<String>();
        List<String> attributesToGet = new ArrayList<String>();
        attributesToGet.add(CardOperation.FLD_NEXT_ID);
        attributesToGet.add(CardOperation.FLD_LOG_RECORD);

        Map<String, Object> attributes = service.getAttributes(
                CardOperation.TABLE_NAME, cardId, period, attributesToGet);

        String nextId = (String) attributes.get(CardOperation.FLD_NEXT_ID);
        extractLogRecords(records, attributes, CardOperation.FLD_LOG_RECORD);

        while (!attributes.isEmpty()) {
            attributes = service.getAttributes(CardOperation.TABLE_NAME,
                    nextId, period, attributesToGet);
            nextId = OperationUtil.generateNextId(nextId);
            extractLogRecords(records, attributes, CardOperation.FLD_LOG_RECORD);
        }

        // берем записи из CardLog
        List<CardLog> cardLogList = service.getItemsByRangeKeyBegin(
                CardLog.class, cardId, period);

        List<LogRecord> logRecordList = new ArrayList<LogRecord>();

        if (records != null) {
            for (String record : records) {
                logRecordList.add((LogRecord) DynamodbEntity.fromJson(record,
                        LogRecord.class));
            }
        }

        List<Drugstore> drugstoreList = new ArrayList<Drugstore>();
        if ((cardLogList != null) || (logRecordList != null)) {
            drugstoreList = dao.getDrugstoreNames();
        }

        List<CardLogRecord> cardLogRecordList = new ArrayList<CardLogRecord>();

        // складываем LogRecord-ы и CardLog-и в общий dto для вывода на
        // вебстранице.

        if (cardLogList != null) {
            for (CardLog cardLog : cardLogList) {
                UUID uuid = cardLog.getVoucherId();
                String voucherId;
                if (uuid != null)
                    voucherId = cardLog.getVoucherId().toString();
                else
                    voucherId = null;
                String ticketId;
                uuid = cardLog.getTicketId();
                if (uuid != null)
                    ticketId = cardLog.getTicketId().toString();
                else
                    ticketId = null;

                String drugstoreName = "";
                for (Drugstore drugstore : drugstoreList)
                    if (drugstore.getId().equalsIgnoreCase(
                            cardLog.getDrugstoreId()))
                        drugstoreName = drugstore.getName();

                CardLogRecord cardLogRecord = new CardLogRecord(
                        cardLog.getDate(),
                        cardLog.getType().getValue(),
                        cardLog.getType().getLabelCode(),
                        cardLog.getDrugstoreId(),
                        drugstoreName,
                        cardLog.getUser(),
                        cardLog.getNote(),
                        voucherId,
                        cardLog.getRowNum() == null ? "" : cardLog.getRowNum().toString(),
                        ticketId,
                        cardLog.isInternalUse(),
                        cardLog.isNotificationSend());
                cardLogRecordList.add(cardLogRecord);
            }
        }

        if (logRecordList != null) {
            for (LogRecord logRecord : logRecordList) {
                String voucherId = logRecord.getType().equals(
                        CardLogType.TRANSACTION)
                        ? logRecord.getVoucherRowId().split(
                                EntityProxy.getDelimeter())[0]
                        : null;
                String drugstoreName = "";
                for (Drugstore drugstore : drugstoreList)
                    if (drugstore.getId().equalsIgnoreCase(
                            logRecord.getDrugstoreId()))
                        drugstoreName = drugstore.getName();
                CardLogRecord cardLogRecord = new CardLogRecord(
                        logRecord.getDate(),
                        logRecord.getType().getValue(),
                        logRecord.getType().getLabelCode(),
                        logRecord.getDrugstoreId(),
                        drugstoreName,
                        logRecord.getUser(),
                        logRecord.getNote(),
                        voucherId, // в LogRecord есть VoucherRowId
                        logRecord.getRowNum(),
                        null, // в LogRecord нет TicketId
                        logRecord.isInternalUse(),
                        logRecord.isNotificationSent());
                cardLogRecordList.add(cardLogRecord);
            }
        }

        Collections.sort(cardLogRecordList, Collections.reverseOrder());

        return cardLogRecordList;
    }
}
