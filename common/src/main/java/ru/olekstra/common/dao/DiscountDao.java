package ru.olekstra.common.dao;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.domain.Discount;

import com.googlecode.ehcache.annotations.Cacheable;
import com.googlecode.ehcache.annotations.DecoratedCacheType;

@Repository
public class DiscountDao {

    private DynamodbService service;
    public static final String CACHE_NAME = "CommonListCache";

    public DiscountDao() {
    }

    @Autowired
    public DiscountDao(DynamodbService service) {
        this.service = service;
    }

    /**
     * Метод с кешированием, используем параметр attributes для того, чтобы
     * работал кеш. Чтобы кеш работал, вызывать нужно с одним и тем же
     * параметром.
     * 
     * @param attributes
     * @return
     * @throws RuntimeException
     * @throws InterruptedException
     * @throws IOException
     */
    @Cacheable(cacheName = CACHE_NAME, refreshInterval = 1000 * 900, decoratedCacheType = DecoratedCacheType.REFRESHING_SELF_POPULATING_CACHE)
    public List<Discount> getAllDiscountsUsingCache() throws RuntimeException,
            InterruptedException, IOException {
        return service.getAllObjects(Discount.class);
    }
}
