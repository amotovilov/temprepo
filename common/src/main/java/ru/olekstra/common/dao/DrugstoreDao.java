package ru.olekstra.common.dao;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.domain.Drugstore;

import com.googlecode.ehcache.annotations.Cacheable;
import com.googlecode.ehcache.annotations.DecoratedCacheType;
import com.googlecode.ehcache.annotations.TriggersRemove;

//  OLX-1456 - кэширование аптек из БД
@Repository
public class DrugstoreDao {

    public static final String CACHE_NAME = "CommonListCache";

    private DynamodbService service;

    public DrugstoreDao() {
    }

    // load cache when bean has been initialized
    @PostConstruct
    public void loadCachePointcut() {
    }

    @Autowired
    public DrugstoreDao(DynamodbService service) {
        this.service = service;
    }

    // set periodical autorefresh cache once per time in milliseconds
    @Cacheable(cacheName = CACHE_NAME,
            refreshInterval = 1000 * 3600,
            decoratedCacheType = DecoratedCacheType.REFRESHING_SELF_POPULATING_CACHE)
    public List<Drugstore> getDrugstoreNames() throws IOException {
        List<Drugstore> drugstoreList = service.getAllObjects(Drugstore.class);
        return drugstoreList;
    }

    // removing keys from cache on each db update
    @TriggersRemove(cacheName = CACHE_NAME, removeAll = true)
    public boolean concurrentUpdateDrugstore(Drugstore drugstore) {
        return service.putObjectWithCondition(drugstore, Drugstore.FLD_AUTH_CODE,
                drugstore.getAuthCode());
    }

}
