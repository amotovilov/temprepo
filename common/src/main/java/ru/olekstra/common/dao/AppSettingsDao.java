package ru.olekstra.common.dao;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.domain.AppSettings;

@Repository
public class AppSettingsDao {

    public static final String SETTINGS_KEY = "Settings";
    private DynamodbService service;

    @Autowired
    public AppSettingsDao(DynamodbService service) {
        this.service = service;
    }

    public boolean saveSettings(AppSettings settings) {
        try {
            return service.putObject(settings);
        } catch (ItemSizeLimitExceededException e) {
            e.printStackTrace();
            return false;
        }
    }

    public AppSettings loadSettings() {
        return service.getObject(AppSettings.class, SETTINGS_KEY);
    }

}
