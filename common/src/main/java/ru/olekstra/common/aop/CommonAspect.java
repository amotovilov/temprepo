package ru.olekstra.common.aop;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.annotation.Annotation;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import ru.olekstra.awsutils.AWSAccess;
import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.SnsService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.azure.model.MessageSendException;
import ru.olekstra.azure.service.QueueSender;
import ru.olekstra.common.annotation.CardNumber;
import ru.olekstra.common.annotation.CardObject;
import ru.olekstra.common.annotation.CardObjectList;
import ru.olekstra.common.dao.DrugstoreDao;
import ru.olekstra.common.service.CardExportService;
import ru.olekstra.common.service.XmlMessageService;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.dto.CardChangeDto;

@Component
@Aspect
@EnableAspectJAutoProxy
public class CommonAspect {
    @Autowired
    SnsService snsService;

    @Autowired
    CardExportService cardExportService;

    @Autowired
    DrugstoreDao drugstoreDao;

    @Autowired
    private QueueSender sender;

    @Autowired
    private DynamodbService service;

    @Autowired
    private XmlMessageService xmlMessageService;

    private String getPostRequestBody(HttpServletRequest req) {
        String body = "";
        if (req.getMethod().equals("POST"))
        {
            StringBuilder sb = new StringBuilder();
            BufferedReader bufferedReader = null;

            try {
                bufferedReader = req.getReader();
                char[] charBuffer = new char[128];
                int bytesRead;
                while ((bytesRead = bufferedReader.read(charBuffer)) != -1) {
                    sb.append(charBuffer, 0, bytesRead);
                }
            } catch (IOException ex) {
                // swallow silently -- can't get body, won't
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException ex) {
                        // swallow silently -- can't get body, won't
                    }
                }
            }
            body = sb.toString();
        }
        return body;
    }

    private void writeEx(JoinPoint jp, Exception ex) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (sra != null) {
            HttpServletRequest req = sra.getRequest();
            if (req != null) {
                pw.println("Current user : " + (req.getUserPrincipal() != null
                        ? req.getUserPrincipal().getName()
                        : ""));
                pw.println("Query : " + req.getQueryString());
                pw.println("Uri : " + req.getRequestURI());
                pw.println("Referer URL : " + req.getHeader("referer"));
                pw.println("Remote addr : " + req.getRemoteAddr());
                pw.println("Cookies : ");
                Cookie[] cookies = req.getCookies();
                if (cookies != null) {
                    for (Cookie cookie : cookies)
                        pw.println(cookie.getName() + " : " + cookie.getValue());
                }
                pw.println("Request Parameters : ");
                Map<String, String[]> parameterMap = req.getParameterMap();
                if (parameterMap != null)
                    for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
                        StringBuilder keyValues = new StringBuilder();
                        keyValues.append("  ").append(entry.getKey()).append(" = ");
                        for (String paramValue : entry.getValue())
                            keyValues.append(paramValue).append("  ");
                        pw.println(keyValues.toString());
                    }
                pw.println("=== end of request parameters ===");
                pw.println("Method args : ");
                MethodSignature signature = (MethodSignature) jp.getSignature();
                String[] parameterNames = signature.getParameterNames();
                Object[] args = jp.getArgs();
                if (parameterNames.length == args.length)
                    for (int i = 0; i < args.length; i++)
                        pw.println("  " + parameterNames[i] + " : " + args[i].toString());
                else
                    for (int i = 0; i < args.length; i++)
                        pw.println("  " + i + " : " + args[i].toString());

                pw.println("=== end of method args ===");
                pw.println("Raw request body:");
                pw.println(this.getPostRequestBody(req));
                pw.println("=== end of request body ===");
            }
        }
        // Stack Trace
        ex.printStackTrace(pw);

        if (snsService.canSend()) {
            snsService.publishAdminAlertMessage(
                    String.format("controller unhadled exception (%s)", AWSAccess.ENVIRONMENT), sw.toString());
        }
    }

    @AfterThrowing(pointcut = "execution(public * ru.olekstra.qupro.processing.*.*(..))", throwing = "ex")
    public void afterThrowingQuProAdvice(JoinPoint jp, Exception ex) {
        writeEx(jp, ex);
    }

    @AfterThrowing(pointcut = "execution(public * ru.olekstra.*.controller.*.*(..))", throwing = "ex")
    public void afterThrowingAdvice(JoinPoint jp, Exception ex) {
        writeEx(jp, ex);
    }

    public Annotation[] getAnn(MethodSignature signature) {
        return signature.getMethod().getParameterAnnotations()[0];
    }

    private void sendCardUpdateMessages(Iterable<String> cardNumbers)
            throws MessageSendException, ParserConfigurationException, TransformerException {
        for (String cardNumber : cardNumbers) {
            Card card = service.getObject(Card.class, cardNumber);
            sender.send(xmlMessageService.createCardUpdateMessage(card));
        }
    }

    @AfterReturning("@annotation(ru.olekstra.common.annotation.CardAudit)")
    public void cardAuditAdvice(JoinPoint joinPoint)
            throws ItemSizeLimitExceededException, InterruptedException, MessageSendException,
            ParserConfigurationException, TransformerException, RuntimeException, IOException {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();

        List<String> cardNumberList = null;
        String cardNumber = null;

        int i = 0;
        for (Annotation ann : getAnn(signature)) {
            if (CardNumber.class.isInstance(ann)) {
                cardNumber = joinPoint.getArgs()[i].toString();
                break;
            }
            i++;
        }

        i = 0;
        for (Annotation ann : getAnn(signature)) {
            if (CardObject.class.isInstance(ann)) {
                cardNumber = ((Card) joinPoint.getArgs()[i]).getNumber();
                break;
            }
            i++;
        }

        i = 0;
        for (Annotation ann : getAnn(signature)) {
            if (CardObjectList.class.isInstance(ann)) {
                cardNumberList = new ArrayList<String>();
                @SuppressWarnings("unchecked")
                List<Card> list = (List<Card>) joinPoint.getArgs()[i];
                for (Card card : list) {
                    cardNumberList.add(card.getNumber());
                }
                break;
            }
            i++;
        }

        if (cardNumber == null & cardNumberList == null) {
            throw new InvalidParameterException(
                    "@CardAudit annotated method found. Any CardNumber annotated parameter required.");
        }

        if (cardNumber != null & cardNumberList != null) {
            throw new InvalidParameterException(
                    "@CardAudit annotated method found. Only one CardNumber annotated parameter allow.");
        }

        Set<String> cardsNumbersToAudit = new HashSet<String>();
        if (cardNumber != null)
            cardsNumbersToAudit.add(cardNumber);

        if (cardNumberList != null) {
            for (String s : cardNumberList) {
                cardsNumbersToAudit.add(s);
            }
        }

        sendCardUpdateMessages(cardsNumbersToAudit);
        List<CardChangeDto> sended = cardExportService.cardsChangeEvent(cardsNumbersToAudit);

    }
}
