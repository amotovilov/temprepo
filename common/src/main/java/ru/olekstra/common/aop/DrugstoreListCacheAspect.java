package ru.olekstra.common.aop;

import java.io.IOException;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

import ru.olekstra.common.dao.DrugstoreDao;

@Component
@Aspect
@EnableAspectJAutoProxy
public class DrugstoreListCacheAspect {
    @Autowired
    DrugstoreDao drugstoreDao;

    @After("@annotation(com.googlecode.ehcache.annotations.TriggersRemove)"
            + " || @annotation(javax.annotation.PostConstruct))"
            + " && @within(ru.olekstra.common.dao.DrugstoreDao")
    public void refreshDrugstoreListCacheOnTriggersRemove() throws IOException {
        drugstoreDao.getDrugstoreNames();
    }

}
