package ru.olekstra.common.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SmsServiceTest {

    private AppSettingsService settingsService;
    private SmsService service;

    @Before
    public void setup() {
        settingsService = mock(AppSettingsService.class);
        service = new SmsService(settingsService);
    }

    @Test
    public void testSms() throws ClientProtocolException, IOException {
        when(settingsService.getSmsGateSettings())
                .thenReturn(
                        "url=http://smsc.ru/sys/send.php|login=pharmanet|psw=01b630fe8d185bcb47005d162f7e0b4a|list=${recipients}|fmt=3|sender=Olekstra.ru|charset=utf-8");

        boolean result = service.send("0000000000",
                "Привет, Hello from olekstra.ru");

        assertTrue(result);
    }

    @Test
    public void testSendSingleSms() throws ClientProtocolException,
            IOException {

        final int NUMBER = 10;
        final int THREHOLD = 1000;

        when(settingsService.getSmsGateSettings())
                .thenReturn(
                        "url=http://smsc.ru/sys/send.php|login=pharmanet|psw=01b630fe8d185bcb47005d162f7e0b4a|list=${recipients}|fmt=3|sender=Olekstra.ru|charset=utf-8");

        when(settingsService.getSmsMaxSendThreshold())
                .thenReturn(THREHOLD);

        String[] phones = new String[NUMBER];
        String message = "Тестовое сообщение от olekstra.ru";

        // mock test message recipient number 000000000x
        for (int i = 0; i < NUMBER; i++) {
            phones[i] = "000000000" + (i + 1);
        }

        System.out.println("Time start at: " + System.currentTimeMillis()
                + ", [ms]");
        System.out.println("Time start date: "
                + new Date(System.currentTimeMillis()).toString());

        service.sendSingleMessage(Arrays.asList(phones), message);

        System.out.println("Time end at: " + System.currentTimeMillis()
                + ", [ms]");
        System.out.println("Time end date: "
                + new Date(System.currentTimeMillis()).toString());
    }

    @Test
    public void testSendMultipleSms() throws ClientProtocolException,
            IOException {

        final int NUMBER = 10;
        final int THREHOLD = 1000;

        when(settingsService.getSmsGateSettings())
                .thenReturn(
                        "url=http://smsc.ru/sys/send.php|login=pharmanet|psw=01b630fe8d185bcb47005d162f7e0b4a|list=${recipients}|fmt=3|sender=Olekstra.ru|charset=utf-8");

        when(settingsService.getSmsMaxSendThreshold())
                .thenReturn(THREHOLD);

        String[] phones = new String[NUMBER];
        String[] messages = new String[NUMBER];

        // mock test message recipient number 0000000000
        for (int i = 0; i < NUMBER; i++) {
            phones[i] = "0000000000";
            messages[i] =
                    "Тестовое сообщение от olekstra.ru #" + (i + 1);
        }

        @SuppressWarnings("unchecked")
        Map<String, String>[] recipients = new
                Map[NUMBER];
        for (int i = 0; i < NUMBER; i++) {
            recipients[i] = new
                    HashMap<String, String>();
            recipients[i].put(phones[i], messages[i]);
        }

        System.out.println("Time start at: " + System.currentTimeMillis()
                + ", [ms]");
        System.out.println("Time start date: "
                + new Date(System.currentTimeMillis()).toString());

        service.sendMultpleMessages(Arrays.asList(recipients));

        System.out.println("Time end at: " + System.currentTimeMillis()
                + ", [ms]");
        System.out.println("Time end date: "
                + new Date(System.currentTimeMillis()).toString());
    }

    @Test
    public void getCardNum4DigitsTest() {
        assertNotNull(service.getCardNumLastFourDigits(null));
        assertEquals("", service.getCardNumLastFourDigits(null));
        assertEquals("*4321", service.getCardNumLastFourDigits("987654321"));
        assertEquals("4321", service.getCardNumLastFourDigits("4321"));
    }
}
