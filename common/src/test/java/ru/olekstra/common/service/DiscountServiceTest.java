package ru.olekstra.common.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.AdditionalMatchers.aryEq;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.dozer.DozerBeanMapper;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;

import ru.olekstra.awsutils.BatchOperationResult;
import ru.olekstra.awsutils.DynamodbEntity;
import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.BatchGetException;
import ru.olekstra.awsutils.exception.OlekstraException;
import ru.olekstra.common.dao.DiscountDao;
import ru.olekstra.common.helper.DozerHelper;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.Discount;
import ru.olekstra.domain.DiscountData;
import ru.olekstra.domain.DiscountLimit;
import ru.olekstra.domain.DiscountLimitUsed;
import ru.olekstra.domain.DiscountPlan;
import ru.olekstra.domain.DiscountReplace;
import ru.olekstra.domain.EntityProxy;
import ru.olekstra.domain.Pack;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.dto.DiscountLimitRemaining;
import ru.olekstra.domain.dto.DiscountLimitUsedDetail;
import ru.olekstra.domain.dto.DiscountLimitUsedDto;
import ru.olekstra.domain.dto.DiscountLimitUsedRecord;
import ru.olekstra.domain.dto.DiscountLimitUsedTotal;
import ru.olekstra.domain.dto.PackDiscountData;
import ru.olekstra.domain.dto.PackDiscountLimitData;
import ru.olekstra.domain.dto.VoucherResponseRow;

@RunWith(MockitoJUnitRunner.class)
public class DiscountServiceTest {

    private DiscountDao dao;
    private DynamodbService dynamodbService;
    private DiscountService service;
    private PlanService planService;
    private DozerBeanMapper mapper;
    private UUID zero_UUID;
    private AppSettingsService settingsService;
    private MessageSource messageSource;
    private DateTimeService dateService;
    private String currentMonthPeriod;
    private DiscountLimitUsedTotal dlur1;
    private DiscountLimitUsedTotal dlur2;
    private List<DiscountLimitUsedTotal> alreadyLoadedDiscountLimitUsedRecordList;

    private DiscountLimitUsed newDiscountLimitUsed(String period, String cardNum, UUID voucherId, Integer row,
            String limitGroupId, String discountId, Integer packCount, Long averagePackCount,
            BigDecimal discountSum, BigDecimal sumBeforeDiscount) {
        String hashKey = null;
        String rangeKey = null;
        if (voucherId == null
                || voucherId.toString().equals(EntityProxy.NULL_UUID)) {
            hashKey = DiscountLimitUsed.buildKey(period, cardNum, DiscountLimitUsed.TYPE_TOTAL);
            rangeKey = DiscountLimitUsed.buildKey(limitGroupId, discountId,
                    EntityProxy.NULL_UUID, "0");
        } else {
            hashKey = DiscountLimitUsed.buildKey(period, cardNum, DiscountLimitUsed.TYPE_DETAIL);
            rangeKey = DiscountLimitUsed.buildKey(limitGroupId, discountId,
                    voucherId.toString(),
                    String.valueOf(row));
        }
        DiscountLimitUsed dlu = new DiscountLimitUsed(hashKey, rangeKey,
                packCount, averagePackCount, discountSum, sumBeforeDiscount);
        return dlu;
    }

    @Before
    public void initData() {
        dao = mock(DiscountDao.class);
        dynamodbService = mock(DynamodbService.class);
        planService = mock(PlanService.class);
        settingsService = mock(AppSettingsService.class);

        dateService = mock(DateTimeService.class);
        when(dateService.createDateTimeWithZone(anyString())).thenReturn(DateTime.now());
        when(dateService.createDefaultDateTime()).thenReturn(DateTime.now());
        when(dateService.createUTCDate()).thenReturn(DateTime.now().withZone(DateTimeZone.UTC));

        mapper = new org.dozer.DozerBeanMapper();
        mapper.setMappingFiles(Collections
                .singletonList("ru/olekstra/common/mappings.xml"));
        messageSource = mock(MessageSource.class);

        service = new DiscountService(dao, dynamodbService, planService,
                settingsService, mapper, messageSource, dateService);
        zero_UUID = UUID.fromString("00000000-0000-0000-0000-000000000000");

        currentMonthPeriod = this.getCurrentPeriod("month", DateTime.now());

        dlur1 = new DiscountLimitUsedTotal(currentMonthPeriod,
                cardNumber, zero_UUID, 0, "group1", "one", 1, 100l,
                new BigDecimal(50), BigDecimal.ZERO);
        dlur2 = new DiscountLimitUsedTotal(currentMonthPeriod,
                cardNumber, zero_UUID, 0, "group2", "one", 1, 100l,
                new BigDecimal(50), BigDecimal.ZERO);
        alreadyLoadedDiscountLimitUsedRecordList = Arrays.asList(new DiscountLimitUsedTotal[] {dlur1, dlur2});

    }

    @Test
    public void testGetDiscounts() throws InterruptedException,
            RuntimeException, OlekstraException, IOException {
        String id1 = "id1";
        String id2 = "id2";
        String id3 = "id3";

        Discount d1 = new Discount();
        d1.setId(id1);
        Discount d2 = new Discount();
        d2.setId(id2);
        Discount d3 = new Discount();
        d3.setId(id3);

        List<Discount> allDiscounts = new ArrayList<Discount>();
        allDiscounts.add(d1);
        allDiscounts.add(d2);
        allDiscounts.add(d3);

        when(dao.getAllDiscountsUsingCache()).thenReturn(allDiscounts);

        List<String> requiredIds = new ArrayList<String>();
        requiredIds.add(id3);
        requiredIds.add(id1);

        List<Discount> result = service.getDiscounts(requiredIds);

        assertEquals(2, result.size());
        assertEquals(d3, result.get(0));
        assertEquals(d1, result.get(1));
    }

    @Test
    public void testGetDiscountDataList() throws JsonParseException,
            JsonMappingException, BatchGetException, IOException {

        String packEkt1 = "packEkt1";
        String packEkt2 = "packEkt2";

        VoucherResponseRow row1 = new VoucherResponseRow();
        row1.setId(packEkt1);

        VoucherResponseRow row2 = new VoucherResponseRow();
        row2.setId(packEkt2);

        ProcessingTransaction transaction = new ProcessingTransaction(DateTime.now());
        transaction.setRows(Arrays.asList(row1, row2));

        int promoPercent = 30;

        int version0 = 0;
        int version1 = 1;
        int version2 = 2;
        int version3 = 3;

        String discountId0 = "discountId0";
        String discountId1 = "discountId1";
        String discountId2 = "discountId2";
        String discountId3 = "discountId3";

        Discount discount0 = new Discount();
        discount0.setId(discountId0);
        discount0.setCurrentVersion(version0 + 5);

        Discount discount1 = new Discount();
        discount1.setId(discountId1);
        discount1.setCurrentVersion(version1);

        Discount discount2 = new Discount();
        discount2.setId(discountId2);
        discount2.setCurrentVersion(version2);

        Discount discount3 = new Discount();
        discount3.setId(discountId3);
        discount3.setCurrentVersion(version3);
        discount3.setPromoPercent(promoPercent);

        Integer percent11 = 10;
        Integer percent12 = 15;
        Integer percent21 = 20;
        Integer percent22 = 25;

        DiscountData discountData01 = new DiscountData();
        discountData01.setDiscount(discountId1);
        discountData01.setEkt(packEkt1);
        discountData01.setCurrentVersion(version0);

        DiscountData discountData02 = new DiscountData();
        discountData02.setDiscount(discountId2);
        discountData02.setEkt(packEkt2);
        discountData02.setCurrentVersion(version0);

        DiscountData discountData11 = new DiscountData();
        discountData11.setDiscount(discountId1);
        discountData11.setEkt(packEkt1);
        discountData11.setPercent(percent11);
        discountData11.setCurrentVersion(version1);

        DiscountData discountData12 = new DiscountData();
        discountData12.setDiscount(discountId1);
        discountData12.setEkt(packEkt2);
        discountData12.setPercent(percent12);
        discountData12.setCurrentVersion(version1);

        DiscountData discountData21 = new DiscountData();
        discountData21.setDiscount(discountId2);
        discountData21.setEkt(packEkt1);
        discountData21.setPercent(percent21);
        discountData21.setCurrentVersion(version2);

        DiscountData discountData22 = new DiscountData();
        discountData22.setDiscount(discountId2);
        discountData22.setEkt(packEkt2);
        discountData22.setPercent(percent22);
        discountData22.setCurrentVersion(version2);

        List<DiscountData> discountDataListFromDb = Arrays.asList(
                discountData01,
                discountData02, discountData11, discountData12, discountData21,
                discountData22);

        when(dynamodbService.getObjectsAllOrDie(
                eq(DiscountData.class),
                any(String[].class),
                any(String[].class)))
                .thenReturn(discountDataListFromDb);

        service.getDiscountDataList(
                Arrays.asList(discount0, discount1, discount2, discount3),
                transaction);

        verify(dynamodbService).getObjectsAllOrDie(
                eq(DiscountData.class),
                eq(new String[] {discountId0, discountId0, discountId1,
                        discountId1, discountId2, discountId2}),
                eq(new String[] {packEkt1, packEkt2, packEkt1, packEkt2,
                        packEkt1, packEkt2}));

    }

    @Test
    public void testSortDiscountData() {
        int curVer = 1;

        int version1 = 1;
        int version2 = 2;

        String ekt1 = "ekt1";
        String ekt2 = "ekt2";
        String ekt3 = "ekt3";

        String discountId1 = "uno";
        String discountId2 = "dos";

        Discount d1 = new Discount(discountId1, "nameUno", true, true, 60,
                curVer, 3);
        Discount d2 = new Discount(discountId2, "nameDos", true, true, 70,
                curVer, 2);
        List<Discount> discountList = Arrays.asList(new Discount[] {d1, d2});

        DiscountData discountData11v1 = new DiscountData();
        discountData11v1.setEkt(ekt1);
        discountData11v1.setDiscount(discountId1);
        discountData11v1.setCurrentVersion(version1);

        DiscountData discountData11v2 = new DiscountData();
        discountData11v2.setEkt(ekt1);
        discountData11v2.setDiscount(discountId1);
        discountData11v2.setCurrentVersion(version2);

        DiscountData discountData12v1 = new DiscountData();
        discountData12v1.setEkt(ekt1);
        discountData12v1.setDiscount(discountId2);
        discountData12v1.setCurrentVersion(version1);

        DiscountData discountData22v1 = new DiscountData();
        discountData22v1.setEkt(ekt2);
        discountData22v1.setDiscount(discountId2);
        discountData22v1.setCurrentVersion(version1);

        DiscountData discountData31v1 = new DiscountData();
        discountData31v1.setEkt(ekt3);
        discountData31v1.setDiscount(discountId1);
        discountData31v1.setCurrentVersion(version1);

        DiscountData discountData32v2 = new DiscountData();
        discountData32v2.setEkt(ekt3);
        discountData32v2.setDiscount(discountId2);
        discountData32v2.setCurrentVersion(version2);

        List<DiscountData> discountDataList = Arrays.asList(
                discountData11v1, discountData22v1,
                discountData11v2, discountData31v1,
                discountData12v1, discountData32v2);

        List<DiscountData> sortedDiscountDataList = service.sortDiscountData(
                discountList, discountDataList);

        for (DiscountData dd : sortedDiscountDataList) {
            System.out.println(dd.getEkt() + " " + dd.getDiscount() + " "
                    + dd.getCurrentVersion());
        }

        assertEquals(4, sortedDiscountDataList.size());
        assertEquals(discountData11v1, sortedDiscountDataList.get(0));
        assertEquals(discountData31v1, sortedDiscountDataList.get(1));
        assertEquals(discountData22v1, sortedDiscountDataList.get(2));
        assertEquals(discountData12v1, sortedDiscountDataList.get(3));

    }

    @Test
    public void testGetFullDiscountLimitList() throws BatchGetException {
        int ver = 1;
        int percent = 50;
        int weight = 10;
        String limitGroup1 = "limitGroup1";
        String limitGroup2 = "limitGroup2";
        String limitGroup3 = "limitGroup3";
        String limitGroup4 = "limitGroup4";
        // для этих дисконтов будем искать лимиты.
        // у пары товаров совпадает LimitGroupId
        DiscountData discountData11 = new DiscountData("id1", "ekt11", ver, percent, limitGroup1, weight);
        DiscountData discountData12 = new DiscountData("id1", "ekt12", ver, percent, limitGroup1, weight);
        DiscountData discountData2 = new DiscountData("id2", "ekt2", ver, percent, limitGroup2, weight);
        DiscountData discountData3 = new DiscountData("id2", "ekt3", ver, percent, limitGroup3, weight);
        List<DiscountData> ddlForFind = Arrays.asList(discountData11, discountData12, discountData2, discountData3);

        // from DB
        DiscountLimit dl1 = new DiscountLimit("id1", limitGroup1, ver, "Month", 8,
                1500l, new BigDecimal(9999), BigDecimal.ZERO);
        DiscountLimit dl2 = new DiscountLimit("id2", limitGroup2, ver, "Month", 8,
                1500l, new BigDecimal(9999), BigDecimal.ZERO);
        List<DiscountLimit> discountLimitListFromDB = Arrays.asList(dl1, dl2);

        // Лимиты, соответствующие дисконту 3 и 4 "уже были" в
        // ProcessingTransaction
        DiscountLimit dl3 = new DiscountLimit("id2", limitGroup3, ver, "Month", 8,
                1500l, new BigDecimal(9999), BigDecimal.ZERO);
        DiscountLimit dl4 = new DiscountLimit("id2", limitGroup4, ver, "Month", 7,
                1500l, new BigDecimal(9999), BigDecimal.ZERO);
        List<DiscountLimit> alreadyKnownDiscountLimitList = Arrays.asList(dl3, dl4);

        // схватить, что там будет подсунуто в сервис БД
        ArgumentCaptor<String[]> discountIdArgs = ArgumentCaptor.forClass(String[].class);
        ArgumentCaptor<String[]> limitGroupIdArgs = ArgumentCaptor.forClass(String[].class);

        when(dynamodbService.getObjectsAllOrDie(eq(DiscountLimit.class), any(String[].class), any(String[].class)))
                .thenReturn(discountLimitListFromDB);

        service.getFullDiscountLimitList(ddlForFind, alreadyKnownDiscountLimitList);

        verify(dynamodbService).getObjectsAllOrDie(eq(DiscountLimit.class),
                discountIdArgs.capture(),
                limitGroupIdArgs.capture());
        // проверим, что запрашивали действительно с 4 парами ключей (две *)
        String[] discountIds = discountIdArgs.getValue();
        String[] limitGroupIds = limitGroupIdArgs.getValue();
        assertEquals(discountLimitListFromDB.size() * DiscountLimit.getAllPurposeLimitId().size()
                + discountLimitListFromDB.size(), discountIds.length);
        assertEquals(discountLimitListFromDB.size() * DiscountLimit.getAllPurposeLimitId().size()
                + discountLimitListFromDB.size(), limitGroupIds.length);

        int starCount = 0;
        for (String lgi : limitGroupIds)
            if (DiscountLimit.isAllPurposeLimitGroupId(lgi))
                starCount++;

        assertEquals(discountLimitListFromDB.size() * DiscountLimit.getAllPurposeLimitId().size(), starCount);
    }

    @Test
    public void testFillRowsDiscountAndLimitsData() throws JsonGenerationException, JsonMappingException, IOException {
        List<VoucherResponseRow> rows = new ArrayList<VoucherResponseRow>();
        VoucherResponseRow row0, row1, row2;

        row0 = new VoucherResponseRow("rowName00", "000", new BigDecimal(10l), 1, 1);
        row0.setId("000");
        row0.setClientPackId("clientPackId000");
        row0.setClientPackName("clientPackName000");
        row0.setDiscountPercent(ProcessingTransaction.FORMATTED_ZERO);
        row0.setDiscountSum(ProcessingTransaction.FORMATTED_ZERO);
        row0.setPackDiscount(null);
        row0.setSumToPay(ProcessingTransaction.FORMATTED_ZERO);
        row0.setCardLimitUsedSum(ProcessingTransaction.FORMATTED_ZERO);
        row0.setRefundSum(ProcessingTransaction.FORMATTED_ZERO);
        rows.add(row0);

        row1 = new VoucherResponseRow("rowName01", "001", new BigDecimal(10l), 1, 2);
        row1.setId("001");
        row1.setClientPackId("clientPackId001");
        row1.setClientPackName("clientPackName001");
        row1.setDiscountPercent(ProcessingTransaction.FORMATTED_ZERO);
        row1.setDiscountSum(ProcessingTransaction.FORMATTED_ZERO);
        row1.setPackDiscount(null);
        row1.setSumToPay(ProcessingTransaction.FORMATTED_ZERO);
        row1.setCardLimitUsedSum(ProcessingTransaction.FORMATTED_ZERO);
        row1.setRefundSum(ProcessingTransaction.FORMATTED_ZERO);
        rows.add(row1);

        row2 = new VoucherResponseRow("rowName02", "002", new BigDecimal(20l), 2, 3);
        row2.setId("002");
        row2.setClientPackId("clientPackId002");
        row2.setClientPackName("clientPackName002");
        row2.setDiscountPercent(ProcessingTransaction.FORMATTED_ZERO);
        row2.setDiscountSum(ProcessingTransaction.FORMATTED_ZERO);
        row2.setPackDiscount(null);
        row2.setSumToPay(ProcessingTransaction.FORMATTED_ZERO);
        row2.setCardLimitUsedSum(ProcessingTransaction.FORMATTED_ZERO);
        row2.setRefundSum(ProcessingTransaction.FORMATTED_ZERO);
        rows.add(row2);

        ProcessingTransaction transaction = new ProcessingTransaction(DateTime.now());
        transaction.setRows(rows);

        DiscountData dd0 = new DiscountData("uno", "000", 2, 50, "group1", 1);
        DiscountData dd1 = new DiscountData("dos", "001", 2, 45, null, null);
        DiscountData dd2 = new DiscountData("dos", "002", 2, 45, null, null);
        DiscountData dd3 = new DiscountData("tre", "002", 2, 40, null, null);

        List<DiscountData> ddl = Arrays.asList(dd0, dd1, dd2, dd3);

        DiscountLimit dl1 = new DiscountLimit("uno", "group1", 2,
                "Month", 8, 150l, new BigDecimal(999), BigDecimal.ZERO);
        DiscountLimit dl1_all = new DiscountLimit("uno", "*", 2,
                "Year", 0, 0L, BigDecimal.ZERO, new BigDecimal(999));
        DiscountLimit dl2_all = new DiscountLimit("dos", "*", 2,
                "Halfyear", 0, 0L, BigDecimal.ZERO, new BigDecimal(1111));
        List<DiscountLimit> dll = Arrays.asList(dl1, dl1_all, dl2_all);

        service.fillRowsDiscountAndLimitsData(transaction, ddl, dll);

        // строка 1 - 2 лимита, общий и group1
        PackDiscountData[] pdd0 = transaction.getRows().get(0).getPackDiscount();
        assertNotNull(pdd0);
        assertEquals(1, pdd0.length);
        PackDiscountLimitData[] pdld0 = pdd0[0].getPackDiscountLimitData();
        assertNotNull(pdld0);
        assertEquals(2, pdld0.length);
        assertEquals("group1", pdld0[0].getLimitGroupId());
        assertEquals("*", pdld0[1].getLimitGroupId());

        // строка 2 - 1 лимит, только общий
        PackDiscountData[] pdd1 = transaction.getRows().get(1).getPackDiscount();
        assertNotNull(pdd1);
        assertEquals(1, pdd1.length);
        PackDiscountLimitData[] pdld1 = pdd1[0].getPackDiscountLimitData();
        assertNotNull(pdld1);
        assertEquals(1, pdld1.length);
        assertEquals("*", pdld1[0].getLimitGroupId());

        // строка 3 - 2 дисконта. Один с 1 лимитом, только общим
        // другой без лимитов
        PackDiscountData[] pdd2 = transaction.getRows().get(2).getPackDiscount();
        assertNotNull(pdd2);
        assertEquals(2, pdd2.length);

        PackDiscountLimitData[] pdld21 = pdd2[0].getPackDiscountLimitData();
        assertNotNull(pdld21);
        assertEquals(1, pdld21.length);
        assertEquals("*", pdld21[0].getLimitGroupId());

        PackDiscountLimitData[] pdld22 = pdd2[1].getPackDiscountLimitData();
        assertNull(pdld22);
    }

    @Test
    public void testGetDiscountLimitList() throws RuntimeException,
            InterruptedException, JsonGenerationException,
            JsonMappingException, IOException, InstantiationException,
            IllegalAccessException {
        Discount d1 = new Discount("one", "nameOne", true, true, 60, 2, 3);
        Discount d2 = new Discount("two", "nameTwo", true, true, 70, 2, 2);
        Discount d3 = new Discount("three", "nameThree", true, true, 80, 2, 5);
        List<Discount> dicountList = Arrays.asList(new Discount[] {d1, d2, d3});

        // Лимиты из БД
        List<DiscountLimit> dicountLimitList = new ArrayList<DiscountLimit>();

        Map<String, Object> item1 = new HashMap<String, Object>();
        item1.put(DiscountLimit.FLD_DISCOUNT_ID, "one");
        item1.put(DiscountLimit.FLD_LIMIT_GROUP_ID, "group1");
        item1.put(DiscountLimit.FLD_CURRENT_VERSION, "1");
        item1.put(DiscountLimit.FLD_LIMIT_PERIOD, "month");
        item1.put(DiscountLimit.FLD_MAX_PACK_COUNT, "5");
        item1.put(DiscountLimit.FLD_MAX_AVERAGE_PACK_COUNT, "1000");
        item1.put(DiscountLimit.FLD_MAX_DISCOUNT_SUM, "9999");
        item1.put(DiscountLimit.FLD_MAX_SUM_BEFORE_DISCOUNT, "0");
        dicountLimitList.add(new DiscountLimit(item1));

        Map<String, Object> item2 = new HashMap<String, Object>();
        item2.put(DiscountLimit.FLD_DISCOUNT_ID, "one");
        item2.put(DiscountLimit.FLD_LIMIT_GROUP_ID, "group1");
        item2.put(DiscountLimit.FLD_CURRENT_VERSION, "2");
        item2.put(DiscountLimit.FLD_LIMIT_PERIOD, "month");
        item2.put(DiscountLimit.FLD_MAX_PACK_COUNT, "6");
        item2.put(DiscountLimit.FLD_MAX_AVERAGE_PACK_COUNT, "1300");
        item2.put(DiscountLimit.FLD_MAX_DISCOUNT_SUM, "9999");
        item2.put(DiscountLimit.FLD_MAX_SUM_BEFORE_DISCOUNT, "0");
        dicountLimitList.add(new DiscountLimit(item2));
        /*
         * Map<String, Object> item3 = new HashMap<String, Object>();
         * item3.put(DiscountLimit.FLD_DISCOUNT_ID, "two");
         * item3.put(DiscountLimit.FLD_LIMIT_GROUP_ID, "group2");
         * item3.put(DiscountLimit.FLD_CURRENT_VERSION, "2");
         * item3.put(DiscountLimit.FLD_LIMIT_PERIOD, "month");
         * item3.put(DiscountLimit.FLD_MAX_PACK_COUNT, "10");
         * item3.put(DiscountLimit.FLD_MAX_AVERAGE_PACK_COUNT, "1500");
         * item3.put(DiscountLimit.FLD_MAX_DISCOUNT_SUM, "9999");
         * item3.put(DiscountLimit.FLD_MAX_SUM_BEFORE_DISCOUNT, "0");
         * 
         * List<Map<String, Object>> dl1List = new ArrayList<Map<String,
         * Object>>(); dl1List.add(item1); dl1List.add(item2); List<Map<String,
         * Object>> dl2List = new ArrayList<Map<String, Object>>();
         * dl2List.add(item3);
         */
        when(
                dynamodbService.queryObjects(eq(DiscountLimit.class),
                        any(String.class))).thenReturn(dicountLimitList);

        // Лимит, соответствующий дисконту d3 "уже был" в ProcessingTransaction
        DiscountLimit dl4 = new DiscountLimit("three", "group3", 2, "Month", 8,
                1500l, new BigDecimal(9999), BigDecimal.ZERO);
        DiscountLimit dl5 = new DiscountLimit("three", "group4", 2, "Month", 7,
                1500l, new BigDecimal(9999), BigDecimal.ZERO);
        List<DiscountLimit> alreadyLoadedDiscountLimit = new ArrayList<DiscountLimit>();
        alreadyLoadedDiscountLimit.add(dl4);
        alreadyLoadedDiscountLimit.add(dl5);

        List<DiscountLimit> discountLimitList = service
                .getDiscountLimitList(dicountList, alreadyLoadedDiscountLimit);

        verify(dynamodbService, times(2)).queryObjects(eq(DiscountLimit.class),
                any(String.class));

        // вернули список из 4 лимитов (2 уже были, 2 из БД)
        assertEquals(4, discountLimitList.size());
    }

    private String cardNumber = "777";

    private DiscountLimit dl1 = new DiscountLimit("one", "group1", 1, "Month", 5,
            1000l, new BigDecimal(9999), BigDecimal.ZERO);
    private DiscountLimit dl2 = new DiscountLimit("one", "group2", 1, "Month", 6,
            1300l, new BigDecimal(9999), BigDecimal.ZERO);
    private DiscountLimit dl3 = new DiscountLimit("two", "group3", 1, "Month", 7,
            1500l, new BigDecimal(9999), BigDecimal.ZERO);
    private DiscountLimit dl4 = new DiscountLimit("three", "group3", 1, "halfyear",
            8, 1500l, new BigDecimal(9999), BigDecimal.ZERO);
    private List<DiscountLimit> discountLimitList = Arrays
            .asList(new DiscountLimit[] {dl1, dl2, dl3, dl4});

    @Test
    public void testGetUsedLimits() throws JsonGenerationException,
            JsonMappingException, IOException, BatchGetException {

        DiscountLimitUsed dlu = newDiscountLimitUsed("Month",
                cardNumber, zero_UUID, 0, "group3", "two", 1, 100l,
                new BigDecimal(50), BigDecimal.ZERO);
        DiscountLimitUsed dluOld = newDiscountLimitUsed("Month",
                cardNumber, zero_UUID, 0, "group3", "four", 1, 100l,
                new BigDecimal(50), BigDecimal.ZERO);
        List<DiscountLimitUsed> discountLimitUsedListFromDb = Arrays
                .asList(new DiscountLimitUsed[] {dlu});

        when(dynamodbService.getObjectsAllOrDie(eq(DiscountLimitUsed.class),
                any(String[].class), any(String[].class)))
                .thenReturn(discountLimitUsedListFromDb);
        // Обычная работа.
        List<DiscountLimitUsed> dlurList = service.getUsedLimits(cardNumber,
                DateTime.now(), discountLimitList, alreadyLoadedDiscountLimitUsedRecordList);

        for (DiscountLimitUsed dlur : dlurList) {
            String transactionString = DynamodbEntity.toJson(dlur);
            System.out.println(transactionString);
        }

        // assertEquals(4, dlurList.size());
        assertEquals(1, dlurList.size());
    }

    // Если пустой список лимитов не должен вернуть список ранее загруженных
    // использований лимитов.
    @Test
    public void testGetUsedLimitsWithNullDiscountLimits() throws BatchGetException, RuntimeException, IOException {
        List<DiscountLimit> discountLimitListdiscountLimitEmptyList = new ArrayList<DiscountLimit>();
        BatchOperationResult<DiscountLimitUsed> result2 = new BatchOperationResult<DiscountLimitUsed>(
                null, null);
        when(dynamodbService.getObjects(eq(DiscountLimitUsed.class),
                any(String[].class), any(String[].class)))
                .thenReturn(result2);
        List<DiscountLimitUsed> dlurListEmpty = service.getUsedLimits(cardNumber,
                DateTime.now(), discountLimitListdiscountLimitEmptyList, alreadyLoadedDiscountLimitUsedRecordList);

        assertEquals(0, dlurListEmpty.size());
        // Если null как список лимитов должен вернуть null, а не список ранее
        // загруженных.
        List<DiscountLimitUsed> dlurListNull = service.getUsedLimits(cardNumber,
                DateTime.now(), null, alreadyLoadedDiscountLimitUsedRecordList);

        assertNull(dlurListNull);

    }

    @Test
    public void testGetDiscountLimitUsedListWithNull()
            throws JsonGenerationException, JsonMappingException, IOException, BatchGetException {

        String cardNumber = "777";

        List<DiscountLimitUsed> dlurList = service.getUsedLimits(cardNumber, DateTime.now(), null, null);

        verify(dynamodbService, never()).getObjectsAllOrDie(
                eq(DiscountLimitUsed.class),
                any(String[].class), any(String[].class));

        assertNull(dlurList);
    }

    private List<DiscountLimitUsed> getChildDiscountLimitUsed() {
        List<DiscountLimitUsed> dataList = new ArrayList<DiscountLimitUsed>();
        DiscountLimitUsed dataValue = newDiscountLimitUsed(
                "period", "cardnum", new UUID(1, 1), new Integer(1),
                "limitGroupId", "discountId", new Integer(1), new Long(2),
                new BigDecimal(100), new BigDecimal(1000));
        dataValue.setType(DiscountLimitUsed.TYPE_DETAIL);
        dataList.add(dataValue);

        dataValue = newDiscountLimitUsed(
                "period", "cardnum", new UUID(1, 1), new Integer(1),
                "limitGroupId", "discountId", new Integer(1), new Long(2),
                new BigDecimal(100), new BigDecimal(1000));
        dataValue.setType(DiscountLimitUsed.TYPE_DETAIL);
        dataList.add(dataValue);

        dataValue = newDiscountLimitUsed(
                "period", "cardnum", new UUID(1, 1), new Integer(1),
                "limitGroupId", "discountId", new Integer(1), new Long(2),
                new BigDecimal(100), new BigDecimal(1000));
        dataValue.setType(DiscountLimitUsed.TYPE_DETAIL);
        dataList.add(dataValue);

        String[] allLimitsId = DiscountLimit.getAllPurposeLimitId().toArray(
                new String[DiscountLimit.getAllPurposeLimitId().size()]);

        dataValue = newDiscountLimitUsed(
                "period", "cardnum", new UUID(1, 1), new Integer(1),
                allLimitsId[0],
                "discountId", new Integer(1), new Long(2),
                new BigDecimal(100), new BigDecimal(1000));
        dataValue.setType(DiscountLimitUsed.TYPE_DETAIL);
        dataList.add(dataValue);

        dataValue = newDiscountLimitUsed(
                "period", "cardnum", new UUID(1, 1), new Integer(1),
                allLimitsId[1],
                "discountId", new Integer(1), new Long(2),
                new BigDecimal(100), new BigDecimal(1000));
        dataValue.setType(DiscountLimitUsed.TYPE_DETAIL);
        dataList.add(dataValue);

        dataValue = newDiscountLimitUsed(
                "period", "cardnum", new UUID(1, 1), new Integer(1),
                allLimitsId[2],
                "discountId", new Integer(1), new Long(2),
                new BigDecimal(100), new BigDecimal(1000));
        dataValue.setType(DiscountLimitUsed.TYPE_DETAIL);
        dataList.add(dataValue);

        return dataList;
    }

    private DiscountLimitUsed getParentDiscountLimitUsed() {
        DiscountLimitUsed parent = newDiscountLimitUsed(
                "period", "cardnum", new UUID(1, 1), new Integer(1),
                "limitGroupId", "discountId", new Integer(1), new Long(2),
                new BigDecimal(100), new BigDecimal(1000));
        parent.setType(DiscountLimitUsed.TYPE_TOTAL);
        return parent;
    }

    @Test
    public void testUnionPreviousLimitUsedTotal() {
        // TODO

        DiscountLimitUsed dlu1 = newDiscountLimitUsed(currentMonthPeriod, cardNumber, zero_UUID, 0, "group3", "two", 1,
                90l,
                new BigDecimal(50), BigDecimal.ZERO);
        DiscountLimitUsed dlu2 = newDiscountLimitUsed(currentMonthPeriod, cardNumber, zero_UUID, 0, "group3", "four",
                1, 100l,
                new BigDecimal(85), BigDecimal.ZERO);
        List<DiscountLimitUsed> discountLimitUsedList = Arrays.asList(new DiscountLimitUsed[] {dlu1, dlu2});

        List<DiscountLimitUsedTotal> completeDlutList = service.unionPreviousLimitUsedTotal(cardNumber, DateTime.now(),
                discountLimitList, discountLimitUsedList, alreadyLoadedDiscountLimitUsedRecordList);

        assertNotNull(completeDlutList);
        assertEquals(5, completeDlutList.size());

        // поищем повторы
        int count = 0;
        for (DiscountLimitUsedTotal dlut : completeDlutList) {
            int z = 0;
            for (DiscountLimitUsedTotal d : completeDlutList) {
                if (d.getPeriod().equals(dlut.getPeriod())
                        && d.getDiscountId().equals(dlut.getDiscountId())
                        && d.getLimitGroupId().equals(dlut.getLimitGroupId()))
                    z++;
            }
            if (z > 1)
                count++;
        }
        assertEquals(0, count);

    }

    @Test
    public void testAppendTotal() {
        // limitGroupId
        DiscountLimitUsed parent = this.getParentDiscountLimitUsed();
        service.appendTotal(parent, this.getChildDiscountLimitUsed());
        assertEquals(new BigDecimal(400), parent.getDiscountSum());
        assertEquals(new BigDecimal(4000), parent.getSumBeforeDiscount());
        assertEquals(4, parent.getPackCount());
        assertEquals(8L, parent.getAveragePackCount());

        // ALL_PURPOSE_LIMIT_GROUP_ID
        parent = this.getParentDiscountLimitUsed();
        service.appendTotal(parent, this.getChildDiscountLimitUsed());
        assertEquals(new BigDecimal(400), parent.getDiscountSum());
        assertEquals(new BigDecimal(4000), parent.getSumBeforeDiscount());
        assertEquals(4, parent.getPackCount());
        assertEquals(8L, parent.getAveragePackCount());
    }

    @Test
    public void testClearTotal() {
        DiscountLimitUsed parent = getParentDiscountLimitUsed();
        service.clearTotal(parent);
        assertEquals(new BigDecimal(0), parent.getDiscountSum());
        assertEquals(new BigDecimal(0), parent.getSumBeforeDiscount());
        assertEquals(0, parent.getPackCount());
        assertEquals(0L, parent.getAveragePackCount());
    }    

    private String getCurrentPeriod(String limitPeriod,
            DateTime transactionDateTime) {
        String currentPeriod;
        if (limitPeriod.equalsIgnoreCase(LimitPeriod.DAY.getPeriod())) {
            currentPeriod = LimitPeriod.DAY
                    .getCurrentPeriod(transactionDateTime);
        } else if (limitPeriod.equalsIgnoreCase(LimitPeriod.WEEK.getPeriod())) {
            currentPeriod = LimitPeriod.WEEK
                    .getCurrentPeriod(transactionDateTime);
        } else if (limitPeriod.equalsIgnoreCase(LimitPeriod.MONTH.getPeriod())) {
            currentPeriod = LimitPeriod.MONTH
                    .getCurrentPeriod(transactionDateTime);
        } else if (limitPeriod.equalsIgnoreCase(LimitPeriod.QUARTER.getPeriod())) {
            currentPeriod = LimitPeriod.QUARTER
                    .getCurrentPeriod(transactionDateTime);
        } else if (limitPeriod.equalsIgnoreCase(LimitPeriod.HALFYEAR.getPeriod())) {
            currentPeriod = LimitPeriod.HALFYEAR
                    .getCurrentPeriod(transactionDateTime);
        } else if (limitPeriod.equalsIgnoreCase(LimitPeriod.YEAR.getPeriod())) {
            currentPeriod = LimitPeriod.YEAR
                    .getCurrentPeriod(transactionDateTime);
        } else {
            throw new IllegalArgumentException("Wrong period keyword");
        }
        return currentPeriod;
    }

    @Test
    public void testGetRemainingLimits()
            throws JsonParseException, JsonMappingException, IOException,
            RuntimeException, InterruptedException, InstantiationException,
            IllegalAccessException, OlekstraException {

        String halfYearPeriod = getCurrentPeriod(LimitPeriod.HALFYEAR.getPeriod(), DateTime.now());
        String quarterPeriod = getCurrentPeriod(LimitPeriod.QUARTER.getPeriod(), DateTime.now());

        Card card = new Card();
        card.setNumber("777");
        card.setDiscountPlan("discountPlanId");

        DiscountPlan plan = new DiscountPlan();
        plan.setId("discountPlanId");
        List<String> discountIds = new ArrayList<String>();
        discountIds.add("one");
        plan.setDiscounts(discountIds);

        when(planService.getDiscountPlan("discountPlanId")).thenReturn(plan);

        Discount d1 = new Discount("one", "nameOne", true, true, 60, 2, 3);
        List<Discount> dicountList = Arrays.asList(new Discount[] {d1});

        // Лимиты из БД
        List<DiscountLimit> listDL = new ArrayList<DiscountLimit>();
        Map<String, Object> item1 = new HashMap<String, Object>();
        item1.put(DiscountLimit.FLD_DISCOUNT_ID, "one");
        item1.put(DiscountLimit.FLD_LIMIT_GROUP_ID, "group1");
        item1.put(DiscountLimit.FLD_CURRENT_VERSION, "1");
        item1.put(DiscountLimit.FLD_LIMIT_PERIOD, "halfyear");
        item1.put(DiscountLimit.FLD_MAX_PACK_COUNT, "5");
        item1.put(DiscountLimit.FLD_MAX_AVERAGE_PACK_COUNT, "1000");
        item1.put(DiscountLimit.FLD_MAX_DISCOUNT_SUM, "9999");
        item1.put(DiscountLimit.FLD_MAX_SUM_BEFORE_DISCOUNT, "20");
        listDL.add(new DiscountLimit(item1));

        Map<String, Object> item2 = new HashMap<String, Object>();
        item2.put(DiscountLimit.FLD_DISCOUNT_ID, "one");
        item2.put(DiscountLimit.FLD_LIMIT_GROUP_ID, "group2");
        item2.put(DiscountLimit.FLD_CURRENT_VERSION, "1");
        item2.put(DiscountLimit.FLD_LIMIT_PERIOD, "halfyear");
        item2.put(DiscountLimit.FLD_MAX_PACK_COUNT, "12");
        item2.put(DiscountLimit.FLD_MAX_AVERAGE_PACK_COUNT, "13000");
        item2.put(DiscountLimit.FLD_MAX_DISCOUNT_SUM, "99999");
        item2.put(DiscountLimit.FLD_MAX_SUM_BEFORE_DISCOUNT, "20");
        listDL.add(new DiscountLimit(item2));

        Map<String, Object> item3 = new HashMap<String, Object>();
        item3.put(DiscountLimit.FLD_DISCOUNT_ID, "one");
        item3.put(DiscountLimit.FLD_LIMIT_GROUP_ID, "group3");
        item3.put(DiscountLimit.FLD_CURRENT_VERSION, "1");
        item3.put(DiscountLimit.FLD_LIMIT_PERIOD, "quarter");
        item3.put(DiscountLimit.FLD_MAX_PACK_COUNT, "10");
        item3.put(DiscountLimit.FLD_MAX_AVERAGE_PACK_COUNT, "1500");
        item3.put(DiscountLimit.FLD_MAX_DISCOUNT_SUM, "9999");
        item3.put(DiscountLimit.FLD_MAX_SUM_BEFORE_DISCOUNT, "100");
        listDL.add(new DiscountLimit(item3));

        when(dynamodbService.queryObjects(eq(DiscountLimit.class), any(String.class))).thenReturn(listDL);

        // Использованные лимиты

        DiscountLimitUsed limitUsed1 = new DiscountLimitUsed(halfYearPeriod + "#777#0",
                "group1#one#00000000-0000-0000-0000-000000000000#0",
                4, 10l, new BigDecimal(10), new BigDecimal(15));

        DiscountLimitUsed limitUsed2 = new DiscountLimitUsed(halfYearPeriod + "#777#0",
                "group2#one#00000000-0000-0000-0000-000000000000#0",
                1, 2l, new BigDecimal(8), new BigDecimal(7));

        DiscountLimitUsed limitUsed3 = new DiscountLimitUsed(quarterPeriod + "#777#0",
                "group3#one#00000000-0000-0000-0000-000000000000#0",
                10, 10l, new BigDecimal(10), new BigDecimal(15));

        List<DiscountLimitUsed> limitsUsed = new ArrayList<DiscountLimitUsed>();
        limitsUsed.add(limitUsed1);
        limitsUsed.add(limitUsed2);
        limitsUsed.add(limitUsed3);

        when(dynamodbService.getObjectsAllOrDie(
                eq(DiscountLimitUsed.class),
                aryEq(new String[] {halfYearPeriod + "#777#0",
                        halfYearPeriod + "#777#0",
                        quarterPeriod + "#777#0"}),
                aryEq(new String[] {
                        "group1#one#00000000-0000-0000-0000-000000000000#0",
                        "group2#one#00000000-0000-0000-0000-000000000000#0",
                        "group3#one#00000000-0000-0000-0000-000000000000#0"})))
                .thenReturn(limitsUsed);
        List<Discount> discounts = new ArrayList<Discount>();
        discounts.add(new Discount("one", "one", true, true, 1, 1, 10));
        when(dao.getAllDiscountsUsingCache()).thenReturn(discounts);

        List<DiscountLimitRemaining> result = service.getRemainingLimits(card);

        assertEquals(result.get(0).getRemainingPackCount(), new Long(1));
        assertEquals(result.get(0).getRemainingAveragePackCount(), new Long(990));
        assertEquals(result.get(0).getRemainingDiscountSum(), new BigDecimal(9989l));
        assertEquals(result.get(0).getRemainingSumBeforeDiscount(), new BigDecimal(5l));

        assertEquals(result.get(1).getRemainingPackCount(), new Long(11));
        assertEquals(result.get(1).getRemainingAveragePackCount(), new Long(12998));
        assertEquals(result.get(1).getRemainingDiscountSum(), new BigDecimal(99991l));
        assertEquals(result.get(1).getRemainingSumBeforeDiscount(), new BigDecimal(13l));

        assertEquals(result.get(2).getRemainingPackCount(), new Long(0));
        assertEquals(result.get(2).getRemainingAveragePackCount(), new Long(1490));
        assertEquals(result.get(2).getRemainingDiscountSum(), new BigDecimal(9989l));
        assertEquals(result.get(2).getRemainingSumBeforeDiscount(), new BigDecimal(85l));
    }

    @Test
    public void testCalculateRemainingLimits()
            throws JsonParseException, JsonMappingException, IOException,
            RuntimeException, InterruptedException, InstantiationException,
            IllegalAccessException, OlekstraException {

        String halfYearPeriod = getCurrentPeriod(LimitPeriod.HALFYEAR.getPeriod(), DateTime.now());
        String quarterPeriod = getCurrentPeriod(LimitPeriod.QUARTER.getPeriod(), DateTime.now());

        Card card = new Card();
        card.setNumber("777");
        card.setDiscountPlan("discountPlanId");

        List<String> discountIds = new ArrayList<String>();
        discountIds.add("one");

        // Лимиты из БД
        List<DiscountLimit> listDL = new ArrayList<DiscountLimit>();
        Map<String, Object> item1 = new HashMap<String, Object>();
        item1.put(DiscountLimit.FLD_DISCOUNT_ID, "one");
        item1.put(DiscountLimit.FLD_LIMIT_GROUP_ID, "group1");
        item1.put(DiscountLimit.FLD_CURRENT_VERSION, "1");
        item1.put(DiscountLimit.FLD_LIMIT_PERIOD, "halfyear");
        item1.put(DiscountLimit.FLD_MAX_PACK_COUNT, "5");
        item1.put(DiscountLimit.FLD_MAX_AVERAGE_PACK_COUNT, "1000");
        item1.put(DiscountLimit.FLD_MAX_DISCOUNT_SUM, "9999");
        item1.put(DiscountLimit.FLD_MAX_SUM_BEFORE_DISCOUNT, "20");
        listDL.add(new DiscountLimit(item1));

        Map<String, Object> item2 = new HashMap<String, Object>();
        item2.put(DiscountLimit.FLD_DISCOUNT_ID, "one");
        item2.put(DiscountLimit.FLD_LIMIT_GROUP_ID, "group2");
        item2.put(DiscountLimit.FLD_CURRENT_VERSION, "1");
        item2.put(DiscountLimit.FLD_LIMIT_PERIOD, "halfyear");
        item2.put(DiscountLimit.FLD_MAX_PACK_COUNT, "12");
        item2.put(DiscountLimit.FLD_MAX_AVERAGE_PACK_COUNT, "13000");
        item2.put(DiscountLimit.FLD_MAX_DISCOUNT_SUM, "99999");
        item2.put(DiscountLimit.FLD_MAX_SUM_BEFORE_DISCOUNT, "20");
        listDL.add(new DiscountLimit(item2));

        Map<String, Object> item3 = new HashMap<String, Object>();
        item3.put(DiscountLimit.FLD_DISCOUNT_ID, "one");
        item3.put(DiscountLimit.FLD_LIMIT_GROUP_ID, "group3");
        item3.put(DiscountLimit.FLD_CURRENT_VERSION, "1");
        item3.put(DiscountLimit.FLD_LIMIT_PERIOD, "quarter");
        item3.put(DiscountLimit.FLD_MAX_PACK_COUNT, "10");
        item3.put(DiscountLimit.FLD_MAX_AVERAGE_PACK_COUNT, "1500");
        item3.put(DiscountLimit.FLD_MAX_DISCOUNT_SUM, "9999");
        item3.put(DiscountLimit.FLD_MAX_SUM_BEFORE_DISCOUNT, "100");
        listDL.add(new DiscountLimit(item3));

        // Использованные лимиты

        DiscountLimitUsed limitUsed1 = new DiscountLimitUsed(halfYearPeriod + "#777#0",
                "group1#one#00000000-0000-0000-0000-000000000000#0",
                4, 10l, new BigDecimal(10), new BigDecimal(15));

        DiscountLimitUsed limitUsed2 = new DiscountLimitUsed(halfYearPeriod + "#777#0",
                "group2#one#00000000-0000-0000-0000-000000000000#0",
                1, 2l, new BigDecimal(8), new BigDecimal(7));

        DiscountLimitUsed limitUsed3 = new DiscountLimitUsed(quarterPeriod + "#777#0",
                "group3#one#00000000-0000-0000-0000-000000000000#0",
                10, 10l, new BigDecimal(10), new BigDecimal(15));

        List<DiscountLimitUsed> limitsUsed = new ArrayList<DiscountLimitUsed>();
        limitsUsed.add(limitUsed1);
        limitsUsed.add(limitUsed2);
        limitsUsed.add(limitUsed3);

        List<Discount> discounts = new ArrayList<Discount>();
        discounts.add(new Discount("one", "one", true, true, 1, 1, 10));
        List<DiscountLimitRemaining> result = service.calculateRemainingLimits(listDL,
                DozerHelper.map(mapper, limitsUsed, DiscountLimitUsedTotal.class),
                card.getNumber());

        assertEquals(result.get(0).getRemainingPackCount(), new Long(1));
        assertEquals(result.get(0).getRemainingAveragePackCount(), new Long(990));
        assertEquals(result.get(0).getRemainingDiscountSum(), new BigDecimal(9989l));
        assertEquals(result.get(0).getRemainingSumBeforeDiscount(), new BigDecimal(5l));

        assertEquals(result.get(1).getRemainingPackCount(), new Long(11));
        assertEquals(result.get(1).getRemainingAveragePackCount(), new Long(12998));
        assertEquals(result.get(1).getRemainingDiscountSum(), new BigDecimal(99991l));
        assertEquals(result.get(1).getRemainingSumBeforeDiscount(), new BigDecimal(13l));

        assertEquals(result.get(2).getRemainingPackCount(), new Long(0));
        assertEquals(result.get(2).getRemainingAveragePackCount(), new Long(1490));
        assertEquals(result.get(2).getRemainingDiscountSum(), new BigDecimal(9989l));
        assertEquals(result.get(2).getRemainingSumBeforeDiscount(), new BigDecimal(85l));
    }

    @Test
    public void testGetDiscountReplaces()
            throws JsonGenerationException, JsonMappingException, IOException, BatchGetException,
            InstantiationException, IllegalAccessException {
        List<VoucherResponseRow> rows = new ArrayList<VoucherResponseRow>();
        VoucherResponseRow row0, row1, row2, row0_;

        row0 = new VoucherResponseRow("rowName00", "000", new BigDecimal(10l), 10, 1);
        row0.setClientPackId("clientPackId000");
        row0.setClientPackName("clientPackName000");
        row0.setId("ekt000");
        row0.setDiscountPercent(ProcessingTransaction.FORMATTED_ZERO);
        row0.setDiscountSum(ProcessingTransaction.FORMATTED_ZERO);
        row0.setPackDiscount(null);
        row0.setSumToPay(ProcessingTransaction.FORMATTED_ZERO);
        row0.setCardLimitUsedSum(ProcessingTransaction.FORMATTED_ZERO);
        row0.setRefundSum(ProcessingTransaction.FORMATTED_ZERO);
        rows.add(row0);

        row1 = new VoucherResponseRow("rowName01", "001", new BigDecimal(10l), 10, 2);
        row1.setClientPackId("clientPackId001");
        row1.setClientPackName("clientPackName001");
        row1.setId("ekt001");
        row1.setDiscountPercent(ProcessingTransaction.FORMATTED_ZERO);
        row1.setDiscountSum(ProcessingTransaction.FORMATTED_ZERO);
        row1.setPackDiscount(null);
        row1.setSumToPay(ProcessingTransaction.FORMATTED_ZERO);
        row1.setCardLimitUsedSum(ProcessingTransaction.FORMATTED_ZERO);
        row1.setRefundSum(ProcessingTransaction.FORMATTED_ZERO);
        rows.add(row1);

        row2 = new VoucherResponseRow("rowName02", "002", new BigDecimal(20l), 20, 3);
        row2.setClientPackId("clientPackId002");
        row2.setClientPackName("clientPackName002");
        row2.setId("ekt002");
        row2.setDiscountPercent(ProcessingTransaction.FORMATTED_ZERO);
        row2.setDiscountSum(ProcessingTransaction.FORMATTED_ZERO);
        row2.setPackDiscount(null);
        row2.setSumToPay(ProcessingTransaction.FORMATTED_ZERO);
        row2.setCardLimitUsedSum(ProcessingTransaction.FORMATTED_ZERO);
        row2.setRefundSum(ProcessingTransaction.FORMATTED_ZERO);
        rows.add(row2);

        row0_ = new VoucherResponseRow("rowName00", "000", new BigDecimal(10l), 10, 4);
        row0_.setClientPackId("clientPackId000");
        row0_.setClientPackName("clientPackName000");
        row0_.setId("ekt000");
        row0_.setDiscountPercent(ProcessingTransaction.FORMATTED_ZERO);
        row0_.setDiscountSum(ProcessingTransaction.FORMATTED_ZERO);
        row0_.setPackDiscount(null);
        row0_.setSumToPay(ProcessingTransaction.FORMATTED_ZERO);
        row0_.setCardLimitUsedSum(ProcessingTransaction.FORMATTED_ZERO);
        row0_.setRefundSum(ProcessingTransaction.FORMATTED_ZERO);
        rows.add(row0_);

        ProcessingTransaction transaction = new ProcessingTransaction(DateTime.now());
        transaction.setRows(rows);

        List<String> discounts = new ArrayList<String>();
        discounts.add("discount000");
        discounts.add("discount001");
        transaction.setDiscounts(discounts);

        Pack pack0 = new Pack("ekt000", "ekt000", "000", "000", "pack000",
                "manufacturer000", "synonym000", "tradeName000", "inn000");
        Pack pack1 = new Pack("ekt001", "ekt001", "001", "001", "pack001",
                "manufacturer001", "synonym001", "tradeName001", "inn001");
        Pack pack2 = new Pack("ekt002", "ekt002", "002", "002", "pack002",
                "manufacturer002", "synonym002", "tradeName002", "inn002");
        when(dynamodbService.getObject(settingsService.getMasterTablePackEntity(), "ekt000", "ekt000"))
                .thenReturn(pack0);
        when(dynamodbService.getObject(settingsService.getMasterTablePackEntity(), "ekt001", "ekt001"))
                .thenReturn(pack1);
        when(dynamodbService.getObject(settingsService.getMasterTablePackEntity(), "ekt002", "ekt002"))
                .thenReturn(pack2);

        List<DiscountReplace> replaces01 = new ArrayList<DiscountReplace>();
        DiscountReplace replace01 = new DiscountReplace("discount000", "synonym000", "ekt010", "0");
        replaces01.add(replace01);
        List<DiscountReplace> replaces02 = new ArrayList<DiscountReplace>();
        DiscountReplace replace02 = new DiscountReplace("discount000", "synonym001", "ekt011", "0");
        replaces02.add(replace02);

        List<DiscountReplace> replaces1 = new ArrayList<DiscountReplace>();
        DiscountReplace replace10 = new DiscountReplace("discount000", "synonym001", "ekt020", "0");
        replaces1.add(replace10);

        String key01 = "discount000#synonym000";
        String key02 = "discount001#synonym000";

        String key11 = "discount000#synonym001";
        String key12 = "discount001#synonym001";

        when(dynamodbService.queryObjects(eq(DiscountReplace.class), eq(key01))).thenReturn(replaces01);
        when(dynamodbService.queryObjects(eq(DiscountReplace.class), eq(key02))).thenReturn(replaces02);
        when(dynamodbService.queryObjects(eq(DiscountReplace.class), eq(key11))).thenReturn(replaces1);
        when(dynamodbService.queryObjects(eq(DiscountReplace.class), eq(key12))).thenReturn(replaces1);

        service.getDiscountReplaces(transaction);

        assertEquals(2, transaction.getRows().get(0).getReplacePacks().length);
        assertEquals(1, transaction.getRows().get(1).getReplacePacks().length);
        assertEquals(0, transaction.getRows().get(2).getReplacePacks().length);
        assertEquals(2, transaction.getRows().get(3).getReplacePacks().length);

        // для повторяющихся строк не было пинков в БД за синонимами
        verify(dynamodbService, times(1)).queryObjects(eq(DiscountReplace.class), eq(key01));
        verify(dynamodbService, times(1)).queryObjects(eq(DiscountReplace.class), eq(key02));
    }
}
