package ru.olekstra.common.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import ru.olekstra.awsutils.dynamodb.TableManagement;
import ru.olekstra.domain.Card;

@RunWith(MockitoJUnitRunner.class)
public class AboutServiceTest {

    private AboutService service;

    protected ServletContext servletContext;
    protected TableManagement tableService;

    @Before
    public void setUp() throws Exception {

        service = new AboutService();
        service.servletContext = mock(ServletContext.class);
        service.tableService = mock(TableManagement.class);
    }

    @Test
    public void testGetBuildStat() throws IOException {

        String buildKey = "buildKey";
        String buildNumber = "buildNumber";
        String buildTimeStamp = "buildTimeStamp";
        String repositoryRevisionNumber = "repositoryRevisionNumber";

        String buildKeyValue = "testKey";
        String buildNumberValue = "testNumber";
        String buildTimeStampValue = String.valueOf(System.currentTimeMillis());
        String repositoryRevisionNumberValue = "testRevisionNumber";
        long numberOfCardsValue = 1L;

        String data = buildKey + "=" + buildKeyValue + "\n" +
                buildNumber + "=" + buildNumberValue + "\n" +
                buildTimeStamp + "=" + buildTimeStampValue + "\n" +
                repositoryRevisionNumber + "=" + repositoryRevisionNumberValue;

        InputStream is = new ByteArrayInputStream(data.getBytes("UTF-8"));

        when(
                service.servletContext
                        .getResourceAsStream("/META-INF/MANIFEST.MF"))
                .thenReturn(is);

        when(service.tableService
                .getItemCount(Card.TABLE_NAME)).thenReturn(numberOfCardsValue);

        Map<String, String> statistics = service.getBuildStat(new Locale(
                "test locale"));

        assertEquals(buildKeyValue, statistics.get("Build key"));
        assertEquals(buildNumberValue, statistics.get("Build number"));
        assertEquals(buildTimeStampValue,
                statistics.get("Build timestamp"));
        assertEquals(repositoryRevisionNumberValue,
                statistics.get("Repository revision number"));
        assertEquals(numberOfCardsValue,
                Long.parseLong(statistics.get("Number of cards")));

        verify(service.servletContext).getResourceAsStream(any(String.class));
        verify(service.tableService).getItemCount(eq(Card.TABLE_NAME));
    }

    @Test
    public void testBuyildStatNoData() throws IOException {
        String noData = "There is no data";
        InputStream emptyStream = new ByteArrayInputStream("".getBytes("UTF-8"));

        when(
                service.servletContext
                        .getResourceAsStream("/META-INF/MANIFEST.MF"))
                .thenReturn(emptyStream);

        when(service.tableService
                .getItemCount(Card.TABLE_NAME)).thenReturn(0L);

        Map<String, String> statisticsNoData = service.getBuildStat(new Locale(
                "test locale"));

        assertEquals(noData, statisticsNoData.get("Build key"));
        assertEquals(noData, statisticsNoData.get("Build number"));
        assertEquals(noData,
                statisticsNoData.get("Build timestamp"));
        assertEquals(noData,
                statisticsNoData.get("Repository revision number"));
        assertEquals(0L,
                Long.parseLong(statisticsNoData.get("Number of cards")));

        verify(service.servletContext).getResourceAsStream(any(String.class));
        verify(service.tableService).getItemCount(eq(Card.TABLE_NAME));
    }
}
