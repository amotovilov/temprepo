package ru.olekstra.common.service;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ru.olekstra.common.dao.DrugstoreDao;
import ru.olekstra.domain.Drugstore;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration()
public class DateTimeServiceTest {

    @Autowired
    private DrugstoreDao drugstoreDao;
    @Autowired
    private DateTimeService dateService;
    @Autowired
    private AppSettingsService settingService;

    @Before
    public void before() {

    }

    @Test
    public void testCreateDefaultDateTime() {
        DateTime d = dateService.createDefaultDateTime();
        assertNotNull(d);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateDrugstoreDateNull() throws IOException {
        dateService.createDrugstoreDate(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCardDateNull() {
        dateService.createCardDate(null);
    }

    @Test
    public void testCreateDrugstoreDate() throws IOException {
        Drugstore drugstore = new Drugstore("id");
        drugstore.setName("name");
        drugstore.setAuthCode("location");
        drugstore.setTimeZone(AppSettingsService.DEFAULT_TIME_ZONE);
        drugstore.setDisabled( false);
        List<Drugstore> l = new ArrayList<Drugstore>();
        l.add(drugstore);
        when(drugstoreDao.getDrugstoreNames()).thenReturn(l);
        DateTime d = dateService.createDrugstoreDate(drugstore.getId());
        assertNotNull(d);
        verify(drugstoreDao).getDrugstoreNames();
    }

    @Configuration
    static class TestConfig {

        @Bean
        public DateTimeService dateService() {
            DateTimeService dateService = new DateTimeService();

            return dateService;
        }

        @Bean
        public DrugstoreDao drugstoreDao() {
            return mock(DrugstoreDao.class);
        }

        @Bean
        public AppSettingsService settingService()
        {
            AppSettingsService s = mock(AppSettingsService.class);
            when(s.getTimeZoneDefault()).thenReturn(AppSettingsService.DEFAULT_TIME_ZONE);
            return s;
        }

    }
}
