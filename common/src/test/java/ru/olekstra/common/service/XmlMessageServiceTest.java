package ru.olekstra.common.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.OlekstraException;
import ru.olekstra.azure.model.IMessage;
import ru.olekstra.common.exception.ValidationException;
import ru.olekstra.common.helper.XmlMessageHelper;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.Pack;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.dto.DiscountDataCleanMessage;
import ru.olekstra.domain.dto.PackDiscountData;
import ru.olekstra.domain.dto.VoucherResponseCard;
import ru.olekstra.domain.dto.VoucherResponsePackReplace;
import ru.olekstra.domain.dto.VoucherResponseRow;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class XmlMessageServiceTest {

    @Autowired
    private XmlMessageService xmlMessageService;
    @Autowired
    private DynamodbService service;
    @Autowired
    private AppSettingsService settingsService;
    @Autowired
    private XmlMessageHelper helper;

    private ProcessingTransaction transaction;

    @Before
    public void before() throws JsonGenerationException, JsonMappingException, IOException {

        String cardNumber = "1230000000321";
        String cardOwner = "user";
        String phone = "0123456789";
        String discountId = "discountId";
        String discountPlan = "discountPlan";
        String comment = "comment";
        String drugstoreId = "drugstoreId";
        String drugstoreName = "drugstoreName";
        UUID voucherId = UUID.fromString("e58052c3-29d5-4d72-8058-93dbc611e5b3");
        UUID confirmationToken = UUID.fromString("a2dfadd7-c2d7-404e-9bd8-9c5b738c6140");

        VoucherResponseCard card = new VoucherResponseCard(cardNumber,
                cardOwner, phone, true, comment, discountId, discountPlan);
        card.setChecks(new String[] { "q1", "q2" });

        VoucherResponseRow row1 = new VoucherResponseRow("name1", "barcode1",
                new BigDecimal("60.00"), 1, new BigDecimal("30.00"),
                new BigDecimal("30.00"), new BigDecimal("50"), new BigDecimal(
                        "30.00"), new BigDecimal("60.00"), new BigDecimal(
                        "30.00"), "clientPackId1", "clientPackName1", 1);
        row1.setId("ekt1");

        PackDiscountData data00 = new PackDiscountData();
        data00.setDiscountId("discount000");
        data00.setUsedPercent(new BigDecimal("50"));
        data00.setUsedSum(new BigDecimal("60.00"));

        PackDiscountData data01 = new PackDiscountData();
        data01.setDiscountId("discount000");
        data01.setUsedPercent(new BigDecimal("15"));
        data01.setUsedSum(new BigDecimal("20.00"));

        row1.setPackDiscount(new PackDiscountData[] { data00, data01 });

        VoucherResponseRow row2 = new VoucherResponseRow("name2", "barcode2",
                new BigDecimal("60.00"), 2, new BigDecimal("120.00"),
                new BigDecimal("0.00"), new BigDecimal("0"), new BigDecimal(
                        "0.00"), new BigDecimal("120.00"), new BigDecimal("0"),
                "clientPackId2", "clientPackName2", 2);
        row2.setId("ekt2");

        // для простоты - те же дисконты для второй строки
        row2.setPackDiscount(new PackDiscountData[] { data00, data01 });

        VoucherResponsePackReplace replacement = new VoucherResponsePackReplace("id", "replaceBarCode", "tradeName");
        row2.setReplacePacks(new VoucherResponsePackReplace[] { replacement });

        List<VoucherResponseRow> rows = Arrays.asList(row1, row2);

        transaction = new ProcessingTransaction(voucherId,
                drugstoreId, card, drugstoreName, new BigDecimal("100.00"),
                new BigDecimal("30.00"), discountId, 20, "5", new BigDecimal(
                        "150.00"), new String[] { "warning" }, rows, DateTime.now().withZone(DateTimeZone.UTC));
        transaction.setConfirmationToken(confirmationToken);
    }

    @Test(expected = OlekstraException.class)
    public void createTransactionCompleteMessageWithNotYetCompletedState() throws JsonParseException,
            JsonMappingException,
            DOMException, ParserConfigurationException, IOException, OlekstraException, TransformerException {
        IMessage message = xmlMessageService.createTransactionCompleteMessage(transaction);
    }

    @Test
    public void createTransactionCompleteMessage() throws JsonParseException, JsonMappingException, DOMException,
            ParserConfigurationException, IOException, OlekstraException, IllegalArgumentException, SAXException,
            ValidationException, TransformerException {
        transaction.setComplete(true);
        transaction.setCompleteTime(DateTime.now());
        transaction.setRefundSum(new BigDecimal("40.00"));
        transaction.setAuthCode("1234");
        List<VoucherResponseRow> rows = new ArrayList<VoucherResponseRow>();
        VoucherResponseRow row1 = new VoucherResponseRow("row1", "00000", new BigDecimal("10.10"), 1, new BigDecimal("10.20"),
                new BigDecimal("1.20"), BigDecimal.ONE, new BigDecimal("5.20"), new BigDecimal("100.20"),
                new BigDecimal("5.20"), "clientPackId0", "clientPackName0", 1);
        row1.setReversed(true);
        row1.setId("0000000000000");
        row1.setExactNonRefundSum(new BigDecimal("100.1"));
        row1.setExactSumToPay(new BigDecimal("100.2"));
        row1.setExactRefundSum(new BigDecimal("100.3"));
        List<PackDiscountData> packDiscounts1 = new ArrayList<PackDiscountData>();
        packDiscounts1.add(new PackDiscountData("discount1", BigDecimal.ONE, new BigDecimal("10.0"), BigDecimal.ONE, new BigDecimal("10.0"), new BigDecimal("20.1"), null));
        row1.setPackDiscount(packDiscounts1.toArray(new PackDiscountData[packDiscounts1.size()]));

        VoucherResponseRow row2 = new VoucherResponseRow("row1", "00000", new BigDecimal("10.10"), 1, new BigDecimal("10.20"),
                new BigDecimal("1.20"), BigDecimal.ONE, new BigDecimal("5.20"), new BigDecimal("100.20"),
                new BigDecimal("5.20"), "clientPackId0", "clientPackName0", 2);
        row2.setId("0000000000001");
        row2.setExactNonRefundSum(new BigDecimal("100.1"));
        row2.setExactSumToPay(new BigDecimal("100.2"));
        row2.setExactRefundSum(new BigDecimal("100.3"));
        rows.add(row1);
        rows.add(row2);
        List<PackDiscountData> packDiscounts2 = new ArrayList<PackDiscountData>();
        packDiscounts2.add(new PackDiscountData("discount2", BigDecimal.ONE, new BigDecimal("10.0"), BigDecimal.ONE, new BigDecimal("10.0"), new BigDecimal("20.1"), null));
        row2.setPackDiscount(packDiscounts2.toArray(new PackDiscountData[packDiscounts2.size()]));

        transaction.setRows(rows);
        IMessage message = xmlMessageService.createTransactionCompleteMessage(transaction);
        Document doc = helper.parseXmlString(message.getBody());
        NodeList rowsElm = helper.findNodeInList("rows",
                helper.findNodeInList("transaction", doc.getDocumentElement().getChildNodes()).getChildNodes()).getChildNodes();
        int rowCount = 0;
        for (int i = 0; i < rowsElm.getLength(); i++) {
            if ("row".equals(rowsElm.item(i).getLocalName())) {
                rowCount++;
            }
        }
        assertEquals(3, rowCount);
        assertEquals("transaction-complete", doc.getDocumentElement().getLocalName());
    }
    
    @Test
    public void createTransactionCompleteNoDetailsMessage() throws JsonParseException, JsonMappingException, DOMException,
            ParserConfigurationException, IOException, OlekstraException, IllegalArgumentException, SAXException,
            ValidationException, TransformerException {
        transaction.setComplete(true);
        transaction.setCompleteTime(DateTime.now());
        transaction.setRefundSum(new BigDecimal("40.00"));
        transaction.setAuthCode("1234");
        List<VoucherResponseRow> rows = new ArrayList<VoucherResponseRow>();
        VoucherResponseRow row1 = new VoucherResponseRow("row1", "00000", new BigDecimal("10.10"), 1, new BigDecimal("10.20"),
                new BigDecimal("1.20"), BigDecimal.ONE, new BigDecimal("5.20"), new BigDecimal("100.20"),
                new BigDecimal("5.20"), "clientPackId0", "clientPackName0", 1);
        row1.setId("rowId1");
        row1.setExactNonRefundSum(new BigDecimal("100.1"));
        row1.setExactSumToPay(new BigDecimal("100.2"));
        row1.setExactRefundSum(new BigDecimal("100.3"));
        row1.setPackDiscount(new PackDiscountData[] {});

        rows.add(row1);

        transaction.setRows(rows);
        IMessage message = xmlMessageService.createTransactionCompleteMessage(transaction);
        Document doc = helper.parseXmlString(message.getBody());
        NodeList rowsElm = helper.findNodeInList("rows",
                helper.findNodeInList("transaction", doc.getDocumentElement().getChildNodes()).getChildNodes()).getChildNodes();
        int rowCount = 0;
        for (int i = 0; i < rowsElm.getLength(); i++) {
            if ("row".equals(rowsElm.item(i).getLocalName())) {
                rowCount++;
            }
        }
        assertEquals("transaction-complete", doc.getDocumentElement().getLocalName());
    }

    @Test
    public void createCardUpdateMessageTest()
            throws ParserConfigurationException, TransformerException, IllegalArgumentException, IOException,
            SAXException, ValidationException {
        Card card = new Card();
        card.setNumber("1234567899");
        card.setHolderName("Иванов");
        card.setDisabled(false);
        card.setTelephoneNumber("0123456789");
        card.setDiscountPlan("testDiscount");
        card.setCvv("666");
        card.setStartDate(new DateTime());
        card.setEndDate(new DateTime());
        card.setLimitRemain(new BigDecimal("100.11"));
        card.setCardBatches(new ArrayList<String>());
        card.getCardBatches().add("batch1");
        card.getCardBatches().add("batch2");
        IMessage mes = xmlMessageService.createCardUpdateMessage(card);
        Document doc = helper.parseXmlString(mes.getBody());
        assertEquals("card-update", doc.getDocumentElement().getLocalName());
    }

    @Test
    public void createDiscountCleanupMessageTest()
            throws ParserConfigurationException, TransformerException, XPathExpressionException,
            IllegalArgumentException, IOException, SAXException, ValidationException {
        DiscountDataCleanMessage msgSource = new DiscountDataCleanMessage("rangeKey", 1);
        IMessage msg = xmlMessageService.createDiscountDataCleanMessage(msgSource);
        Document doc = helper.parseXmlString(msg.getBody());
        DiscountDataCleanMessage msgDest = xmlMessageService.parseDiscountDataCleanMessage(doc);
        assertEquals(msgSource.getDiscountId(), msgDest.getDiscountId());
    }

    @Configuration
    static class MessageListenerTestConfig {
        @Bean
        public DynamodbService service() {
            return mock(DynamodbService.class);
        }

        @Bean
        public AppSettingsService settingsService() {
            AppSettingsService settingsService = settingsService = mock(AppSettingsService.class);
            when(settingsService.getMasterTablePackEntity()).thenReturn(Pack.class);
            return settingsService;
        }

        @Bean
        public XmlMessageService xmlMessageService() {
            return new XmlMessageService();
        }

        @Bean
        public XmlMessageHelper helper() {
            return new XmlMessageHelper();
        }
    }

}
