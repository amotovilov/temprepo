package ru.olekstra.common.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.dozer.DozerBeanMapper;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ru.olekstra.awsutils.BatchOperationResult;
import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.DynamodbService.LimitObjectsResult;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.common.dao.DrugstoreDao;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.CardChange;
import ru.olekstra.domain.dto.CardChangeDto;
import ru.olekstra.domain.dto.CardExportResponse;

import com.amazonaws.services.dynamodb.model.Key;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class CardExportServiceTest {

    @Autowired
    CardExportService exportService;

    @Autowired
    DynamodbService service;

    @Before
    public void setup() {

    }

    @Test
    public void cardChangeEventTest() throws ItemSizeLimitExceededException, InterruptedException, RuntimeException,
            IOException {
        Set<String> cardNumbers = new HashSet<String>();
        cardNumbers.add("111");
        cardNumbers.add("222");
        List<CardChange> saved = new ArrayList<CardChange>();
        CardChange val = new CardChange();
        val.setNumber("111");
        saved.add(val);
        val = new CardChange();
        val.setNumber("222");
        saved.add(val);

        BatchOperationResult<CardChange> savedBatch = new BatchOperationResult<CardChange>();
        savedBatch.setSuccessList(saved);
        when(service.putObjects((CardChange[]) any())).thenReturn(savedBatch);
        List<CardChangeDto> result = exportService.cardsChangeEvent(cardNumbers);
        verify(service).putObjects((CardChange[]) any());
        assertEquals(result.size(), cardNumbers.size());
        for (CardChangeDto dto : result) {
            assertEquals(true, cardNumbers.contains(dto.getNumber()));
        }
    }

    @Test
    public void exportDataTest() throws InstantiationException, IllegalAccessException, RuntimeException,
            InterruptedException, IOException {
        List<CardChange> readed = new ArrayList<CardChange>();
        CardChange readVal = new CardChange();
        readVal.setNumber("111");
        readed.add(readVal);
        readVal = new CardChange();
        readVal.setNumber("222");
        readed.add(readVal);
        LimitObjectsResult<CardChange> limitObjectsResult = service.new LimitObjectsResult<CardChange>();
        limitObjectsResult.setResult(readed);
        DateTime date = new DateTime();
        String parsedDate = exportService.dateToString(date);

        List<Card> cards = new ArrayList<Card>();
        cards.add(new Card("111", false, "aaa", "111"));
        cards.add(new Card("222", false, "bbb", "222"));
        BatchOperationResult<Card> cardResult = new BatchOperationResult<Card>();
        cardResult.setSuccessList(cards);
        when(service.getObjects(eq(Card.class), (String[]) any())).thenReturn(cardResult);

        when(
                service.getLimitObjects(eq(CardChange.class), eq(AppSettingsService.EXPORT_BATCH_SIZE), eq(parsedDate),
                        (Key) any())).thenReturn(limitObjectsResult);
        CardExportResponse result = exportService.exportData(date, "1234");
        assertEquals(result.getDate(), parsedDate);
        assertEquals(result.getValues().size(), readed.size());
        assertNull(limitObjectsResult.getLastEvaluatedKey());
    }

    @Configuration
    static class TestConfiguration {
        @Bean
        public CardExportService exportService() {
            return new CardExportService();
        }

        @Bean(name = "org.dozer.Mapper")
        public DozerBeanMapper mapper() {
            return new org.dozer.DozerBeanMapper();
        }

        @Bean
        public DynamodbService service() {
            return mock(DynamodbService.class);
        }

        @Bean
        public AppSettingsService settingService() {
            return mock(AppSettingsService.class);
        }

        @Bean
        public DrugstoreDao drugstoreDao() {
            return mock(DrugstoreDao.class);
        }

        @Bean
        public DateTimeService dateService() {
            DateTimeService dateService = mock(DateTimeService.class);
            when(dateService.createDateTimeWithZone(anyString())).thenReturn(DateTime.now());
            when(dateService.createDefaultDateTime()).thenReturn(DateTime.now());
            when(dateService.createUTCDate()).thenReturn(DateTime.now().withZone(DateTimeZone.UTC));

            return dateService;
        }
    }
}
