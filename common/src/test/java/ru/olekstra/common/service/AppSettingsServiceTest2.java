package ru.olekstra.common.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;

import ru.olekstra.common.dao.AppSettingsDao;

public class AppSettingsServiceTest2 {

    @Test
    public void testGetDefaultSettings() {

        AppSettingsDao dao = mock(AppSettingsDao.class);

        when(dao.loadSettings()).thenReturn(null);

        AppSettingsService service = new AppSettingsService(dao);

        assertEquals(AppSettingsService.DEFAULT_BACKOFFICE_REPORT_BUCKET,
                service.getBackofficeReportBucketName());
        assertEquals(AppSettingsService.DEFAULT_MANUFACTURER_REPORT_BUCKET,
                service.getManufacturerReportBucketName());
        assertEquals(AppSettingsService.DEFAULT_MASTER_TABLE, service
                .getPackMasterTableName());
        assertEquals(AppSettingsService.DEFAULT_SLAVE_TABLE, service
                .getPackSlaveTableName());
        assertEquals(AppSettingsService.DEFAULT_SMS_MAX_SEND_THRESHOLD, service
                .getSmsMaxSendThreshold());
        assertEquals(AppSettingsService.DEFAULT_STRING_VALUE, service
                .getOlekstraSupportPhone());
        assertEquals(AppSettingsService.DEFAULT_STRING_VALUE, service
                .getSmsGateSettings());
        assertEquals(AppSettingsService.DEFAULT_STRING_VALUE, service
                .getTicketStatusReopen());
        assertEquals(AppSettingsService.DEFAULT_LIST_VALUE, service
                .getTicketTheme());
        assertEquals(AppSettingsService.DEFAULT_LIST_VALUE, service
                .getTicketStatusProcessing());
        assertEquals(AppSettingsService.DEFAULT_LIST_VALUE, service
                .getTicketStatusNewTemp());
        assertEquals(AppSettingsService.DEFAULT_LIST_VALUE, service
                .getTicketStatusNew());
        assertEquals(AppSettingsService.DEFAULT_LIST_VALUE, service
                .getTicketStatusClosed());
        assertEquals(AppSettingsService.DEFAULT_LIST_VALUE, service
                .getTicketStatusAll());
    }

}
