package ru.olekstra.common.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.refEq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.domain.TransactionTotalInfo;
import ru.olekstra.domain.dto.ProcessingTransactionMessage;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class TransactionTotalInfoServiceTest {

    @Autowired
    private TransactionTotalInfoService service;
    @Autowired
    private DynamodbService dynamodbService;

    @SuppressWarnings("unchecked")
    @Test
    public void updateTransactionTotalInfoTest() {
        final String cardNumber = "00000000000123";
        final String drugstore = "drugstore";
        final String last3 = "123";
        final String last2 = "23";
        final String period = LimitPeriod.DAY.getCurrentPeriod(DateTime.now());
        
        BigDecimal sum3 = new BigDecimal(1000);
        Integer count3 = 10;
        when(dynamodbService.getObject(TransactionTotalInfo.class, period, last3))
                .thenReturn(new TransactionTotalInfo(period, last3, sum3, count3));
        BigDecimal sum2 = new BigDecimal(500);
        Integer count2 = 5;
        when(dynamodbService.getObject(TransactionTotalInfo.class, period, last2))
                .thenReturn(new TransactionTotalInfo(period, last2, sum2, count2));
        when(dynamodbService.addToAttributes(eq(TransactionTotalInfo.TABLE_NAME), eq(period), eq(last2), any(Map.class))).thenReturn(true);
        when(dynamodbService.addToAttributes(eq(TransactionTotalInfo.TABLE_NAME), eq(period), eq(last3), any(Map.class))).thenReturn(true);
        
        ProcessingTransactionMessage mes = new ProcessingTransactionMessage("transactionKey", drugstore);
        mes.setCardNumber(cardNumber);
        mes.setCount(10);
        mes.setDrugstoreId(drugstore);
        mes.setSumToPay(new BigDecimal(100));
        
        service.updateTransactionTotalInfo(mes);

        Map<String, Object> valuesToUpdate = new HashMap<String, Object>();
        valuesToUpdate.put(TransactionTotalInfo.FLD_COUNT, mes.getCount());
        valuesToUpdate.put(TransactionTotalInfo.FLD_TOTAL_SUM_TO_PAY, mes.getSumToPay());
        
        verify(dynamodbService).addToAttributes(eq(TransactionTotalInfo.TABLE_NAME), eq(period),
                eq(last3), eq(valuesToUpdate));


        verify(dynamodbService).addToAttributes(eq(TransactionTotalInfo.TABLE_NAME), eq(period),
                eq(last2), eq(valuesToUpdate));
    }

    @Test
    public void updateTransactionTotalInfoCreateNewTest() {
        final String cardNumber = "00000000000123";
        final String drugstore = "drugstore";
        final String last3 = "123";
        final String last2 = "23";
        final String period = LimitPeriod.DAY.getCurrentPeriod(DateTime.now());
               
        ProcessingTransactionMessage mes = new ProcessingTransactionMessage("transactionKey", drugstore);
        mes.setCardNumber(cardNumber);
        mes.setCount(10);
        mes.setDrugstoreId(drugstore);
        mes.setSumToPay(new BigDecimal(100));
        
        service.updateTransactionTotalInfo(mes);
        
        TransactionTotalInfo info3 = new TransactionTotalInfo(period, last3, mes.getSumToPay(), mes.getCount());
        
        verify(dynamodbService).putObjectOrDie(refEq(info3));

        TransactionTotalInfo info2 = new TransactionTotalInfo(period, last2, mes.getSumToPay(), mes.getCount());

        verify(dynamodbService).putObjectOrDie(refEq(info2));
    }

    @Configuration
    static class Config {
        @Bean
        public DynamodbService dynamodbService() {
            return mock(DynamodbService.class);
        }

        @Bean
        public TransactionTotalInfoService service() {
            return new TransactionTotalInfoService();
        }
        
        @Bean
        public Mapper dozerBeanMapper() {
            return new DozerBeanMapper();
        }
    }

}
