package ru.olekstra.common.service;

import static org.junit.Assert.assertEquals;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;

public class LimitPeriodTest {
    @Test
    public void weekConstructorTest() {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy.MM.dd");
        DateTime dateTime0 = formatter.parseDateTime("2013.01.13");
        assertEquals("2013w02", LimitPeriod.WEEK.getCurrentPeriod(dateTime0));
        DateTime dateTime1 = formatter.parseDateTime("2013.01.14");
        assertEquals("2013w03", LimitPeriod.WEEK.getCurrentPeriod(dateTime1));
    }

    @Test
    public void dayConstructorTest() {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy.MM.dd");
        DateTime dateTime0 = formatter.parseDateTime("2013.01.13");
        assertEquals("20130113", LimitPeriod.DAY.getCurrentPeriod(dateTime0));
    }
}
