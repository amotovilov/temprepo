package ru.olekstra.common.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import ru.olekstra.common.dao.AppSettingsDao;
import ru.olekstra.domain.AppSettings;
import ru.olekstra.domain.Pack;

@RunWith(MockitoJUnitRunner.class)
public class AppSettingsServiceTest {

    private AppSettingsDao dao;
    AppSettings appSettings;
    AppSettingsService service;

    @Before
    public void setUp() throws Exception {

        appSettings = new AppSettings();
        appSettings.setBackofficeReportBucketName("backofficeReportBucketName");
        appSettings.setPackMasterTableName("Pack1");
        appSettings.setSmsMaxSendThreshold(1000);

        dao = mock(AppSettingsDao.class);
        service = new AppSettingsService(dao);
        when(dao.loadSettings()).thenReturn(appSettings);

    }

    @Test
    public void testAppSettingsService() {
        assertNotNull(service);
        assertTrue(AppSettingsService.class.isInstance(service));
    }

    @Test
    public void testGetPackMasterTableName() {
        String value = service.getPackMasterTableName();
        assertEquals(Pack.DEFAULT_MASTER_TABLE_NAME, value);
    }

    @Test
    public void testLoadAppSettings() {
        when(dao.loadSettings()).thenReturn(appSettings);
        service.loadAppSettings();
        verify(dao, times(2)).loadSettings();
    }
}
