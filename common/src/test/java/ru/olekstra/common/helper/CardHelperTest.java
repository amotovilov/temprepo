package ru.olekstra.common.helper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import ru.olekstra.awsutils.BatchOperationResult;
import ru.olekstra.awsutils.ComplexKey;
import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.common.helper.CardHelper;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.CardBatch;
import ru.olekstra.domain.TelCardIndex;

import com.amazonaws.AmazonServiceException;

@RunWith(MockitoJUnitRunner.class)
public class CardHelperTest {

    private CardHelper helper;
    private DynamodbService service;

    @Before
    public void setup() {
        service = mock(DynamodbService.class);
        helper = new CardHelper(service);
    }

    @Test
    public void testGetAllCards() throws IOException {

        String cardNumber = "1234";
        String holder = "holder";
        BigDecimal limit = new BigDecimal(154);
        String telephoneNumber = "123456";

        Card card = new Card(cardNumber, false, holder, telephoneNumber);
        card.setLimitRemain(limit);

        List<Card> cards = new LinkedList<Card>();
        cards.add(card);

        when(service.getAllObjects(Card.class)).thenReturn(
                cards);

        List<Card> actualCards = helper.getAllCards();

        assertEquals(actualCards.size(), cards.size());
        assertEquals(actualCards.get(0).getNumber(), card.getNumber());
        assertEquals(actualCards.get(0).getHolderName(), card.getHolderName());
        assertEquals(actualCards.get(0).getLimitRemain(), card.getLimitRemain());
        assertEquals(actualCards.get(0).getTelephoneNumber(), card
                .getTelephoneNumber());
    }

    @Test
    public void getCardsInBatchMode() throws RuntimeException,
            InterruptedException, IOException {
        Card dummyCard = new Card();

        String[] keysFullBatch = new String[25];
        List<Card> resultFullBatch = new ArrayList<Card>();
        List<String> sourceKeys = new ArrayList<String>();
        for (int i = 0; i < keysFullBatch.length; i++) {
            keysFullBatch[i] = String.valueOf(i);
            resultFullBatch.add(dummyCard);
            sourceKeys.add(String.valueOf(i));
        }

        String[] keysPartlyBatch = new String[4];
        List<Card> resultPartlyBatch = new ArrayList<Card>();
        for (int i = 0; i < keysPartlyBatch.length; i++) {
            keysPartlyBatch[i] = String.valueOf(i);
            resultPartlyBatch.add(dummyCard);
            sourceKeys.add(String.valueOf(i));
        }

        BatchOperationResult<Card> fullOperationResponse = new BatchOperationResult<Card>(
                resultFullBatch, null);
        BatchOperationResult<Card> partOperationResponse = new BatchOperationResult<Card>(
                resultPartlyBatch, null);

        when(service.getObjects(eq(Card.class), (String[]) anyObject()))
                .thenReturn(fullOperationResponse).thenReturn(
                        partOperationResponse);

        List<Card> result = helper.getCardsInBatchMode(sourceKeys)
                .getSuccessList();

        assertEquals(keysFullBatch.length + keysPartlyBatch.length, result
                .size());

        verify(service, times(2)).getObjects(eq(Card.class),
                (String[]) anyObject());
    }

    @Test
    public void updateCardsInBatchModeWithoutUnprocessed()
            throws InterruptedException, AmazonServiceException,
            IllegalArgumentException, ItemSizeLimitExceededException {

        Card[] cardFullBatch = new Card[25];
        Card[] cardPartBatch = new Card[4];
        List<ComplexKey> unprocessedNumbersForFullBatch = new ArrayList<ComplexKey>();
        List<ComplexKey> unprocessedNumbersForPartlyBatch = new ArrayList<ComplexKey>();
        List<Card> sourceCards = new ArrayList<Card>();

        for (int i = 0; i < cardFullBatch.length; i++) {
            Card tmp = new Card(String.valueOf(i));
            cardFullBatch[i] = tmp;
            sourceCards.add(tmp);
        }

        for (int i = 0; i < cardPartBatch.length; i++) {
            Card tmp = new Card(String.valueOf(i));
            cardPartBatch[i] = tmp;
            sourceCards.add(tmp);
        }

        BatchOperationResult<Card> fullOperationResponse = new BatchOperationResult<Card>(
                null, unprocessedNumbersForFullBatch);
        BatchOperationResult<Card> partOperationResponse = new BatchOperationResult<Card>(
                null, unprocessedNumbersForPartlyBatch);

        when(service.putObjects((Card[]) anyObject())).thenReturn(
                fullOperationResponse).thenReturn(partOperationResponse);

        List<String> result = helper.updateCards(sourceCards);

        assertEquals(0, result.size());

        // verify(service, times(2)).putObjects((Card[]) anyObject());
    }

    @Test
    public void updateCardsInBatchModeWithUnprocessed()
            throws InterruptedException, AmazonServiceException,
            IllegalArgumentException, ItemSizeLimitExceededException {

        Card[] cardFullBatch = new Card[25];
        Card[] cardPartBatch = new Card[4];
        List<ComplexKey> unprocessedNumbersForFullBatch = new ArrayList<ComplexKey>();
        List<ComplexKey> unprocessedNumbersForPartlyBatch = new ArrayList<ComplexKey>();
        List<Card> sourceCards = new ArrayList<Card>();

        for (int i = 0; i < cardFullBatch.length; i++) {
            Card tmp = new Card(String.valueOf(i));
            cardFullBatch[i] = tmp;
            sourceCards.add(tmp);
            unprocessedNumbersForFullBatch.add(new ComplexKey(
                    String.valueOf(i), null));
        }

        for (int i = 0; i < cardPartBatch.length; i++) {
            Card tmp = new Card(String.valueOf(i));
            cardPartBatch[i] = tmp;
            sourceCards.add(tmp);
            unprocessedNumbersForPartlyBatch.add(new ComplexKey(String
                    .valueOf(i), null));
        }

        BatchOperationResult<Card> fullOperationResponse = new BatchOperationResult<Card>(
                null, unprocessedNumbersForFullBatch);
        BatchOperationResult<Card> partOperationResponse = new BatchOperationResult<Card>(
                null, unprocessedNumbersForPartlyBatch);

        when(service.putObjects((Card[]) anyObject())).thenReturn(
                fullOperationResponse).thenReturn(partOperationResponse);

        when(service.getMaxBatchSize()).thenReturn(25);

        List<String> result = helper.updateCards(sourceCards);

        assertEquals(cardFullBatch.length + cardPartBatch.length, result.size());

        verify(service, times(2)).putObjects((Card[]) anyObject());
    }

    @Test
    public void updateIndexesInBatchModeWithoutUnprocessed()
            throws InterruptedException, AmazonServiceException,
            IllegalArgumentException, ItemSizeLimitExceededException {

        TelCardIndex[] indexFullBatch = new TelCardIndex[25];
        TelCardIndex[] indexPartBatch = new TelCardIndex[4];
        List<ComplexKey> unprocessedNumbersForFullBatch = new ArrayList<ComplexKey>();
        List<ComplexKey> unprocessedNumbersForPartlyBatch = new ArrayList<ComplexKey>();
        List<TelCardIndex> sourceIndexes = new ArrayList<TelCardIndex>();

        for (int i = 0; i < indexFullBatch.length; i++) {
            TelCardIndex tmp = new TelCardIndex(String.valueOf(i), "prefix");
            indexFullBatch[i] = tmp;
            sourceIndexes.add(tmp);
        }

        for (int i = 0; i < indexPartBatch.length; i++) {
            TelCardIndex tmp = new TelCardIndex(String.valueOf(i), "prefix");
            indexPartBatch[i] = tmp;
            sourceIndexes.add(tmp);
        }

        BatchOperationResult<TelCardIndex> fullOperationResponse = new BatchOperationResult<TelCardIndex>(
                null, unprocessedNumbersForFullBatch);
        BatchOperationResult<TelCardIndex> partOperationResponse = new BatchOperationResult<TelCardIndex>(
                null, unprocessedNumbersForPartlyBatch);

        when(service.putObjects((TelCardIndex[]) anyObject())).thenReturn(
                fullOperationResponse).thenReturn(partOperationResponse);
        when(service.getMaxBatchSize()).thenReturn(25);

        List<String> result = helper.updateIndexes(sourceIndexes);

        assertEquals(0, result.size());

        // verify(service, times(2)).putObjects((Card[]) anyObject());
    }

    @Test
    public void updateIndexesInBatchModeWithUnprocessed()
            throws InterruptedException, AmazonServiceException,
            IllegalArgumentException, ItemSizeLimitExceededException {

        TelCardIndex[] indexFullBatch = new TelCardIndex[25];
        TelCardIndex[] indexPartBatch = new TelCardIndex[4];
        List<ComplexKey> unprocessedNumbersForFullBatch = new ArrayList<ComplexKey>();
        List<ComplexKey> unprocessedNumbersForPartlyBatch = new ArrayList<ComplexKey>();
        List<TelCardIndex> sourceIndexes = new ArrayList<TelCardIndex>();

        for (int i = 0; i < indexFullBatch.length; i++) {
            TelCardIndex tmp = new TelCardIndex(String.valueOf(i), "prefix");
            indexFullBatch[i] = tmp;
            sourceIndexes.add(tmp);
            unprocessedNumbersForFullBatch.add(new ComplexKey(
                    String.valueOf(i), null));
        }

        for (int i = 0; i < indexPartBatch.length; i++) {
            TelCardIndex tmp = new TelCardIndex(String.valueOf(i), "prefix");
            indexPartBatch[i] = tmp;
            sourceIndexes.add(tmp);
            unprocessedNumbersForPartlyBatch.add(new ComplexKey(String
                    .valueOf(i), null));
        }

        BatchOperationResult<TelCardIndex> fullOperationResponse = new BatchOperationResult<TelCardIndex>(
                null, unprocessedNumbersForFullBatch);
        BatchOperationResult<TelCardIndex> partOperationResponse = new BatchOperationResult<TelCardIndex>(
                null, unprocessedNumbersForPartlyBatch);

        when(service.putObjects((TelCardIndex[]) anyObject())).thenReturn(
                fullOperationResponse).thenReturn(partOperationResponse);
        when(service.getMaxBatchSize()).thenReturn(25);

        List<String> result = helper.updateIndexes(sourceIndexes);

        assertEquals(indexFullBatch.length + indexPartBatch.length, result
                .size());
    }

    @Test
    public void testUpdateCardBatchesWithAdd()
            throws ItemSizeLimitExceededException {

        Card card1 = new Card("Number1");
        Card card2 = new Card("Number2");
        List<Card> cardList = new ArrayList<Card>();
        cardList.add(card1);
        cardList.add(card2);

        String batchId = "BatchId";
        CardBatch cardBatch = new CardBatch();
        cardBatch.setBatchId(batchId);

        when(service.getObject(CardBatch.class, batchId)).thenReturn(cardBatch);
        when(service.putObject(cardBatch)).thenReturn(true);

        boolean result = helper.updateCardBatches(cardList, batchId, false);

        assertEquals(true, result);
        assertEquals(2, cardBatch.getBatchList().size());

        verify(service).getObject(CardBatch.class, batchId);
        verify(service).putObject(cardBatch);
    }

    @Test
    public void testUpdateCardBatchesWithDelete()
            throws ItemSizeLimitExceededException {

        Card card1 = new Card("Number1");
        Card card2 = new Card("Number2");
        List<Card> cardList = new ArrayList<Card>();
        cardList.add(card1);
        cardList.add(card2);

        String batchId = "BatchId";
        CardBatch cardBatch = new CardBatch();
        cardBatch.setBatchId(batchId);

        List<String> numberList = new ArrayList<String>();
        numberList.add(card1.getNumber());
        numberList.add(card2.getNumber());
        cardBatch.setBatchList(numberList);

        when(service.getObject(CardBatch.class, batchId)).thenReturn(cardBatch);
        when(service.putObject(cardBatch)).thenReturn(true);

        boolean result = helper.updateCardBatches(cardList, batchId, true);

        assertEquals(true, result);
        assertEquals(0, cardBatch.getBatchList().size());

        verify(service).getObject(CardBatch.class, batchId);
        verify(service).putObject(cardBatch);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testCheckPhones() {

        String phone = "1112223344";
        Map<String, Object> attributes = new HashMap<String, Object>();

        when(
                service.getAttributes(eq(TelCardIndex.TABLE_NAME), eq(phone),
                        eq(TelCardIndex.PREFIX), any(List.class))).thenReturn(
                attributes);
        String res = helper.checkPhone(phone);

        assertNull(res);

        boolean used = helper.isPhoneUsed(phone);

        assertFalse(used);

        used = helper.isPhoneUsed("");
        assertFalse(used);
        used = helper.isPhoneUsed(null);
        assertFalse(used);
    }

    @Test
    public void testDeleteTelephoneNumber() {
        String telephoneNumber = "1234567890";

        helper.deleteTelephoneIndex(telephoneNumber);

        verify(service).deleteItem(eq(TelCardIndex.TABLE_NAME),
                eq(telephoneNumber), eq(TelCardIndex.PREFIX));
    }
}
