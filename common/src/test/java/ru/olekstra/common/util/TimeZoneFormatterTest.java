package ru.olekstra.common.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TimeZoneFormatterTest {

    @Test
    public void testIsTimeZoneValid() {

        String timeZone1 = "Europe/Moscow";
        String timeZone2 = "any text value";
        String timeZone3 = null;

        assertTrue(TimeZoneFormatter.isTimeZoneValid(timeZone1));
        assertFalse(TimeZoneFormatter.isTimeZoneValid(timeZone2));
        assertFalse(TimeZoneFormatter.isTimeZoneValid(timeZone3));
    }
/*
    @Test
    public void testGetTimeZone() {

        String timeZoneName1 = "Europe/Moscow";
        String timeZoneName2 = "any text value";

        TimeZone timeZone1 = TimeZone.getTimeZone(timeZoneName1);
        TimeZone timeZone2 = TimeZone
                .getTimeZone(AppSettingsService.DEFAULT_TIME_ZONE);

        assertEquals(timeZone1, TimeZoneFormatter.getTimeZone(timeZoneName1));
        assertEquals(timeZone2, TimeZoneFormatter.getTimeZone(timeZoneName2));
    }

    @Test
    public void testDateTimeZone() {

        String timeZoneName1 = "Europe/Moscow";
        String timeZoneName2 = "any text value";

        DateTimeZone timeZone1 = DateTimeZone.forID(timeZoneName1);
        DateTimeZone timeZone2 = DateTimeZone
                .forID(AppSettingsService.DEFAULT_TIME_ZONE);

        assertEquals(timeZone1,
                TimeZoneFormatter.getDateTimeZone(timeZoneName1));
        assertEquals(timeZone2,
                TimeZoneFormatter.getDateTimeZone(timeZoneName2));
    }
    */
}