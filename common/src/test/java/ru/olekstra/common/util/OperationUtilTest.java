package ru.olekstra.common.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class OperationUtilTest {

    @Test
    public void testGenerateNextId() {

        String id = "testId";
        String nextId = id + OperationUtil.SEPARATOR
                + OperationUtil.NEXT_ID_START_INDEX;
        String result = OperationUtil.generateNextId(id);

        assertEquals(nextId, result);

        String currentId = nextId;
        nextId = id + OperationUtil.SEPARATOR + 3;
        result = OperationUtil.generateNextId(currentId);

        assertEquals(nextId, result);

        currentId = id + OperationUtil.SEPARATOR + 9;
        nextId = id + OperationUtil.SEPARATOR + 10;
        result = OperationUtil.generateNextId(currentId);

        assertEquals(nextId, result);
    }

    @Test
    public void testHasNextRecord() {

        String id = "testId";
        boolean result = OperationUtil.hasNextRecord(id);

        assertFalse(result);

        id = "testId#2";
        result = OperationUtil.hasNextRecord(id);

        assertTrue(result);
    }

    @Test
    public void testIsReadyToStoreNextId() {

        String currentId = "testId";
        String nextId = "";
        boolean result = OperationUtil.isReadyToStoreNextId(currentId, nextId);

        assertFalse(result);

        currentId = "testId";
        nextId = "testId";
        result = OperationUtil.isReadyToStoreNextId(currentId, nextId);

        assertFalse(result);

        currentId = "testId";
        nextId = "testId#0";
        result = OperationUtil.isReadyToStoreNextId(currentId, nextId);

        assertTrue(result);

        currentId = "testId#2";
        nextId = "testId#0";
        result = OperationUtil.isReadyToStoreNextId(currentId, nextId);

        assertTrue(result);

        currentId = "testId#9";
        nextId = "testId#0";
        result = OperationUtil.isReadyToStoreNextId(currentId, nextId);

        assertFalse(result);

        currentId = "testId#9";
        nextId = "testId#00";
        result = OperationUtil.isReadyToStoreNextId(currentId, nextId);

        assertTrue(result);
    }

    @Test
    public void testGenerateNextIdReserve() {

        String currentId = "testId";
        String nextId = "testId#0";
        String result = OperationUtil.generateNextIdReserve(currentId);

        assertEquals(nextId, result);

        currentId = "testId#1";
        nextId = "testId#0";
        result = OperationUtil.generateNextIdReserve(currentId);

        assertEquals(nextId, result);

        currentId = "testId#2";
        nextId = "testId#0";
        result = OperationUtil.generateNextIdReserve(currentId);

        assertEquals(nextId, result);

        currentId = "testId#9";
        nextId = "testId#0";
        result = OperationUtil.generateNextIdReserve(currentId);

        assertFalse(nextId.equals(result));

        currentId = "testId#9";
        nextId = "testId#00";
        result = OperationUtil.generateNextIdReserve(currentId);

        assertTrue(nextId.equals(result));

        currentId = "testId#99";
        nextId = "testId#000";
        result = OperationUtil.generateNextIdReserve(currentId);

        assertTrue(nextId.equals(result));
    }

    @Test
    public void testMakeReserveIndex() {

        int scale = 1;
        String index = "0";
        String result = OperationUtil.makeReserveIndex(scale);

        assertEquals(index, result);

        scale = 2;
        index = "00";
        result = OperationUtil.makeReserveIndex(scale);

        assertEquals(index, result);

        scale = 3;
        index = "000";
        result = OperationUtil.makeReserveIndex(scale);

        assertEquals(index, result);
    }

    @Test
    public void testDeleteSpaces() {
        String input = "  123    456  789  ";
        String expect = "123456789";

        String result = OperationUtil.deleteSpaces(input);

        assertEquals(expect, result);
    }

    @Test
    public void testDeleteSplashes() {
        String input = "123-456-789";
        String expect = "123456789";

        String result = OperationUtil.deleteSplashes(input);

        assertEquals(expect, result);
    }

    @Test
    public void testValidateEmail() {

        String[] invalidEmails = {"test@test@mail.com", ".test@mail.com",
                "test@.com", "test@.1ru", "test@.r"};

        String[] validEmails = {"test@mail.com", "test-1@test.com",
                "test.1@test.net", "test.1@test.com.ru",
                "test-mail@test-1.mail.ru"};

        for (String value : validEmails) {
            assertTrue(OperationUtil.validateEmail(value));
        }

        for (String value : invalidEmails) {
            assertFalse(OperationUtil.validateEmail(value));
        }
    }
}
