package ru.olekstra.common.util;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;

@RunWith(MockitoJUnitRunner.class)
public class VelocityMessageFormatterTest {

    private MessageSource messageSource;
    private VelocityMessageFormatter formatter;

    @Before
    public void setUp() throws Exception {
        messageSource = mock(MessageSource.class);
        formatter = new VelocityMessageFormatter(messageSource);
    }

    @Test
    public void testGetMessage() {

        String messageCode = "test";
        String value1 = "value1";
        String value2 = "value2";
        String value3 = "value3";
        List<Object> values = new ArrayList<Object>(Arrays.asList(value1,
                value2, value3));

        String formatedMessage = "result text: value1, value2, value3";
        when(messageSource.getMessage(messageCode, values.toArray(), null))
                .thenReturn(
                        String.format("result text: %s, %s, %s", value1,
                                value2,
                                value3));

        String result = formatter.getMessage(messageCode, values);

        verify(messageSource).getMessage(messageCode, values.toArray(), null);
        assertEquals(formatedMessage, result);
    }

    @Test
    public void testGetMessageWithNullParameters() {

        String messageCode = "test";
        String value1 = "value1";
        String value2 = null;
        String value3 = null;
        String value4 = "value4";
        List<Object> values = new ArrayList<Object>(Arrays.asList(value1,
                value2, value3, value4));

        String formatedMessage = "result text: value1, , , value4";
        when(
                messageSource.getMessage(messageCode, new String[] {value1, "",
                        "", value4}, null))
                .thenReturn(
                        String.format("result text: %s, %s, %s, %s", value1,
                                "",
                                "",
                                value4));

        String result = formatter.getMessage(messageCode, values);

        verify(messageSource).getMessage(messageCode, new String[] {value1, "",
                "", value4}, null);
        assertEquals(formatedMessage, result);
    }
}
