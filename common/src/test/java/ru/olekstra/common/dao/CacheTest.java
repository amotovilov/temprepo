package ru.olekstra.common.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.domain.Drugstore;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:ru/olekstra/common/applicationContext-test.xml"})
public class CacheTest {

    @Autowired
    public DynamodbService service;

    @Autowired
    private DrugstoreDao dao;

    @Autowired
    private CacheManager commonCacheManager;

    @Before
    public void before() {
        assertNotNull(commonCacheManager);
    }

    @Test
    public void testCache() throws IOException {

        Drugstore drugstore = new Drugstore("id");
        drugstore.setName("name");
        drugstore.setAuthCode("location");
        drugstore.setTimeZone("TZ");
        drugstore.setDisabled(false);

        List<Drugstore> itemList = new ArrayList<Drugstore>();
        itemList.add(drugstore);

        when(service.getAllObjects(Drugstore.class)).thenReturn(
                itemList);

        List<Drugstore> drugstoreList = dao.getDrugstoreNames();

        Cache cache = commonCacheManager.getCache(DrugstoreDao.CACHE_NAME);
        assertNotNull(drugstoreList);
        assertNotNull(cache);

        assertEquals(cache.getSize(), drugstoreList.size());

        when(
                service.putObjectWithCondition(drugstore,
                        Drugstore.FLD_AUTH_CODE, drugstore
                                .getAuthCode())).thenReturn(true);

        dao.concurrentUpdateDrugstore(drugstore);

        verify(service, times(2)).getAllObjects(Drugstore.class);

        assertEquals(1, cache.getSize());

    }
}
