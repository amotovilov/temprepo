package ru.olekstra.common.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.domain.Discount;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:ru/olekstra/common/applicationContext-test.xml"})
public class DiscountDaoTest {

    @Autowired
    private DynamodbService service;

    @Autowired
    private DiscountDao discountDao;

    @Autowired
    private CacheManager commonCacheManager;

    @Before
    public void setUp() throws Exception {
        assertNotNull(commonCacheManager);
    }

    @Test
    public void testGetDiscountWithCache() throws RuntimeException,
            InterruptedException, IOException {

        String id1 = "id1";
        String id2 = "id2";
        String id3 = "id3";

        List<String> discountsId = new ArrayList<String>();
        discountsId.add(id1);
        discountsId.add(id2);
        discountsId.add(id3);

        List<Discount> disconts = new ArrayList<Discount>();
        disconts.add(new Discount());
        disconts.add(new Discount());
        disconts.add(new Discount());

        Cache cache = commonCacheManager.getCache(DiscountDao.CACHE_NAME);

        assertNotNull(cache);
        assertEquals(0, cache.getSize());

        // Первый запуск без кеша
        when(service.getAllObjects(eq(Discount.class))).thenReturn(disconts);
        List<Discount> actualResult = discountDao
                .getAllDiscountsUsingCache();
        assertNotNull(actualResult);
        assertEquals(disconts.size(), actualResult.size());
        verify(service).getAllObjects(eq(Discount.class));

        cache = commonCacheManager.getCache(DiscountDao.CACHE_NAME);
        // В кеше должна быть 1 запись
        assertNotNull(cache);
        assertEquals(1, cache.getSize());

        // Второй запуск с использованием кеша. не должно быть вызова к
        // dynamoDBService
        // Второй запуск не делаем, т.к. почему то в тестах все равно
        // выполняется вызов, хотя в продакшене все нормально
        // OLX-1942
        //
        // fixed: помогает reset для мока.

        reset(service);

        when(service.getAllObjects(eq(Discount.class))).thenReturn(disconts);
        actualResult = discountDao.getAllDiscountsUsingCache();

        assertNotNull(actualResult);
        assertEquals(disconts.size(), actualResult.size());
        verify(service, times(0)).getAllObjects(eq(Discount.class));
    }

}
