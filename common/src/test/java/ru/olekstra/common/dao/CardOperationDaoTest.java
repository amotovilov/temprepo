package ru.olekstra.common.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;

import ru.olekstra.awsutils.BatchOperationResult;
import ru.olekstra.awsutils.DynamodbEntity;
import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.domain.CardLog;
import ru.olekstra.domain.CardOperation;
import ru.olekstra.domain.Drugstore;
import ru.olekstra.domain.dto.CardLogRecord;
import ru.olekstra.domain.dto.CardLogType;
import ru.olekstra.domain.dto.LogRecord;

public class CardOperationDaoTest {

    private CardOperationDao cardOperationDao;
    private DrugstoreDao drugstoreDao;
    private DynamodbService service;

    private static final DateTimeFormatter cardFormatter = DateTimeFormat
            .forPattern("yyyyMM");

    @Before
    public void setup() {
        service = mock(DynamodbService.class);
        drugstoreDao = mock(DrugstoreDao.class);
        cardOperationDao = new CardOperationDao(service, drugstoreDao);
    }

    private String getActualPeriod() {
        return DateTime.now().withZone(DateTimeZone.UTC)
                .toString(cardFormatter);
    }

    @Test
    public void testSaveLogRecord() throws JsonGenerationException,
            JsonMappingException, ItemSizeLimitExceededException, IOException {

        String tableName = CardOperation.TABLE_NAME;
        String id = "777";
        String range = getActualPeriod();
        String nextId = "777#0";

        List<String> attributesToGet = new ArrayList<String>();
        attributesToGet.add(CardOperation.FLD_NEXT_ID);

        Map<String, Object> attributes = new HashMap<String, Object>();
        attributes.put(CardOperation.FLD_NEXT_ID, nextId);

        LogRecord record = new LogRecord("any drugstore id", "any note",
                CardLogType.CHANGE_PLAN, DateTime.now().withZone(DateTimeZone.UTC));

        when(service.getAttributes(eq(tableName),
                eq(id), eq(range), eq(attributesToGet))).thenReturn(
                attributes);

        when(
                service.appendAttribute(eq(tableName), eq(id), eq(range),
                        eq(CardOperation.FLD_LOG_RECORD),
                        eq(DynamodbEntity.toJson(record)))).thenReturn(true);

        cardOperationDao.saveLogRecord(id, record);

        verify(service).getAttributes(eq(tableName),
                eq(id), eq(range), eq(attributesToGet));

        verify(service).appendAttribute(eq(tableName), eq(id), eq(range),
                eq(CardOperation.FLD_LOG_RECORD),
                eq(DynamodbEntity.toJson(record)));
    }

    @Test
    public void testSaveLogRecordItemSizeLimitExceeded()
            throws JsonGenerationException,
            JsonMappingException, ItemSizeLimitExceededException, IOException {

        String tableName = CardOperation.TABLE_NAME;
        String id = "777";
        String range = getActualPeriod();
        String nextId = "777#0";
        String updatedNextId = "777#2";

        List<String> attributesToGet = new ArrayList<String>();
        attributesToGet.add(CardOperation.FLD_NEXT_ID);

        Map<String, Object> attributes = new HashMap<String, Object>();
        attributes.put(CardOperation.FLD_NEXT_ID, nextId);

        LogRecord record = new LogRecord("any drugstore id", "any note",
                CardLogType.CHANGE_PLAN, DateTime.now().withZone(DateTimeZone.UTC));

        when(service.getAttributes(eq(tableName),
                eq(id), eq(range), eq(attributesToGet))).thenReturn(
                attributes);

        when(
                service.appendAttribute(eq(tableName), eq(id), eq(range),
                        eq(CardOperation.FLD_LOG_RECORD),
                        eq(DynamodbEntity.toJson(record)))).thenThrow(
                new ItemSizeLimitExceededException("test"));

        when(
                service.updateAttribute(eq(tableName), eq(id), eq(range),
                        eq(CardOperation.FLD_NEXT_ID), eq(nextId))).thenReturn(
                true);

        cardOperationDao.saveLogRecord(id, record);

        verify(service).getAttributes(eq(tableName),
                eq(id), eq(range), eq(attributesToGet));
        verify(service).appendAttribute(eq(tableName), eq(id), eq(range),
                eq(CardOperation.FLD_LOG_RECORD),
                eq(DynamodbEntity.toJson(record)));

        verify(service).updateAttribute(eq(tableName), eq(id), eq(range),
                eq(CardOperation.FLD_NEXT_ID), eq(updatedNextId));
        verify(service).appendAttribute(eq(tableName), eq(updatedNextId),
                eq(range),
                eq(CardOperation.FLD_LOG_RECORD),
                eq(DynamodbEntity.toJson(record)));
    }

    @Test
    public void testSaveLogRecords() throws JsonGenerationException,
            JsonMappingException, ItemSizeLimitExceededException, IOException,
            RuntimeException, InterruptedException {

        String range = getActualPeriod();
        LogRecord record = new LogRecord("any drugstore id", "any note",
                CardLogType.CHANGE_PLAN, DateTime.now().withZone(DateTimeZone.UTC));
        Map<String, LogRecord> records = new HashMap<String, LogRecord>();
        records.put("7771", record);
        records.put("7772", record);

        Set<String> cardNumbers = records.keySet();
        String[] ranges = new String[cardNumbers.size()];
        Arrays.fill(ranges, getActualPeriod());

        CardOperation operation1 = new CardOperation("7771", range);
        operation1.getLogRecord().add("some old log record");
        CardOperation operation2 = new CardOperation("7772", range);
        operation2.getLogRecord().add("some old log record");

        BatchOperationResult<CardOperation> loadedOperationsBatch = new BatchOperationResult<CardOperation>();
        loadedOperationsBatch.getSuccessList().add(operation1);
        loadedOperationsBatch.getSuccessList().add(operation2);

        List<CardOperation> batch = new ArrayList<CardOperation>();
        batch.add(operation1);
        batch.add(operation2);

        when(
                service.getObjects(
                        eq(CardOperation.class),
                        eq(cardNumbers.toArray(new String[cardNumbers.size()])),
                        eq(ranges))).thenReturn(loadedOperationsBatch);

        when(
                service.putObjects(eq(batch.toArray(new CardOperation[batch
                        .size()]))))
                .thenReturn(new BatchOperationResult<CardOperation>());

        cardOperationDao.saveLogRecords(records);

        verify(service).getObjects(
                eq(CardOperation.class),
                eq(cardNumbers.toArray(new String[cardNumbers.size()])),
                eq(ranges));
        verify(service).putObjects(eq(batch.toArray(new CardOperation[batch
                .size()])));
    }

    @Test
    public void testSaveLogRecordsItemSizeLimitExceeded()
            throws JsonGenerationException,
            JsonMappingException, ItemSizeLimitExceededException, IOException,
            RuntimeException, InterruptedException {

        String range = getActualPeriod();
        LogRecord record = new LogRecord("any drugstore id", "any note",
                CardLogType.CHANGE_PLAN, DateTime.now().withZone(DateTimeZone.UTC));
        Map<String, LogRecord> records = new HashMap<String, LogRecord>();
        records.put("7771", record);
        records.put("7772", record);

        Set<String> cardNumbers = records.keySet();
        String[] ranges = new String[cardNumbers.size()];
        Arrays.fill(ranges, getActualPeriod());

        CardOperation operation1 = new CardOperation("7771", range);
        operation1.getLogRecord().add("some old log record");
        CardOperation operation2 = new CardOperation("7772", range);
        operation2.getLogRecord().add("some old log record");

        BatchOperationResult<CardOperation> loadedOperationsBatch = new BatchOperationResult<CardOperation>();
        loadedOperationsBatch.getSuccessList().add(operation1);
        loadedOperationsBatch.getSuccessList().add(operation2);

        List<CardOperation> batch = new ArrayList<CardOperation>();
        batch.add(operation1);
        batch.add(operation2);

        when(
                service.getObjects(
                        eq(CardOperation.class),
                        eq(cardNumbers.toArray(new String[cardNumbers.size()])),
                        eq(ranges))).thenReturn(loadedOperationsBatch);

        when(
                service.putObjects(eq(batch.toArray(new CardOperation[batch
                        .size()]))))
                .thenThrow(new ItemSizeLimitExceededException("test"));

        when(service.putObject(any(CardOperation.class))).thenReturn(true);

        cardOperationDao.saveLogRecords(records);

        verify(service).getObjects(
                eq(CardOperation.class),
                eq(cardNumbers.toArray(new String[cardNumbers.size()])),
                eq(ranges));
        verify(service).putObjects(eq(batch.toArray(new CardOperation[batch
                .size()])));
        verify(service, times(2)).putObject(any(CardOperation.class));
    }

    @Test
    public void testSaveLogRecordsItemSizeLimitExceededMore()
            throws JsonGenerationException,
            JsonMappingException, ItemSizeLimitExceededException, IOException,
            RuntimeException, InterruptedException {

        String range = getActualPeriod();
        LogRecord record = new LogRecord("any drugstore id", "any note",
                CardLogType.CHANGE_PLAN, DateTime.now().withZone(DateTimeZone.UTC));
        Map<String, LogRecord> records = new HashMap<String, LogRecord>();
        records.put("7771", record);
        records.put("7772", record);

        Set<String> cardNumbers = records.keySet();
        String[] ranges = new String[cardNumbers.size()];
        Arrays.fill(ranges, getActualPeriod());

        CardOperation operation1 = new CardOperation("7771", range);
        operation1.getLogRecord().add("some old log record");
        CardOperation operation2 = new CardOperation("7772", range);
        operation2.getLogRecord().add("some old log record");

        BatchOperationResult<CardOperation> loadedOperationsBatch = new BatchOperationResult<CardOperation>();
        loadedOperationsBatch.getSuccessList().add(operation1);
        loadedOperationsBatch.getSuccessList().add(operation2);

        List<CardOperation> batch = new ArrayList<CardOperation>();
        batch.add(operation1);
        batch.add(operation2);

        when(
                service.getObjects(
                        eq(CardOperation.class),
                        eq(cardNumbers.toArray(new String[cardNumbers.size()])),
                        eq(ranges))).thenReturn(loadedOperationsBatch);

        when(
                service.putObjects(eq(batch.toArray(new CardOperation[batch
                        .size()]))))
                .thenThrow(new ItemSizeLimitExceededException("test"));

        when(service.putObject(eq(operation1))).thenThrow(
                new ItemSizeLimitExceededException("test"));
        when(service.putObject(eq(operation2))).thenReturn(true);

        cardOperationDao.saveLogRecords(records);

        verify(service).getObjects(
                eq(CardOperation.class),
                eq(cardNumbers.toArray(new String[cardNumbers.size()])),
                eq(ranges));
        verify(service).putObjects(eq(batch.toArray(new CardOperation[batch
                .size()])));
        verify(service).putObject(eq(operation1));
        verify(service).putObject(eq(operation2));
        verify(service, times(3)).putObject(any(CardOperation.class));
    }

    @Test
    public void testSaveLogRecordsWithNewOperation()
            throws JsonGenerationException,
            JsonMappingException, ItemSizeLimitExceededException, IOException,
            RuntimeException, InterruptedException {

        String range = getActualPeriod();
        LogRecord record = new LogRecord("any drugstore id", "any note",
                CardLogType.CHANGE_PLAN, DateTime.now().withZone(DateTimeZone.UTC));
        Map<String, LogRecord> records = new HashMap<String, LogRecord>();
        records.put("7771", record);
        records.put("7772", record);

        Set<String> cardNumbers = records.keySet();
        String[] ranges = new String[cardNumbers.size()];
        Arrays.fill(ranges, getActualPeriod());

        CardOperation operation1 = new CardOperation("7771", range);
        operation1.getLogRecord().add("some old log record");
        BatchOperationResult<CardOperation> loadedOperationsBatch = new BatchOperationResult<CardOperation>();
        loadedOperationsBatch.getSuccessList().add(operation1);

        when(
                service.getObjects(
                        eq(CardOperation.class),
                        eq(cardNumbers.toArray(new String[cardNumbers.size()])),
                        eq(ranges))).thenReturn(loadedOperationsBatch);

        when(
                service.putObjects(any(CardOperation[].class)))
                .thenReturn(new BatchOperationResult<CardOperation>());

        cardOperationDao.saveLogRecords(records);

        verify(service).getObjects(
                eq(CardOperation.class),
                eq(cardNumbers.toArray(new String[cardNumbers.size()])),
                eq(ranges));
        verify(service).putObjects(any(CardOperation[].class));
    }

    @Test
    public void testAppendLogRecords()
            throws ItemSizeLimitExceededException, JsonGenerationException,
            JsonMappingException, IOException {

        String cardNumber = "777";
        List<String> records = Arrays.asList("record1", "record2", "record3");

        String tableName = CardOperation.TABLE_NAME;
        String range = getActualPeriod();
        String nextId = "777#0";

        Map<String, Object> attributes = new HashMap<String, Object>();
        attributes.put(CardOperation.FLD_NEXT_ID, nextId);
        List<String> attributesToGet = new ArrayList<String>();
        attributesToGet.add(CardOperation.FLD_NEXT_ID);

        CardOperation cardOperation = new CardOperation(cardNumber, range);
        cardOperation.getLogRecord().addAll(records);

        when(
                service.getAttributes(eq(tableName), eq(cardNumber), eq(range),
                        eq(attributesToGet))).thenReturn(attributes);

        when(
                service.getObject(eq(CardOperation.class), eq(cardNumber),
                        eq(range)))
                .thenReturn(cardOperation);

        when(service.putObject(eq(cardOperation))).thenReturn(true);

        cardOperationDao.appendLogRecords(cardNumber, records);

        verify(service).getAttributes(eq(tableName), eq(cardNumber), eq(range),
                eq(attributesToGet));

        verify(service).getObject(eq(CardOperation.class), eq(cardNumber),
                eq(range));

        verify(service).putObject(eq(cardOperation));
    }

    @Test
    public void testAppendLogRecordsWhenItemLimitIsReached()
            throws ItemSizeLimitExceededException, JsonGenerationException,
            JsonMappingException, IOException {

        String cardNumber = "777";
        List<String> records = Arrays.asList("record1", "record2", "record3");

        String tableName = CardOperation.TABLE_NAME;
        String range = getActualPeriod();
        String reservedForNextId = "777#0";
        String updatedNextId = "777#2";

        Map<String, Object> attributes = new HashMap<String, Object>();
        attributes.put(CardOperation.FLD_NEXT_ID, reservedForNextId);
        List<String> attributesToGet = new ArrayList<String>();
        attributesToGet.add(CardOperation.FLD_NEXT_ID);

        CardOperation cardOperation = new CardOperation(cardNumber, range);

        when(
                service.getAttributes(eq(tableName), eq(cardNumber), eq(range),
                        eq(attributesToGet))).thenReturn(attributes);

        when(
                service.getObject(eq(CardOperation.class), eq(cardNumber),
                        eq(range)))
                .thenReturn(cardOperation);

        when(service.putObject(eq(cardOperation))).thenThrow(
                new ItemSizeLimitExceededException("test"));

        cardOperationDao.appendLogRecords(cardNumber, records);

        verify(service).getAttributes(eq(tableName), eq(cardNumber), eq(range),
                eq(attributesToGet));

        verify(service).getObject(eq(CardOperation.class), eq(cardNumber),
                eq(range));

        verify(service).putObject(eq(cardOperation));

        verify(service).updateAttribute(eq(tableName), eq(cardNumber),
                eq(range),
                eq(CardOperation.FLD_NEXT_ID), eq(updatedNextId));

        verify(service, times(2)).putObject(any(CardOperation.class));
    }

    @Test
    public void testAppendLogRecordsNullRecordsListLoaded()
            throws ItemSizeLimitExceededException, JsonGenerationException,
            JsonMappingException, IOException {

        String cardNumber = "777";
        List<String> records = Arrays.asList("record1", "record2", "record3");

        String tableName = CardOperation.TABLE_NAME;
        String range = getActualPeriod();
        String nextId = "777#0";

        Map<String, Object> attributes = new HashMap<String, Object>();
        attributes.put(CardOperation.FLD_NEXT_ID, nextId);
        List<String> attributesToGet = new ArrayList<String>();
        attributesToGet.add(CardOperation.FLD_NEXT_ID);

        CardOperation cardOperation = new CardOperation(cardNumber, range);
        cardOperation.setLogRecord(null);

        when(
                service.getAttributes(eq(tableName), eq(cardNumber), eq(range),
                        eq(attributesToGet))).thenReturn(attributes);

        when(
                service.getObject(eq(CardOperation.class), eq(cardNumber),
                        eq(range)))
                .thenReturn(cardOperation);

        when(service.putObject(eq(cardOperation))).thenReturn(true);

        cardOperationDao.appendLogRecords(cardNumber, records);

        verify(service).getAttributes(eq(tableName), eq(cardNumber), eq(range),
                eq(attributesToGet));

        verify(service).getObject(eq(CardOperation.class), eq(cardNumber),
                eq(range));

        verify(service).putObject(eq(cardOperation));
    }

    @Test
    public void testGetLogRecord() throws JsonParseException,
            JsonMappingException, IOException {
        String currentId = "cardId";
        String nextId = "cardId#0";
        String status = "true";
        String drugstoreId = "drugstoreId";
        String period = "201204";
        String record = "{\"date\":1333416571789,\"drugstoreId\":\"drugstoreId\",\"note\":true}";

        List<String> records = new ArrayList<String>();
        records.add(record);

        Map<String, Object> result = new HashMap<String, Object>();
        result.put(CardOperation.FLD_NEXT_ID, nextId);
        result.put(CardOperation.FLD_LOG_RECORD, records);

        List<String> attributesToGet = new ArrayList<String>();
        attributesToGet.add(CardOperation.FLD_NEXT_ID);
        attributesToGet.add(CardOperation.FLD_LOG_RECORD);

        when(service.getAttributes(CardOperation.TABLE_NAME,
                currentId,
                period,
                attributesToGet)).thenReturn(result);

        when(drugstoreDao.getDrugstoreNames()).thenReturn(
                new ArrayList<Drugstore>());

        List<CardLogRecord> resultList = cardOperationDao.getLogRecords(
                currentId, period);

        verify(service).getAttributes(CardOperation.TABLE_NAME,
                currentId,
                period,
                attributesToGet);

        assertNotNull(resultList);
        assertEquals(1, resultList.size());
        assertEquals(drugstoreId,
                ((CardLogRecord) resultList.get(0)).getDrugstoreId());
        assertEquals(status, ((CardLogRecord) resultList.get(0)).getNote());
    }

    @Test
    public void testGetLogRecords() throws JsonParseException,
            JsonMappingException, IOException {
        String currentId = "cardId";
        String nextId2 = "cardId#2";
        String nextId3 = "cardId#3";
        String cardIdReserve = "cardId#0";
        String id = "testId";
        String status = "true";
        String period = "201204";

        String record1 = "{\"date\":1333416571789,\"drugstoreId\":\"" + id
                + "\",\"note\":true}";
        String record2 = "{\"date\":1333416571789,\"drugstoreId\":\"" + id
                + "2\",\"note\":true}";
        String record3 = "{\"date\":1333416571789,\"drugstoreId\":\"" + id
                + "3\",\"note\":true}";

        List<String> records1 = new ArrayList<String>();
        records1.add(record1);
        List<String> records2 = new ArrayList<String>();
        records2.add(record2);
        List<String> records3 = new ArrayList<String>();
        records3.add(record3);

        Map<String, Object> result = new HashMap<String, Object>();
        result.put(CardOperation.FLD_NEXT_ID, nextId2);
        result.put(CardOperation.FLD_LOG_RECORD, records1);

        Map<String, Object> result2 = new HashMap<String, Object>();
        result2.put(CardOperation.FLD_NEXT_ID, nextId3);
        result2.put(CardOperation.FLD_LOG_RECORD, records2);

        Map<String, Object> result3 = new HashMap<String, Object>();
        result3.put(CardOperation.FLD_NEXT_ID, cardIdReserve);
        result3.put(CardOperation.FLD_LOG_RECORD, records3);

        List<String> attributesToGet = new ArrayList<String>();
        attributesToGet.add(CardOperation.FLD_NEXT_ID);
        attributesToGet.add(CardOperation.FLD_LOG_RECORD);

        when(service.getAttributes(eq(CardOperation.TABLE_NAME),
                eq(currentId),
                eq(period), eq(attributesToGet))).thenReturn(result);

        when(service.getAttributes(eq(CardOperation.TABLE_NAME),
                eq(nextId2),
                eq(period), eq(attributesToGet))).thenReturn(result2);
        when(service.getAttributes(eq(CardOperation.TABLE_NAME),
                eq(nextId3),
                eq(period), eq(attributesToGet))).thenReturn(result3);

        when(service.getItemsByRangeKeyBegin(eq(CardLog.class), eq(currentId),
                eq(period))).thenReturn(null);

        when(drugstoreDao.getDrugstoreNames()).thenReturn(
                new ArrayList<Drugstore>());

        List<CardLogRecord> resultList = cardOperationDao.getLogRecords(
                currentId, period);

        verify(service).getAttributes(eq(CardOperation.TABLE_NAME),
                eq(currentId), eq(period), eq(attributesToGet));

        verify(service).getAttributes(eq(CardOperation.TABLE_NAME),
                eq(nextId2),
                eq(period), eq(attributesToGet));

        verify(service).getAttributes(eq(CardOperation.TABLE_NAME),
                eq(nextId3),
                eq(period), eq(attributesToGet));

        assertNotNull(resultList);
        assertEquals(3, resultList.size());

        assertEquals(id, ((CardLogRecord) resultList.get(0)).getDrugstoreId());
        assertEquals(status, ((CardLogRecord) resultList.get(0)).getNote());

        assertEquals(id + "2", ((CardLogRecord) resultList.get(1))
                .getDrugstoreId());
        assertEquals(status, ((CardLogRecord) resultList.get(1)).getNote());

        assertEquals(id + "3", ((CardLogRecord) resultList.get(2))
                .getDrugstoreId());
        assertEquals(status, ((CardLogRecord) resultList.get(2)).getNote());
    }

}
