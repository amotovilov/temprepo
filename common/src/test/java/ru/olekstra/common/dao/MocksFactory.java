package ru.olekstra.common.dao;

import org.mockito.Mockito;
import org.springframework.beans.factory.FactoryBean;

@SuppressWarnings("unchecked")
public class MocksFactory implements FactoryBean {

    private Class type;

    public void setType(final Class type) {
        this.type = type;
    }

    @Override
    public Object getObject() throws Exception {
        return Mockito.mock(type);
    }

    @Override
    public Class getObjectType() {
        return type;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

}
