package ru.olekstra.common.map;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ru.olekstra.domain.Drugstore;
import ru.olekstra.domain.dto.DrugstoreResponse;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class DrugstoreToDrugstoreResponseTest {

    @Autowired
    Mapper mapper;

    @Before
    public void setUp() {
    }

    @Test
    public void testGetResponseListFromDrugstoreList() throws JsonGenerationException,
            JsonMappingException, IOException {
        // Создаем объект, в уже существущем конструкторе заполняются в том
        // числе все маппируемые поля
        Drugstore drugstore = new Drugstore("testId");
        drugstore.setName("testName");
        drugstore.setAuthCode("testLocation");
        drugstore.setTimeZone("timeZone");
        drugstore.setDisabled(Boolean.TRUE);
        // Маппируем вперед
        DrugstoreResponse drugstoreResponse = mapper.map(drugstore, DrugstoreResponse.class);
        // Проверяем маппируемые поля (все поля целевой сущности)
        assertEquals(drugstore.getId(), drugstoreResponse.getId());
        assertEquals(drugstore.getId(), drugstoreResponse.getId());
        assertEquals(drugstore.getName(), drugstoreResponse.getName());
        assertEquals(drugstore.getTimeZone(), drugstoreResponse.getTimeZone());
        assertEquals(drugstore.getDisabled(), drugstoreResponse.getDisabled());
    }

    @Configuration
    static class TestConfig {

        @Bean(name = "org.dozer.Mapper")
        public DozerBeanMapper mapper() {
            return new org.dozer.DozerBeanMapper();
        }
    }

}
