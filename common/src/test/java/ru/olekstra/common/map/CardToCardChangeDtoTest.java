package ru.olekstra.common.map;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ru.olekstra.domain.Card;
import ru.olekstra.domain.dto.CardChangeDto;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration()
public class CardToCardChangeDtoTest {

    @Autowired
    Mapper mapper;

    @Before
    public void setUp() {
    }

    @Test
    public void testGetDtoFromEntity() throws JsonGenerationException,
            JsonMappingException, IOException {
        // Создаем объект
        Card entity = new Card("111", false, "aaa", "111", null, null); // new
        entity.setDiscountPlan("aaa");                                                                // DateTime(),
                                                                        // new
                                                                        // DateTime());
        List<String> b = new ArrayList<String>();
        b.add("qq");
        b.add("mm");
        entity.setCardBatches(b);
        // Маппируем вперед
        CardChangeDto dto = mapper.map(entity, CardChangeDto.class);
        // Проверяем маппируемые поля (все поля целевой сущности)
        assertEquals(entity.getNumber(), dto.getNumber());
        assertEquals(entity.getStartDate(), dto.getStartDate());
        assertEquals(entity.getEndDate(), dto.getEndDate());
        assertEquals(entity.getDisabled(), dto.getDisabled());
        assertEquals(entity.getTelephoneNumber(), dto.getTelephoneNumber());
        assertEquals(entity.getDiscountPlanId(), dto.getDiscountPlanId());
        assertEquals(entity.getCardBatches().size(), dto.getCardBatches().size());
    }

    @Configuration
    static class TestConfig {

        @Bean(name = "org.dozer.Mapper")
        public DozerBeanMapper mapper() {
            DozerBeanMapper result = new org.dozer.DozerBeanMapper();
            return result;
        }
    }

}
