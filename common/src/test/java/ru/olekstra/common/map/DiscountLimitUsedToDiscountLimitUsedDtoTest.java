package ru.olekstra.common.map;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.UUID;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ru.olekstra.domain.DiscountLimitUsed;
import ru.olekstra.domain.EntityProxy;
import ru.olekstra.domain.dto.DiscountLimitUsedDetail;
import ru.olekstra.domain.dto.DiscountLimitUsedDto;
import ru.olekstra.domain.dto.DiscountLimitUsedRecord;
import ru.olekstra.domain.dto.DiscountLimitUsedTotal;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class DiscountLimitUsedToDiscountLimitUsedDtoTest {

    @Autowired
    Mapper mapper;

    @Before
    public void setUp() {
    }    
        
    @Test
    public void map() throws JsonGenerationException,
            JsonMappingException, IOException {
        // Создаем объект, в уже существущем конструкторе заполняются в том
        // числе все маппируемые поля
        DiscountLimitUsed srcTotal = new DiscountLimitUsed();// "hashKey",
                                                        // "rangeKey", 0, 0L,
                                                        // new BigDecimal(0.0),
                                                        // new BigDecimal(0.0));
        //srcTotal.setHashKey(DiscountLimitUsed.buildTotalHashKey("20000101", "cardNum", DiscountLimitUsed.TYPE_TOTAL));
        //srcTotal.setRangeKey(DiscountLimitUsed.buildTotalRangeKey("limitGroupId", "did", EntityProxy.NULL_UUID, "0"));
        
        srcTotal.setHashKey(DiscountLimitUsed.buildTotalHashKey("cardNum", "20000101"));
        srcTotal.setRangeKey(DiscountLimitUsed.buildTotalRangeKey("did", "limitGroupId"));        
        srcTotal.setAveragePackCount(0L);
        srcTotal.setCardNumber("cardNum");
        srcTotal.setDiscountId("did");
        srcTotal.setDiscountSum(new BigDecimal(0.0));
        srcTotal.setLimitGroupId("limitGroupId");
        srcTotal.setPackCount(0);
        srcTotal.setPeriod("20000101");
        srcTotal.setRow(0);
        srcTotal.setSumBeforeDiscount(new BigDecimal(0.0));
        srcTotal.setType(DiscountLimitUsed.TYPE_TOTAL);
        srcTotal.setVoucherId(UUID.fromString(EntityProxy.NULL_UUID));
        
        DiscountLimitUsed srcDetail = new DiscountLimitUsed();
        srcDetail.setHashKey(DiscountLimitUsed.buildHashKey("20000101", "cardNum", DiscountLimitUsed.TYPE_DETAIL));
        srcDetail.setRangeKey(DiscountLimitUsed.buildRangeKey("limitGroupId", "did", UUID.fromString(EntityProxy.NULL_UUID), 1));

        srcDetail.setAveragePackCount(0L);
        srcDetail.setCardNumber("cardNum");
        srcDetail.setDiscountId("did");
        srcDetail.setDiscountSum(new BigDecimal(0.0));
        srcDetail.setLimitGroupId("limitGroupId");
        srcDetail.setPackCount(0);
        srcDetail.setPeriod("20000101");
        srcDetail.setRow(1);
        srcDetail.setSumBeforeDiscount(new BigDecimal(0.0));
        srcDetail.setType(DiscountLimitUsed.TYPE_DETAIL);
        srcDetail.setVoucherId(UUID.fromString(EntityProxy.NULL_UUID));
        
        // Маппируем вперед
        DiscountLimitUsedTotal tgtTotal = mapper.map(srcTotal, DiscountLimitUsedTotal.class);
        DiscountLimitUsedDetail tgtDetail = mapper.map(srcDetail, DiscountLimitUsedDetail.class);
        
     // Маппируем назад
        DiscountLimitUsed itogTotal = mapper.map(tgtTotal, DiscountLimitUsed.class);
        DiscountLimitUsed itogDetail = mapper.map(tgtDetail, DiscountLimitUsed.class);
        
        assertEntity(srcTotal, itogTotal);
        assertEntity(srcDetail, itogDetail);
    }
    
    private void assertEntity(DiscountLimitUsed sorce, DiscountLimitUsed target){
        assertEquals((long) target.getAveragePackCount(), (long) sorce.getAveragePackCount());
        assertEquals(target.getType(), sorce.getType());
        assertEquals(target.getCardNumber(), sorce.getCardNumber());
        assertEquals(target.getDiscountId(), sorce.getDiscountId());
        assertEquals(target.getDiscountSum(), sorce.getDiscountSum());
        assertEquals((long) target.getPackCount(), (long) sorce.getPackCount());
        assertEquals(target.getPeriod(), sorce.getPeriod());
        assertEquals(target.getRow(), sorce.getRow());
        assertEquals(target.getSumBeforeDiscount(), sorce.getSumBeforeDiscount());
        assertEquals(target.getVoucherId(), sorce.getVoucherId());    	
    	
    }

    @Configuration
    static class TestConfig {

        @Bean(name = "org.dozer.Mapper")
        public DozerBeanMapper mapper() {
            DozerBeanMapper result = new org.dozer.DozerBeanMapper();
            result.setMappingFiles(Collections.singletonList("ru/olekstra/common/mappings.xml"));
            return result;
        }
    }

}
