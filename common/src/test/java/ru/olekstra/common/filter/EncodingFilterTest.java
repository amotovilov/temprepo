package ru.olekstra.common.filter;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import ru.olekstra.common.filter.EncodingFilter.ParametersWrapper;
import ru.olekstra.common.filter.EncodingFilter.Util;

@RunWith(MockitoJUnitRunner.class)
public class EncodingFilterTest {

    Filter filter;
    FilterConfig filterConfig;

    @Before
    public void init() {
        filter = new EncodingFilter();
        filterConfig = mock(FilterConfig.class);
    }

    @Test
    public void testInit() throws ServletException {
        Vector<String> params = new Vector<String>();
        params.add("encodings");

        Enumeration<String> en = params.elements();

        when(filterConfig.getInitParameterNames()).thenReturn(en);
        when(filterConfig.getInitParameter("encodings")).thenReturn(
                "US-ASCII, UTF-8, ISO-8859-1, ISO-8859-5");

        filter.init(filterConfig);

        verify(filterConfig).getInitParameterNames();
        verify(filterConfig).getInitParameter("encodings");
    }

    @Test
    public void testDoFilter() throws IOException, ServletException {

        HttpServletRequest arg0 = mock(HttpServletRequest.class);
        HttpServletResponse arg1 = mock(HttpServletResponse.class);
        FilterChain arg2 = mock(FilterChain.class);

        Map<String, String[]> params = new HashMap<String, String[]>();
        params.put("param0", new String[] {"value0", "value1", "value2"});

        when(arg0.getParameterMap()).thenReturn(params);
        filter.doFilter(arg0, arg1, arg2);

        verify(arg0, times(2)).getParameterMap();
    }

    @Test
    public void testDestroy() {
        filter.destroy();
    }

    @Test
    public void testStrictDecoder() {

        CharsetDecoder decoder = Util.strictDecoder(Charset.forName("UTF-8"));
        assertTrue(decoder != null);
        assertTrue(decoder.charset().equals(Charset.forName("UTF-8")));
    }

    @Test
    public void testIoCodePoints() {

        String value = "test";
        int[] result = Util.toCodePoints(value);
        assertTrue(result.length == value.length());
    }

    @Test
    public void testRecode() throws IOException {

        String value = "test";
        String result = null;

        CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder();
        result = Util.recode(value, decoder);

        assertTrue(result != null);
        assertTrue(!result.isEmpty());
    }

    @Test
    public void testEnsureDefinedUnicode() throws IOException {

        String value = new String("test".getBytes("UTF-8"));
        Util.ensureDefinedUnicode(value);
    }

    @Test
    public void testDecode() throws IOException {

        Map<String, String[]> params = new HashMap<String, String[]>();
        params.put("param0", new String[] {"value0", "value1", "value2"});

        List<Charset> charsets = Arrays.asList(Charset.forName("US-ASCII"),
                Charset.forName("UTF-8"),
                Charset.forName("ISO-8859-1"),
                Charset.forName("ISO-8859-5"));

        Map<String, String[]> result = Util.decodeParameters(params, charsets);
        assertTrue(params.size() == result.size());
    }

    @Test
    public void testParametersWrapper() {

        HttpServletRequest request = mock(HttpServletRequest.class);
        Map<String, String[]> params = new HashMap<String, String[]>();
        params.put("param0", new String[] {"value0", "value1", "value2"});

        ParametersWrapper requestWrapper = new ParametersWrapper(request,
                params);

        @SuppressWarnings("unchecked")
        Map<String, String[]> paramsFromWrapper = (Map<String, String[]>) requestWrapper
                .getParameterMap();
        assertTrue(paramsFromWrapper.containsKey("param0"));

        HttpServletRequest requestFromWrapper = (HttpServletRequest) requestWrapper
                .getRequest();
        assertTrue(request.equals(requestFromWrapper));
    }

    @Test
    public void getParameter() {

        HttpServletRequest request = mock(HttpServletRequest.class);
        Map<String, String[]> params = new HashMap<String, String[]>();
        params.put("param0", new String[] {"value0", "value1", "value2"});

        ParametersWrapper requestWrapper = new ParametersWrapper(request,
                params);

        @SuppressWarnings("unchecked")
        Map<String, String[]> paramsFromWrapper = (Map<String, String[]>) requestWrapper
                .getParameterMap();
        assertTrue(paramsFromWrapper.containsKey("param0"));
    }

    @Test
    public void getParameterNames() {

        HttpServletRequest request = mock(HttpServletRequest.class);
        Map<String, String[]> params = new HashMap<String, String[]>();
        List<String> paramNames = new ArrayList<String>();

        String paramName0 = "param0";
        String paramName1 = "param1";
        String paramName2 = "param2";

        params.put(paramName0, new String[] {"value0", "value1", "value2"});
        params.put(paramName1, new String[] {"value0", "value1", "value2"});
        params.put(paramName2, new String[] {"value0", "value1", "value2"});

        paramNames.add(paramName0);
        paramNames.add(paramName1);
        paramNames.add(paramName2);

        ParametersWrapper requestWrapper = new ParametersWrapper(request,
                params);

        @SuppressWarnings("unchecked")
        Enumeration<String> en = requestWrapper.getParameterNames();
        while (en.hasMoreElements()) {
            String value = en.nextElement();
            assertTrue(paramNames.contains(value));
        }
    }

    @Test
    public void getParameterValues() {

        HttpServletRequest request = mock(HttpServletRequest.class);
        Map<String, String[]> params = new HashMap<String, String[]>();
        List<String> paramNames = new ArrayList<String>();
        String[] paramValues = new String[] {"value0", "value1", "value2"};

        String paramName0 = "param0";
        String paramName1 = "param1";
        String paramName2 = "param2";

        params.put(paramName0, paramValues);
        params.put(paramName1, paramValues);
        params.put(paramName2, paramValues);

        paramNames.add(paramName0);
        paramNames.add(paramName1);
        paramNames.add(paramName2);

        ParametersWrapper requestWrapper = new ParametersWrapper(request,
                params);

        @SuppressWarnings("unchecked")
        Enumeration<String> en = requestWrapper.getParameterNames();
        while (en.hasMoreElements()) {
            String name = en.nextElement();
            assertTrue(paramNames.contains(name));
            String[] values = requestWrapper.getParameterValues(name);
            assertTrue(paramValues.equals(values));
        }
    }
}
