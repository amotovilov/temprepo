#parse("WEB-INF/vm/headHtml.vm")

<body>

#parse("WEB-INF/vm/mainMenu.vm")

<div class="container">
    <div class="row">
    	<div class="span6 offset3">
			
			<legend>#springMessage("dialog.recipeReceive")</legend>

			<form class="form-horizontal" id="recipeReceiveForm" action="#springUrl("/recipe/receive/save")" method="POST">
				<fieldset>
					<div class="control-group" id="cg_drugstore">
						<label for="drugstore" class="control-label">#springMessage("label.drugstore")</label>
						<div class="controls">
			                <select class="prettyprint linenums span4" id="drugstoreId" name="drugstoreId">
								<option value=""></option>
								#foreach ($drugstore in $!drugstores)
									<option value="$!drugstore.id">$!drugstore.name</option>
								#end
							</select>
							<span class="help-inline" id="drugstoreError"></span>
						</div>
					</div>
					<div class="control-group" id="cg_period">
						<label class="control-label">#springMessage("label.period")</label>
						<div class="controls">
							<div class="input-daterange">
								<input class="input-small" id="recipePeriod" name="recipePeriod"/>
							</div>
							<span class="help-inline" id="periodError"><span>
						</div>
					</div>
					<div class="control-group" id="cg_file">
						<label class="control-label" for="file">#springMessage("label.file")&nbsp;</label>
						<div class="controls">
							<input type="file" class="input-file" id="file" name="file" accept="image/jpeg,image/png"/>
							<span class="help-inline" id="fileError"><span>
						</div>
					</div>
				</fieldset>
				<div class="form-actions">
                    <button type="button" class="btn btn-primary" onclick="saveRecipe()">
                        <i class="icon-ok icon-white"></i>&nbsp;#springMessage("button.add")
                    </button>
                </div>
    		</form>
    	</div>
    </div>
</div>

<script type="text/javascript">
	
	var recipeReceive = {
   	    msg: {
   	        success: '#springMessage("msg.success")',
   	        error: '#springMessage("msg.error")',
   	        drugstoreNotSet: '#springMessage("msg.recipe.drugstoreNotSet")',
   	        dateNotSet: '#springMessage("msg.recipe.dateNotSet")',
   	        fileNotAttached: '#springMessage("msg.recipe.fileNotAttached")'
   	        
   	    }
   	};
	
#[[ // Turn off Velocity Engine
	
	function showProgressBar(bit) {
   	    $('#recipeReceiveForm').after('<div class="progress progress-striped active"><div class="bar" style="width: '+bit+'%;"></div></div>');
   	};

   	function hideProgressBar() {
   		if ($('div.progress'))
        	$('div.progress').hide();
    };
	
	function initControls() {
   		$('#recipePeriod').datepicker({
	    	changeMonth: true,
	        changeYear: true,
	        showButtonPanel: true,
	        dateFormat: 'mm yy', 
		    onClose: function() {
		        var month = $('#ui-datepicker-div .ui-datepicker-month :selected').val();
		        var year = $('#ui-datepicker-div .ui-datepicker-year :selected').val();
		        $(this).val($.datepicker.formatDate('mm.yy', new Date(year, month, 1)));
		    }
	    });
	    $('#recipePeriod').focus(function () {
	        $('.ui-datepicker-calendar').hide();
		    $('#ui-datepicker-div').position({
	            my: "center top",
	            at: "center bottom",
	            of: $(this)
	        })
	    });
   	};
   	
   	function validateForm() {
   		
   		var result = false;
   		var recipePeriod = $('#recipePeriod').val();
   		$('.alert').remove();
   		
        if ( recipePeriod != '' ) {
            $('#cg_period').removeClass('error');
            $('#periodError').html('');
            result = true;
        } else {
            $('#cg_period').addClass('error');
            $('#periodError').html(recipeReceive.msg.dateNotSet);
            result = false;
        }
        
        if ($('#drugstoreId').val() != '') {
         	$('#cg_drugstore').removeClass('error');
            $('#drugstoreError').html('');
            result = true;
        } else {
        	$('#cg_drugstore').addClass('error');
            $('#drugstoreError').html(recipeReceive.msg.drugstoreNotSet);
            result = false;
        }
        
         if ($('#file').val() != '') {
         	$('#cg_file').removeClass('error');
            $('#fileError').html('');
            result = true;
        } else {
        	$('#cg_file').addClass('error');
            $('#fileError').html(recipeReceive.msg.fileNotAttached);
            result = false;
        }
        
        return result;
    };
   	
   	function clear() {
   		$('#file').val('');
   	};
   	
   	function saveRecipe() {
   		
   		if (validateForm()) {
   			
            if ($('div.alert'))
                $('div.alert').hide();
			
            showProgressBar(50);
			
            $('#recipeReceiveForm').ajaxSubmit( {
                success: function(response, status, xhr, $form) {
	   	            hideProgressBar();
	   	            if (response.success) {
	   	            	console.log('response: ' + response.data);
	   	            	clear();
	   	                $('#recipeReceiveForm').after('<div class="alert alert-success"><a class="close" data-dismiss="alert">&times;</a><span class="badge badge-success">'+recipeReceive.msg.success+'</span> '+response.data+'.</div>');
                    } else {
                    	console.log('response: ' + response.message);
                        $('#recipeReceiveForm').after('<div class="alert alert-error"><a class="close" data-dismiss="alert">&times;</a><span class="badge badge-error">'+recipeReceive.msg.error+'</span> '+response.message+'.</div>');
	   	            }
   	            }
            });
   		}
    };
   	
   	$(document).ready(function() {
    	initControls();
    });
    
]]#
</script>

#parse("WEB-INF/vm/footer.vm")