#parse("WEB-INF/vm/headHtml.vm")

<body>

#parse("WEB-INF/vm/mainMenu.vm")

  <div class="container">

      <h3>#springMessage('dialog.drugstoreDetails')</h3>
      <hr/>

      <div class="row">
        <div class="span5 offset1">
          <form class="form-inline" action="#springUrl('/store/details')" method="GET">
            <div class="control-group" id="cg_searchinput">
	            <input class="string span4" id="searchinput" name="term" type="text" value="$!store.getName()" placeholder="#springMessage('label.drugstoreSearchPlaceholder')" />
	            <input type="hidden" id="storeidinput" name="storeid" value="$!store.getId()" />
	            <input type="hidden" id="periodinput" name="period" value="$!reportperiod" />
	            <input class="btn" id="searchbtn" type="submit" value="#springMessage('button.search')" disabled />
	            <span class="help-inline"></span>
            </div>
          </form>
          <form class="form-inline">
            <label class="selectperiod">#springMessage('label.reportingperiod')
              <select class="span4" id="selectperiod">
                <option value="1">#springMessage('label.thisweek')</option>
                <option value="2">#springMessage('label.previousweek')</option>
                <option value="4">#springMessage('label.thismonth')</option>
                <option value="8">#springMessage('label.previousmonth')</option>
              </select>
            </label>
          </form>
        </div>
        <div class="span5">
          <table class="table">
          <tbody>
            <tr>
              <th>#springMessage('label.name')</th>
              <td id="storename">$!store.getName()</td>
            </tr>
            <tr>
              <th>#springMessage('label.id')</th>
              <td id="storeid">$!store.getId()</td>
            </tr>
            <tr>
              <th>#springMessage('label.timeZoneDrugstore')</th>
              <td id="drugstoretimezone">$!store.getTimeZone()</td>
            </tr>
            <tr>
              <th>#springMessage('label.nonRefundPercent')</th>
              <td id="nonrefundpercent">$!store.getNonRefundPercent()</td>
            </tr>
            <tr>
              <th>#springMessage('label.drugstoreDisabled')</th>
              <td id="drugstoredisabled">
                #if(!$store.getId())
                  &nbsp
                #else
                  #if($store.getDisabled())
                    #springMessage('label.yes')
                  #else
                    #springMessage('label.no')
                  #end
                #end
              </td>
            </tr>
            #if($logonUser.hasRoleDrugstoreEdit())
                <tr>
                  <td colspan="2">
                    <button type="button" class="btn" id="editButton">
                      <i class="icon-edit"></i>&nbsp;#springMessage("button.edit")
                    </button>
                  </td>
                </tr>
            #end
          </tbody>
          </table>

          #if($logonUser.hasRoleDrugstoreView())
          <table class="table table-bordered">
          <tbody>
            <tr>
              <th bgcolor="#dff0d8"></th>
              <td>#springMessage('label.completedTransactionMark')</td>
            </tr>
          </tbody>
          </table>
          #end

        </div>
      </div>  <!-- row  -->
        #if(!$errorMessage)
          #if($logonUser.hasRoleDrugstoreView())
            <div class="row">
                <div class="span12">
                    #set ($pos_count = 1)
                    #foreach ($transaction in $transactions)
                      <table class="table table-striped table-condensed table-bordered">
                          <thead>
                              <tr
                               #if ($transaction.complete)
                                   bgcolor="#dff0d8"
                               #end
                              >
                                <th colspan="11">$!dateFormatter.print($!transaction.date) $!timeFormatter.print($!transaction.startTime) <small>($transaction.voucherId)</small></th>
                            </tr>
                          </thead>
                        <tbody>
                        <!-- OLP-844 -->
                    	#set($showExactSum = false)
                        #if($!transaction.getExactRefundSum() || $!transaction.getExactNonRefundSum() || $!transaction.getExactSumToPay())
                        	#set($showExactSum = true)
                        #end
                        <tr>
                            <td>
                                #springMessage('label.card'):
                                <b>
                                    $!transaction.card.number (
                                    #if($!transaction.card.active)
                                        #springMessage('msg.cardActive')
                                    #else
                                        #springMessage('msg.cardIsNotActive')
                                    #end)
                                </b>,
                                #springMessage('label.cardOwner'): <b>$!transaction.card.owner</b>,
                                #springMessage('label.planName'): <b>$!transaction.card.planName</b>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                $messageFormatter.getMessage('label.checkTotalSum', [$!transaction.getSumWithoutDiscount(), $!transaction.getDiscountSum(), $!transaction.getSumToPay()])
                                #if($showExactSum)
                                	</br>
                                	<span class="muted">$messageFormatter.getExactSumMessage('label.exactSum', [$!transaction.getExactRefundSum(), $!transaction.getExactNonRefundSum(), $!transaction.getExactSumToPay()])</span>
                                #end
                            </th>
                        </tr>
                        #if($transaction.getWarnings() && !$transaction.getWarnings().isEmpty())
                        	<tr>
                                <td class="text-error">
                                    <b>#springMessage('label.transactionWarning')</b>
                                    <ul>
                                        #foreach ($transactionWarning in $transaction.getWarnings())
                                            <li>$!transactionWarning</li>
                                        #end
                                    </ul>
                                </td>
                            </tr>
                        #end
                        #set($limitList = [])
                        #foreach ($dlu in $!transaction.getDiscountLimitUsedPreviousTotal())
                            #if($dlu.getType() == "0")
                                #set($tmp = $limitList.add($messageFormatter.getMessageWithHtml('label.checkLimits', [$!dlu.getLimitGroupId(), $!dlu.getPeriod(), $!dlu.getDiscountId(), $!dlu.getAveragePackCount(), $!dlu.getPackCount(), $!dlu.getDiscountSum(), $!dlu.getSumBeforeDiscount()])))
                            #end
                        #end
                        #if(!$!limitList.isEmpty())
                            <tr>
                                <td>
                                    <b>#springMessage('label.startLimits')</b>
                                    <ol class="limitList">
                                        #foreach ($ltWithHtml in $limitList)
                                            <li>$!ltWithHtml</li>
                                        #end
                                    </ol>
                                </td>
                            </tr>
                        #end
                        <tr>
                            <td><ol>
                        #set($rown = 1)
                        #foreach ($row in $transaction.rows)
                            #set ($processed_sum =  $row.sumWithoutDiscount - $row.discountSum)
                             <li>
                                 $messageFormatter.getMessage('label.transactionPostion', [$row.barcode, $row.name, $row.clientPackId, $row.clientPackName ,$row.quantity, $row.price, $row.sumWithoutDiscount, $row.discountPercent, $row.discountSum, $processed_sum, $row.refundSum, $row.cardLimitUsedSum])
                                 #if($showExactSum)
                                     </br>
                                	 <span class="muted">$messageFormatter.getExactSumMessage('label.exactSum', [$!row.getExactRefundSum(), $!row.getExactNonRefundSum(), $!row.getExactSumToPay()])</span>
                                 #end
                             </li>
                             <ul>
                             #foreach ($packDiscount in $row.getPackDiscount())
                                  <li>
                                    $messageFormatter.getMessage('label.discountPlanPosition', [$packDiscount.getDiscountId(), $packDiscount.getOriginalPercent(), $packDiscount.getOriginalSum(), $packDiscount.getUsedPercent(), $packDiscount.getUsedSum()])
                                    #set($discountLimitList = [])
                                    #foreach ($dlu in $!transaction.getDiscountLimitUsedCurrentDetail())
                                        #if($dlu.getType() == "1" && $dlu.getDiscountId() == $packDiscount.getDiscountId() && $rown == $dlu.getRow())
                                            #set($tmp = $discountLimitList.add($messageFormatter.getMessageWithHtml('label.packDiscountDataLimits', [$!dlu.getLimitGroupId(), $!dlu.getPeriod(), $!dlu.getAveragePackCount(), $!dlu.getPackCount(), $!dlu.getDiscountSum(), $!dlu.getSumBeforeDiscount()])))
                                        #end
                                    #end
                                    #if(!$!$discountLimitList.isEmpty())
                                        <br>
                                        #springMessage('label.usedLimits')
                                        <ul class="limitList">
                                            #foreach ($ltWithHtml in $discountLimitList)
                                                <li>$!ltWithHtml</li>
                                            #end
                                        </ul>
                                    #end
                                 </li>
                             #end
                             #if($row.getDiscountLimitReachedWarnings() && $row.getDiscountLimitReachedWarnings().size() > 0)
                                <li class="text-error">
                                    #springMessage('label.discountLimitReachedWarnings')
                                    <ul class="limitList">
                                    #foreach ($warn in $row.getDiscountLimitReachedWarnings())
                                        <li>$!warn</li>
                                    #end
                                    </ul>
                                </li>
                             #end
                             </ul>
                             #set($rown = $rown + 1)
                        #end
                            </ol></td>
                        </tr>
                        <!-- New Total Used Limits -->
                        #set($dluntList = [])
                        #foreach ($dlu in $!transaction.getDiscountLimitUsedNewTotal())
                            #if($dlu.getType() == "0")
                                #set($tmp = $dluntList.add($messageFormatter.getMessageWithHtml('label.checkLimits', [$!dlu.getLimitGroupId(), $!dlu.getPeriod(), $!dlu.getDiscountId(), $!dlu.getAveragePackCount(), $!dlu.getPackCount(), $!dlu.getDiscountSum(), $!dlu.getSumBeforeDiscount()])))
                            #end
                        #end
                        #if(!$!dluntList.isEmpty())
                            <tr>
                                <td>
                                    <b>#springMessage('label.newTotalLimits')</b>
                                    <ol class="limitList">
                                        #foreach ($ltWithHtml in $dluntList)
                                            <li>$!ltWithHtml</li>
                                        #end
                                    </ol>
                                </td>
                            </tr>
                        #end
                      </tbody>
                    </table>
                    #set ($pos_count = $pos_count + 1)
                    #end
                </div>
            </div>
          #end
        #else
            <div class="alert alert-error">
                <a data-dismiss="alert" class="close">×</a>
                <span class="badge badge-error">#springMessage('msg.error')</span>&nbsp;$errorMessage
            </div>
        #end <!-- row  -->

  </div> <!-- container -->

<script type="text/javascript">

    var url_drugstore_autocomplete = "#springUrl('/drugstore/autocomplete')";
    var url_store_edit = "location.href=\'" + "#springUrl('/store/edit')" + "?storeid=";
    var label_yes = "#springMessage('label.yes')";
    var label_no  = "#springMessage('label.no')";

    $(document).ready(function() {
        $('#searchinput').autocomplete({
            minLength: 3,
            sortResults: false,
            source: url_drugstore_autocomplete,
            focus: function(event, ui) {
                $('#searchinput').val(ui.item.name);
                return false;
            },
            response: function(event, ui) {
		        if (ui.content.length === 0) {
		           $("#cg_searchinput").addClass("error");
		           $("#cg_searchinput .help-inline").text("#springMessage('msg.drugstoreNotFound')");
		        } else {
		           $("#cg_searchinput").removeClass("error");
		           $("#cg_searchinput .help-inline").text("");
		        }
            },
            select: function(event, ui) {
                $('#searchinput').val(ui.item.name);
                $('#storeidinput').val(ui.item.id);
                $('#storename').html(ui.item.name);
                $('#storeid').html(ui.item.id);
                $('#drugstoretimezone').html(ui.item.timeZone);
                $('#nonrefundpercent').html(ui.item.nonRefundPercent);
                if(ui.item.disabled) {
                    $('#drugstoredisabled').html(label_yes);
                } else {
                    $('#drugstoredisabled').html(label_no);
                };
                $('#editButton').attr('onclick', url_store_edit + ui.item.id + "\'" );
                $("#searchbtn")[0].disabled = false;
                
                return false;
            }
        }).data( "autocomplete" )._renderItem = function( ul, item ) {
            ul.addClass('dropdown-menu'); // override styles by the bootstraps
            return $( "<li></li>" )
                .data( "item.autocomplete", item )
                .append( $('<a></a>')
                    .append( item.name ) )
                .appendTo( ul );
        };
        
        $('#selectperiod').change(function() {
            var period = $('#selectperiod option:selected').val();
            $('#periodinput').val(period);
        });

        var depth = $('#periodinput').val();
        $("#selectperiod option").each(function() {
            if($(this).val() == depth) {
                $(this).attr("selected","selected");
            }
        });

        var storeId = $('#storeidinput').val();
        if(storeId != '') {
            $('#editButton').attr('onclick', url_store_edit + storeId + "\'" );
        }

    });

</script>

#parse("WEB-INF/vm/footer.vm")
