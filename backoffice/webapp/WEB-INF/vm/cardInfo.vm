#parse("WEB-INF/vm/headHtml.vm")

<body>

  <style type="text/css">
    .table th, table td { padding: 8px 20px; }
    .tabbable { padding-top: 20px; }
    #cardInfoRow { margin-bottom: 20px; }
    #cardInfo .btn { width: 90px; }
    .help-inline { margin-top: 10px; }
    #favouriteCardsTable tbody tr { cursor: pointer; }
  </style>

#parse("WEB-INF/vm/mainMenu.vm")

  <div class="container">
    <div class="row" id="cardInfoRow">
      <div class="span6">
        <h4>#springMessage("dialog.cardSearch")</h4>
        <form class="form-search" action="#springUrl('/card/details')" method="GET">
            <div class="well">
              <input type="text" class="input-large search-query" name="term" id="term" value="$!searchterm" />
              <input type="submit" class="btn" id="searchbutton" value="#springMessage('button.find')" />
              <span class="help-inline error">$!notfound</span>
            </div>
        </form>
      </div>
      <div class="span6">
        <h4>#springMessage("label.favouriteCards")</h4>
        <table class="table table-condensed  table-hover" id="favouriteCardsTable">
          <tbody>
             #foreach ($card in $favouriteCardsData)
               <tr data-cardnumber="$card.getNumber()">
                   <td><a href="#springUrl('/card/details')?term=$card.getNumber()">$card.getNumber()</a></td>
                   <td>$card.getHolderName()</td>
                   <td>$card.getDiscountPlanId()</td>
               </tr>
             #end
          </tbody>
        </table>
      </div>
      <div class="span6 offset3">
        <h4>#springMessage("dialog.cardDetails")</h4>
	    <table class="table" id="cardInfo">
	        <tbody>
	          <tr>
	            <th>#springMessage("label.number")</th>
	            <td id="cardNumber">$!card.getNumber()</td>
	          </tr>
	            #if($logonUser.hasRoleAdministration())
	          <tr>
	            <th>#springMessage("label.Cvv")</th>
	            <td>$!card.getCvv()</td>
	          </tr>
	            #end
	          <tr>
	            <th>#springMessage("label.phone")</th>
	            <td>
	                $!card.getTelephoneInFormattedView()
	                #if($card && $logonUser.hasRoleCardGeneral())
	                    #if($!card.isPersonalized())
	                        <button class="btn btn-mini pull-right" type="button" data-toggle="modal" data-target="#modalCardEditBox">#springMessage('label.change')</button>
	                    #else
	                        <button class="btn btn-mini pull-right" disabled="disabled" type="button" data-toggle="modal" data-target="#modalCardEditBox">#springMessage('label.change')</button>
	                    #end
	                #end
	            </td>
	          </tr>
	          <tr>
	            <th>#springMessage("label.holder")</th>
	            <td>
	                $!card.getHolderName()
	                #if($card && $logonUser.hasRoleCardGeneral())
	                    #if($!card.isPersonalized())
	                        <button class="btn btn-mini pull-right" type="button" data-toggle="modal" data-target="#modalCardEditBox">#springMessage('label.change')</button>
	                    #else
	                        <button class="btn btn-mini pull-right" disabled="disabled" type="button" data-toggle="modal" data-target="#modalCardEditBox">#springMessage('label.change')</button>
	                    #end
	                #end
	            </td>
	          </tr>  
	          <tr>
	            <th>#springMessage("label.active")</th>
	            <td>
                    <form class="form-inline" action="#springUrl("/card/activation")" method="GET">
                        <input type="hidden" name="cardnum" value="$!card.getNumber()" />
                        <input type="hidden" name="discountplan" value="$!card.getDiscountPlanId()" />
                        <input type="hidden" name="holder" value="$!card.getHolderName()" />
                        <input type="hidden" name="startDate" value="$!activationFormatter.print($card.getStartDate())" />
                        <input type="hidden" name="endDate" value="$!activationFormatter.print($card.getEndDate())" />
		                #if($card && $card.isActive())
		                    #springMessage("msg.yes")
		                #elseif($card && !$card.isActive())
		                    #springMessage("msg.no")
		                #end
	                    #if($card && $logonUser.hasRoleCardGeneral())
	                        #if($!card.isPersonalized())
	                            <button class="btn btn-mini btn-primary pull-right disabled" disabled="disabled" id="activatebutton">#springMessage('button.activate')</button>
		                    #else
	                            <button class="btn btn-mini btn-primary pull-right" id="activatebutton">#springMessage('button.activate')</button>
	                        #end
	                    #end
                    </form>
	            </td>
	          </tr>
	          <tr>
	            <th>#springMessage("label.isblocked")</th>
	            <td>
		            #if($card && $card.getDisabled())
		                #springMessage("msg.yes")
		                <button class="btn btn-mini pull-right" data-toggle="modal" data-target="#modalDisableCard" id="lockbutton">#springMessage('button.unlock')</button>
		            #elseif($card && !$card.getDisabled()) 
		                #springMessage("msg.no")
		                <button class="btn btn-mini pull-right" data-toggle="modal" data-target="#modalDisableCard" id="lockbutton">#springMessage('button.lock')</button>                    
		            #end
	            </td>
	          </tr>
	          <tr>
	            <th>#springMessage("label.tariffPlan")</th>
	            <td>$!discountplan</td>
	          </tr>
	          <tr>
	            <th>#springMessage("label.limitRemain")</th>
	            <td>$!card.getLimitRemain()</td>
	          </tr>  
	          <tr>
	            <th>#springMessage("label.validityperiod")</th>
	            <td>
		            #if($card)
			            #if(!$card.getStartDate() && !$card.getEndDate())
			              #springMessage("label.unlimited")
			            #else
			              $!formatter.print($card.getStartDate()) - $!formatter.print($card.getEndDate())
			            #end
		            #end
	            </td>
	          </tr>
	          <tr>
	            <th>#springMessage("label.cardBatches")</th>
	            <td id="cardBatch">
	            	#foreach( $batch in $cardBatches )
	              		[$batch]
	              	#end
	                #if($card && $logonUser.hasRoleCardGeneral())
	                    <button class="btn btn-mini pull-right" type="button" data-toggle="modal" data-target="#modalListBox">#springMessage("label.change")</button>
	                #end
	            </td>
	          </tr>  
	          <tr>
	          	<th></th>
	          	<td></td>
	          </tr>
	        </tbody>
	    </table>
        <!-- Modal Batch Dialog -->
		<div class="modal hide fade" id="modalListBox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <form id="cardBatchForm" class="form-horizontal">
                <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				    <h3 id="myModalLabel">#springMessage("dialog.batchEditTitle")</h3>
				    <label id="responselabel"></label>
				</div>
				<div class="modal-body">
				    <div class="span4 row">
						<div class="span1 offset1">
							<b>#springMessage("label.cardBatches")</b><br/>
				           <select multiple="multiple" id='lstBox1'>
				           		#foreach( $batch in $cardBatches )
				              		<option value="$batch">$batch</option>
				              	#end
				        	</select>
				        </div>
			        </div>
			        <div class="span4 row">
				        <div class="span1 offset2">
				        	<input type='button' id='btnRight' value ='  &darr;  '/>
				        	<input type='button' id='btnLeft' value ='  &uarr;  '/>
			        	</div>
		        	</div>
		        	<div class="span4 row">
			        	<div class="span1 offset1">
			        		<b>#springMessage("label.availableBatches")</b><br/>
					        <select multiple="multiple" id='lstBox2'>
					        	#foreach ( $batch in $allBatches )
					        		<option value="$batch">$batch</option>
					        	#end
					        </select>
			        	</div>
					</div>
				</div>
				<div class="modal-footer">
				    <div id="reports">
			
				    </div>
				    <input type="submit" class="btn btn-primary" value="#springMessage("button.save")" />
				    <button class="btn" data-dismiss="modal" aria-hidden="true">#springMessage("button.cancel")</button>
			    </div>
            </form>
		</div>
		<!--End of Modal Batch Dialog-->
		
		<!-- Modal CardEdit Dialog -->
		<div class="modal hide fade" id="modalCardEditBox" tabindex="-1" role="dialog" aria-labelledby="cardEditLabel" aria-hidden="true">
            <form id="cardEditForm" class="form-horizontal">
			    <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				    <h3 id="cardEditLabel">#springMessage("dialog.editCardOwner")</h3>
				    <div id="cg_editowneresponse" class="control-group">
					    <label id="cardEditResponse"></label>
				    </div>
			    </div>
			    <div class="modal-body">
   				    <fieldset>
   					    <div id="cg_ownername" class="control-group">
						    <label for="ownerName" class="control-label">#springMessage("label.cardOwner")</label>
						    <div class="controls">
							    <input type="text" value="$!card.getHolderName()" id="ownerName" name="ownerName" class="string span3">
							    <span id="ownerNameError" class="help-inline"></span>
	                	    </div>
					    </div>
					    <div id="cg_ownerphone" class="control-group">
						    <label for="ownerPhone" class="control-label">#springMessage("label.phone")</label>
						    <div class="controls">
							    <input type="text" value="$!card.getTelephoneNumber()" id="ownerPhone" name="ownerPhone" class="string span3">
							    <span id="ownerPhoneError" class="help-inline"></span>
		                    </div>
					    </div>
				    </fieldset>
			    </div>
			    <div class="modal-footer">
				    <input type="submit" class="btn btn-primary" value="#springMessage("button.save")" />
				    <button class="btn" data-dismiss="modal" aria-hidden="true">#springMessage("button.cancel")</button>
			    </div>
            </form>
		</div>
		<!--End of Modal CardEdit Dialog-->

        <!-- Modal DisableCard Dialog -->
        <div class="modal hide fade" id="modalDisableCard" tabindex="-1" role="dialog" aria-labelledby="lockCardLabel" aria-hidden="true">
            <form id="cardDisableForm" class="form-horizontal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="lockCardLabel">
                        #if($card.getDisabled())
                            #springMessage("dialog.cardEnable")
                        #else
                            #springMessage("dialog.cardDisable")
                        #end                    
                    </h3>                    
                    <div id="cg_lockcardresponse" class="control-group">
                        <label id="lockCardResponse"></label>
                    </div>
                </div>
                <div class="modal-body">
                    <fieldset>
                        <div id="cg_lockreason" class="control-group">
                            <label for="lockReason" class="control-label">
                                #if($card.getDisabled())
                                    #springMessage("label.unlockReason")
                                #else
                                    #springMessage("label.lockReason")
                                #end
                            </label>
                            <div class="controls">
                                <textarea rows="3" id="lockReason" name="lockReason"></textarea>
                                <span id="lockReasonError" class="help-inline"></span>
                            </div>
                        </div>
                        <div id="cg_jiratask" class="control-group">
                            <label for="jiraTask" class="control-label">#springMessage("label.jiraTask")</label>
                            <div class="controls">
                                <input type="text" id="jiraTask" name="jiraTask" class="string span3">
                                <span id="jiraTaskError" class="help-inline"></span>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="#springMessage("button.save")" />
                    <button class="btn" data-dismiss="modal" aria-hidden="true">#springMessage("button.cancel")</button>
                </div>
            </form>
        </div>
        <!--End of Modal DisableCard Dialog-->
        		
        <!-- Create ticket button-->
		<div class="form-actions">
		    #if ($card && $card.getNumber())
		         #set ($cardNumberSuffix = "?cardNumber=" + $card.getNumber())
		    #else
		         #set ($cardNumberSuffix = "")
		    #end
            <a class="btn btn-block btn-primary" href="#springUrl('/helpdesk/ticket/create')$!cardNumberSuffix">#springMessage("label.helpdesk.button.text.createMiscTicket")</a>
            <span class="help-inline"><small>#springMessage("label.helpdesk.button.helpInline.createMiscTicket")</small></span>
		</div>
		
    </div>
    </div> <!--/row -->
    
    #if($logonUser.hasRoleCardView())
        <div class="tabbable">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#cardOperations" data-toggle="tab">#springMessage("dialog.cardOperations")</a></li>
                <li><a href="#remainingLimits" data-toggle="tab">#springMessage("dialog.cardLimitsRemaining")</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="cardOperations">
			        <!-- Card operations history-->
				    <div class="row">
				        <div class="span12 offset0">
				            <legend>#springMessage("dialog.cardOperations")</legend>
				            <table class="table table-bordered table-condensed">
				                <tbody>
				                    <tr>
				                        <th class="label-info"></th>
				                        <td>#springMessage("label.transactionComeplted")</td>
				                        <th class="label-warning"></th>
				                        <td>#springMessage("label.transactionReversed")</td>
				                        <th></th>
				                        <td class="muted">#springMessage("label.internalOperations")</td>
				                    </tr>
				                </tbody>
				            </table>
				            <input id="prevPeriod" type="hidden" value="$!prevPeriod" />
				            <table class="table table-condensed  table-hover" id="cardRecordsTable">
				                <thead>
				                    <tr>
				                        <th>#springMessage("label.datetime")</th>
				                        <th>#springMessage("label.operationType")</th>
				                        <th>#springMessage("label.drugstore")</th>
				                        <th>#springMessage("label.note")</th>
				                    </tr>
				                </thead>
				                #if ($!logRecordList && $logRecordList.size() > 0)
				                    <tbody>
				                        #parse("WEB-INF/vm/cardRecords.vm")
				                    </tbody>
				                #else
				                    <tbody>
				                        <tr>
				                            #if($!showInfo)
				                                <td colspan="6">
				                                    <span class="badge badge-error">#springMessage("label.noStatsOnCard") $!periodOutputString</span>
				                                </td>
				                            #end
				                        </tr>                   
				                    </tbody>
				                #end
				            </table>
				            <button type="button" class="btn btn-primary" id="showPrevMonth" 
				                #if(!$card) 
				                    disabled="disabled" 
				                #end >
				                #springMessage('button.showPrevMonth')
				            </button>
				        </div>
				    </div>
    		        <!-- End of history -->
		        </div>
                <div class="tab-pane" id="remainingLimits">
			        <!-- Limits remaining -->
			        <div class="row">
			            <div class="span12 offset0">
				            <legend>#springMessage("dialog.cardLimitsRemaining")</legend>
			                <table class="table table-condensed  table-hover" id="cardRecordsTable">
			                    <thead>
			                        <tr>
			                            <th>#springMessage("label.period")</th>
			                            <th>#springMessage("label.limitGr")</th>
			                            <th>#springMessage("label.remainingAveragePackCount")</th>
			                            <th>#springMessage("label.remainingDiscountSum")</th>
			                            <th>#springMessage("label.remainingPackCount")</th>
			                            <th>#springMessage("label.remainingSumBeforeDiscount")</th>
			                        </tr>
			                    </thead>
			                    #if ($!limitsByDiscount && $limitsByDiscount.size() > 0)
			                        <tbody>
			                            #parse("WEB-INF/vm/remainingLimits.vm")
			                        </tbody>
			                    #else
			                        <tbody>
			                            <tr>
			                                #if($!showInfo)
			                                    <td colspan="7">
			                                        <span class="badge badge-error">#springMessage("label.noRemainingLimitsOnCard")</span>
			                                    </td>
			                                #end
			                            </tr>                   
			                        </tbody>
			                    #end
			                </table>
			            </div>
			        </div>
		            <!-- End of limits remaining -->
                </div>
            </div>
        </div><!-- End of tab-content -->
    #end
    
  </div> <!-- /container -->
  
<script type="text/javascript">

	var cardInfo = {
   	    msg: {
   	        process_query: '#springMessage("msg.processquery")',
   	        success: '#springMessage("msg.success")',
   	        error: '#springMessage("msg.error")',
   	        save_error: '#springMessage("msg.saveError")',
   	        save_success: '#springMessage("msg.saved")',
   	        owner_name_error: '#springMessage("msg.fieldIsEmpty")',
   	        owner_phone_error: '#springMessage("msg.phoneNotValid")',
   	        no_records: '#springMessage("label.noStatsOnCard")',
   	        lock_reason_error: '#springMessage("msg.fieldIsEmpty")'
   	    }
   	};
	
	function saveBatches() {
		$('#reports').html('');
		var opts = $.map($('#lstBox1 option'), function(a){return a.value;}).join(',');
		var cardNumber = '';
		#if($!card.getNumber())
			cardNumber = $!card.getNumber();
		#end
		console.log(opts);
		$('#responselabel').html(cardInfo.msg.process_query);
		$.ajax({
			type: "POST",
			url: "#springUrl('/card/batches/save')",
			data: { cardnum: cardNumber, cardBatches: opts },
			success: function (data, textStatus) { // вешаем свой обработчик на функцию success
				console.log(data);
				var text = "";
				for(var i=0; i<data.data.length; i++) {
					text = text + "[" + data.data[i] + "]";
				}
				$('#cardBatch').text(text);
			
				$('#modalListBox').modal('hide');
				window.location.reload();
			}
		});
	};

	function saveOwnerInfo() {
		var cardNumber = '';
		#if($!card.getNumber())
			cardNumber = $!card.getNumber();
		#end
		
		$('#cg_editowneresponse').removeClass('error');
		$('#cardEditResponse').html('');
		
		var check = true;
		if ( $('#ownerName').val() != '' ) {
            $("#cg_ownername").removeClass('error');
            $("#ownerNameError").html('');
        } else {
            $("#cg_ownername").addClass('error');
            $("#ownerNameError").html(cardInfo.msg.owner_name_error);
            check = false;
        }
        
        var phone = $('#ownerPhone').val().replace(/\s+/g, '');
		if ( /^\d{10}$/.test(phone) ) {
            $("#cg_ownerphone").removeClass('error');
            $("#ownerPhoneError").html('');
        } else {
            $("#cg_ownerphone").addClass('error');
            $("#ownerPhoneError").html(cardInfo.msg.owner_phone_error);
            check = false;
        }
		
		if (check) {
			$('#cardEditResponse').html(cardInfo.msg.process_query);
			$.ajax({
				type: "POST",
				url: "#springUrl('/card/owner/update')",
				data: { cardNumber: cardNumber, ownerName: $('#ownerName').val(), ownerPhone: phone},
				success: function (data, textStatus) { 
					console.log(data);
					if (data.success == true) {
						$('#cardEditResponse').html(data.message);
						$('#modalCardEditBox').modal('hide');
						window.location.reload();
					} else {
					    $('#cg_editowneresponse').addClass('error');
						$('#cardEditResponse').html(data.message);
					}
    	  		}
			});
		}
	};
	
	function disableCard() {
	    var check = true;
	    var cardNumber = '';
        #if($!card.getNumber())
            cardNumber = $!card.getNumber();
        #end
	    
        if ( $('#lockReason').val() != '' ) {
            $("#cg_lockreason").removeClass('error');
            $("#lockReasonError").html('');
        } else {
            $("#cg_lockreason").addClass('error');
            $("#lockReasonError").html(cardInfo.msg.lock_reason_error);
            check = false;
        }
        if (cardNumber == "") {
            check = false;
        }
        
        if (check) {
	        $.ajax({
	            type: "POST",
	            url: "#springUrl('/card/lock')",
	            data: {
                   #if($card)
    	               cardNumber: cardNumber,
	               #end
	               lockReason: $('#lockReason').val(),
	               jiraTask: $("#jiraTask").val(),
                   #if($!card)
	                   disable: !$!card.getDisabled()
                   #end
                },
	            success: function (data, textStatus) { 
	                console.log(data);
	                if (data.success == true) {
	                    $('#lockCardResponse').html(data.message);
	                    $('#modalDisableCard').modal('hide');
	                    window.location.reload();
	                } else {
	                    $('#cg_lockcardresponse').addClass('error');
	                    $('#lockCardResponse').html(data.message);
	                }
	            } 
	        });
        }
	}

	function getForPreviousMonth() {
		$.ajax({
			type: "GET",
			url: "#springUrl('/card/records')",
			data: { period: $('#prevPeriod').val(), card: $("#cardInfo #cardNumber").text() },
			success: function (data, textStatus, prevPeriod) {	        
				$('#prevPeriod').val(data.prevPeriod);
				if (data.cardRecords != '') {
					$('#cardRecordsTable > tbody:last').append(data.cardRecords); 
				} else {
					$('#cardRecordsTable > tbody:last').append('<tr><td colspan="6"><span class="badge badge-error">' + cardInfo.msg.no_records + ' ' + data.periodOutputString + '</span></td></tr>');
				}
			} 
		});
	};
	
	$(document).ready(function() {
		$('#reports').html('');
	    $('#btnRight').click(function(e) {
	        var selectedOpts = $('#lstBox1 option:selected');
	        if (selectedOpts.length == 0) {
	            e.preventDefault();
	        }
	
	        $('#lstBox2').append($(selectedOpts).clone());
	        $(selectedOpts).remove();
	        e.preventDefault();
	    });
	
	    $('#btnLeft').click(function(e) {
	        var selectedOpts = $('#lstBox2 option:selected');
	        if (selectedOpts.length == 0) {
	            e.preventDefault();
	        }
	
	        $('#lstBox1').append($(selectedOpts).clone());
	        $(selectedOpts).remove();
	        e.preventDefault();
	    });
	    
        $('#cardDisableForm').submit(function(e){
            e.preventDefault();
			disableCard();
		});		
	    
        $('#cardEditForm').submit(function(e){
            e.preventDefault();
            saveOwnerInfo();
        });		
	    
        $('#cardBatchForm').submit(function(e){
            e.preventDefault();
			saveBatches();
        });		
	    
        $('#showPrevMonth').click(getForPreviousMonth);
        
        $('#favouriteCardsTable tbody tr').click(function () {
            window.location.href = "#springUrl('/card/details')?term=" + $(this).data("cardnumber");
        });
	});
</script>

#parse("WEB-INF/vm/footer.vm")
