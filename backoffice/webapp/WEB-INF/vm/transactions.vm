#parse("WEB-INF/vm/headHtml.vm")

<body>

#parse("WEB-INF/vm/mainMenu.vm")

<div class="container">
    <div class="row">
    	<div class="span8 offset2">
    		<legend>#springMessage("dialog.cardTransactions")</legend>
    		<form id="transactionsForm" name="transactionsForm" class="form-horizontal" method="get" action="#springUrl('/transactions/completed/generate')"">
   				<fieldset>
   					<div class="control-group" id="cg_period">
   						<label class="control-label">#springMessage("label.period")</label>
		    			<div class="controls">
                            <div class="input-daterange">
                                <input class="input-small" id="transactionfromdate" name="dateFrom"/>
                                <span class="add-on"> - </span>
                                <input class="input-small" id="transactiontodate" name="dateTo"/>
                            </div>
                            <span class="help-inline" id="invalidDateRange"><span>
                        </div>
		    		</div>
		    		<br/>
		            <div class="control-group">
						<label class="control-label">#springMessage("label.reportFileType")</label>
		                <div class="controls">
		                    <label class="radio">
		                        <input type="radio" name="filetyperadios" id="csvfileradio" value="csvfile" checked>
		                        #springMessage("label.csvFileType") 
		                    </label>
		                    <label class="radio">
		                        <input type="radio" name="filetyperadios" id="xmlfileradio" value="xmlfile">
		                        #springMessage("label.xmlFileType")
		                    </label>
		                </div>
		            </div>
		    		
					<div class="control-group">
						<br/>
						<div id="file-to-download" class="controls">
		    				<!-- link to generated transaction report -->
		    			</div>
					</div>
		    		
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">
                            <i class="icon-list-alt icon-white"></i>&nbsp;#springMessage("button.export")
                        </button>
                        <button type="button" class="btn" onclick="resetForm()">
                            <i class="icon-trash"></i>&nbsp;#springMessage("button.clear")
                        </button>
                    </div>
				</fieldset>
    		</form>    		
		</div>
	</div>
</div>

<script type="text/javascript">

    var error_msg_datesNotValid = '#springMessage("msg.datesNotValid")';
	var transactions = {
   	    msg: {
   	        success: '#springMessage("msg.success")',
   	        error: '#springMessage("msg.error")'
   	    },
   	    label: {
   	    	fileLink: '#springMessage("msg.transactionsDownload")'
   	    }
   	};
	
#[[ // Turn off Velocity Engine
   	
   	function showProgressBar(bit) {
   	    $('#transactionsForm').after('<div class="progress progress-striped active"><div class="bar" style="width: '+bit+'%;"></div></div>');
   	}

   	function hideProgressBar() {
   		if ($('div.progress'))
        	$('div.progress').hide();
    }
    
    function resetForm() {
   	 	$('#transactionsForm')[0].reset();
   	 	$('#file-to-download').html('');
   	 	hideProgressBar();
   	 	if ($('div.alert'))
			$('div.alert').hide();
   	}
   	
   	function initControls() {
   		$('#transactionfromdate').datepicker({
	    	changeMonth: true,
	        changeYear: true,
	        showButtonPanel: true,
	        dateFormat: 'mm yy', 
		    onClose: function() {
		        var month = $('#ui-datepicker-div .ui-datepicker-month :selected').val();
		        var year = $('#ui-datepicker-div .ui-datepicker-year :selected').val();
		        $(this).val($.datepicker.formatDate('mm.yy', new Date(year, month, 1)));
		    }
	    })
	    $('#transactiontodate').datepicker({
	        changeMonth: true,
	        changeYear: true,
	        showButtonPanel: true,
	        dateFormat: 'mm yy', 
	        onClose: function() {
		        var month = $('#ui-datepicker-div .ui-datepicker-month :selected').val();
		        var year = $('#ui-datepicker-div .ui-datepicker-year :selected').val();
		        $(this).val($.datepicker.formatDate('mm.yy', new Date(year, month, 1)));
		    }
	    })
	    $('#transactionfromdate').focus(function () {
	        $('.ui-datepicker-calendar').hide();
		    $('#ui-datepicker-div').position({
	            my: "center top",
	            at: "center bottom",
	            of: $(this)
	        })
	    })
	    $('#transactiontodate').focus(function () {
	        $('.ui-datepicker-calendar').hide();
		    $('#ui-datepicker-div').position({
	            my: "center top",
	            at: "center bottom",
	            of: $(this)
	        })
	    })
   	}
   	
   	function validateForm(){
  	  var success = true;
  	  var fromdate = $('#transactionfromdate').val();
  	  var todate = $('#transactiontodate').val();
  	  
  	  //  -- проверяем, что даты введены обе
        if ( fromdate == '' && todate == '' ) {
            $('#cg_period').removeClass('error');
            $('#invalidDateRange').html('');
        }
        else if ( fromdate != '' && todate != '' ) {
            $('#cg_period').removeClass('error');
            $('#invalidDateRange').html('');
        }
        else {
            $('#cg_period').addClass('error');
            $('#invalidDateRange').html(error_msg_datesNotValid);
            success = false;
        }    
        return success;
    };
 	   	
   	$("#transactionsForm").on("submit", function (e) {
   		
   		if (validateForm()) {
   	
            if ($('div.alert'))
                $('div.alert').hide();

            showProgressBar(50);

            $('#transactionsForm').ajaxSubmit( {
                success: function(response, status, xhr, $form) {
	   	            console.log('response: ' + response.message);
	   	            hideProgressBar();
	   	            if (response.success) {
	   	                $('#transactionsForm').after('<div class="alert alert-success"><a class="close" data-dismiss="alert">&times;</a><span class="badge badge-success">'+transactions.msg.success+'</span> '+response.message+'.</div>');
                    } else {
                        $('#transactionsForm').after('<div class="alert alert-error"><a class="close" data-dismiss="alert">&times;</a><span class="badge badge-error">'+transactions.msg.error+'</span> '+response.message+'.</div>');
	   	            }

	   	            if (response.data) {
                        $('#file-to-download').html('<a href="'+response.data+'">'+transactions.label.fileLink+'</a>');
                    } else {
                        $('#file-to-download').html('');
                    }
   	            }
            });
   		}
   		e.preventDefault();
    });
    
    $(document).ready(function() {
    	initControls();
    });
    
]]#    
</script>

#parse("WEB-INF/vm/footer.vm")