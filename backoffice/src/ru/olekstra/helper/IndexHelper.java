package ru.olekstra.helper;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.domain.IndexEntity;

@Repository
public class IndexHelper {

    private DynamodbService service;

    @Autowired
    public IndexHelper(DynamodbService service) {
        this.service = service;
    }

    public boolean addNewRecord(IndexEntity entity) throws ItemSizeLimitExceededException {
        return service.putObject(entity);
    }

    public Map<String, Object> getIndex(String index, String prefix) {
        Map<String, Object> result = service.getAttributes(
                IndexEntity.TABLE_NAME, index, prefix, null);
        return result;
    }

    public void deleteIndex(String index, String prefix) {
        service.deleteItem(IndexEntity.TABLE_NAME, index, prefix);
    }
}
