package ru.olekstra.helper;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ru.olekstra.awsutils.BatchOperationResult;
import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.domain.Recipe;
import ru.olekstra.domain.RecipeState;
import ru.olekstra.service.RecipeService;

import com.amazonaws.services.dynamodb.model.Key;
import com.amazonaws.services.dynamodb.model.ProvisionedThroughputExceededException;

@Repository
public class RecipeHelper {

    private DynamodbService service;

    @Autowired
    public RecipeHelper(DynamodbService service) {
        this.service = service;
    }

    public boolean addRecipe(Recipe recipe)
            throws ItemSizeLimitExceededException {
        return service.putObject(recipe);
    }

    public void deleteRecipeState(String state, String period, String uuid) {
        service.deleteItem(RecipeState.TABLE_NAME, state, RecipeState
                .buildRangeKey(period, uuid));
    }

    public boolean addRecipeState(RecipeState recipeState)
            throws ItemSizeLimitExceededException {
        return service.putObject(recipeState);
    }

    public RecipeState getRecipeState(String state, String period, String uuid) {
        return service.getObject(RecipeState.class, state, RecipeState
                .buildRangeKey(period, uuid));
    }

    public boolean saveHistory(String period, String uuid, String note)
            throws ItemSizeLimitExceededException {
        return service.appendAttribute(Recipe.TABLE_NAME, period, uuid,
                Recipe.FLD_HISTORY, note);
    }

    public boolean saveRecipeAuthcode(String period, String uuid,
            String authcode) {
        return service.updateAttribute(Recipe.TABLE_NAME, period, uuid,
                Recipe.FLD_AUTH_CODE, authcode);
    }

    public boolean updateRecipeState(String period, String uuid,
            String oldState, String newState) {
        return service.updateAttributeWithCondition(Recipe.TABLE_NAME, period,
                uuid, Recipe.FLD_STATE, oldState, newState);
    }

    public Map<String, Object> getNextRecipeWithStateX(String state,
            Key lastKeyEvaluated) {
        return service.getFirstEntryWithSpecificAtributeValues(
                RecipeState.TABLE_NAME, state, lastKeyEvaluated, null);
    }

    public boolean saveOperatorAndTime(Map<String, Object> recipeStateMap,
            String operatorName) throws IOException {
        RecipeState recipeState = new RecipeState(recipeStateMap);
        String expectedOperator = (String) recipeStateMap
                .get(RecipeState.FLD_PROCESSING_BY);
        recipeState.setProcessingBy(operatorName);
        return service
                .putObjectWithCondition(recipeState,
                        RecipeState.FLD_PROCESSING_BY,
                        expectedOperator);
    }

    public Recipe getRecipe(String period, String uuid) {
        return service.getObject(Recipe.class, period, uuid);
    }

    public boolean saveRecipeRejectReason(String period, String uuid,
            String rejectReason) {
        return service.updateAttribute(Recipe.TABLE_NAME, period, uuid,
                Recipe.FLD_REJECT_REASON, rejectReason);
    }

    /**
     * Возвращает список рецептов, находящихся в работе (статусы new, parsed,
     * problem)
     * 
     * @param period
     * @return список рецептов
     * @throws IOException
     * @throws ProvisionedThroughputExceededException
     */
    public List<RecipeState> getActiveRecipesForPeriod(String period) throws ProvisionedThroughputExceededException,
            IOException {

        List<RecipeState> recipeList = service.getItemsByRangeKeyBegin(
                RecipeState.class, Recipe.STATE_NEW, period);
        recipeList.addAll(service.getItemsByRangeKeyBegin(RecipeState.class,
                Recipe.STATE_PARSED, period));
        recipeList.addAll(service.getItemsByRangeKeyBegin(RecipeState.class,
                Recipe.STATE_PROBLEM, period));
        return recipeList;
    }

    public List<Recipe> getRecipesForPeriod(String period,
            List<String> s3RecipeFiles) throws RuntimeException, IOException {

        Set<String> recipeFiles = new HashSet<String>(s3RecipeFiles);
        String[] recipesUUID = new String[recipeFiles.size()];

        int index = 0;
        for (String drugstoreRecipeFile : recipeFiles) {
            recipesUUID[index] = drugstoreRecipeFile
                    .split(RecipeService.IMAGE_FILE_FORMAT_DELIMETER)[0];
            index++;
        }

        String[] recipesPeriod = new String[recipeFiles.size()];
        Arrays.fill(recipesPeriod, period);

        BatchOperationResult<Recipe> recipeResult = service.getObjects(
                Recipe.class, recipesPeriod, recipesUUID);

        return recipeResult.getSuccessList();
    }
}