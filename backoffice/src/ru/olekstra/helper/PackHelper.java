package ru.olekstra.helper;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ru.olekstra.awsutils.ComplexKey;
import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.dynamodb.TableManagement;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.domain.Pack;
import ru.olekstra.service.PackService;

import com.amazonaws.AmazonServiceException;

@Repository
public class PackHelper {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(PackService.class);

    private DynamodbService service;
    private TableManagement table;
    private AppSettingsService settings;

    @Autowired
    public PackHelper(DynamodbService service, TableManagement table,
            AppSettingsService settings) {
        this.service = service;
        this.table = table;
        this.settings = settings;
    }

    /**
     * Check pack table. Create table if it doesn't exist.
     * 
     * @param tableName table name
     * @return {@code True} when tables exist, else - {@code False}
     */
    public boolean checkTables(String tableName) {

        boolean exist = true;
        if (!table.isExist(tableName)) {
            // pack
            exist = exist
                    & table.create(tableName, Pack.FLD_ID, Pack.FLD_EKT,
                            settings.getReadCapacity(), settings
                                    .getWriteCapacity());
        }
        return exist;
    }

    /**
     * Get item count. The count is calculated by the smaller value.
     * 
     * @param tableName table name
     * @return item count
     */
    public long getItemCount(String tableName) {
        long countInPrimaryTable = -1;
        if (table.isExist(tableName)) {
            // pack
            countInPrimaryTable = table.getItemCount(tableName);
            LOGGER.debug("countInPrimaryTable = " + countInPrimaryTable);
        }

        return countInPrimaryTable;
    }

    /**
     * Prepare pack table. Delete existing table and create empty table.
     * 
     * @param tableName table name
     * @return {@code True} if success, else - {@code False}
     */
    public boolean prepareTables(String tableName) {

        boolean success = false;

        // pack
        if (table.isExist(tableName)) {
            table.delete(tableName);
        }

        success = table.create(tableName, Pack.FLD_ID, Pack.FLD_EKT, settings
                .getReadCapacity(), settings.getWriteCapacity());
        return success;
    }

    /**
     * Batch add packs into table specified by derived instance name (PackA,
     * PackB)
     * 
     * @param packs
     * @return unprocessed items count
     * @throws ItemSizeLimitExceededException
     * @throws IllegalArgumentException
     * @throws AmazonServiceException
     */
    public List<ComplexKey> savePacks(List<Pack> packs)
            throws InterruptedException, AmazonServiceException,
            IllegalArgumentException, ItemSizeLimitExceededException {

        Pack[] packArray = packs.toArray(new Pack[packs.size()]);
        List<ComplexKey> unprocessed = new ArrayList<ComplexKey>();
        unprocessed = service.putObjects(packArray).getUnSuccessList();
        return unprocessed;
    }

    /**
     * get Amazon DB max size for batch save
     * 
     * @return size of batch
     */
    public int getMaxBatchSize() {
        return service.getMaxBatchSize();
    }
}
