package ru.olekstra.helper;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.domain.CardBatch;

@Repository
public class CardBatchHelper {

    private DynamodbService service;

    @Autowired
    public CardBatchHelper(DynamodbService service) {
        this.service = service;
    }

    public CardBatch getBatch(String batchName) {
        return service.getObject(CardBatch.class, batchName);
    }

    public List<String> getAllBatchNames() {
        List<Map<String, Object>> batchListFromDb = new LinkedList<Map<String, Object>>();
        List<String> batchNamesList = new LinkedList<String>();
        List<String> fieldListToGet = new LinkedList<String>();
        fieldListToGet.add(CardBatch.FLD_BATCH_ID);
        batchListFromDb = service.getAllItems(CardBatch.TABLE_NAME,
                fieldListToGet);
        if (!batchListFromDb.isEmpty()) {
            for (Map<String, Object> batchMap : batchListFromDb) {
                batchNamesList.add((String) batchMap
                        .get(CardBatch.FLD_BATCH_ID));
            }
        }
        return batchNamesList;
    }

    public boolean updateBatch(CardBatch batch) {
        try {
            return service.putObject(batch);
        } catch (ItemSizeLimitExceededException e) {
            e.printStackTrace();
            return false;
        }
    }

}
