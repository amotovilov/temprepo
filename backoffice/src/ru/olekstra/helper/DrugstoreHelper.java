package ru.olekstra.helper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Repository;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.common.dao.DrugstoreDao;
import ru.olekstra.domain.CompletedTransaction;
import ru.olekstra.domain.Drugstore;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.Refund;
import ru.olekstra.exception.NotSavedException;

import com.amazonaws.services.dynamodb.model.ProvisionedThroughputExceededException;

@Repository
public class DrugstoreHelper {

    private DynamodbService service;
    private MessageSource messageSource;
    private DrugstoreDao dao;

    @Autowired
    public DrugstoreHelper(DynamodbService service,
            MessageSource messageSource, DrugstoreDao dao) {
        this.service = service;
        this.messageSource = messageSource;
        this.dao = dao;
    }

    public List<Drugstore> getAllDrugstores() throws IOException {
        return service.getAllObjects(Drugstore.class);
    }

    public List<String> getAllDrugstoresIds() {

        List<Map<String, Object>> items = service.getAllItems(
                Drugstore.TABLE_NAME, Arrays.asList(Drugstore.FLD_ID));

        List<String> drugsoreIds = new ArrayList<String>();
        for (Map<String, Object> item : items) {
            if (item.get(Drugstore.FLD_ID) != null) {
                drugsoreIds.add((String) item.get(Drugstore.FLD_ID));
            }
        }
        return drugsoreIds;
    }

    public List<Drugstore> findDrugstores(String value) throws IOException {
        List<Drugstore> foundDrugstoreList = new ArrayList<Drugstore>();
        List<Drugstore> allDrugstoreList = dao.getDrugstoreNames();
        for (Drugstore drugstore : allDrugstoreList) {
            String drugstoreName = drugstore.getName().toUpperCase();
            if (drugstoreName.contains(value.toUpperCase()))
                foundDrugstoreList.add(drugstore);
        }
        return foundDrugstoreList;
    }

    public Drugstore getDrugstoreById(String id) {
        return service.getObject(Drugstore.class, id);
    }

    public boolean add(Drugstore drugstore) throws ItemSizeLimitExceededException {
        return service.putObject(drugstore);
    }

    public void save(Drugstore drugstore) throws NotSavedException {
        boolean result = dao.concurrentUpdateDrugstore(drugstore);
        if (!result) {
            String updatedAuthCode = getAuthCode(drugstore.getId());
            drugstore.setAuthCode(updatedAuthCode);
            result = dao.concurrentUpdateDrugstore(drugstore);
            if (!result) {
                throw new NotSavedException(messageSource.getMessage(
                        "msg.saveDataError", null, null));
            }
        }
    }

    /**
     * Get specified by yyyyMM formatted period refund transactions
     * 
     * @param period
     * @param discount Id
     * @return refund transactions for specified period
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws ProvisionedThroughputExceededException
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    public List<Refund> getRefundTransactionsForPeriod(
            String period, String discountId) throws InstantiationException, IllegalAccessException, IOException {

        return service.queryObjects(Refund.class, period + Refund.getDelimeter() + discountId);
    }

    /**
     * Get specified by yyyyMM formatted period completed transactions
     * 
     * @param period
     * @return completed transactions for specified period
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws ProvisionedThroughputExceededException
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    public List<CompletedTransaction> getCompletedTransactionsForPeriod(
            String period) throws JsonParseException, JsonMappingException,
            IOException, ProvisionedThroughputExceededException, InstantiationException, IllegalAccessException {

        return service.queryObjects(CompletedTransaction.class, period);
    }

    /**
     * Get specified drugstore's id processing transactions by date range
     * 
     * @param id
     * @param dateFrom
     * @param dateTo
     * @return processing transaction for period and specified drugstore id
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws ProvisionedThroughputExceededException
     */
    public List<ProcessingTransaction> getDrugstoreProcessingTransactions(
            String id, String dateFrom, String dateTo)
            throws JsonParseException, JsonMappingException, IOException,
            ProvisionedThroughputExceededException {

        List<Map<String, Object>> atributes = service
                .getAttributesWithinTimePeriod(
                        ProcessingTransaction.TABLE_NAME, id, dateFrom, dateTo,
                        null);

        List<ProcessingTransaction> transactions = new ArrayList<ProcessingTransaction>(
                atributes.size());

        for (Map<String, Object> atribute : atributes) {
            transactions.add(new ProcessingTransaction(atribute));
        }

        return transactions;
    }

    public String getAuthCode(String drugstoreId) {
        Map<String, Object> atributes = service.getAttributes(
                Drugstore.TABLE_NAME, drugstoreId, Arrays
                        .asList(Drugstore.FLD_AUTH_CODE));
        return (String) atributes.get(Drugstore.FLD_AUTH_CODE);
    }
}
