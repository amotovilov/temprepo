package ru.olekstra.helper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ru.olekstra.awsutils.BatchOperationResult;
import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.domain.Discount;
import ru.olekstra.domain.DiscountData;
import ru.olekstra.domain.DiscountLimit;
import ru.olekstra.domain.DiscountPlan;
import ru.olekstra.domain.DiscountReplace;

import com.amazonaws.AmazonServiceException;

@Repository
public class DiscountHelper {

    private DynamodbService service;

    @Autowired
    public DiscountHelper(DynamodbService service) {
        this.service = service;
    }

    public List<Discount> getAllDiscounts() throws IOException {

        List<Discount> discounts = new ArrayList<Discount>();
        List<Map<String, Object>> items = service.getAllItems(
                Discount.TABLE_NAME, null);

        for (Map<String, Object> attributes : items) {
            discounts.add(new Discount(attributes));
        }

        return discounts;
    }

    public Discount getDiscount(String id) {
        return service.getObject(Discount.class, id);
    }

    public boolean saveDiscount(Discount discount)
            throws ItemSizeLimitExceededException {
        return service.putObject(discount);
    }

    public void deleteDiscount(String id) {
        service.deleteItem(Discount.TABLE_NAME, id);
    }

    public boolean saveDiscountData(List<DiscountData> discountPacks)
            throws AmazonServiceException, IllegalArgumentException,
            InterruptedException, ItemSizeLimitExceededException {

        int counter = 0;
        boolean result = true;
        List<DiscountData> batch = new ArrayList<DiscountData>(
                service.getMaxBatchSize());

        for (DiscountData discountData : discountPacks) {

            batch.add(discountData);
            counter++;

            if ((batch.size() == service.getMaxBatchSize())
                    || (counter == discountPacks.size())) {

                BatchOperationResult<DiscountData> batchResult = service
                        .putObjects(batch.toArray(new DiscountData[batch
                                .size()]));
                batch.clear();

                if (!batchResult.getUnSuccessList().isEmpty()) {
                    result = false;
                }
            }
        }
        return result;
    }

    public boolean saveDiscountLimits(List<DiscountLimit> discountLimits)
            throws AmazonServiceException, IllegalArgumentException,
            InterruptedException, ItemSizeLimitExceededException {

        int counter = 0;
        boolean result = true;
        List<DiscountLimit> batch = new ArrayList<DiscountLimit>(
                service.getMaxBatchSize());

        for (DiscountLimit discountLimit : discountLimits) {

            batch.add(discountLimit);
            counter++;

            if ((batch.size() == service.getMaxBatchSize())
                    || (counter == discountLimits.size())) {

                BatchOperationResult<DiscountLimit> batchResult = service
                        .putObjects(batch.toArray(new DiscountLimit[batch
                                .size()]));
                batch.clear();

                if (!batchResult.getUnSuccessList().isEmpty()) {
                    result = false;
                }
            }
        }
        return result;
    }

    public boolean saveDiscountReplaces(List<DiscountReplace> discountReplaces)
            throws AmazonServiceException, IllegalArgumentException,
            InterruptedException, ItemSizeLimitExceededException {

        int counter = 0;
        boolean result = true;
        List<DiscountReplace> batch = new ArrayList<DiscountReplace>(
                service.getMaxBatchSize());

        for (DiscountReplace discountReplace : discountReplaces) {

            batch.add(discountReplace);
            counter++;

            if ((batch.size() == service.getMaxBatchSize())
                    || (counter == discountReplaces.size())) {

                BatchOperationResult<DiscountReplace> batchResult = service
                        .putObjects(batch.toArray(new DiscountReplace[batch
                                .size()]));
                batch.clear();

                if (!batchResult.getUnSuccessList().isEmpty()) {
                    result = false;
                }
            }
        }
        return result;
    }

    public List<DiscountPlan> getAllDiscountPlans() throws IOException {
        return service.getAllObjects(DiscountPlan.class);
    }
}