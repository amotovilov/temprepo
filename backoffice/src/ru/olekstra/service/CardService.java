package ru.olekstra.service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.dozer.Mapper;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import ru.olekstra.awsutils.BatchOperationResult;
import ru.olekstra.awsutils.ComplexKey;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.awsutils.exception.NotUpdatedException;
import ru.olekstra.backoffice.dto.BalanceOperationType;
import ru.olekstra.backoffice.dto.CardsLoadResult;
import ru.olekstra.backoffice.util.CsvParser;
import ru.olekstra.backoffice.util.DateTimeUtil;
import ru.olekstra.common.helper.CardHelper;
import ru.olekstra.common.service.SmsService;
import ru.olekstra.common.util.OperationUtil;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.TelCardIndex;
import ru.olekstra.domain.dto.CardChangeDto;
import ru.olekstra.exception.DuplicatePhoneException;
import ru.olekstra.exception.InvalidDateRangeException;
import ru.olekstra.exception.NotFoundException;

import com.amazonaws.AmazonServiceException;

@Service
public class CardService {

    private CardHelper helper;
    private SmsService smsService;
    private MessageSource messageSource;
    private Mapper mapper;

    private static final Logger LOGGER = Logger.getLogger(CardService.class);

    public CardService() {

    }

    @Autowired
    public CardService(CardHelper cardHelper, SmsService smsService,
            MessageSource messageSource, Mapper mapper) {
        this.helper = cardHelper;
        this.smsService = smsService;
        this.messageSource = messageSource;
        this.mapper = mapper;
    }

    /**
     * Get card by number
     * 
     * @param number card number
     * @return card
     * @throws NotFoundException
     */
    public Card getCard(String number) throws NotFoundException {

        Card card = helper.getCard(number);
        if (card == null) {
            throw new NotFoundException("Card not found by #" + number);
        }
        return card;
    }

    public List<Card> getCards(List<String> numbers)
            throws UnsupportedEncodingException, IOException, RuntimeException,
            InterruptedException {
        return helper.getCardsInBatchMode(numbers).getSuccessList();
    }

    /**
     * Load cards
     * 
     * @param planId discount plan id
     * @param numberList card list
     * @return loaded card count
     * @throws InvalidDateRangeException
     */
    public List<String> createCards(String planId, String fromDate,
            String toDate, Map<String, String> numberCvvList)
            throws IllegalArgumentException, InvalidDateRangeException {

        List<String> cardNumList = new LinkedList<String>();
        DateTime startDate = null;
        DateTime endDate = null;
        if (StringUtils.isNotBlank(fromDate) & StringUtils.isNotBlank(toDate)) {
            DateTimeUtil datesFormatter = new DateTimeUtil();
            startDate = datesFormatter.parse(fromDate + " "
                    + DateTimeUtil.START_DAY_TIME,
                    DateTimeUtil.FORM_INPUT_PARSE_DATE_PATTERN);
            endDate = datesFormatter.parse(toDate + " "
                    + DateTimeUtil.END_DAY_TIME,
                    DateTimeUtil.FORM_INPUT_PARSE_DATE_PATTERN);
            if (startDate.isAfter(endDate))
                throw new InvalidDateRangeException(
                        "Reverse date range did not allow");
        }

        for (Map.Entry<String, String> numberCvv : numberCvvList.entrySet()) {
            Card newCard = new Card(numberCvv.getKey(), numberCvv.getValue(), false);
            if (startDate != null)
                newCard.setStartDate(startDate);
            if (endDate != null)
                newCard.setEndDate(endDate);
            if (StringUtils.isNotBlank(planId))
                newCard.setDiscountPlan(planId);
            // save card
            if (helper.addNewCard(newCard)) {
                cardNumList.add(newCard.getNumber());
            }
        }
        return cardNumList;
    }

    public void updateCardBalance(Map<String, BigDecimal> values, List<Card> cards,
            Map<String, Boolean> updatedCards,
            Map<String, BigDecimal> updatedLimits) throws RuntimeException,
            InterruptedException {

        for (Card card : cards) {

            BigDecimal limit = null;
            boolean success = false;

            if (card.getLimitRemain() != null && values.containsKey(card.getNumber())) {
                limit = card.getLimitRemain().add(values.get(card.getNumber()));

                success = helper.updateCardLimit(card.getNumber(), card.getLimitRemain(), limit);
            } else {
                limit = values.get(card.getNumber());
                List<String> limitFieldList = new ArrayList<String>(1);
                limitFieldList.add(Card.FLD_LIMIT_REMAIN);

                success = helper.updateCardWithNullConditions(card,
                        limitFieldList);
            }

            if (success) {
                updatedLimits.put(card.getNumber(), limit);
            }
            updatedCards.put(card.getNumber(), success);
        }
    }

    public void updateCardBalance(BigDecimal value, List<Card> cards,
            Map<String, Boolean> updatedCards,
            Map<String, BigDecimal> updatedLimits, List<String> sameLimitCards,
            Map<String, BigDecimal> moreLimitCards,
            BalanceOperationType operationType) throws RuntimeException,
            InterruptedException {

        for (Card card : cards) {

            BigDecimal limit = null;
            boolean success = false;

            if (card.getLimitRemain() != null) {
                if (operationType == BalanceOperationType.RECAHARGE) {
                    limit = card.getLimitRemain();
                    limit = value.add(limit);
                } else if (operationType == BalanceOperationType.UPTATE) {
                    if (card.getLimitRemain().compareTo(value) > 0) {
                        moreLimitCards.put(card.getNumber(), card
                                .getLimitRemain());
                        continue;
                    } else if (card.getLimitRemain().compareTo(value) == 0) {
                        sameLimitCards.add(card.getNumber());
                        continue;
                    } else {
                        limit = value;
                    }
                } else if (operationType == BalanceOperationType.SET) {
                    limit = value;
                }

                success = helper.updateCardLimit(card.getNumber(), card
                        .getLimitRemain(), limit);
            } else {
                limit = value;
                card.setLimitRemain(value);
                List<String> limitFieldList = new ArrayList<String>(1);
                limitFieldList.add(Card.FLD_LIMIT_REMAIN);

                success = helper.updateCardWithNullConditions(card,
                        limitFieldList);
            }

            if (success) {
                updatedLimits.put(card.getNumber(), limit);
            }
            updatedCards.put(card.getNumber(), success);
        }
    }

    public boolean sendSmsAfterUpdateCardBalance(Map<String, BigDecimal> updatedLimits, List<Card> cards) {

        List<Map<String, String>> recipients = new ArrayList<Map<String, String>>();
        Map<String, String> cardsPhoneNumbers = getCardsPhoneNumbers(cards);

        for (String cardNumber : updatedLimits.keySet()) {
            String message = "";

            message = messageSource
                    .getMessage(
                            "msg.limitSet",
                            new Object[] {
                                    smsService
                                            .getCardNumLastFourDigits(cardNumber),
                                    updatedLimits.get(cardNumber)
                                            .toString()}, null);

            String phone = cardsPhoneNumbers.get(cardNumber);
            if (phone != null && !phone.isEmpty()) {
                Map<String, String> recipient = new HashMap<String, String>();
                recipient.put(phone, message);
                recipients.add(recipient);
            }
        }

        try {
            smsService.sendMultpleMessages(recipients);
            return true;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean sendSmsAfterUpdateCardBalance(BigDecimal value,
            Map<String, BigDecimal> updatedLimits, List<Card> cards,
            BalanceOperationType operationType) {

        List<Map<String, String>> recipients = new ArrayList<Map<String, String>>();
        Map<String, String> cardsPhoneNumbers = getCardsPhoneNumbers(cards);

        for (String cardNumber : updatedLimits.keySet()) {
            String message = "";
            if (operationType == BalanceOperationType.RECAHARGE) {
                message = messageSource
                        .getMessage(
                                "msg.logrecord.limit",
                                new Object[] {
                                        smsService
                                                .getCardNumLastFourDigits(cardNumber),
                                        value.toString(),
                                        updatedLimits.get(cardNumber)
                                                .toString()}, null);
            } else {
                message = messageSource
                        .getMessage(
                                "msg.limitSet",
                                new Object[] {
                                        smsService
                                                .getCardNumLastFourDigits(cardNumber),
                                        updatedLimits.get(cardNumber)
                                                .toString()}, null);
            }

            String phone = cardsPhoneNumbers.get(cardNumber);
            if (phone != null && !phone.isEmpty()) {
                Map<String, String> recipient = new HashMap<String, String>();
                recipient.put(phone, message);
                recipients.add(recipient);
            }
        }

        try {
            smsService.sendMultpleMessages(recipients);
            return true;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private List<String> getCardList(Map<String, Boolean> cardMap, Boolean b) {
        Boolean entryVal;
        List<String> cardList = new LinkedList<String>();
        for (Map.Entry<String, Boolean> entry : cardMap.entrySet()) {
            entryVal = entry.getValue();
            if (entryVal ^= b)
                cardList.add(entry.getKey());
        }
        return cardList;
    }

    public List<String> getUpdatedCardList(Map<String, Boolean> cardMap) {
        return getCardList(cardMap, Boolean.FALSE);
    }

    public List<String> getNotUpdatedCardsList(Map<String, Boolean> cardMap) {
        return getCardList(cardMap, Boolean.TRUE);
    }

    private List<String> getCardNumbersByActive(
            Map<String, Boolean> updatedCards,
            Map<String, Boolean> activetedCards, Boolean activeState) {

        List<String> result = new ArrayList<String>();
        for (Map.Entry<String, Boolean> entry : updatedCards.entrySet()) {
            String cardNumber = entry.getKey();
            Boolean isUpdated = entry.getValue();
            Boolean isActivated = activetedCards.get(cardNumber);
            if (isUpdated && isActivated.equals(activeState)) {
                result.add(cardNumber);
            }
        }
        return result;
    }

    public List<String> getUpdatedActiveCardNumbers(
            Map<String, Boolean> updatedCards,
            Map<String, Boolean> activatedCards) {
        return getCardNumbersByActive(updatedCards, activatedCards,
                Boolean.TRUE);
    }

    public List<String> getUpdatedInactiveCardNumbers(
            Map<String, Boolean> updatedCards,
            Map<String, Boolean> activatedCards) {
        return getCardNumbersByActive(updatedCards, activatedCards,
                Boolean.FALSE);
    }

    /**
     * Search card by phone or number
     * 
     * @param term card number or phone number
     * @return card
     * @throws NotFoundException
     */
    public Card searchCard(String term) throws NotFoundException {

        Card card = helper.getCard(term);
        if (card == null) {
            String number = helper.checkPhone(term);
            if (number == null) {
                throw new NotFoundException("Card not found by #" + term);
            }
            card = getCard(number);
        }
        return card;
    }

    /**
     * Activate card
     * 
     * @param number card number
     * @param phone phone number
     * @param holder card holder
     * @param planId discount plan id
     * @return activated card
     * @throws NotFoundException
     * @throws NotUpdatedException
     * @throws DuplicatePhoneException
     * @throws ItemSizeLimitExceededException
     */
    public Card activate(String number, String phone, String holder,
            String planId, String fromDate, String toDate)
            throws NotFoundException, NotUpdatedException,
            DuplicatePhoneException, IllegalArgumentException,
            InvalidDateRangeException, ItemSizeLimitExceededException {

        String cardByPhone = helper.checkPhone(phone);
        if (cardByPhone != null)
            throw new DuplicatePhoneException(cardByPhone);

        DateTime startDate = null;
        DateTime endDate = null;
        if (StringUtils.isNotBlank(fromDate) & StringUtils.isNotBlank(toDate)) {
            DateTimeUtil datesFormatter = new DateTimeUtil();
            startDate = datesFormatter.parse(fromDate + " "
                    + DateTimeUtil.START_DAY_TIME,
                    DateTimeUtil.FORM_INPUT_PARSE_DATE_PATTERN);
            endDate = datesFormatter.parse(toDate + " "
                    + DateTimeUtil.END_DAY_TIME,
                    DateTimeUtil.FORM_INPUT_PARSE_DATE_PATTERN);
            if (startDate.isAfter(endDate))
                throw new InvalidDateRangeException(
                        "Reverse date range did not allow");
        }

        Card card = this.getCard(number);
        card.setTelephoneNumber(phone);
        card.setHolderName(holder);
        card.setDiscountPlan(planId);
        card.setDisabled(Boolean.FALSE);
        // format dates for DB
        card.setStartDate(startDate);
        card.setEndDate(endDate);

        if (!helper.updateCard(card)) {
            throw new NotUpdatedException("Card not updated by " + number);
        }

        return card;
    }

    public List<Card> getAllCards() throws IOException {
        return helper.getAllCards();
    }

    public boolean updateCard(Card card) throws ItemSizeLimitExceededException {
        return helper.updateCard(card);
    }

    public void deleteTelephoneIndex(String telephone) {
        helper.deleteTelephoneIndex(telephone);
    }

    public void updateCardOwner(Card card, String oldPhone, String newPhone)
            throws DuplicatePhoneException,
            NotUpdatedException, ItemSizeLimitExceededException {

        if (!newPhone.equals(oldPhone)) {
            String phoneUsedByNumber = helper.checkPhone(newPhone);
            if (phoneUsedByNumber != null)
                throw new DuplicatePhoneException(phoneUsedByNumber);

            helper.deleteTelephoneIndex(oldPhone);
        }

        if (!helper.updateCard(card)) {
            throw new NotUpdatedException("Card not updated by "
                    + card.getNumber());
        }
    }

    public List<String> updateCardsBatches(List<Card> cards, String batchName,
            boolean toDelete) throws InterruptedException,
            AmazonServiceException, IllegalArgumentException,
            ItemSizeLimitExceededException {

        List<String> batches = null;
        List<Card> listForUpdate = new ArrayList<Card>();

        List<String> debugList = new ArrayList<String>();

        for (Card card : cards) {
            if (card.getCardBatches() == null) {
                card.setCardBatches(new ArrayList<String>());
            }

            if (!toDelete) {
                if (!card.getCardBatches().contains(batchName)) {
                    batches = card.getCardBatches();
                    batches.add(batchName);
                    card.setCardBatches(batches);
                    listForUpdate.add(card);
                    debugList.add(card.getNumber());
                }
            }

            if (toDelete) {
                if (card.getCardBatches().contains(batchName)) {
                    batches = card.getCardBatches();
                    batches.remove(batchName);
                    card.setCardBatches(batches);
                    listForUpdate.add(card);
                    debugList.add(card.getNumber());
                }
            }
        }

        LOGGER.debug("Cards for update: " + StringUtils.join(debugList, ","));
        // обнволяем список серии в таблице Card
        List<String> unprocessedNumbers = helper.updateCards(listForUpdate);
        LOGGER.debug("Unprocessed card: " + unprocessedNumbers);
        List<Card> successUpdatedCards = new ArrayList<Card>();
        debugList.clear();
        for (Card card : cards) {
            // Добавим успешно обновленные карты
            if (!unprocessedNumbers.contains(card.getNumber())) {
                successUpdatedCards.add(card);
                debugList.add(card.getNumber());
            }
            // И про всякий случай обновим те карты которые не обновляли,
            // т.к. в них уже есть (или нет, если удаляем) указанные серии
            if (!listForUpdate.contains(card.getNumber())) {
                successUpdatedCards.add(card);
            }
        }
        // обновляем список серий в таблице cardBatches, но только у тех карт
        // которые удалось обновить на предыдущем этапе

        LOGGER.debug("Cards for update in cardBatch: "
                + StringUtils.join(debugList, ","));
        helper.updateCardBatches(successUpdatedCards, batchName, toDelete);
        return unprocessedNumbers;
    }

    /**
     * Рассылка уведомлений держателям карт после пакетных операций
     * 
     * @param message текст сообщения
     * @param updatedActiveCards список карт
     * @return true если удалось отправить смс, false если в процессе отправки
     *         произошли ошибки
     */
    public boolean sendSmsAfterBatchCardUpdate(String message,
            List<Card> updatedActiveCards) {

        List<Map<String, String>> recipients = new ArrayList<Map<String, String>>();
        for (Card card : updatedActiveCards) {
            // if (card.isActive()) {
            if (card.isActive() && (card.getTelephoneNumber() != null)
                    && !card.getTelephoneNumber().isEmpty()) {
                Map<String, String> recipient = new HashMap<String, String>();
                recipient.put(card.getTelephoneNumber(), message);
                recipients.add(recipient);
            }
        }

        try {
            smsService.sendMultpleMessages(recipients);
            return true;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * Рассылка уведомлений держателям карт после пакетной активации карт, в
     * тексте сообщения происходит персонализация, а именно рассылка последних 4
     * цифр номера карты
     * 
     * @param updatedActiveCards список карт
     * @return true если удалось отправить смс, false если в процессе отправки
     *         произошли ошибки
     */
    public boolean sendSmsAfterBatchCardActivation(List<Card> updatedActiveCards) {

        List<Map<String, String>> recipients = new ArrayList<Map<String, String>>();
        for (Card card : updatedActiveCards) {
            if (card.isActive() && (card.getTelephoneNumber() != null)
                    && !card.getTelephoneNumber().isEmpty()) {
                Map<String, String> recipient = new HashMap<String, String>();
                recipient.put(card.getTelephoneNumber(), messageSource
                        .getMessage("msg.public.activation",
                                new Object[] {Card.getCuttingNumber(card.getNumber())}, null));
                recipients.add(recipient);
            }
        }

        try {
            smsService.sendMultpleMessages(recipients);
            return true;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * Метод выполняет загрузку карт по их номерам, используя пакетные операции.
     * В результате возвращается структура, содержащая список полученных карт,
     * список не найденных номеров и номеров карт, которые были в списке более 1
     * раза (дубли).
     * 
     * @param cardNumbers
     * @return
     * @throws RuntimeException
     * @throws InterruptedException
     * @throws IOException
     */
    public CardsLoadResult loadCards(List<String> cardNumbers)
            throws RuntimeException, InterruptedException, IOException {

        List<String> cardNumbersToLoad = new ArrayList<String>();
        List<String> duplicates = new ArrayList<String>();
        List<String> notFound = new ArrayList<String>();
        List<Card> cards = new ArrayList<Card>();
        for (String cardNumber : cardNumbers) {
            if (!cardNumbersToLoad.contains(cardNumber)) {
                cardNumbersToLoad.add(cardNumber);
            } else {
                if (!duplicates.contains(cardNumber))
                    duplicates.add(cardNumber);
            }
        }

        cards.addAll(helper.getCardsBatch(cardNumbersToLoad));

        Set<String> loadedCardNumbers = new HashSet<String>();
        for (Card card : cards) {
            loadedCardNumbers.add(card.getNumber());
        }

        for (String cardNumber : cardNumbers) {
            if (!loadedCardNumbers.contains(cardNumber))
                if (!notFound.contains(cardNumber))
                    notFound.add(cardNumber);
        }

        return new CardsLoadResult(cards, notFound, duplicates);
    }

    public void skipAlreadySubscribed(List<Card> cards, String planId,
            List<String> alreadySubscribed) {
        List<Card> cardsToRemove = new ArrayList<Card>();
        for (Card card : cards) {
            String discountPlanId = card.getDiscountPlanId();
            if (discountPlanId != null && discountPlanId.equals(planId)) {
                alreadySubscribed.add(card.getNumber());
                cardsToRemove.add(card);
            }
        }
        cards.removeAll(cardsToRemove);
    }

    public void updateCardDiscountPlan(String planId, List<Card> cards,
            Map<String, Boolean> updatedCards,
            Map<String, Boolean> activatedCards, List<String> notUpdatedCards)
            throws InterruptedException, AmazonServiceException,
            IllegalArgumentException, ItemSizeLimitExceededException {

        for (Card card : cards) {
            card.setDiscountPlan(planId);
            // сначала предполагаем, что обновлены все карты
            updatedCards.put(card.getNumber(), Boolean.TRUE);
            if (!card.isActive())
                activatedCards.put(card.getNumber(), Boolean.FALSE);
            else
                activatedCards.put(card.getNumber(), Boolean.TRUE);
        }
        // пакетная запись
        List<String> unprocessedNumbers = helper.updateCards(cards);
        // ставим флаг для незаписанных карт
        for (String num : unprocessedNumbers)
            updatedCards.put(num, Boolean.FALSE);
        notUpdatedCards.addAll(unprocessedNumbers);
    }

    public void updateCardDate(String fromDate, String toDate,
            List<Card> cards, Map<String, Boolean> updatedCards,
            Map<String, Boolean> activatedCards, List<String> notUpdatedCards)
            throws InterruptedException, InvalidDateRangeException,
            AmazonServiceException, IllegalArgumentException,
            ItemSizeLimitExceededException {

        DateTime startDate = null;
        DateTime endDate = null;
        if (StringUtils.isNotBlank(fromDate) & StringUtils.isNotBlank(toDate)) {
            DateTimeUtil datesFormatter = new DateTimeUtil();
            startDate = datesFormatter.parse(fromDate + " "
                    + DateTimeUtil.START_DAY_TIME,
                    DateTimeUtil.FORM_INPUT_PARSE_DATE_PATTERN);
            endDate = datesFormatter.parse(toDate + " "
                    + DateTimeUtil.END_DAY_TIME,
                    DateTimeUtil.FORM_INPUT_PARSE_DATE_PATTERN);
            if (startDate.isAfter(endDate))
                throw new InvalidDateRangeException(
                        "Reverse date range did not allow");
        }

        for (Card card : cards) {
            card.setStartDate(startDate);
            card.setEndDate(endDate);
            // сначала предполагаем, что обновлены все карты
            updatedCards.put(card.getNumber(), Boolean.TRUE);
            if (!card.isActive())
                activatedCards.put(card.getNumber(), Boolean.FALSE);
            else
                activatedCards.put(card.getNumber(), Boolean.TRUE);
        }
        // пакетная запись
        List<String> unprocessedNumbers = helper.updateCards(cards);
        // ставим флаг для незаписанных карт
        for (String num : unprocessedNumbers)
            updatedCards.put(num, Boolean.FALSE);
        notUpdatedCards.addAll(unprocessedNumbers);
    }

    public List<String> saveCardAttributes(
            List<Card> cards,
            Map<String, String> clearPhones)
            throws InterruptedException,
            AmazonServiceException, IllegalArgumentException,
            ItemSizeLimitExceededException {
        // пакетная запись
        List<String> notUpdatedCards = new ArrayList<String>();
        notUpdatedCards.addAll(helper.updateCards(cards));
        // очищаем телефоны
        Set<String> phones = new HashSet<String>(clearPhones.values());
        for (String cardN : clearPhones.keySet()) {
            if (notUpdatedCards.contains(cardN))
                phones.remove(clearPhones.get(cardN));
        }
        if (phones.size() > 0)
            helper.deleteTelephoneIndexes(phones);
        return notUpdatedCards;
    }

    public List<Card> getUpdatedActiveCards(List<Card> cards,
            List<String> updatedActiveCardNumbers) {
        List<Card> updatedActiveCards = new ArrayList<Card>();
        for (Card card : cards) {
            if (updatedActiveCardNumbers.contains(card.getNumber()))
                updatedActiveCards.add(card);
        }
        return updatedActiveCards;
    }

    public void prepareCardNumbers(List<String> cardNumbers,
            List<String> numbersToUpdate, List<String> duplicates) {
        Set<String> checkingNumbers = new HashSet<String>(cardNumbers.size());
        for (String cardNumber : cardNumbers) {
            if (!checkingNumbers.contains(cardNumber)) {
                if (!cardNumber.isEmpty()) {
                    numbersToUpdate.add(cardNumber);
                    checkingNumbers.add(cardNumber);
                }
            } else {
                if (!duplicates.contains(cardNumber)) {
                    duplicates.add(cardNumber);
                }
            }
        }
    }

    public void loadCardsByNumbers(List<String> cardNumbers, List<Card> cards,
            List<String> notLoadedCards) throws RuntimeException,
            InterruptedException, IOException {

        BatchOperationResult<Card> cardsResult = helper
                .getCardsInBatchMode(cardNumbers);

        cards.addAll(cardsResult.getSuccessList());

        Set<String> loadedCardNumbers = new HashSet<String>();
        for (Card card : cards) {
            loadedCardNumbers.add(card.getNumber());
        }

        for (String cardNumber : cardNumbers) {
            if (!loadedCardNumbers.contains(cardNumber)) {
                notLoadedCards.add(cardNumber);
            }
        }

        if (!cardsResult.getUnSuccessList().isEmpty()) {
            List<ComplexKey> unprocessedCards = cardsResult.getUnSuccessList();
            for (ComplexKey key : unprocessedCards) {
                if (!notLoadedCards.contains(key.getHashKey())) {
                    notLoadedCards.add(key.getHashKey());
                }
            }
        }
    }

    public List<Card> loadCardList(List<Card> allValidCardList,
            List<String> notFoundCardsList,
            List<String> alreadyActivatedCardsList,
            List<String> duplicatePhones, List<String> duplicatePhonesInDb,
            String discountPlanId) throws RuntimeException,
            InterruptedException, IOException {

        List<Card> cards = new ArrayList<Card>();
        HashSet<String> phonesInBatch = new HashSet<String>();

        // выбираем из БД по полному списку номеров карт
        List<String> cardNumberList = new ArrayList<String>();
        for (Card card : allValidCardList)
            cardNumberList.add(card.getNumber());
        BatchOperationResult<Card> cardsResult = helper
                .getCardsInBatchMode(cardNumberList);

        // добавляем в ненайденные список незагруженных из БД карт
        for (ComplexKey cardKey : cardsResult.getUnSuccessList())
            notFoundCardsList.add(cardKey.getHashKey());

        // удаляем из полного списка номеров незагруженные
        cardNumberList.removeAll(notFoundCardsList);

        // обходим список выбраных, формируем список уже активированных,
        // обновляем не найденные
        for (Card cardFromDb : cardsResult.getSuccessList()) {
            Card card = getCardByCardNumber(allValidCardList, cardFromDb
                    .getNumber());
            if (card != null) {
                if (phonesInBatch.contains(card.getTelephoneNumber())) {
                    if (card.getTelephoneNumber() != null
                            && !card.getTelephoneNumber().isEmpty()) {
                        duplicatePhones.add(card.getNumber());
                    }
                } else {
                    if (card.getTelephoneNumber() != null
                            && !card.getTelephoneNumber().isEmpty()) {
                        phonesInBatch.add(card.getTelephoneNumber());
                    }
                    if (helper.isPhoneUsed(card.getTelephoneNumber())) {
                        duplicatePhonesInDb.add(card.getNumber());
                    } else {

                        if (cardFromDb.isHoldernameSet()) {
                            alreadyActivatedCardsList.add(card.getNumber());
                        } else {
                            card.setDisabled(cardFromDb.getDisabled());
                            card.setCardBatches(cardFromDb.getCardBatches());
                            card.setLimitRemain(cardFromDb.getLimitRemain());
                            card.setDiscountPlan(discountPlanId);
                            cards.add(card);
                        }
                    }
                }
                cardNumberList.remove(card.getNumber());
            }
        }
        notFoundCardsList.addAll(cardNumberList);
        return cards;
    }

    private Card getCardByCardNumber(List<Card> cardList, String cardNumber) {
        Card card = null;
        for (Card c : cardList) {
            if (c.getNumber().equalsIgnoreCase(cardNumber))
                card = c;
        }
        return card;
    }

    /**
     * 
     * @param inputString
     * @param linesWithError
     * @param linesWithDuplicate
     * @return
     * @throws IOException
     */
    public List<Card> getCardsFromCsvString(String inputString,
            List<String> linesWithError, List<String> linesWithDuplicate)
            throws IOException {

        DateTimeUtil dtUtil = new DateTimeUtil();
        List<Card> cards = new ArrayList<Card>();
        BufferedReader buffer = new BufferedReader(new InputStreamReader(
                new ByteArrayInputStream(inputString.getBytes("UTF-8")),
                "UTF-8"));
        String strLine = "";
        String[] data = null;
        char delim = CsvParser.TAB;
        int lineCounter = 0;
        Set<String> cardSet = new HashSet<String>();

        while ((strLine = buffer.readLine()) != null) {

            lineCounter++;
            strLine = strLine.replaceAll(
                    "[\\u0000-\\u0008\\u000A-\\u001F\\u007F]", "");
            data = CsvParser.readFields(strLine, delim);

            String errLine = "";
            if (strLine.length() > 32)
                errLine = strLine.substring(0, 31) + "...";
            else
                errLine = strLine;

            if (!isDataValid(data)) { // проверяем строку
                linesWithError.add(lineCounter + ": " + errLine);
            } else if (cardSet.contains(data[0].trim())) { // если повтор
                linesWithDuplicate.add(lineCounter + ": " + errLine);
            } else { // если всё в порядке, сохраняем карту
                String cardNumber = OperationUtil.clearCardNumber(data[0]);
                String cardHolder = data[1].trim();
                String phone = OperationUtil.clearPhoneNumber(data[2]);
                DateTime startDateTime = null;
                DateTime endDateTime = null;
                if (!data[3].trim().isEmpty())
                    startDateTime = dtUtil.parse(data[3].trim() + " "
                            + DateTimeUtil.START_DAY_TIME,
                            DateTimeUtil.FORM_INPUT_PARSE_DATE_PATTERN);
                if (!data[4].trim().isEmpty())
                    endDateTime = dtUtil.parse(data[4].trim() + " "
                            + DateTimeUtil.END_DAY_TIME,
                            DateTimeUtil.FORM_INPUT_PARSE_DATE_PATTERN);
                Card card = new Card(cardNumber, false, cardHolder, phone,
                        startDateTime, endDateTime);
                cards.add(card);
                cardSet.add(cardNumber);
            }
        }

        return cards;
    }

    private Boolean isDataValid(String[] data) {
        Boolean valid = Boolean.TRUE;
        DateTimeFormatter formatter = DateTimeFormat
                .forPattern(DateTimeUtil.FILE_INPUT_PARSE_DATE_PATTERN);

        if (data.length != 5)
            return Boolean.FALSE;

        if ((OperationUtil.clearCardNumber(data[0]).length() == 0)
                || (data[1].trim().length() == 0))
            return Boolean.FALSE;

        int phoneNumberLength = OperationUtil.clearPhoneNumber(data[2])
                .length();
        if ((phoneNumberLength != 10) && (phoneNumberLength != 0))
            return Boolean.FALSE;

        if ((data[3].trim().length() > 0) || (data[4].trim().length() > 0)) {
            try {
                DateTime startDateTime = new DateTime(formatter
                        .parseDateTime(data[3].trim()));
                DateTime endDateTime = new DateTime(formatter
                        .parseDateTime(data[4].trim()));
                if (startDateTime.isAfter(endDateTime))
                    valid = Boolean.FALSE;
            } catch (IllegalArgumentException iae) {
                valid = Boolean.FALSE;
            }
        }

        return valid;
    }

    /**
     * Метод для пакетной активации карточек и записи индексных записей по номер
     * телефонов
     * 
     * @param cardList список карт
     * @return список неактивированных карт
     * @throws AmazonServiceException
     * @throws IllegalArgumentException
     * @throws InterruptedException
     * @throws ItemSizeLimitExceededException
     */
    public List<String> activateCardsInBatch(List<Card> cardList)
            throws AmazonServiceException, IllegalArgumentException,
            InterruptedException, ItemSizeLimitExceededException {
        List<String> unactivatedCards = helper.updateCards(cardList);
        List<TelCardIndex> phoneIndexes = new ArrayList<TelCardIndex>();
        for (Card card : cardList) {
            if (!unactivatedCards.contains(card.getNumber())) {
                phoneIndexes.add(new TelCardIndex(card.getTelephoneNumber(),
                        card.getNumber()));
            }
        }
        helper.updateIndexes(phoneIndexes);
        return unactivatedCards;
    }

    private Map<String, String> getCardsPhoneNumbers(List<Card> cards) {
        Map<String, String> cardsPhoneNumbers = new HashMap<String, String>();
        for (Card card : cards) {
            cardsPhoneNumbers.put(card.getNumber(), card.getTelephoneNumber());
        }
        return cardsPhoneNumbers;
    }

    public List<String> getSuccessfullyActivatedCardList(
            List<Card> cardsForActivationList,
            List<String> unactivatedCardsList,
            List<Card> successfullyActivatedCardList) {
        List<String> cardList = new ArrayList<String>();
        for (Card card : cardsForActivationList) {
            cardList.add(card.getNumber());
            successfullyActivatedCardList.add(card);
        }
        for (Card card : cardsForActivationList)
            for (String num : unactivatedCardsList)
                if (card.getNumber().equalsIgnoreCase(num)) {
                    cardList.remove(card.getNumber());
                    successfullyActivatedCardList.remove(card);
                }
        return cardList;
    }

    public boolean saveCardIfNotExists(CardChangeDto cardDto)
            throws ItemSizeLimitExceededException {
        if (StringUtils.isNotEmpty(cardDto.getTelephoneNumber())) {
            if (helper.isPhoneUsed(cardDto.getTelephoneNumber())) {
                return false;
            }
        }
        Card card = mapper.map(cardDto, Card.class);
        return helper.saveCardIfNotExists(card);
    }
}
