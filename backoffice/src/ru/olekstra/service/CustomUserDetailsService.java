package ru.olekstra.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.openid.OpenIDAttribute;
import org.springframework.security.openid.OpenIDAuthenticationToken;
import org.springframework.stereotype.Service;

import ru.olekstra.domain.User;
import ru.olekstra.exception.EmailNotFoundException;
import ru.olekstra.exception.MessageCodeConstants;
import ru.olekstra.exception.UserRoleNotFoundException;

@Service
public class CustomUserDetailsService implements UserDetailsService,
        AuthenticationUserDetailsService<OpenIDAuthenticationToken> {

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService,
            MessageSource messageSource) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String id)
            throws UsernameNotFoundException {

        User user = userService.getUser(id);
        if (user != null) {
            if (user.hasValidRoles()) {
                List<String> roles = user.getRoles();
                roles.add(User.ROLE_DEFAULT);
                List<GrantedAuthority> authorities = AuthorityUtils
                        .createAuthorityList(roles.toArray(new String[roles
                                .size()]));

                return new CustomUserDetails(id, authorities);
            } else {
                throw new UserRoleNotFoundException(
                        MessageCodeConstants.USER_ROLE_NOT_FOUND_CODE);
            }
        }

        throw new UsernameNotFoundException(
                MessageCodeConstants.USER_NOT_FOUND_CODE);
    }

    @Override
    public UserDetails loadUserDetails(OpenIDAuthenticationToken token)
            throws UsernameNotFoundException {

        String email = null;
        String firstName = null;
        String lastName = null;
        String fullName = null;

        List<OpenIDAttribute> attributes = token.getAttributes();
        for (OpenIDAttribute attribute : attributes) {
            if (attribute.getName().equals("email")) {
                email = attribute.getValues().get(0);
            }
            if (attribute.getName().equals("firstname")) {
                firstName = attribute.getValues().get(0);
            }
            if (attribute.getName().equals("lastname")) {
                lastName = attribute.getValues().get(0);
            }
            if (attribute.getName().equals("fullname")) {
                fullName = attribute.getValues().get(0);
            }
        }

        if (fullName == null) {
            StringBuilder fullNameBldr = new StringBuilder();
            if (firstName != null) {
                fullNameBldr.append(firstName);
            }
            if (lastName != null) {
                fullNameBldr.append(" ").append(lastName);
            }
            fullName = fullNameBldr.toString();
        }

        if (email == null) {
            throw new EmailNotFoundException(
                    MessageCodeConstants.EMAIL_NOT_FOUND_CODE);
        }

        return loadUserByUsername(email);
    }
}
