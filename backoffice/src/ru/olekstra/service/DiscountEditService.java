package ru.olekstra.service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.backoffice.util.CsvParser;
import ru.olekstra.common.service.LimitPeriod;
import ru.olekstra.common.util.OperationUtil;
import ru.olekstra.domain.Discount;
import ru.olekstra.domain.DiscountData;
import ru.olekstra.domain.DiscountLimit;
import ru.olekstra.domain.DiscountPlan;
import ru.olekstra.domain.DiscountReplace;
import ru.olekstra.helper.DiscountHelper;

import com.amazonaws.AmazonServiceException;

@Service
public class DiscountEditService {

    private DiscountHelper helper;

    public static final String CSV_REPLACE_REGEX = "[\\u0000-\\u0008\\u000A-\\u001F\\u007F]";

    public static final int CSV_LINE_BEGIN = 0;
    public static final int CSV_LINE_END = 32;

    public static final int CSV_INDEX_EKT = 0;
    public static final int CSV_INDEX_DISCOUNT_PERCENT = 1;
    public static final int CSV_INDEX_LIMIT_GROUP_ID = 2;
    public static final int CSV_INDEX_LIMIT_GROUP_WEIGHT = 3;
    public static final int CSV_INDEX_SYNONYM = 4;

    public static final int CSV_LIMIT_GROUP_ID = 0;
    public static final int CSV_LIMIT_PERIOD = 1;
    public static final int CSV_LIMIT_MAX_PACK_COUNT = 2;
    public static final int CSV_LIMIT_MAX_AVERAGE_PACK_COUNT = 3;
    public static final int CSV_LIMIT_MAX_DISCOUNT_SUM = 4;
    public static final int CSV_LIMIT_MAX_SUM_BEFORE_DISCOUNT = 5;

    @Autowired
    public DiscountEditService(DiscountHelper helper) {
        this.helper = helper;
    }

    public List<Discount> getAll() throws IOException {
        return helper.getAllDiscounts();
    }

    public Discount getDiscount(String id) {
        return helper.getDiscount(id);
    }

    public boolean saveDiscount(String id, String name, Integer promoPercent,
            int version, int packCount, boolean newDiscount,
            boolean useCardLimit, boolean alwaysFullRefund)
            throws ItemSizeLimitExceededException {

        return helper.saveDiscount(new Discount(id, name, useCardLimit,
                alwaysFullRefund, promoPercent, version, packCount));
    }

    public void deleteDiscount(String id) {
        helper.deleteDiscount(id);
    }

    public List<String> getRelatedDiscountPlans(String id)
            throws JsonParseException, JsonMappingException, IOException {

        List<String> relatedPlans = new ArrayList<String>();
        List<DiscountPlan> plans = helper.getAllDiscountPlans();

        for (DiscountPlan plan : plans) {
            if (plan.getDiscounts() != null && plan.getDiscounts().contains(id)) {
                relatedPlans.add(plan.getPlanName());
            }
        }

        return relatedPlans;
    }

    public boolean isAlreadyExists(String id) {
        return helper.getDiscount(id) != null ? true : false;
    }

    public boolean saveDiscountData(String discountId,
            Map<String, DiscountData> packs, int version)
            throws AmazonServiceException, IllegalArgumentException,
            InterruptedException, ItemSizeLimitExceededException {

        return helper.saveDiscountData(new ArrayList<DiscountData>(packs.values()));
    }

    public boolean saveDiscountLimits(String discountId,
            Collection<DiscountLimit> limits) throws AmazonServiceException,
            IllegalArgumentException, InterruptedException,
            ItemSizeLimitExceededException {

        return helper.saveDiscountLimits(new ArrayList<DiscountLimit>(limits));
    }

    public boolean saveDiscountReplaces(String discountId,
            Map<String, String> packsEktSynonym, int version)
            throws AmazonServiceException, IllegalArgumentException,
            InterruptedException, ItemSizeLimitExceededException {

        List<DiscountReplace> discountReplaces = new ArrayList<DiscountReplace>(
                packsEktSynonym.size());

        for (String packEkt : packsEktSynonym.keySet()) {
            discountReplaces.add(new DiscountReplace(discountId,
                    packsEktSynonym.get(packEkt), packEkt, String
                            .valueOf(version)));
        }

        return helper.saveDiscountReplaces(discountReplaces);
    }

    public void parsePacksCsv(String id, Integer version, String packsCsv, Map<String, DiscountData> packs,
            Map<String, String> packsEktSynonym, List<String> errorLines,
            List<String> duplicateLines) throws IOException {

        BufferedReader buffer = new BufferedReader(new InputStreamReader(
                new ByteArrayInputStream(packsCsv.getBytes("UTF-8")), "UTF-8"));

        String line = null;
        String[] data = null;
        int lineCounter = 0;

        while ((line = buffer.readLine()) != null) {

            lineCounter++;
            line = line.replaceAll(CSV_REPLACE_REGEX, "");
            data = CsvParser.readFields(line, CsvParser.TAB);

            String errorLine = lineCounter
                    + ": "
                    + (line.length() > CSV_LINE_END ? line.substring(
                            CSV_LINE_BEGIN, CSV_LINE_END)
                            + "..." : line);

            if (!isPackDataValid(data)) {
                errorLines.add(errorLine);
            } else {
                String packEkt = OperationUtil.clearEkt(data[CSV_INDEX_EKT]);
                if (packs.containsKey(packEkt)) {
                    duplicateLines.add(errorLine);
                } else {
                    DiscountData pack = new DiscountData();
                    pack.setEkt(packEkt);
                    pack.setPercent(Integer.parseInt(data[CSV_INDEX_DISCOUNT_PERCENT]));
                    if (id != null) {
                        pack.setDiscount(id);
                    }
                    if (version != null) {
                        pack.setCurrentVersion(version);
                    }
                    if (data.length > CSV_INDEX_LIMIT_GROUP_ID) {
                        pack.setLimitGroupId(data[CSV_INDEX_LIMIT_GROUP_ID]);
                    }
                    if (data.length > CSV_INDEX_LIMIT_GROUP_WEIGHT) {
                        pack.setLimitGroupWeight((StringUtils.isEmpty(data[CSV_INDEX_LIMIT_GROUP_WEIGHT]) ?
                                1 : Integer.parseInt(data[CSV_INDEX_LIMIT_GROUP_WEIGHT])));
                    }
                    packs.put(packEkt, pack);
                    if (data.length > CSV_INDEX_SYNONYM
                            && data[CSV_INDEX_SYNONYM] != null
                            && !data[CSV_INDEX_SYNONYM].trim().isEmpty()) {
                        String packSynonym = data[CSV_INDEX_SYNONYM].trim();
                        packsEktSynonym.put(packEkt, packSynonym);
                    }
                }
            }
        }
    }

    boolean isPackDataValid(String[] data) {
        final String percentPattern = "\\d+";
        if (data.length < CSV_INDEX_LIMIT_GROUP_ID) {
            return false;
        } else if (!data[CSV_INDEX_DISCOUNT_PERCENT].matches(percentPattern)
                || Integer.parseInt(data[CSV_INDEX_DISCOUNT_PERCENT]) < 0
                || Integer.parseInt(data[CSV_INDEX_DISCOUNT_PERCENT]) > 100) {
            return false;
        } else if (data.length > CSV_INDEX_LIMIT_GROUP_WEIGHT
                && StringUtils.isEmpty(data[CSV_INDEX_LIMIT_GROUP_ID])
                && StringUtils.isNotEmpty(data[CSV_INDEX_LIMIT_GROUP_WEIGHT])) {
            return false;
        } else if (data.length > CSV_INDEX_LIMIT_GROUP_WEIGHT
                && StringUtils.isNotEmpty(data[CSV_INDEX_LIMIT_GROUP_WEIGHT])
                && Integer.parseInt(data[CSV_INDEX_LIMIT_GROUP_WEIGHT]) < 0) {
            return false;
        } else {
            return true;
        }
    }

    public void parseLimitsCsv(String limitCsv, Map<String, DiscountLimit> limits,
            List<String> errorLines, List<String> duplicateLines, String discountId, Integer version)
            throws IOException {

        BufferedReader buffer = new BufferedReader(new InputStreamReader(
                new ByteArrayInputStream(limitCsv.getBytes("UTF-8")), "UTF-8"));

        String line = null;
        String[] data = null;
        int lineCounter = 0;

        while ((line = buffer.readLine()) != null) {

            lineCounter++;
            line = line.replaceAll(CSV_REPLACE_REGEX, "");
            data = CsvParser.readFields(line, CsvParser.TAB);

            String errorLine = lineCounter
                    + ": "
                    + (line.length() > CSV_LINE_END ? line.substring(
                            CSV_LINE_BEGIN, CSV_LINE_END)
                            + "..." : line);

            if (!isLimitValid(data)) {
                errorLines.add(errorLine);
            } else {
                String limitGroupId = OperationUtil.clearEkt(data[CSV_LIMIT_GROUP_ID]);
                if (limits.containsKey(limitGroupId)) {
                    duplicateLines.add(errorLine);
                } else {
                    DiscountLimit item = new DiscountLimit();
                    item.setDiscountId(discountId);
                    item.setCurrentVersion(version);
                    item.setLimitGroupId(data[CSV_LIMIT_GROUP_ID]);
                    item.setLimitPeriod(data[CSV_LIMIT_PERIOD]);
                    if (data.length > CSV_LIMIT_MAX_PACK_COUNT) {
                        item.setMaxPackCount(Integer.parseInt(data[CSV_LIMIT_MAX_PACK_COUNT]));
                    }
                    if (data.length > CSV_LIMIT_MAX_AVERAGE_PACK_COUNT) {
                        item.setMaxAveragePackCount(Long.parseLong(data[CSV_LIMIT_MAX_AVERAGE_PACK_COUNT]));
                    }
                    if (data.length > CSV_LIMIT_MAX_DISCOUNT_SUM) {
                        item.setMaxDiscountSum(new BigDecimal(data[CSV_LIMIT_MAX_DISCOUNT_SUM]));
                    }
                    if (data.length > CSV_LIMIT_MAX_SUM_BEFORE_DISCOUNT) {
                        item.setMaxSumBeforeDiscount(new BigDecimal(data[CSV_LIMIT_MAX_SUM_BEFORE_DISCOUNT]));
                    }
                    if (data.length > CSV_LIMIT_MAX_PACK_COUNT) {
                        item.setMaxPackCount(Integer.parseInt(data[CSV_LIMIT_MAX_PACK_COUNT]));
                    }

                    limits.put((String) item.getLimitGroupId(), item);
                }
            }
        }
    }

    private boolean validateBigDecimal(String val) {
        BigDecimal sum;
        try {
            sum = new BigDecimal(val);
        } catch (Exception e) {
            return false;
        }
        if (sum.compareTo(BigDecimal.ZERO) < 0) {
            return false;
        }
        return true;

    }

    boolean isLimitValid(String[] data) {
        final String percentPattern = "\\d+";
        if (data.length < CSV_LIMIT_PERIOD) {
            return false;
        } else if (!data[CSV_LIMIT_MAX_PACK_COUNT].matches(percentPattern)
                || Integer.parseInt(data[CSV_LIMIT_MAX_PACK_COUNT]) < 0
                || !data[CSV_LIMIT_MAX_AVERAGE_PACK_COUNT].matches(percentPattern)
                || Long.parseLong(data[CSV_LIMIT_MAX_AVERAGE_PACK_COUNT]) < 0) {
            return false;
        } else if (!(data[CSV_LIMIT_PERIOD].equals(LimitPeriod.DAY.getPeriod())
                || data[CSV_LIMIT_PERIOD].equals(LimitPeriod.WEEK.getPeriod())
                || data[CSV_LIMIT_PERIOD].equals(LimitPeriod.MONTH.getPeriod())
                || data[CSV_LIMIT_PERIOD].equals(LimitPeriod.QUARTER.getPeriod())
                || data[CSV_LIMIT_PERIOD].equals(LimitPeriod.HALFYEAR.getPeriod())
                || data[CSV_LIMIT_PERIOD].equals(LimitPeriod.YEAR.getPeriod()))) {
            return false;
        } else if (!validateBigDecimal(data[CSV_LIMIT_MAX_DISCOUNT_SUM])
                || !validateBigDecimal(data[CSV_LIMIT_MAX_SUM_BEFORE_DISCOUNT])) {
            return false;
        } else {
            return true;
        }
    }
}