package ru.olekstra.service.dto;

import java.math.BigDecimal;

public class BaseReportPackDto {

    private String ekt;
    private String name;
    private String manufacturer;
    private String inn;
    private int count;
    private BigDecimal retailSum;
    private BigDecimal discountSum;
    private BigDecimal paymentSum;
    private BigDecimal averageDiscountPercent;
    private BigDecimal refundSum;

    public BaseReportPackDto() {
    }

    public String getEkt() {
        return ekt;
    }

    public void setEkt(String ekt) {
        this.ekt = ekt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public BigDecimal getRetailSum() {
        return retailSum;
    }

    public void setRetailSum(BigDecimal retailSum) {
        this.retailSum = retailSum;
    }

    public BigDecimal getDiscountSum() {
        return discountSum;
    }

    public void setDiscountSum(BigDecimal discountSum) {
        this.discountSum = discountSum;
    }

    public BigDecimal getPaymentSum() {
        return paymentSum;
    }

    public void setPaymentSum(BigDecimal paymentSum) {
        this.paymentSum = paymentSum;
    }

    public BigDecimal getAverageDiscountPercent() {
        return averageDiscountPercent;
    }

    public void setAverageDiscountPercent(BigDecimal averageDiscountPercent) {
        this.averageDiscountPercent = averageDiscountPercent;
    }

    public BigDecimal getRefundSum() {
        return refundSum;
    }

    public void setRefundSum(BigDecimal refundSum) {
        this.refundSum = refundSum;
    }

}
