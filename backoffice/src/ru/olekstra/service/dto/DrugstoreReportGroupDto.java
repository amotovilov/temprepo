package ru.olekstra.service.dto;

import org.joda.time.DateTime;

public class DrugstoreReportGroupDto {

    private String drugstore;
    private String month;
    private DateTime createDateTime;
    private DrugstoreBaseReportDto baseReport;
    private DrugstoreDetailReportDto detailReport;

    public DrugstoreReportGroupDto(String drugstore, String month,
            DrugstoreBaseReportDto baseReport,
            DrugstoreDetailReportDto detailReport) {
        this.drugstore = drugstore;
        this.month = month;
        this.createDateTime = DateTime.now();
        this.baseReport = baseReport;
        this.detailReport = detailReport;
    }

    public String getDrugstore() {
        return drugstore;
    }

    public void setDrugstore(String drugstore) {
        this.drugstore = drugstore;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public DrugstoreBaseReportDto getBaseReport() {
        return baseReport;
    }

    public void setBaseReport(DrugstoreBaseReportDto baseReport) {
        this.baseReport = baseReport;
    }

    public DrugstoreDetailReportDto getDetailReport() {
        return detailReport;
    }

    public void setDetailReport(DrugstoreDetailReportDto detailReport) {
        this.detailReport = detailReport;
    }

    public DateTime getCreateDateTime() {
        return createDateTime;
    }

}
