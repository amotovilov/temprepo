package ru.olekstra.service.dto;

import java.net.URL;

public class ReportLinkDto {

    private String id;
    private String name;
    private URL url;

    public ReportLinkDto(String id, String name, URL url) {
        this.id = id;
        this.name = name;
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

}
