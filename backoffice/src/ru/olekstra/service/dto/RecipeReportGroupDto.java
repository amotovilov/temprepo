package ru.olekstra.service.dto;

import java.util.List;

import org.joda.time.DateTime;

public class RecipeReportGroupDto {

    private List<RecipeReportDto> recipes;
    private String drugstore;
    private String month;
    private DateTime createDateTime;
    private int total;
    private int refund;
    private int rejected;
    private int processing;

    public RecipeReportGroupDto() {
    }

    public RecipeReportGroupDto(List<RecipeReportDto> recipes,
            String drugstore, String month, DateTime createDateTime, int total,
            int refund, int rejected, int processing) {
        this.recipes = recipes;
        this.drugstore = drugstore;
        this.month = month;
        this.createDateTime = createDateTime;
        this.total = total;
        this.refund = refund;
        this.rejected = rejected;
        this.processing = processing;
    }

    public List<RecipeReportDto> getRecipes() {
        return recipes;
    }

    public void setRecipes(List<RecipeReportDto> recipes) {
        this.recipes = recipes;
    }

    public String getDrugstore() {
        return drugstore;
    }

    public void setDrugstore(String drugstore) {
        this.drugstore = drugstore;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public DateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(DateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getRefund() {
        return refund;
    }

    public void setRefund(int refund) {
        this.refund = refund;
    }

    public int getRejected() {
        return rejected;
    }

    public void setRejected(int rejected) {
        this.rejected = rejected;
    }

    public int getProcessing() {
        return processing;
    }

    public void setProcessing(int processing) {
        this.processing = processing;
    }
}
