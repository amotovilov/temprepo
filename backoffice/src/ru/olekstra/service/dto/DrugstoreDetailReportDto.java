package ru.olekstra.service.dto;

import java.math.BigDecimal;
import java.util.Set;

public class DrugstoreDetailReportDto {

    private BigDecimal totalSum = new BigDecimal("0.00");
    private BigDecimal totalDiscountSum = new BigDecimal("0.00");
    private BigDecimal totalRefundSum = new BigDecimal("0.00");
    private Set<DetailReportPackDto> saleList;

    public DrugstoreDetailReportDto(Set<DetailReportPackDto> saleList) {
        this.saleList = saleList;
        calculateTotal();
    }

    private void calculateTotal() {
        for (DetailReportPackDto sale : saleList) {
            totalSum = totalSum.add(sale.getTotalPayment());
            totalDiscountSum = totalDiscountSum.add(sale.getTotalDiscount());
            totalRefundSum = totalRefundSum.add(sale.getTotalRefund());
        }
    }

    public BigDecimal getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(BigDecimal totalSum) {
        this.totalSum = totalSum;
    }

    public BigDecimal getTotalDiscountSum() {
        return totalDiscountSum;
    }

    public void setTotalDiscountSum(BigDecimal totalDiscountSum) {
        this.totalDiscountSum = totalDiscountSum;
    }

    public BigDecimal getTotalRefundSum() {
        return totalRefundSum;
    }

    public void setTotalRefundSum(BigDecimal totalRefundSum) {
        this.totalRefundSum = totalRefundSum;
    }

    public Set<DetailReportPackDto> getSaleList() {
        return saleList;
    }

    public void setSaleList(Set<DetailReportPackDto> saleList) {
        this.saleList = saleList;
    }

}
