package ru.olekstra.service.dto;

import java.math.BigDecimal;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;

public class DetailReportPackDto implements Comparable<DetailReportPackDto> {

    private String uuidAndRow;
    private String barcode;
    private String packName;
    private String clientPackId;
    private String clientPackName;
    private DateTime date;
    private String storeName;
    private String cardNumber;
    private int count;
    private BigDecimal totalWithoutDiscount;
    private BigDecimal discountPercent;
    private BigDecimal totalDiscount;
    private BigDecimal totalPayment;
    private BigDecimal totalRefund;

    private DateTimeFormatter formatter;

    public DetailReportPackDto(DateTimeFormatter formatter) {
        this.formatter = formatter;
    }

    @Override
    public int compareTo(DetailReportPackDto packArg) {
        return (formatter.print(date) + uuidAndRow)
                .compareToIgnoreCase(formatter.print(packArg.getDate())
                        + packArg.getUuidAndRow());
    }

    public String getUuidAndRow() {
        return uuidAndRow;
    }

    public void setUuidAndRow(String uuidAndRow) {
        this.uuidAndRow = uuidAndRow;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getPackName() {
        return packName;
    }

    public void setPackName(String packName) {
        this.packName = packName;
    }

    public String getClientPackId() {
        return clientPackId;
    }

    public void setClientPackId(String clientPackId) {
        this.clientPackId = clientPackId;
    }

    public String getClientPackName() {
        return clientPackName;
    }

    public void setClientPackName(String clientPackName) {
        this.clientPackName = clientPackName;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public BigDecimal getTotalWithoutDiscount() {
        return totalWithoutDiscount;
    }

    public void setTotalWithoutDiscount(BigDecimal totalWithoutDiscount) {
        this.totalWithoutDiscount = totalWithoutDiscount;
    }

    public BigDecimal getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(BigDecimal discountPercent) {
        this.discountPercent = discountPercent;
    }

    public BigDecimal getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(BigDecimal totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public BigDecimal getTotalPayment() {
        return totalPayment;
    }

    public void setTotalPayment(BigDecimal totalPayment) {
        this.totalPayment = totalPayment;
    }

    public BigDecimal getTotalRefund() {
        return totalRefund;
    }

    public void setTotalRefund(BigDecimal totalRefund) {
        this.totalRefund = totalRefund;
    }

}
