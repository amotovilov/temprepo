package ru.olekstra.service.dto;

import java.util.List;

import ru.olekstra.domain.CompletedTransaction;

public class DrugstoreBaseReportDto extends ManufacturerBaseReportDto {

    private String drugstoreId;
    private String drugstoreName;

    public DrugstoreBaseReportDto(String drugstore, String month,
            List<CompletedTransaction> transactionList) {
        super(null, month, transactionList);
        this.drugstoreName = drugstore;
    }

    public String getDrugstoreId() {
        return drugstoreId;
    }

    public void setDrugstoreId(String drugstoreId) {
        this.drugstoreId = drugstoreId;
    }

    public String getDrugstoreName() {
        return drugstoreName;
    }

    public void setDrugstoreName(String drugstoreName) {
        this.drugstoreName = drugstoreName;
    }

}
