package ru.olekstra.service.dto;

import java.util.List;

public class ManufacturerDetailReportDto {

    private String barcode;
    private String packName;
    private List<DetailReportPackDto> packSaleList;

    public ManufacturerDetailReportDto() {
        super();
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getPackName() {
        return packName;
    }

    public void setPackName(String packName) {
        this.packName = packName;
    }

    public List<DetailReportPackDto> getPackSaleList() {
        return packSaleList;
    }

    public void setPackSaleList(List<DetailReportPackDto> packSaleList) {
        this.packSaleList = packSaleList;
    }

}
