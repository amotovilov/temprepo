package ru.olekstra.service.dto;

import ru.olekstra.domain.Recipe;

public class RecipeReportDto {

    private String url;
    private String state;
    private String rejectReason;

    public RecipeReportDto() {
    }

    public RecipeReportDto(String url, String state, String rejectReason) {
        this.url = url;
        this.state = state;
        this.rejectReason = rejectReason;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    public boolean isNew() {
        return this.state.equals(Recipe.STATE_NEW);
    }

    public boolean isParsed() {
        return this.state.equals(Recipe.STATE_PARSED);
    }

    public boolean isProblem() {
        return this.state.equals(Recipe.STATE_PROBLEM);
    }

    public boolean isChecked() {
        return this.state.equals(Recipe.STATE_CHECKED);
    }

    public boolean isRejected() {
        return this.state.equals(Recipe.STATE_REJECTED);
    }
}
