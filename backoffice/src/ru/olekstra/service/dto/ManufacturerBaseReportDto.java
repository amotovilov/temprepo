package ru.olekstra.service.dto;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ru.olekstra.domain.CompletedTransaction;

public class ManufacturerBaseReportDto {

    private String manufacturer;
    private String month;
    private BigDecimal totalWithoutDiscount = new BigDecimal(0);
    private BigDecimal averageDiscountPercent = new BigDecimal(0);
    private BigDecimal totalDiscount = new BigDecimal(0);
    private BigDecimal totalRefund = new BigDecimal(0);
    private List<CompletedTransaction> transactionList = new LinkedList<CompletedTransaction>();
    private Map<String, BaseReportPackDto> packSaleMap = new HashMap<String, BaseReportPackDto>();

    public ManufacturerBaseReportDto(String manufacturer, String month,
            List<CompletedTransaction> transactionList) {
        super();
        this.manufacturer = manufacturer;
        this.month = month;
        this.transactionList = transactionList;
        setTotal(transactionList);
    }

    private void setSaleMap(List<CompletedTransaction> transactionList) {
        for (CompletedTransaction transaction : transactionList) {
            // если скидка предоставлялась
            if (BigDecimal.ZERO.compareTo(transaction.getDiscountSum()) != 0) {
                // если такой же товар уже учитывали, добавляем суммы и
                // количество
                if (packSaleMap.containsKey(transaction.getPackBarcode())) {
                    BaseReportPackDto pack = packSaleMap.get(transaction
                            .getPackBarcode());
                    pack.setCount(pack.getCount() + transaction.getPackCount());
                    pack.setPaymentSum(pack.getPaymentSum().add(
                            transaction.getSumWithoutDiscount()));
                    pack.setDiscountSum(pack.getDiscountSum().add(
                            transaction.getDiscountSum()));
                    pack.setRefundSum(pack.getRefundSum().add(
                            transaction.getRefundSum()));
                    // иначе добавляем в карту
                } else {
                    BaseReportPackDto pack = new BaseReportPackDto();
                    pack.setName(transaction.getPackName());
                    pack.setCount(transaction.getPackCount());
                    pack.setPaymentSum(transaction.getSumWithoutDiscount());
                    pack.setDiscountSum(transaction.getDiscountSum());
                    pack.setRefundSum(transaction.getRefundSum());
                    packSaleMap.put(transaction.getPackBarcode(), pack);
                }
            }
        }
    }

    private void setAverages() {
        for (Map.Entry<String, BaseReportPackDto> pack : packSaleMap.entrySet()) {
            // средняя скидка
            BigDecimal tempAverage = pack.getValue().getDiscountSum()
                    .multiply(new BigDecimal(100));
            BigDecimal averageDiscountPercentForPack = tempAverage.divide(pack
                    .getValue().getPaymentSum(), 1, BigDecimal.ROUND_HALF_UP);
            pack.getValue().setAverageDiscountPercent(
                    averageDiscountPercentForPack);
            //
            totalWithoutDiscount = totalWithoutDiscount.add(pack.getValue()
                    .getPaymentSum());
            // сумма со скидкой общая
            totalDiscount = totalDiscount.add(pack.getValue().getDiscountSum());
            // средняя скидка
            BigDecimal temp = totalDiscount.multiply(new BigDecimal(100));
            if (BigDecimal.ZERO.compareTo(totalWithoutDiscount) != 0)
                averageDiscountPercent = temp.divide(totalWithoutDiscount, 1,
                        BigDecimal.ROUND_HALF_UP);
            else
                averageDiscountPercent = new BigDecimal("0.0");
            // сумма к возмещению
            totalRefund = totalRefund.add(pack.getValue().getRefundSum());
        }
    }

    private void setTotal(List<CompletedTransaction> transactionList) {
        setSaleMap(transactionList);
        setAverages();
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public BigDecimal getTotalWithoutDiscount() {
        return totalWithoutDiscount;
    }

    public void setTotalWithoutDiscount(BigDecimal totalWithoutDiscount) {
        this.totalWithoutDiscount = totalWithoutDiscount;
    }

    public BigDecimal getAverageDiscountPercent() {
        return averageDiscountPercent;
    }

    public void setAverageDiscountPercent(BigDecimal averageDiscountPercent) {
        this.averageDiscountPercent = averageDiscountPercent;
    }

    public BigDecimal getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(BigDecimal totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public BigDecimal getTotalRefund() {
        return totalRefund;
    }

    public void setTotalRefund(BigDecimal totalRefund) {
        this.totalRefund = totalRefund;
    }

    public List<CompletedTransaction> getTransactionList() {
        return transactionList;
    }

    public void setTransactionList(List<CompletedTransaction> transactionList) {
        this.transactionList = transactionList;
    }

    public Map<String, BaseReportPackDto> getSaleMap() {
        return packSaleMap;
    }

}
