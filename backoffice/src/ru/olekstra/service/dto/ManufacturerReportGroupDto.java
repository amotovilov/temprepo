package ru.olekstra.service.dto;

import java.util.List;

import org.joda.time.DateTime;

public class ManufacturerReportGroupDto {

    private String manufacturer;
    private String month;
    private DateTime createDateTime;
    private ManufacturerBaseReportDto baseReport;
    private List<ManufacturerDetailReportDto> detailReport;

    public ManufacturerReportGroupDto(String manufacturer, String month,
            ManufacturerBaseReportDto baseReport,
            List<ManufacturerDetailReportDto> detailReport) {
        super();
        this.manufacturer = manufacturer;
        this.month = month;
        this.createDateTime = DateTime.now();
        this.baseReport = baseReport;
        this.detailReport = detailReport;
    }

    public ManufacturerBaseReportDto getBaseReport() {
        return baseReport;
    }

    public void setBaseReport(ManufacturerBaseReportDto baseReport) {
        this.baseReport = baseReport;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public List<ManufacturerDetailReportDto> getDetailReport() {
        return detailReport;
    }

    public void setDetailReport(List<ManufacturerDetailReportDto> detailReport) {
        this.detailReport = detailReport;
    }

    public DateTime getCreateDateTime() {
        return createDateTime;
    }

}
