package ru.olekstra.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.olekstra.awsutils.SesService;

@Service
public class NotificationService {

    private SesService service;

    public static final String DEFAULT_EMAIL = "spam@olekstra.ru";

    @Autowired
    NotificationService(SesService service) {
        this.service = service;
    }

    private MimeMessage composeActivationCardMessage(String sender,
            String recipients, String messageBody) throws AddressException,
            MessagingException, MalformedURLException,
            UnsupportedEncodingException {

        // JavaMail representation of the message
        Session s = Session.getInstance(new Properties(), null);
        MimeMessage message = new MimeMessage(s);

        // Sender and recipient
        message.setFrom(new InternetAddress(sender));
        message.setRecipients(Message.RecipientType.TO, recipients);
        message.setRecipients(Message.RecipientType.CC, sender);

        // Subject
        message.setSubject("Уведомление от Olekstra.");

        // Add a MIME part to the message
        MimeMultipart multipart = new MimeMultipart();

        BodyPart bodypart = new MimeBodyPart();

        bodypart.setText(messageBody);
        bodypart.setHeader("Content-Type", "text/html; charset=\"utf-8\"");

        multipart.addBodyPart(bodypart);

        message.setContent(multipart);

        return message;
    }

    public void sendEmailNotificationMessage(String sender, String recipients,
            String messageBody) throws AddressException, MalformedURLException,
            UnsupportedEncodingException, IOException, MessagingException {
        service.send(composeActivationCardMessage(sender, recipients,
                messageBody));
    }

}
