package ru.olekstra.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.backoffice.dto.BalanceOperationType;
import ru.olekstra.common.dao.CardOperationDao;
import ru.olekstra.common.helper.CardHelper;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.common.service.CardLogService;
import ru.olekstra.common.service.DateTimeService;
import ru.olekstra.common.service.SmsService;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.CardLog;
import ru.olekstra.domain.dto.CardLogRecord;
import ru.olekstra.domain.dto.CardLogType;

@Service
public class CardOperationService {

    private DynamodbService dbService;
    private SmsService smsService;
    private CardHelper cardHelper;
    private MessageSource messageSource;
    private SecurityService securityService;
    private CardLogService cardLogService;
    private CardOperationDao operationDao;
    private DateTimeService dateService;

    private static final DateTimeFormatter cardFormatter = DateTimeFormat
            .forPattern("yyyyMM");
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat
            .forPattern("dd.MM.yyyy HH:mm:ss");

    private static final Logger LOGGER = Logger
            .getLogger(CardOperationService.class);

    public CardOperationService() {

    }

    @Autowired
    public CardOperationService(DynamodbService dbService,
            CardOperationDao operationDao, SmsService smsService,
            CardHelper cardHelper, MessageSource messageSource,
            SecurityService securityService,
            CardLogService cardLogService,
            DateTimeService dateService) {
        this.dbService = dbService;
        this.smsService = smsService;
        this.cardHelper = cardHelper;
        this.messageSource = messageSource;
        this.securityService = securityService;
        this.operationDao = operationDao;
        this.cardLogService = cardLogService;
        this.dateService = dateService;
    }

    /**
     * Запись в лог пакетной операции пополнения/изменения лимита карт
     * 
     * @param updatedLimits
     * @param newRechargeValue
     * @param notificationSent
     * @param operationType
     * @param reason
     * 
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     * @throws ItemSizeLimitExceededException
     * @throws RuntimeException
     * @throws InterruptedException
     */
    public void saveUpdateLimitRecords(Map<String, BigDecimal> updatedLimits, boolean notificationSent, String reason)
            throws JsonGenerationException, JsonMappingException, IOException,
            ItemSizeLimitExceededException, RuntimeException,
            InterruptedException {

        List<CardLog> records = new ArrayList<CardLog>();
        List<CardLog> serviceRecords = new ArrayList<CardLog>();
        for (String cardNumber : updatedLimits.keySet()) {

            String cardNote = "";
            String cardServiceNote = "";

            cardNote = messageSource
                    .getMessage("msg.limitSet", new Object[] {cardNumber,
                            updatedLimits.get(cardNumber).toString()}, null);
            cardServiceNote = messageSource.getMessage(
                    "msg.service.limitSet", new Object[] {
                            updatedLimits.get(cardNumber).toString(),
                            reason}, null);

            // Общедоступная запись о изменении баланса
            CardLog limitRecord = cardLogService.createLimitCardLog(cardNumber, cardNote,
                    securityService.getCurrentUser(),
                    notificationSent, false);
            records.add(limitRecord);
            // Служебная запись о пополнении баланса
            CardLog limitServiceRecord = cardLogService.createLimitCardLog(cardNumber, cardServiceNote,
                    securityService.getCurrentUser(),
                    notificationSent, true);
            serviceRecords.add(limitServiceRecord);

        }
        cardLogService.saveCardLog(records);
        cardLogService.saveCardLog(serviceRecords);
    }

    public void saveUpdateLimitRecords(Map<String, BigDecimal> updatedLimits,
            BigDecimal newRechargeValue, boolean notificationSent,
            BalanceOperationType operationType, String reason)
            throws JsonGenerationException, JsonMappingException, IOException,
            ItemSizeLimitExceededException, RuntimeException,
            InterruptedException {

        List<CardLog> records = new ArrayList<CardLog>();
        List<CardLog> serviceRecords = new ArrayList<CardLog>();
        for (String cardNumber : updatedLimits.keySet()) {

            String cardNote = "";
            String cardServiceNote = "";
            if (operationType == BalanceOperationType.RECAHARGE) {
                cardNote = messageSource.getMessage("msg.logrecord.limit",
                        new Object[] {
                                cardNumber, newRechargeValue.toString(),
                                updatedLimits.get(cardNumber).toString()
                        },
                        null);
                cardServiceNote = messageSource.getMessage(
                        "msg.service.logrecord.limit", new Object[] {
                                newRechargeValue.toString(),
                                updatedLimits.get(cardNumber).toString(),
                                reason}, null);
            } else {
                cardNote = messageSource
                        .getMessage("msg.limitSet", new Object[] {cardNumber,
                                updatedLimits.get(cardNumber).toString()}, null);
                cardServiceNote = messageSource.getMessage(
                        "msg.service.limitSet", new Object[] {
                                updatedLimits.get(cardNumber).toString(),
                                reason}, null);
            }
            // Общедоступная запись о изменении баланса
            CardLog limitRecord = cardLogService.createLimitCardLog(cardNumber, cardNote,
                    securityService.getCurrentUser(),
                    notificationSent, false);
            records.add(limitRecord);
            // Служебная запись о пополнении баланса
            CardLog limitServiceRecord = cardLogService.createLimitCardLog(cardNumber, cardServiceNote,
                    securityService.getCurrentUser(),
                    notificationSent, true);
            serviceRecords.add(limitServiceRecord);

        }
        cardLogService.saveCardLog(records);
        cardLogService.saveCardLog(serviceRecords);
    }

    void saveBackupTransactionRecord(DateTime dateTime, String drugstoreId,
            int quantity, BigDecimal sumToPay, String cardNumber,
            String clientPackName, BigDecimal discountSum,
            String transactionRangeKey, int rowNum,
            BigDecimal newLimit) throws JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException {

        String cardNote = messageSource.getMessage(
                "label.helpdesk.ticket.recharge.message",
                new Object[] {
                        dateTimeFormatter.print(dateTime),
                        clientPackName, String.valueOf(quantity),
                        sumToPay.toString()
                },
                null);
        Boolean notificationSent = this.sendSmsNotification(cardNumber,
                cardNote);

        CardLog cardTransaction = cardLogService.createTransactionStornoCardLog(
                cardNumber, drugstoreId,
                cardNote, rowNum,
                securityService.getCurrentUser(), notificationSent,
                dateService.createDefaultDateTime());

        cardLogService.saveCardLog(cardTransaction);

    }

    BigDecimal saveBackupLimitRecord(String drugstoreId, String cardNumber,
            BigDecimal discountSum) throws JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException {

        Card card = dbService.getObject(Card.class, cardNumber);
        boolean success = false;
        if (card == null) {
            LOGGER.debug("Card with #" + cardNumber
                    + " was not found. Limit record hasn`t been saved");
            return null;
        }

        BigDecimal limit = card.getLimitRemain() == null ? new BigDecimal(0)
                : card.getLimitRemain();
        BigDecimal newLimit = limit.add(discountSum);

        if (newLimit.compareTo(BigDecimal.ZERO) >= 0) {

            CardLog limitRecord = cardLogService.createLimitCardLog(cardNumber, drugstoreId,
                    messageSource.getMessage("msg.cardlimitnote",
                            new Object[] {discountSum.toString()}, null),
                    dateService.createDefaultDateTime());

            if (discountSum.compareTo(BigDecimal.ZERO) == 0)
                limitRecord.setInternalUse(true);

            cardLogService.saveCardLog(limitRecord);

            success = cardHelper.updateCardLimit(cardNumber, limit, newLimit);
        }

        if (!success) {
            LOGGER.debug("Failure to update limit value on card " + cardNumber);
            return limit;
        }
        return newLimit;
    }

    public void saveChangePlanRecord(String cardNumber, String planId)
            throws JsonGenerationException, JsonMappingException, IOException,
            ItemSizeLimitExceededException {

        String cardNote = messageSource.getMessage(
                "msg.cardPlanChangeWithOutReason", new Object[] {planId}, null);

        CardLog cardRecord = cardLogService.createChangePlanCardLog(cardNumber, cardNote,
                true, securityService.getCurrentUser());

        cardLogService.saveCardLog(cardRecord);
    }

    /**
     * Сохранение служебной записи о смене тарифного плана на картах
     * 
     * @param cardNumbers список номеров карт
     * @param planId id нового плана
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     * @throws ItemSizeLimitExceededException
     * @throws InterruptedException
     * @throws RuntimeException
     */
    public void saveChangePlanRecords(List<String> cardNumbers, String planId,
            String reason) throws JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException,
            RuntimeException, InterruptedException {

        String user = securityService.getCurrentUser();
        List<CardLog> records = new ArrayList<CardLog>();

        for (String cardNumber : cardNumbers) {
            CardLog cardLog = cardLogService.createChangePlanCardLog(cardNumber,
                    messageSource.getMessage(
                            "msg.cardPlanChange",
                            new Object[] {planId,
                                    reason}, null),
                    true, user);

            records.add(cardLog);
        }

        cardLogService.saveCardLog(records);
    }

    /**
     * Сохранение служебной записи об очистке атрибутов карт
     * 
     * @param cardNumbers список номеров карт с атрибутами, которые у данной
     *            карточки будут очищены
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     * @throws ItemSizeLimitExceededException
     * @throws InterruptedException
     * @throws RuntimeException
     */
    public void saveClearAttributesRecords(Map<String, String> cards,
            String reason)
            throws JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException,
            RuntimeException, InterruptedException {

        String user = securityService.getCurrentUser();
        List<CardLog> records = new ArrayList<CardLog>();

        for (String cardNumber : cards.keySet()) {
            CardLog cardLog = cardLogService.createCardLog(cardNumber,
                    messageSource
                            .getMessage("msg.clearCardAttributes",
                                    new Object[] {cards.get(cardNumber),
                                            reason}, null),
                    null, dateService.createDefaultDateTime(), CardLogType.CLEAR_ATTRIBUTES, true);
            cardLog.setUser(user);
            records.add(cardLog);
        }

        cardLogService.saveCardLog(records);
    }

    /**
     * Запись в лог публичной записи об операции смены дат по картам
     * 
     * @param cardNumbers список номеров карт
     * @param fromDate новая дата "от"
     * @param toDate новая дата "до"
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     * @throws ItemSizeLimitExceededException
     * @throws InterruptedException
     * @throws RuntimeException
     */
    public void saveChangeDateRecords(List<String> cardNumbers,
            String fromDate, String toDate, String reason)
            throws JsonGenerationException, JsonMappingException, IOException,
            ItemSizeLimitExceededException, RuntimeException,
            InterruptedException {

        List<CardLog> records = new ArrayList<CardLog>();
        List<CardLog> serviceRecords = new ArrayList<CardLog>();

        for (String cardNumber : cardNumbers) {

            CardLog cardLog = cardLogService.createCardLog(cardNumber,
                    messageSource
                            .getMessage("msg.cardDateChange",
                                    new Object[] {fromDate,
                                            toDate}, null),
                    null, dateService.createDefaultDateTime(), CardLogType.CHANGE_DATE, false);
            cardLog.setNotificationSend(true);

            records.add(cardLog);

            CardLog cardServiceLog = cardLogService.createCardLog(cardNumber,
                    messageSource
                            .getMessage("msg.service.cardDateChange", new Object[] {
                                    fromDate, toDate, reason}, null),
                    null, dateService.createDefaultDateTime(), CardLogType.CHANGE_DATE, true);

            serviceRecords.add(cardServiceLog);
        }

        cardLogService.saveCardLog(records);
        cardLogService.saveCardLog(serviceRecords);
    }

    /**
     * Сохранение служебной записи о смене серий у карты
     * 
     * @param card карта
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     * @throws ItemSizeLimitExceededException
     */
    public void saveChangeBatchRecord(Card card)
            throws JsonGenerationException, JsonMappingException, IOException,
            ItemSizeLimitExceededException {

        String cardNote = messageSource.getMessage("msg.cardSingleBatchChange",
                new Object[] {StringUtils.join(card.getCardBatches().toArray(),
                        ",")}, null);

        CardLog cardLog = cardLogService.createCardLog(card.getNumber(), cardNote, null,
                dateService.createDefaultDateTime(), CardLogType.CHANGE_BATCH, true);
        cardLog.setUser(securityService.getCurrentUser());
        cardLogService.saveCardLog(cardLog);
    }

    /**
     * Сохранение в лог служебной записи о смене серий у карт
     * 
     * @param cards список карт
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     * @throws ItemSizeLimitExceededException
     * @throws InterruptedException
     * @throws RuntimeException
     */
    public void saveChangeBatchRecords(List<Card> cards, String reason)
            throws JsonGenerationException, JsonMappingException, IOException,
            ItemSizeLimitExceededException, RuntimeException,
            InterruptedException {

        String user = securityService.getCurrentUser();
        List<CardLog> records = new ArrayList<CardLog>();

        for (Card card : cards) {
            CardLog cardLog = cardLogService.createCardLog(card.getNumber(),
                    messageSource.getMessage(
                            "msg.cardBatchChange",
                            new Object[] {
                                    StringUtils.join(
                                            card.getCardBatches()
                                                    .toArray(),
                                            ","), reason}, null), null,
                    dateService.createDefaultDateTime(), CardLogType.CHANGE_BATCH, true);
            cardLog.setUser(user);
            records.add(cardLog);
        }

        cardLogService.saveCardLog(records);
    }

    /**
     * Сохранение служебной записи об изменении данных держателя карты
     * 
     * @param card карта
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     * @throws ItemSizeLimitExceededException
     */
    public void saveChangeOwnerInfoRecord(String cardId, String oldName,
            String newName, String oldPhone, String newPhone)
            throws JsonGenerationException, JsonMappingException, IOException,
            ItemSizeLimitExceededException {

        String cardNote = messageSource.getMessage(
                "msg.cardOwnerInfoChange",
                new Object[] {oldName, newName, oldPhone, newPhone}, null);

        CardLog cardLog = cardLogService.createCardLog(cardId, cardNote, null,
                dateService.createDefaultDateTime(), CardLogType.CHANGE_OWNER_INFO, true);

        cardLog.setUser(securityService.getCurrentUser());

        cardLogService.saveCardLog(cardLog);
    }

    /**
     * Сохранение в лог служебной и публичной записей об активации карт
     * 
     * @param cards список карт
     * @param reason причина активации
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     * @throws ItemSizeLimitExceededException
     * @throws InterruptedException
     * @throws RuntimeException
     */
    public void saveCardActivateRecords(List<Card> cards, String reason)
            throws JsonGenerationException, JsonMappingException, IOException,
            ItemSizeLimitExceededException, RuntimeException,
            InterruptedException {

        String user = securityService.getCurrentUser();
        List<CardLog> records = new ArrayList<CardLog>();

        for (Card card : cards) {
            // Служебная запись
            CardLog cardServiceLog = cardLogService.createCardLog(card.getNumber(),
                    messageSource.getMessage("msg.service.activation", new Object[] {
                            card.getHolderName(), card.getTelephoneNumber(),
                            card.getDiscountPlanId(), reason}, null), null,
                    dateService.createDefaultDateTime(),
                    CardLogType.ACTIVATION, true);
            records.add(cardServiceLog);
        }
        cardLogService.saveCardLog(records);

        records.clear();
        for (Card card : cards) {
            // Публичная запись
            CardLog cardLog = cardLogService.createCardLog(card.getNumber(),
                    messageSource.getMessage(
                            "msg.public.activation",
                            new Object[] {Card.getCuttingNumber(card
                                    .getNumber())}, null), null,
                    dateService.createDefaultDateTime(),
                    CardLogType.ACTIVATION, false);
            records.add(cardLog);
        }

        cardLogService.saveCardLog(records);
    }

    /**
     * Получение текущего временного периода в заданном формате, необходимо для
     * корректного сохранения записей в лог
     * 
     * @return строковое представление периода
     */
    public String getActualPeriod() {
        return DateTime.now().withZone(DateTimeZone.UTC)
                .toString(cardFormatter);
    }

    /**
     * Отправка смс уведомления
     * 
     * @param cardNumber номер карты по которой произошло событие
     * @param note сообщение
     * @return true если отправка удалась, false в противном случае
     */
    boolean sendSmsNotification(String cardNumber, String note) {

        Card card = dbService.getObject(Card.class, cardNumber);
        if (card == null) {
            LOGGER.debug("Card with #" + cardNumber + " was not found");
            return false;
        }
        String phoneNumber = card.getTelephoneNumber();
        boolean result = false;
        if (phoneNumber != null) {
        	if (!phoneNumber.isEmpty()) {
            try {
                result = smsService.send(phoneNumber, note);
            } catch (ClientProtocolException e) {
                LOGGER.debug(e.getLocalizedMessage(), e);
            } catch (IOException e) {
                LOGGER.debug(e.getLocalizedMessage(), e);
            }
        }
        }

        return result;
    }

    /**
     * Метод для получения лог-записей по указанной карте и за указанный период
     * 
     * @param cardId номер карты
     * @param period период
     * @return список лог записей
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     */
    public List<CardLogRecord> getLogRecordsOnCardStat(String cardId,
            String period)
            throws JsonParseException, JsonMappingException, IOException {

        return operationDao.getLogRecords(cardId, period);
    }
}
