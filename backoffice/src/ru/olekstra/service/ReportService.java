package ru.olekstra.service;

import java.io.IOException;
import java.math.RoundingMode;
import java.net.URL;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.velocity.app.VelocityEngine;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.ui.velocity.VelocityEngineUtils;

import ru.olekstra.awsutils.S3Service;
import ru.olekstra.awsutils.exception.ReporNotFoundException;
import ru.olekstra.awsutils.exception.S3ConnectionException;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.common.util.VelocityMessageFormatter;
import ru.olekstra.service.dto.ReportLinkDto;

@Service
public class ReportService {

    public static final String DRUGSTORE_REPORT_TYPE = "DRUGSTORE_REPORT_TYPE";
    public static final String MANUFACTURER_REPORT_TYPE = "MANUFACTURER_REPORT_TYPE";
    public static final String RECIPE_REPORT_TYPE = "RECIPE_REPORT_TYPE";

    private static final String REPORT_LINKS_TEMPLATE_FILE = "reportList.vm";
    private static final String LINKS_FILE_NAME = "reportList.html";

    private static final String REPORT_TEMPLATE_PATH = "ru/olekstra/service/templates/vm/";
    private static final String RECIPE_REPORT_TEMPLATE_FILE = "recipeReport.vm";

    private DateTimeFormatter reportDateTimeFormatter;

    private NumberFormat moneyFormat;
    private NumberFormat percentFormat;

    private S3Service s3Service;
    private VelocityEngine velocityEngine;
    private MessageSource messageSource;

    @Autowired
    public ReportService(S3Service s3Service, VelocityEngine velocityEngine,
            MessageSource messageSource, DateTimeFormatter dateFormatter) {
        this.s3Service = s3Service;
        this.velocityEngine = velocityEngine;
        this.messageSource = messageSource;
        this.reportDateTimeFormatter = dateFormatter;

        this.moneyFormat = NumberFormat.getNumberInstance();
        this.moneyFormat.setGroupingUsed(true);
        this.moneyFormat.setMaximumFractionDigits(2);
        this.moneyFormat.setMinimumFractionDigits(2);
        this.moneyFormat.setRoundingMode(RoundingMode.HALF_UP);

        this.percentFormat = NumberFormat.getNumberInstance();
        this.percentFormat.setMaximumFractionDigits(1);
        this.percentFormat.setMinimumFractionDigits(1);
        this.percentFormat.setRoundingMode(RoundingMode.HALF_UP);
    }

    private String getBucketName(String reportType) {
        String bucket = "";
        if (reportType.equalsIgnoreCase(RECIPE_REPORT_TYPE))
            bucket = AppSettingsService.DEFAULT_RECIPE_REPORT_BUCKET;
        return bucket;
    }

    public URL saveReportToS3(String period, String subject, String reportType,
            Object report, DateTimeFormatter formatter) {
        URL url = null;
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("reportGroup", report);
        model.put("noArgs", new Object[] {});
        model.put("locale", new Locale("ru", "RU"));
        model.put("messageSource", messageSource);
        model.put("formatter", reportDateTimeFormatter);
        model.put("moneyFormat", moneyFormat);
        model.put("percentFormat", percentFormat);
        model.put("messageFormatter", new VelocityMessageFormatter(
                messageSource));
        String templateFileName = "";
        String bucket = this.getBucketName(reportType);
        if (reportType.equalsIgnoreCase(RECIPE_REPORT_TYPE))
            templateFileName = REPORT_TEMPLATE_PATH
                    + RECIPE_REPORT_TEMPLATE_FILE;
        String htmlText = VelocityEngineUtils.mergeTemplateIntoString(
                velocityEngine, templateFileName, model);
        String key = period + "/" + subject + ".html";
        s3Service.upload(bucket, key, IOUtils.toInputStream(htmlText));
        s3Service.addPublicReadAccess(bucket, key);
        url = s3Service.getUrl(bucket, key);
        return url;
    }

    public void saveReportListPageToS3(String month, String reportType,
            List<ReportLinkDto> linkList) {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("links", linkList);
        model.put("noArgs", new Object[] {});
        model.put("locale", new Locale("ru", "RU"));
        model.put("messageSource", messageSource);

        String templateFileName = REPORT_TEMPLATE_PATH
                + REPORT_LINKS_TEMPLATE_FILE;
        String bucket = this.getBucketName(reportType);
        String key = month + "/" + LINKS_FILE_NAME;
        String htmlText = VelocityEngineUtils.mergeTemplateIntoString(
                velocityEngine, templateFileName, model);
        s3Service.upload(bucket, key, IOUtils.toInputStream(htmlText));
        s3Service.addPublicReadAccess(bucket, key);
    }

    public String getHtmlReportList(String period, String reportType)
            throws S3ConnectionException, ReporNotFoundException, IOException {
        String key = period + "/reportList.html";
        String bucket = this.getBucketName(reportType);
        return IOUtils.toString(s3Service.download(bucket, key));
    }

}
