package ru.olekstra.service;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.dozer.Mapper;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.common.helper.DozerHelper;
import ru.olekstra.domain.Drugstore;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.dto.DrugstoreResponse;
import ru.olekstra.exception.DuplicateDrugstoreException;
import ru.olekstra.exception.NotSavedException;
import ru.olekstra.helper.DrugstoreHelper;

import com.amazonaws.services.dynamodb.model.ProvisionedThroughputExceededException;

@Service
public class DrugstoreService {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(DrugstoreService.class);

    private DrugstoreHelper helper;

    @Autowired
    Mapper dozerBeanMapper;

    @Autowired
    public DrugstoreService(DrugstoreHelper helper) {
        this.helper = helper;
    }

    public List<Drugstore> getAllDrugstores() throws IOException {
        return helper.getAllDrugstores();
    }

    public void checkExistDrugstore(String id, Boolean isNew)
            throws DuplicateDrugstoreException {
        Drugstore drugstore = helper.getDrugstoreById(id);
        if ((drugstore != null) && (isNew))
            throw new DuplicateDrugstoreException("Drugstore already exist");
    }

    /**
     * Find drugstores
     * 
     * @param term search term
     * @return found drugstores list
     * @throws IOException
     */
    public List<Drugstore> findDrugstores(String term) throws IOException {
        return helper.findDrugstores(term);
    }

    /**
     * Find drugstore by drugstore id
     * 
     * @param id drugstore id
     * @return found drugstore
     */
    public Drugstore findDrugstoreById(String id) {
        return helper.getDrugstoreById(id);
    }

    /**
     * Save new or existing drugstores data
     * 
     * @param id drigstore identifier
     * @param name drugstore name
     * @param location drugstore address
     * @param timeZone Timezone
     * @param discountSupport is drugstore supports Discount Program
     * @param insuranceSupport is drugstore supports Insurance program
     * @throws NotSavedException
     */
    public void save(String id, String name, String timeZone,
            Integer nonRefundPercent, String authCode, Boolean disabled)
            throws NotSavedException {

        Drugstore drugstore = new Drugstore(id);
        drugstore.setName(name);
        drugstore.setTimeZone(timeZone);
        drugstore.setAuthCode((authCode != null && authCode.isEmpty()) ? null : authCode);
        drugstore.setDisabled(disabled);
        if (nonRefundPercent != null) {
            drugstore.setNonRefundPercent(nonRefundPercent);
        }

        helper.save(drugstore);
    }

    /**
     * Get processing transactions by drugstore for specific month index
     * 
     * @param index index of week, where 1 - current week, 2 - current and
     *            previous week, 4 - current month, 8 - current and last month
     * @param id drugstore id
     * @return list of operations
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    public List<ProcessingTransaction> getProcessingTransactions(String id,
            int index) throws JsonParseException, JsonMappingException,
            IOException, ProvisionedThroughputExceededException {

        String dateTo = DateTime
                .now()
                .plusDays(1)
                .withZone(DateTimeZone.UTC)
                .toString(
                        DateTimeFormat
                                .forPattern(ProcessingTransaction.DB_DATE_FORMAT));

        String dateFrom = DateTime
                .now()
                .minusWeeks(index)
                .withZone(DateTimeZone.UTC)
                .toString(
                        DateTimeFormat
                                .forPattern(ProcessingTransaction.DB_DATE_FORMAT));

        LOGGER.debug(
                "Request processing transactions with drugstore id: '{}', {}'",
                id, "date from :" + dateFrom + ", date to: " + dateTo);

        List<ProcessingTransaction> transactions = helper
                .getDrugstoreProcessingTransactions(id, dateFrom, dateTo);

        Collections.sort(transactions, Collections.reverseOrder());
        return transactions;
    }

    /**
     * Возвращает подготовленный ответ из списка аптек для автокомплита
     * 
     * @param drugstoreList
     * @return
     */
    public List<DrugstoreResponse> getResponseListFromDrugstoreList(
            List<Drugstore> drugstoreList) {
        List<DrugstoreResponse> l = DozerHelper.map(dozerBeanMapper, drugstoreList, DrugstoreResponse.class);
        Collections.sort(l);
        return l;
    }

}
