package ru.olekstra.service;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class SecurityService {

    public String getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext()
                .getAuthentication();

        if (auth != null)
            return auth.getName();
        return null;
    }
}
