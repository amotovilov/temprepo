package ru.olekstra.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.w3c.dom.DOMException;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.SnsService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.awsutils.exception.NotUpdatedException;
import ru.olekstra.awsutils.exception.OlekstraException;
import ru.olekstra.azure.model.IMessage;
import ru.olekstra.azure.model.MessageSendException;
import ru.olekstra.azure.service.QueueSender;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.common.service.DiscountService;
import ru.olekstra.common.service.TicketCommonService;
import ru.olekstra.common.service.XmlMessageService;
import ru.olekstra.domain.EntityProxy;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.Refund;
import ru.olekstra.domain.Ticket;
import ru.olekstra.domain.TicketAssigned;
import ru.olekstra.domain.TicketInfo;
import ru.olekstra.domain.TicketPost;
import ru.olekstra.domain.TicketUnassigned;
import ru.olekstra.domain.dto.PackDiscountData;
import ru.olekstra.domain.dto.VoucherResponseRow;

@Service
public class TicketService extends TicketCommonService {

    public static final int TICKET_ID_CARD_INDEX = 0;
    public static final int TICKET_ID_TRANSACTION_INDEX = 1;

    public static final int TRANSACTION_VOUCHER_ID_INDEX = 0;
    public static final int TRANSACTION_ROW_NUMBER_INDEX = 1;

    private static final DateTimeFormatter dateFormatter = DateTimeFormat
            .forPattern("yyyyMMdd");

    private CardOperationService operationService;
    private DiscountService discountService;
    private XmlMessageService xmlMessageService;
    private QueueSender sender;

    @Autowired
    public TicketService(DynamodbService dynamodbService,
            AppSettingsService appSettings,
            CardOperationService operationService,
            DiscountService discountService,
            SnsService snsService,
            MessageSource messageSource,
            XmlMessageService xmlMessageService,
            QueueSender sender) {
        super(dynamodbService, appSettings, snsService, messageSource);

        this.operationService = operationService;
        this.discountService = discountService;
        this.xmlMessageService = xmlMessageService;
        this.sender = sender;
    }

    public List<Ticket> getTicketListForPeriod(String period) throws IOException, InstantiationException,
            IllegalAccessException {
        return dynamodbService.queryObjects(Ticket.class, period);
    }

    /**
     * Get new (unassigned) tickets with customer personal info for back office
     * table
     * 
     * @return List<TicketView> list of tickets with it's customer personal info
     * @throws IOException
     */
    public List<TicketInfo> getNewTickets() throws IOException {

        List<TicketUnassigned> items = dynamodbService
                .getAllObjects(TicketUnassigned.class);
        List<TicketInfo> tickets = null;
        if (items != null && items.size() > 0) {
            tickets = new ArrayList<TicketInfo>();
            for (TicketInfo item : items)
                tickets.add(item);
        }
        return tickets;
    }

    /**
     * Возвращает взятые в работу тикеты. Если параметр executive равен null,
     * возвращает все взятые в работу тикеты. Если не null, то только для
     * указанного пользователя.
     * 
     * @param executive - пользователь, взявший в работу тикеты
     * @return
     */
    public List<TicketInfo> getAssignedTickets(String executive) {

        List<TicketAssigned> items;
        try {
            items = dynamodbService.getAllObjects(TicketAssigned.class);
        } catch (Exception e) {
            return null;
        }
        List<TicketInfo> tickets = null;
        if (items != null && items.size() > 0) {
            tickets = new ArrayList<TicketInfo>();
            for (TicketAssigned item : items) {
                if (executive != null) {
                    if (item.getExecutive().equals(executive))
                        tickets.add(item);
                } else
                    tickets.add(item);
            }
        }

        return tickets;
    }

    /**
     * Сохранить тикет в БД
     * 
     * @param ticket
     * @throws ItemSizeLimitExceededException
     */
    public void saveTicket(Ticket ticket) throws ItemSizeLimitExceededException {
        dynamodbService.putObject(ticket);
    }

    /**
     * Get ticket executive
     * 
     * @param ticketId
     * @return ticket view object
     */
    public String getTicketExecutive(String ticketId) {

        List<TicketAssigned> tickets = null;
        try {
            tickets = dynamodbService
                    .getObjects(TicketAssigned.class, new String[] {ticketId}).getSuccessList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (tickets != null && tickets.size() > 0) {
            return tickets.get(0).getExecutive();
        }
        return null;
    }

    /**
     * Сохранить новый тикет не в работе и сообщение
     * 
     * @param ticket
     * @param post
     * @return
     */
    public boolean saveNewTicket(Ticket ticket, TicketPost post) {

        boolean result = true;
        try {
            result &= dynamodbService.putObject(ticket);
            result &= dynamodbService.putObject(post);
            TicketUnassigned ticketUnassigned = new TicketUnassigned(ticket);
            result &= dynamodbService.putObject(ticketUnassigned);
        } catch (ItemSizeLimitExceededException e) {
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Сохранить тикет с взятием в работу
     * 
     * @param ticket
     * @param post
     * @throws ItemSizeLimitExceededException
     * @throws IOException
     */
    public void saveAndAssignTicket(Ticket ticket, TicketPost post)
            throws ItemSizeLimitExceededException, IOException {
        dynamodbService.putObject(ticket);
        TicketAssigned ticketAssigned = new TicketAssigned(ticket);
        dynamodbService.putObject(ticketAssigned);
        dynamodbService.putObject(post);
    }

    /**
     * Add specified ticket post
     * 
     * @param post
     * @return true if success, otherwise false
     */
    public boolean addPost(TicketPost post) {
        try {
            return dynamodbService.putObject(post);
        } catch (ItemSizeLimitExceededException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Assign specified ticket to executive
     * 
     * @param post
     * @return true if success, otherwise false
     * @throws IOException
     */
    public void assignTicket(Ticket ticket, String executive) throws IOException {
        ticket.setExecutive(executive);
        TicketAssigned ticketAssigned = new TicketAssigned(ticket);

        try {
            dynamodbService.deleteItem(TicketUnassigned.TABLE_NAME, ticket
                    .getTicketId().toString());
            dynamodbService.putObject(ticket);
            dynamodbService.putObject(ticketAssigned);
        } catch (ItemSizeLimitExceededException e) {
            e.printStackTrace();
        }
    }

    /**
     * Remove specified assigned ticket
     * 
     * @param post
     * 
     */
    public void removeAssignedTicket(String executive, String ticketId) {
        dynamodbService.deleteItem(TicketAssigned.TABLE_NAME, executive,
                ticketId);
    }

    /**
     * Get count of new (unassigned) tickets
     * 
     * @return number of new (unassigned) tickets
     */
    public int getNewTicketsCount() {
        List<Map<String, Object>> ticketUnassignedAttributes = dynamodbService
                .getAllItems(
                        TicketUnassigned.TABLE_NAME, null);

        return ticketUnassignedAttributes.size();
    }

    /**
     * Check if specified ticket status is in close keywords group
     * 
     * @param status
     * @return true if in specified group, else false
     */
    public boolean isClosedStatus(String status) {
        if (status != null) {
            List<String> closeStatus = appSettings.getTicketStatusClosed();
            return closeStatus.contains(status);
        }
        return false;
    }

    /**
     * Update ticket reversal state
     * 
     * @param cardId
     * @param transactionId
     * @param value
     * @return true if updated succeeded, otherwise false
     */
    public boolean updateTicketReversal(String cardId, String transactionId,
            Boolean value) {
        Ticket ticket = dynamodbService.getObject(Ticket.class, cardId, transactionId);
        ticket.setReversal(value);
        try {
            dynamodbService.putObject(ticket);
            return true;
        } catch (ItemSizeLimitExceededException e) {
            return false;
        }
    }

    public boolean isTransactionFound(DateTime date,
            String voucherRowId, String drugstoreId) throws JsonParseException,
            JsonMappingException, IOException {
        String transactionId = voucherRowId.split(EntityProxy.getDelimeter())[0];
        int row;
        try {
            row = Integer.parseInt(voucherRowId.split(EntityProxy
                    .getDelimeter())[1]);
        } catch (NumberFormatException nfe) {
            return false;
        } catch (ArrayIndexOutOfBoundsException aioobe) {
            return false;
        }
        ProcessingTransaction transaction = this.getTransaction(date,
                transactionId, drugstoreId);

        if (transaction != null) {
            if (row <= transaction.getRows().size() && row > 0) {
                return true;
            } else
                return false;
        } else
            return false;
    }

    /**
     * Restore card limit and provide reversal transactions for specified ticket
     * 
     * @param cardNumber
     * @param transactionCode
     * @return true reversal transactions succeeded , otherwise false
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     * @throws ItemSizeLimitExceededException
     * @throws TransformerException
     * @throws OlekstraException
     * @throws ParserConfigurationException
     * @throws DOMException
     * @throws MessageSendException
     */
    public boolean reverseTicketTransaction(Ticket ticket) throws JsonParseException, JsonMappingException,
            IOException, ItemSizeLimitExceededException, DOMException, ParserConfigurationException, OlekstraException,
            TransformerException, MessageSendException {
        if (ticket.getVoucherRowId().contains(
                ProcessingTransaction.getDelimeter())
                && ticket.getDrugstoreId() != null) {
            String voucherId = ticket.getVoucherRowId().split(
                    ProcessingTransaction.getDelimeter())[TRANSACTION_VOUCHER_ID_INDEX];
            int rowNumber = Integer
                    .parseInt(ticket.getVoucherRowId().split(
                            ProcessingTransaction.getDelimeter())[TRANSACTION_ROW_NUMBER_INDEX]);
            ProcessingTransaction transaction = this.getTransaction(
                    ticket.getDate(), voucherId, ticket.getDrugstoreId());
            if (transaction != null) {
                VoucherResponseRow row = this.getRowForReverseTransaction(
                        transaction, rowNumber);
                if (row != null && !row.isReversed()) {
                    List<Refund> refunds = this
                            .getReverseTransactionRowRefunds(
                                    transaction, row);
                    this.saveReverseTransactionRefunds(refunds);
                    BigDecimal newLimit = operationService
                            .saveBackupLimitRecord(ticket.getDrugstoreId(),
                                    ticket.getCardNumber(),
                                    row.getCardLimitUsedSum());
                    discountService.stornDiscountData(transaction.getDiscountLimitUsedCurrentDetail(), row.getNumber());
                    // ProcessingTransactionMessage msg = new
                    // ProcessingTransactionMessage(transaction.getRangeKey(),
                    // transaction.getDrugstoreId());
                    row.setReversed(true);

                    IMessage message = xmlMessageService.createTransactionCompleteMessage(transaction);
                    message.setLabel(XmlMessageService.TRANSACTION_COMPLETE);
                    sender.send(message);

                    transaction.recalcTransactionSums();

                    dynamodbService.putObjectOrDie(transaction);

                    operationService.saveBackupTransactionRecord(
                            transaction.getCompleteTime(),
                            ticket.getDrugstoreId(),
                            row.getQuantity(),
                            row.getSumToPay(),
                            ticket.getCardNumber(),
                            row.getClientPackName(),
                            row.getCardLimitUsedSum(),
                            voucherId,
                            rowNumber,
                            newLimit);
                    return true;
                }
            }
        }
        return false;
    }

    private ProcessingTransaction getTransaction(DateTime date,
            String voucherId, String drugstoreId) {
        String period = date.toString(dateFormatter);
        return dynamodbService.getObject(ProcessingTransaction.class,
                drugstoreId,
                period + ProcessingTransaction.getDelimeter() + voucherId);
    }

    private VoucherResponseRow getRowForReverseTransaction(
            ProcessingTransaction transaction, int rowNumber)
            throws JsonParseException, JsonMappingException, IOException {

        for (VoucherResponseRow row : transaction.getRows()) {
            if (row.getNumber() == rowNumber) {
                return row;
            }
        }

        return null;
    }

    List<Refund> getReverseTransactionRowRefunds(
            ProcessingTransaction transaction,
            VoucherResponseRow row) throws JsonParseException,
            JsonMappingException, IOException {

        List<Refund> refunds = new ArrayList<Refund>();

        // DateTime period =
        // DateTime.now().withZone(TimeZoneFormatter.getDateTimeZone(transaction.getDrugstoreTimeZone()));

        // пишем в Refund сторнирующие записи с датой транзакции
        // OLP-528
        DateTime period = transaction.getStartTime();
        if (transaction.getCompleteTime() != null)
            period = transaction.getCompleteTime();

        for (PackDiscountData pack : row.getPackDiscount()) {
            if (!pack.getOriginalPercent().equals(BigDecimal.ZERO)) {
                refunds.add(createReverseRefund(transaction, row, pack,
                        period));
            }
        }

        return refunds;
    }

    Refund createReverseRefund(ProcessingTransaction transaction,
            VoucherResponseRow row, PackDiscountData pack, DateTime period)
            throws JsonParseException, JsonMappingException,
            IOException {

        Refund refund = new Refund();

        refund.setHashKey(period, pack.getDiscountId());
        refund.setRangeKey(transaction.getVoucherId().toString(),
                "-" + String.valueOf(row.getNumber()));

        refund.setAuthCode(transaction.getAuthCode());
        refund.setCardId(transaction.getCard().getNumber());
        refund.setDateTime(period);

        refund.setRowPrice(row.getPrice());
        refund.setRowCount(new BigDecimal(row.getQuantity()).negate());
        refund.setRowSumWithoutDiscount(row.getSumWithoutDiscount().negate());
        refund.setRowDiscountPercent(row.getDiscountPercent());
        refund.setRowDiscountSum(row.getDiscountSum().negate());
        refund.setRowPaidSum(row.getSumToPay().negate());
        refund.setRowDrugstoreRefund(row.getRefundSum().negate());

        refund.setDiscountPercentOriginal(pack.getOriginalPercent());
        refund.setDiscountPercentUsed(pack.getUsedPercent());
        refund.setDiscountRefundOriginalSum(pack.getOriginalSum().negate());
        refund.setDiscountRefundUsedSum(pack.getUsedSum().negate());
        refund.setDiscountRefundSum(pack.getRefundSum().negate());

        refund.setClientPackId(row.getClientPackId());
        refund.setClientPackName(row.getClientPackName());
        refund.setPackId(row.getId());
        refund.setPackName(row.getName());

        refund.setManufacturer(row.getManufacturer());

        refund.setDrugstoreId(transaction.getDrugstoreId());
        refund.setDrugstoreName(transaction.getDrugstoreName());

        return refund;
    }

    void saveReverseTransactionRefunds(List<Refund> refunds) throws NotUpdatedException {
        dynamodbService.putObjectsAllOrDie(refunds);
    }
}
