package ru.olekstra.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.domain.User;

@Service
public class UserService {

    private DynamodbService service;

    @Autowired
    public UserService(DynamodbService service) {
        this.service = service;
    }

    /**
     * Check user existence by email
     * 
     * @param email user email
     * @return 'true' if user exists and 'false' otherwise
     */
    public boolean isExist(String email) {
        User user = service.getObject(User.class, email);
        return (user != null);
    }

    public User getUser(String id) {
        return service.getObject(User.class, id);
    }

    public List<User> getAllUsers() throws IOException {
        return service.getAllObjects(User.class);
    }

    public boolean saveUser(User user) throws ItemSizeLimitExceededException {
        return service.putObject(user);
    }

    public void deleteUser(String userId) {
        service.deleteItem(User.TABLE_NAME, userId);
    }
}
