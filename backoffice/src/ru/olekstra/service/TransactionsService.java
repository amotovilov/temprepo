package ru.olekstra.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.olekstra.domain.CompletedTransaction;
import ru.olekstra.domain.Discount;
import ru.olekstra.domain.Refund;
import ru.olekstra.exception.InvalidDateRangeException;
import ru.olekstra.helper.DiscountHelper;
import ru.olekstra.helper.DrugstoreHelper;

import com.amazonaws.services.dynamodb.model.ProvisionedThroughputExceededException;

@Service
public class TransactionsService {

    public static final String CSV_FILE_TYPE = "CSV";
    public static final String XML_FILE_TYPE = "XML";
    public static final String TRANSACTIONS_FILE_CSV = "transacions.csv";
    public static final String TRANSACTIONS_FILE_XML = "transacions.xml";
    public static final String TRANSACTIONS_FOLDER = "/transactions/";
    public static final int TRANSACTIONS_MAX_UNLOAD_FILES = 10;

    public static final String ATTR_PERIOD = "Period";
    public static final String ATTR_DATETIME = "DateTime";
    public static final String ATTR_TRANSACTION_ID = "TransactionId";
    public static final String ATTR_VOUCHER_ID = "VoucherId";
    public static final String ATTR_VOUCHER_ROW = "VoucherRow";
    public static final String ATTR_DISCOUNT_PLAN_ID = "DiscountPlanId";
    public static final String ATTR_DISCOUNT_PLAN_NAME = "DiscountPlanName";
    public static final String ATTR_CARD = "Card";
    public static final String ATTR_DRUGSTORE_ID = "DrugstoreId";
    public static final String ATTR_DRUGSTORE_NAME = "DrugstoreName";
    public static final String ATTR_CLIENT_PACK_ID = "ClientPackId";
    public static final String ATTR_CLIENT_PACK_NAME = "ClientPackName";
    public static final String ATTR_PACK_EKT = "PackEKT";
    public static final String ATTR_PACK_BARCODE = "PackBarcode";
    public static final String ATTR_PACK_NAME = "PackName";
    public static final String ATTR_COUNT = "Count";
    public static final String ATTR_PRICE = "Price";
    public static final String ATTR_SUM_WITHOUT_DISCOUNT = "SumWithoutDiscount";
    public static final String ATTR_DISCOUNT_VALUE = "DiscountValue";
    public static final String ATTR_DISCOUNT_SUM = "DiscountSum";
    public static final String ATTR_SUM_TO_PAY = "SumToPay";
    public static final String ATTR_REFUND = "Refund";
    public static final String ATTR_AUTH_CODE = "AuthCode";
    public static final String ATTR_MANUFACTURER = "Manufacturer";
    public static final String ATTR_INN = "Inn";

    public static final String ATTR_REFUND_PERIOD = "Period";
    public static final String ATTR_REFUND_TRANSACTION_ID = "TransactionId";
    public static final String ATTR_REFUND_VOUCHER_ID = "VoucherId";
    public static final String ATTR_REFUND_ROW = "Row";

    public static final String ATTR_REFUND_AUTH_CODE = "AuthCode";
    public static final String ATTR_REFUND_CARD_NUMBER = "CardNumber";
    public static final String ATTR_REFUND_DISCOUNT_ID = "DiscountId";
    public static final String ATTR_REFUND_DATETIME = "DateTime";

    public static final String ATTR_REFUND_ROW_PRICE = "RowPrice";
    public static final String ATTR_REFUND_ROW_COUNT = "RowCount";
    public static final String ATTR_REFUND_ROW_SUM_WITHOUT_DISCOUNT = "RowSumWithoutDiscount";
    public static final String ATTR_REFUND_ROW_DISCOUNT_PERCENT = "RowDiscountPercent";
    public static final String ATTR_REFUND_ROW_DISCOUNT_SUM = "RowDiscountSum";
    public static final String ATTR_REFUND_ROW_PAID_SUM = "RowPaidSum";
    public static final String ATTR_REFUND_ROW_DRUGSTORE_REFUND = "RowDrugstoreRefund";

    public static final String ATTR_REFUND_DISCOUNT_PERCENT_ORIGINAL = "DiscountPercentOriginal";
    public static final String ATTR_REFUND_DISCOUNT_PERCENT_USED = "DiscountPercentUsed";
    public static final String ATTR_REFUND_DISCOUNT_REFUND_ORIGINAL_SUM = "DiscountRefundOriginalSum";
    public static final String ATTR_REFUND_DISCOUNT_REFUND_SUM = "DiscountRefundSum";
    public static final String ATTR_REFUND_DISCOUNT_REFUND_USED_SUM = "DiscountRefundUsedSum";

    private static final String ATTR_REFUND_EXACT_REFUND_SUM = "ExactRefundSum";
    private static final String ATTR_REFUND_EXACT_NONREFUND_SUM = "ExactNonRefundSum";
    private static final String ATTR_REFUND_EXACT_SUM_TO_PAY = "ExactSumToPay";

    public static final String ATTR_REFUND_PACK_ID = "PackId";
    public static final String ATTR_REFUND_PACK_NAME = "PackName";
    public static final String ATTR_REFUND_CLIENT_PACK_ID = "ClientPackId";
    public static final String ATTR_REFUND_CLIENT_PACK_NAME = "ClientPackName";

    public static final String ATTR_REFUND_MANUFACTURER = "Manufacturer";

    public static final String ATTR_REFUND_DRUGSTORE_ID = "DrugstoreId";
    public static final String ATTR_REFUND_DRUGSTORE_NAME = "DrugstoreName";

    private Map<String, Boolean> xmlAttributeMap;
    private Map<String, Boolean> xmlRefundAttributes;

    private DrugstoreHelper drugstoreHelper;
    private DiscountHelper discountHelper;
    public static final String REPORT_DATE_FORMAT = "MM.yyyy";
    private DateTimeFormatter transactionReportFormatter = DateTimeFormat
            .forPattern(REPORT_DATE_FORMAT);
    private DateTimeFormatter transactionsPeriodFormatter = DateTimeFormat
            .forPattern(CompletedTransaction.DATE_MONTH_FORMATTER);

    @Autowired
    public TransactionsService(DrugstoreHelper drugstoreHelper,
            DiscountHelper discountHelper) {

        this.drugstoreHelper = drugstoreHelper;
        this.discountHelper = discountHelper;

        initCompletedTransactionXmlAttributes();
        initRefundTransactionXmlAttributes();
    }

    private DateTime getDateTime(String date) throws IllegalArgumentException {
        return transactionReportFormatter.parseDateTime(date)
                .withZoneRetainFields(DateTimeZone.UTC);
    }

    public String convertDateToReportFormat(DateTime date) {
        return date.toString(transactionReportFormatter);
    }

    /**
     * Get refund transactions by specified date range
     * 
     * @param dateFrom report period begin date
     * @param dateTo report period end date (including)
     * 
     * @return refund transactions for specified date range
     * 
     * @throws IOException
     * @throws InvalidDateRangeException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws JsonMappingException
     * @throws JsonParseException
     * @throws ProvisionedThroughputExceededException
     */

    public List<Refund> getRefundTransactions(String dateFrom,
            String dateTo) throws IOException, InvalidDateRangeException, InstantiationException,
            IllegalAccessException {

        DateTime dateTimeFrom = getDateTime(dateFrom);

        DateTime dateTimeTo = getDateTime(dateTo);

        if (dateTimeFrom.isAfter(dateTimeTo))
            throw new InvalidDateRangeException("Invalid date range");

        dateTimeTo = dateTimeTo.plusMonths(1);

        List<Refund> transactions = new ArrayList<Refund>();
        String periodFrom = dateTimeFrom
                .toString(transactionsPeriodFormatter);
        String periodTo = dateTimeTo.toString(transactionsPeriodFormatter);
        List<Discount> discounts = discountHelper.getAllDiscounts();

        while (!periodFrom.equals(periodTo)
                && !dateTimeFrom.isAfter(dateTimeTo.getMillis())) {

            for (Discount discount : discounts) {
                transactions.addAll(drugstoreHelper
                        .getRefundTransactionsForPeriod(periodFrom,
                                discount.getId()));
            }

            dateTimeFrom = dateTimeFrom.plusMonths(1);
            periodFrom = dateTimeFrom.toString(transactionsPeriodFormatter);
        }

        return transactions;
    }

    /**
     * Get completed transactions by specified date range
     * 
     * @param dateFrom report period begin date
     * @param dateTo report period end date (including)
     * 
     * @return completed transactions for specified date range
     * 
     * @throws IOException
     * @throws InvalidDateRangeException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws JsonMappingException
     * @throws JsonParseException
     * @throws ProvisionedThroughputExceededException
     */
    @Deprecated
    public List<CompletedTransaction> getCompletedTransactions(String dateFrom,
            String dateTo) throws IOException, InvalidDateRangeException, ProvisionedThroughputExceededException,
            InstantiationException, IllegalAccessException {

        DateTime dateTimeFrom = getDateTime(dateFrom);

        DateTime dateTimeTo = getDateTime(dateTo);

        if (dateTimeFrom.isAfter(dateTimeTo))
            throw new InvalidDateRangeException("Invalid date range");

        dateTimeTo = dateTimeTo.plusMonths(1);

        List<CompletedTransaction> transactions = new ArrayList<CompletedTransaction>();
        String periodFrom = dateTimeFrom
                .toString(transactionsPeriodFormatter);
        String periodTo = dateTimeTo.toString(transactionsPeriodFormatter);

        while (!periodFrom.equals(periodTo)
                && !dateTimeFrom.isAfter(dateTimeTo.getMillis())) {

            transactions.addAll(drugstoreHelper
                    .getCompletedTransactionsForPeriod(periodFrom));

            dateTimeFrom = dateTimeFrom.plusMonths(1);
            periodFrom = dateTimeFrom.toString(transactionsPeriodFormatter);
        }

        return transactions;
    }

    private void writeRefundXmlElement(XMLStreamWriter writer,
            String localName,
            String value) throws XMLStreamException {

        if (value != null) {
            writer.writeAttribute(localName, value);
        } else if (xmlRefundAttributes.get(localName)) {
            writer.writeAttribute(localName, "");
        }
    }

    @Deprecated
    private void writeXmlElement(XMLStreamWriter writer, String localName,
            String value) throws XMLStreamException {

        if (value != null) {
            writer.writeAttribute(localName, value);
        } else if (xmlAttributeMap.get(localName)) {
            writer.writeAttribute(localName, "");
        }
    }

    public void writeXmlData(XMLStreamWriter writer, Refund transaction,
            DateTimeFormatter formatter) throws XMLStreamException {
        writer.writeStartElement("Transaction");

        writeRefundXmlElement(writer, ATTR_REFUND_PERIOD,
                transaction.getPeriod());
        writeRefundXmlElement(writer, ATTR_REFUND_TRANSACTION_ID,
                transaction.getRangeValue());
        writeRefundXmlElement(writer, ATTR_REFUND_VOUCHER_ID,
                transaction.getVoucherId());
        writeRefundXmlElement(writer, ATTR_REFUND_ROW, transaction.getRow());

        writeRefundXmlElement(writer, ATTR_REFUND_AUTH_CODE,
                transaction.getAuthCode());
        writeRefundXmlElement(writer, ATTR_REFUND_CARD_NUMBER,
                transaction.getCardId());
        writeRefundXmlElement(writer, ATTR_REFUND_DISCOUNT_ID,
                transaction.getDiscountId());
        writeRefundXmlElement(writer, ATTR_REFUND_DATETIME, transaction
                .getDateTime()
                .toString());

        writeRefundXmlElement(writer, ATTR_REFUND_ROW_PRICE, transaction
                .getRowPrice()
                .toString());
        writeRefundXmlElement(writer, ATTR_REFUND_ROW_COUNT, transaction
                .getRowCount()
                .toString());
        writeRefundXmlElement(writer, ATTR_REFUND_ROW_SUM_WITHOUT_DISCOUNT,
                transaction
                        .getRowSumWithoutDiscount().toString());
        writeRefundXmlElement(writer, ATTR_REFUND_ROW_DISCOUNT_PERCENT,
                transaction
                        .getRowDiscountPercent().toString());
        writeRefundXmlElement(writer, ATTR_REFUND_ROW_DISCOUNT_SUM, transaction
                .getRowDiscountSum().toString());
        writeRefundXmlElement(writer, ATTR_REFUND_ROW_PAID_SUM, transaction
                .getRowPaidSum()
                .toString());
        writeRefundXmlElement(writer, ATTR_REFUND_ROW_DRUGSTORE_REFUND,
                transaction
                        .getRowDrugstoreRefund().toString());

        writeRefundXmlElement(writer, ATTR_REFUND_DISCOUNT_PERCENT_ORIGINAL,
                transaction
                        .getDiscountPercentOriginal().toString());
        writeRefundXmlElement(writer, ATTR_REFUND_DISCOUNT_PERCENT_USED,
                transaction
                        .getDiscountPercentUsed().toString());
        writeRefundXmlElement(writer, ATTR_REFUND_DISCOUNT_REFUND_ORIGINAL_SUM,
                transaction
                        .getDiscountRefundOriginalSum().toString());
        writeRefundXmlElement(writer, ATTR_REFUND_DISCOUNT_REFUND_USED_SUM,
                transaction
                        .getDiscountRefundUsedSum().toString());
        writeRefundXmlElement(writer, ATTR_REFUND_DISCOUNT_REFUND_SUM,
                transaction.getDiscountRefundSum().toString());

        writeRefundXmlElement(writer, ATTR_REFUND_EXACT_REFUND_SUM,
                transaction.getDiscountRefundSum().toString());
        writeRefundXmlElement(writer, ATTR_REFUND_EXACT_NONREFUND_SUM,
                transaction.getDiscountRefundSum().toString());
        writeRefundXmlElement(writer, ATTR_REFUND_EXACT_SUM_TO_PAY,
                transaction.getDiscountRefundSum().toString());

        writeRefundXmlElement(writer, ATTR_REFUND_PACK_ID,
                transaction.getPackId());
        writeRefundXmlElement(writer, ATTR_REFUND_PACK_NAME,
                transaction.getPackName());
        writeRefundXmlElement(writer, ATTR_REFUND_CLIENT_PACK_ID,
                transaction.getClientPackId());
        writeRefundXmlElement(writer, ATTR_REFUND_CLIENT_PACK_NAME,
                transaction.getClientPackName());

        writeRefundXmlElement(writer, ATTR_REFUND_MANUFACTURER,
                transaction.getManufacturer());

        writeRefundXmlElement(writer, ATTR_REFUND_DRUGSTORE_ID,
                transaction.getDrugstoreId());
        writeRefundXmlElement(writer, ATTR_REFUND_DRUGSTORE_NAME,
                transaction.getDrugstoreName());

        writer.writeEndElement();
    }

    @Deprecated
    public void writeXmlData(XMLStreamWriter writer, CompletedTransaction data,
            DateTimeFormatter formatter) throws XMLStreamException {
        writer.writeStartElement("Transaction");
        writeXmlElement(writer, ATTR_PERIOD, data.getPeriod());
        writeXmlElement(writer, ATTR_DATETIME, data.getTransactionDateTime()
                .toString());
        writeXmlElement(writer, ATTR_TRANSACTION_ID, data.getRangeKey());
        writeXmlElement(writer, ATTR_VOUCHER_ID, data.getVoucherId());
        writeXmlElement(writer, ATTR_VOUCHER_ROW, data.getRangeKeyRow());
        writeXmlElement(writer, ATTR_DISCOUNT_PLAN_ID, data.getDiscountPlanId());
        writeXmlElement(writer, ATTR_DISCOUNT_PLAN_NAME,
                data.getDiscountPlanName());
        writeXmlElement(writer, ATTR_CARD, data.getCardNumber());
        writeXmlElement(writer, ATTR_DRUGSTORE_ID, data.getDrugstoreId());
        writeXmlElement(writer, ATTR_DRUGSTORE_NAME, data.getDrugstoreName());
        writeXmlElement(writer, ATTR_CLIENT_PACK_ID, data.getClientPackId());
        writeXmlElement(writer, ATTR_CLIENT_PACK_NAME, data.getClientPackName());
        writeXmlElement(writer, ATTR_PACK_EKT, data.getPackId());
        writeXmlElement(writer, ATTR_PACK_BARCODE, data.getPackBarcode());
        writeXmlElement(writer, ATTR_PACK_NAME, data.getPackName());
        writeXmlElement(writer, ATTR_COUNT, data.getPackCount().toString());
        writeXmlElement(writer, ATTR_PRICE, data.getPrice().toString());
        writeXmlElement(writer, ATTR_SUM_WITHOUT_DISCOUNT,
                data.getSumWithoutDiscount().toString());
        writeXmlElement(writer, ATTR_DISCOUNT_VALUE, data.getDiscountValue()
                .toString());
        writeXmlElement(writer, ATTR_DISCOUNT_SUM, data.getDiscountSum()
                .toString());
        writeXmlElement(writer, ATTR_SUM_TO_PAY, data.getSumWithoutDiscount()
                .subtract(data.getDiscountSum()).toString());
        writeXmlElement(writer, ATTR_REFUND, data.getRefundSum().toString());
        writeXmlElement(writer, ATTR_AUTH_CODE, data.getAuthCode());
        writeXmlElement(writer, ATTR_MANUFACTURER, data.getManufacturer());
        writeXmlElement(writer, ATTR_INN, data.getInn());

        writer.writeEndElement();
    }

    public Object[] getRefundTransactionAsArray(Refund transaction) {
        return new Object[] {
                transaction.getPeriod(),
                transaction.getRangeValue(),
                transaction.getVoucherId(),
                transaction.getRow(),

                transaction.getAuthCode(),
                transaction.getCardId(),
                transaction.getDiscountId(),
                transaction.getDateTime(),

                transaction.getRowPrice(),
                transaction.getRowCount(),
                transaction.getRowSumWithoutDiscount(),
                transaction.getRowDiscountPercent(),
                transaction.getRowDiscountSum(),
                transaction.getRowPaidSum(),
                transaction.getRowDrugstoreRefund(),

                transaction.getDiscountPercentOriginal(),
                transaction.getDiscountPercentUsed(),
                transaction.getDiscountRefundOriginalSum(),
                transaction.getDiscountRefundUsedSum(),
                transaction.getDiscountRefundSum(),

                transaction.getPackId(),
                transaction.getPackName(),
                transaction.getClientPackId(),
                transaction.getClientPackName(),

                transaction.getManufacturer(),

                transaction.getDrugstoreId(),
                transaction.getDrugstoreName()
        };
    }

    @Deprecated
    public Object[] getCompletedTransactionAsArray(
            CompletedTransaction transaction) {
        return new Object[] {
                transaction.getPeriod(),
                transaction.getRangeKey(),
                transaction.getVoucherId(),
                transaction.getRangeKeyRow(),
                transaction.getTransactionDateTime(),
                transaction.getDiscountPlanId(),
                transaction.getDiscountPlanName(),
                transaction.getCardNumber(),
                transaction.getDrugstoreId(),
                transaction.getDrugstoreName(),
                transaction.getPackBarcode(),
                transaction.getPackName(),
                transaction.getPackCount(),
                transaction.getPrice(),
                transaction.getSumWithoutDiscount(),
                transaction.getDiscountValue(),
                transaction.getDiscountSum(),
                transaction.getRefundSum(),
                transaction.getSumWithoutDiscount().subtract(
                        transaction.getDiscountSum()),
                transaction.getManufacturer(), transaction.getInn(),
                transaction.getAuthCode(),
                transaction.getClientPackId(),
                transaction.getClientPackName()};
    }

    /**
     * Generate temp file path and name for transactions
     * 
     * @param fileName
     * @return temp file path
     */
    public String generateTransactionsFilePath(String fileName, String fileType) {
        String directoryForFiles = System.getProperty("user.home")
                + TRANSACTIONS_FOLDER + fileType;
        String resultFile = "";
        File dir = new File(directoryForFiles);

        if (!dir.exists())
            dir.mkdirs();

        File[] files = dir.listFiles();
        if (files.length < TRANSACTIONS_MAX_UNLOAD_FILES) {
            resultFile = directoryForFiles;
            resultFile += "/";
            resultFile += fileName;
            resultFile += ".";
            resultFile += files.length;
        } else {
            Arrays.sort(files, new Comparator<File>() {
                public int compare(File f1, File f2) {
                    return Long.valueOf(f2.lastModified()).compareTo(
                            f1.lastModified());
                }
            });
            resultFile = files[files.length - 1].getAbsolutePath();
            File clearFile = new File(resultFile);
            clearFile.delete();
        }

        return resultFile;
    }

    /**
     * Get transactions file index
     * 
     * @return report file index
     */
    public String getTransactionsFileIndex(String filePath) {
        String[] parsedName = filePath.split("\\.");
        return parsedName[parsedName.length - 1];
    }

    /**
     * Get tmp transactions file path to store
     * 
     * @param fileType
     * @param fileIndex
     * 
     * @return temp transactions path
     * @throws Exception
     */
    public String getTransactionsFilePath(String fileType, String fileIndex)
            throws Exception {

        if (Integer.parseInt(fileIndex) < 0
                || Integer.parseInt(fileIndex) >= TRANSACTIONS_MAX_UNLOAD_FILES)
            throw new Exception("Incorect transactions report index");

        String fileName = "";
        if (fileType.equalsIgnoreCase(CSV_FILE_TYPE)) {
            fileName = TRANSACTIONS_FILE_CSV;
        } else if (fileType.equalsIgnoreCase(XML_FILE_TYPE)) {
            fileName = TRANSACTIONS_FILE_XML;
        }

        return System.getProperty("user.home") + TRANSACTIONS_FOLDER
                + fileType + "/"
                + fileName + "." + fileIndex;
    }

    /**
     * Load bytes from specified file path
     * 
     * @param filePath
     * @return
     */
    public byte[] loadTransactionsFile(String filePath)
            throws FileNotFoundException,
            IOException {
        byte[] data = null;
        File inputFile = new File(filePath);
        Long fileSize = inputFile.length();
        data = new byte[Integer.parseInt(fileSize.toString())];
        FileInputStream fis = new FileInputStream(inputFile);
        fis.read(data, 0, data.length);
        fis.close();
        return data;
    }

    /**
     * Return link to transactions file
     * 
     * @param url
     * @param fileType
     * @param reportFileIndex
     * @return HTML-formated link to transactions report file
     */
    public String getTransactionsFileLink(String url, String fileType,
            String reportFileIndex) {
        return url + "/" + fileType + "/" + reportFileIndex;
    }

    /**
     * Get Refund transaction headers
     * 
     * @return Array with headers
     */
    public String[] getRefundTransactionHeaders() {
        return new String[] {
                ATTR_REFUND_PERIOD,
                ATTR_REFUND_TRANSACTION_ID,
                ATTR_REFUND_VOUCHER_ID,
                ATTR_REFUND_ROW,

                ATTR_REFUND_AUTH_CODE,
                ATTR_REFUND_CARD_NUMBER,
                ATTR_REFUND_DISCOUNT_ID,
                ATTR_REFUND_DATETIME,

                ATTR_REFUND_ROW_PRICE,
                ATTR_REFUND_ROW_COUNT,
                ATTR_REFUND_ROW_SUM_WITHOUT_DISCOUNT,
                ATTR_REFUND_ROW_DISCOUNT_PERCENT,
                ATTR_REFUND_ROW_DISCOUNT_SUM,
                ATTR_REFUND_ROW_PAID_SUM,
                ATTR_REFUND_ROW_DRUGSTORE_REFUND,

                ATTR_REFUND_DISCOUNT_PERCENT_ORIGINAL,
                ATTR_REFUND_DISCOUNT_PERCENT_USED,
                ATTR_REFUND_DISCOUNT_REFUND_ORIGINAL_SUM,
                ATTR_REFUND_DISCOUNT_REFUND_USED_SUM,
                ATTR_REFUND_DISCOUNT_REFUND_SUM,
                ATTR_REFUND_EXACT_REFUND_SUM,
                ATTR_REFUND_EXACT_NONREFUND_SUM,
                ATTR_REFUND_EXACT_SUM_TO_PAY,

                ATTR_REFUND_PACK_ID,
                ATTR_REFUND_PACK_NAME,
                ATTR_REFUND_CLIENT_PACK_ID,
                ATTR_REFUND_CLIENT_PACK_NAME,

                ATTR_REFUND_MANUFACTURER,

                ATTR_REFUND_DRUGSTORE_ID,
                ATTR_REFUND_DRUGSTORE_NAME};
    }

    /**
     * Get completed transaction headers
     * 
     * @return Array with headers
     */
    /*
     * @Deprecated public String[] getCompletedTransactionHeaders() { return new
     * String[] {FLD_PERIOD, FLD_TRANSACTION_ID, FLD_VOUCHER_ID,
     * FLD_VOUCHER_ROW, FLD_DATE_TIME, FLD_DISCOUNT_PLAN_ID,
     * FLD_DISCOUNT_PLAN_NAME, FLD_CARD_ID, FLD_DRUGSTORE_ID,
     * FLD_DRUGSTORE_NAME, FLD_PACK_BARCODE, FLD_PACK_NAME, FLD_PACK_COUNT,
     * FLD_PACK_PRICE, FLD_SUM_WITHOUT_DISCOUNT, FLD_DISCOUNT_VALUE,
     * FLD_DISCOUNT_SUM, FLD_REFUND_SUM, "SumToPay", FLD_MANUFACTURER, FLD_INN,
     * FLD_AUTH_CODE, FLD_CLIENT_PACK_ID, FLD_CLIENT_PACK_NAME}; }
     */

    private void initRefundTransactionXmlAttributes() {

        xmlRefundAttributes = new HashMap<String, Boolean>();

        xmlRefundAttributes.put(ATTR_REFUND_PERIOD, true);
        xmlRefundAttributes.put(ATTR_REFUND_TRANSACTION_ID, true);
        xmlRefundAttributes.put(ATTR_REFUND_VOUCHER_ID, true);
        xmlRefundAttributes.put(ATTR_REFUND_ROW, true);

        xmlRefundAttributes.put(ATTR_REFUND_AUTH_CODE, true);
        xmlRefundAttributes.put(ATTR_REFUND_CARD_NUMBER, true);
        xmlRefundAttributes.put(ATTR_REFUND_DISCOUNT_ID, true);
        xmlRefundAttributes.put(ATTR_REFUND_DATETIME, true);

        xmlRefundAttributes.put(ATTR_REFUND_ROW_PRICE, true);
        xmlRefundAttributes.put(ATTR_REFUND_ROW_COUNT, true);
        xmlRefundAttributes.put(ATTR_REFUND_ROW_SUM_WITHOUT_DISCOUNT, true);
        xmlRefundAttributes.put(ATTR_REFUND_ROW_DISCOUNT_PERCENT, true);
        xmlRefundAttributes.put(ATTR_REFUND_ROW_DISCOUNT_SUM, true);
        xmlRefundAttributes.put(ATTR_REFUND_ROW_PAID_SUM, true);
        xmlRefundAttributes.put(ATTR_REFUND_ROW_DRUGSTORE_REFUND, true);

        xmlRefundAttributes.put(ATTR_REFUND_DISCOUNT_PERCENT_ORIGINAL, true);
        xmlRefundAttributes.put(ATTR_REFUND_DISCOUNT_PERCENT_USED, true);
        xmlRefundAttributes.put(ATTR_REFUND_DISCOUNT_REFUND_ORIGINAL_SUM, true);
        xmlRefundAttributes.put(ATTR_REFUND_DISCOUNT_REFUND_SUM, true);
        xmlRefundAttributes.put(ATTR_REFUND_DISCOUNT_REFUND_USED_SUM, true);

        xmlRefundAttributes.put(ATTR_REFUND_EXACT_REFUND_SUM, true);
        xmlRefundAttributes.put(ATTR_REFUND_EXACT_NONREFUND_SUM, true);
        xmlRefundAttributes.put(ATTR_REFUND_EXACT_SUM_TO_PAY, true);

        xmlRefundAttributes.put(ATTR_REFUND_PACK_ID, false);
        xmlRefundAttributes.put(ATTR_REFUND_PACK_NAME, false);
        xmlRefundAttributes.put(ATTR_REFUND_CLIENT_PACK_ID, true);
        xmlRefundAttributes.put(ATTR_REFUND_CLIENT_PACK_NAME, true);

        xmlRefundAttributes.put(ATTR_REFUND_MANUFACTURER, true);

        xmlRefundAttributes.put(ATTR_REFUND_DRUGSTORE_ID, true);
        xmlRefundAttributes.put(ATTR_REFUND_DRUGSTORE_NAME, true);
    }

    @Deprecated
    private void initCompletedTransactionXmlAttributes() {

        xmlAttributeMap = new HashMap<String, Boolean>();
        xmlAttributeMap.put(ATTR_PERIOD, true);
        xmlAttributeMap.put(ATTR_DATETIME, true);
        xmlAttributeMap.put(ATTR_TRANSACTION_ID, true);
        xmlAttributeMap.put(ATTR_VOUCHER_ID, true);
        xmlAttributeMap.put(ATTR_VOUCHER_ROW, true);
        xmlAttributeMap.put(ATTR_DISCOUNT_PLAN_ID, true);
        xmlAttributeMap.put(ATTR_DISCOUNT_PLAN_NAME, false);
        xmlAttributeMap.put(ATTR_CARD, true);
        xmlAttributeMap.put(ATTR_DRUGSTORE_ID, true);
        xmlAttributeMap.put(ATTR_DRUGSTORE_NAME, false);
        xmlAttributeMap.put(ATTR_CLIENT_PACK_ID, true);
        xmlAttributeMap.put(ATTR_CLIENT_PACK_NAME, true);
        xmlAttributeMap.put(ATTR_PACK_EKT, false);
        xmlAttributeMap.put(ATTR_PACK_BARCODE, false);
        xmlAttributeMap.put(ATTR_PACK_NAME, false);
        xmlAttributeMap.put(ATTR_COUNT, true);
        xmlAttributeMap.put(ATTR_PRICE, true);
        xmlAttributeMap.put(ATTR_SUM_WITHOUT_DISCOUNT, true);
        xmlAttributeMap.put(ATTR_DISCOUNT_VALUE, true);
        xmlAttributeMap.put(ATTR_DISCOUNT_SUM, true);
        xmlAttributeMap.put(ATTR_SUM_TO_PAY, true);
        xmlAttributeMap.put(ATTR_REFUND, true);
        xmlAttributeMap.put(ATTR_AUTH_CODE, true);
        xmlAttributeMap.put(ATTR_MANUFACTURER, false);
        xmlAttributeMap.put(ATTR_INN, false);
    }
}