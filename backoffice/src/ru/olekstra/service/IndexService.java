package ru.olekstra.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.domain.IndexEntity;
import ru.olekstra.helper.IndexHelper;

@Service
public class IndexService {

    private IndexHelper helper;

    @Autowired
    public IndexService(IndexHelper helper) {
        this.helper = helper;
    }

    /**
     * Добавляет новую запись в таблицу индексов (ключ-значение)
     * 
     * @param record запись IndexEntity
     * @return true если успешно добавлено
     * @throws ItemSizeLimitExceededException
     */
    public boolean addNewIndex(IndexEntity record) throws ItemSizeLimitExceededException {
        return helper.addNewRecord(record);
    }

    /**
     * Проверяет, существует ли указанный ключ
     * 
     * @param index ключ который ищется
     * @param prefix префикс в котором ищется ключ
     * @return true если ключ уже есть в таблице
     */
    public boolean ifIndexExists(String index, String prefix) {
        Map<String, Object> result = helper.getIndex(index, prefix);
        return result.size() > 0;
    }

    /**
     * Возвращает запись по указанному ключу
     * 
     * @param index поисковый ключ
     * @param prefix префикс в котором ищем запись
     * @return найденная запись
     */
    public Map<String, Object> getIndex(String index, String prefix) {
        return helper.getIndex(index, prefix);
    }

    /**
     * Удаляет указанный индекс
     * 
     * @param index ключ записи
     * @param prefix префикс в котром ищем указанный ключ
     */
    public void deleteIndex(String index, String prefix) {
        helper.deleteIndex(index, prefix);
    }

}
