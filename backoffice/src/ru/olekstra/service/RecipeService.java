package ru.olekstra.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.olekstra.awsutils.DynamodbEntity;
import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.S3Service;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.awsutils.exception.ReporNotFoundException;
import ru.olekstra.awsutils.exception.S3ConnectionException;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.common.service.DateTimeService;
import ru.olekstra.domain.AuthCodeIndex;
import ru.olekstra.domain.CompletedTransaction;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.Recipe;
import ru.olekstra.domain.RecipeState;
import ru.olekstra.domain.dto.RecipeRecord;
import ru.olekstra.exception.NotSavedException;
import ru.olekstra.helper.RecipeHelper;
import ru.olekstra.service.dto.RecipeReportDto;
import ru.olekstra.service.dto.RecipeReportGroupDto;
import ru.olekstra.service.dto.ReportLinkDto;

import com.amazonaws.services.dynamodb.model.Key;
import com.amazonaws.services.dynamodb.model.ProvisionedThroughputExceededException;

@Service
public class RecipeService {

    public static final String IMAGE_FILE_FORMAT_JPG = "jpg";
    public static final String IMAGE_FILE_FORMAT_JPEG = "jpeg";
    public static final String IMAGE_FILE_FORMAT_PNG = "png";
    public static final String IMAGE_FILE_FORMAT_DELIMETER = "\\.";

    private static final Byte[] JPG_FILE_DESCRIPTOR = {(byte) 0xFF,
            (byte) 0xD8, (byte) 0xFF, (byte) 0xE0};
    private static final Byte[] PNG_FILE_DESCRIPTOR = {(byte) 0x89,
            (byte) 0x50, (byte) 0x4E, (byte) 0x47};

    private static Map<String, Byte[]> formatMap;

    private DateTimeFormatter reportMonthFormatter = DateTimeFormat
            .forPattern("MMMM yyyy");
    private DateTimeFormatter recipeDateParser = DateTimeFormat
            .forPattern("MM.yyyy");

    private DateTimeFormatter recipeDateFormatter = DateTimeFormat
            .forPattern(CompletedTransaction.DATE_MONTH_FORMATTER);
    private DateTimeFormatter processingTimeFormatter = DateTimeFormat
            .forPattern(DynamodbEntity.DB_DATETIME_FORMAT);

    private RecipeHelper helper;
    private S3Service s3Service;
    private AppSettingsService settings;
    private DynamodbService dynamoService;
    private ReportService reportService;
    private DateTimeService dateTimeService;

    @Autowired
    public RecipeService(RecipeHelper helper, S3Service s3Service,
            AppSettingsService settings, DynamodbService dynamoService,
            ReportService reportService, DateTimeService dateTimeService) {
        this.helper = helper;
        this.s3Service = s3Service;
        this.settings = settings;
        this.dynamoService = dynamoService;
        this.reportService = reportService;
        this.dateTimeService = dateTimeService;
        formatMap = new HashMap<String, Byte[]>();
        formatMap.put(IMAGE_FILE_FORMAT_JPG, JPG_FILE_DESCRIPTOR);
        formatMap.put(IMAGE_FILE_FORMAT_JPEG, JPG_FILE_DESCRIPTOR);
        formatMap.put(IMAGE_FILE_FORMAT_PNG, PNG_FILE_DESCRIPTOR);
    }

    public boolean isValidImageFormat(String fileName) {
        if (fileName != null && !fileName.isEmpty()) {
            String[] parsedFileName = fileName
                    .split(IMAGE_FILE_FORMAT_DELIMETER);
            String formatExtension = parsedFileName[parsedFileName.length - 1];
            if (formatExtension.equalsIgnoreCase(IMAGE_FILE_FORMAT_JPG)
                    || formatExtension.equalsIgnoreCase(IMAGE_FILE_FORMAT_PNG)) {
                return true;
            }
        }
        return false;
    }

    public boolean isValidImageFormat(InputStream stream, String extension)
            throws IOException {
        byte[] fileDescriptor = new byte[4];
        stream.read(fileDescriptor, 0, 4);
        stream.reset();
        Byte[] formatDescriptor = formatMap.get(extension);

        boolean eq = true;

        if (formatDescriptor != null)
            for (int i = 0; i < 4; i++) {
                if (fileDescriptor[i] != formatDescriptor[i]) {
                    eq = false;
                    break;
                }
            }
        else
            eq = false;
        return eq;
    }

    public void saveRecipeImage(String filePath, InputStream image) {
        String key = filePath;
        String bucket = settings.getRecipeBucketName();
        s3Service.upload(bucket, key, image);
        s3Service.addPublicReadAccess(bucket, key);
    }

    public String getImageExtension(String fileName) {
        String[] parsedFileName = fileName.split(IMAGE_FILE_FORMAT_DELIMETER);
        return parsedFileName[parsedFileName.length - 1];
    }

    public String genarateImageName(String uuid, String fileExtension) {
        return uuid + "." + fileExtension;
    }

    public String convertRecipePeriod(String period) {
        DateTime datetime = recipeDateParser.parseDateTime(period);
        return datetime.toString(recipeDateFormatter);
    }

    public String getRecipeFilePath(String period, String drugstoreId,
            String fileName) {
        return period + "/" + drugstoreId + "/" + fileName;
    }

    public boolean saveRecipe(Recipe recipe)
            throws ItemSizeLimitExceededException {
        return helper.addRecipe(recipe);
    }

    /**
     * Обновляет статус рецепта
     * 
     * @param recipe рецепт
     * @param newState новый статус
     * @param note комментарий к изменению статуса
     * @param user пользователь производящий смену статуса
     * @throws ItemSizeLimitExceededException
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     * @throws NotSavedException
     */
    public void updateRecipeState(Recipe recipe, String newState, String note,
            String user) throws ItemSizeLimitExceededException,
            JsonGenerationException, JsonMappingException, IOException,
            NotSavedException {

        RecipeState recipeState = new RecipeState(recipe.getPeriod(), newState,
                recipe.getUUID(), recipe.getDrugstoreId(), recipe.getAuthCode());
        // если только создаем рецепт, то добавляем дату создания
        if (newState.equals(Recipe.STATE_NEW)) {
            recipeState.setCreateDate(DateTime.now());
            recipeState.setFileType(recipe.getFileType());
        } else { // если уже был рецепт то переносим из старого состояния
            RecipeState tmpRecipeState = helper.getRecipeState(recipe
                    .getState(), recipe.getPeriod(), recipe.getUUID());
            recipeState
                    .setCreateDate(tmpRecipeState.getCreateDate() != null ? tmpRecipeState
                            .getCreateDate()
                            : DateTime.now());
            recipeState.setFileType(tmpRecipeState.getFileType());
        }
        RecipeRecord record = new RecipeRecord(dateTimeService.createDefaultDateTime(), newState, user,
                note);

        helper.deleteRecipeState(recipe.getState(), recipe.getPeriod(), recipe
                .getUUID());
        boolean isAddState = helper.addRecipeState(recipeState);
        boolean isSaveHistory = helper.saveHistory(recipe.getPeriod(),
                recipe.getUUID(), DynamodbEntity.toJson(record));
        boolean isUdateState = true;

        if (!recipe.getState().equals(newState)) {
            isUdateState = helper.updateRecipeState(recipe.getPeriod(),
                    recipe.getUUID(), recipe.getState(), newState);
        }

        if (isAddState & isSaveHistory & isUdateState)
            return;
        else
            throw new NotSavedException("DB operation error");
    }

    public void updateRecipeAuthcode(String period, String uuid, String authcode) {
        helper.saveRecipeAuthcode(period, uuid, authcode);
    }

    /**
     * Возвращает первый незаблокированный рецепт с указанным статусом и
     * блокирует его на взявшем его операторе
     * 
     * @param state статус рецепта
     * @param operatorName имя оператора
     * @return объект RecipeStatus
     * @throws NotSavedException
     * @throws IOException
     */
    public RecipeState getNextRecipeWithStateX(String state, String operatorName)
            throws NotSavedException, IOException {

        if (operatorName == null)
            throw new IllegalArgumentException(
                    "parameter String operatorName can't be null");

        Map<String, Object> recipeStateMap;
        Key lastKeyEvaluated = null;

        // в цикле ищем первый подходящий, выбирая из БД по одному
        do {
            recipeStateMap = helper.getNextRecipeWithStateX(state,
                    lastKeyEvaluated);
            lastKeyEvaluated = (Key) recipeStateMap
                    .get(DynamodbService.LAST_EVALUATED_KEY);
        } while (isRecipeFound(operatorName, recipeStateMap));

        // записываем кто взял и время
        if (!recipeStateMap.isEmpty()) {
            // правим карту для записи
            recipeStateMap.remove(DynamodbService.LAST_EVALUATED_KEY);
            recipeStateMap.put(RecipeState.FLD_PROCESSING_STARTED,
                    DateTimeFormat
                            .forPattern(DynamodbEntity.DB_DATETIME_FORMAT)
                            .print(DateTime.now()));
            if (!helper.saveOperatorAndTime(recipeStateMap, operatorName))
                throw new NotSavedException(
                        "concurrent modification DynamoDb data exception");
        }

        if (!recipeStateMap.isEmpty())
            return new RecipeState(recipeStateMap);
        else
            return null;
    }

    private boolean isRecipeFound(String operatorName,
            Map<String, Object> recipeStateMap) {

        boolean found;

        if (recipeStateMap.isEmpty()) {
            found = false;
        } else {
            String processingBy = (String) recipeStateMap
                    .get(RecipeState.FLD_PROCESSING_BY);
            String processingStarted = (String) recipeStateMap
                    .get(RecipeState.FLD_PROCESSING_STARTED);
            if (operatorName.equalsIgnoreCase(processingBy)
                    || isRecipeExpired(processingStarted))
                found = false;
            else
                found = true;
        }

        return found;
    }

    private boolean isRecipeExpired(String dateTime) {
        DateTime expireDateTime;
        if (dateTime != null)
            expireDateTime = processingTimeFormatter.parseDateTime(dateTime)
                    .plusMinutes(60);
        else
            return true;
        return expireDateTime.isBeforeNow();
    }

    /**
     * Получаем транзакцию по периоду, аптеке и коду через промежуточную таблицу
     * индексов, которая связывает рецепты и транзакции проведенными по ним
     * 
     * @param period период
     * @param drugstoreId код аптеки
     * @param authCode код авторизации рецепта
     * @return processing transaction
     */
    public ProcessingTransaction getTransaction(String period,
            String drugstoreId, String authCode) {
        AuthCodeIndex index = dynamoService.getObject(AuthCodeIndex.class,
                AuthCodeIndex.composeKey(period, authCode, drugstoreId),
                AuthCodeIndex.prefix);
        ProcessingTransaction transaction = null;
        if (index != null) {
            transaction = dynamoService.getObject(ProcessingTransaction.class,
                    drugstoreId, index.getTransactionRangeKey());
        }
        return transaction;
    }

    /**
     * Получает из БД рецепт по периоду и uuid рецепта
     * 
     * @param period период
     * @param uuid уникальный идентификатор рецепта
     * @return объект Recipe
     */
    public Recipe getRecipe(String period, String uuid) {
        return helper.getRecipe(period, uuid);
    }

    /**
     * Сохраняем причину отклонения рецепта
     * 
     * @param period период рецепта
     * @param uuid уникальный идентификатор рецепта
     * @param rejectReason причина отклонения рецепта
     * @return признак успешности записи
     */
    public boolean saveRecipeRejectReason(String period, String uuid,
            String rejectReason) {
        return helper.saveRecipeRejectReason(period, uuid, rejectReason);
    }

    /**
     * Возвращает список рецептов, находящихся в работе (статусы new, parsed,
     * problem)
     * 
     * @param period
     * @return список рецептов
     * @throws IOException
     * @throws ProvisionedThroughputExceededException
     */
    public List<RecipeState> getActiveRecipesForPeriod(String period) throws ProvisionedThroughputExceededException,
            IOException {
        List<RecipeState> list = helper.getActiveRecipesForPeriod(period);
        if (list != null && list.size() > 0) {
            Collections.sort(list);
        } else {
            list = new ArrayList<RecipeState>();
        }
        return list;
    }

    public String getHtmlReportList(String period)
            throws S3ConnectionException, ReporNotFoundException, IOException {
        return reportService.getHtmlReportList(period,
                ReportService.RECIPE_REPORT_TYPE);
    }

    public void generateReport(String period) throws RuntimeException, IOException {

        List<String> s3RecipeFiles = s3Service.getChildsByParent(
                AppSettingsService.DEFAULT_RECIPE_REPORT_BUCKET, period);

        List<Recipe> recipes = helper
                .getRecipesForPeriod(period, s3RecipeFiles);
        Collections.sort(recipes);

        String[] recipesPeriod = new String[recipes.size()];
        Arrays.fill(recipesPeriod, period);

        Map<String, List<Recipe>> recipesByDrugstore = getRecipesByDrugstore(recipes);
        List<ReportLinkDto> links = new ArrayList<ReportLinkDto>();
        String month = recipeDateFormatter.parseDateTime(period).toString(
                reportMonthFormatter);

        for (String drugstore : recipesByDrugstore.keySet()) {

            List<Recipe> recipesDrugstore = recipesByDrugstore.get(drugstore);
            RecipeReportGroupDto recipeGroup = this.makeReportRecipeGroup(
                    period,
                    month, drugstore, recipesDrugstore);

            URL url = reportService.saveReportToS3(period, drugstore,
                    ReportService.RECIPE_REPORT_TYPE, recipeGroup, null);

            if (url != null) {
                links.add(new ReportLinkDto(period, drugstore, url));
            }
        }

        reportService.saveReportListPageToS3(period,
                ReportService.RECIPE_REPORT_TYPE, links);
    }

    public RecipeReportGroupDto makeReportRecipeGroup(String period,
            String month, String drugstore, List<Recipe> recipes) {

        int total = 0;
        int refund = 0;
        int rejected = 0;
        int processing = 0;
        List<RecipeReportDto> recipesView = new ArrayList<RecipeReportDto>(
                recipes.size());

        for (Recipe recipe : recipes) {
            if (recipe.getState().equals(Recipe.STATE_CHECKED)) {
                refund++;
            } else if (recipe.getState().equals(Recipe.STATE_REJECTED)) {
                rejected++;
            } else {
                processing++;
            }
            total++;

            String fileExtension = recipe.getFileType();
            String fileName = this.genarateImageName(recipe.getUUID(),
                    fileExtension);
            String filePath = this.getRecipeFilePath(period, recipe
                    .getDrugstoreId(), fileName);
            URL url = s3Service.getUrl(
                    AppSettingsService.DEFAULT_RECIPE_REPORT_BUCKET, filePath);

            recipesView.add(new RecipeReportDto(url.toString(), recipe
                    .getState(), recipe.getRejectReason()));
        }

        return new RecipeReportGroupDto(recipesView, drugstore, month,
                dateTimeService.createDefaultDateTime(), total, refund, rejected, processing);
    }

    public Map<String, List<Recipe>> getRecipesByDrugstore(List<Recipe> recipes) {
        Map<String, List<Recipe>> recipesByDrugstore = new HashMap<String, List<Recipe>>();
        for (Recipe recipe : recipes) {
            if (recipesByDrugstore.get(recipe.getDrugstoreId()) == null) {
                recipesByDrugstore.put(recipe.getDrugstoreId(),
                        new ArrayList<Recipe>());
            }
            recipesByDrugstore.get(recipe.getDrugstoreId()).add(recipe);
        }
        return recipesByDrugstore;
    }

    /**
     * Возвращает RecipeState
     * 
     * @param period период рецепта
     * @param uuid uuid рецепта
     * @return объект RecipeState из бд
     */
    public RecipeState getRecipeState(String period, String uuid) {
        Recipe recipe = helper.getRecipe(period, uuid);
        RecipeState recipeState = null;
        if (recipe != null) {
            recipeState = helper
                    .getRecipeState(recipe.getState(), period, uuid);
        }
        return recipeState;
    }
}
