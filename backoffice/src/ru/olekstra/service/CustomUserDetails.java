package ru.olekstra.service;

import static ru.olekstra.domain.User.ROLE_ADMINISTRATION;
import static ru.olekstra.domain.User.ROLE_CARD_BALANCE;
import static ru.olekstra.domain.User.ROLE_CARD_GENERAL;
import static ru.olekstra.domain.User.ROLE_CARD_VIEW;
import static ru.olekstra.domain.User.ROLE_DRUGSTORE_EDIT;
import static ru.olekstra.domain.User.ROLE_DRUGSTORE_REPORTS;
import static ru.olekstra.domain.User.ROLE_DRUGSTORE_VIEW;
import static ru.olekstra.domain.User.ROLE_HELPDESK;
import static ru.olekstra.domain.User.ROLE_MANUFACTURER_REPORTS;
import static ru.olekstra.domain.User.ROLE_RECIPE_PARSE;
import static ru.olekstra.domain.User.ROLE_RECIPE_PROBLEM;
import static ru.olekstra.domain.User.ROLE_RECIPE_RECEIVE;
import static ru.olekstra.domain.User.ROLE_RECIPE_VERIFY;
import static ru.olekstra.domain.User.ROLE_TRANSACTIONS_EXPORT;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class CustomUserDetails extends User {

    /**
     * 
     */
    private static final long serialVersionUID = -6260242448772409903L;

    public static final GrantedAuthority AUTHORITY_CARD_VIEW = new SimpleGrantedAuthority(
            ROLE_CARD_VIEW);
    public static final GrantedAuthority AUTHORITY_CARD_GENERAL = new SimpleGrantedAuthority(
            ROLE_CARD_GENERAL);
    public static final GrantedAuthority AUTHORITY_CARD_BALANCE = new SimpleGrantedAuthority(
            ROLE_CARD_BALANCE);
    public static final GrantedAuthority AUTHORITY_DRUGSTORE_VIEW = new SimpleGrantedAuthority(
            ROLE_DRUGSTORE_VIEW);
    public static final GrantedAuthority AUTHORITY_DRUGSTORE_EDIT = new SimpleGrantedAuthority(
            ROLE_DRUGSTORE_EDIT);
    public static final GrantedAuthority AUTHORITY_DRUGSTORE_REPORTS = new SimpleGrantedAuthority(
            ROLE_DRUGSTORE_REPORTS);
    public static final GrantedAuthority AUTHORITY_MANUFACTURER_REPORTS = new SimpleGrantedAuthority(
            ROLE_MANUFACTURER_REPORTS);
    public static final GrantedAuthority AUTHORITY_HELPDESK = new SimpleGrantedAuthority(
            ROLE_HELPDESK);
    public static final GrantedAuthority AUTHORITY_TRANSACTIONS_EXPORT = new SimpleGrantedAuthority(
            ROLE_TRANSACTIONS_EXPORT);
    public static final GrantedAuthority AUTHORITY_ADMINISTRATION = new SimpleGrantedAuthority(
            ROLE_ADMINISTRATION);
    public static final GrantedAuthority AUTHORITY_RECIPE_RECEIVE = new SimpleGrantedAuthority(
            ROLE_RECIPE_RECEIVE);
    public static final GrantedAuthority AUTHORITY_RECIPE_PARSE = new SimpleGrantedAuthority(
            ROLE_RECIPE_PARSE);
    public static final GrantedAuthority AUTHORITY_RECIPE_VERIFY = new SimpleGrantedAuthority(
            ROLE_RECIPE_VERIFY);
    public static final GrantedAuthority AUTHORITY_RECIPE_PROBLEM = new SimpleGrantedAuthority(
            ROLE_RECIPE_PROBLEM);

    private String email;
    private String name;
    private boolean newUser;

    public CustomUserDetails(String username,
            Collection<GrantedAuthority> authorities) {
        super(username, "unused", authorities);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isNewUser() {
        return newUser;
    }

    public void setNewUser(boolean newUser) {
        this.newUser = newUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean hasRoleCardView() {
        return getAuthorities().contains(AUTHORITY_CARD_VIEW);
    }

    public boolean hasRoleCardGeneral() {
        return getAuthorities().contains(AUTHORITY_CARD_GENERAL);
    }

    public boolean hasRoleCardBalance() {
        return getAuthorities().contains(AUTHORITY_CARD_BALANCE);
    }

    public boolean hasRoleDrugstoreView() {
        return getAuthorities().contains(AUTHORITY_DRUGSTORE_VIEW);
    }

    public boolean hasRoleDrugstoreEdit() {
        return getAuthorities().contains(AUTHORITY_DRUGSTORE_EDIT);
    }

    public boolean hasRoleDrugstoreReports() {
        return getAuthorities().contains(AUTHORITY_DRUGSTORE_REPORTS);
    }

    public boolean hasRoleManufacturerReports() {
        return getAuthorities().contains(AUTHORITY_MANUFACTURER_REPORTS);
    }

    public boolean hasRoleHelpdesk() {
        return getAuthorities().contains(AUTHORITY_HELPDESK);
    }

    public boolean hasRoleTransactionExport() {
        return getAuthorities().contains(AUTHORITY_TRANSACTIONS_EXPORT);
    }

    public boolean hasRoleAdministration() {
        return getAuthorities().contains(AUTHORITY_ADMINISTRATION);
    }

    public boolean hasRoleRecipeReceive() {
        return getAuthorities().contains(AUTHORITY_RECIPE_RECEIVE);
    }

    public boolean hasRoleRecipeParse() {
        return getAuthorities().contains(AUTHORITY_RECIPE_PARSE);
    }

    public boolean hasRoleRecipeVerify() {
        return getAuthorities().contains(AUTHORITY_RECIPE_VERIFY);
    }

    public boolean hasRoleRecipeProblem() {
        return getAuthorities().contains(AUTHORITY_RECIPE_PROBLEM);
    }

    @Override
    public String toString() {
        return "CustomUserDetails [email=" + email + ", name=" + name
                + ", newUser=" + newUser + ", getEmail=" + getEmail()
                + ", isNewUser=" + isNewUser() + ", getName=" + getName()
                + ", getAuthorities=" + getAuthorities() + ", getPassword="
                + getPassword() + ", getUsername=" + getUsername() + "]";
    }
}
