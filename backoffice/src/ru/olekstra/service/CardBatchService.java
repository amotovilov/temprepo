package ru.olekstra.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.olekstra.domain.CardBatch;
import ru.olekstra.helper.CardBatchHelper;

@Service
public class CardBatchService {

    private CardBatchHelper helper;

    @Autowired
    public CardBatchService(CardBatchHelper helper) {
        this.helper = helper;
    }

    public boolean addCardsToBatch(String batchId, String batchDescription,
            List<String> cardNumList) {
        // check that batch with specified name already created
        CardBatch cardBatch = helper.getBatch(batchId);
        if (cardBatch != null) {
            Set<String> cardNumSet = new HashSet<String>(cardBatch
                    .getBatchList());
            cardNumSet.addAll(cardNumList);
            cardBatch.setBatchList(new ArrayList<String>(cardNumSet));
            return helper.updateBatch(cardBatch);
        } else {
            return helper.updateBatch(new CardBatch(batchId, batchDescription,
                    DateTime.now(), cardNumList));
        }
    }

    public boolean createBatchIfNotExists(String batchId,
            String batchDescription) {
        CardBatch cardBatch = helper.getBatch(batchId);
        if (cardBatch == null) {
            return helper.updateBatch(new CardBatch(batchId, batchDescription,
                    DateTime.now(), null));
        }
        return false;
    }

    public List<String> getBatchCardList(String batchId) {
        CardBatch batch = helper.getBatch(batchId);
        return batch.getBatchList();
    }

    public List<String> getAllBatchNames() {
        return helper.getAllBatchNames();
    }

}
