package ru.olekstra.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.olekstra.awsutils.ComplexKey;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.backoffice.dto.ImportDto;
import ru.olekstra.backoffice.dto.PackImportDto;
import ru.olekstra.backoffice.util.CsvParser;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.domain.Pack;
import ru.olekstra.exception.InvalidFormatDataException;
import ru.olekstra.helper.PackHelper;

import com.amazonaws.AmazonServiceException;

@Service
public class PackService {

    private PackHelper helper;
    private AppSettingsService settingsService;

    public static final int GOODS_ID = 0;
    public static final int GOODS_NAME = 1;
    public static final int GOODS_BARCODE = 2;
    public static final int GOODS_MANUFACTURER = 3;
    public static final int GOODS_SYNONYM = 4;
    public static final int GOODS_TRADE_NAME = 5;
    public static final int GOODS_INN = 6;
    public static final int GOODS_EKT = 7;
    public static final int GOODS_DECL_NUMBER = 8;

    @Autowired
    public PackService(PackHelper helper, AppSettingsService settingsService) {
        this.helper = helper;
        this.settingsService = settingsService;
    }

    private void checkData(String[] values, int index)
            throws InvalidFormatDataException {
        if (values.length <= index || values[index].isEmpty()) {
            throw new InvalidFormatDataException(
                    "Invalid format data in the file");
        }
    }

    private Pack makePack(Class<Pack> pack, String id, String ekt, String name,
            String barcode, String declNumber, String manufacturer,
            String synonym, String tradeName, String inn) {
        try {
            return pack.getConstructor(
                    new Class[] {String.class, String.class, String.class,
                            String.class, String.class, String.class,
                            String.class, String.class, String.class})
                    .newInstance(id, ekt, barcode, declNumber, name,
                            manufacturer, synonym, tradeName, inn);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean checkTables(Class<Pack> packEntity) {
        try {
            return helper.checkTables(packEntity.newInstance().getTableName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public PackImportDto savePacks(Class<Pack> packEntity, ImportDto dto)
            throws InvalidFormatDataException, IOException,
            InterruptedException, AmazonServiceException,
            IllegalArgumentException, ItemSizeLimitExceededException {

        if (!checkTables(packEntity))
            return null;

        InputStream file = dto.getFile().getInputStream();
        char delim = dto.getDelim();
        Boolean skipFirstLine = dto.getSkipFirstLine();

        BufferedReader buffer = new BufferedReader(new InputStreamReader(file,
                "UTF-8"));
        String strLine = "";
        String[] data = null;
        int maxBatchSize = helper.getMaxBatchSize();
        // int maxBatchSize = 12;

        PackImportDto packProcessing = new PackImportDto(maxBatchSize);

        int fileLineCounter = 0;
        int counter = 0;
        int packsPerLine = 1;

        List<Pack> packs = new ArrayList<Pack>(maxBatchSize + packsPerLine);

        if (skipFirstLine) {
            buffer.readLine();
        }

        while ((strLine = buffer.readLine()) != null) {

            strLine = strLine.replaceAll("\\p{C}", "");
            data = CsvParser.readFields(strLine, delim);

            checkData(data, GOODS_ID);
            String id = data[GOODS_ID];
            checkData(data, GOODS_NAME);
            String name = data[GOODS_NAME];
            String barcode = data[GOODS_BARCODE];
            checkData(data, GOODS_MANUFACTURER);
            String manufacturer = data[GOODS_MANUFACTURER];
            String synonym = data[GOODS_SYNONYM];
            String tradeName = data[GOODS_TRADE_NAME];
            String inn = "";
            if (data.length > GOODS_INN) {
                inn = data[GOODS_INN];
            }
            checkData(data, GOODS_EKT);
            String ekt = data[GOODS_EKT];
            String declNumber = "";
            if (data.length > GOODS_DECL_NUMBER) {
                declNumber = data[GOODS_DECL_NUMBER];
            }

            Pack pack = makePack(packEntity, id, ekt, name, barcode,
                    declNumber, manufacturer, synonym, tradeName, inn);

            if (pack != null) {
                packs.add(pack);
                counter += packsPerLine;
            }

            if (counter == maxBatchSize) {
                List<ComplexKey> unprocessedItemList = helper.savePacks(packs);
                packs.clear();
                counter = 0;
                if (unprocessedItemList.size() > 0) { // если фэйл при записи
                    packProcessing.setSuccess(Boolean.FALSE);
                    packProcessing
                            .setFileProcessingLinePointer(fileLineCounter);
                    packProcessing
                            .setUnprocessedItemIdList(unprocessedItemList);
                    break;
                } else { // если успешно
                    fileLineCounter += maxBatchSize;
                    packProcessing
                            .setFileProcessingLinePointer(fileLineCounter);
                }
            }
        }

        if (!packs.isEmpty()) {
            List<ComplexKey> unprocessedItemList = helper.savePacks(packs);
            if (unprocessedItemList.size() > 0) {
                packProcessing.setSuccess(Boolean.FALSE);
                packProcessing.setFileProcessingLinePointer(fileLineCounter);
                packProcessing.setUnprocessedItemIdList(unprocessedItemList);
            } else {
                fileLineCounter += packs.size();
                packProcessing.setFileProcessingLinePointer(fileLineCounter);
            }
        }

        return packProcessing;
    }

    public boolean prepareSlaveTable() {
        return helper.prepareTables(settingsService.getPackSlaveTableName());
    }
}
