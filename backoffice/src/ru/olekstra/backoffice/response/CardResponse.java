package ru.olekstra.backoffice.response;

import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;

public class CardResponse {

    private String number;
    private String holder;
    private String phone;
    private boolean disabled;
    private String plan;
    private boolean active;
    private String startDate;
    private String endDate;
    private BigDecimal limitRemain;

    public CardResponse() {
        this.number = "";
        this.setPhone("");
        this.holder = "";
        this.disabled = Boolean.FALSE;
        this.setPlan("");
        this.active = Boolean.FALSE;
        this.startDate = "";
        this.endDate = "";
    }

    public CardResponse(String number, String holder, String phone, boolean disabled,
            String plan, boolean active, String startDate, String endDate,
            BigDecimal limitRemain) {
        super();
        this.number = number;
        this.holder = holder;
        this.phone = phone;
        this.disabled = disabled;
        this.plan = plan;
        this.active = active;
        this.startDate = startDate;
        this.endDate = endDate;
        this.limitRemain = limitRemain;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getHolder() {
        return holder;
    }

    public void setHolder(String holder) {
        this.holder = holder;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        if (StringUtils.isNotBlank(phone)) {
            this.phone = "(" + phone.substring(0, 3) + ")" + " "
                    + phone.substring(3, 6) + "-" + phone.substring(6, 8)
                    + "-" + phone.substring(8);
        }
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getLimitRemain() {
        return limitRemain;
    }

    public void setLimitRemain(BigDecimal limitRemain) {
        this.limitRemain = limitRemain;
    }

}
