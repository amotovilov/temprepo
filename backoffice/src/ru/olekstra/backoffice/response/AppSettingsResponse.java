package ru.olekstra.backoffice.response;

import java.util.ArrayList;
import java.util.List;

public class AppSettingsResponse {

    private String name;
    private String currentValue;
    private List<String> values = new ArrayList<String>();

    public AppSettingsResponse() {
    }

    public AppSettingsResponse(String name, String currentValue,
            String... values) {

        this.name = name;
        this.currentValue = currentValue;
        setValues(values);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(String currentValue) {
        this.currentValue = currentValue;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(String[] values) {
        this.values.clear();
        for (int i = 0; i < values.length; i++) {
            this.values.add(values[i]);
        }
    }
}
