package ru.olekstra.backoffice.dto;

public enum BatchOperationType {

    ADD(), // добавить карты к существующей серии
    NEW(), // добавить карты к новой серии
    DELETE(); // удалить карты из серии

}
