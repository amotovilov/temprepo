package ru.olekstra.backoffice.dto;

import java.util.List;

import ru.olekstra.awsutils.ComplexKey;

public class PackImportDto {

    private Integer fileProcessingLinePointer;
    private Integer batchSize;
    private List<ComplexKey> unprocessedItemIdList;
    private Boolean success;

    public PackImportDto(int batchSize) {
        this.success = Boolean.TRUE;
        this.fileProcessingLinePointer = 0;
        this.batchSize = batchSize;
    }

    public Integer getFileProcessingLinePointer() {
        return fileProcessingLinePointer;
    }

    public void setFileProcessingLinePointer(Integer fileProcessingLinePointer) {
        this.fileProcessingLinePointer = fileProcessingLinePointer;
    }

    public Integer getBatchSize() {
        return batchSize;
    }

    public void setBatchSize(Integer batchSize) {
        this.batchSize = batchSize;
    }

    public List<ComplexKey> getUnprocessedItemIdList() {
        return unprocessedItemIdList;
    }

    public void setUnprocessedItemIdList(List<ComplexKey> unprocessedItemIdList) {
        this.unprocessedItemIdList = unprocessedItemIdList;
    }

    public Boolean isSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

}
