package ru.olekstra.backoffice.dto;

import org.springframework.web.multipart.MultipartFile;

import ru.olekstra.backoffice.util.CsvParser;

public class ImportDto {

    private MultipartFile file;
    private char delim = CsvParser.SEMICOLON;
    private short action = 0;
    private boolean skipLine;

    public ImportDto() {
    }

    public ImportDto(MultipartFile file, short action, boolean skipLine) {
        this.file = file;
        this.action = action;
        this.skipLine = skipLine;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public char getDelim() {
        return delim;
    }

    public void setDelim(char delim) {
        this.delim = delim;
    }

    public short getAction() {
        return action;
    }

    public void setAction(short action) {
        this.action = action;
    }

    public boolean getSkipFirstLine() {
        return skipLine;
    }

    public void setSkipFirstLine(boolean skipLine) {
        this.skipLine = skipLine;
    }

}
