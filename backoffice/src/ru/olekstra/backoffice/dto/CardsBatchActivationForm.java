package ru.olekstra.backoffice.dto;

public class CardsBatchActivationForm {

    private String plan;
    private String reason;
    private String textInput;

    public CardsBatchActivationForm() {
    }

    public CardsBatchActivationForm(String textInput, String plan, String reason) {
        this.textInput = textInput;
        this.plan = plan;
        this.reason = reason;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getTextInput() {
        return textInput;
    }

    public void setTextInput(String textInput) {
        this.textInput = textInput;
    }

}
