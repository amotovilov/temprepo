package ru.olekstra.backoffice.dto;

import java.util.ArrayList;
import java.util.List;

import ru.olekstra.domain.Card;

public class CardsLoadResult {

    private List<Card> loadedCards;
    private List<String> notFoundCards;
    private List<String> duplicateCards;

    public CardsLoadResult(List<Card> loadedCards, List<String> notFoundCards,
            List<String> duplicateCards) {

        if (loadedCards != null) {
            this.loadedCards = loadedCards;
        } else {
            this.loadedCards = new ArrayList<Card>();
        }
        if (notFoundCards != null) {
            this.notFoundCards = notFoundCards;
        } else {
            this.notFoundCards = new ArrayList<String>();
        }
        if (duplicateCards != null) {
            this.duplicateCards = duplicateCards;
        } else {
            this.duplicateCards = new ArrayList<String>();
        }
    }

    public List<Card> getLoadedCards() {
        return loadedCards;
    }

    public void setLoadedCards(List<Card> loadedCards) {
        this.loadedCards = loadedCards;
    }

    public List<String> getNotFoundCards() {
        return notFoundCards;
    }

    public void setNotFoundCards(List<String> notFoundCards) {
        this.notFoundCards = notFoundCards;
    }

    public List<String> getDuplicateCards() {
        return duplicateCards;
    }

    public void setDuplicateCards(List<String> duplicateCards) {
        this.duplicateCards = duplicateCards;
    }

}
