package ru.olekstra.backoffice.dto;

public class DiscountPlanForm {

    private String id;
    private String name;
    private Integer maxDiscount;
    private String description;
    private String detailsLink;
    private String discount;
    private boolean addNew;

    public DiscountPlanForm() {
    }

    public DiscountPlanForm(String id, String name, Integer maxDiscount,
            String description, String detailsLink,
            String discount, boolean addNew) {
        super();
        this.id = id;
        this.name = name;
        this.maxDiscount = maxDiscount;
        this.description = description;
        this.detailsLink = detailsLink;
        this.discount = discount;
        this.addNew = addNew;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAddNew() {
        return addNew;
    }

    public void setAddNew(boolean addNew) {
        this.addNew = addNew;
    }

    public Integer getMaxDiscount() {
        return maxDiscount;
    }

    public void setMaxDiscount(Integer maxDiscount) {
        this.maxDiscount = maxDiscount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDetailsLink() {
        return detailsLink;
    }

    public void setDetailsLink(String detailsLink) {
        this.detailsLink = detailsLink;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

}
