package ru.olekstra.backoffice.util;

import java.util.HashMap;
import java.util.Map;

public class AjaxUtil {

    public static final String JSON_SUCCESS = "success";
    public static final String JSON_MESSAGE = "message";
    public static final String JSON_DATA = "data";

    /**
     * Получить успешный AJAX ответ с данными
     * 
     * @param data данные для пользователя
     * @return json data
     */
    public static Map<String, Object> getSuccessResponse(Object data) {
        Map<String, Object> modelMap = new HashMap<String, Object>(2);
        modelMap.put(JSON_SUCCESS, true);
        modelMap.put(JSON_DATA, data);
        return modelMap;
    }

    /**
     * Получить успешный AJAX ответ с сообщением
     * 
     * @param message сообщение для пользователя
     * @return json message
     */
    public static Map<String, Object> getSuccessResponseWithMessage(
            String message) {
        Map<String, Object> modelMap = new HashMap<String, Object>(2);
        modelMap.put(JSON_SUCCESS, true);
        modelMap.put(JSON_MESSAGE, message);
        return modelMap;
    }

    /**
     * Получить успешный AJAX ответ с данными и сообщением
     * 
     * @param message сообщение для пользователя
     * @param data данные для пользователя
     * @return json message, json data
     */
    public static Map<String, Object> getSuccessResponseWithMessage(
            String message, Object data) {
        Map<String, Object> modelMap = new HashMap<String, Object>(3);
        modelMap.put(JSON_SUCCESS, true);
        modelMap.put(JSON_MESSAGE, message);
        modelMap.put(JSON_DATA, data);
        return modelMap;
    }

    /**
     * Получить провальный AJAX ответ с сообщением об ошибке
     * 
     * @param message сообщение для пользователя
     * @return карта
     */
    public static Map<String, Object> getFailureResponse(String message) {
        Map<String, Object> modelMap = new HashMap<String, Object>(2);
        modelMap.put(JSON_SUCCESS, false);
        modelMap.put(JSON_MESSAGE, message);
        return modelMap;
    }

    /**
     * Получить провальный AJAX ответ с данными
     * 
     * @param data данные для пользователя
     * @return карта
     */
    public static Map<String, Object> getFailureResponse(Object data) {
        Map<String, Object> modelMap = new HashMap<String, Object>(2);
        modelMap.put(JSON_SUCCESS, false);
        modelMap.put(JSON_DATA, data);
        return modelMap;
    }
}
