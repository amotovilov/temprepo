package ru.olekstra.backoffice.util;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import ru.olekstra.exception.MessageCodeConstants;

public class FailureAuthenticationHandler extends
        SimpleUrlAuthenticationFailureHandler {

    public static final String MESSAGE_CODE_NAME = "messageCode";

    @Override
    public void onAuthenticationFailure(HttpServletRequest request,
            HttpServletResponse response, AuthenticationException exception)
            throws IOException, ServletException {

        String message = exception.getMessage();
        boolean identified = false;

        if (MessageCodeConstants.EMAIL_NOT_FOUND_CODE.equals(message) ||
                MessageCodeConstants.USER_NOT_FOUND_CODE.equals(message) ||
                MessageCodeConstants.USER_ROLE_NOT_FOUND_CODE.equals(message)) {

            identified = true;
        }
        String url = "/login?error=" + identified + "&message="
                + URLEncoder.encode(message, "UTF-8");

        this.setDefaultFailureUrl(url);

        super.onAuthenticationFailure(request, response, exception);
    }
}
