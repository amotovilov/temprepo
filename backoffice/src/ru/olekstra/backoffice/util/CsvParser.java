package ru.olekstra.backoffice.util;

import java.util.ArrayList;
import java.util.List;

public class CsvParser {

    public static final char QUOUTE = '"';
    public static final char COMMA = ',';
    public static final char SEMICOLON = ';';
    public static final char TAB = '\t';

    private CsvParser() {
        // Utility class. All methods assumed to used in a static way
    }

    /**
     * Get csv formated string and parse it as specified fields
     * 
     * @param line value to be parsed
     * @param separator delimiter used
     * @return array of parsed fields
     */
    public static String[] readFields(String line, char separator) {

        StringBuilder field = new StringBuilder();
        List<String> parsedValues = new ArrayList<String>();

        boolean insideQuotes = false;
        boolean nextQuote = false;

        char[] values = line.toCharArray();
        int counter = 0;

        for (char value : values) {
            switch (value) {
            case QUOUTE:
                if (insideQuotes == false) {
                    insideQuotes = true;
                } else {
                    if (nextQuote == false) {
                        if (values.length > counter + 1) {
                            if (values[counter + 1] == separator) {
                                insideQuotes = false;
                            } else {
                                nextQuote = true;
                            }
                        } else {
                            insideQuotes = false;
                        }
                    } else {
                        nextQuote = false;
                        field.append(QUOUTE);
                    }
                }
                break;
            case SEMICOLON:
                if (separator != SEMICOLON) {
                    field.append(value);
                } else {
                    if (insideQuotes) {
                        field.append(value);
                    } else {
                        parsedValues.add(field.toString());
                        field.delete(0, field.length());
                    }
                }
                break;
            case TAB:
                if (separator != TAB) {
                    field.append(value);
                } else {
                    if (insideQuotes) {
                        field.append(value);
                    } else {
                        parsedValues.add(field.toString());
                        field.delete(0, field.length());
                    }
                }
                break;
            case COMMA:
                if (separator != COMMA) {
                    field.append(value);
                } else {
                    if (insideQuotes) {
                        field.append(value);
                    } else {
                        parsedValues.add(field.toString());
                        field.delete(0, field.length());
                    }
                }
                break;
            default:
                field.append(value);
                break;
            }
            counter++;
        }

        if (field.length() != 0) {
            parsedValues.add(field.toString());
            field.delete(0, field.length());
        } else {
            parsedValues.add("");
        }

        return (String[]) parsedValues.toArray(new String[parsedValues.size()]);
    }
}
