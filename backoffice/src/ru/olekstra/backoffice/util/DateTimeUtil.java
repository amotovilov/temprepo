package ru.olekstra.backoffice.util;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateTimeUtil {

    private static final String DEFAULT_TIMEZONE = "Europe/Moscow";

    public static final String FILE_INPUT_PARSE_DATE_PATTERN = "dd.MM.yyyy";
    public static final String FORM_INPUT_PARSE_DATE_PATTERN = "dd.MM.yyyy HH:mm:ss";
    public static final String DB_STORE_DATE_PATTERN = "dd.MM.yyyy HH:mm:ss ZZZZ";
    public static final String VIEW_DATE_PATTERN = "dd MMM yyyy";

    public static final String START_DAY_TIME = "00:00:01";
    public static final String END_DAY_TIME = "23:59:59";

    public DateTime parse(String date, String pattern)
            throws IllegalArgumentException {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern)
                .withZone(DateTimeZone.forID(DEFAULT_TIMEZONE));
        return formatter.parseDateTime(date);
    }

    public String toStringValue(DateTime date, String pattern) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
        return formatter.print(date);
    }

}
