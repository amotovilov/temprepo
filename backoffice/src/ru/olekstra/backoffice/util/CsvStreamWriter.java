package ru.olekstra.backoffice.util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;

public class CsvStreamWriter {

    private OutputStream outStream;
    private String delimiter = ";";
    private String newLine = "\r\n";
    private String encoding = "Cp1251";
    private DateTimeFormatter reportDateTimeFormatter;

    public CsvStreamWriter(OutputStream outStream,
            DateTimeFormatter reportDateTimeFormatter) {
        this.outStream = outStream;
        this.reportDateTimeFormatter = reportDateTimeFormatter;
    }

    /**
     * Appends new line to output stream, taking value from array, but ALL
     * elements from array are treated as String, so be quoted by '"'
     * 
     * @param array value to write
     * @throws IOException
     */
    public void appendLine(Object[] array) throws IOException {

        for (int i = 0; i < array.length; i++) {
            outStream.write("\"".getBytes(encoding));
            if (array[i] != null) {
                String value = array[i].toString().replace("\"", "\"\"");
                outStream.write(value.getBytes(encoding));
            } else {
                outStream.write(" ".getBytes(encoding));
            }
            outStream.write("\"".getBytes(encoding));

            if (i != array.length - 1) {
                outStream.write(delimiter.getBytes(encoding));
            }
        }
        outStream.write(newLine.getBytes(encoding));
    }

    public void appendField(String value) throws UnsupportedEncodingException,
            IOException {
        outStream.write("\"".getBytes(encoding));
        if (value != null) {
            String str = value.toString().replace("\"", "\"\"");
            outStream.write(str.getBytes(encoding));
        } else {
            outStream.write(" ".getBytes(encoding));
        }
        outStream.write("\"".getBytes(encoding));
        outStream.write(delimiter.getBytes(encoding));
    }

    public void appendField(BigDecimal value)
            throws UnsupportedEncodingException, IOException {
        if (value != null) {
            outStream.write(value.toString().getBytes(encoding));
        } else {
            outStream.write(" ".getBytes(encoding));
        }
        outStream.write(delimiter.getBytes(encoding));
    }

    public void appendField(Integer value) throws UnsupportedEncodingException,
            IOException {
        if (value != null) {
            outStream.write(value.toString().getBytes(encoding));
        } else {
            outStream.write(" ".getBytes(encoding));
        }
        outStream.write(delimiter.getBytes(encoding));
    }

    public void appendField(DateTime value)
            throws UnsupportedEncodingException, IOException {
        outStream.write("\"".getBytes(encoding));
        if (value != null) {
            String str = reportDateTimeFormatter.print(value).replace("\"",
                    "\"\"");
            outStream.write(str.getBytes(encoding));
        } else {
            outStream.write(" ".getBytes(encoding));
        }
        outStream.write("\"".getBytes(encoding));
        outStream.write(delimiter.getBytes(encoding));
    }

    public void appenNewLine() throws UnsupportedEncodingException, IOException {
        outStream.write(newLine.getBytes(encoding));
    }
}
