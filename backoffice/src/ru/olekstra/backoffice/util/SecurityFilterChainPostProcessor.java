package ru.olekstra.backoffice.util;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.savedrequest.RequestCacheAwareFilter;

public class SecurityFilterChainPostProcessor implements BeanPostProcessor {

    Logger LOGGER = Logger.getLogger(getClass());

    public Object postProcessAfterInitialization(Object bean, String beanName)
            throws BeansException {

        if (beanName.equals("customFilterChain")) {
            DefaultSecurityFilterChain fc = (DefaultSecurityFilterChain) bean;
            List<Filter> filters = fc.getFilters();

            List<Filter> newFilters = new ArrayList<Filter>();
            for (int i = 0; i < filters.size(); i++) {

                LOGGER.debug(filters.get(i).getClass());
                if (filters.get(i).getClass() != RequestCacheAwareFilter.class) {
                    LOGGER.debug("Prepare custom filter chain.");
                    LOGGER.debug("Loaded filters in chains:");
                    newFilters.add(filters.get(i));
                } else {
                    filters.get(i).destroy();
                }
            }

            return new DefaultSecurityFilterChain(fc.getRequestMatcher(),
                    newFilters);
        }

        return bean;
    }

    public Object postProcessBeforeInitialization(Object bean, String beanName)
            throws BeansException {
        return bean;
    }
}
