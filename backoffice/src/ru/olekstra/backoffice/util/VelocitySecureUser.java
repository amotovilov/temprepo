package ru.olekstra.backoffice.util;

import org.springframework.security.core.context.SecurityContextHolder;

import ru.olekstra.service.CustomUserDetails;

public class VelocitySecureUser {

    public boolean hasRoleCardView() {
        return ((CustomUserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal()).hasRoleCardView();
    }

    public boolean hasRoleCardGeneral() {
        return ((CustomUserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal()).hasRoleCardGeneral();
    }

    public boolean hasRoleCardBalance() {
        return ((CustomUserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal()).hasRoleCardBalance();
    }

    public boolean hasRoleDrugstoreView() {
        return ((CustomUserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal()).hasRoleDrugstoreView();
    }

    public boolean hasRoleDrugstoreEdit() {
        return ((CustomUserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal()).hasRoleDrugstoreEdit();
    }

    public boolean hasRoleDrugstoreReports() {
        return ((CustomUserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal()).hasRoleDrugstoreReports();
    }

    public boolean hasRoleManufacturerReports() {
        return ((CustomUserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal()).hasRoleManufacturerReports();
    }

    public boolean hasRoleHelpdesk() {
        return ((CustomUserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal()).hasRoleHelpdesk();
    }

    public boolean hasRoleTransactionExport() {
        return ((CustomUserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal()).hasRoleTransactionExport();
    }

    public boolean hasRoleAdministration() {
        return ((CustomUserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal()).hasRoleAdministration();
    }

    public boolean hasRoleRecipeReceive() {
        return ((CustomUserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal()).hasRoleRecipeReceive();
    }

    public boolean hasRoleRecipeParse() {
        return ((CustomUserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal()).hasRoleRecipeParse();
    }

    public boolean hasRoleRecipeVerify() {
        return ((CustomUserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal()).hasRoleRecipeVerify();
    }

    public boolean hasRoleRecipeProblem() {
        return ((CustomUserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal()).hasRoleRecipeProblem();
    }
}
