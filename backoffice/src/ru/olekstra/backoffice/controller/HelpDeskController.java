package ru.olekstra.backoffice.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.security.RolesAllowed;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.w3c.dom.DOMException;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.awsutils.exception.OlekstraException;
import ru.olekstra.azure.model.MessageSendException;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.common.service.DateTimeService;
import ru.olekstra.common.util.MessagesUtil;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.Drugstore;
import ru.olekstra.domain.EntityProxy;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.Ticket;
import ru.olekstra.domain.TicketInfo;
import ru.olekstra.domain.TicketPost;
import ru.olekstra.domain.User;
import ru.olekstra.exception.HttpBadRequestException;
import ru.olekstra.exception.NotFoundException;
import ru.olekstra.service.CardService;
import ru.olekstra.service.DrugstoreService;
import ru.olekstra.service.TicketService;

@Controller
public class HelpDeskController {

    public static final String HELPDESK_URL = "/helpdesk";
    public static final String NEW_TICKETS_URL = HELPDESK_URL + "/tickets/new";
    public static final String EXECUTIVE = "executive";
    public static final String TICKET_STATUSES = "ticketStatuses";

    public static final String TICKETS_NEW_URL = HELPDESK_URL + "/new";
    public static final String TICKETS_ALL_URL = HELPDESK_URL + "/all";
    public static final String TICKETS_PROCESSING_PERSONAL_URL = HELPDESK_URL
            + "/processing/personal";
    public static final String TICKETS_PROCESSING_ALL_URL = HELPDESK_URL
            + "/processing/all";
    public static final String TICKET_EDIT_URL = HELPDESK_URL + "/ticket";

    public static final String TICKETS_PAGE = "tickets";
    public static final String TICKET_VIEWS = "ticketViews";
    public static final String SHOW_TYPE = "show";
    public static final String SHOW_NEW = "new";
    public static final String SHOW_PROCESSING_PERSONAL = "processingPersonal";
    public static final String SHOW_PROCESSING_ALL = "processingAll";

    public static final String VIEW_TICKET_PAGE = "ticketView";
    public static final String VIEW_TICKET = "ticket";
    public static final String VIEW_TICKET_POSTS = "posts";
    public static final String VIEW_PATH_TICKET_ID = "ticketId";
    public static final String VIEW_PARAM_TYPE = "type";
    public static final String VIEW_TYPE = "view";
    public static final String VIEW_SHOW_NEW = "new";
    public static final String VIEW_SHOW_PERSONAL = "processingPersonal";
    public static final String VIEW_SHOW_ALL = "processingAll";
    private static final String TRANSACTION_ID = "transaction";
    private static final String TRANSACTION_RANGE_KEY = "transactionRangeKey";
    private static final String TRANSACTION_DRUGSTORE = "drugstore";
    private static final String TRANSACTION_DATE = "date";
    private static final String TRANSACTION_ROW = "row";

    public static final String TICKET_POST_URL = TICKET_EDIT_URL + "/post";
    public static final String TICKET_ADD_URL = TICKET_EDIT_URL + "/add";
    public static final String TICKET_SUBMIT_URL = TICKET_EDIT_URL + "/submit";
    public static final String TICKET_CREATE_URL = TICKET_EDIT_URL + "/create";
    public static final String TICKET_MISC_SUBMIT_URL = TICKET_EDIT_URL
            + "/submitmiscticket";
    public static final String TICKET_ASSIGN_URL = TICKET_EDIT_URL + "/assign";
    public static final String TICKET_REASSIGN_URL = TICKET_EDIT_URL
            + "/reassign";

    public static final String TICKET_REVERSAL = "reversal";
    public static final String TICKET_MISC = "ticketMisc";
    public static final String HELPDESK_MESSAGE = "message";
    public static final String HELPDESK_MESSAGE_SUCCESS = "messageSuccess";
    public static final String HELPDESK_MESSAGE_FAILURE = "messageFailure";

    private DateTimeFormatter periodFormatter = DateTimeFormat
            .forPattern("yyyyMM");

    private TicketService ticketService;
    private AppSettingsService appSettingsService;
    private CardService cardService;
    private DrugstoreService drugstoreService;
    private MessageSource messageSource;
    private DateTimeService dateService;
    private DynamodbService dynamodbService;

    public HelpDeskController() {
    }

    @Autowired
    public HelpDeskController(
            TicketService service,
            AppSettingsService appSettingsService,
            CardService cardService,
            DrugstoreService drugstoreService,
            MessageSource messageSource,
            DateTimeService dateService,
            DynamodbService dynamodbService) {

        this.ticketService = service;
        this.appSettingsService = appSettingsService;
        this.cardService = cardService;
        this.drugstoreService = drugstoreService;
        this.messageSource = messageSource;
        this.dateService = dateService;
        this.dynamodbService = dynamodbService;
    }

    @RolesAllowed(User.ROLE_HELPDESK)
    @RequestMapping(value = "/helpdesk", method = RequestMethod.GET)
    public ModelAndView helpdesk(ModelMap model) {
        return new ModelAndView("helpdesk");
    }

    @RolesAllowed(User.ROLE_HELPDESK)
    @RequestMapping(value = TICKETS_NEW_URL, method = RequestMethod.GET)
    public ModelAndView helpdeskTicketsNew(ModelMap model) throws IOException {

        List<TicketInfo> tickets = ticketService.getNewTickets();
        if (tickets != null) {
            Collections.sort(tickets);
        }
        model.put(TICKET_VIEWS, tickets);
        model.put(SHOW_TYPE, SHOW_NEW);
        model.put("periodFormatter", periodFormatter);

        return new ModelAndView(TICKETS_PAGE, model);
    }

    @RolesAllowed(User.ROLE_HELPDESK)
    @RequestMapping(value = TICKETS_PROCESSING_PERSONAL_URL, method = RequestMethod.GET)
    public ModelAndView helpdeskTicketsProcessingPersonal(
            ModelMap model, Principal principal) {

        String executive = principal.getName();
        List<TicketInfo> tickets = ticketService.getAssignedTickets(executive);
        if (tickets != null) {
            Collections.sort(tickets);
        }

        model.put(TICKET_VIEWS, tickets);
        model.put(SHOW_TYPE, SHOW_PROCESSING_PERSONAL);
        model.put("periodFormatter", periodFormatter);

        return new ModelAndView(TICKETS_PAGE, model);
    }

    @RolesAllowed(User.ROLE_HELPDESK)
    @RequestMapping(value = TICKETS_PROCESSING_ALL_URL, method = RequestMethod.GET)
    public ModelAndView helpdeskTicketsProcessingAll(ModelMap model) {

        List<TicketInfo> tickets = ticketService.getAssignedTickets(null);
        if (tickets != null) {
            Collections.sort(tickets);
        }
        model.put(TICKET_VIEWS, tickets);
        model.put(SHOW_TYPE, SHOW_PROCESSING_ALL);
        model.put("periodFormatter", periodFormatter);

        return new ModelAndView(TICKETS_PAGE, model);
    }

    @RolesAllowed({User.ROLE_CARD_VIEW, User.ROLE_CARD_GENERAL})
    @RequestMapping(value = TICKET_ADD_URL, method = RequestMethod.GET)
    public ModelAndView helpdeskAddTicket(
            @RequestParam(TRANSACTION_RANGE_KEY) String rangeKey,
            @RequestParam(TRANSACTION_DRUGSTORE) String drugstoreId,
            @RequestParam(TRANSACTION_ROW) String row,
            @RequestParam(value = "isEmptyDesc", required = false) Boolean isEmptyDesc) {
        String voucherRowId;
        try {
            voucherRowId = rangeKey.substring(rangeKey.indexOf(EntityProxy.getDelimeter()) + 1)
                    + EntityProxy.getDelimeter() + row;
        } catch (StringIndexOutOfBoundsException ex) {
            voucherRowId = "";
        }

        ProcessingTransaction transaction = dynamodbService.getObject(ProcessingTransaction.class,
                drugstoreId, rangeKey);
        String cardNumber = "";
        if (transaction != null) {
            try {
                cardNumber = transaction.getCard().getNumber();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        ModelAndView mav = new ModelAndView("ticketAdd")
                .addObject("cardNumber", cardNumber)
                .addObject("transactionRangeKey", rangeKey)
                .addObject("voucherRowId", voucherRowId)
                .addObject("drugstoreId", drugstoreId)
                .addObject("ticketSubject", appSettingsService.getTicketTheme())
                .addObject("isEmptyDesc", isEmptyDesc);
        return mav;
    }

    @RolesAllowed({User.ROLE_CARD_VIEW, User.ROLE_CARD_GENERAL})
    @RequestMapping(value = TICKET_SUBMIT_URL, method = RequestMethod.POST)
    public ModelAndView helpdeskSubmitTicket(
            @RequestParam("voucherRowId") String voucherRowId,
            @RequestParam(TRANSACTION_RANGE_KEY) String rangeKey,
            @RequestParam("drugstoreId") String drugstoreId,
            @RequestParam("subject") String subject,
            @RequestParam("ticketDesc") String ticketDesc,
            @RequestParam(value = "assignTicket", required = false) Boolean assign,
            Principal principal)
            throws ItemSizeLimitExceededException, JsonParseException,
            JsonMappingException, IOException, HttpBadRequestException {

        if (assign == null)
            assign = false;

        ModelAndView mav = null;
        String errorMsg = "";

        ProcessingTransaction transaction = dynamodbService.getObject(ProcessingTransaction.class,
                drugstoreId, rangeKey);

        // Валидация формы
        // 1. Существующая транзакция
        if (transaction == null) {
            throw new HttpBadRequestException(MessagesUtil.getMessage(messageSource, "error"));
        }
        String cardNumber = transaction.getCard().getNumber();
        DateTime dateTime = transaction.getDate();

        // 2. Существующая карта
        Card card;
        try {
            card = cardService.getCard(cardNumber);
        } catch (NotFoundException nfe) {
            throw new HttpBadRequestException(MessagesUtil.getMessage(
                    messageSource, "error"));
        }

        // 3. Cуществующая аптека
        Drugstore drugstore = drugstoreService.findDrugstoreById(drugstoreId);
        if (drugstore == null) {
            throw new HttpBadRequestException(MessagesUtil.getMessage(
                    messageSource, "error"));
        }

        // 4. Была ли такая транзакция
        // TODO добавить проверку карты в транзакции, а то может в реквесте
        // прийти существующая карта, но окажется, что транзакция не по этой
        // карте.
        if (!ticketService.isTransactionFound(dateTime, voucherRowId,
                drugstoreId)) {
            throw new HttpBadRequestException(MessagesUtil.getMessage(
                    messageSource, "error"));
        }

        // 5. Не пустое сообщение
        if (ticketDesc.trim().isEmpty()) {
            Map<String, Object> modelMap = new HashMap<String, Object>();
            modelMap.put("card", cardNumber);
            String transactionId = voucherRowId.split(EntityProxy
                    .getDelimeter())[0];
            modelMap.put(TRANSACTION_ID, transactionId);
            String row = voucherRowId.split(EntityProxy.getDelimeter())[1];
            modelMap.put(TRANSACTION_ROW, row);
            modelMap.put(TRANSACTION_RANGE_KEY, rangeKey);
            modelMap.put(TRANSACTION_DATE, dateTime);
            modelMap.put(TRANSACTION_DRUGSTORE, drugstoreId);
            modelMap.put("isEmptyDesc", true);
            return new ModelAndView("redirect:" + TICKET_ADD_URL, modelMap);
        }

        // создаем новый тикет
        Ticket ticket = new Ticket(dateTime, cardNumber, card.getHolderName(),
                card.getTelephoneNumber(),
                drugstoreId, drugstore.getName(),
                ticketDesc, subject,
                appSettingsService.getTicketStatusNew().get(0), false,
                voucherRowId);

        TicketPost ticketPost = new TicketPost(ticket.getTicketId(),
                ticket.getName(), ticket.getSubject(),
                ticket.getLastPost(),
                ticket.getLastStatus(), ticket.getVoucherRowId(),
                dateService.createDefaultDateTime());

        if (assign) {
            ticket.setExecutive(principal.getName());
            ticketService.saveAndAssignTicket(ticket, ticketPost);
        } else
            ticketService.saveNewTicket(ticket, ticketPost);

        Map<String, Object> model = new HashMap<String, Object>();
        model.put(VIEW_PATH_TICKET_ID, ticket.getTicketId().toString());
        model.put(VIEW_PARAM_TYPE, assign ? VIEW_SHOW_PERSONAL : VIEW_SHOW_NEW);
        model.put("period", ticket.getPeriod());
        model.put(HELPDESK_MESSAGE, MessagesUtil.getMessage(messageSource,
                "ticketPostSent"));
        return new ModelAndView("redirect:" + TICKET_EDIT_URL, model);

    }

    @RolesAllowed({User.ROLE_CARD_VIEW, User.ROLE_CARD_GENERAL})
    @RequestMapping(value = TICKET_CREATE_URL, method = RequestMethod.GET)
    public ModelAndView helpdeskCreateMiscTicket(
            @RequestParam(value = "cardNumber", required = false) String cardNumber,
            @RequestParam(value = TRANSACTION_DRUGSTORE, required = false) String drugstoreId,
            @RequestParam(value = "isEmptyDesc", required = false) Boolean isEmptyDesc,
            @RequestParam(value = "isDrugstoreBad", required = false) Boolean isDrugstoreBad) {

        ModelAndView mav = new ModelAndView("ticketCreate")
                .addObject("cardNumber", cardNumber)
                .addObject("drugstoreId", drugstoreId)
                .addObject("ticketSubject", appSettingsService.getTicketTheme())
                .addObject("defaultSubject",
                        MessagesUtil.getMessage(messageSource,
                                "ticketDefaultSubject"))
                .addObject("isEmptyDesc", isEmptyDesc)
                .addObject("isDrugstoreBad", isDrugstoreBad);
        return mav;
    }

    @RolesAllowed({User.ROLE_CARD_VIEW, User.ROLE_CARD_GENERAL})
    @RequestMapping(value = TICKET_MISC_SUBMIT_URL, method = RequestMethod.POST)
    public ModelAndView helpdeskSubmitMiscTicket(
            @RequestParam("cardNumber") String cardNumber,
            @RequestParam("drugstoreId") String drugstoreId,
            @RequestParam("subject") String subject,
            @RequestParam("ticketDesc") String ticketDesc,
            @RequestParam(value = "assignTicket", required = false) Boolean assign,
            Principal principal) throws HttpBadRequestException,
            ItemSizeLimitExceededException, IOException {

        if (assign == null)
            assign = false;

        // Валидация формы
        // 1. Существующая карта
        Card card;
        try {
            card = cardService.getCard(cardNumber);
        } catch (NotFoundException nfe) {
            throw new HttpBadRequestException(MessagesUtil.getMessage(
                    messageSource, "error"));
        }

        // 2. Cуществующая аптека
        Drugstore drugstore = null;
        if (StringUtils.isNotEmpty(drugstoreId)) {
            drugstore = drugstoreService.findDrugstoreById(drugstoreId);
        }
        if (drugstore == null) {
            Map<String, Object> modelMap = new HashMap<String, Object>();
            modelMap.put("cardNumber", cardNumber);
            modelMap.put(TRANSACTION_DRUGSTORE, drugstoreId);
            modelMap.put("ticketSubject", appSettingsService.getTicketTheme());
            modelMap.put("drugstoreId", drugstoreId);
            modelMap.put("ticketDesc", ticketDesc);
            modelMap.put("defaultSubject",
                    MessagesUtil.getMessage(messageSource,
                            "ticketDefaultSubject"));
            modelMap.put("isDrugstoreBad", true);
            return new ModelAndView("redirect:" + TICKET_CREATE_URL, modelMap);
        }

        // 3. Не пустое сообщение
        if (ticketDesc.trim().isEmpty()) {
            Map<String, Object> modelMap = new HashMap<String, Object>();
            modelMap.put("cardNumber", cardNumber);
            modelMap.put(TRANSACTION_DRUGSTORE, drugstoreId);
            modelMap.put("ticketSubject", appSettingsService.getTicketTheme());
            modelMap.put("drugstoreId", drugstoreId);
            modelMap.put("ticketDesc", ticketDesc);
            modelMap.put("defaultSubject",
                    MessagesUtil.getMessage(messageSource,
                            "ticketDefaultSubject"));
            modelMap.put("isEmptyDesc", true);
            return new ModelAndView("redirect:" + TICKET_CREATE_URL, modelMap);
        }

        // создаем новый тикет
        Ticket ticket = new Ticket(
                dateService.createDefaultDateTime(),
                cardNumber,
                card.getHolderName(),
                card.getTelephoneNumber(),
                drugstoreId,
                drugstore.getName(),
                ticketDesc,
                subject,
                appSettingsService.getTicketStatusNew().get(0),
                false,
                null // тикет без привязки к чеку
        );

        TicketPost ticketPost = new TicketPost(
                ticket.getTicketId(),
                ticket.getName(),
                ticket.getSubject(),
                ticket.getLastPost(),
                ticket.getLastStatus(),
                null, // тикет без привязки к чеку
                dateService.createDefaultDateTime()
                );

        String viewType;
        if (assign) {
            ticket.setExecutive(principal.getName());
            viewType = VIEW_SHOW_PERSONAL;
            ticketService.saveAndAssignTicket(ticket, ticketPost);
        } else {
            viewType = VIEW_SHOW_NEW;
            ticketService.saveNewTicket(ticket, ticketPost);
        }

        Map<String, Object> model = new HashMap<String, Object>();
        model.put(VIEW_PATH_TICKET_ID, ticket.getTicketId().toString());
        model.put(VIEW_PARAM_TYPE, viewType);
        model.put("period", ticket.getPeriod());
        model.put(HELPDESK_MESSAGE, MessagesUtil.getMessage(messageSource,
                "ticketPostSent"));
        return new ModelAndView("redirect:" + TICKET_EDIT_URL, model);

    }

    @ExceptionHandler(HttpBadRequestException.class)
    public ResponseEntity<String> handleException(
            HttpBadRequestException ex) {
        return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
    }

    @RolesAllowed({User.ROLE_CARD_VIEW, User.ROLE_CARD_GENERAL})
    @RequestMapping(value = TICKET_EDIT_URL, method = RequestMethod.GET)
    public ModelAndView helpdeskShowTicket(
            @RequestParam(VIEW_PATH_TICKET_ID) String ticketId,
            @RequestParam("period") String period,
            @RequestParam(VIEW_PARAM_TYPE) String type,
            @RequestParam(HELPDESK_MESSAGE) String message,
            Principal principal,
            ModelMap model) throws HttpBadRequestException, InstantiationException, IllegalAccessException, IOException {

        if (type != null) {

            Ticket ticket = ticketService.getTicket(period, ticketId);

            if (ticket == null)
                throw new HttpBadRequestException(MessagesUtil.getMessage(
                        messageSource, "error"));

            model.put(VIEW_TICKET, ticket);

            List<TicketPost> ticketPosts = ticketService
                    .getTicketPosts(ticketId);
            model.put(VIEW_TICKET_POSTS, ticketPosts);

            List<String> statuses = appSettingsService.getTicketStatusAll();
            model.put(TICKET_STATUSES, statuses);

            String currentExecutive = principal.getName();
            model.put(EXECUTIVE, currentExecutive);

            if (message != null && !message.isEmpty())
                model.put(HELPDESK_MESSAGE, message);

            if (!ticket.getReversal())
                model.put(TICKET_REVERSAL, true);

            // тикет может быть без привязки к транзакции
            // для таких нужно запретить сторнирование, поэтому
            // вводим этот признак
            if (ticket.getVoucherRowId() != null)
                model.put(TICKET_MISC, true);
            else
                model.put(TICKET_MISC, false);

            System.out.println("==============================");
            System.out.println("checking voucherRowId presents");
            System.out.println("");
            System.out.println("voucherRowId: " + ticket.getVoucherRowId());
            System.out.println("ticketMisc:   " + model.get(TICKET_MISC));
            System.out.println("");

            if (type.equals(VIEW_SHOW_NEW)) {
                model.put(VIEW_TYPE, VIEW_SHOW_NEW);
            } else if (type.equals(VIEW_SHOW_PERSONAL)) {
                model.put(VIEW_TYPE, VIEW_SHOW_PERSONAL);
            } else if (type.equals(VIEW_SHOW_ALL)) {
                model.put(VIEW_TYPE, VIEW_SHOW_ALL);
            }

        }

        return new ModelAndView(VIEW_TICKET_PAGE, model);
    }

    @RolesAllowed(User.ROLE_HELPDESK)
    @RequestMapping(value = TICKET_POST_URL, method = RequestMethod.POST)
    public ModelAndView helpdeskAddPost(
            @RequestParam(VIEW_PATH_TICKET_ID) String ticketId,
            @RequestParam("period") String period,
            @RequestParam("subject") String subject,
            @RequestParam("ticketDesc") String ticketDesc,
            @RequestParam("ticketStatus") String ticketStatus,
            @RequestParam("ticketReversal") Boolean ticketReversal,
            @RequestParam("transactionRowId") String transactionRowId,
            Principal principal) throws ItemSizeLimitExceededException,
            HttpBadRequestException, JsonParseException, JsonMappingException, DOMException, IOException, ParserConfigurationException, OlekstraException, TransformerException, MessageSendException {

        Ticket ticket = ticketService.getTicket(period, ticketId);
        if (ticket == null)
            throw new HttpBadRequestException(MessagesUtil.getMessage(
                    messageSource, "error"));

        if (ticketDesc.trim().isEmpty()) {
            Map<String, Object> model = new HashMap<String, Object>();
            model.put(VIEW_PATH_TICKET_ID, ticketId);
            model.put(VIEW_PARAM_TYPE, VIEW_SHOW_PERSONAL);
            model.put("period", period);
            model.put(HELPDESK_MESSAGE, MessagesUtil.getMessage(messageSource,
                    "ticketPostEmpty"));
            return new ModelAndView("redirect:" + TICKET_EDIT_URL, model);
        }

        boolean ticketClose = ticketService.isClosedStatus(ticketStatus);

        if (!ticket.getLastStatus().equalsIgnoreCase(ticketStatus)
                && ticketReversal.booleanValue() == false) {
            ticket.setLastStatus(ticketStatus);
            ticket.setLastPost(ticketDesc);
            ticketService.saveTicket(ticket);
        }

        TicketPost post = new TicketPost(UUID.fromString(ticketId),
                principal.getName(),
                subject,
                ticketDesc,
                ticketStatus,
                transactionRowId,
                dateService.createDefaultDateTime());

        ticketService.addPost(post);

        if (ticketReversal.booleanValue() == true) {
            if (!ticket.getReversal()) {

                ticketService.reverseTicketTransaction(ticket);

                ticket.setReversal(true);
                ticket.setLastStatus(ticketStatus);
                ticketService.saveTicket(ticket);
            }
        }

        if (ticketClose == true) {
            ticket.setLastStatus(ticketStatus);
            ticketService.removeAssignedTicket(ticket.getExecutive(), ticketId);
            return new ModelAndView(HELPDESK_URL, new ModelMap(
                    HELPDESK_MESSAGE_SUCCESS, MessagesUtil.getMessage(
                            messageSource,
                            "ticketPostClosed")));
        } else {
            Map<String, Object> model = new HashMap<String, Object>();
            model.put(VIEW_PATH_TICKET_ID, ticketId);
            model.put(VIEW_PARAM_TYPE, VIEW_SHOW_PERSONAL);
            model.put("period", period);
            model.put(HELPDESK_MESSAGE, MessagesUtil.getMessage(messageSource,
                    "ticketPostSent"));
            return new ModelAndView("redirect:" + TICKET_EDIT_URL, model);
        }
    }

    @RolesAllowed(User.ROLE_HELPDESK)
    @RequestMapping(value = TICKET_ASSIGN_URL, method = RequestMethod.POST)
    public ModelAndView helpdeskAssignTicket(
            @RequestParam("ticketId") String ticketId,
            @RequestParam("period") String period,
            Principal principal) throws IOException {

        String executive = principal.getName();

        Ticket ticket = ticketService.getTicket(period, ticketId);

        if (ticketService.getTicketExecutive(ticketId) != null) {
            return new ModelAndView(HELPDESK_URL,
                    new ModelMap(HELPDESK_MESSAGE_FAILURE,
                            MessagesUtil.getMessage(messageSource,
                                    "ticketAssignFailure")));
        }

        ticketService.assignTicket(ticket, executive);

        Map<String, Object> model = new HashMap<String, Object>();
        model.put(VIEW_PATH_TICKET_ID, ticketId);
        model.put(VIEW_PARAM_TYPE, VIEW_SHOW_PERSONAL);
        model.put("period", period);
        model.put(HELPDESK_MESSAGE, MessagesUtil.getMessage(messageSource,
                "ticketAssignSuccess"));

        return new ModelAndView("redirect:" + TICKET_EDIT_URL, model);
    }

    @RolesAllowed(User.ROLE_HELPDESK)
    @RequestMapping(value = TICKET_REASSIGN_URL, method = RequestMethod.POST)
    public ModelAndView helpdeskReassignTicket(
            @RequestParam("ticketId") String ticketId,
            @RequestParam("period") String period,
            Principal principal) throws IOException {

        String executive = principal.getName();
        Ticket ticket = ticketService.getTicket(period, ticketId);
        String prevExecutive = ticket.getExecutive();

        ticketService.removeAssignedTicket(prevExecutive, ticketId);

        ticketService.assignTicket(ticket, executive);

        Map<String, Object> model = new HashMap<String, Object>();
        model.put(VIEW_PATH_TICKET_ID, ticketId);
        model.put(VIEW_PARAM_TYPE, VIEW_SHOW_PERSONAL);
        model.put("period", period);
        model.put(HELPDESK_MESSAGE, MessagesUtil.getMessage(messageSource,
                "ticketReassignSuccess"));

        return new ModelAndView("redirect:" + TICKET_EDIT_URL, model);
    }

    @RolesAllowed(User.ROLE_HELPDESK)
    @RequestMapping(value = NEW_TICKETS_URL, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> helpdeskNewTicketsCount() {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        return new ResponseEntity<String>("{\"count\":\""
                + ticketService.getNewTicketsCount() + "\"}", headers,
                HttpStatus.OK);
    }
}
