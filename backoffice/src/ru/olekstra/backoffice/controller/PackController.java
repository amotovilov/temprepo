package ru.olekstra.backoffice.controller;

import java.util.Map;

import javax.annotation.security.RolesAllowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ru.olekstra.awsutils.ComplexKey;
import ru.olekstra.backoffice.dto.ImportDto;
import ru.olekstra.backoffice.dto.PackImportDto;
import ru.olekstra.backoffice.util.AjaxUtil;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.common.util.MessagesUtil;
import ru.olekstra.domain.Pack;
import ru.olekstra.domain.User;
import ru.olekstra.exception.InvalidFormatDataException;
import ru.olekstra.service.PackService;

@Controller
public class PackController {

    private PackService service;
    private AppSettingsService settingsService;
    private MessageSource messageSource;

    public static final String URL_ROOT = "/goods/";
    public static final String URL_UPLOAD = URL_ROOT + "upload";

    public static final String FILE_UPLOAD_FORM = "fileUploadForm";

    public PackController() {
    }

    @Autowired
    public PackController(PackService service,
            AppSettingsService settingsService, MessageSource messageSource) {

        this.service = service;
        this.settingsService = settingsService;
        this.messageSource = messageSource;
    }

    @RolesAllowed(User.ROLE_ADMINISTRATION)
    @RequestMapping(value = "/goods/import", method = RequestMethod.GET)
    public ModelAndView goodsImport(ModelMap model) {
        return new ModelAndView("goodsImport");
    }

    // Отдельным параметром принимаем skipLine, т.к. не удалось сделать
    // автоматический маппинг чекбокс в UI
    // на dto сущность
    @RolesAllowed(User.ROLE_ADMINISTRATION)
    @RequestMapping(value = URL_UPLOAD, method = RequestMethod.POST)
    @ResponseBody
    public Map<String, ?> importGoods(
            @ModelAttribute(FILE_UPLOAD_FORM) ImportDto dto,
            BindingResult result,
            @RequestParam(value = "skipLine", required = false) boolean skipLine)
            throws InterruptedException {

        dto.setSkipFirstLine(skipLine);

        Map<String, Object> responseBody = null;
        switch (dto.getAction()) {
        // ///////////////////////////////////////////////////////////////
        case 1: // Append to slave table
            responseBody = storePacks(result, dto, settingsService
                    .getSlaveTablePackEntity());
            break;
        // ///////////////////////////////////////////////////////////////
        case 2: // Prepare slave table
            boolean success = service.prepareSlaveTable();
            if (success) {
                responseBody = AjaxUtil.getSuccessResponse(MessagesUtil
                        .getMessage(messageSource, "slaveTableIsReady"));
            } else {
                responseBody = AjaxUtil.getFailureResponse(MessagesUtil
                        .getMessage(messageSource, "slaveTableIsNotReady"));
            }
            break;
        // ///////////////////////////////////////////////////////////////
        case 3: // Append to master table
            responseBody = storePacks(result, dto, settingsService
                    .getMasterTablePackEntity());
            break;
        // ///////////////////////////////////////////////////////////////
        default: // Action not found
            responseBody = AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                    messageSource, "actionNotFound"));
        }
        return responseBody;
    }

    private Map<String, Object> storePacks(BindingResult result, ImportDto dto,
            Class<Pack> packEntity) throws InterruptedException {

        Map<String, Object> responseBody = null;
        if (result.hasErrors()) { // file not found
            responseBody = AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                    messageSource, "fileNotFound"));
        } else { // store packs
            try {
                PackImportDto packImportResult = service.savePacks(packEntity,
                        dto);

                if (packImportResult.isSuccess()) {
                    responseBody = AjaxUtil.getSuccessResponse(MessagesUtil
                            .getMessage(messageSource, "loaded")
                            + " "
                            + packImportResult.getFileProcessingLinePointer()
                            + " "
                            + MessagesUtil
                                    .getMessage(messageSource, "theGoods"));
                } else {
                    StringBuilder keys = new StringBuilder();
                    for (ComplexKey key : packImportResult
                            .getUnprocessedItemIdList()) {
                        keys.append(key.getHashKey());
                        keys.append(", ");
                    }
                    String message = MessagesUtil.getMessage(messageSource,
                            "packimporterror")
                            + ".  ";
                    if (packImportResult.getFileProcessingLinePointer() > 0)
                        message += MessagesUtil.getMessage(messageSource,
                                "packimportlinesprocessed")
                                + " "
                                + packImportResult
                                        .getFileProcessingLinePointer() + ".  ";
                    message += MessagesUtil.getMessage(messageSource,
                            "packimportlineserror")
                            + "  "
                            + (packImportResult.getFileProcessingLinePointer() + 1)
                            + " - "
                            + (packImportResult.getFileProcessingLinePointer() + packImportResult
                                    .getBatchSize())
                            + "  "
                            + MessagesUtil.getMessage(messageSource,
                                    "packimportobjectsnotloaded")
                            + keys.toString()
                            + "  "
                            + MessagesUtil.getMessage(messageSource,
                                    "packimportunprocessedlines1")
                            + "  "
                            + (packImportResult.getFileProcessingLinePointer() + packImportResult
                                    .getBatchSize())
                            + "  "
                            + MessagesUtil.getMessage(messageSource,
                                    "packimportunprocessedlines2");

                    responseBody = AjaxUtil.getFailureResponse(message);

                }
            } catch (InvalidFormatDataException e) {
                responseBody = AjaxUtil.getFailureResponse(MessagesUtil
                        .getMessage(messageSource, "invalidDataFormat"));
            } catch (Exception e) {
                responseBody = AjaxUtil.getFailureResponse(e.getMessage());
            }
        }
        return responseBody;
    }
}
