package ru.olekstra.backoffice.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;

import ru.olekstra.backoffice.util.AjaxUtil;
import ru.olekstra.common.dao.AppSettingsDao;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.common.util.MessagesUtil;
import ru.olekstra.common.util.TimeZoneFormatter;
import ru.olekstra.domain.AppSettings;
import ru.olekstra.domain.Formular;
import ru.olekstra.domain.User;

@Controller
public class AppSettingsController {

    private AppSettingsService appSettingsService;
    private AppSettingsDao appSettingsDao;
    private MessageSource messageSource;

    public static final String URL_APP_SETTINGS = "/settings";
    public static final String URL_APP_SETTINGS_SAVE = URL_APP_SETTINGS
            + "/save";
    public static final String URL_APP_SETTINGS_EDIT = URL_APP_SETTINGS
            + "/edit";

    public static final String ATTR_NUMBER = "number";
    public static final String ATTR_ACTIVATED = "Activated";

    private LocaleResolver localeResolver;

    public AppSettingsController() {
    }

    @Autowired
    public AppSettingsController(AppSettingsService appSettingsService,
            AppSettingsDao appSettingsDao, MessageSource messageSource,
            LocaleResolver localeResolver) {
        this.appSettingsService = appSettingsService;
        this.appSettingsDao = appSettingsDao;
        this.messageSource = messageSource;
        this.localeResolver = localeResolver;
    }

    @RolesAllowed(User.ROLE_ADMINISTRATION)
    @RequestMapping(value = URL_APP_SETTINGS_EDIT, method = RequestMethod.GET)
    public ModelAndView appSettings(HttpServletRequest request) {
        Locale locale = localeResolver.resolveLocale(request);
        return new ModelAndView("settings").addObject("appSettings",
                appSettingsService).addObject("timeZoneFormatter",
                new TimeZoneFormatter(appSettingsService, locale));
    }

    @RolesAllowed(User.ROLE_ADMINISTRATION)
    @RequestMapping(value = URL_APP_SETTINGS_SAVE, method = RequestMethod.POST)
    @ResponseBody
    public Map<String, ?> saveAppSettings(
            @RequestParam(value = "packMasterTableName") String packMasterTableName,
            @RequestParam(value = "packSlaveTableName") String packSlaveTableName,
            @RequestParam(value = "smsGateSettings") String smsGateSettings,
            @RequestParam(value = "ticketStatusReopen") String ticketStatusReopen,
            @RequestParam(value = "olekstraSupportPhone") String olekstraSupportPhone,
            @RequestParam(value = "ticketStatusProcessing") List<String> ticketStatusProcessing,
            @RequestParam(value = "ticketStatusNew") List<String> ticketStatusNew,
            @RequestParam(value = "smsMaxSendThreshold") Integer smsMaxSendThreshold,
            @RequestParam(value = "ticketStatusClosed") List<String> ticketStatusClosed,
            @RequestParam(value = "ticketTheme") List<String> ticketTheme,
            @RequestParam(value = "backofficeReportBucketName") String backofficeReportBucketName,
            @RequestParam(value = "manufacturerReportBucketName") String manufacturerReportBucketName,
            @RequestParam(value = "recipeBucketName") String recipeBucketName,
            @RequestParam(value = "ticketStatusAll") List<String> ticketStatusAll,
            @RequestParam(value = "readCapacity") Long readCapacity,
            @RequestParam(value = "writeCapacity") Long writeCapacity,
            @RequestParam(value = "timeZoneDefault") String timeZoneDefault,
            @RequestParam(value = "timeZones") List<String> timeZones,
            @RequestParam(value = "nonRefundPercent") Integer nonRefundPercent,
            @RequestParam(value = "defaultPackEkt") String defaultPackEkt,
            @RequestParam(value = "apiTransactionsToken") String apiTransactinsToken,
            @RequestParam(value = "favouriteCards") List<String> favouriteCards,
            @RequestParam(value = "formularList") List<String> formularList) throws JsonGenerationException, JsonMappingException, IOException {

        Map<String, Object> responseBody = null;

        AppSettings appSetting = new AppSettings();
        appSetting.setKey(AppSettingsDao.SETTINGS_KEY);
        appSetting.setPackMasterTableName(packMasterTableName);
        appSetting.setPackSlaveTableName(packSlaveTableName);
        appSetting.setSmsGateSettings(smsGateSettings);
        appSetting.setTicketStatusReopen(ticketStatusReopen);
        appSetting.setOlekstraSupportPhone(olekstraSupportPhone);
        appSetting.setTicketStatusProcessing(ticketStatusProcessing);
        appSetting.setTicketStatusNew(ticketStatusNew);
        appSetting.setTicketStatusNewTemp(appSettingsService
                .getTicketStatusNewTemp());
        appSetting.setSmsMaxSendThreshold(smsMaxSendThreshold);
        appSetting.setTicketStatusClosed(ticketStatusClosed);
        appSetting.setTicketTheme(ticketTheme);
        appSetting.setBackofficeReportBucketName(backofficeReportBucketName);
        appSetting
                .setManufacturerReportBucketName(manufacturerReportBucketName);
        appSetting.setRecipeBucketName(recipeBucketName);
        appSetting.setTicketStatusAll(ticketStatusAll);
        appSetting.setReadCapacity(readCapacity);
        appSetting.setWriteCapacity(writeCapacity);
        appSetting.setTimeZoneDefault(timeZoneDefault);
        appSetting.setTimeZoneItems(timeZones);
        appSetting.setNonRefundPercent(nonRefundPercent);
        appSetting.setDefaultPackEkt(defaultPackEkt.trim());
        appSetting.setApiTransactionsToken(apiTransactinsToken);
        appSetting.setFavouriteCards(favouriteCards);
        List<Formular> fl = new ArrayList<Formular>();
        for(String s: formularList){
        	if(!s.trim().equals("")){
        		String[] values = s.split("=");
        		if(values.length != 2){
                    responseBody = AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                            messageSource, "saveError"));        			
        		} else {
        			Formular f = new Formular();
        			f.setId(values[0]);
        			f.setName(values[1]);
        			fl.add(f);
        		}
        	}
        }
        appSetting.setFormularList(fl);
        boolean success = appSettingsDao.saveSettings(appSetting);

        if (success) {
            appSettingsService.loadAppSettings();
            responseBody = AjaxUtil.getSuccessResponse(MessagesUtil.getMessage(
                    messageSource, "success"));
        } else {
            responseBody = AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                    messageSource, "saveError"));
        }

        return responseBody;
    }
}
