package ru.olekstra.backoffice.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.annotation.security.RolesAllowed;

import org.apache.log4j.Logger;
import org.apache.velocity.app.VelocityEngine;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.awsutils.exception.ReporNotFoundException;
import ru.olekstra.awsutils.exception.S3ConnectionException;
import ru.olekstra.backoffice.util.AjaxUtil;
import ru.olekstra.common.service.DateTimeService;
import ru.olekstra.common.util.MessagesUtil;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.Recipe;
import ru.olekstra.domain.RecipeState;
import ru.olekstra.domain.User;
import ru.olekstra.exception.NotFoundException;
import ru.olekstra.exception.NotSavedException;
import ru.olekstra.service.DrugstoreService;
import ru.olekstra.service.RecipeService;
import ru.olekstra.service.SecurityService;

import com.amazonaws.services.dynamodb.model.ProvisionedThroughputExceededException;

@Controller
public class RecipeController {

    private RecipeService service;
    private DrugstoreService drugstoreService;
    private MessageSource messageSource;
    private SecurityService securityService;
    private VelocityEngine velocityEngineApp;
    private DateTimeService dateTimeService;

    private static Logger LOGGER = Logger.getLogger(RecipeController.class);

    public static final String URL_ROOT = "/recipe/";
    public static final String URL_RECIPE_RECEIVE_VIEW = URL_ROOT + "receive";
    public static final String URL_RECIPE_PARSE_VIEW = URL_ROOT + "parse";
    public static final String URL_RECIPE_IMAGE = URL_ROOT + "image";
    public static final String URL_RECIPE_RECEIVE_SAVE = URL_ROOT
            + "receive/save";
    public static final String URL_RECIPE_PARSE_SAVE = URL_ROOT + "parse/save";
    public static final String URL_RECIPE_TRANSACTION_CHECK = URL_ROOT
            + "check";
    public static final String URL_RECIPE_PROBLEM_VIEW = URL_ROOT + "problem";
    public static final String URL_RECIPE_PROBLEM_ACCEPT = URL_ROOT
            + "problem/accept";
    public static final String URL_RECIPE_PROBLEM_REJECT = URL_ROOT
            + "problem/reject";
    public static final String URL_RECIPE_PROBLEM_TRANSACTION = URL_ROOT
            + "problem/transaction";
    public static final String URL_RECIPE_TRANSACTION_CHECK_SAVE = URL_ROOT
            + "check/save";
    public static final String URL_RECIPE_IN_PROCESS = URL_ROOT + "active";
    public static final String URL_RECIPE_REPORT_VIEW = URL_ROOT + "report";
    public static final String URL_RECIPE_REPORT_LOAD = URL_ROOT
            + "report/load";
    public static final String URL_RECIPE_REPORT_GENEARATE = URL_ROOT
            + "report/generate";

    public static final String URL_RECIPE_S3_BUCKET = "http://s3.amazonaws.com/recipe.backoffice.olekstra.ru/";

    public static final String AUTHCODE_REGEX = "[\\d]{3,}";
    public static final Pattern AUTHCODE_PATTERN = Pattern
            .compile(AUTHCODE_REGEX);

    @Autowired
    public RecipeController(RecipeService service,
            DrugstoreService drugstoreService, MessageSource messageSource,
            SecurityService securityService, VelocityEngine velocityEngineApp, DateTimeService dateTimeService) {
        this.service = service;
        this.drugstoreService = drugstoreService;
        this.messageSource = messageSource;
        this.securityService = securityService;
        this.velocityEngineApp = velocityEngineApp;
        this.dateTimeService = dateTimeService;
    }

    public RecipeController() {
    }

    @RolesAllowed(User.ROLE_RECIPE_RECEIVE)
    @RequestMapping(value = URL_RECIPE_RECEIVE_VIEW, method = RequestMethod.GET)
    public ModelAndView recipeAdd() throws JsonParseException,
            JsonMappingException, IOException {
        return new ModelAndView("recipeReceive").addObject("drugstores",
                drugstoreService.getAllDrugstores());
    }

    @RolesAllowed(User.ROLE_RECIPE_RECEIVE)
    @RequestMapping(value = URL_RECIPE_RECEIVE_SAVE, method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Map<String, Object> recipeRecieve(
            @RequestParam(value = "drugstoreId") String drugstoreId,
            @RequestParam("recipePeriod") String recipePeriod,
            @RequestParam("file") MultipartFile file) {

        try {
            InputStream image = new ByteArrayInputStream(file.getBytes());
            String fileExtension = service.getImageExtension(
                    file.getOriginalFilename()).toLowerCase();
            // проверка соответствия формата файла расширению
            boolean recipeImageCheck = service.isValidImageFormat(image,
                    fileExtension);
            // проверка формата по расширению файла
            // boolean recipeImageCheck =
            // service.isValidImageFormat(file.getOriginalFilename());

            if (recipeImageCheck) {

                String uuid = UUID.randomUUID().toString();
                String period = service.convertRecipePeriod(recipePeriod);
                String fileName = service
                        .genarateImageName(uuid, fileExtension);
                String filePath = service.getRecipeFilePath(period,
                        drugstoreId, fileName);
                String currentUser = securityService.getCurrentUser();

                Recipe recipe = new Recipe(period, uuid, Recipe.STATE_NEW,
                        drugstoreId, file.getOriginalFilename(), fileExtension,
                        dateTimeService.createDefaultDateTime());

                service.saveRecipeImage(filePath, image);
                service.saveRecipe(recipe);
                service.updateRecipeState(recipe, Recipe.STATE_NEW,
                        MessagesUtil.getMessage(messageSource,
                                "recipe.state.new",
                                new Object[] {currentUser}),
                        currentUser);

                return AjaxUtil.getSuccessResponse((MessagesUtil.getMessage(
                        messageSource, "recipe.filePutToQueueSuccess")));
            } else {
                return AjaxUtil.getFailureResponse((MessagesUtil.getMessage(
                        messageSource, "recipe.filePutToQueueFailure")));
            }
        } catch (NotSavedException nse) {
            nse.printStackTrace();
            String errorMessage = MessagesUtil.getMessage(messageSource,
                    "saveError");
            return AjaxUtil.getFailureResponse(errorMessage);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxUtil.getFailureResponse(e.getMessage());
        }
    }

    @RolesAllowed(User.ROLE_RECIPE_PARSE)
    @RequestMapping(value = URL_RECIPE_PARSE_VIEW, method = RequestMethod.GET)
    public ModelAndView recipeParse(
            @RequestParam(value = "prev", required = false) String prevSaveStatus,
            @RequestParam(value = "authcode", required = false) String prevAuthCode,
            Principal principal) throws NotSavedException, IOException {

        RecipeState recipeStateItem = service.getNextRecipeWithStateX(
                Recipe.STATE_NEW, principal.getName());

        if (recipeStateItem != null) {
            String imageURL = this.getImageUrl(recipeStateItem);
            ModelAndView mv = new ModelAndView("recipeParse");
            mv.addObject("imageURL", imageURL);
            mv.addObject("recipeStateItem", recipeStateItem);
            if ((prevSaveStatus != null && prevSaveStatus
                    .equalsIgnoreCase("ok"))
                    && (prevAuthCode != null && !prevAuthCode.isEmpty())) {
                String message = MessagesUtil
                        .getMessage(messageSource, "recipe.savedWithAuthCode",
                                new Object[] {prevAuthCode});
                mv.addObject("successMessage", message);
                mv.addObject("prevSaved", true);
            }
            return mv;
        } else {
            return new ModelAndView("recipeNothing");
        }
    }

    @RolesAllowed(User.ROLE_RECIPE_PARSE)
    @RequestMapping(value = URL_RECIPE_PARSE_SAVE, method = RequestMethod.POST)
    public ModelAndView saveParsedRecipe(@RequestParam("period") String period,
            @RequestParam("UUID") String uuid,
            @RequestParam("drugstoreId") String drugstoreId,
            @RequestParam("recipeAuthcode") String recipeAuthCode,
            @RequestParam(required = false, value = "save") String saveFlag,
            @RequestParam(required = false, value = "check") String checkFlag,
            Principal principal) throws JsonGenerationException,
            JsonMappingException, ItemSizeLimitExceededException, IOException {

        String authCode = recipeAuthCode.trim();
        RecipeState recipeStateItem = new RecipeState(period, Recipe.STATE_NEW,
                uuid, drugstoreId, authCode);
        String imageURL = this.getImageUrl(recipeStateItem);

        String nextRecipeState = "";
        String note = "";
        if (saveFlag != null) {
            nextRecipeState = Recipe.STATE_PARSED;
            note = MessagesUtil.getMessage(messageSource,
                    "recipe.state.checked", new Object[] {principal.getName()});
            if (!authCode.matches("\\d{3,}")) {
                String errorMessage = MessagesUtil.getMessage(messageSource,
                        "recipe.invalidAuthcode");
                return new ModelAndView("recipeParse").addObject("imageURL",
                        imageURL).addObject("hasError", true).addObject(
                        "errorMessage", errorMessage).addObject(
                        "recipeStateItem", recipeStateItem);
            }
        } else if (checkFlag != null) {
            nextRecipeState = Recipe.STATE_PROBLEM;
            note = MessagesUtil.getMessage(messageSource,
                    "recipe.state.problem", new Object[] {principal.getName()});
        }
        try {
            Recipe recipe = new Recipe(period, uuid, Recipe.STATE_NEW,
                    drugstoreId, null, null, dateTimeService.createDefaultDateTime());
            recipe.setAuthCode(authCode);
            service.updateRecipeState(recipe, nextRecipeState, note, principal
                    .getName());
            service.updateRecipeAuthcode(period, uuid, authCode);
        } catch (NotSavedException nse) {
            nse.printStackTrace();
            String errorMessage = MessagesUtil.getMessage(messageSource,
                    "saveError");
            return new ModelAndView("recipeParse").addObject("imageURL",
                    imageURL).addObject("hasError", true).addObject(
                    "errorMessage", errorMessage).addObject("recipeStateItem",
                    recipeStateItem);
        } catch (Exception e) {
            String errorMessage = MessagesUtil.getMessage(messageSource,
                    "recipe.statusNotUpdated");
            return new ModelAndView("recipeParse").addObject("imageURL",
                    imageURL).addObject("hasError", true).addObject(
                    "errorMessage", errorMessage).addObject("recipeStateItem",
                    recipeStateItem);
        }
        return new ModelAndView("redirect:" + URL_RECIPE_PARSE_VIEW
                + "?prev=ok&authcode=" + authCode);

    }

    /**
     * Контроллер страницы для сверки рецептов с транзакциями в БД
     * 
     * @param hasError параметр указывающий, была ли ошибка при сохранении
     *            предыдущего просмотренного рецепта
     * @return
     * @throws S3ConnectionException
     * @throws ReporNotFoundException
     * @throws IOException
     * @throws NotSavedException
     */
    @RolesAllowed(User.ROLE_RECIPE_VERIFY)
    @RequestMapping(value = URL_RECIPE_TRANSACTION_CHECK, method = RequestMethod.GET)
    public ModelAndView checkRecipe(
            // необязательный параметр, который показывает был ли на предыдущем
            // шаге успешно сохранена информация по рецепту
            @RequestParam(value = "noerror", required = false) String hasError)
            throws S3ConnectionException, ReporNotFoundException, IOException,
            NotSavedException {

        RecipeState recipeStateItem = service.getNextRecipeWithStateX(
                Recipe.STATE_PARSED, securityService.getCurrentUser());
        if (recipeStateItem != null) {
            String imageURL = this.getImageUrl(recipeStateItem);
            ProcessingTransaction transaction = service.getTransaction(
                    recipeStateItem.getPeriod(), recipeStateItem
                            .getDrugstoreId(), recipeStateItem.getAuthCode());

            ModelAndView mav = new ModelAndView("recipeCheck").addObject(
                    "imageURL", imageURL).addObject("authCode",
                    recipeStateItem.getAuthCode()).addObject("uuid",
                    recipeStateItem.getUUID()).addObject("recipePeriod",
                    recipeStateItem.getPeriod());
            if (hasError != null && !hasError.isEmpty()) {
                if (hasError.equals("false")) {
                    mav.addObject("message", MessagesUtil
                            .getMessageWithOutPrefix(messageSource,
                                    "msg.recipe.statusUpdated"));
                } else {
                    mav.addObject("error", MessagesUtil
                            .getMessageWithOutPrefix(messageSource,
                                    "msg.recipe.statusNotUpdated"));
                }
            }
            if (transaction != null) {
                mav.addObject("transactionRows", transaction.getRows())
                        .addObject("cardOwner",
                                transaction.getCard().getOwner());
            }

            return mav;
        } else {
            return new ModelAndView("recipeNothing");
        }
    }

    /**
     * Метод контроллера, выполняющий обновление статуса рецепта после сверки
     * его с транзакцией хранимой в БД
     * 
     * @param recipeItemsCorrect признак, что позиции в рецепте совпадают с
     *            транзакцией
     * @param recipePersonCorrect признак, что имя на которое выполнена
     *            транзакция, совпадает с именем в рецепте
     * @param recipeUUID UUID рецепта, необходимо для обновлениея статуса
     * @param recipePeriod период рецепта, необходимо для обновления статуса
     * @return
     * @throws S3ConnectionException
     * @throws ReporNotFoundException
     * @throws NotFoundException
     * @throws NotSavedException
     * @throws ItemSizeLimitExceededException
     */
    @RolesAllowed(User.ROLE_RECIPE_VERIFY)
    @RequestMapping(value = URL_RECIPE_TRANSACTION_CHECK_SAVE, method = RequestMethod.POST)
    public ModelAndView saveRecipeCheckResult(
            @RequestParam(value = "recipeItemsCorrect", required = false) String recipeItemsCorrect,
            @RequestParam(value = "recipePersonCorrect", required = false) String recipePersonCorrect,
            @RequestParam("recipeUUID") String recipeUUID,
            @RequestParam("recipePeriod") String recipePeriod)
            throws S3ConnectionException, ReporNotFoundException,
            NotFoundException, NotSavedException,
            ItemSizeLimitExceededException {
        Recipe recipe = service.getRecipe(recipePeriod, recipeUUID);
        boolean hasError = false;
        if (recipePersonCorrect != null && recipeItemsCorrect != null
                && !recipePersonCorrect.isEmpty()
                && !recipeItemsCorrect.isEmpty()) {
            try {
                service.updateRecipeState(recipe, Recipe.STATE_CHECKED,
                        MessagesUtil
                                .getMessage(messageSource,
                                        "recipe.state.checked",
                                        new Object[] {securityService
                                                .getCurrentUser()}),
                        securityService.getCurrentUser());
            } catch (Exception e) {
                LOGGER.debug(e);
                hasError = true;
            }
        } else {
            try {
                service.updateRecipeState(recipe, Recipe.STATE_PROBLEM,
                        MessagesUtil
                                .getMessage(messageSource,
                                        "recipe.state.problem",
                                        new Object[] {securityService
                                                .getCurrentUser()}),
                        securityService.getCurrentUser());
            } catch (Exception e) {
                LOGGER.debug(e);
                hasError = true;
            }
        }

        return new ModelAndView("redirect:" + URL_RECIPE_TRANSACTION_CHECK
                + "?hasError=" + hasError);
    }

    @RolesAllowed(User.ROLE_RECIPE_PROBLEM)
    @RequestMapping(value = URL_RECIPE_PROBLEM_VIEW, method = RequestMethod.GET)
    public ModelAndView recipeProblem(
            @RequestParam(value = "result", required = false) Boolean result,
            @RequestParam(value = "message", required = false) String message,
            @RequestParam(value = "uuid", required = false) String recipeUUID,
            @RequestParam(value = "period", required = false) String period)
            throws NotSavedException, IOException {

        RecipeState recipeState = null;

        if (recipeUUID != null && !recipeUUID.isEmpty() && period != null
                && !period.isEmpty()) {
            recipeState = service.getRecipeState(period, recipeUUID);
        } else {
            recipeState = service.getNextRecipeWithStateX(Recipe.STATE_PROBLEM,
                    securityService.getCurrentUser());
        }

        if (recipeState != null) {
            String imageURL = this.getImageUrl(recipeState);

            ModelAndView model = new ModelAndView("recipeProblem");
            if (result != null && message != null) {
                model.addObject("result", result);
                model.addObject("message", message);
            }

            return model.addObject("imageURL", imageURL).addObject("period",
                    recipeState.getPeriod()).addObject("uuid",
                    recipeState.getUUID()).addObject("authCode",
                    recipeState.getAuthCode());
        } else {
            return new ModelAndView("recipeNothing");
        }
    }

    @RolesAllowed(User.ROLE_RECIPE_PROBLEM)
    @RequestMapping(value = URL_RECIPE_PROBLEM_ACCEPT, method = RequestMethod.POST)
    public ModelAndView recipeProblemAccept(
            @RequestParam(value = "period") String period,
            @RequestParam(value = "uuid") String uuid,
            @RequestParam(value = "authcode") String authCode,
            @RequestParam(value = "cardOwner") boolean cardOwnerCorrect,
            @RequestParam(value = "transaction") boolean transactionCorrect) {

        Map<String, Object> model = new HashMap<String, Object>();
        try {
            authCode = authCode.trim();
            if (cardOwnerCorrect && transactionCorrect
                    && AUTHCODE_PATTERN.matcher(authCode).matches()) {

                Recipe recipe = service.getRecipe(period, uuid);
                String currentUser = securityService.getCurrentUser();

                service.updateRecipeState(recipe, Recipe.STATE_CHECKED,
                        MessagesUtil.getMessage(messageSource,
                                "recipe.state.accepted",
                                new Object[] {currentUser}), currentUser);

                model.put("result", true);
                model.put("message", MessagesUtil.getMessage(messageSource,
                        "recipe.accepted"));
            } else {
                model.put("result", false);
                model.put("message", MessagesUtil.getMessage(messageSource,
                        "recipe.acceptedNotSaved"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            model.put("result", false);
            model.put("message", MessagesUtil.getMessage(messageSource,
                    "recipe.statusNotUpdated"));
        }
        return new ModelAndView("redirect:" + URL_RECIPE_PROBLEM_VIEW, model);
    }

    @RolesAllowed(User.ROLE_RECIPE_PROBLEM)
    @RequestMapping(value = URL_RECIPE_PROBLEM_REJECT, method = RequestMethod.POST)
    public ModelAndView recipeProblemReject(
            @RequestParam(value = "period") String period,
            @RequestParam(value = "uuid") String uuid,
            @RequestParam(value = "rejectReason") String rejectReason) {

        Map<String, Object> model = new HashMap<String, Object>();
        try {
            Recipe recipe = service.getRecipe(period, uuid);
            String currentUser = securityService.getCurrentUser();

            service.saveRecipeRejectReason(period, uuid, rejectReason);
            service.updateRecipeState(recipe, Recipe.STATE_REJECTED,
                    MessagesUtil.getMessage(messageSource,
                            "recipe.state.rejected", new Object[] {currentUser,
                                    rejectReason}), currentUser);

            model.put("result", true);
            model.put("message", MessagesUtil.getMessage(messageSource,
                    "recipe.rejected"));
        } catch (Exception e) {
            e.printStackTrace();
            model.put("result", false);
            model.put("message", MessagesUtil.getMessage(messageSource,
                    "recipe.statusNotUpdated"));
        }
        return new ModelAndView("redirect:" + URL_RECIPE_PROBLEM_VIEW, model);
    }

    @RolesAllowed(User.ROLE_RECIPE_PROBLEM)
    @RequestMapping(value = URL_RECIPE_IN_PROCESS, method = RequestMethod.GET)
    public ModelAndView showActiveRecipeList(
            @RequestParam(value = "period", required = false) String recipePeriod)
            throws ProvisionedThroughputExceededException, IOException {

        ModelAndView mav = new ModelAndView("recipesList");
        if (recipePeriod != null && !recipePeriod.isEmpty()) {
            String period = service.convertRecipePeriod(recipePeriod);
            List<RecipeState> activeRecipies = service
                    .getActiveRecipesForPeriod(period);
            mav.addObject("recipies", activeRecipies).addObject("formatter",
                    DateTimeFormat.forPattern(Card.DB_DATETIME_FORMAT));
        }
        return mav;
    }

    @RolesAllowed(User.ROLE_RECIPE_PROBLEM)
    @RequestMapping(value = URL_RECIPE_PROBLEM_TRANSACTION, method = RequestMethod.GET)
    @ResponseBody
    public Map<String, ?> recipeProblemTransaction(
            @RequestParam(value = "period") String period,
            @RequestParam(value = "uuid") String uuid,
            @RequestParam(value = "authcode") String authCode)
            throws JsonParseException, JsonMappingException, IOException {

        try {
            Map<String, Object> modelMap = new HashMap<String, Object>();

            Recipe recipe = service.getRecipe(period, uuid);
            ProcessingTransaction transaction = service.getTransaction(period,
                    recipe.getDrugstoreId(), authCode);

            if (transaction != null) {
                Map<String, Object> model = new HashMap<String, Object>();
                model.put("cardOwnerHeader", messageSource.getMessage(
                        "label.cardOwner", null, null));
                model.put("cardOwner", transaction.getCard().getOwner());
                model.put("recipeTransactionHeader", messageSource.getMessage(
                        "label.transactionPositions", null, null));
                model.put("transactionRows", transaction.getRows());

                String parsedPostions = VelocityEngineUtils
                        .mergeTemplateIntoString(velocityEngineApp,
                                "recipeTransaction.vm", model);
                modelMap.put("parsedPositions", parsedPostions);
            }

            return modelMap;
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxUtil.getFailureResponse(e.getMessage());
        }
    }

    private String getImageUrl(RecipeState recipeState) {
        return URL_RECIPE_S3_BUCKET + recipeState.getPeriod() + "/"
                + recipeState.getDrugstoreId() + "/" + recipeState.getUUID()
                + "." + recipeState.getFileType();
    }

    @RolesAllowed({User.ROLE_DRUGSTORE_REPORTS, User.ROLE_RECIPE_PROBLEM})
    @RequestMapping(value = URL_RECIPE_REPORT_VIEW, method = RequestMethod.GET)
    public ModelAndView recipeReport() {
        return new ModelAndView("recipeReport");
    }

    @RolesAllowed({User.ROLE_DRUGSTORE_REPORTS, User.ROLE_RECIPE_PROBLEM})
    @RequestMapping(value = URL_RECIPE_REPORT_LOAD, method = RequestMethod.GET)
    @ResponseBody
    public Map<String, ?> getRecipeReportForPeriod(
            @RequestParam("period") String period) {

        try {
            period = service.convertRecipePeriod(period);
            String reportList = service.getHtmlReportList(period);
            return AjaxUtil.getSuccessResponse(reportList);
        } catch (ReporNotFoundException rnfe) {
            return AjaxUtil.getFailureResponse(1);
        } catch (S3ConnectionException sce) {
            return AjaxUtil.getFailureResponse(0);
        } catch (Exception e) {
            return AjaxUtil
                    .getFailureResponse((Object) e.getLocalizedMessage());
        }
    }

    @RolesAllowed({User.ROLE_DRUGSTORE_REPORTS, User.ROLE_RECIPE_PROBLEM})
    @RequestMapping(value = URL_RECIPE_REPORT_GENEARATE, method = RequestMethod.GET)
    @ResponseBody
    public Map<String, ?> recipeReportGenerate(
            @RequestParam("period") String period) {

        try {
            period = service.convertRecipePeriod(period);
            service.generateReport(period);
            return AjaxUtil.getSuccessResponse(null);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxUtil.getFailureResponse(e.getMessage());
        }
    }
}
