package ru.olekstra.backoffice.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.TransformerException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.dozer.Mapper;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.OlekstraException;
import ru.olekstra.azure.model.IMessage;
import ru.olekstra.azure.model.MessageSendException;
import ru.olekstra.azure.service.QueueSender;
import ru.olekstra.backoffice.util.AjaxUtil;
import ru.olekstra.backoffice.util.CsvStreamWriter;
import ru.olekstra.backoffice.util.DateTimeUtil;
import ru.olekstra.common.helper.DozerHelper;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.common.service.DateTimeService;
import ru.olekstra.common.service.LimitPeriod;
import ru.olekstra.common.service.XmlMessageService;
import ru.olekstra.common.util.MessagesUtil;
import ru.olekstra.domain.CompletedTransaction;
import ru.olekstra.domain.EntityProxy;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.Refund;
import ru.olekstra.domain.TransactionExportInfo;
import ru.olekstra.domain.TransactionTotalInfo;
import ru.olekstra.domain.User;
import ru.olekstra.domain.dto.TransactionTotalInfoDto;
import ru.olekstra.exception.InvalidDateRangeException;
import ru.olekstra.service.TransactionsService;

@Controller
public class TransactionsController {

    public static final String PERIOD_ID = "periodId";
    public static final String PARAM_DATE_FROM = "dateFrom";
    public static final String PARAM_DATE_TO = "dateTo";
    public static final String PARAM_LAST_DIGITS = "lastDigits";

    public static final String PARAM_FILE_TYPE = "filetyperadios";
    public static final String CSV_FILE_TYPE_VALUE = "csvfile";
    public static final String XML_FILE_TYPE_VALUE = "xmlfile";

    public static final String URL_ROOT = "transactions/";

    public static final String URL_TRANSACTIONS_COMPLITED_VIEW = URL_ROOT
            + "completed";
    public static final String URL_TRANSACTIONS_COMPELTED_GENERATE = URL_ROOT
            + "completed/generate";
    public static final String URL_TRANSACTIONS_COMPELTED_DOWNLOAD = URL_ROOT
            + "completed/download";
    public static final String URL_TRANSACTIONS_COMPLITED_LINK_PART = "completed/download";

    public static final String URL_TRANSACTIONS_REFUND_VIEW = URL_ROOT
            + "refund";
    public static final String URL_TRANSACTIONS_REFUND_GENERATE = URL_ROOT
            + "refund/generate";
    public static final String URL_TRANSACTIONS_REFUND_GET_XML = "api/transactions";
    public static final String URL_TRANSACTIONS_REFUND_DOWNLOAD = URL_ROOT
            + "refund/download";
    public static final String URL_TRANSACTIONS_REFUND_LINK_PART = "refund/download";
    
    public static final String URL_TRANSACTIONS_REEXPORT = URL_ROOT + "reexport";
    public static final String URL_TRANSACTIONS_REEXPORT_FIND = URL_ROOT + "reexport/find";
    public static final String URL_TRANSACTIONS_REEXPORT_RESEND = URL_ROOT + "reexport/resend";

    private TransactionsService service;
    private MessageSource messageSource;
    private DateTimeFormatter formatter;
    private AppSettingsService appSettingService;
    private DateTimeService dateTimeService;
    private DynamodbService dynamodbService;
    private Mapper dozerBeanMapper;
    private XmlMessageService xmlMessageService;
    private QueueSender sender;

    public TransactionsController() {
    }

    @Autowired
    public TransactionsController(TransactionsService service,
            MessageSource messageSource,
            DateTimeFormatter formatter,
            AppSettingsService appSettingService, 
            DateTimeService dateTimeService, 
            DynamodbService dynamodbService,
            Mapper dozerBeanMapper,
            XmlMessageService xmlMessageService,
            QueueSender sender) {

        this.service = service;
        this.messageSource = messageSource;
        this.formatter = formatter;
        this.appSettingService = appSettingService;
        this.dateTimeService = dateTimeService;
        this.dynamodbService = dynamodbService;
        this.dozerBeanMapper = dozerBeanMapper;
        this.xmlMessageService = xmlMessageService;
        this.sender = sender;
    }

    @RolesAllowed(User.ROLE_TRANSACTIONS_EXPORT)
    @RequestMapping(value = URL_TRANSACTIONS_COMPLITED_VIEW, method = RequestMethod.GET)
    @Deprecated
    public ModelAndView viewCompletedTransactions() {
        return new ModelAndView("transactions");
    }

    @RolesAllowed(User.ROLE_TRANSACTIONS_EXPORT)
    @RequestMapping(value = URL_TRANSACTIONS_COMPELTED_GENERATE, method = RequestMethod.GET)
    @ResponseBody
    @Deprecated
    public Map<String, ?> generateCompletedTransactions(
            @RequestParam(PARAM_DATE_FROM) String dateFrom,
            @RequestParam(PARAM_DATE_TO) String dateTo,
            @RequestParam(PARAM_FILE_TYPE) String fileType) throws IOException,
            XMLStreamException {

        OutputStream out = null;
        XMLStreamWriter writer = null;
        String filePath = "";
        String fileTypeIndex = "";
        try {
            List<CompletedTransaction> transactions = service
                    .getCompletedTransactions(dateFrom, dateTo);

            if (fileType.equalsIgnoreCase(CSV_FILE_TYPE_VALUE)) {
                fileTypeIndex = TransactionsService.CSV_FILE_TYPE;
                filePath = service.generateTransactionsFilePath(
                        TransactionsService.TRANSACTIONS_FILE_CSV,
                        TransactionsService.CSV_FILE_TYPE);
                out = new FileOutputStream(new File(filePath));
                CsvStreamWriter csvGenerator = new CsvStreamWriter(out,
                        formatter);

                csvGenerator.appendLine(CompletedTransaction.
                        getCompletedTransactionHeaders());
                for (CompletedTransaction transaction : transactions)
                    csvGenerator.appendLine(service
                            .getCompletedTransactionAsArray(transaction));
                out.flush();

            } else if (fileType.equalsIgnoreCase(XML_FILE_TYPE_VALUE)) {
                fileTypeIndex = TransactionsService.XML_FILE_TYPE;
                filePath = service
                        .generateTransactionsFilePath(
                                TransactionsService.TRANSACTIONS_FILE_XML,
                                TransactionsService.XML_FILE_TYPE);
                out = new FileOutputStream(new File(filePath));

                XMLOutputFactory factory = XMLOutputFactory.newInstance();
                writer = factory.createXMLStreamWriter(out);

                writer.writeStartDocument("1.0");

                writer.writeStartElement("Transactions");
                writer.writeDefaultNamespace("http://www.olekstra.ru/schema/transactions");

                for (CompletedTransaction transaction : transactions)
                    service.writeXmlData(writer, transaction, formatter);

                writer.writeEndDocument();
                writer.flush();
            }

            String fileIndex = service
                    .getTransactionsFileIndex(filePath);
            String responseData = service.getTransactionsFileLink(
                    URL_TRANSACTIONS_COMPLITED_LINK_PART, fileTypeIndex,
                    fileIndex);
            String responseMessage = MessagesUtil.getMessage(messageSource,
                    "transactionsUnloadSuccessful");

            return AjaxUtil.getSuccessResponseWithMessage(responseMessage,
                    responseData);

        } catch (IllegalArgumentException iae) {
            return AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                    messageSource, "dateFormatNotValid"));
        } catch (InvalidDateRangeException dre) {
            return AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                    messageSource, "invalidDateRange"));
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                    messageSource, "transactionsUnloadError"));
        } finally {
            if (writer != null)
                writer.close();
            if (out != null)
                out.close();
        }
    }

    @RolesAllowed(User.ROLE_TRANSACTIONS_EXPORT)
    @Deprecated
    @RequestMapping(value = URL_TRANSACTIONS_COMPELTED_DOWNLOAD + "/{fileType}"
            + "/{fileIndex}", method = RequestMethod.GET)
    public void downloadCompeltedTransactions(
            @PathVariable("fileType") String fileType,
            @PathVariable("fileIndex") String fileIndex,
            HttpServletResponse response) {

        try {
            String path = service.getTransactionsFilePath(fileType, fileIndex);

            if (fileType.equalsIgnoreCase(TransactionsService.CSV_FILE_TYPE)) {
                response.setContentType("text/csv");
                response.setHeader("Content-Disposition",
                        "attachment; filename=transactions.csv");
            } else if (fileType
                    .equalsIgnoreCase(TransactionsService.XML_FILE_TYPE)) {
                response.setContentType("text/xml");
                response.setHeader("Content-Disposition",
                        "attachment; filename=transactions.xml");
            }

            int readedBytes = 0;
            byte[] outputByte = new byte[1024];
            InputStream input = new FileInputStream(path);
            ServletOutputStream out = response.getOutputStream();

            while (-1 != (readedBytes = input.read(outputByte))) {
                out.write(outputByte, 0, readedBytes);
            }

            out.flush();
            out.close();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @RolesAllowed(User.ROLE_TRANSACTIONS_EXPORT)
    @RequestMapping(value = URL_TRANSACTIONS_REFUND_VIEW, method = RequestMethod.GET)
    public ModelAndView viewRefundTransactions() {
        return new ModelAndView("transactionsRefund");
    }

    @RolesAllowed(User.ROLE_TRANSACTIONS_EXPORT)
    @RequestMapping(value = URL_TRANSACTIONS_REFUND_GENERATE, method = RequestMethod.POST)
    @ResponseBody
    public Map<String, ?> generateRefundTransactions(
            @RequestParam(PARAM_DATE_FROM) String dateFrom,
            @RequestParam(PARAM_DATE_TO) String dateTo,
            @RequestParam(PARAM_FILE_TYPE) String fileType) throws IOException,
            XMLStreamException {

        OutputStream out = null;
        XMLStreamWriter writer = null;
        String filePath = "";
        String fileTypeIndex = "";

        try {
            List<Refund> transactions = service.getRefundTransactions(
                    dateFrom, dateTo);

            if (fileType.equalsIgnoreCase(CSV_FILE_TYPE_VALUE)) {

                fileTypeIndex = TransactionsService.CSV_FILE_TYPE;
                filePath = service.generateTransactionsFilePath(
                        TransactionsService.TRANSACTIONS_FILE_CSV,
                        TransactionsService.CSV_FILE_TYPE);
                out = new FileOutputStream(new File(filePath));
                CsvStreamWriter csvGenerator = new CsvStreamWriter(out,
                        formatter);

                csvGenerator.appendLine(service.getRefundTransactionHeaders());
                for (Refund transaction : transactions) {
                    csvGenerator.appendLine(service
                            .getRefundTransactionAsArray(transaction));
                }

                out.flush();

            } else if (fileType.equalsIgnoreCase(XML_FILE_TYPE_VALUE)) {

                fileTypeIndex = TransactionsService.XML_FILE_TYPE;
                filePath = service.generateTransactionsFilePath(
                        TransactionsService.TRANSACTIONS_FILE_XML,
                        TransactionsService.XML_FILE_TYPE);
                out = new FileOutputStream(new File(filePath));

                XMLOutputFactory factory = XMLOutputFactory.newInstance();
                writer = factory.createXMLStreamWriter(out);

                writer.writeStartDocument("1.0");
                writer.writeStartElement("Transactions");
                writer.writeDefaultNamespace("http://www.olekstra.ru/schema/transactions");

                for (Refund transaction : transactions) {
                    service.writeXmlData(writer, transaction, formatter);
                }

                writer.writeEndDocument();
                writer.flush();
            }

            String fileIndex = service
                    .getTransactionsFileIndex(filePath);
            String responseData = service.getTransactionsFileLink(
                    URL_TRANSACTIONS_REFUND_LINK_PART, fileTypeIndex,
                    fileIndex);
            String responseMessage = MessagesUtil.getMessage(messageSource,
                    "transactionsUnloadSuccessful");

            return AjaxUtil.getSuccessResponseWithMessage(responseMessage,
                    responseData);

        } catch (IllegalArgumentException iae) {
            return AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                    messageSource, "dateFormatNotValid"));
        } catch (InvalidDateRangeException dre) {
            dre.printStackTrace();
            return AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                    messageSource, "invalidDateRange"));
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                    messageSource, "transactionsUnloadError"));
        } finally {
            if (writer != null) {
                writer.close();
            }
            if (out != null) {
                out.close();
            }
        }
    }

    @RequestMapping(value = URL_TRANSACTIONS_REFUND_GET_XML, method = RequestMethod.GET, produces = "application/xml")
    public @ResponseBody
    String getRefundTransactionsXML(
            @RequestParam(value = "from", required = false, defaultValue = "") String dateFrom,
            @RequestParam(value = "to", required = false, defaultValue = "") String dateTo,
            @RequestParam(value = "token", required = true) String token,
            HttpServletResponse response) throws XMLStreamException, IOException, Exception {

        if (!appSettingService.getApiTransactionsToken().equals(token)) {
            response.sendError(403, MessagesUtil.getMessage(messageSource, "invalidApiToken"));
            return "";
        }

        DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("YYYYMM");

        DateTime d1 = null;
        DateTime d2 = null;
        try {
            d1 = dateFrom.equalsIgnoreCase("") ? (dateTimeService.createDefaultDateTime()) : dateFormatter
                    .parseDateTime(dateFrom);
            d2 = dateFrom.equalsIgnoreCase("") ? (dateTimeService.createDefaultDateTime()) : dateFormatter
                    .parseDateTime(dateTo);
        } catch (Exception e) {
            response.sendError(
                    400,
                    MessagesUtil.getMessage(messageSource, "dateFormatNotValidExample",
                            new Object[] {dateTimeService.createDefaultDateTime().toString(dateFormatter)}));
            return "";
        }

        OutputStream out = new OutputStream()
        {

            private StringBuilder string = new StringBuilder();

            @Override
            public void write(int b) throws IOException {
                this.string.append((char) b);
            }

            public String toString() {
                return this.string.toString();
            }
        };

        XMLStreamWriter writer = null;

        try {

            List<Refund> transactions = service.getRefundTransactions(
                    service.convertDateToReportFormat(d1),
                    service.convertDateToReportFormat(d2));

            XMLOutputFactory factory = XMLOutputFactory.newInstance();
            writer = factory.createXMLStreamWriter(out, "UTF-8");

            writer.writeStartDocument("UTF-8", "1.0");
            writer.writeStartElement("Transactions");
            writer.writeDefaultNamespace("http://www.olekstra.ru/schema/transactions");

            for (Refund transaction : transactions) {
                service.writeXmlData(writer, transaction, formatter);
            }

            writer.writeEndDocument();
            writer.flush();

            response.setContentType("application/xml; charset=UTF-8");
            response.setHeader("Content-Type", "application/xml");

            return out.toString();

        } catch (InvalidDateRangeException dre) {
            response.sendError(400, MessagesUtil.getMessage(messageSource, "invalidDateRange"));
            return "";
        } catch (Exception e) {
            response.sendError(400, MessagesUtil.getMessage(messageSource, "transactionsUnloadError"));
            return "";
        } finally {
            if (writer != null) {
                writer.close();
            }
            if (out != null) {
                out.close();
            }
        }
    }

    @RolesAllowed(User.ROLE_TRANSACTIONS_EXPORT)
    @RequestMapping(value = URL_TRANSACTIONS_REFUND_DOWNLOAD + "/{fileType}"
            + "/{fileIndex}", method = RequestMethod.GET)
    public void downloadRefundTransactions(
            @PathVariable("fileType") String fileType,
            @PathVariable("fileIndex") String fileIndex,
            HttpServletResponse response) {

        try {
            String path = service.getTransactionsFilePath(fileType, fileIndex);

            if (fileType.equalsIgnoreCase(TransactionsService.CSV_FILE_TYPE)) {
                response.setContentType("text/csv");
                response.setHeader("Content-Disposition",
                        "attachment; filename=transactions.csv");
            } else if (fileType
                    .equalsIgnoreCase(TransactionsService.XML_FILE_TYPE)) {
                response.setContentType("text/xml");
                response.setHeader("Content-Disposition",
                        "attachment; filename=transactions.xml");
            }

            int readedBytes = 0;
            byte[] outputByte = new byte[1024];
            InputStream input = new FileInputStream(path);
            ServletOutputStream out = response.getOutputStream();

            while (-1 != (readedBytes = input.read(outputByte))) {
                out.write(outputByte, 0, readedBytes);
            }

            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @RolesAllowed(User.ROLE_TRANSACTIONS_EXPORT)
    @RequestMapping(value = URL_TRANSACTIONS_REEXPORT, method = RequestMethod.GET)
    public ModelAndView reexportTransactions(@RequestParam(value="sendcount", required=false, defaultValue="0") int count) {
    	ModelAndView mav = new ModelAndView("reexport");
    	if(count > 0)
    		mav.addObject("sendcountmessage", "Было успешно послано транзакций: " + count);
        return mav;
    }
    
    @RolesAllowed(User.ROLE_TRANSACTIONS_EXPORT)
    @RequestMapping(value = URL_TRANSACTIONS_REEXPORT_FIND, method = RequestMethod.GET)
    public ModelAndView reexportTransactionsFind(
    		@RequestParam(PARAM_DATE_FROM) String dateFrom,
    		@RequestParam(PARAM_LAST_DIGITS) String lastDigits) throws OlekstraException {
    	
    	String period = convertReexportParamsAndTest(dateFrom, lastDigits);
        
        String value = "";
        List<TransactionTotalInfo> totalInfo = new ArrayList<TransactionTotalInfo>();
        for(int i = 0; i < 10; i++){
        	value = String.valueOf(i) + lastDigits;
        	totalInfo.addAll(dynamodbService.getObjectsAllOrDie(TransactionTotalInfo.class, new String[]{period}, new String[]{value}));
        }
        
        List<TransactionTotalInfoDto> totalInfoDto =  DozerHelper.map(dozerBeanMapper, totalInfo, TransactionTotalInfoDto.class);
        
        ModelAndView mav = new ModelAndView("reexport").addObject("totalList", totalInfoDto);
        if(totalInfoDto.size() == 0)
        	mav.addObject("notfoundmessage", "Для периода " + dateFrom + " для кард с последними двумя цифрами в номере " + lastDigits + " не найдено ни одной транзакции.");
        return mav; 
    }
    
    private String convertReexportParamsAndTest(String dateFrom, String lastDigits) throws NoSuchMessageException, OlekstraException{
    	if(Pattern.matches("\\d{2}", lastDigits) == false){
    		throw new OlekstraException(
    				messageSource.getMessage("label.reexportLastDigits", null, null)  
    				+ " должен состоять из двух цифр");
    	}    	
    	
        DateTimeUtil datesFormatter = new DateTimeUtil();
        DateTime startDate;
        String period;
        try{
	        startDate = datesFormatter.parse(dateFrom + " "
	                + DateTimeUtil.START_DAY_TIME,
	                DateTimeUtil.FORM_INPUT_PARSE_DATE_PATTERN);
	        period = LimitPeriod.DAY.getCurrentPeriod(startDate);
        }catch(Exception e){
    		throw new OlekstraException(
    				MessagesUtil.getMessage(messageSource,
                    "dateFormatNotValidExample", new Object[]{"dd.MM.yyyy"}));        	
        } 
        
        return period;
    }
    
    @RolesAllowed(User.ROLE_TRANSACTIONS_EXPORT)
    @RequestMapping(value = URL_TRANSACTIONS_REEXPORT_RESEND, method = RequestMethod.POST)
    public ModelAndView reexportTransactionsResend(
    		@RequestParam(PARAM_DATE_FROM) String dateFrom,
            @RequestParam(PARAM_LAST_DIGITS) String lastDigits) throws JsonParseException, JsonMappingException, ParserConfigurationException, IOException, OlekstraException, TransformerException, MessageSendException{
    	    	    	
    	List<TransactionExportInfo> transactionsExport = dynamodbService.getItemsByRangeKeyBegin(TransactionExportInfo.class, dateFrom + EntityProxy.getDelimeter() + lastDigits, dateFrom);
    	String[] hash = new String[transactionsExport.size()];
    	String[] range = new String[transactionsExport.size()];
    	for(int i = 0; i < transactionsExport.size();i++){
    		hash[i] = transactionsExport.get(i).getDrugstoreId();
    		range[i] = transactionsExport.get(i).getRangeKey();
    	};
    	List<ProcessingTransaction> transactions = dynamodbService.getObjectsAllOrDie(ProcessingTransaction.class, hash, range);
    	
    	for(ProcessingTransaction transaction: transactions ){
            IMessage completeMessage = xmlMessageService.createTransactionCompleteMessage(transaction);
            completeMessage.setLabel(XmlMessageService.TRANSACTION_COMPLETE);
            sender.send(completeMessage);    	
    	};
    	return new ModelAndView("redirect:").addObject("sendcount", transactions.size());
	}    
}
