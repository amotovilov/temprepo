package ru.olekstra.backoffice.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ru.olekstra.backoffice.util.AjaxUtil;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.common.service.PlanService;
import ru.olekstra.common.util.MessagesUtil;
import ru.olekstra.domain.Discount;
import ru.olekstra.domain.DiscountPlan;
import ru.olekstra.domain.Formular;
import ru.olekstra.domain.User;
import ru.olekstra.domain.dto.DiscountPlanForm;

@Controller
public class PlanController {

    private PlanService service;
    private MessageSource messageSource;
    private AppSettingsService settingService;

    public static final String QUESTIONS = "questions";

    public static final String URL_ROOT = "/plan/";
    public static final String URL_GET_ALL = URL_ROOT + "getAll";
    public static final String URL_SAVE = URL_ROOT + "save";
    public static final String URL_DELETE = URL_ROOT + "delete";

    public static final String URL_DISCOUNT_PLAN = "discount/plan";
    public static final String URL_DISCOUNT_PLAN_SAVE = "discount/plan/save";

    public PlanController() {
    }

    @Autowired
    public PlanController(PlanService service, MessageSource messageSource, AppSettingsService settingService) {
        this.service = service;
        this.messageSource = messageSource;
        this.settingService = settingService;
    }

    @RolesAllowed(User.ROLE_ADMINISTRATION)
    @RequestMapping(value = URL_DELETE, method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Map<String, Object> delete(@RequestParam(value = "id") String id) {

        service.deletePlan(id);
        return AjaxUtil.getSuccessResponse(id);

    }

    @RolesAllowed(User.ROLE_ADMINISTRATION)
    @RequestMapping(value = "/plan/import/edit", method = RequestMethod.GET)
    public ModelAndView editDiscountPlan(ModelMap model,
            @RequestParam(value = "id") String id) throws JsonParseException,
            JsonMappingException, IOException {

        DiscountPlan plan = service.getDiscountPlan(id);
        model.put("id", plan.getId());
        model.put("name", plan.getPlanName());
        model.put("maxDiscount", plan.getMaxDiscountPercent());
        model.put("description", plan.getDescription());
        model.put("detailsLink", plan.getDetailsLink());
        model.put("discounts", plan.getDiscounts());
        model.put("discountList", service.getAllDiscounts());
        model.put("selfActivationBonus", plan.getSelfActivationBonus());
        model.put("formular", plan.getFormularId());
        model.put("formularList", settingService.getFormularList());

        return new ModelAndView("discountPlanEdit", model);
    }

    @RolesAllowed(User.ROLE_ADMINISTRATION)
    @RequestMapping(value = URL_DISCOUNT_PLAN, method = RequestMethod.GET)
    public ModelAndView addDiscountPlan(ModelMap model) throws JsonParseException, JsonMappingException, IOException {
        List<Discount> discounts = service.getAllDiscounts();
        List<Formular> formularList = settingService.getFormularList();
        return new ModelAndView("discountPlanEdit").addObject("discountList",
                discounts).addObject("formularList", formularList);
    }

    @RolesAllowed(User.ROLE_ADMINISTRATION)
    @RequestMapping(value = URL_DISCOUNT_PLAN_SAVE, method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Map<String, Object> saveDiscountPlan(

            @ModelAttribute DiscountPlanForm form, BindingResult result) {

        DiscountPlanForm discountPlan = new DiscountPlanForm();
        discountPlan.setId(form.getId().trim());
        discountPlan.setName(form.getName().trim());
        discountPlan.setMaxDiscount(form.getMaxDiscount());
        discountPlan.setDescription(form.getDescription().trim());
        discountPlan.setDetailsLink(form.getDetailsLink().trim());
        discountPlan.setDiscount(form.getDiscount());
        discountPlan.setSelfActivationBonus(form.getSelfActivationBonus());
        discountPlan.setAddNew(form.isAddNew());
        discountPlan.setFormular(form.getFormular());

        Map<String, Object> responseBody = null;
        boolean successSave = false;
        if (discountPlan.isAddNew() && service.hasPlan(discountPlan.getId()))
            return AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                    messageSource, "discountPlanIdExists"));

        if (discountPlan.getDescription().length() > 1000)
            return AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                    messageSource, "descriptionTooLong"));

        String link = discountPlan.getDetailsLink();
        if (link != null && !link.isEmpty() && !link.startsWith("http://"))
            return AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                    messageSource, "badUrl"));

        String[] discountsAr = (discountPlan.getDiscount() != null ? discountPlan
                .getDiscount()
                : "").split(",");
        List<String> discountsList = new ArrayList<String>();

        for (String discount : discountsAr) {
            if (!discount.isEmpty()) {
                if (discountsList.contains(discount))
                    return AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                            messageSource, "duplicateDescount"));
                else
                    discountsList.add(discount);
            }
        }

        try {
            successSave = service.savePlan(discountPlan, discountsList);

            if (successSave) {
                responseBody = AjaxUtil.getSuccessResponse(MessagesUtil
                        .getMessage(messageSource, "saved"));
            } else {
                responseBody = AjaxUtil.getFailureResponse(MessagesUtil
                        .getMessage(messageSource, "saveError"));
            }
        } catch (Exception e) {
            responseBody = AjaxUtil.getFailureResponse(e.getLocalizedMessage());
        }

        return responseBody;
    }

    @RolesAllowed(User.ROLE_ADMINISTRATION)
    @RequestMapping(value = "/plan/import", method = RequestMethod.GET)
    public ModelAndView discountPlans(ModelMap model) throws IOException {
        List<DiscountPlan> discountPlans = service.getAllPlans();
        Collections.sort(discountPlans);
        return new ModelAndView("discount").addObject("discountPlans",
                discountPlans);
    }

}
