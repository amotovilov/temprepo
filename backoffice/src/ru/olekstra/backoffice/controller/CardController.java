package ru.olekstra.backoffice.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLEncoder;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.app.VelocityEngine;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.awsutils.exception.OlekstraException;
import ru.olekstra.backoffice.dto.BalanceOperationType;
import ru.olekstra.backoffice.dto.BatchOperationType;
import ru.olekstra.backoffice.dto.CardsBatchActivationForm;
import ru.olekstra.backoffice.dto.CardsLoadResult;
import ru.olekstra.backoffice.response.CardResponse;
import ru.olekstra.backoffice.util.AjaxUtil;
import ru.olekstra.backoffice.util.DateTimeUtil;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.common.service.CardExportService;
import ru.olekstra.common.service.CardLogService;
import ru.olekstra.common.service.DiscountService;
import ru.olekstra.common.service.PlanService;
import ru.olekstra.common.util.MessagesUtil;
import ru.olekstra.common.util.OperationUtil;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.CardLog;
import ru.olekstra.domain.DiscountPlan;
import ru.olekstra.domain.Ticket;
import ru.olekstra.domain.User;
import ru.olekstra.domain.dto.CardChangeDto;
import ru.olekstra.domain.dto.CardExportResponse;
import ru.olekstra.domain.dto.CardLogRecord;
import ru.olekstra.domain.dto.CardLogType;
import ru.olekstra.domain.dto.DiscountLimitRemaining;
import ru.olekstra.exception.DuplicatePhoneException;
import ru.olekstra.exception.InvalidDateRangeException;
import ru.olekstra.exception.NotFoundException;
import ru.olekstra.service.CardBatchService;
import ru.olekstra.service.CardOperationService;
import ru.olekstra.service.CardService;
import ru.olekstra.service.CustomUserDetails;
import ru.olekstra.service.NotificationService;
import ru.olekstra.service.TicketService;

import com.amazonaws.services.cloudfront_2012_03_15.model.InvalidArgumentException;
import com.amazonaws.services.dynamodb.model.ProvisionedThroughputExceededException;
import com.google.common.collect.Lists;

@Controller
public class CardController {
    private static final String FLD_HOLDER_NAME = "HolderName";
    private static final String FLD_IS_DISABLED = "IsDisabled";
    private static final String FLD_TEL_NUMBER = "TelephoneNumber";
    private static final String FLD_DISCOUNT_PLAN_ID = "DiscountPlanId";
    private static final String FLD_START_DATE = "StartDate";
    private static final String FLD_END_DATE = "EndDate";
    private static final String FLD_LIMIT_REMAIN = "LimitRemain";

    @Autowired
    private ServletContext servletContext;
    private CardExportService exportService;
    private AppSettingsService appSettingService;

    private CardService service;
    private CardBatchService batchService;
    private PlanService planService;
    private TicketService ticketService;
    private NotificationService notificationService;
    private CardOperationService cardOperationService;
    private CardLogService cardLogService;
    private MessageSource messageSource;
    private VelocityEngine velocityEngine;
    private DiscountService discountService;

    private static DateTimeFormatter periodFormat = DateTimeFormat
            .forPattern("yyyyMM");
    private static DateTimeFormatter outputFormat = DateTimeFormat
            .forPattern("MM.yyyy");
    private static DateTimeFormatter activationFormatter = DateTimeFormat
            .forPattern("dd.MM.yyyy");
    private static DateTimeFormatter ticketFormatter = DateTimeFormat
            .forPattern("dd.MM.yyyy HH:mm:ss");

    private static final String TEMPLATE_PATH = "ru/olekstra/service/templates/vm/";
    private static final String CARD_RECORD_TEMPLATE = "cardOperationRecords.vm";

    public static final String URL_ROOT = "/card/";
    public static final String URL_LOAD = URL_ROOT + "load";
    public static final String URL_SEARCH = URL_ROOT + "search";
    public static final String URL_ACTIVATION = URL_ROOT + "activation";
    public static final String URL_ACTIVATE = URL_ROOT + "activate";
    public static final String URL_BATCH_ACTIVATION = URL_ROOT
            + "batchcardactivation";
    public static final String URL_BATCH_ACTIVATE = URL_ROOT + "batchactivate";
    public static final String URL_BALANCE = URL_ROOT + "balance";
    public static final String URL_BALANCE_NEW = URL_ROOT + "balanceNew";
    public static final String URL_DETAILS = URL_ROOT + "details";
    public static final String URL_IMPORT = URL_ROOT + "import";
    public static final String URL_LIMITS = URL_ROOT + "limits";
    public static final String URL_PLANS = URL_ROOT + "plans";
    public static final String URL_REPORT = URL_ROOT + "report";
    public static final String URL_SAVE_BATCHES = URL_ROOT + "batches/save";
    public static final String URL_SHOW_BATCHES = URL_ROOT + "batches";
    public static final String URL_EDIT_BATCHES = URL_ROOT + "batches/edit";
    public static final String URL_PLAN_CHANGE = URL_ROOT + "plan/change";
    public static final String URL_CARD_RECORDS = URL_ROOT + "records";
    public static final String URL_CARD_DATE = URL_ROOT + "date";
    public static final String URL_CARD_CLEAR = URL_ROOT + "clear";
    public static final String URL_DATE_CHANGE = URL_ROOT + "date/change";
    public static final String URL_UPDATE_OWNER = URL_ROOT + "owner/update";
    public static final String URL_CARD_LOCK = URL_ROOT + "lock";
    public static final String URL_EXPORT_XML = "/api/export.xml";
    public static final String URL_CARD_XML = "/api/cards/{number}.xml";
    public static final String URL_CREATE_CARD_XML = "/api/cards/";

    public static final String ATTR_NUMBER = "number";
    public static final String ATTR_ACTIVATED = "Activated";

    public CardController() {
    }

    @Autowired
    public CardController(CardService service, CardBatchService batchService,
            NotificationService notificationService, PlanService planService,
            TicketService ticketService,
            CardOperationService CardOperationService,
            MessageSource messageSource, VelocityEngine velocityEngine,
            CardLogService cardLogService, DiscountService discountService,
            CardExportService exportService,
            AppSettingsService appSettingService) {

        this.service = service;
        this.batchService = batchService;
        this.planService = planService;
        this.ticketService = ticketService;
        this.notificationService = notificationService;
        this.cardOperationService = CardOperationService;
        this.discountService = discountService;
        this.messageSource = messageSource;
        this.velocityEngine = velocityEngine;
        this.cardLogService = cardLogService;
        this.appSettingService = appSettingService;
        this.exportService = exportService;
    }

    @RolesAllowed({User.ROLE_CARD_VIEW, User.ROLE_CARD_GENERAL})
    @RequestMapping(value = URL_DETAILS, method = RequestMethod.GET)
    public ModelAndView cardDetails(
            @RequestParam(value = "term", required = false) String term)
            throws JsonParseException, JsonMappingException, IOException,
            RuntimeException, InterruptedException, InstantiationException,
            IllegalAccessException, OlekstraException {
        ModelAndView mav = new ModelAndView("cardInfo");

        if (!StringUtils.isBlank(term)) {
            try {
                term = OperationUtil.clearCardNumber(term);
                Card card = service.searchCard(term);
                String planName = "";
                if (!StringUtils.isBlank(card.getDiscountPlanId()))
                    planName = planService.getDiscountPlanName(card
                            .getDiscountPlanId());

                List<String> allAvailableBatchesForCard = batchService
                        .getAllBatchNames();

                if (card.getCardBatches() != null) {
                    allAvailableBatchesForCard.removeAll(card.getCardBatches());
                }

                String period = "";
                String prevPeriod = "";
                String periodOutputString = "";
                period = DateTime.now().withZone(DateTimeZone.UTC).toString(
                        periodFormat);
                periodOutputString = DateTime.now().withZone(DateTimeZone.UTC)
                        .toString(outputFormat);
                prevPeriod = DateTime.now().withZone(DateTimeZone.UTC)
                        .minusMonths(1).toString(periodFormat);
                List<CardLogRecord> logRecordList = cardOperationService
                        .getLogRecordsOnCardStat(term, period);

                List<DiscountLimitRemaining> remainingLimits = discountService
                        .getRemainingLimits(card);
                Map<String, List<DiscountLimitRemaining>> limitsByDiscount =
                        new HashMap<String, List<DiscountLimitRemaining>>();

                if (remainingLimits != null) {
                    for (DiscountLimitRemaining limit : remainingLimits) {
                        if (limitsByDiscount.containsKey(limit.getDiscountId())) {
                            limitsByDiscount.get(limit.getDiscountId()).add(
                                    limit);
                        } else {
                            List<DiscountLimitRemaining> newList = new ArrayList<DiscountLimitRemaining>();
                            newList.add(limit);
                            limitsByDiscount
                                    .put(limit.getDiscountId(), newList);
                        }
                    }
                }

                // проверим, есть ли тикеты и заполним соответствующие
                // CardLogRecord-ы
                if (!logRecordList.isEmpty()) {
                    List<Ticket> tickets = ticketService
                            .getTicketListForPeriod(period);
                    if (tickets != null)
                        for (CardLogRecord cardLogRecord : logRecordList) {
                            if (cardLogRecord.getRecordType().equals(CardLogType.TRANSACTION.getValue())) {
                                String voucherRowId = cardLogRecord.getVoucherId()
                                        + "#" + cardLogRecord.getRow();
                                for (Ticket ticket : tickets)
                                    if (voucherRowId.equals(ticket.getVoucherRowId()))
                                        cardLogRecord.setTicketId(ticket
                                                .getTicketId()
                                                .toString());
                            }
                        }
                }

                mav.addObject("card", card)
                        .addObject("cardNumber", card.getNumber())
                        .addObject("discountplan", planName)
                        .addObject("allBatches", allAvailableBatchesForCard)
                        .addObject("cardBatches", card.getCardBatches())
                        .addObject("logRecordList", logRecordList)
                        .addObject("limitsByDiscount", limitsByDiscount)
                        .addObject("period", period)
                        .addObject("prevPeriod", prevPeriod)
                        .addObject("periodOutputString", periodOutputString)
                        .addObject("showInfo", card != null ? true : false)
                        .addObject("formatter", ticketFormatter)
                        .addObject("periodFormatter", periodFormat)
                        .addObject("activationFormatter", activationFormatter);
            } catch (NotFoundException e) {
                mav.addObject("card", null).addObject("notfound", "Не найдено")
                        .addObject("searchterm", term);
            }
        }

        List<Card> favouriteCardsData = new ArrayList<Card>();

        List<String> favouriteCardsList = appSettingService.getFavouriteCards();
        if (favouriteCardsList != null && favouriteCardsList.size() > 0) {
            favouriteCardsData = service.getCards(favouriteCardsList);
        }

        mav.addObject("favouriteCardsData", favouriteCardsData);
        return mav;
    }

    @RolesAllowed({User.ROLE_CARD_VIEW, User.ROLE_CARD_GENERAL})
    @RequestMapping(value = URL_CARD_RECORDS, method = RequestMethod.GET)
    @ResponseBody
    public Map<String, ?> cardRecords(
            @RequestParam(value = "card") String cardNumber,
            @RequestParam(value = "period") String period)
            throws JsonParseException, JsonMappingException, IOException,
            InstantiationException, IllegalAccessException {

        String prevPeriod = "";
        String periodOutputString = "";

        if (period != null) {
            DateTime requestPeriod = periodFormat.parseDateTime(period);
            periodOutputString = requestPeriod.toString(outputFormat);
            prevPeriod = requestPeriod.minusMonths(1).toString(periodFormat);
        } else {
            // Считаем от UTC
            // TODO добавить обработку ошибки периода в параметре HTTP-реквеста
            period = DateTime.now().withZone(DateTimeZone.UTC)
                    .toString(periodFormat);
            periodOutputString = DateTime.now().withZone(DateTimeZone.UTC)
                    .toString(outputFormat);
            prevPeriod = DateTime.now().withZone(DateTimeZone.UTC)
                    .minusMonths(1).toString(periodFormat);
        }

        List<CardLogRecord> cardRecords = cardOperationService
                .getLogRecordsOnCardStat(cardNumber, period);

        // проверим, есть ли тикеты и заполним соответствующие
        // CardLogRecord-ы
        if (!cardRecords.isEmpty()) {
            List<Ticket> tickets = ticketService
                    .getTicketListForPeriod(period);
            if (tickets != null)
                for (CardLogRecord cardLogRecord : cardRecords) {
                    if (cardLogRecord.getRecordType().equals(
                            CardLogType.TRANSACTION.getValue())) {
                        String voucherRowId = cardLogRecord
                                .getVoucherId()
                                + "#"
                                + cardLogRecord.getRow();
                        for (Ticket ticket : tickets)
                            if (voucherRowId.equals(ticket
                                    .getVoucherRowId()))
                                cardLogRecord.setTicketId(ticket
                                        .getTicketId()
                                        .toString());
                    }
                }
        }

        CustomUserDetails logonUser = (CustomUserDetails) SecurityContextHolder
                .getContext().getAuthentication().getPrincipal();

        Map<String, Object> model = new HashMap<String, Object>();
        model.put("cardNumber", cardNumber);
        model.put("logRecordList", cardRecords);
        model.put("period", period);
        model.put("formatter", ticketFormatter);
        model.put("springMacroRequestContext", servletContext);
        model.put("messageSource", messageSource);
        model.put("noArgs", new Object[] {});
        model.put("locale", new Locale("ru", "RU"));
        model.put("logonUser", logonUser);
        model.put("URLEncoder", URLEncoder.class);

        String parsedCardRecords = VelocityEngineUtils.mergeTemplateIntoString(
                velocityEngine, TEMPLATE_PATH + CARD_RECORD_TEMPLATE, model);

        Map<String, Object> modelMap = new HashMap<String, Object>();
        modelMap.put("prevPeriod", prevPeriod);
        modelMap.put("periodOutputString", periodOutputString);
        modelMap.put("cardRecords", parsedCardRecords);

        return modelMap;
    }

    @RolesAllowed(User.ROLE_CARD_GENERAL)
    @RequestMapping(value = URL_IMPORT, method = RequestMethod.GET)
    public ModelAndView cardsImport() throws IOException {
        List<DiscountPlan> planList = planService.getAllPlans();
        List<String> batchNamesList = batchService.getAllBatchNames();
        return new ModelAndView("cardImport").addObject("planlist", planList)
                .addObject("batchlist", batchNamesList).addObject("typeAdd",
                        BatchOperationType.ADD).addObject("typeNew",
                        BatchOperationType.NEW);
    }

    @RolesAllowed(User.ROLE_CARD_BALANCE)
    // @RequestMapping(value = URL_LIMITS, method = RequestMethod.GET)
    // For debug purpose connected for OLX-1571 change path
    @RequestMapping(value = URL_BALANCE, method = RequestMethod.GET)
    public ModelAndView cardBalance(ModelMap model) {
        return new ModelAndView("cardBalance").addObject("typeRecharge",
                BalanceOperationType.RECAHARGE).addObject("typeUpdate",
                BalanceOperationType.UPTATE).addObject("typeSet",
                BalanceOperationType.SET).addObject("messageSource", messageSource);
    }

    @RolesAllowed(User.ROLE_CARD_BALANCE)
    // @RequestMapping(value = URL_LIMITS, method = RequestMethod.GET)
    // For debug purpose connected for OLX-1571 change path
    @RequestMapping(value = URL_BALANCE_NEW, method = RequestMethod.GET)
    public ModelAndView cardBalanceNew(ModelMap model) {
        return new ModelAndView("cardBalanceNew").addObject("messageSource", messageSource);
    }

    @RolesAllowed(User.ROLE_CARD_GENERAL)
    @RequestMapping(value = URL_PLANS, method = RequestMethod.GET)
    public ModelAndView cardPlan(ModelMap model) throws IOException {
        List<DiscountPlan> plans = planService.getAllPlans();
        return new ModelAndView("cardPlan").addObject("plans", plans);
    }

    @RolesAllowed(User.ROLE_CARD_VIEW)
    @RequestMapping(value = URL_REPORT, method = RequestMethod.GET)
    public ModelAndView cardReport(ModelMap model) {
        List<String> batchNamesList = batchService.getAllBatchNames();
        Collections.sort(batchNamesList);
        return new ModelAndView("cardReport").addObject("batchlist",
                batchNamesList);
    }

    @RolesAllowed(User.ROLE_CARD_GENERAL)
    @RequestMapping(value = URL_LOAD, method = RequestMethod.POST)
    @ResponseBody
    public Map<String, ?> loadCards(
            @RequestParam(value = "plan", required = false) String planId,
            @RequestParam(value = "fromdate", required = false) String fromDate,
            @RequestParam(value = "todate", required = false) String toDate,
            @RequestParam("numbers") List<String> dirtyNumbers,
            @RequestParam("batchradios") BatchOperationType type,
            @RequestParam(value = "selectbatchname", required = false) String selectbatchname,
            @RequestParam(value = "newbatchname", required = false) String newbatchname,
            @RequestParam(value = "newbatchtitle", required = false) String newbatchtitle) {

        Map<String, Object> responseBody;
        try {
            dirtyNumbers = clearStringList(dirtyNumbers);
            Map<String, String> numberCVV = new HashMap<String, String>();
            // разобрать поступившие строки на пары number - cvv
            for (String dirtyNumber : dirtyNumbers) {
                String[] a = dirtyNumber.split("\t");
                if (a.length == 2 && a[0].matches("\\s*\\d+\\s*")
                        && a[1].matches("\\s*\\d{3}\\s*")) {
                    numberCVV.put(a[0].trim(), a[1].trim());
                }
            }
            List<String> loadedCardsList = service.createCards(planId,
                    fromDate,
                    toDate, numberCVV);

            if (loadedCardsList != null && !loadedCardsList.isEmpty()) {
                if (type == BatchOperationType.NEW) {
                    batchService.addCardsToBatch(newbatchname, newbatchtitle,
                            loadedCardsList);
                } else if (type == BatchOperationType.ADD) {
                    batchService.addCardsToBatch(selectbatchname, "",
                            loadedCardsList);
                }
                responseBody = AjaxUtil.getSuccessResponseWithMessage(
                        MessagesUtil.getMessage(messageSource, "saved"),
                        loadedCardsList.toString());
                return responseBody;
            } else {
                responseBody = AjaxUtil.getFailureResponse(MessagesUtil
                        .getMessage(messageSource, "notloaded"));
                return responseBody;
            }
        } catch (InvalidDateRangeException e) {
            responseBody = AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                    messageSource, "invalidDateRange"));
            return responseBody;
        } catch (Exception e) {
            responseBody = AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                    messageSource, "appException"));
            return responseBody;
        }

        /*
         * Map<String, Object> responseBody = null; try { responseBody =
         * AjaxUtil.getSuccessResponse(service .loadCards(planId, fromDate,
         * toDate, numbers)); } catch (Exception e) { responseBody =
         * AjaxUtil.getFailureResponse(e.getMessage()); }
         */

    }

    @RolesAllowed(User.ROLE_CARD_BALANCE)
    @RequestMapping(value = URL_BALANCE_NEW, method = RequestMethod.POST)
    @ResponseBody
    public Map<String, ?> cardsBalanceNew(
            String numbersAndValues,
            @RequestParam("reason") String reason)
            throws JsonGenerationException, JsonMappingException, IOException,
            ItemSizeLimitExceededException, RuntimeException,
            InterruptedException {

        List<String> duplicates = new ArrayList<String>();
        List<String> badLines = new ArrayList<String>();
        Map<String, BigDecimal> values = parseNumbersAndValues(numbersAndValues, duplicates, badLines);

        Map<String, Boolean> updatedCards = new HashMap<String, Boolean>();
        Map<String, BigDecimal> updatedLimits = new HashMap<String, BigDecimal>();
        List<Card> cards = new ArrayList<Card>();
        List<String> notLoadedCardNumbers = new ArrayList<String>();
        List<String> sameLimitCards = new ArrayList<String>();
        Map<String, BigDecimal> moreLimitCards = new HashMap<String, BigDecimal>();

        // загружаем карты, формируем список карт и номеров незагруженных карт
        service.loadCardsByNumbers(new ArrayList<String>(values.keySet()), cards,
                notLoadedCardNumbers);

        service.updateCardBalance(values, cards, updatedCards, updatedLimits);

        boolean notificationSent = service.sendSmsAfterUpdateCardBalance(updatedLimits, cards);

        cardOperationService.saveUpdateLimitRecords(updatedLimits, notificationSent, reason);

        List<String> notUpdatedCardList = service
                .getNotUpdatedCardsList(updatedCards);
        notUpdatedCardList.addAll(notLoadedCardNumbers);

        Map<String, Object> responseBody = null;
        try {
            String message = "";

            if (!badLines.isEmpty()) {
                message = generateMessageWithLines(
                        "msg.linesWithError", badLines);
            }
            if (!notUpdatedCardList.isEmpty()) {
                message = generateMessageWithListOfCards(
                        "msg.cardBalanceError", notUpdatedCardList);
            }
            if (!duplicates.isEmpty()) {
                if (!message.isEmpty())
                    message += "<br>";
                message += generateMessageWithListOfCards(
                        "msg.duplicateCardNumber", duplicates);
            }
            if (!sameLimitCards.isEmpty()) {
                if (!message.isEmpty())
                    message += "<br>";
                message += generateMessageWithListOfCards(
                        "msg.limitAlreadySame", sameLimitCards);
            }
            if (!moreLimitCards.isEmpty()) {
                if (!message.isEmpty())
                    message += "<br>";
                message += generateMessageWithListOfCards(
                        "msg.limitAlreadyMore",
                        formatAlreadyMoreLimitCards(moreLimitCards));
            }

            if (!message.isEmpty()) {
                responseBody = AjaxUtil.getFailureResponse(message);
            } else {
                responseBody = AjaxUtil.getSuccessResponse(null);// все ок
            }

        } catch (Exception e) {
            responseBody = AjaxUtil.getFailureResponse(e.getMessage());
        }
        return responseBody;
    }

    @RolesAllowed(User.ROLE_CARD_BALANCE)
    @RequestMapping(value = URL_BALANCE, method = RequestMethod.POST)
    @ResponseBody
    public Map<String, ?> cardsBalance(@RequestParam("summ") BigDecimal summ,
            @RequestParam("numbers") List<String> numbers,
            @RequestParam("operationType") BalanceOperationType operationType,
            @RequestParam("reason") String reason)
            throws JsonGenerationException, JsonMappingException, IOException,
            ItemSizeLimitExceededException, RuntimeException,
            InterruptedException {

        summ = summ.setScale(2, RoundingMode.HALF_UP);
        numbers = clearStringList(numbers);

        Map<String, Boolean> updatedCards = new HashMap<String, Boolean>();
        Map<String, BigDecimal> updatedLimits = new HashMap<String, BigDecimal>();
        List<Card> cards = new ArrayList<Card>();
        List<String> notLoadedCardNumbers = new ArrayList<String>();
        List<String> duplicates = new ArrayList<String>();
        List<String> numbersToUpdate = new ArrayList<String>();
        List<String> sameLimitCards = new ArrayList<String>();
        Map<String, BigDecimal> moreLimitCards = new HashMap<String, BigDecimal>();

        // готовим список номеров карт на обновление и список дубликатов
        service.prepareCardNumbers(numbers, numbersToUpdate, duplicates);
        // загружаем карты, формируем список карт и номеров незагруженных карт
        service
                .loadCardsByNumbers(numbersToUpdate, cards,
                        notLoadedCardNumbers);

        service.updateCardBalance(summ, cards, updatedCards, updatedLimits,
                sameLimitCards, moreLimitCards, operationType);

        boolean notificationSent = service.sendSmsAfterUpdateCardBalance(summ,
                updatedLimits, cards, operationType);

        cardOperationService.saveUpdateLimitRecords(updatedLimits, summ,
                notificationSent, operationType, reason);

        List<String> notUpdatedCardList = service
                .getNotUpdatedCardsList(updatedCards);
        notUpdatedCardList.addAll(notLoadedCardNumbers);

        Map<String, Object> responseBody = null;
        try {
            String message = "";
            if (!notUpdatedCardList.isEmpty()) {
                message = generateMessageWithListOfCards(
                        "msg.cardBalanceError", notUpdatedCardList);
            }
            if (!duplicates.isEmpty()) {
                if (!message.isEmpty())
                    message += "<br>";
                message += generateMessageWithListOfCards(
                        "msg.duplicateCardNumber", duplicates);
            }
            if (!sameLimitCards.isEmpty()) {
                if (!message.isEmpty())
                    message += "<br>";
                message += generateMessageWithListOfCards(
                        "msg.limitAlreadySame", sameLimitCards);
            }
            if (!moreLimitCards.isEmpty()) {
                if (!message.isEmpty())
                    message += "<br>";
                message += generateMessageWithListOfCards(
                        "msg.limitAlreadyMore",
                        formatAlreadyMoreLimitCards(moreLimitCards));
            }

            if (!message.isEmpty()) {
                responseBody = AjaxUtil.getFailureResponse(message);
            } else {
                responseBody = AjaxUtil.getSuccessResponse(null);// все ок
            }

        } catch (Exception e) {
            responseBody = AjaxUtil.getFailureResponse(e.getMessage());
        }
        return responseBody;
    }

    @RolesAllowed({User.ROLE_CARD_VIEW, User.ROLE_CARD_GENERAL})
    @RequestMapping(value = URL_SEARCH, method = RequestMethod.GET)
    @ResponseBody
    public Map<String, ?> searchCard(@RequestParam("term") String term) {

        Map<String, Object> responseBody = null;
        try {
            Card card = this.service.searchCard(term);
            String planName = "";
            if ((card.getDiscountPlanId() != null)
                    && !card.getDiscountPlanId().isEmpty())
                planName = planService.getDiscountPlanName(card
                        .getDiscountPlanId());

            responseBody = AjaxUtil.getSuccessResponse(this.getResponse(card,
                    planName));

        } catch (NotFoundException e) {
            responseBody = AjaxUtil.getFailureResponse(e.getMessage());
        }
        return responseBody;
    }

    @RolesAllowed(User.ROLE_CARD_GENERAL)
    @RequestMapping(value = URL_ACTIVATION, method = RequestMethod.GET)
    public ModelAndView cardActivation(
            ModelMap model,
            @RequestParam("cardnum") String cardnum,
            @RequestParam(value = "discountplan", required = false) String discountPlan,
            @RequestParam(value = "holder", required = false) String holder,
            @RequestParam(value = "startDate", required = false) String startDate,
            @RequestParam(value = "endDate", required = false) String endDate) throws IOException {

        List<DiscountPlan> plans = planService.getAllPlans();

        return new ModelAndView("activation")
                .addObject("cardnum", cardnum)
                .addObject("discountplan", discountPlan)
                .addObject("plans", plans)
                .addObject("holder", holder)
                .addObject("startDate", startDate)
                .addObject("endDate", endDate);
    }

    @RolesAllowed(User.ROLE_CARD_GENERAL)
    @RequestMapping(value = URL_SAVE_BATCHES, method = RequestMethod.POST)
    @ResponseBody
    public Map<String, ?> saveCardBatches(
            @RequestParam("cardnum") String cardId,
            @RequestParam("cardBatches") List<String> cardBatches)
            throws NotFoundException, JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException {

        Card card = service.getCard(cardId);
        card.setCardBatches(cardBatches);
        if (service.updateCard(card)) {
            cardOperationService.saveChangeBatchRecord(card);
            return AjaxUtil.getSuccessResponseWithMessage(MessagesUtil
                    .getMessage(messageSource, "success"), card
                    .getCardBatches());
        } else {
            return AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                    messageSource, "appException"));
        }
    }

    @RolesAllowed(User.ROLE_CARD_GENERAL)
    @RequestMapping(value = URL_SHOW_BATCHES, method = RequestMethod.GET)
    public ModelAndView showCardsBatchesEdit() throws NotFoundException {

        return new ModelAndView("cardBatches").addObject("batchlist",
                batchService.getAllBatchNames()).addObject("typeAdd",
                BatchOperationType.ADD).addObject("typeNew",
                BatchOperationType.NEW).addObject("typeDelete",
                BatchOperationType.DELETE);
    }

    @RolesAllowed(User.ROLE_CARD_GENERAL)
    @RequestMapping(value = URL_EDIT_BATCHES, method = RequestMethod.POST)
    @ResponseBody
    public Map<String, ?> editCardsBatches(
            @RequestParam("numbers") List<String> numbers,
            @RequestParam("batchradios") BatchOperationType type,
            @RequestParam(value = "selectbatchname", required = false) String selectbatchname,
            @RequestParam(value = "newbatchname", required = false) String newbatchname,
            @RequestParam(value = "newbatchtitle", required = false) String newbatchtitle,
            @RequestParam("reason") String reason) throws NotFoundException,
            UnsupportedEncodingException, IOException, InterruptedException,
            ItemSizeLimitExceededException {

        try {
            String responseMessage = "";
            numbers = clearStringList(numbers);

            List<String> duplicateCards = new ArrayList<String>();
            List<String> notFoundCards = new ArrayList<String>();
            List<Card> cards = new ArrayList<Card>();

            // берём список карт из БД (пакетное чтение);
            // исключаем из списка карт повторы и отсутствующие в БД
            // service.loadCards(numbers, cards, duplicateCards, notFoundCards);
            CardsLoadResult loadResult = service.loadCards(numbers);
            cards = loadResult.getLoadedCards();
            duplicateCards = loadResult.getDuplicateCards();
            notFoundCards = loadResult.getNotFoundCards();

            if (type == BatchOperationType.NEW) {
                batchService
                        .createBatchIfNotExists(newbatchname, newbatchtitle);
                selectbatchname = newbatchname;
            }

            List<String> unprocessedCards = new ArrayList<String>();
            unprocessedCards = service.updateCardsBatches(cards,
                    selectbatchname, type.equals(BatchOperationType.DELETE));

            cardOperationService.saveChangeBatchRecords(cards, reason);

            // Формируем список обновленных карт
            List<String> updatedCards = new ArrayList<String>(numbers);
            updatedCards.removeAll(unprocessedCards);
            updatedCards.removeAll(notFoundCards);

            if (!updatedCards.isEmpty()) {
                responseMessage = MessagesUtil.getMessage(messageSource,
                        "updatedCards")
                        + StringUtils.join(updatedCards, ",");
            }

            if (!notFoundCards.isEmpty()) {
                if (!responseMessage.isEmpty())
                    responseMessage += "\n";
                responseMessage += MessagesUtil.getMessage(messageSource,
                        "cardNotFound", new Object[] {notFoundCards.size(),
                                StringUtils.join(notFoundCards, ",")});
            }

            if (!duplicateCards.isEmpty()) {
                if (!responseMessage.isEmpty())
                    responseMessage += "\n";
                responseMessage += MessagesUtil.getMessage(messageSource,
                        "cardDuplicateNumbers", new Object[] {StringUtils.join(
                                duplicateCards, ",")});
            }

            if (!unprocessedCards.isEmpty()) {
                if (!responseMessage.isEmpty())
                    responseMessage += "\n";
                responseMessage += MessagesUtil.getMessage(messageSource,
                        "notUpdatedCards")
                        + StringUtils.join(unprocessedCards, ",");
            }
            return AjaxUtil.getSuccessResponse(responseMessage);

        } catch (ProvisionedThroughputExceededException pte) {
            return AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                    messageSource, "throwputError"));
        } catch (Exception e) {
            return AjaxUtil.getFailureResponse(e.getMessage());
        }

    }

    @RolesAllowed(User.ROLE_CARD_GENERAL)
    @RequestMapping(value = URL_ACTIVATE, method = RequestMethod.POST)
    @ResponseBody
    public Map<String, ?> activateCard(@RequestParam("number") String number,
            @RequestParam("phone") String phone,
            @RequestParam("holder") String holder,
            @RequestParam("planId") String planId,
            @RequestParam("fromDate") String fromDate,
            @RequestParam("toDate") String toDate) {

        Map<String, Object> responseBody = null;
        try {
            service.activate(OperationUtil.clearCardNumber(number),
                    OperationUtil.clearPhoneNumber(phone), holder.trim(),
                    planId, fromDate, toDate);

            cardOperationService.saveChangePlanRecord(number, planId);

            responseBody = AjaxUtil.getSuccessResponseWithMessage(MessagesUtil
                    .getMessage(messageSource, "activated"), MessagesUtil
                    .getMessage(messageSource, "success"));

            String message = "<p>Уважаемый клиент, карта № "
                    + String.valueOf(number)
                    + " была активирована на Ваш мобильный телефон.<p><br>"
                    + "<p>С любовью,<p>"
                    + "<p>&nbsp;&nbsp;Тестовый активатор, живущий на облаке.<p>";
            notificationService.sendEmailNotificationMessage(
                    NotificationService.DEFAULT_EMAIL,
                    NotificationService.DEFAULT_EMAIL, message);

        } catch (InvalidDateRangeException e) {
            responseBody = AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                    messageSource, "invalidDateRange"));
        } catch (DuplicatePhoneException e) {
            responseBody = AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                    messageSource, "duplicatePhone")
                    + e.getMessage());
        } catch (Exception e) {
            responseBody = AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                    messageSource, "appException"));
        }

        return responseBody;
    }

    @RolesAllowed(User.ROLE_CARD_GENERAL)
    @RequestMapping(value = URL_PLAN_CHANGE, method = RequestMethod.POST)
    @ResponseBody
    public Map<String, ?> cardPlanChange(@RequestParam("planId") String planId,
            @RequestParam("notification") String notification,
            @RequestParam("numbers") List<String> numbers,
            @RequestParam("reason") String reason)
            throws JsonGenerationException, JsonMappingException, IOException,
            ItemSizeLimitExceededException {

        try {
            String responseMessage = "";
            List<Card> cards = new ArrayList<Card>();
            Map<String, Boolean> updatedCards = new HashMap<String, Boolean>();
            Map<String, Boolean> activatedCards = new HashMap<String, Boolean>();
            List<String> duplicateCards = new ArrayList<String>();
            List<String> notFoundCards = new ArrayList<String>();
            List<String> alreadySubscribedCards = new ArrayList<String>();
            List<String> notUpdatedCards = new ArrayList<String>();

            numbers = clearStringList(numbers);

            // берём список карт из БД (пакетное чтение);
            // исключаем из списка карт повторы и отсутствующие в БД

            CardsLoadResult loadResult = service.loadCards(numbers);
            cards = loadResult.getLoadedCards();
            duplicateCards = loadResult.getDuplicateCards();
            notFoundCards = loadResult.getNotFoundCards();

            // исключаем из списка те, у которых уже такой дисконтплан
            service.skipAlreadySubscribed(cards,
                    planId,
                    alreadySubscribedCards);

            // непосредственно сохранение данных
            service.updateCardDiscountPlan(planId, cards, updatedCards,
                    activatedCards, notUpdatedCards);

            List<String> updatedActiveCards = service
                    .getUpdatedActiveCardNumbers(updatedCards, activatedCards);
            List<String> updatedInactiveCards = service
                    .getUpdatedInactiveCardNumbers(updatedCards, activatedCards);

            if ((notification != null) && (!notification.isEmpty())) {
                service.sendSmsAfterBatchCardUpdate(notification, service
                        .getUpdatedActiveCards(cards, updatedActiveCards));
            }

            List<String> updatedCardNumbers = new ArrayList<String>(
                    updatedActiveCards.size() + updatedInactiveCards.size());
            updatedCardNumbers.addAll(updatedActiveCards);
            updatedCardNumbers.addAll(updatedInactiveCards);

            cardOperationService.saveChangePlanRecords(updatedCardNumbers,
                    planId, reason);

            if (!updatedActiveCards.isEmpty()) {
                responseMessage = MessagesUtil.getMessage(messageSource,
                        "cardPlanUpdatedActive", new Object[] {
                                updatedActiveCards.size(),
                                StringUtils.join(updatedActiveCards, ",")});
            }

            if (!updatedInactiveCards.isEmpty()) {
                if (!responseMessage.isEmpty())
                    responseMessage += "\n";
                responseMessage += MessagesUtil.getMessage(messageSource,
                        "cardPlanUpdatedInactive", new Object[] {
                                updatedInactiveCards.size(),
                                StringUtils.join(updatedInactiveCards, ",")});
            }

            if ((notification != null) && (!notification.isEmpty())) {
                if (!responseMessage.isEmpty())
                    responseMessage += "\n";
                responseMessage += MessagesUtil.getMessage(messageSource,
                        "smsSend", new Object[] {StringUtils.join(
                                updatedActiveCards, ",")});
            }

            if (!notFoundCards.isEmpty()) {
                if (!responseMessage.isEmpty())
                    responseMessage += "\n";
                responseMessage += MessagesUtil.getMessage(messageSource,
                        "cardNotFound", new Object[] {notFoundCards.size(),
                                StringUtils.join(notFoundCards, ",")});
            }

            if (!duplicateCards.isEmpty()) {
                if (!responseMessage.isEmpty())
                    responseMessage += "\n";
                responseMessage += MessagesUtil.getMessage(messageSource,
                        "cardDuplicateNumbers", new Object[] {StringUtils.join(
                                duplicateCards, ",")});
            }

            if (!alreadySubscribedCards.isEmpty()) {
                if (!responseMessage.isEmpty())
                    responseMessage += "\n";
                responseMessage += MessagesUtil.getMessage(messageSource,
                        "cardPlanAlreadySubscribed", new Object[] {
                                alreadySubscribedCards.size(),
                                StringUtils.join(alreadySubscribedCards, ",")});
            }

            if (!notUpdatedCards.isEmpty()) {
                if (!responseMessage.isEmpty())
                    responseMessage += "\n";
                responseMessage += MessagesUtil.getMessage(messageSource,
                        "cardPlanNotUpdated", new Object[] {
                                notUpdatedCards.size(),
                                StringUtils.join(notUpdatedCards, ",")});
            }

            return AjaxUtil.getSuccessResponse(responseMessage);
            // } catch (ValidationException ve) {

        } catch (ProvisionedThroughputExceededException pte) {
            return AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                    messageSource, "throwputError"));
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxUtil.getFailureResponse(e.getMessage());
        }
    }

    @RolesAllowed(User.ROLE_CARD_GENERAL)
    @RequestMapping(value = URL_CARD_DATE, method = RequestMethod.GET)
    public ModelAndView showCardsDateEdit() throws NotFoundException {
        return new ModelAndView("cardDate");
    }

    @RolesAllowed(User.ROLE_CARD_GENERAL)
    @RequestMapping(value = URL_DATE_CHANGE, method = RequestMethod.POST)
    @ResponseBody
    public Map<String, ?> cardDateChange(
            @RequestParam("fromDate") String fromDate,
            @RequestParam("toDate") String toDate,
            @RequestParam("notification") String notification,
            @RequestParam("numbers") List<String> numbers,
            @RequestParam("reason") String reason)
            throws JsonGenerationException, JsonMappingException, IOException,
            ItemSizeLimitExceededException {

        try {
            String responseMessage = "";
            List<Card> cards = new ArrayList<Card>();
            Map<String, Boolean> updatedCards = new HashMap<String, Boolean>();
            Map<String, Boolean> activatedCards = new HashMap<String, Boolean>();
            List<String> duplicateCards = new ArrayList<String>();
            List<String> notFoundCards = new ArrayList<String>();
            List<String> notUpdatedCards = new ArrayList<String>();

            numbers = clearStringList(numbers);

            // берём список карт из БД (пакетное чтение);
            // исключаем из списка карт повторы и отсутствующие в БД
            CardsLoadResult loadResult = service.loadCards(numbers);
            cards = loadResult.getLoadedCards();
            duplicateCards = loadResult.getDuplicateCards();
            notFoundCards = loadResult.getNotFoundCards();

            // непосредственно сохранение данных
            service.updateCardDate(fromDate, toDate, cards, updatedCards,
                    activatedCards, notUpdatedCards);

            List<String> updatedActiveCards = service
                    .getUpdatedActiveCardNumbers(updatedCards, activatedCards);
            List<String> updatedInactiveCards = service
                    .getUpdatedInactiveCardNumbers(updatedCards, activatedCards);

            if ((notification != null) && (!notification.isEmpty())) {
                service.sendSmsAfterBatchCardUpdate(notification, service
                        .getUpdatedActiveCards(cards, updatedActiveCards));
            }

            List<String> updatedCardNumbers = new ArrayList<String>(
                    updatedActiveCards.size() + updatedInactiveCards.size());
            updatedCardNumbers.addAll(updatedActiveCards);
            updatedCardNumbers.addAll(updatedInactiveCards);

            cardOperationService.saveChangeDateRecords(updatedCardNumbers,
                    fromDate, toDate, reason);

            if (!updatedActiveCards.isEmpty()) {
                responseMessage = MessagesUtil.getMessage(messageSource,
                        "cardDateUpdatedActive", new Object[] {
                                updatedActiveCards.size(),
                                StringUtils.join(updatedActiveCards, ",")});
            }

            if (!updatedInactiveCards.isEmpty()) {
                if (!responseMessage.isEmpty())
                    responseMessage += "\n";
                responseMessage += MessagesUtil.getMessage(messageSource,
                        "cardDateUpdatedInactive", new Object[] {
                                updatedInactiveCards.size(),
                                StringUtils.join(updatedInactiveCards, ",")});
            }

            if ((notification != null) && (!notification.isEmpty())) {
                if (!responseMessage.isEmpty())
                    responseMessage += "\n";
                responseMessage += MessagesUtil.getMessage(messageSource,
                        "smsSend", new Object[] {StringUtils.join(
                                updatedActiveCards, ",")});
            }

            if (!notFoundCards.isEmpty()) {
                if (!responseMessage.isEmpty())
                    responseMessage += "\n";
                responseMessage += MessagesUtil.getMessage(messageSource,
                        "cardNotFound", new Object[] {notFoundCards.size(),
                                StringUtils.join(notFoundCards, ",")});
            }

            if (!duplicateCards.isEmpty()) {
                if (!responseMessage.isEmpty())
                    responseMessage += "\n";
                responseMessage += MessagesUtil.getMessage(messageSource,
                        "cardDuplicateNumbers", new Object[] {StringUtils.join(
                                duplicateCards, ",")});
            }

            if (!notUpdatedCards.isEmpty()) {
                if (!responseMessage.isEmpty())
                    responseMessage += "\n";
                responseMessage += MessagesUtil.getMessage(messageSource,
                        "cardDateNotUpdated", new Object[] {
                                notUpdatedCards.size(),
                                StringUtils.join(notUpdatedCards, ",")});
            }

            return AjaxUtil.getSuccessResponse(responseMessage);

        } catch (ProvisionedThroughputExceededException pte) {
            return AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                    messageSource, "throwputError"));
        } catch (Exception e) {
            return AjaxUtil.getFailureResponse(e.getMessage());
        }
    }

    @RolesAllowed(User.ROLE_CARD_GENERAL)
    @RequestMapping(value = URL_CARD_CLEAR, method = RequestMethod.GET)
    public ModelAndView showCardsClear() throws NotFoundException {
        return new ModelAndView("cardClear");
    }

    @RolesAllowed(User.ROLE_CARD_GENERAL)
    @RequestMapping(value = URL_CARD_CLEAR, method = RequestMethod.POST)
    @ResponseBody
    public Map<String, ?> cardClear(
            @RequestParam(value = "holder", required = false) boolean clearCardHolder,
            @RequestParam(value = "disabled", required = false) boolean clearDisabled,
            @RequestParam(value = "phone", required = false) boolean clearPhone,
            @RequestParam(value = "dates", required = false) boolean clearDates,
            @RequestParam(value = "discount", required = false) boolean clearDiscountPlan,
            @RequestParam(value = "limit", required = false) boolean clearLimit,
            @RequestParam(value = "numbers", required = true) List<String> numbers,
            @RequestParam(value = "reason", required = true) String reason)
            throws JsonGenerationException, JsonMappingException, IOException,
            ItemSizeLimitExceededException {

        try {
            if ((!clearCardHolder) && (!clearDisabled) && (!clearPhone)
                    && (!clearDates) && (!clearDiscountPlan) && (!clearLimit)) {
                throw new InvalidArgumentException(messageSource.getMessage(
                        "msg.optionsError", null, null));
            }
            if (numbers.size() == 0) {
                throw new InvalidArgumentException(messageSource.getMessage(
                        "msg.enterCardNumber", null, null));
            }
            if (reason.trim().equals("")) {
                throw new InvalidArgumentException(messageSource.getMessage(
                        "msg.reasonEmpty", null, null));
            }

            String responseMessage = "";

            numbers = clearStringList(numbers);

            // берём список карт из БД (пакетное чтение);
            // исключаем из списка карт повторы и отсутствующие в БД
            CardsLoadResult loadResult = service.loadCards(numbers);
            List<Card> cards = loadResult.getLoadedCards();
            List<String> duplicateCards = loadResult.getDuplicateCards();
            List<String> notFoundCards = loadResult.getNotFoundCards();
            Map<String, String> cardsWithValues = new HashMap<String, String>();
            Map<String, String> phones = new HashMap<String, String>();

            for (Card card : cards) {
                if (clearCardHolder == true) {
                    if (card.getHolderName() != null)
                        cardsWithValues
                                .put(card.getNumber(),
                                        cardsWithValues.get(card.getNumber()) == null ? FLD_HOLDER_NAME
                                                : cardsWithValues.get(card
                                                        .getNumber())
                                                        + ", "
                                                        + FLD_HOLDER_NAME);
                    card.setHolderName(null);
                }
                if (clearDisabled == true) {
                    if (card.getDisabled() == true)
                        cardsWithValues
                                .put(card.getNumber(),
                                        cardsWithValues.get(card.getNumber()) == null ? FLD_IS_DISABLED
                                                : cardsWithValues.get(card
                                                        .getNumber())
                                                        + ", "
                                                        + FLD_IS_DISABLED);
                    card.setDisabled(false);
                }
                if (clearDates == true) {
                    if (card.getStartDate() != null)
                        cardsWithValues
                                .put(card.getNumber(),
                                        cardsWithValues.get(card.getNumber()) == null ? FLD_START_DATE
                                                : cardsWithValues.get(card
                                                        .getNumber())
                                                        + ", "
                                                        + FLD_START_DATE);
                    if (card.getStartDate() != null)
                        cardsWithValues
                                .put(card.getNumber(),
                                        cardsWithValues.get(card.getNumber()) == null ? FLD_END_DATE
                                                : cardsWithValues.get(card
                                                        .getNumber())
                                                        + ", "
                                                        + FLD_END_DATE);
                    card.setStartDate(null);
                    card.setEndDate(null);
                }
                if (clearDiscountPlan == true) {
                    if (card.getDiscountPlanId() != null)
                        cardsWithValues
                                .put(card.getNumber(),
                                        cardsWithValues.get(card.getNumber()) == null ? FLD_DISCOUNT_PLAN_ID
                                                : cardsWithValues.get(card
                                                        .getNumber())
                                                        + ", "
                                                        + FLD_DISCOUNT_PLAN_ID);
                    card.setDiscountPlan(null);
                }
                if (clearPhone == true) {
                    if (card.getTelephoneNumber() != null) {
                        cardsWithValues
                                .put(card.getNumber(),
                                        cardsWithValues.get(card.getNumber()) == null ? FLD_TEL_NUMBER
                                                : cardsWithValues.get(card
                                                        .getNumber())
                                                        + ", "
                                                        + FLD_TEL_NUMBER);
                        phones.put(card.getNumber(), card.getTelephoneNumber());
                    }
                    card.setTelephoneNumber(null);
                }
                if (clearLimit == true) {
                    if (card.getLimitRemain() != null)
                        cardsWithValues
                                .put(card.getNumber(),
                                        cardsWithValues.get(card.getNumber()) == null ? FLD_LIMIT_REMAIN
                                                : cardsWithValues.get(card
                                                        .getNumber())
                                                        + ", "
                                                        + FLD_LIMIT_REMAIN);
                    card.setLimitRemain(null);
                }
            }
            // убрать из списка обновляемых те, в которых ничего не меняется
            // запомнить их для ответа
            Set<String> alreadyEmptyCards = new HashSet<String>();
            Iterator<Card> ic = cards.iterator();
            while (ic.hasNext()) {
                String num = ic.next().getNumber();
                if (!cardsWithValues.containsKey(num)) {
                    ic.remove();
                    alreadyEmptyCards.add(num);
                }
            }
            List<String> notUpdatedCards = service.saveCardAttributes(cards,
                    phones);

            // для записей лога оставить только те карты, которые удалось
            // обновить
            for (String notUpdatesCard : notUpdatedCards) {
                cardsWithValues.remove(notUpdatesCard);
            }

            cardOperationService.saveClearAttributesRecords(cardsWithValues,
                    reason);
            if (!cardsWithValues.isEmpty()) {
                if (!responseMessage.isEmpty())
                    responseMessage += "\n";
                responseMessage += MessagesUtil
                        .getMessage(
                                messageSource,
                                "cardAttributeCleared",
                                new Object[] {
                                        cardsWithValues.size(),
                                        StringUtils.join(
                                                cardsWithValues.keySet(), ",")});
            }

            if (!notFoundCards.isEmpty()) {
                if (!responseMessage.isEmpty())
                    responseMessage += "\n";
                responseMessage += MessagesUtil.getMessage(messageSource,
                        "cardNotFound", new Object[] {notFoundCards.size(),
                                StringUtils.join(notFoundCards, ",")});
            }

            if (!duplicateCards.isEmpty()) {
                if (!responseMessage.isEmpty())
                    responseMessage += "\n";
                responseMessage += MessagesUtil.getMessage(messageSource,
                        "cardDuplicateNumbers", new Object[] {StringUtils.join(
                                duplicateCards, ",")});
            }

            if (!notUpdatedCards.isEmpty()) {
                if (!responseMessage.isEmpty())
                    responseMessage += "\n";
                responseMessage += MessagesUtil.getMessage(messageSource,
                        "cardAttributeNotUpdated", new Object[] {
                                notUpdatedCards.size(),
                                StringUtils.join(notUpdatedCards, ",")});
            }
            if (!alreadyEmptyCards.isEmpty()) {
                if (!responseMessage.isEmpty())
                    responseMessage += "\n";
                responseMessage += MessagesUtil.getMessage(messageSource,
                        "cardAlreadyEmpty", new Object[] {
                                alreadyEmptyCards.size(),
                                StringUtils.join(alreadyEmptyCards, ",")});
            }

            return AjaxUtil.getSuccessResponse(responseMessage);

        } catch (ProvisionedThroughputExceededException pte) {
            return AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                    messageSource, "throwputError"));
        } catch (Exception e) {
            return AjaxUtil.getFailureResponse(e.getMessage());
        }
    }

    @RolesAllowed(User.ROLE_CARD_GENERAL)
    @RequestMapping(value = URL_BATCH_ACTIVATION, method = RequestMethod.GET)
    public ModelAndView showBatchCardsActivation() throws IOException {
        List<DiscountPlan> planList = planService.getAllPlans();
        return new ModelAndView("cardBatchActivation").addObject("planlist",
                planList);
    }

    @RolesAllowed(User.ROLE_CARD_GENERAL)
    @RequestMapping(value = URL_BATCH_ACTIVATE, method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Map<String, Object> activateCardBatch(
            @ModelAttribute CardsBatchActivationForm form, BindingResult result) {

        String responseMessage = "";

        // список строк файла, содержащих ошибки
        List<String> linesWithErrorList = new ArrayList<String>();
        // список строк файла с повторными картами (кроме первого вхождения)
        List<String> linesWithDuplicateList = new ArrayList<String>();
        // список не найденых карт (в том числе которые не удалось загрузить
        // из БД в операции пакетного чтерия)
        List<String> notFoundCardsList = new ArrayList<String>();
        // список уже активированных карт
        List<String> alreadyActivatedCardsList = new ArrayList<String>();
        // список успешно активированных карт
        List<String> successfullyActivatedCardNumberList = new ArrayList<String>();
        List<Card> successfullyActivatedCardList = new ArrayList<Card>();
        // список карт, которые не удалось активировать в пакетной операции
        List<String> unactivatedCardsList = new ArrayList<String>();
        // Список карт у которых в пачке указана активация на один и тот же
        // номер телефона
        List<String> duplicatesPhones = new ArrayList<String>();
        // Список карт у которых активация на номер телефона на который уже
        // активирована карта в БД
        List<String> duplicatesPhonesInDb = new ArrayList<String>();

        try {
            // берем все карты из файла для дальнейшей обработки
            List<Card> allValidCardNumsList = service.getCardsFromCsvString(
                    form.getTextInput(), linesWithErrorList,
                    linesWithDuplicateList);

            // берем карты из БД по выбранным из файла номерам и формируем
            // список карт, готовых к активации
            List<Card> cardsForActivationList = service.loadCardList(
                    allValidCardNumsList, notFoundCardsList,
                    alreadyActivatedCardsList, duplicatesPhones,
                    duplicatesPhonesInDb, form.getPlan());

            unactivatedCardsList = service
                    .activateCardsInBatch(cardsForActivationList);

            successfullyActivatedCardNumberList = service
                    .getSuccessfullyActivatedCardList(cardsForActivationList,
                            unactivatedCardsList, successfullyActivatedCardList);

            cardOperationService.saveCardActivateRecords(
                    successfullyActivatedCardList, form.getReason());

            service
                    .sendSmsAfterBatchCardActivation(successfullyActivatedCardList);

            if (!linesWithErrorList.isEmpty()) {
                responseMessage = generateMessageWithLines(
                        "msg.linesWithError", linesWithErrorList);
            }

            if (!linesWithDuplicateList.isEmpty()) {
                if (!responseMessage.isEmpty())
                    responseMessage += "\n";
                responseMessage += generateMessageWithLines(
                        "msg.linesWithDuplicates", linesWithDuplicateList);
            }

            if (!notFoundCardsList.isEmpty()) {
                if (!responseMessage.isEmpty())
                    responseMessage += "\n";
                responseMessage += generateMessageWithListOfCards(
                        "msg.cardsNotFound", notFoundCardsList);
            }

            if (!alreadyActivatedCardsList.isEmpty()) {
                if (!responseMessage.isEmpty())
                    responseMessage += "\n";
                responseMessage += generateMessageWithListOfCards(
                        "msg.alreadyActivatedCards", alreadyActivatedCardsList);
            }

            if (!unactivatedCardsList.isEmpty()) {
                if (!responseMessage.isEmpty())
                    responseMessage += "\n";
                responseMessage += generateMessageWithListOfCards(
                        "msg.notActivatedCards", unactivatedCardsList);
            }

            if (!successfullyActivatedCardNumberList.isEmpty()) {
                if (!responseMessage.isEmpty())
                    responseMessage += "\n";
                responseMessage += generateMessageWithListOfCards(
                        "msg.successfullyActivatedCards",
                        successfullyActivatedCardNumberList);
            }

            if (!duplicatesPhones.isEmpty()) {
                if (!responseMessage.isEmpty())
                    responseMessage += "\n";
                responseMessage += generateMessageWithListOfCards(
                        "msg.phoneNumbersDuplicates", duplicatesPhones);
            }

            if (!duplicatesPhonesInDb.isEmpty()) {
                if (!responseMessage.isEmpty())
                    responseMessage += "\n";
                responseMessage += generateMessageWithListOfCards(
                        "msg.dbPhoneNumbersDuplicates", duplicatesPhonesInDb);
            }

            if (!responseMessage.isEmpty())
                responseMessage += "\n";
            responseMessage += messageSource.getMessage(
                    "msg.activatedCardsCount",
                    new Object[] {successfullyActivatedCardNumberList.size()},
                    null);

            return AjaxUtil.getSuccessResponse(responseMessage);

        } catch (IOException e) {
            return AjaxUtil.getFailureResponse(e.getMessage());
        } catch (Exception e) {
            return AjaxUtil.getFailureResponse(e.getMessage());
        }
    }

    @RolesAllowed(User.ROLE_CARD_GENERAL)
    @RequestMapping(value = URL_UPDATE_OWNER, method = RequestMethod.POST)
    @ResponseBody
    public Map<String, ?> updateCardOwner(
            @RequestParam("cardNumber") String cardNumber,
            @RequestParam("ownerName") String newName,
            @RequestParam("ownerPhone") String newPhone)
            throws NotFoundException, JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException {

        Map<String, ?> response = null;
        try {
            Card card = service.getCard(cardNumber);
            String oldName = card.getHolderName();
            String oldPhone = card.getTelephoneNumber();

            card.setHolderName(newName);
            card.setTelephoneNumber(OperationUtil.clearPhoneNumber(newPhone));

            service.updateCardOwner(card, oldPhone, OperationUtil
                    .clearPhoneNumber(newPhone));

            cardOperationService.saveChangeOwnerInfoRecord(cardNumber, oldName,
                    newName, oldPhone, newPhone);

            response = AjaxUtil.getSuccessResponseWithMessage(MessagesUtil
                    .getMessage(messageSource, "saved"));
        } catch (DuplicatePhoneException e) {
            response = AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                    messageSource, "duplicatePhone")
                    + e.getMessage());
        } catch (Exception e) {
            response = AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                    messageSource, "appException"));
        }
        return response;
    }

    @RolesAllowed(User.ROLE_CARD_GENERAL)
    @RequestMapping(value = URL_CARD_LOCK, method = RequestMethod.POST)
    @ResponseBody
    public Map<String, ?> disableCard(
            @RequestParam("cardNumber") String cardNumber,
            @RequestParam("lockReason") String lockReason,
            @RequestParam("jiraTask") String jiraTask,
            @RequestParam("disable") boolean disable,
            Principal principal)
            throws NotFoundException, JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException {

        Map<String, ?> response = null;

        try {
            Card card = service.getCard(cardNumber);
            if (card == null) {
                throw new NotFoundException("Card not found by #" + cardNumber);
            }
            CardLog cardLog = cardLogService.createDisableCardTypeCardLog(card, lockReason, jiraTask, disable);
            if (principal == null) {
                throw new InvalidArgumentException("principal is null");
            }
            if (cardLog == null) {
                throw new InvalidArgumentException("cardLog is null");
            }
            cardLog.setUser(principal.getName());
            cardLogService.saveCardLog(cardLog);
            card.setDisabled(disable);
            service.updateCard(card);
            response = AjaxUtil.getSuccessResponse(messageSource);
        } catch (NotFoundException e) {
            response = AjaxUtil.getFailureResponse(AjaxUtil
                    .getFailureResponse("cardNotFound"));
        } catch (Exception e) {
            response = AjaxUtil.getFailureResponse(AjaxUtil
                    .getFailureResponse("saveError"));
        }
        return response;
    }

    private CardResponse getResponse(Card card, String planName) {
        String startDateForView = "";
        String endDateForView = "";
        if (card.getStartDate() != null & card.getEndDate() != null) {
            DateTimeUtil datesFormatter = new DateTimeUtil();
            startDateForView = datesFormatter.toStringValue(
                    card.getStartDate(), DateTimeUtil.VIEW_DATE_PATTERN);
            endDateForView = datesFormatter.toStringValue(card.getEndDate(),
                    DateTimeUtil.VIEW_DATE_PATTERN);
        }
        return new CardResponse(card.getNumber(), card.getHolderName(), card
                .getTelephoneInFormattedView(), card.getDisabled(), planName,
                card.isActive(), startDateForView, endDateForView, card
                        .getLimitRemain());
    }

    private String generateMessageWithLines(String messageCode,
            List<String> cardNumbers) {
        String newLine = System.getProperty("line.separator");
        return messageSource.getMessage(messageCode, new Object[] {newLine,
                StringUtils.join(cardNumbers, newLine)}, null);
    }

    private String generateMessageWithListOfCards(String messageCode,
            List<String> cardNumbers) {
        return messageSource.getMessage(messageCode, new Object[] {StringUtils
                .join(cardNumbers, ", ")}, null);
    }

    private Map<String, BigDecimal> parseNumbersAndValues(String numbersAndValues, List<String> duplicates,
            List<String> badLines) {
        Map<String, BigDecimal> result = new HashMap<String, BigDecimal>();
        if (StringUtils.isEmpty(numbersAndValues)) {
            return result;
        }
        List<String> list = Lists.newArrayList(numbersAndValues.split("\n"));
        Iterator<String> i = list.iterator();
        Pattern p = Pattern.compile("(\\d+)\\s+(\\d+\\.\\d\\d?)");
        while (i.hasNext()) {
            String val = i.next().trim();
            Matcher m = p.matcher(val);
            if (m.matches()) {
                try {
                    String number = m.group(1);
                    BigDecimal value = new BigDecimal(m.group(2));
                    if (result.get(number) != null) {
                        duplicates.add(number);
                    } else {
                        result.put(number, value);
                    }
                } catch (NumberFormatException ex) {
                    continue;
                }
            } else {
                badLines.add(val);
            }
        }
        return result;
    }

    private List<String> clearStringList(List<String> list) {
        List<String> result = new ArrayList<String>();
        Iterator<String> i = list.iterator();
        while (i.hasNext()) {
            String val = i.next();
            if (!val.trim().isEmpty())
                result.add(val);
        }
        return result;
    }

    private List<String> formatAlreadyMoreLimitCards(
            Map<String, BigDecimal> moreLimitCards) {
        List<String> formattedValues = new ArrayList<String>(moreLimitCards
                .size());
        for (String cardNumber : moreLimitCards.keySet()) {
            formattedValues.add(cardNumber + " - "
                    + moreLimitCards.get(cardNumber).toString());
        }
        return formattedValues;
    }

    @RequestMapping(value = URL_EXPORT_XML, method = RequestMethod.GET, produces = "application/xml")
    @ResponseBody
    public CardExportResponse getChangedCardsAsXml(
            @RequestParam(value = "changeDate", required = true) String changeDate,
            @RequestParam(value = "lastKey", required = false) String lastKey,
            @RequestParam(value = "token", required = true) String token,
            HttpServletResponse response) throws InstantiationException, IllegalAccessException, IOException,
            RuntimeException, InterruptedException {

        if (!appSettingService.getApiTransactionsToken().equals(token)) {
            response.sendError(403, MessagesUtil.getMessage(messageSource, "invalidApiToken"));
            return null;
        }

        return exportService.exportData(exportService.stringToDate(changeDate), lastKey);
    }

    @RequestMapping(value = URL_CARD_XML, method = RequestMethod.GET, produces = "application/xml")
    @ResponseBody
    public CardChangeDto getCardAsXml(
            @PathVariable("number") String cardNumber,
            @RequestParam String token,
            HttpServletResponse response) throws IOException {

        if (!appSettingService.getApiTransactionsToken().equals(token)) {
            response.sendError(403, MessagesUtil.getMessage(messageSource, "invalidApiToken"));
            return null;
        }

        return exportService.exportCard(cardNumber);
    }

    @RequestMapping(value = URL_CREATE_CARD_XML, method = RequestMethod.POST, consumes = "application/xml")
    @ResponseBody
    public void saveCardXml(@RequestBody CardChangeDto card, @RequestParam String token, HttpServletResponse response)
            throws IOException, ItemSizeLimitExceededException {

        if (!appSettingService.getApiTransactionsToken().equals(token)) {
            response.sendError(403, MessagesUtil.getMessage(messageSource, "invalidApiToken"));
            return;
        }
        if (StringUtils.isEmpty(card.getNumber())) {
            response.sendError(422);
        }
        boolean success = service.saveCardIfNotExists(card);
        if (!success) {
            response.sendError(422);
        }
    }
}