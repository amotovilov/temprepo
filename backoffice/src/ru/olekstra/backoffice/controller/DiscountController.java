package ru.olekstra.backoffice.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ru.olekstra.azure.model.IMessage;
import ru.olekstra.azure.service.QueueSender;
import ru.olekstra.backoffice.util.AjaxUtil;
import ru.olekstra.common.service.XmlMessageService;
import ru.olekstra.common.util.MessagesUtil;
import ru.olekstra.domain.Discount;
import ru.olekstra.domain.DiscountData;
import ru.olekstra.domain.DiscountLimit;
import ru.olekstra.domain.User;
import ru.olekstra.domain.dto.DiscountDataCleanMessage;
import ru.olekstra.service.DiscountEditService;

@Controller
public class DiscountController {

    private DiscountEditService service;
    private MessageSource messageSource;
    private XmlMessageService xmlMessageService;
    private QueueSender sender;

    public static final String URL_ROOT = "/discounts/";
    public static final String URL_DISCOUNT_LIST_ALL_VIEW = URL_ROOT + "import";
    public static final String URL_DISCOUNT_ADD_VIEW = URL_ROOT + "add";
    public static final String URL_DISCOUNT_EDIT_VIEW = URL_ROOT + "edit";
    public static final String URL_SAVE = URL_ROOT + "save";
    public static final String URL_DELETE = URL_ROOT + "delete";

    public DiscountController() {
    }

    @Autowired
    public DiscountController(DiscountEditService service,
            MessageSource messageSource,
            XmlMessageService xmlMessageService, QueueSender sender) {
        this.service = service;
        this.messageSource = messageSource;
        this.xmlMessageService = xmlMessageService;
        this.sender = sender;
    }

    @RolesAllowed(User.ROLE_ADMINISTRATION)
    @RequestMapping(value = URL_DISCOUNT_LIST_ALL_VIEW, method = RequestMethod.GET)
    public ModelAndView viewAll() throws IOException {
        List<Discount> discounts = service.getAll();
        Collections.sort(discounts);
        return new ModelAndView("discounts").addObject("discounts", discounts);
    }

    @RolesAllowed(User.ROLE_ADMINISTRATION)
    @RequestMapping(value = URL_DISCOUNT_ADD_VIEW, method = RequestMethod.GET)
    public ModelAndView add() {
        return new ModelAndView("discountsEdit");
    }

    @RolesAllowed(User.ROLE_ADMINISTRATION)
    @RequestMapping(value = URL_DISCOUNT_EDIT_VIEW, method = RequestMethod.GET)
    public ModelAndView edit(@RequestParam(value = "id") String id)
            throws JsonParseException, JsonMappingException, IOException {

        Discount discount = service.getDiscount(id);

        return new ModelAndView("discountsEdit").addObject("id",
                discount.getId()).addObject("name", discount.getName())
                .addObject("promoPercent", discount.getPromoPercent())
                .addObject("useCardLimit", discount.getUseCardLimit())
                .addObject("alwaysFullRefund", discount.getAlwaysFullRefund())
                .addObject("packCount", discount.getPackCount()).addObject(
                        "version", discount.getCurrentVersion());
    }

    @RolesAllowed(User.ROLE_ADMINISTRATION)
    @RequestMapping(value = URL_DELETE, method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Map<String, Object> delete(@RequestParam(value = "id") String id,
            @RequestParam(value = "name") String name) {

        try {
            List<String> relatedPlanNames = service.getRelatedDiscountPlans(id);
            if (relatedPlanNames.isEmpty()) {
                service.deleteDiscount(id);
                return AjaxUtil.getSuccessResponse(MessagesUtil.getMessage(
                        messageSource, "discountDeletingSuccess",
                        new Object[] {name}));
            } else {
                return AjaxUtil
                        .getFailureResponse(messageSource
                                .getMessage(
                                        "msg.discountDeletingIsUsed",
                                        new Object[] {
                                                name,
                                                StringUtils.join(
                                                        relatedPlanNames, ",")},
                                        null));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                    messageSource, "saveError"));
        }
    }

    @RolesAllowed(User.ROLE_ADMINISTRATION)
    @RequestMapping(value = URL_SAVE, method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Map<String, Object> save(
            @RequestParam(value = "id") String id,
            @RequestParam(value = "name") String name,
            @RequestParam(value = "version") Integer version,
            @RequestParam(value = "newDiscount") Boolean newDiscount,
            @RequestParam(value = "promoPercent", required = false) Integer promoPercent,
            @RequestParam(value = "useCardLimit", required = false) Boolean useCardLimit,
            @RequestParam(value = "alwaysFullRefund", required = false) Boolean alwaysFullRefund,
            @RequestParam(value = "packs") String packsCsv,
            @RequestParam(value = "limits") String limitsCsv) {

        try {
            if (newDiscount && service.isAlreadyExists(id)) {
                return AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                        messageSource, "discountIdExists"));

            } else if (promoPercent != null
                    && (promoPercent < 1 || promoPercent > 100)) {
                return AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                        messageSource, "promoPercentIncorrect"));

            } else {
                Map<String, DiscountData> packs = new HashMap<String, DiscountData>();
                Map<String, DiscountLimit> limits = new HashMap<String, DiscountLimit>();
                List<String> errorLines = new ArrayList<String>();
                List<String> duplicateLines = new ArrayList<String>();
                Map<String, String> packsEktSynonym = new HashMap<String, String>();

                service.parsePacksCsv(id, version, packsCsv, packs, packsEktSynonym,
                        errorLines, duplicateLines);

                service.parseLimitsCsv(limitsCsv, limits, errorLines, duplicateLines, id, version);

                if (errorLines.isEmpty() && duplicateLines.isEmpty()) {
                    int packCount = packs.size();
                    if (!newDiscount) {
                        Discount discount = service.getDiscount(id);
                        if (discount.getCurrentVersion() >= version) {
                            packCount += discount.getPackCount();
                        }
                    }
                    service.saveDiscount(id, name, promoPercent, version,
                            packCount, newDiscount,
                            useCardLimit != null ? useCardLimit : false,
                            alwaysFullRefund != null ? alwaysFullRefund : false);

                    if (!packs.isEmpty()) {
                        if (!service.saveDiscountData(id, packs, version)) {
                            throw new RuntimeException("DiscontData import fails");
                        }
                        if (!service.saveDiscountReplaces(id, packsEktSynonym,
                                version)) {
                            throw new RuntimeException("DiscontReplaces import fails");
                        }
                    }

                    if (!limits.isEmpty()) {
                        if (!service.saveDiscountLimits(id, limits.values())) {
                            throw new RuntimeException("DiscontLimit import fails");
                        }
                    }
                }

                DiscountDataCleanMessage msg = new DiscountDataCleanMessage();
                msg.setDiscountId(id);
                msg.setDiscountVersion(version);
                IMessage message = xmlMessageService.createDiscountDataCleanMessage(msg);
                sender.send(message);

                return AjaxUtil.getSuccessResponse(processCsvParsingResult(
                        errorLines, duplicateLines));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                    messageSource, "saveError"));
        }
    }

    String processCsvParsingResult(List<String> errorLines,
            List<String> duplicateLines) {

        String message = "";
        if (!errorLines.isEmpty()) {
            if (!message.isEmpty())
                message += "\n";
            message += messageSource.getMessage("msg.linesWithError",
                    new Object[] {"\n", StringUtils.join(errorLines, "\n")},
                    null);
        }

        if (!duplicateLines.isEmpty()) {
            if (!message.isEmpty())
                message += "\n";
            message += messageSource
                    .getMessage("msg.packLinesWithDuplicates", new Object[] {
                            "\n", StringUtils.join(duplicateLines, "\n")}, null);
        }

        if (message.isEmpty()) {
            message += messageSource.getMessage("msg.saved", null, null);
        }

        return message;
    }
}
