package ru.olekstra.backoffice.controller;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ru.olekstra.backoffice.util.AjaxUtil;
import ru.olekstra.common.util.MessagesUtil;
import ru.olekstra.common.util.OperationUtil;
import ru.olekstra.domain.User;
import ru.olekstra.service.UserService;

@Controller
public class UserController {

    private UserService userService;
    private MessageSource messageSource;

    public static final String URL_ROOT = "/user";
    public static final String URL_ADD = URL_ROOT + "/add";
    public static final String URL_EDIT = URL_ROOT + "/edit";
    public static final String URL_SAVE = URL_ROOT + "/save";
    public static final String URL_DELETE = URL_ROOT + "/delete/{id:.+}";

    public UserController() {
    }

    @Autowired
    public UserController(UserService userService, MessageSource messageSource) {
        this.userService = userService;
        this.messageSource = messageSource;
    }

    @RolesAllowed(User.ROLE_ADMINISTRATION)
    @RequestMapping(value = URL_ROOT, method = RequestMethod.GET)
    public ModelAndView viewUsers(ModelMap model) throws IOException {
        List<User> users = userService.getAllUsers();
        Collections.sort(users);
        return new ModelAndView("user").addObject("users", users);
    }

    @RolesAllowed(User.ROLE_ADMINISTRATION)
    @RequestMapping(value = URL_ADD, method = RequestMethod.GET)
    public ModelAndView addUser() {
        return new ModelAndView("userEdit");
    }

    @RolesAllowed(User.ROLE_ADMINISTRATION)
    @RequestMapping(value = URL_EDIT, method = RequestMethod.GET)
    public ModelAndView editUser(ModelMap model,
            @RequestParam(value = "id") String id) {
        model.addAttribute("user", userService.getUser(id));
        return new ModelAndView("userEdit", model);
    }

    @RolesAllowed(User.ROLE_ADMINISTRATION)
    @RequestMapping(value = URL_SAVE, method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Map<String, Object> saveUser(
            @RequestParam(value = "id") String id,
            @RequestParam(value = "roleCardView", required = false) boolean roleCardView,
            @RequestParam(value = "roleCardGeneral", required = false) boolean roleCardGeneral,
            @RequestParam(value = "roleCardBalance", required = false) boolean roleCardBalance,
            @RequestParam(value = "roleDrugstoreView", required = false) boolean roleDrugstoreView,
            @RequestParam(value = "roleDrugstoreEdit", required = false) boolean roleDrugstoreEdit,
            @RequestParam(value = "roleDrugstoreReports", required = false) boolean roleDrugstoreReports,
            @RequestParam(value = "roleManufacturerReports", required = false) boolean roleManufacturerReports,
            @RequestParam(value = "roleHelpdesk", required = false) boolean roleHelpdesk,
            @RequestParam(value = "roleTransactionsExport", required = false) boolean roleTransactionsExport,
            @RequestParam(value = "roleAdministration", required = false) boolean roleAdministration,
            @RequestParam(value = "roleRecipeReceive", required = false) boolean roleRecipeReceive,
            @RequestParam(value = "roleRecipeParse", required = false) boolean roleRecipeParse,
            @RequestParam(value = "roleRecipeVerify", required = false) boolean roleRecipeVerify,
            @RequestParam(value = "roleRecipeProblem", required = false) boolean roleRecipeProblem) {

        try {
            if (!OperationUtil.validateEmail(id)) {
                return AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                        messageSource, "userMailIncorrect"));
            }

            Map<String, Object> responseBody = null;
            boolean success = false;
            User user = new User(id, roleCardView, roleCardGeneral,
                    roleCardBalance, roleDrugstoreView, roleDrugstoreEdit,
                    roleDrugstoreReports, roleManufacturerReports,
                    roleHelpdesk, roleTransactionsExport, roleAdministration,
                    roleRecipeReceive, roleRecipeParse, roleRecipeVerify,
                    roleRecipeProblem);

            success = userService.saveUser(user);

            if (success) {
                responseBody = AjaxUtil.getSuccessResponse(MessagesUtil
                        .getMessage(messageSource, "saved"));
            } else {
                responseBody = AjaxUtil.getFailureResponse(MessagesUtil
                        .getMessage(messageSource, "saveError"));
            }
            return responseBody;

        } catch (Exception e) {
            return AjaxUtil.getFailureResponse(e.getLocalizedMessage());
        }
    }

    @RolesAllowed(User.ROLE_ADMINISTRATION)
    @RequestMapping(value = URL_DELETE, method = RequestMethod.DELETE, produces = "application/json")
    @ResponseBody
    public Map<String, Object> deleteUser(@PathVariable("id") String id) {
        userService.deleteUser(id);
        return AjaxUtil.getSuccessResponse(id);
    }
}
