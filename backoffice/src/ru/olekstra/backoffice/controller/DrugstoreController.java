package ru.olekstra.backoffice.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;

import ru.olekstra.backoffice.util.AjaxUtil;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.common.util.MessagesUtil;
import ru.olekstra.common.util.TimeZoneFormatter;
import ru.olekstra.common.util.VelocityMessageFormatter;
import ru.olekstra.domain.Drugstore;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.User;
import ru.olekstra.domain.dto.DrugstoreResponse;
import ru.olekstra.exception.DuplicateDrugstoreException;
import ru.olekstra.service.DrugstoreService;

import com.amazonaws.services.dynamodb.model.ProvisionedThroughputExceededException;

@Controller
public class DrugstoreController {

    private DrugstoreService service;
    private AppSettingsService appSettingsService;
    private MessageSource messageSource;
    private DateTimeFormatter dateTimeFormatter;
    private static final DateTimeFormatter dateFormatter = DateTimeFormat
            .forPattern("dd.MM.yyyy");
    private static final DateTimeFormatter timeFormatter = DateTimeFormat
            .forPattern("HH:mm:ss Z");

    public static final String URL_ROOT = "/drugstore/";
    public static final String URL_SAVE = URL_ROOT + "save";
    public static final String URL_AUTOCOMLETE = URL_ROOT + "autocomplete";
    public static final String URL_GET_OPERATIONS = URL_ROOT + "operations";

    public static final String URL_STORE_ROOT = "/store/";
    public static final String URL_STORE_DETAILS = URL_STORE_ROOT + "details";
    public static final String URL_STORE_EDIT = URL_STORE_ROOT + "edit";
    public static final String URL_STORE_SAVE = URL_STORE_ROOT + "save";

    public static final String PARAM_TERM = "term";
    public static final String PARAM_INDEX = "index";
    public static final String PARAM_ID = "id";

    private LocaleResolver localeResolver;

    public DrugstoreController() {
    }

    @Autowired
    public DrugstoreController(DrugstoreService service,
            AppSettingsService appSettingsService, MessageSource messageSource,
            DateTimeFormatter dateTimeFormatter, LocaleResolver localeResolver) {
        this.service = service;
        this.appSettingsService = appSettingsService;
        this.messageSource = messageSource;
        this.dateTimeFormatter = dateTimeFormatter;
        this.localeResolver = localeResolver;
    }

    @RolesAllowed({User.ROLE_DRUGSTORE_VIEW, User.ROLE_DRUGSTORE_EDIT})
    @RequestMapping(value = URL_AUTOCOMLETE, method = RequestMethod.GET)
    @ResponseBody
    public List<DrugstoreResponse> autocomplete(
            @RequestParam(PARAM_TERM) String term) throws IOException {

        return this.service.getResponseListFromDrugstoreList(service
                .findDrugstores(term));
    }

    @RolesAllowed(User.ROLE_DRUGSTORE_EDIT)
    @RequestMapping(value = URL_STORE_SAVE, method = RequestMethod.POST)
    @ResponseBody
    public Map<String, ?> save(@RequestParam("id") String id,
            @RequestParam("name") String name,
            @RequestParam("timeZone") String timeZone,
            @RequestParam("nonRefundPercent") Integer nonRefundPercent,
            @RequestParam("isNew") Boolean isNew,
            @RequestParam("authCode") String authCode,
            @RequestParam("disabled") Boolean disabled) {

        if ((nonRefundPercent != null)
                && ((nonRefundPercent < 1) || (nonRefundPercent > 100))) {
            return AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                    messageSource, "nonDigitsHundred"));
        }

        try {
            service.checkExistDrugstore(id.trim(), isNew);
            service.save(id, name, timeZone, nonRefundPercent,
                    authCode, disabled);
            return AjaxUtil.getSuccessResponse(0);
        } catch (DuplicateDrugstoreException dde) {
            return AjaxUtil.getFailureResponse(MessagesUtil.getMessage(
                    messageSource, "duplicateDrugstoreId"));
        } catch (Exception e) {
            return AjaxUtil.getFailureResponse(e.getLocalizedMessage());
        }
    }

    @RolesAllowed(User.ROLE_DRUGSTORE_EDIT)
    @RequestMapping(value = URL_STORE_EDIT, method = RequestMethod.GET)
    public ModelAndView storeEdit(
            @RequestParam(value = "storeid", required = false) String storeId,
            ModelMap model, HttpServletRequest request) {
        Drugstore store = null;
        ModelAndView mav = new ModelAndView("drugstoreEdit");
        if (!StringUtils.isBlank(storeId))
            store = service.findDrugstoreById(storeId);
        if (store == null)
            mav.addObject("isNew", Boolean.TRUE)
                    .addObject("appSettings", appSettingsService)
                    .addObject("timeZoneFormatter",
                            new TimeZoneFormatter(appSettingsService,
                                    localeResolver.resolveLocale(request)));
        else
            mav.addObject("isNew", Boolean.FALSE)
                    .addObject("appSettings", appSettingsService)
                    .addObject("timeZoneFormatter",
                            new TimeZoneFormatter(appSettingsService,
                                    localeResolver.resolveLocale(request)))
                    .addObject("store", store);
        return mav;
    }

    @RolesAllowed({User.ROLE_DRUGSTORE_VIEW, User.ROLE_DRUGSTORE_EDIT})
    @RequestMapping(value = URL_STORE_DETAILS, method = RequestMethod.GET)
    public ModelAndView storeDetails(ModelMap model,
            @RequestParam(value = "term", required = false) String term,
            @RequestParam(value = "storeid", required = false) String storeId,
            @RequestParam(value = "period", required = false) Integer period) {

        int reportPeriod;
        if (period == null) {
            reportPeriod = 1;
        } else {
            reportPeriod = period;
        }

        if (!StringUtils.isBlank(storeId)) {
            try {

                Drugstore store = service.findDrugstoreById(storeId);

                Boolean noTransactionFound = true;
                List<ProcessingTransaction> transactions = service
                        .getProcessingTransactions(storeId, reportPeriod);
                if (!transactions.isEmpty())
                    noTransactionFound = false;

                return new ModelAndView("storeDetails").addObject("transactions", transactions)
                        .addObject("reportperiod", reportPeriod)
                        .addObject("store", store)
                        .addObject("messageFormatter", new VelocityMessageFormatter(messageSource))
                        .addObject("dateFormatter", dateFormatter)
                        .addObject("timeFormatter", timeFormatter)
                        .addObject("noTransactionFound", noTransactionFound);

            } catch (JsonParseException e) {
                e.printStackTrace();
            } catch (JsonMappingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ProvisionedThroughputExceededException e) {

                e.printStackTrace();
                return new ModelAndView("storeDetails").addObject(
                        "reportperiod", reportPeriod).addObject("store", "")
                        .addObject(
                                "errorMessage",
                                MessagesUtil.getMessage(messageSource,
                                        "throughputLimitExceeded"));
            }
        }
        return new ModelAndView("storeDetails");
    }

    @RolesAllowed(User.ROLE_DRUGSTORE_REPORTS)
    @RequestMapping(value = "/store/report", method = RequestMethod.GET)
    public ModelAndView storeReport(ModelMap model) {
        return new ModelAndView("storeReport");
    }
}
