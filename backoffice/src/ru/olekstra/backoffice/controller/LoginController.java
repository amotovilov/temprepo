package ru.olekstra.backoffice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {

    public static final String URL_LOGIN = "/login";
    public static final String VIEW_LOGIN = "login";

    public static final String ERROR = "error";
    public static final String MESSAGE = "message";

    @RequestMapping(value = URL_LOGIN, method = RequestMethod.GET)
    public ModelAndView displayLoginPage(
            @RequestParam(value = ERROR, required = false) Boolean error,
            @RequestParam(value = MESSAGE, required = false) String message) {

        ModelAndView mav = new ModelAndView(VIEW_LOGIN);
        if (error != null) {
            mav.addObject(ERROR, error);
            mav.addObject(MESSAGE, message);
        }
        return mav;
    }
}
