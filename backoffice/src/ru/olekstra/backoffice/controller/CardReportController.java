package ru.olekstra.backoffice.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.backoffice.util.CsvStreamWriter;
import ru.olekstra.common.util.MessagesUtil;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.User;
import ru.olekstra.service.CardBatchService;
import ru.olekstra.service.CardService;

@Controller
public class CardReportController {

    public static final String URL_CARDS_ROOT = "/cards/";
    public static final String URL_CARDS_REPORT = URL_CARDS_ROOT + "report/";

    private CardService cardService;
    private CardBatchService batchService;
    private DynamodbService dynamoService;
    private DateTimeFormatter reportDateTimeFormatter;
    private MessageSource messageSource;

    public CardReportController() {
    }

    @Autowired
    public CardReportController(CardService cardService,
            CardBatchService batchService, DynamodbService dynamoService,
            DateTimeFormatter reportDateTimeFormatter,
            MessageSource messageSource) {
        this.cardService = cardService;
        this.batchService = batchService;
        this.dynamoService = dynamoService;
        this.reportDateTimeFormatter = reportDateTimeFormatter;
        this.messageSource = messageSource;
    }

    @RolesAllowed(User.ROLE_CARD_VIEW)
    @RequestMapping(value = URL_CARDS_REPORT, method = RequestMethod.POST)
    public void getCardReport(@RequestParam("batchnames") String[] batchNames,
            HttpServletResponse response) throws IOException, RuntimeException,
            InterruptedException {

        List<String> cardNumList = new LinkedList<String>();
        for (int i = 0; i < batchNames.length; i++) {
            List<String> batchNamesList = batchService
                    .getBatchCardList(batchNames[i]);
            if (batchNamesList != null)
                cardNumList.addAll(batchNamesList);
        }

        DateTimeFormatter fmt = DateTimeFormat.forPattern("dd-MM-yyyy_hh:mm");
        response.setContentType("text/csv");
        response.setHeader("Content-Disposition",
                "attachment; filename=\"cardsReport"
                        + fmt.print(DateTime.now()) + ".csv\"");
        CsvStreamWriter csvWriter = new CsvStreamWriter(response
                .getOutputStream(), reportDateTimeFormatter);
        // Припишем заголовки полей

        csvWriter.appendField(MessagesUtil.getMessageWithOutPrefix(
                messageSource, "report.title.cardNumber"));
        csvWriter.appendField(MessagesUtil.getMessageWithOutPrefix(
                messageSource, "report.title.cardHolder"));
        csvWriter.appendField(MessagesUtil.getMessageWithOutPrefix(
                messageSource, "report.title.phoneNumber"));
        csvWriter.appendField(MessagesUtil.getMessageWithOutPrefix(
                messageSource, "report.title.limit"));
        csvWriter.appendField(MessagesUtil.getMessageWithOutPrefix(
                messageSource, "report.title.discountPlan"));
        csvWriter.appendField(MessagesUtil.getMessageWithOutPrefix(
                messageSource, "report.title.startDate"));
        csvWriter.appendField(MessagesUtil.getMessageWithOutPrefix(
                messageSource, "report.title.endDate"));
        csvWriter.appenNewLine();

        List<String> tmpCardNumList = new ArrayList<String>();
        for (int i = 0; i < cardNumList.size(); i++) {

            tmpCardNumList.add(cardNumList.get(i));

            if (tmpCardNumList.size() == dynamoService.getMaxBatchSize()
                    || i == cardNumList.size() - 1) {
                List<Card> cardList = cardService.getCards(tmpCardNumList);
                tmpCardNumList.clear();

                for (Card card : cardList) {
                    BigDecimal limit = card.getLimitRemain() == null ? BigDecimal.ZERO
                            : card.getLimitRemain();
                    csvWriter.appendField(card.getNumber());
                    csvWriter.appendField(card.getHolderName());
                    csvWriter.appendField(card.getTelephoneNumber());
                    csvWriter.appendField(limit);
                    csvWriter.appendField(card.getDiscountPlanId());
                    csvWriter.appendField(card.getStartDate());
                    csvWriter.appendField(card.getEndDate());
                    csvWriter.appenNewLine();
                }
                response.flushBuffer();
            }
        }
        response.flushBuffer();
    }
}
