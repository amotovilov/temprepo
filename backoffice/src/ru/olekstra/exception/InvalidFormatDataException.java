package ru.olekstra.exception;

public class InvalidFormatDataException extends Exception {

    public InvalidFormatDataException(String msg) {
        super(msg);
    }

}
