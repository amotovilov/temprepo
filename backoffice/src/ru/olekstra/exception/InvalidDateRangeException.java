package ru.olekstra.exception;

public class InvalidDateRangeException extends Exception {

    public InvalidDateRangeException(String msg) {
        super(msg);
    }

}
