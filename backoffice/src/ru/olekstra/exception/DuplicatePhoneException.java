package ru.olekstra.exception;

public class DuplicatePhoneException extends Exception {

    public DuplicatePhoneException(String msg) {
        super(msg);
    }
}
