package ru.olekstra.exception;

public class DuplicateDrugstoreException extends Exception {

    public DuplicateDrugstoreException(String msg) {
        super(msg);
    }

}
