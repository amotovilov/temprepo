package ru.olekstra.exception;

public final class MessageCodeConstants {

    public static final String USER_NOT_FOUND_CODE = "userNotFound";
    public static final String USER_ROLE_NOT_FOUND_CODE = "userRoleNotFound";
    public static final String EMAIL_NOT_FOUND_CODE = "emailNotFound";

    private MessageCodeConstants() {
    }

}
