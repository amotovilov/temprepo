package ru.olekstra.exception;

public class HttpBadRequestException extends Exception {

    public HttpBadRequestException(String message) {
        super(message);
    }

}
