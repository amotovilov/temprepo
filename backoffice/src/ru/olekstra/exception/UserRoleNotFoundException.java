package ru.olekstra.exception;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class UserRoleNotFoundException extends UsernameNotFoundException {

    public UserRoleNotFoundException(String msg) {
        super(msg);
    }
}
