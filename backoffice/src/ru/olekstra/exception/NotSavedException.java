package ru.olekstra.exception;

public class NotSavedException extends Exception {

    public NotSavedException(String msg) {
        super(msg);
    }
}