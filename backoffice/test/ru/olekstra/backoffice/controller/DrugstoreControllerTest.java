package ru.olekstra.backoffice.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;

import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.domain.Drugstore;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.dto.DrugstoreResponse;
import ru.olekstra.exception.NotSavedException;
import ru.olekstra.service.DrugstoreService;

import com.amazonaws.services.dynamodb.model.ProvisionedThroughputExceededException;

@RunWith(MockitoJUnitRunner.class)
public class DrugstoreControllerTest {

    private DrugstoreController controller;
    private DrugstoreService service;
    private AppSettingsService appSettingsService;
    private MessageSource messageSource;
    private DateTimeFormatter dateFormatter;
    private LocaleResolver localeResolver;

    private Boolean enabledDrugstore;

    @Before
    public void setUp() throws Exception {
        service = mock(DrugstoreService.class);
        appSettingsService = mock(AppSettingsService.class);
        messageSource = mock(MessageSource.class);
        dateFormatter = mock(DateTimeFormatter.class);
        localeResolver = mock(LocaleResolver.class);
        controller = new DrugstoreController(service, appSettingsService,
                messageSource, dateFormatter, localeResolver);
        enabledDrugstore = Boolean.TRUE;
    }

    @Test
    public void testDrugstoreController() {

        assertTrue(controller != null);
        assertTrue(DrugstoreController.class.isInstance(controller));
    }

    @Test
    public void testCreate() throws NotSavedException {

        String id = "testID";
        String name = "testName";
        String timeZone = "timeZone";
        Integer nonRefundPercent = 5;
        boolean isNew = true;
        String authCode = "100";

        doNothing().when(service).save(id, name, timeZone,
                nonRefundPercent, authCode, enabledDrugstore);
        controller.save(id, name, timeZone, nonRefundPercent, isNew,
                authCode, enabledDrugstore);

        doNothing().when(service).save(id, name, timeZone,
                nonRefundPercent, authCode, enabledDrugstore);
        controller.save(id, name, timeZone, nonRefundPercent, isNew,
                authCode, enabledDrugstore);

        verify(service, times(2)).save(id, name, timeZone,
                nonRefundPercent, authCode, enabledDrugstore);
    }

    @Test
    public void testAutocomplete() throws IOException {

        String term = "autoCompliteTerm";
        List<Drugstore> drugstores = new ArrayList<Drugstore>();
        Drugstore drugstore = new Drugstore("testId");
        drugstore.setName("testName");
        drugstore.setAuthCode("testLocation");
        drugstore.setTimeZone("timeZone");
        drugstore.setDisabled(enabledDrugstore);
        drugstores.add(drugstore);

        List<DrugstoreResponse> responseDrugstores = new LinkedList<DrugstoreResponse>();
        for (Drugstore item : drugstores) {
            DrugstoreResponse dr = new DrugstoreResponse();
            dr.setId(item.getId());
            dr.setName(item.getName());
            dr.setTimeZone(item.getTimeZone());
            dr.setDisabled(item.getDisabled());

            responseDrugstores.add(dr);
        }

        when(service.findDrugstores(term)).thenReturn(drugstores);
        when(service.getResponseListFromDrugstoreList(drugstores)).thenReturn(
                responseDrugstores);

        List<DrugstoreResponse> result = controller.autocomplete(term);
        assertEquals(responseDrugstores.size(), result.size());

        verify(service).findDrugstores(term);
        verify(service).getResponseListFromDrugstoreList(drugstores);
    }

    @Test
    public void testStoreEdit() {
        String drugstoreId = "drugstoreId";
        Drugstore drugstore = new Drugstore(drugstoreId);

        ModelMap model = mock(ModelMap.class);
        HttpServletRequest request = mock(HttpServletRequest.class);

        // edit drugstore
        when(service.findDrugstoreById(eq(drugstoreId))).thenReturn(drugstore);
        when(localeResolver.resolveLocale(eq(request))).thenReturn(
                Locale.getDefault());

        ModelAndView result = controller.storeEdit(drugstoreId, model, request);

        verify(service).findDrugstoreById(eq(drugstoreId));
        verify(localeResolver).resolveLocale(eq(request));

        assertNotNull(result);
        assertNotNull(result.getModelMap().get("timeZoneFormatter"));
        assertFalse((Boolean) result.getModelMap().get("isNew"));
        assertEquals(appSettingsService, result.getModelMap()
                .get("appSettings"));
        assertEquals(drugstore, result.getModelMap().get("store"));

        // add new drugstore
        result = controller.storeEdit(null, model, request);

        verify(localeResolver, times(2)).resolveLocale(eq(request));

        assertNotNull(result);
        assertNotNull(result.getModelMap().get("timeZoneFormatter"));
        assertTrue((Boolean) result.getModelMap().get("isNew"));
        assertEquals(appSettingsService, result.getModelMap()
                .get("appSettings"));
        assertNull(result.getModelMap().get("store"));
    }

    @Test
    public void testStoreDetails()
            throws ProvisionedThroughputExceededException, JsonParseException,
            JsonMappingException, IOException {
        ModelMap model = mock(ModelMap.class);

        // no parameters
        ModelAndView result = controller.storeDetails(model, null, null, null);

        assertEquals("storeDetails", result.getViewName());

        // only period is specified
        result = controller.storeDetails(model, null, null, 1);

        assertEquals("storeDetails", result.getViewName());

        result = controller.storeDetails(model, null, null, 1);

        // period and drugstore id are specified
        String drugstoreId = "test drugsotore";
        Drugstore drugstore = new Drugstore(drugstoreId);
        int period = 1;
        List<ProcessingTransaction> transactions = Arrays.asList(
                new ProcessingTransaction(), new ProcessingTransaction());

        when(service.findDrugstoreById(eq(drugstoreId))).thenReturn(drugstore);

        when(service.getProcessingTransactions(eq(drugstoreId), eq(period)))
                .thenReturn(transactions);

        result = controller.storeDetails(model, null, drugstoreId, period);

        verify(service).findDrugstoreById(eq(drugstoreId));
        verify(service).getProcessingTransactions(eq(drugstoreId), eq(period));

        assertEquals("storeDetails", result.getViewName());
        assertEquals(transactions, result.getModelMap().get("transactions"));
        assertEquals(period, result.getModelMap().get("reportperiod"));
        assertNotNull(result.getModelMap().get("dateFormatter"));
        assertNotNull(result.getModelMap().get("timeFormatter"));
        assertFalse((Boolean) result.getModelMap().get("noTransactionFound"));
    }
}
