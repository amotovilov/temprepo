package ru.olekstra.backoffice.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import org.apache.velocity.app.VelocityEngine;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.awsutils.exception.ReporNotFoundException;
import ru.olekstra.awsutils.exception.S3ConnectionException;
import ru.olekstra.backoffice.util.AjaxUtil;
import ru.olekstra.common.service.DateTimeService;
import ru.olekstra.common.util.MessagesUtil;
import ru.olekstra.domain.DiscountPlan;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.Recipe;
import ru.olekstra.domain.RecipeState;
import ru.olekstra.domain.dto.VoucherResponseCard;
import ru.olekstra.domain.dto.VoucherResponseRow;
import ru.olekstra.exception.NotSavedException;
import ru.olekstra.service.DrugstoreService;
import ru.olekstra.service.RecipeService;
import ru.olekstra.service.SecurityService;

@RunWith(MockitoJUnitRunner.class)
public class RecipeControllerTest {

    private RecipeController controller;
    private RecipeService service;
    private DrugstoreService drugstoreService;
    private MessageSource messageSource;
    private SecurityService securityService;
    private VelocityEngine velocityEngineApp;
    private DateTimeService dateTimeService;

    @Before
    public void setup() {
        service = mock(RecipeService.class);
        drugstoreService = mock(DrugstoreService.class);
        messageSource = mock(MessageSource.class);
        securityService = mock(SecurityService.class);
        velocityEngineApp = mock(VelocityEngine.class);
        dateTimeService = mock(DateTimeService.class);

        controller = new RecipeController(service, drugstoreService,
                messageSource, securityService, velocityEngineApp, dateTimeService);
    }

    @Test
    public void recipeRecieveCheckFileFormat() throws IOException {

        String recipePeriod = "02.2013";
        String drugstoreId = "test drugstore";
        String fileName = "notsuitableimage.gif";
        MultipartFile file = new MockMultipartFile(fileName, new byte[] {
                (byte) 0xFF, (byte) 0xD8, (byte) 0xFF, (byte) 0xE0});
        // when(file.getOriginalFilename()).thenReturn(fileName);

        String incorrectFileFormatMessage = "incorrect file format message";

        when(service.getImageExtension(anyString())).thenReturn("gif");

        when(service.isValidImageFormat(any(InputStream.class), anyString()))
                .thenReturn(false);
        when(
                messageSource.getMessage(
                        eq("msg.recipe.filePutToQueueFailure"),
                        eq((Object[]) null), eq((Locale) null))).thenReturn(
                incorrectFileFormatMessage);

        Map<String, Object> result = controller.recipeRecieve(drugstoreId,
                recipePeriod, file);

        // verify(service).isValidImageFormat(any(InputStream.class));
        verify(messageSource).getMessage(
                eq("msg.recipe.filePutToQueueFailure"), eq((Object[]) null),
                eq((Locale) null));

        assertFalse((Boolean) result.get(AjaxUtil.JSON_SUCCESS));
        assertEquals(incorrectFileFormatMessage, (String) result
                .get(AjaxUtil.JSON_MESSAGE));
    }

    @Test
    public void testRecipeRecieve() throws IOException,
            ItemSizeLimitExceededException {
        String recipePeriod = "02.2013";
        String drugstoreId = "test drugstore";
        String originalFileName = "test.jpg";
        byte[] fileContnet = "recipe image bytes".getBytes();

        String fileExtension = "jpg";
        String period = "201302";
        String uuid = UUID.randomUUID().toString();
        String s3FileName = uuid + "." + fileExtension;
        String s3FilePath = period + "/" + drugstoreId + "/" + s3FileName;
        String filePutToQueueMessage = "recipe is put to queue message";
        String currentUser = "testUser";
        String newRecipeNote = "New recipe note";

        MultipartFile file = mock(MultipartFile.class);
        when(file.getOriginalFilename()).thenReturn(originalFileName);
        when(file.getBytes()).thenReturn(fileContnet);

        when(service.isValidImageFormat(any(InputStream.class), anyString()))
                .thenReturn(true);
        when(service.getImageExtension(eq(originalFileName))).thenReturn(
                fileExtension);
        when(service.convertRecipePeriod(eq(recipePeriod))).thenReturn(period);
        when(service.genarateImageName(any(String.class), eq(fileExtension)))
                .thenReturn(s3FileName);
        when(
                service.getRecipeFilePath(eq(period), eq(drugstoreId),
                        eq(s3FileName))).thenReturn(s3FilePath);
        when(securityService.getCurrentUser()).thenReturn(currentUser);
        when(
                messageSource.getMessage(
                        eq("msg.recipe.filePutToQueueSuccess"),
                        eq((Object[]) null), eq((Locale) null))).thenReturn(
                filePutToQueueMessage);
        when(service.saveRecipe(any(Recipe.class))).thenReturn(true);
        when(
                messageSource.getMessage(eq("msg.recipe.state.new"),
                        eq((Object[]) new String[] {currentUser}),
                        eq((Locale) null))).thenReturn(newRecipeNote);

        Map<String, Object> result = controller.recipeRecieve(drugstoreId,
                recipePeriod, file);

        // verify(service).isValidImageFormat(eq(originalFileName));
        verify(service).getImageExtension(eq(originalFileName));
        verify(service).convertRecipePeriod(eq(recipePeriod));
        verify(service).genarateImageName(any(String.class), eq(fileExtension));
        verify(service).getRecipeFilePath(eq(period), eq(drugstoreId),
                eq(s3FileName));
        verify(securityService).getCurrentUser();
        verify(messageSource).getMessage(
                eq("msg.recipe.filePutToQueueSuccess"), eq((Object[]) null),
                eq((Locale) null));
        verify(service).saveRecipe(any(Recipe.class));
        verify(messageSource).getMessage(eq("msg.recipe.state.new"),
                eq((Object[]) new String[] {currentUser}), eq((Locale) null));

        assertTrue((Boolean) result.get(AjaxUtil.JSON_SUCCESS));
        assertEquals(filePutToQueueMessage, (String) result
                .get(AjaxUtil.JSON_DATA));
    }

    @Test
    public void testCheckRecipe() throws NotSavedException,
            JsonGenerationException, JsonMappingException, IOException,
            S3ConnectionException, ReporNotFoundException {

        String period = "201302";
        String uuid = UUID.randomUUID().toString();
        String drugstoreId = "drugstoreId";
        String authCode = "1000";
        String discountPlanId = "planId";
        DiscountPlan discountPlan = new DiscountPlan();
        discountPlan.setId(discountPlanId);

        RecipeState recipeState = new RecipeState(period, Recipe.STATE_PARSED,
                uuid, drugstoreId, authCode);
        recipeState.setFileType("jpg");
        List<VoucherResponseRow> rows = new ArrayList<VoucherResponseRow>();
        rows.add(new VoucherResponseRow());
        @SuppressWarnings("deprecation")
        ProcessingTransaction transaction = new ProcessingTransaction(UUID
                .randomUUID(), "drugstoreId", new VoucherResponseCard(),
                new DiscountPlan(), new BigDecimal(0), new BigDecimal(0),
                new BigDecimal(0), new String[0], rows, DateTime.now().withZone(DateTimeZone.UTC));

        when(
                service.getNextRecipeWithStateX(Recipe.STATE_PARSED,
                        securityService.getCurrentUser())).thenReturn(
                recipeState);

        when(
                service.getTransaction(recipeState.getPeriod(), recipeState
                        .getDrugstoreId(), recipeState.getAuthCode()))
                .thenReturn(transaction);

        // first time look at picture
        when(
                MessagesUtil.getMessageWithOutPrefix(messageSource,
                        "msg.recipe.statusNotUpdated")).thenReturn("test");

        ModelAndView mav = controller.checkRecipe("");

        assertNotNull(mav);
        assertEquals("recipeCheck", mav.getViewName());
        assertEquals(authCode, (String) mav.getModel().get("authCode"));
        assertEquals(uuid, (String) mav.getModel().get("uuid"));
        assertEquals(period, (String) mav.getModel().get("recipePeriod"));
        assertEquals(RecipeController.URL_RECIPE_S3_BUCKET
                + recipeState.getPeriod() + "/" + recipeState.getDrugstoreId()
                + "/" + recipeState.getUUID() + ".jpg", (String) mav.getModel()
                .get("imageURL"));
        assertNotNull(mav.getModel().get("transactionRows"));
        assertNotNull(mav.getModel().get("cardOwner"));
        assertNull(mav.getModel().get("error"));
        assertNull(mav.getModel().get("message"));

        // second time look at picture with no errors
        when(
                MessagesUtil.getMessageWithOutPrefix(messageSource,
                        "msg.recipe.statusUpdated")).thenReturn("test");

        mav = controller.checkRecipe("false");

        assertNotNull(mav);
        assertEquals("recipeCheck", mav.getViewName());
        assertEquals(authCode, (String) mav.getModel().get("authCode"));
        assertEquals(uuid, (String) mav.getModel().get("uuid"));
        assertEquals(period, (String) mav.getModel().get("recipePeriod"));
        assertEquals(RecipeController.URL_RECIPE_S3_BUCKET
                + recipeState.getPeriod() + "/" + recipeState.getDrugstoreId()
                + "/" + recipeState.getUUID() + ".jpg", (String) mav.getModel()
                .get("imageURL"));
        assertNotNull(mav.getModel().get("transactionRows"));
        assertNotNull(mav.getModel().get("cardOwner"));
        assertNotNull(mav.getModel().get("message"));
        assertNull(mav.getModel().get("error"));

        // third time look at picture with errors
        when(
                MessagesUtil.getMessageWithOutPrefix(messageSource,
                        "msg.recipe.statusNotUpdated")).thenReturn("test");

        mav = controller.checkRecipe("true");

        assertNotNull(mav);
        assertEquals("recipeCheck", mav.getViewName());
        assertEquals(authCode, (String) mav.getModel().get("authCode"));
        assertEquals(uuid, (String) mav.getModel().get("uuid"));
        assertEquals(period, (String) mav.getModel().get("recipePeriod"));
        assertEquals(RecipeController.URL_RECIPE_S3_BUCKET
                + recipeState.getPeriod() + "/" + recipeState.getDrugstoreId()
                + "/" + recipeState.getUUID() + ".jpg", (String) mav.getModel()
                .get("imageURL"));
        assertNotNull(mav.getModel().get("transactionRows"));
        assertNotNull(mav.getModel().get("cardOwner"));
        assertNotNull(mav.getModel().get("error"));
        assertNull(mav.getModel().get("message"));
    }

    @Test
    public void testCheckRecipeWithEmptyView() throws NotSavedException,
            JsonGenerationException, JsonMappingException, IOException,
            S3ConnectionException, ReporNotFoundException {

        when(
                service.getNextRecipeWithStateX(Recipe.STATE_PARSED,
                        securityService.getCurrentUser())).thenReturn(null);

        ModelAndView mav = controller.checkRecipe("");

        assertNotNull(mav);
        assertEquals("recipeNothing", mav.getViewName());
    }

    @Test
    public void testRecipeProblem() throws NotSavedException, IOException {

        String period = "201302";
        String uuid = UUID.randomUUID().toString();
        String drugstoreId = "test drugstore";
        String authCode = "1101";
        String currentUser = "testUser";

        RecipeState recipeState = new RecipeState(period, Recipe.STATE_PROBLEM,
                uuid, drugstoreId, authCode);

        when(securityService.getCurrentUser()).thenReturn(currentUser);
        when(
                service.getNextRecipeWithStateX(eq(Recipe.STATE_PROBLEM),
                        eq(currentUser))).thenReturn(recipeState);

        ModelAndView mav = controller.recipeProblem(null, null, null, null);

        verify(securityService).getCurrentUser();
        verify(service).getNextRecipeWithStateX(eq(Recipe.STATE_PROBLEM),
                eq(currentUser));

        assertEquals("recipeProblem", mav.getViewName());
        assertEquals(period, mav.getModel().get("period"));
        assertEquals(uuid, mav.getModel().get("uuid"));
        assertEquals(authCode, mav.getModel().get("authCode"));
        assertNotNull(mav.getModel().get("imageURL"));

        assertNull(mav.getModel().get("result"));
        assertNull(mav.getModel().get("message"));

        // parameters check
        Boolean result = true;
        String message = "test save is ok";

        mav = controller.recipeProblem(result, message, null, null);

        verify(securityService, times(2)).getCurrentUser();
        verify(service, times(2)).getNextRecipeWithStateX(
                eq(Recipe.STATE_PROBLEM), eq(currentUser));

        assertEquals(result, mav.getModel().get("result"));
        assertEquals(message, mav.getModel().get("message"));
    }

    @Test
    public void testRecipeProblemAccept() throws NotSavedException {

        String period = "201302";
        String uuid = UUID.randomUUID().toString();
        String authCode = "1101";
        boolean cardOwnerCorrect = true;
        boolean transactionCorrect = true;
        String currentUser = "testUser";
        String drugstoreId = "test drugstore";
        String fileName = "example.jpg";
        String fileType = "jpg";

        String messageRecipeAccepted = "Recipe is accepted";
        String messageRecipeStateAccepted = "Recipe state is changed";

        Recipe recipe = new Recipe(period, uuid, Recipe.STATE_PROBLEM,
                drugstoreId, fileName, fileType, new DateTime());

        when(securityService.getCurrentUser()).thenReturn(currentUser);
        when(service.getRecipe(eq(period), eq(uuid))).thenReturn(recipe);
        when(
                messageSource.getMessage(eq("msg.recipe.state.accepted"),
                        eq((Object[]) new String[] {currentUser}),
                        eq((Locale) null))).thenReturn(
                messageRecipeStateAccepted);
        when(
                messageSource.getMessage(eq("msg.recipe.accepted"),
                        eq((Object[]) null), eq((Locale) null))).thenReturn(
                messageRecipeAccepted);

        ModelAndView mav = controller.recipeProblemAccept(period, uuid,
                authCode, cardOwnerCorrect, transactionCorrect);

        verify(securityService).getCurrentUser();
        verify(service).getRecipe(eq(period), eq(uuid));
        verify(messageSource).getMessage(eq("msg.recipe.state.accepted"),
                eq((Object[]) new String[] {currentUser}), eq((Locale) null));
        verify(messageSource).getMessage(eq("msg.recipe.accepted"),
                eq((Object[]) null), eq((Locale) null));

        assertTrue((Boolean) mav.getModel().get("result"));
        assertEquals(messageRecipeAccepted, mav.getModel().get("message"));
    }

    @Test
    public void testRecipeProblemAcceptConditionCheck()
            throws NotSavedException {

        String period = "201302";
        String uuid = UUID.randomUUID().toString();

        String messageRecipeAccepted = "Recipe is accepted";
        String messageRecipeNotAccepted = "Recipe is not accepted";

        when(
                messageSource.getMessage(eq("msg.recipe.accepted"),
                        eq((Object[]) null), eq((Locale) null))).thenReturn(
                messageRecipeAccepted);

        when(
                messageSource.getMessage(eq("msg.recipe.acceptedNotSaved"),
                        eq((Object[]) null), eq((Locale) null))).thenReturn(
                messageRecipeNotAccepted);

        ModelAndView mav = controller.recipeProblemAccept(period, uuid, "1101",
                true, true);
        assertTrue((Boolean) mav.getModel().get("result"));
        assertEquals(messageRecipeAccepted, mav.getModel().get("message"));

        mav = controller.recipeProblemAccept(period, uuid, "11", true, true);
        assertFalse((Boolean) mav.getModel().get("result"));
        assertEquals(messageRecipeNotAccepted, mav.getModel().get("message"));

        mav = controller.recipeProblemAccept(period, uuid, "110b", true, true);
        assertFalse((Boolean) mav.getModel().get("result"));
        assertEquals(messageRecipeNotAccepted, mav.getModel().get("message"));

        mav = controller.recipeProblemAccept(period, uuid, "1101", false, true);
        assertFalse((Boolean) mav.getModel().get("result"));
        assertEquals(messageRecipeNotAccepted, mav.getModel().get("message"));

        mav = controller.recipeProblemAccept(period, uuid, "1101", true, false);
        assertFalse((Boolean) mav.getModel().get("result"));
        assertEquals(messageRecipeNotAccepted, mav.getModel().get("message"));
    }

    @Test
    public void testRecipeProblemReject() throws NotSavedException {

        String period = "201302";
        String uuid = UUID.randomUUID().toString();
        String currentUser = "testUser";
        String drugstoreId = "test drugstore";
        String fileName = "example.jpg";
        String fileType = "jpg";

        String rejectReason = "Simply rejected";
        String messageRecipeRejected = "Recipe is accepted";
        String messageRecipeStateRejected = "Recipe state is changed";

        Recipe recipe = new Recipe(period, uuid, Recipe.STATE_PROBLEM,
                drugstoreId, fileName, fileType, new DateTime());

        when(securityService.getCurrentUser()).thenReturn(currentUser);
        when(service.getRecipe(eq(period), eq(uuid))).thenReturn(recipe);
        when(
                messageSource
                        .getMessage(eq("msg.recipe.state.rejected"),
                                eq((Object[]) new String[] {currentUser,
                                        rejectReason}), eq((Locale) null)))
                .thenReturn(messageRecipeStateRejected);
        when(
                messageSource.getMessage(eq("msg.recipe.rejected"),
                        eq((Object[]) null), eq((Locale) null))).thenReturn(
                messageRecipeRejected);

        ModelAndView mav = controller.recipeProblemReject(period, uuid,
                rejectReason);

        verify(securityService).getCurrentUser();
        verify(service).getRecipe(eq(period), eq(uuid));
        verify(messageSource).getMessage(eq("msg.recipe.state.rejected"),
                eq((Object[]) new String[] {currentUser, rejectReason}),
                eq((Locale) null));
        verify(messageSource).getMessage(eq("msg.recipe.rejected"),
                eq((Object[]) null), eq((Locale) null));

        assertTrue((Boolean) mav.getModel().get("result"));
        assertEquals(messageRecipeRejected, mav.getModel().get("message"));
    }

    @Test
    public void testRecipeProblemTransaction() throws JsonGenerationException,
            JsonMappingException, IOException {

        String period = "201302";
        String uuid = UUID.randomUUID().toString();
        String authCode = "1101";
        String drugstoreId = "test drugstore";
        String fileName = "example.jpg";
        String fileType = "jpg";

        String cardOwner = "test card owner";

        String labelCardOwner = "Card owner header";
        String labelTransaction = "Transaction header";

        Recipe recipe = new Recipe(period, uuid, Recipe.STATE_PROBLEM,
                drugstoreId, fileName, fileType, new DateTime());

        ProcessingTransaction transaction = new ProcessingTransaction();
        transaction.setCard(new VoucherResponseCard("777", cardOwner,
                "0123456789", true, "comment", "planId", "planName"));

        VoucherResponseRow row = new VoucherResponseRow();
        row.setQuantity(1);
        row.setName("test");
        transaction.setRows(Arrays.asList(row));

        when(service.getRecipe(eq(period), eq(uuid))).thenReturn(recipe);
        when(service.getTransaction(eq(period), eq(drugstoreId), eq(authCode)))
                .thenReturn(transaction);
        when(
                messageSource.getMessage(eq("label.cardOwner"),
                        eq((Object[]) null), eq((Locale) null))).thenReturn(
                labelCardOwner);
        when(
                messageSource.getMessage(eq("label.transactionPositions"),
                        eq((Object[]) null), eq((Locale) null))).thenReturn(
                labelTransaction);

        Map<String, ?> result = controller.recipeProblemTransaction(period,
                uuid, authCode);

        verify(service).getRecipe(eq(period), eq(uuid));
        verify(service).getTransaction(eq(period), eq(drugstoreId),
                eq(authCode));
        verify(messageSource).getMessage(eq("label.cardOwner"),
                eq((Object[]) null), eq((Locale) null));
        verify(messageSource).getMessage(eq("label.transactionPositions"),
                eq((Object[]) null), eq((Locale) null));

        assertNotNull(result.get("parsedPositions"));
    }

    @Test
    public void testRecipeReport() {
        ModelAndView mav = controller.recipeReport();
        assertEquals("recipeReport", mav.getViewName());
    }

    @Test
    public void testGetRecipeReportForPeriod() throws S3ConnectionException,
            ReporNotFoundException, IOException {
        String period = "02.2013";
        String convertedperiod = "201302";
        String reportLinks = "test report links";

        when(service.convertRecipePeriod(eq(period))).thenReturn(
                convertedperiod);
        when(service.getHtmlReportList(eq(convertedperiod))).thenReturn(
                reportLinks);

        Map<String, ?> response = controller.getRecipeReportForPeriod(period);

        verify(service).convertRecipePeriod(eq(period));
        verify(service).getHtmlReportList(eq(convertedperiod));

        assertTrue((Boolean) response.get(AjaxUtil.JSON_SUCCESS));
        assertEquals(reportLinks, response.get(AjaxUtil.JSON_DATA));
    }

    @Test
    public void testRecipeReportGenerate() {
        String period = "02.2013";
        String convertedperiod = "201302";

        when(service.convertRecipePeriod(eq(period))).thenReturn(
                convertedperiod);

        Map<String, ?> response = controller.recipeReportGenerate(period);

        verify(service).convertRecipePeriod(eq(period));

        assertTrue((Boolean) response.get(AjaxUtil.JSON_SUCCESS));
        assertNull(response.get(AjaxUtil.JSON_DATA));
    }
}
