package ru.olekstra.backoffice.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.web.servlet.ModelAndView;

import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.azure.model.Message;
import ru.olekstra.azure.service.QueueSender;
import ru.olekstra.backoffice.util.AjaxUtil;
import ru.olekstra.common.service.XmlMessageService;
import ru.olekstra.domain.Discount;
import ru.olekstra.domain.dto.DiscountDataCleanMessage;
import ru.olekstra.service.DiscountEditService;

@RunWith(MockitoJUnitRunner.class)
public class DiscountControllerTest {

    private DiscountEditService service;
    private MessageSource messageSource;
    private XmlMessageService xmlMessageService;
    private DiscountController controller;
    private QueueSender queueSender;

    @Before
    public void setup() throws ParserConfigurationException, TransformerException {
        service = mock(DiscountEditService.class);
        messageSource = mock(MessageSource.class);
        xmlMessageService = mock(XmlMessageService.class);
        when(xmlMessageService.createDiscountDataCleanMessage(any(DiscountDataCleanMessage.class)))
                .thenReturn(new Message());
        queueSender = mock(QueueSender.class);
        controller = new DiscountController(service, messageSource, xmlMessageService, queueSender);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testViewAll() throws IOException {

        String discountId = "testId";
        Discount discount = new Discount();
        discount.setId(discountId);
        discount.setName(discountId);

        String anotherDiscountId = "another testId";
        Discount anotherDiscount = new Discount();
        anotherDiscount.setId(anotherDiscountId);
        anotherDiscount.setName(anotherDiscountId);

        List<Discount> discounts = Arrays.asList(discount, anotherDiscount);

        when(service.getAll()).thenReturn(discounts);

        ModelAndView mav = controller.viewAll();

        verify(service).getAll();

        assertEquals("discounts", mav.getViewName());
        assertEquals(discounts.size(), ((List<Discount>) mav.getModelMap().get(
                "discounts")).size());
        // Важно чтобы были упорядочены дисконты
        assertEquals(anotherDiscountId, ((List<Discount>) mav.getModelMap()
                .get("discounts")).get(0).getId());
        assertEquals(discountId, ((List<Discount>) mav.getModelMap().get(
                "discounts")).get(1).getId());

    }

    @Test
    public void testAdd() {
        ModelAndView mav = controller.add();
        assertEquals("discountsEdit", mav.getViewName());
    }

    @Test
    public void testEdit() throws JsonParseException, JsonMappingException,
            IOException {

        String id = "testId";
        String name = "name";
        boolean useCardLimit = true;
        boolean alwaysFullRefund = true;
        int promoPackEkt = 5;
        int version = 1;
        int packCount = 0;

        Discount discount = new Discount(id, name, useCardLimit,
                alwaysFullRefund, promoPackEkt, version, packCount);

        when(service.getDiscount(eq(id))).thenReturn(discount);

        ModelAndView mav = controller.edit(id);

        verify(service).getDiscount(eq(id));

        assertEquals("discountsEdit", mav.getViewName());
        assertEquals(id, mav.getModelMap().get("id"));
        assertEquals(name, mav.getModelMap().get("name"));
        assertEquals(promoPackEkt, mav.getModelMap().get("promoPercent"));
        assertEquals(useCardLimit, mav.getModelMap().get("useCardLimit"));
        assertEquals(alwaysFullRefund, mav.getModelMap()
                .get("alwaysFullRefund"));
        assertEquals(packCount, mav.getModelMap().get("packCount"));
        assertEquals(version, mav.getModelMap().get("version"));
    }

    @Test
    public void testDelete() throws JsonParseException, JsonMappingException,
            IOException {

        String discountId = "testId";
        String discountName = "testName";

        String planName1 = "planname1";
        String planName2 = "planname2";
        String planName3 = "planname3";
        List<String> relatedPlanNames = Arrays.asList(planName1, planName2,
                planName3);

        // discount is used in some plans
        String messageDiscountDeletingIsUsed = "discountDeletingIsUsed";

        when(service.getRelatedDiscountPlans(eq(discountId))).thenReturn(
                relatedPlanNames);
        when(
                messageSource.getMessage(eq("msg.discountDeletingIsUsed"),
                        eq(new Object[] {discountName,
                                StringUtils.join(relatedPlanNames, ",")}),
                        eq((Locale) null))).thenReturn(
                messageDiscountDeletingIsUsed);

        Map<String, Object> result = controller
                .delete(discountId, discountName);

        verify(service).getRelatedDiscountPlans(eq(discountId));
        verify(messageSource).getMessage(
                eq("msg.discountDeletingIsUsed"),
                eq(new Object[] {discountName,
                        StringUtils.join(relatedPlanNames, ",")}),
                eq((Locale) null));

        assertFalse((Boolean) result.get(AjaxUtil.JSON_SUCCESS));
        assertEquals(messageDiscountDeletingIsUsed, result
                .get(AjaxUtil.JSON_MESSAGE));

        // discount is unused
        String messageDiscountDeletingSuccess = "discountDeletingSuccess";

        when(service.getRelatedDiscountPlans(eq(discountId))).thenReturn(
                new ArrayList<String>());
        when(
                messageSource.getMessage(eq("msg.discountDeletingSuccess"),
                        eq(new Object[] {discountName}), eq((Locale) null)))
                .thenReturn(messageDiscountDeletingSuccess);

        result = controller.delete(discountId, discountName);

        verify(service).deleteDiscount(eq(discountId));
        verify(messageSource).getMessage(eq("msg.discountDeletingSuccess"),
                eq(new Object[] {discountName}), eq((Locale) null));

        assertTrue((Boolean) result.get(AjaxUtil.JSON_SUCCESS));
        assertEquals(messageDiscountDeletingSuccess, result
                .get(AjaxUtil.JSON_DATA));
    }

    @Test
    public void testSave() throws ItemSizeLimitExceededException {

        String id = "testId";
        String name = "testName";
        Integer promoPercent = null;
        Integer version = 1;
        Boolean newDiscount = true;
        Boolean useCardLimit = true;
        Boolean alwaysFullRefund = true;
        String packsCsv = "1234567890123\tSome Pack\n1234567890124\tSome Other Pack\n";
        String limitsCsv = "123\tabc\tdef\tghi\t1\t2\t1.5\t1.6\n";

        // discount with same id already exists
        String messageDiscountIdExists = "discountIdExists";

        when(service.isAlreadyExists(eq(id))).thenReturn(true);
        when(
                messageSource.getMessage(eq("msg.discountIdExists"),
                        eq((Object[]) null), eq((Locale) null))).thenReturn(
                messageDiscountIdExists);

        when(
                messageSource.getMessage(eq("msg.saved"), eq((Object[]) null),
                        eq((Locale) null))).thenReturn("");

        Map<String, Object> result = controller.save(id, name, version,
                newDiscount, promoPercent, useCardLimit, alwaysFullRefund,
                packsCsv, limitsCsv);

        verify(service).isAlreadyExists(eq(id));
        verify(messageSource).getMessage(eq("msg.discountIdExists"),
                eq((Object[]) null), eq((Locale) null));

        assertFalse((Boolean) result.get(AjaxUtil.JSON_SUCCESS));
        assertEquals(messageDiscountIdExists, result.get(AjaxUtil.JSON_MESSAGE));

        // discount percent incorrect
        String messagePromoPercentIncorrect = "promoPercentIncorrect";

        when(service.isAlreadyExists(eq(id))).thenReturn(false);
        when(
                messageSource.getMessage(eq("msg.promoPercentIncorrect"),
                        eq((Object[]) null), eq((Locale) null))).thenReturn(
                messagePromoPercentIncorrect);

        result = controller.save(id, name, version, newDiscount, -1,
                useCardLimit, alwaysFullRefund, packsCsv, limitsCsv);

        verify(service, times(2)).isAlreadyExists(eq(id));
        verify(messageSource).getMessage(eq("msg.promoPercentIncorrect"),
                eq((Object[]) null), eq((Locale) null));

        assertFalse((Boolean) result.get(AjaxUtil.JSON_SUCCESS));
        assertEquals(messagePromoPercentIncorrect, result
                .get(AjaxUtil.JSON_MESSAGE));

        result = controller.save(id, name, version, newDiscount, 101,
                useCardLimit, alwaysFullRefund, packsCsv, limitsCsv);

        verify(service, times(3)).isAlreadyExists(eq(id));
        verify(messageSource, times(2)).getMessage(
                eq("msg.promoPercentIncorrect"), eq((Object[]) null),
                eq((Locale) null));

        assertFalse((Boolean) result.get(AjaxUtil.JSON_SUCCESS));
        assertEquals(messagePromoPercentIncorrect, result
                .get(AjaxUtil.JSON_MESSAGE));

        // save discount with new id
        when(service.isAlreadyExists(eq(id))).thenReturn(false);
        when(
                service.saveDiscount(eq(id), eq(name), eq(promoPercent),
                        eq(version), any(Integer.class), eq(newDiscount),
                        eq(useCardLimit), eq(alwaysFullRefund))).thenReturn(
                true);

        result = controller.save(id, name, version, newDiscount, promoPercent,
                useCardLimit, alwaysFullRefund, packsCsv, limitsCsv);

        verify(service, times(4)).isAlreadyExists(eq(id));
        verify(service).saveDiscount(eq(id), eq(name), eq(promoPercent),
                eq(version), any(Integer.class), eq(newDiscount),
                eq(useCardLimit), eq(alwaysFullRefund));

        assertTrue((Boolean) result.get(AjaxUtil.JSON_SUCCESS));
        assertEquals("", result.get(AjaxUtil.JSON_DATA));
    }

    @Test
    public void testProcessCsvParsingResult() {

        String line1 = "line1";
        String line2 = "line2";
        String line3 = "line3";

        List<String> errorLines = Arrays.asList(line1, line2);
        List<String> duplicateLines = Arrays.asList(line3);

        String messageLinesWithError = "linesWithError";
        String messagePackLinesWithDuplicates = "packLinesWithDuplicates";

        when(
                messageSource.getMessage(eq("msg.linesWithError"),
                        eq(new Object[] {"\n",
                                StringUtils.join(errorLines, "\n")}),
                        eq((Locale) null))).thenReturn(messageLinesWithError);
        when(
                messageSource.getMessage(eq("msg.packLinesWithDuplicates"),
                        eq(new Object[] {"\n",
                                StringUtils.join(duplicateLines, "\n")}),
                        eq((Locale) null))).thenReturn(
                messagePackLinesWithDuplicates);

        String result = controller.processCsvParsingResult(errorLines,
                duplicateLines);

        verify(messageSource).getMessage(eq("msg.linesWithError"),
                eq(new Object[] {"\n", StringUtils.join(errorLines, "\n")}),
                eq((Locale) null));
        verify(messageSource)
                .getMessage(
                        eq("msg.packLinesWithDuplicates"),
                        eq(new Object[] {"\n",
                                StringUtils.join(duplicateLines, "\n")}),
                        eq((Locale) null));

        assertTrue(result.contains(messageLinesWithError));
        assertTrue(result.contains(messagePackLinesWithDuplicates));
    }
}
