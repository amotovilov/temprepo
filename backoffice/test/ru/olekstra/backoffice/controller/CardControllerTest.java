package ru.olekstra.backoffice.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.app.VelocityEngine;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.awsutils.exception.NotUpdatedException;
import ru.olekstra.awsutils.exception.OlekstraException;
import ru.olekstra.backoffice.dto.BalanceOperationType;
import ru.olekstra.backoffice.dto.BatchOperationType;
import ru.olekstra.backoffice.dto.CardsBatchActivationForm;
import ru.olekstra.backoffice.dto.CardsLoadResult;
import ru.olekstra.backoffice.response.CardResponse;
import ru.olekstra.backoffice.util.AjaxUtil;
import ru.olekstra.backoffice.util.DateTimeUtil;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.common.service.CardExportService;
import ru.olekstra.common.service.CardLogService;
import ru.olekstra.common.service.DiscountService;
import ru.olekstra.common.service.PlanService;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.CardLog;
import ru.olekstra.domain.dto.CardLogType;
import ru.olekstra.exception.DuplicatePhoneException;
import ru.olekstra.exception.InvalidDateRangeException;
import ru.olekstra.exception.NotFoundException;
import ru.olekstra.service.CardBatchService;
import ru.olekstra.service.CardOperationService;
import ru.olekstra.service.CardService;
import ru.olekstra.service.NotificationService;
import ru.olekstra.service.TicketService;

@RunWith(MockitoJUnitRunner.class)
public class CardControllerTest {

    private CardService service;
    private PlanService planService;
    private TicketService ticketService;
    private CardBatchService batchService;
    private CardController controller;
    private NotificationService notificationService;
    private CardOperationService cardOperationService;
    private MessageSource messageSource;
    private VelocityEngine velocityEngineApp;
    private CardLogService cardLogService;
    private Principal principal;
    private DiscountService discountService;
    private CardExportService exportService;
    private AppSettingsService appSettingService;

    @Before
    public void setup() {
        service = mock(CardService.class);
        planService = mock(PlanService.class);
        ticketService = mock(TicketService.class);
        notificationService = mock(NotificationService.class);
        cardOperationService = mock(CardOperationService.class);
        messageSource = mock(MessageSource.class);
        batchService = mock(CardBatchService.class);
        velocityEngineApp = mock(VelocityEngine.class);
        cardLogService = mock(CardLogService.class);
        principal = mock(Principal.class);
        discountService = mock(DiscountService.class);
        exportService = mock(CardExportService.class);
        appSettingService = mock(AppSettingsService.class);

        controller = new CardController(service, batchService,
                notificationService, planService, ticketService,
                cardOperationService,
                messageSource, velocityEngineApp, cardLogService, discountService, exportService, appSettingService);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testLoadCards() throws IllegalArgumentException,
            InvalidDateRangeException {

        List<String> cards = Arrays.asList("222", "333");
        String responseMessage = "response message";

        String plan = "plan";
        String fromDate = "22.06.2012";
        String toDate = "22.06.2012";
        List<String> numbers = new ArrayList<String>();
        // пройдут только с CVV
        numbers.add("\t222");
        numbers.add(" \t222");
        numbers.add("\t ");
        numbers.add("11\t");
        numbers.add("");
        numbers.add("222\t222");
        numbers.add("");
        numbers.add("333\t333");
        Map<String, String> correctNumbers = new HashMap<String, String>();
        correctNumbers.put("222", "222");
        correctNumbers.put("333", "333");
        String selectedBatchName = "";
        String newBatchName = "zero";
        String newBatchTitle = "";

        // type new
        when(service.createCards(plan, fromDate, toDate, correctNumbers))
                .thenReturn(cards);
        when(
                batchService.addCardsToBatch(newBatchName, newBatchTitle,
                        cards)).thenReturn(true);
        when(
                messageSource.getMessage("msg.saved", (Object[]) null,
                        (Locale) null)).thenReturn(responseMessage);

        Map<String, ?> responseBody = (Map<String, Object>) controller
                .loadCards(plan, fromDate, toDate, numbers,
                        BatchOperationType.NEW,
                        selectedBatchName, newBatchName, newBatchTitle);

        verify(service).createCards(plan, fromDate, toDate, correctNumbers);
        verify(batchService).addCardsToBatch(newBatchName, newBatchTitle,
                cards);
        verify(messageSource).getMessage("msg.saved", (Object[]) null,
                (Locale) null);

        assertNotNull(responseBody);
        assertTrue((Boolean) responseBody.get(AjaxUtil.JSON_SUCCESS));
        assertEquals(cards.toString(), responseBody.get(AjaxUtil.JSON_DATA));

        // type add
        responseBody = (Map<String, Object>) controller
                .loadCards(plan, fromDate, toDate, numbers,
                        BatchOperationType.ADD,
                        selectedBatchName, newBatchName, newBatchTitle);

        verify(service, times(2)).createCards(plan, fromDate, toDate,
                correctNumbers);
        verify(batchService, times(1)).addCardsToBatch(newBatchName, "", cards);
        verify(messageSource, times(2)).getMessage("msg.saved",
                (Object[]) null,
                (Locale) null);

        assertNotNull(responseBody);
        assertTrue((Boolean) responseBody.get(AjaxUtil.JSON_SUCCESS));
        assertEquals(cards.toString(), responseBody.get(AjaxUtil.JSON_DATA));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testLoadCardsWithException() {

        String plan = "plan";
        String fromDate = "22.06.2012";
        String toDate = "22.06.2012";
        List<String> numbers = new ArrayList<String>();
        BatchOperationType batchRadio = BatchOperationType.NEW;
        String selectedBatchName = "";
        String newBatchName = "zero";
        String newBatchTitle = "";

        doThrow(new Exception()).when(service);

        Map<String, ?> responseBody = (Map<String, Object>) controller
                .loadCards(plan, fromDate, toDate, numbers, batchRadio,
                        selectedBatchName, newBatchName, newBatchTitle);

        assertNotNull(responseBody);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testSaveBatches() throws NotFoundException,
            JsonGenerationException, JsonMappingException, IOException,
            ItemSizeLimitExceededException {

        String cardNum = "num";
        List<String> batches = new ArrayList<String>();
        batches.add("batch1");
        batches.add("batch2");
        Card card = new Card();
        card.setCardBatches(batches);

        when(service.getCard(cardNum)).thenReturn(card);
        when(service.updateCard(card)).thenReturn(true);

        Map<String, ?> responseBody = (Map<String, Object>) controller
                .saveCardBatches(cardNum, batches);

        assertNotNull(responseBody);
        assertNotNull(responseBody.get("success"));
        assertEquals(batches, responseBody.get("data"));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testEditBatchesSuccess() throws NotFoundException,
            UnsupportedEncodingException, IOException, InterruptedException,
            ItemSizeLimitExceededException {

        String batch = "num";
        BatchOperationType mode = BatchOperationType.ADD;
        String reason = "reason";

        String cardNum1 = "222";
        String cardNum2 = "333";
        String cardNum3 = "444";

        List<String> numbers = new ArrayList<String>();
        numbers.add("");
        numbers.add(" " + cardNum1 + " ");
        numbers.add("");
        numbers.add(" " + cardNum2 + " ");
        numbers.add(cardNum2);
        numbers.add(cardNum3);

        List<String> correctNumbers = new ArrayList<String>();

        correctNumbers.add(cardNum1);
        correctNumbers.add(cardNum2);
        Card card1 = new Card(cardNum1);
        Card card2 = new Card(cardNum2);
        List<Card> cardList = new ArrayList<Card>();
        cardList.add(card1);
        cardList.add(card2);

        List<String> duplicates = new ArrayList<String>();
        duplicates.add(cardNum2);

        List<String> notFound = new ArrayList<String>();
        notFound.add(cardNum3);

        CardsLoadResult result = new CardsLoadResult(cardList, notFound,
                duplicates);

        String responseMessage1 = "Successfull cards updated message";
        String responseMessage3 = "Have some not found cards";
        String responseMessage4 = "Have some duplicated cards";

        when(
                messageSource.getMessage(eq("msg.updatedCards"),
                        any(Object[].class), any(Locale.class))).thenReturn(
                responseMessage1);
        when(
                messageSource.getMessage(eq("msg.cardNotFound"),
                        any(Object[].class), any(Locale.class))).thenReturn(
                responseMessage3);

        when(
                messageSource.getMessage(eq("msg.cardDuplicateNumbers"),
                        any(Object[].class), any(Locale.class))).thenReturn(
                responseMessage4);

        when(service.updateCardsBatches(any(List.class), eq(batch), eq(false)))
                .thenReturn(new ArrayList<String>());

        when(service.loadCards(any(List.class))).thenReturn(result);

        Map<String, ?> responseBody = (Map<String, Object>) controller
                .editCardsBatches(numbers, mode, batch, null, null, reason);

        verify(service).loadCards(any(List.class));
        verify(service).updateCardsBatches(any(List.class), eq(batch),
                eq(false));

        verify(messageSource).getMessage(eq("msg.updatedCards"),
                any(Object[].class), any(Locale.class));
        verify(messageSource).getMessage(eq("msg.cardNotFound"),
                any(Object[].class), any(Locale.class));
        verify(messageSource).getMessage(eq("msg.cardDuplicateNumbers"),
                any(Object[].class), any(Locale.class));

        assertNotNull(responseBody);
        assertTrue((Boolean) responseBody.get("success"));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testEditBatchesFailure() throws NotFoundException,
            UnsupportedEncodingException, IOException, InterruptedException,
            ItemSizeLimitExceededException {

        String batch = "num";
        BatchOperationType mode = BatchOperationType.ADD;
        String msg = "test ";
        String reason = "reason";
        List<String> numbers = new ArrayList<String>();
        numbers.add("222");
        numbers.add("333");
        Card card1 = new Card("222");
        Card card2 = new Card("333");
        List<Card> cardList = new ArrayList<Card>();
        cardList.add(card1);
        cardList.add(card2);

        CardsLoadResult result = new CardsLoadResult(cardList, null, null);

        when(service.updateCardsBatches(any(List.class), eq(batch), eq(false)))
                .thenReturn(numbers);

        when(service.loadCards(any(List.class))).thenReturn(result);

        when(
                messageSource.getMessage(eq("msg.notUpdatedCards"),
                        any(Object[].class), any(Locale.class)))
                .thenReturn(msg);

        Map<String, ?> responseBody = (Map<String, Object>) controller
                .editCardsBatches(numbers, mode, batch, null, null, reason);

        assertNotNull(responseBody);
        assertTrue((Boolean) responseBody.get("success"));
        assertEquals(msg + StringUtils.join(numbers, ","), responseBody
                .get("data"));

        verify(service).loadCards(any(List.class));
        verify(service).updateCardsBatches(any(List.class), eq(batch),
                eq(false));
        verify(messageSource).getMessage(eq("msg.notUpdatedCards"),
                any(Object[].class), any(Locale.class));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testEditBatchesAddNewSuccess() throws NotFoundException,
            UnsupportedEncodingException, IOException, InterruptedException,
            ItemSizeLimitExceededException {

        String batch = "newnum";
        String description = "newnum description";
        String reason = "reason";
        BatchOperationType mode = BatchOperationType.NEW;
        List<String> numbers = new ArrayList<String>();
        numbers.add("");
        numbers.add(" 222 ");
        numbers.add("");
        numbers.add(" 333 ");

        List<String> correctNumbers = new ArrayList<String>();
        correctNumbers.add("222");
        correctNumbers.add("333");
        Card card1 = new Card("222");
        Card card2 = new Card("333");
        List<Card> cardList = new ArrayList<Card>();
        cardList.add(card1);
        cardList.add(card2);

        CardsLoadResult result = new CardsLoadResult(cardList, null, null);

        when(service.loadCards(any(List.class))).thenReturn(result);

        when(service.updateCardsBatches(any(List.class), eq(batch), eq(false)))
                .thenReturn(new ArrayList<String>());

        when(batchService.createBatchIfNotExists(eq(batch), eq(description)))
                .thenReturn(true);

        Map<String, ?> responseBody = (Map<String, Object>) controller
                .editCardsBatches(numbers, mode, null, batch, description,
                        reason);

        verify(service).loadCards(any(List.class));
        verify(batchService).createBatchIfNotExists(eq(batch), eq(description));
        verify(service).updateCardsBatches(any(List.class), eq(batch),
                eq(false));

        assertNotNull(responseBody);
        assertTrue((Boolean) responseBody.get("success"));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testCardPlanChange() throws JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException,
            InterruptedException {

        String planId = "planId";
        String reason = "reason";
        String notifcation = "notification text";
        List<String> numbers = new ArrayList<String>();

        String cardNumber1 = "7771";
        String cardNumber2 = "7772";
        String cardNumber3 = "7773";
        String cardNumber4 = "7774";
        numbers.add(cardNumber1);
        numbers.add("");
        numbers.add(cardNumber2);

        List<String> correctNumbers = new ArrayList<String>();
        correctNumbers.add(cardNumber1);
        correctNumbers.add(cardNumber2);

        List<String> updatedActiveNumbers = new ArrayList<String>();
        List<String> updatedInactivedNumbers = new ArrayList<String>();
        updatedActiveNumbers.add(cardNumber1);
        updatedInactivedNumbers.add(cardNumber2);

        List<String> notFoundNumbers = new ArrayList<String>();
        notFoundNumbers.add(cardNumber3);

        List<String> duplicateNumbers = new ArrayList<String>();
        duplicateNumbers.add(cardNumber4);

        List<Card> cards = new ArrayList<Card>();
        List<Card> updatedActiveCards = new ArrayList<Card>();
        Card card1 = new Card(cardNumber1);
        Card card2 = new Card(cardNumber2);
        cards.add(card1);
        cards.add(card2);
        updatedActiveCards.add(card1);

        CardsLoadResult result = new CardsLoadResult(cards, notFoundNumbers,
                duplicateNumbers);

        String responseMessage1 = "Successfull active cards updated message";
        String responseMessage2 = "Successfull inactive cards updated message";
        String responseMessage3 = "Have some not found cards";
        String responseMessage4 = "Have some duplicated cards";

        when(
                service.getUpdatedActiveCardNumbers(any(Map.class),
                        any(Map.class))).thenReturn(updatedActiveNumbers);
        when(
                service.getUpdatedInactiveCardNumbers(any(Map.class),
                        any(Map.class))).thenReturn(updatedInactivedNumbers);
        when(
                service.getUpdatedActiveCards(any(List.class),
                        eq(updatedActiveNumbers))).thenReturn(
                updatedActiveCards);
        when(
                service.sendSmsAfterBatchCardUpdate(notifcation,
                        updatedActiveCards)).thenReturn(true);
        when(
                messageSource.getMessage(eq("msg.cardPlanUpdatedActive"),
                        any(Object[].class), any(Locale.class))).thenReturn(
                responseMessage1);
        when(
                messageSource.getMessage(eq("msg.cardPlanUpdatedInactive"),
                        any(Object[].class), any(Locale.class))).thenReturn(
                responseMessage2);

        when(
                messageSource.getMessage(eq("msg.cardNotFound"),
                        any(Object[].class), any(Locale.class))).thenReturn(
                responseMessage3);

        when(
                messageSource.getMessage(eq("msg.cardDuplicateNumbers"),
                        any(Object[].class), any(Locale.class))).thenReturn(
                responseMessage4);

        when(service.loadCards(any(List.class))).thenReturn(result);

        Map<String, ?> response = controller.cardPlanChange(planId,
                notifcation, numbers, reason);

        assertNotNull(response);
        assertNotNull(response.get("success"));
        assertTrue(((String) response.get("data")).contains(responseMessage1));
        assertTrue(((String) response.get("data")).contains(responseMessage2));
        assertTrue(((String) response.get("data")).contains(responseMessage3));
        assertTrue(((String) response.get("data")).contains(responseMessage4));

        verify(service).loadCards(any(List.class));

        verify(messageSource).getMessage(eq("msg.cardPlanUpdatedActive"),
                any(Object[].class), any(Locale.class));
        verify(messageSource).getMessage(eq("msg.cardPlanUpdatedInactive"),
                any(Object[].class), any(Locale.class));
        verify(messageSource).getMessage(eq("msg.cardNotFound"),
                any(Object[].class), any(Locale.class));
        verify(messageSource).getMessage(eq("msg.cardDuplicateNumbers"),
                any(Object[].class), any(Locale.class));

        verify(service).skipAlreadySubscribed(any(List.class), eq(planId),
                any(List.class));
        verify(service).updateCardDiscountPlan(eq(planId), any(List.class),
                any(Map.class), any(Map.class), any(List.class));
        verify(service).getUpdatedActiveCardNumbers(any(Map.class),
                any(Map.class));
        verify(service).getUpdatedInactiveCardNumbers(any(Map.class),
                any(Map.class));
        verify(service).getUpdatedActiveCards(any(List.class),
                eq(updatedActiveNumbers));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testCardDateChange() throws JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException,
            InterruptedException, InvalidDateRangeException {

        String fromDate = "20.10.2012";
        String toDate = "30.10.2012";
        String notifcation = "notification text";
        String reason = "reason";
        List<String> numbers = new ArrayList<String>();

        String cardNumber1 = "7771";
        String cardNumber2 = "7772";
        String cardNumber3 = "7773";
        String cardNumber4 = "7774";
        numbers.add(cardNumber1);
        numbers.add("");
        numbers.add(cardNumber2);

        List<String> correctNumbers = new ArrayList<String>();
        correctNumbers.add(cardNumber1);
        correctNumbers.add(cardNumber2);

        List<String> notFoundNumbers = new ArrayList<String>();
        notFoundNumbers.add(cardNumber3);

        List<String> duplicateNumbers = new ArrayList<String>();
        duplicateNumbers.add(cardNumber4);

        List<String> updatedActiveNumbers = new ArrayList<String>();
        List<String> updatedInactivedNumbers = new ArrayList<String>();
        updatedActiveNumbers.add(cardNumber1);
        updatedInactivedNumbers.add(cardNumber2);

        List<Card> cards = new ArrayList<Card>();
        List<Card> updatedActiveCards = new ArrayList<Card>();
        Card card1 = new Card(cardNumber1);
        Card card2 = new Card(cardNumber2);
        cards.add(card1);
        cards.add(card2);
        updatedActiveCards.add(card1);

        CardsLoadResult result = new CardsLoadResult(cards, notFoundNumbers,
                duplicateNumbers);

        String responseMessage1 = "Successfull active cards updated message";
        String responseMessage2 = "Successfull inactive cards updated message";
        String responseMessage3 = "Have some not found cards";
        String responseMessage4 = "Have some duplicated cards";

        when(
                service.getUpdatedActiveCardNumbers(any(Map.class),
                        any(Map.class))).thenReturn(updatedActiveNumbers);
        when(
                service.getUpdatedInactiveCardNumbers(any(Map.class),
                        any(Map.class))).thenReturn(updatedInactivedNumbers);
        when(
                service.getUpdatedActiveCards(any(List.class),
                        eq(updatedActiveNumbers))).thenReturn(
                updatedActiveCards);
        when(
                service.sendSmsAfterBatchCardUpdate(notifcation,
                        updatedActiveCards)).thenReturn(true);
        when(
                messageSource.getMessage(eq("msg.cardDateUpdatedActive"),
                        any(Object[].class), any(Locale.class))).thenReturn(
                responseMessage1);
        when(
                messageSource.getMessage(eq("msg.cardDateUpdatedInactive"),
                        any(Object[].class), any(Locale.class))).thenReturn(
                responseMessage2);

        when(
                messageSource.getMessage(eq("msg.cardNotFound"),
                        any(Object[].class), any(Locale.class))).thenReturn(
                responseMessage3);

        when(
                messageSource.getMessage(eq("msg.cardDuplicateNumbers"),
                        any(Object[].class), any(Locale.class))).thenReturn(
                responseMessage4);

        when(service.loadCards(any(List.class))).thenReturn(result);

        Map<String, ?> response = controller.cardDateChange(fromDate, toDate,
                notifcation, numbers, reason);

        assertNotNull(response);
        assertNotNull(response.get("success"));
        assertTrue(((String) response.get("data")).contains(responseMessage1));
        assertTrue(((String) response.get("data")).contains(responseMessage2));
        assertTrue(((String) response.get("data")).contains(responseMessage3));
        assertTrue(((String) response.get("data")).contains(responseMessage4));

        verify(service).loadCards(any(List.class));

        verify(messageSource).getMessage(eq("msg.cardDateUpdatedActive"),
                any(Object[].class), any(Locale.class));
        verify(messageSource).getMessage(eq("msg.cardDateUpdatedInactive"),
                any(Object[].class), any(Locale.class));
        verify(messageSource).getMessage(eq("msg.cardNotFound"),
                any(Object[].class), any(Locale.class));
        verify(messageSource).getMessage(eq("msg.cardDuplicateNumbers"),
                any(Object[].class), any(Locale.class));

        verify(service).updateCardDate(eq(fromDate), eq(toDate),
                any(List.class), any(Map.class), any(Map.class),
                any(List.class));
        verify(service).getUpdatedActiveCardNumbers(any(Map.class),
                any(Map.class));
        verify(service).getUpdatedInactiveCardNumbers(any(Map.class),
                any(Map.class));
        verify(service).getUpdatedActiveCards(any(List.class),
                eq(updatedActiveNumbers));
        verify(service).sendSmsAfterBatchCardUpdate(anyString(),
                any(List.class));
    }

    @Test
    public void testGetDetails() throws NotFoundException, JsonParseException,
            JsonMappingException, IOException, RuntimeException,
            InterruptedException, InstantiationException,
            IllegalAccessException, OlekstraException {

        String term = " 2 2 2 ";// spaces should trimmed
        String number = "222";
        Card card = new Card(number);
        List<String> batches = new ArrayList<String>();
        batches.add("Test");

        when(service.searchCard(number)).thenReturn(card);
        when(batchService.getAllBatchNames()).thenReturn(batches);

        ModelAndView mav = controller.cardDetails(term);
        assertEquals("cardInfo", mav.getViewName());
        assertTrue(mav.getModel().containsKey("card"));
        assertTrue(mav.getModel().containsKey("discountplan"));
        assertTrue(mav.getModel().containsKey("allBatches"));
        assertTrue(mav.getModel().containsKey("cardBatches"));

    }

    @Test
    public void testCardsBalance() throws RuntimeException,
            InterruptedException, JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException {

        BigDecimal summ = new BigDecimal(15);
        BalanceOperationType operationType = BalanceOperationType.RECAHARGE;

        String cardNumber1 = "7771";
        String cardNumber2 = "7772";
        String reason = "reason";

        List<String> numbers = Arrays.asList(cardNumber1, "", cardNumber2,
                cardNumber2);
        List<String> numbersWithoutEmpty = Arrays.asList(cardNumber1,
                cardNumber2, cardNumber2);

        List<String> updatedActiveNumbers = new ArrayList<String>();
        List<String> updatedInactivedNumbers = new ArrayList<String>();
        updatedActiveNumbers.add(cardNumber1);
        updatedInactivedNumbers.add(cardNumber2);

        List<String> numbersToUpdate = new ArrayList<String>();
        Map<String, Boolean> updatedCards = new HashMap<String, Boolean>();
        Map<String, BigDecimal> updatedLimits = new HashMap<String, BigDecimal>();
        List<String> notLoadedCardNumbers = new ArrayList<String>();

        List<String> duplicates = new ArrayList<String>();

        List<String> sameLimitCards = new ArrayList<String>();
        Map<String, BigDecimal> moreLimitCards = new HashMap<String, BigDecimal>();

        when(
                service.sendSmsAfterUpdateCardBalance(eq(summ.setScale(2,
                        RoundingMode.HALF_UP)), eq(updatedLimits),
                        anyListOf(Card.class), any(BalanceOperationType.class)))
                .thenReturn(true);
        when(service.getNotUpdatedCardsList(eq(updatedCards))).thenReturn(
                new ArrayList<String>());

        Map<String, ?> response = controller.cardsBalance(summ, numbers,
                operationType, reason);

        verify(service).prepareCardNumbers(eq(numbersWithoutEmpty),
                eq(numbersToUpdate), eq(duplicates));
        verify(service).loadCardsByNumbers(eq(numbersToUpdate),
                eq(new ArrayList<Card>()), eq(notLoadedCardNumbers));
        verify(service).updateCardBalance(
                eq(summ.setScale(2, RoundingMode.HALF_UP)),
                eq(new ArrayList<Card>()), eq(updatedCards), eq(updatedLimits),
                eq(sameLimitCards), eq(moreLimitCards), eq(operationType));
        verify(service).sendSmsAfterUpdateCardBalance(
                eq(summ.setScale(2, RoundingMode.HALF_UP)), eq(updatedLimits),
                anyListOf(Card.class), any(BalanceOperationType.class));
        verify(cardOperationService).saveUpdateLimitRecords(eq(updatedLimits),
                eq(summ.setScale(2, RoundingMode.HALF_UP)), eq(true),
                eq(operationType), eq(reason));
        verify(service).getNotUpdatedCardsList(eq(updatedCards));

        assertNotNull(response);
        assertNotNull(response.get("success"));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testCardsBalanceNew() throws RuntimeException,
            InterruptedException, JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException {

        String cardNumber1 = "7771";
        String cardNumber2 = "7772";
        String reason = "reason";

        String numbersAndValues = cardNumber1 + "\t" + "10.00" + "\n\n" + cardNumber2
                + "\t" + "99.99" + "\n" + cardNumber2;
        List<String> cardNumbers = Arrays.asList(cardNumber1, cardNumber2);

        List<String> updatedActiveNumbers = new ArrayList<String>();
        List<String> updatedInactivedNumbers = new ArrayList<String>();
        updatedActiveNumbers.add(cardNumber1);
        updatedInactivedNumbers.add(cardNumber2);

        Map<String, Boolean> updatedCards = new HashMap<String, Boolean>();
        Map<String, BigDecimal> updatedLimits = new HashMap<String, BigDecimal>();
        List<String> notLoadedCardNumbers = new ArrayList<String>();

        when(
                service.sendSmsAfterUpdateCardBalance(eq(updatedLimits), anyListOf(Card.class)))
                .thenReturn(true);
        when(service.getNotUpdatedCardsList(eq(updatedCards))).thenReturn(
                new ArrayList<String>());

        Map<String, ?> response = controller.cardsBalanceNew(numbersAndValues, reason);

        verify(service).loadCardsByNumbers(eq(cardNumbers),
                eq(new ArrayList<Card>()), eq(notLoadedCardNumbers));
        verify(service).updateCardBalance(
                any(Map.class), eq(new ArrayList<Card>()), eq(updatedCards), eq(updatedLimits));
        verify(service).sendSmsAfterUpdateCardBalance(eq(updatedLimits), anyListOf(Card.class));
        verify(cardOperationService).saveUpdateLimitRecords(eq(updatedLimits), eq(true), eq(reason));
        verify(service).getNotUpdatedCardsList(eq(updatedCards));

        assertNotNull(response);
        assertNotNull(response.get("success"));
    }

    @Test
    public void testUpdateCardOwner() throws NotFoundException,
            JsonGenerationException, JsonMappingException, IOException,
            ItemSizeLimitExceededException, DuplicatePhoneException,
            NotUpdatedException {

        String cardNumber = "777";
        String ownerName = "test user name";
        String ownerPhone = "1234567890";
        String oldOwnerName = "old user name";
        String oldOwnerPhone = "0987654321";
        String message = "message";

        Card card = new Card(cardNumber, false, oldOwnerName, oldOwnerPhone);

        when(service.getCard(eq(cardNumber))).thenReturn(card);
        when(
                messageSource.getMessage(eq("msg.saved"),
                        any(Object[].class), any(Locale.class))).thenReturn(
                message);

        Map<String, ?> result = controller.updateCardOwner(cardNumber,
                ownerName, ownerPhone);

        verify(service).getCard(eq(cardNumber));
        verify(messageSource).getMessage(eq("msg.saved"), any(Object[].class),
                any(Locale.class));

        verify(service).updateCardOwner(eq(card), eq(oldOwnerPhone),
                eq(ownerPhone));
        verify(cardOperationService).saveChangeOwnerInfoRecord(eq(cardNumber),
                eq(oldOwnerName), eq(ownerName), eq(oldOwnerPhone),
                eq(ownerPhone));

        assertTrue((Boolean) result.get(AjaxUtil.JSON_SUCCESS));
        assertEquals(message, result.get(AjaxUtil.JSON_MESSAGE));
    }

    @Test
    public void testLockCard() throws Exception {
        String cardNumber = "123";
        String lockReason = "test lock reason";
        String jiraTask = "test Jira task";
        boolean disabled = true;

        String ownerName = "test user name";
        String ownerPhone = "1234567890";
        String oldOwnerName = "old user name";
        String oldOwnerPhone = "0987654321";
        String message = "message";

        Card card = new Card(cardNumber, false, oldOwnerName, oldOwnerPhone);

        when(service.getCard(eq(cardNumber))).thenReturn(card);
        when(
                messageSource.getMessage(eq("msg.success"),
                        any(Object[].class), any(Locale.class))).thenReturn(
                message);

        when(principal.getName()).thenReturn("user0");
        CardLog cardLog = new CardLog("", DateTime.now(), "",
                CardLogType.DISABLED, false);
        when(
                cardLogService.createDisableCardTypeCardLog(eq(card),
                        eq(lockReason), eq(jiraTask), eq(disabled)))
                .thenReturn(cardLog);

        Map<String, ?> result = controller.disableCard(cardNumber, lockReason,
                jiraTask, disabled, principal);

        verify(cardLogService).createDisableCardTypeCardLog(eq(card),
                eq(lockReason), eq(jiraTask), eq(disabled));

        assertTrue((Boolean) result.get(AjaxUtil.JSON_SUCCESS));
    }

    @Test
    public void testSearchCard() throws NotFoundException {

        String cardNumber = "777";
        String term = cardNumber;
        String planId = "test plan";
        String planName = "test plan name";

        Card card = new Card(term);
        card.setDiscountPlan(planId);

        when(service.searchCard(eq(term))).thenReturn(card);
        when(planService.getDiscountPlanName(eq(planId))).thenReturn(planName);

        Map<String, ?> result = controller.searchCard(term);

        verify(service).searchCard(eq(term));
        verify(planService).getDiscountPlanName(eq(planId));

        assertNotNull(result);
        assertTrue((Boolean) result.get(AjaxUtil.JSON_SUCCESS));
        assertEquals(cardNumber,
                ((CardResponse) result.get(AjaxUtil.JSON_DATA)).getNumber());
        assertEquals(planName,
                ((CardResponse) result.get(AjaxUtil.JSON_DATA)).getPlan());
    }

    @Test
    public void testActivateCard() throws IllegalArgumentException,
            NotFoundException, NotUpdatedException, DuplicatePhoneException,
            InvalidDateRangeException, JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException {

        String number = "777";
        String phone = "1234567890";
        String holder = "Test Owner";
        String planId = "test plan";
        String dateFrom = "01.03.2013";
        String dateTo = "01.03.2014";

        Card card = new Card(number, false, holder, phone, DateTimeFormat
                .forPattern(DateTimeUtil.FILE_INPUT_PARSE_DATE_PATTERN)
                .parseDateTime(dateFrom), DateTimeFormat.forPattern(
                DateTimeUtil.FILE_INPUT_PARSE_DATE_PATTERN).parseDateTime(
                dateTo));

        String messageActivated = "message activated";
        String messageSuccess = "message success";

        when(
                service.activate(eq(number), eq(phone), eq(holder), eq(planId),
                        eq(dateFrom), eq(dateTo))).thenReturn(card);
        when(
                messageSource.getMessage(eq("msg.activated"),
                        eq((Object[]) null), eq((Locale) null))).thenReturn(
                messageActivated);
        when(
                messageSource.getMessage(eq("msg.success"),
                        eq((Object[]) null), eq((Locale) null))).thenReturn(
                messageSuccess);

        Map<String, ?> result = controller.activateCard(number, phone, holder,
                planId, dateFrom, dateTo);

        verify(service).activate(eq(number), eq(phone), eq(holder), eq(planId),
                eq(dateFrom), eq(dateTo));
        verify(cardOperationService).saveChangePlanRecord(number, planId);

        assertNotNull(result);
        assertTrue((Boolean) result.get(AjaxUtil.JSON_SUCCESS));
        assertEquals(messageActivated, result.get(AjaxUtil.JSON_MESSAGE));
        assertEquals(messageSuccess, result.get(AjaxUtil.JSON_DATA));
    }

    @Test
    public void testActivateCardBatch() throws RuntimeException,
            InterruptedException,
            JsonGenerationException, JsonMappingException, IOException,
            ItemSizeLimitExceededException {

        String rowCards = "7771;Test User\n7772;Test User\n7773;Test User\7772;Test User\n777a;Test User\n";
        String planId = "test plan id";
        String reason = "reason";

        CardsBatchActivationForm form = new CardsBatchActivationForm(rowCards,
                planId, reason);
        BindingResult bindingResult = mock(BindingResult.class);

        Card card1 = new Card("7771");
        card1.setHolderName("Test User");
        Card card2 = new Card("7772");
        card2.setHolderName("Test User");
        Card card3 = new Card("7773");
        card3.setHolderName("Test User");

        List<Card> allValidCardNumsList = Arrays.asList(card1, card2, card3);
        List<Card> cardsForActivationList = Arrays.asList(card1, card2);

        List<String> unactivatedCardsList = Arrays.asList("7772");
        List<String> successfullyActivatedCardNumberList = Arrays
                .asList("7771");

        String responseMessageCheck = "responseMessageCheck";

        when(
                service.getCardsFromCsvString(eq(rowCards),
                        anyListOf(String.class), anyListOf(String.class)))
                .thenReturn(allValidCardNumsList);
        when(
                service.loadCardList(eq(allValidCardNumsList),
                        anyListOf(String.class), anyListOf(String.class),
                        anyListOf(String.class), anyListOf(String.class),
                        eq(planId))).thenReturn(cardsForActivationList);
        when(service.activateCardsInBatch(eq(cardsForActivationList)))
                .thenReturn(unactivatedCardsList);
        when(
                service.getSuccessfullyActivatedCardList(
                        eq(cardsForActivationList), eq(unactivatedCardsList),
                        anyListOf(Card.class))).thenReturn(
                successfullyActivatedCardNumberList);
        when(service.sendSmsAfterBatchCardActivation(anyListOf(Card.class)))
                .thenReturn(true);
        when(
                messageSource.getMessage(eq("msg.activatedCardsCount"),
                        eq(new Object[] {successfullyActivatedCardNumberList
                                .size()}), eq((Locale) null))).thenReturn(
                responseMessageCheck);

        Map<String, Object> result = controller.activateCardBatch(form,
                bindingResult);

        verify(service).getCardsFromCsvString(eq(rowCards),
                anyListOf(String.class), anyListOf(String.class));
        verify(service).loadCardList(eq(allValidCardNumsList),
                anyListOf(String.class), anyListOf(String.class),
                anyListOf(String.class), anyListOf(String.class),
                eq(planId));
        verify(service).activateCardsInBatch(eq(cardsForActivationList));
        verify(service).getSuccessfullyActivatedCardList(
                eq(cardsForActivationList), eq(unactivatedCardsList),
                anyListOf(Card.class));
        verify(cardOperationService).saveCardActivateRecords(
                anyListOf(Card.class), eq(reason));
        verify(service).sendSmsAfterBatchCardActivation(anyListOf(Card.class));
        verify(messageSource).getMessage(eq("msg.activatedCardsCount"),
                eq(new Object[] {successfullyActivatedCardNumberList
                        .size()}), eq((Locale) null));

        assertNotNull(result);
        assertTrue((Boolean) result.get(AjaxUtil.JSON_SUCCESS));
        assertTrue(((String) result.get(AjaxUtil.JSON_DATA))
                .contains(responseMessageCheck));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testCardClear() throws JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException,
            InterruptedException {

        String reason = "reason";
        List<String> numbers = new ArrayList<String>();

        List<Card> cards = new ArrayList<Card>();
        Card card1 = new Card("1", false, "Aa", "12345");
        Card card2 = new Card("2", false, "Bb", "42345");
        Card card3 = new Card("3", false, null, null);
        cards.add(card1);
        cards.add(card2);
        cards.add(card3);

        numbers.add(card1.getNumber());
        numbers.add(card1.getNumber());
        numbers.add(card2.getNumber());
        numbers.add(card3.getNumber());
        numbers.add("4");
        numbers.add("");

        List<String> notFoundNumbers = new ArrayList<String>();
        notFoundNumbers.add("3");

        List<String> duplicateNumbers = new ArrayList<String>();
        duplicateNumbers.add(card1.getNumber());

        CardsLoadResult result = new CardsLoadResult(cards, notFoundNumbers,
                duplicateNumbers);

        String successClear = "Successfull clear message";
        String someNotFound = "Have some not found cards";
        String dublicateFoud = "Have some duplicated cards";
        String optError = "Error in input parameters";
        String emptyCardNumbers = "Card number not present";
        String reasonEmpty = "Reason not present";
        String alreadyEmpty = "Already empty cards found";

        when(
                messageSource.getMessage(eq("msg.cardAttributeCleared"),
                        any(Object[].class), any(Locale.class))).thenReturn(
                successClear);
        when(
                messageSource.getMessage(eq("msg.cardNotFound"),
                        any(Object[].class), any(Locale.class))).thenReturn(
                someNotFound);
        when(
                messageSource.getMessage(eq("msg.cardDuplicateNumbers"),
                        any(Object[].class), any(Locale.class))).thenReturn(
                dublicateFoud);
        when(
                messageSource.getMessage(eq("msg.cardAlreadyEmpty"),
                        any(Object[].class), any(Locale.class))).thenReturn(
                alreadyEmpty);
        when(
                messageSource.getMessage(eq("msg.enterCardNumber"),
                        any(Object[].class), any(Locale.class))).thenReturn(
                emptyCardNumbers);
        when(
                messageSource.getMessage(eq("msg.optionsError"),
                        any(Object[].class), any(Locale.class))).thenReturn(
                optError);
        when(
                messageSource.getMessage(eq("msg.reasonEmpty"),
                        any(Object[].class), any(Locale.class))).thenReturn(
                reasonEmpty);

        when(service.loadCards(any(List.class))).thenReturn(result);

        Map<String, ?> response = controller.cardClear(true, true, true, true,
                true, true, numbers, reason);

        assertNotNull(response);
        assertNotNull(response.get("success"));
        assertNotNull(response.get("data"));

        assertTrue(((String) response.get("data")).contains(successClear));
        assertTrue(((String) response.get("data")).contains(someNotFound));
        assertTrue(((String) response.get("data")).contains(dublicateFoud));
        assertTrue(((String) response.get("data")).contains(alreadyEmpty));

        verify(service).loadCards(any(List.class));

        verify(messageSource).getMessage(eq("msg.cardAttributeCleared"),
                any(Object[].class), any(Locale.class));
        verify(messageSource).getMessage(eq("msg.cardNotFound"),
                any(Object[].class), any(Locale.class));
        verify(messageSource).getMessage(eq("msg.cardDuplicateNumbers"),
                any(Object[].class), any(Locale.class));
        verify(messageSource).getMessage(eq("msg.cardAlreadyEmpty"),
                any(Object[].class), any(Locale.class));

        verify(service).saveCardAttributes(any(List.class), any(Map.class));
        verify(cardOperationService).saveClearAttributesRecords(any(Map.class),
                any(String.class));
    }
}
