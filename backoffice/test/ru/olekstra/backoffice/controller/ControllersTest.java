package ru.olekstra.backoffice.controller;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.ModelAndViewAssert.assertModelAttributeAvailable;
import static org.springframework.test.web.ModelAndViewAssert.assertViewName;

import javax.servlet.ServletContext;

import org.dozer.DozerBeanMapper;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.servlet.HandlerAdapter;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.SqsService;
import ru.olekstra.awsutils.dynamodb.TableManagement;
import ru.olekstra.common.dao.AppSettingsDao;
import ru.olekstra.common.dao.DrugstoreDao;
import ru.olekstra.common.service.AboutService;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.common.service.CardExportService;
import ru.olekstra.common.service.CardLogService;
import ru.olekstra.common.service.DateTimeService;
import ru.olekstra.common.service.DiscountService;
import ru.olekstra.common.service.PlanService;
import ru.olekstra.domain.Card;
import ru.olekstra.service.CardBatchService;
import ru.olekstra.service.CardOperationService;
import ru.olekstra.service.CardService;
import ru.olekstra.service.NotificationService;
import ru.olekstra.service.TicketService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class ControllersTest {

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private AboutController aboutController;

    @Autowired
    private AppSettingsController appSettingsController;

    @Autowired
    private CardController cardController;

    @Autowired
    private AboutService aboutService;

    @Autowired
    private TableManagement tableService;

    @Autowired
    private HandlerAdapter handlerAdapter;

    @Autowired
    private SqsService sqsService;

    @Autowired
    private CardExportService exportService;

    @Autowired
    private AppSettingsService appSettingService;

    private MockHttpServletRequest request;
    private MockHttpServletResponse response;

    private static CardService cardService;

    @Before
    public void setUp() {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        handlerAdapter = applicationContext
                .getBean(AnnotationMethodHandlerAdapter.class);
    }

    @Test
    public void testAbout() throws Exception {
        request.setRequestURI("/about");
        ModelAndView mav = handlerAdapter.handle(request, response,
                aboutController);
        assertViewName(mav, "about");
    }

    @Test
    public void testSettings() throws Exception {
        request.setRequestURI("/settings/edit");
        request.setMethod("GET");
        ModelAndView mav = handlerAdapter.handle(request, response,
                appSettingsController);
        assertViewName(mav, "settings");
    }

    @Test
    public void testCards() throws Exception {
        request.setRequestURI("/card/details");
        request.setMethod("GET");
        request.setParameter("term", "123");

        Card card = new Card("123", false);
        card.setDiscountPlan("discountPlanId");

        when(cardService.searchCard("123")).thenReturn(card);

        ModelAndView mav = handlerAdapter.handle(request, response,
                cardController);

        assertViewName(mav, "cardInfo");
        assertModelAttributeAvailable(mav, "discountplan");
        assertModelAttributeAvailable(mav, "allBatches");
        assertModelAttributeAvailable(mav, "cardBatches");
        assertModelAttributeAvailable(mav, "logRecordList");
        assertModelAttributeAvailable(mav, "showInfo");

    }

    @Configuration
    static class AboutControllerTestConfig {
        @Bean
        public SqsService sqsService() {
            return Mockito.mock(SqsService.class);
        }

        @Bean
        public JmsTemplate jmsTemplate() {
            return Mockito.mock(JmsTemplate.class);
        }

        @Bean
        public DynamodbService service() {
            return mock(DynamodbService.class);
        }

        @Bean(name = "org.dozer.Mapper")
        public DozerBeanMapper mapper() {
            return new org.dozer.DozerBeanMapper();
        }

        @Bean
        public CardExportService exportService() {
            return mock(CardExportService.class);
        }

        @Bean
        public AppSettingsService appSettingsService() {
            return mock(AppSettingsService.class);
        }

        @Bean
        public ServletContext servletContext() {
            return new MockServletContext();
        }

        @Bean
        public HandlerAdapter handlerAdapter() {
            return new AnnotationMethodHandlerAdapter();
        }

        @Bean
        public AboutService aboutService() {
            return Mockito.mock(AboutService.class);
        }

        @Bean
        public TableManagement tableService() {
            return Mockito.mock(TableManagement.class);
        }

        @Bean
        public AboutController aboutController() {
            return new AboutController();
        }

        @Bean
        public AppSettingsController appSettingsController() {
            AppSettingsService appSettingService = mock(AppSettingsService.class);
            AppSettingsDao appSettingsDao = mock(AppSettingsDao.class);
            MessageSource messageSource = mock(MessageSource.class);
            LocaleResolver localeResolver = mock(LocaleResolver.class);
            return new AppSettingsController(appSettingService, appSettingsDao,
                    messageSource, localeResolver);
        }

        @Bean
        public DrugstoreDao drugstoreDao() {
            return mock(DrugstoreDao.class);
        }

        @Bean
        public DateTimeService dateTimeService() {
            DateTimeService dateTimeService = mock(DateTimeService.class);
            when(dateTimeService.createDateTimeWithZone(anyString())).thenReturn(DateTime.now());
            when(dateTimeService.createDefaultDateTime()).thenReturn(DateTime.now());

            return dateTimeService;
        }

        @Bean
        public CardController cardController() {

            cardService = mock(CardService.class);
            CardBatchService batchService = mock(CardBatchService.class);
            PlanService planService = mock(PlanService.class);
            TicketService ticketService = mock(TicketService.class);
            NotificationService notificationService = mock(NotificationService.class);
            CardOperationService cardOperationService = mock(CardOperationService.class);
            MessageSource messageSource = mock(MessageSource.class);
            // VelocityEngine velocityEngineApp = mock(VelocityEngine.class);
            CardLogService cardLogService = mock(CardLogService.class);
            DiscountService discountService = mock(DiscountService.class);

            return new CardController(cardService, batchService,
                    notificationService, planService, ticketService,
                    cardOperationService,
                    messageSource, null, cardLogService, discountService, exportService(), appSettingsService());
        }

    }

}
