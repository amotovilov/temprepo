package ru.olekstra.backoffice.controller;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;

import ru.olekstra.backoffice.util.AjaxUtil;
import ru.olekstra.common.dao.AppSettingsDao;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.domain.AppSettings;

@RunWith(MockitoJUnitRunner.class)
public class AppSettingControllerTest {

    private AppSettingsService service;
    private AppSettingsDao appSettingsDao;
    private MessageSource messageSource;
    private AppSettingsController controller;

    @Before
    public void setup() {
        service = mock(AppSettingsService.class);
        appSettingsDao = mock(AppSettingsDao.class);
        messageSource = mock(MessageSource.class);
        controller = new AppSettingsController(service, appSettingsDao,
                messageSource, null);
    }

    @Test
    public void testSaveSetting() throws JsonGenerationException, JsonMappingException, IOException {

        when(appSettingsDao.saveSettings(any(AppSettings.class))).thenReturn(
                true);

        Map<String, ?> result = controller.saveAppSettings("", "", "", "", "",
                Arrays.asList(new String[] {"New1", "Old1"}), Arrays
                        .asList(new String[] {"New2", "Old2"}), 1000, Arrays
                        .asList(new String[] {"New3", "Old3"}), Arrays
                        .asList(new String[] {"New4", "Old4"}), "", "", "",
                Arrays.asList(new String[] {"New5", "Old5"}), 5L, 10L, "",
                Arrays.asList(new String[] {"new6", "old6"}), 20,
                "0000000000000", "Aa", Arrays.asList(new String[] {"card000", "card001"}),
                Arrays.asList(new String[] {"formular1=formular1", "formular2=formular2"}));

        verify(appSettingsDao).saveSettings(any(AppSettings.class));
        verify(service).loadAppSettings();

        assertTrue((Boolean) result.get(AjaxUtil.JSON_SUCCESS));
    }

}
