package ru.olekstra.backoffice.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.validation.BindingResult;
import org.springframework.web.multipart.MultipartFile;

import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.backoffice.dto.ImportDto;
import ru.olekstra.backoffice.dto.PackImportDto;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.domain.Pack;
import ru.olekstra.exception.InvalidFormatDataException;
import ru.olekstra.service.PackService;

import com.amazonaws.AmazonServiceException;

@RunWith(MockitoJUnitRunner.class)
public class PackControllerTest {

    private PackService packService;
    private AppSettingsService settingsService;
    private MessageSource messageSource;
    private PackController controller;

    @Before
    public void setup() {
        packService = mock(PackService.class);
        settingsService = mock(AppSettingsService.class);
        messageSource = mock(MessageSource.class);
        controller = new PackController(packService, settingsService,
                messageSource);
    }

    @Test
    public void testImportGoods_AppendToSlave()
            throws InvalidFormatDataException, IOException,
            InterruptedException, AmazonServiceException,
            IllegalArgumentException, ItemSizeLimitExceededException {

        short testingAction = 1;
        InputStream content = new ByteArrayInputStream("content".getBytes());
        MultipartFile file = new MockMultipartFile("fileName", content);

        BindingResult result = mock(BindingResult.class);
        when(result.hasErrors()).thenReturn(false);

        ImportDto dto = new ImportDto(file, testingAction, false);
        Class<Pack> packEntity = settingsService.getSlaveTablePackEntity();

        when(packService.savePacks(eq(packEntity), eq(dto))).thenReturn(
                new PackImportDto(12));

        Map<String, ?> response = controller.importGoods(dto, result, false);

        verify(settingsService, times(2)).getSlaveTablePackEntity();
        verify(packService).savePacks(eq(packEntity), any(ImportDto.class));

        assertEquals(true, response.get("success"));
        assertEquals(true, response.containsKey("data"));
    }

    @Test
    public void testImportGoods_AppendToSlaveWhenFileNoFound()
            throws InvalidFormatDataException, IOException,
            InterruptedException {

        short testingAction = 1;
        InputStream content = new ByteArrayInputStream("content".getBytes());
        MultipartFile file = new MockMultipartFile("fileName", content);

        BindingResult result = mock(BindingResult.class);
        when(result.hasErrors()).thenReturn(true);

        ImportDto dto = new ImportDto(file, testingAction, false);

        Map<String, ?> response = controller.importGoods(dto, result, false);

        verify(settingsService).getSlaveTablePackEntity();

        assertEquals(false, response.get("success"));
        assertEquals(true, response.containsKey("message"));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testImportGoods_AppendToSlaveWhenFileIsWrong()
            throws InvalidFormatDataException, IOException,
            InterruptedException, AmazonServiceException,
            IllegalArgumentException, ItemSizeLimitExceededException {

        short testingAction = 1;
        InputStream content = new ByteArrayInputStream("content".getBytes());
        MultipartFile file = new MockMultipartFile("fileName", content);

        BindingResult result = mock(BindingResult.class);
        when(result.hasErrors()).thenReturn(false);

        ImportDto dto = new ImportDto(file, testingAction, false);
        Class<Pack> packEntity = settingsService.getSlaveTablePackEntity();

        when(packService.savePacks(eq(packEntity), eq(dto))).thenThrow(
                new InvalidFormatDataException(""));

        Map<String, ?> response = controller.importGoods(dto, result, false);

        verify(settingsService, times(2)).getSlaveTablePackEntity();
        verify(packService).savePacks(eq(packEntity), any(ImportDto.class));

        assertEquals(false, response.get("success"));
        assertEquals(true, response.containsKey("message"));
    }

    @Test
    public void testImportGoods_PrepareSlave()
            throws InvalidFormatDataException, IOException,
            InterruptedException {

        short testingAction = 2;

        BindingResult result = mock(BindingResult.class);
        when(result.hasErrors()).thenReturn(false);

        ImportDto dto = new ImportDto(null, testingAction, false);

        when(packService.prepareSlaveTable()).thenReturn(true);

        Map<String, ?> response = controller.importGoods(dto, result, false);

        verify(packService).prepareSlaveTable();

        assertEquals(true, response.get("success"));
        assertEquals(true, response.containsKey("data"));
    }

    @Test
    public void testImportGoods_PrepareSlaveWhenImpossibly()
            throws InvalidFormatDataException, IOException,
            InterruptedException {

        short testingAction = 2;

        BindingResult result = mock(BindingResult.class);
        when(result.hasErrors()).thenReturn(false);

        ImportDto dto = new ImportDto(null, testingAction, false);

        when(packService.prepareSlaveTable()).thenReturn(false);

        Map<String, ?> response = controller.importGoods(dto, result, false);

        verify(packService).prepareSlaveTable();

        assertEquals(false, response.get("success"));
        assertEquals(true, response.containsKey("message"));
    }

    @Test
    public void testImportGoods_AppendToMaster()
            throws InvalidFormatDataException, IOException,
            InterruptedException, AmazonServiceException,
            IllegalArgumentException, ItemSizeLimitExceededException {

        short testingAction = 3;
        InputStream content = new ByteArrayInputStream("content".getBytes());
        MultipartFile file = new MockMultipartFile("fileName", content);

        BindingResult result = mock(BindingResult.class);
        when(result.hasErrors()).thenReturn(false);

        ImportDto dto = new ImportDto(file, testingAction, false);
        Class<Pack> packEntity = settingsService.getMasterTablePackEntity();

        when(packService.savePacks(eq(packEntity), eq(dto))).thenReturn(
                new PackImportDto(12));

        Map<String, ?> response = controller.importGoods(dto, result, false);

        verify(settingsService, times(2)).getMasterTablePackEntity();
        verify(packService).savePacks(eq(packEntity), any(ImportDto.class));

        assertEquals(true, response.get("success"));
        assertEquals(true, response.containsKey("data"));
    }

    @Test
    public void testImportGoods_AppendToMasterWhenFileNoFound()
            throws InvalidFormatDataException, IOException,
            InterruptedException {

        short testingAction = 3;
        InputStream content = new ByteArrayInputStream("content".getBytes());
        MultipartFile file = new MockMultipartFile("fileName", content);

        BindingResult result = mock(BindingResult.class);
        when(result.hasErrors()).thenReturn(true);

        ImportDto dto = new ImportDto(file, testingAction, false);

        Map<String, ?> response = controller.importGoods(dto, result, false);

        verify(settingsService).getMasterTablePackEntity();

        assertEquals(false, response.get("success"));
        assertEquals(true, response.containsKey("message"));
    }

    @Test
    public void testImportGoods_AppendToMasterWhenFileIsWrong()
            throws InvalidFormatDataException, IOException,
            InterruptedException, AmazonServiceException,
            IllegalArgumentException, ItemSizeLimitExceededException {

        short testingAction = 3;
        InputStream content = new ByteArrayInputStream("content".getBytes());
        MultipartFile file = new MockMultipartFile("fileName", content);

        BindingResult result = mock(BindingResult.class);
        when(result.hasErrors()).thenReturn(false);

        ImportDto dto = new ImportDto(file, testingAction, false);
        Class<Pack> packEntity = settingsService.getMasterTablePackEntity();

        when(packService.savePacks(eq(packEntity), any(ImportDto.class)))
                .thenThrow(new InvalidFormatDataException(""));

        Map<String, ?> response = controller.importGoods(dto, result, false);

        verify(settingsService, times(2)).getMasterTablePackEntity();
        verify(packService).savePacks(eq(packEntity), any(ImportDto.class));

        assertEquals(false, response.get("success"));
        assertEquals(true, response.containsKey("message"));
    }
}
