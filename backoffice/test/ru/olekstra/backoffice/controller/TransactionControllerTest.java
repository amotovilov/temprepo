package ru.olekstra.backoffice.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.TransformerException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.OlekstraException;
import ru.olekstra.azure.model.IMessage;
import ru.olekstra.azure.model.Message;
import ru.olekstra.azure.model.MessageSendException;
import ru.olekstra.azure.service.QueueSender;
import ru.olekstra.backoffice.util.AjaxUtil;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.common.service.DateTimeService;
import ru.olekstra.common.service.XmlMessageService;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.Refund;
import ru.olekstra.domain.TransactionExportInfo;
import ru.olekstra.domain.TransactionTotalInfo;
import ru.olekstra.exception.InvalidDateRangeException;
import ru.olekstra.service.TransactionsService;

@RunWith(MockitoJUnitRunner.class)
public class TransactionControllerTest {

    private TransactionsService service;
    private MessageSource messageSource;
    private DateTimeFormatter formatter;
    private TransactionsController controller;
    private AppSettingsService appSettingService;
    private DateTimeService dateTimeService;
    private DynamodbService dynamodbService;
    private Mapper dozerBeanMapper;
    private XmlMessageService xmlMessageService;
    private QueueSender sender;

    @Before
    public void setup() {
        service = mock(TransactionsService.class);
        messageSource = mock(MessageSource.class);
        formatter = mock(DateTimeFormatter.class);
        appSettingService = mock(AppSettingsService.class);
        dateTimeService = mock(DateTimeService.class);
        dynamodbService = mock(DynamodbService.class);
        dozerBeanMapper = new DozerBeanMapper() ;
        xmlMessageService = mock(XmlMessageService.class);
        sender = mock(QueueSender.class);
        
        controller = new TransactionsController(service, messageSource,
                formatter, appSettingService, dateTimeService, 
                dynamodbService, dozerBeanMapper, xmlMessageService, sender);
    }

    @Test
    public void testViewRefundTransactions() {
        ModelAndView mav = controller.viewRefundTransactions();
        assertEquals("transactionsRefund", mav.getViewName());
    }

    @Test
    public void testGenerateRefundTransactionsCsv() throws IOException,
            XMLStreamException, InvalidDateRangeException, InstantiationException, IllegalAccessException {

        String dateFrom = "02.2013";
        String dateTo = "03.2013";

        String messageGenerationSuccessfull = "messageGenerationSuccessfull";

        String csvPath = System.getProperty("user.home")
                + "/transactions_test/" + TransactionsService.CSV_FILE_TYPE;
        String csvFilePath = csvPath + "/"
                + TransactionsService.TRANSACTIONS_FILE_CSV + ".0";
        String[] csvParsedName = csvFilePath.split("\\.");
        String csvFileIndex = csvParsedName[csvParsedName.length - 1];
        String csvFileLink = TransactionsController.URL_TRANSACTIONS_REFUND_LINK_PART
                + "/" + TransactionsService.CSV_FILE_TYPE + "/" + csvFileIndex;

        String[] csvHeaders = new String[] {"Test header", "Test header"};
        String[] csvValues = new String[] {"Test value", "Test value"};

        File dir = new File(csvPath);
        if (!dir.exists())
            dir.mkdirs();

        when(
                service.generateTransactionsFilePath(
                        eq(TransactionsService.TRANSACTIONS_FILE_CSV),
                        eq(TransactionsService.CSV_FILE_TYPE))).thenReturn(
                csvFilePath);
        when(service.getTransactionsFileIndex(eq(csvFilePath))).thenReturn(
                csvFileIndex);
        when(
                service.getTransactionsFileLink(
                        eq(TransactionsController.URL_TRANSACTIONS_REFUND_LINK_PART),
                        eq(TransactionsService.CSV_FILE_TYPE), eq(csvFileIndex)))
                .thenReturn(csvFileLink);
        when(
                messageSource.getMessage(
                        eq("msg.transactionsUnloadSuccessful"),
                        eq((Object[]) null), eq((Locale) null))).thenReturn(
                messageGenerationSuccessfull);

        when(service.getRefundTransactions(eq(dateFrom), eq(dateTo)))
                .thenReturn(
                        Arrays.asList(new Refund(), new Refund(), new Refund()));
        when(service.getRefundTransactionHeaders()).thenReturn(csvHeaders);
        when(service.getRefundTransactionAsArray(any(Refund.class)))
                .thenReturn(csvValues);

        Map<String, ?> result = controller.generateRefundTransactions(dateFrom,
                dateTo, TransactionsController.CSV_FILE_TYPE_VALUE);

        verify(service).generateTransactionsFilePath(
                eq(TransactionsService.TRANSACTIONS_FILE_CSV),
                eq(TransactionsService.CSV_FILE_TYPE));
        verify(service).getTransactionsFileIndex(eq(csvFilePath));
        verify(service).getTransactionsFileLink(
                eq(TransactionsController.URL_TRANSACTIONS_REFUND_LINK_PART),
                eq(TransactionsService.CSV_FILE_TYPE), eq(csvFileIndex));
        verify(messageSource).getMessage(
                eq("msg.transactionsUnloadSuccessful"),
                eq((Object[]) null), eq((Locale) null));

        verify(service).getRefundTransactions(eq(dateFrom), eq(dateTo));
        verify(service).getRefundTransactionHeaders();
        verify(service, times(3))
                .getRefundTransactionAsArray(any(Refund.class));

        assertTrue((Boolean) result.get(AjaxUtil.JSON_SUCCESS));
        assertEquals(csvFileLink, result.get(AjaxUtil.JSON_DATA));
        assertEquals(messageGenerationSuccessfull,
                result.get(AjaxUtil.JSON_MESSAGE));
    }

    @Test
    public void testGenerateRefundTransactionsXml() throws IOException,
            XMLStreamException, InvalidDateRangeException, InstantiationException, IllegalAccessException {

        String dateFrom = "02.2013";
        String dateTo = "03.2013";

        String messageGenerationSuccessfull = "messageGenerationSuccessfull";

        String xmlPath = System.getProperty("user.home")
                + "/transactions_test/" + TransactionsService.XML_FILE_TYPE;
        String xmlFilePath = xmlPath + "/"
                + TransactionsService.TRANSACTIONS_FILE_XML + ".0";
        String[] xmlParsedName = xmlFilePath.split("\\.");
        String xmlFileIndex = xmlParsedName[xmlParsedName.length - 1];
        String xmlFileLink = TransactionsController.URL_TRANSACTIONS_REFUND_LINK_PART
                + "/" + TransactionsService.XML_FILE_TYPE + "/" + xmlFileIndex;

        File dir = new File(xmlPath);
        if (!dir.exists())
            dir.mkdirs();

        when(
                service.generateTransactionsFilePath(
                        eq(TransactionsService.TRANSACTIONS_FILE_XML),
                        eq(TransactionsService.XML_FILE_TYPE))).thenReturn(
                xmlFilePath);
        when(service.getTransactionsFileIndex(eq(xmlFilePath))).thenReturn(
                xmlFileIndex);
        when(
                service.getTransactionsFileLink(
                        eq(TransactionsController.URL_TRANSACTIONS_REFUND_LINK_PART),
                        eq(TransactionsService.XML_FILE_TYPE), eq(xmlFileIndex)))
                .thenReturn(xmlFileLink);
        when(
                messageSource.getMessage(
                        eq("msg.transactionsUnloadSuccessful"),
                        eq((Object[]) null), eq((Locale) null))).thenReturn(
                messageGenerationSuccessfull);

        when(service.getRefundTransactions(eq(dateFrom), eq(dateTo)))
                .thenReturn(
                        Arrays.asList(new Refund(), new Refund(), new Refund()));

        Map<String, ?> result = controller.generateRefundTransactions(dateFrom,
                dateTo, TransactionsController.XML_FILE_TYPE_VALUE);

        verify(service).generateTransactionsFilePath(
                eq(TransactionsService.TRANSACTIONS_FILE_XML),
                eq(TransactionsService.XML_FILE_TYPE));
        verify(service).getTransactionsFileIndex(eq(xmlFilePath));
        verify(service).getTransactionsFileLink(
                eq(TransactionsController.URL_TRANSACTIONS_REFUND_LINK_PART),
                eq(TransactionsService.XML_FILE_TYPE), eq(xmlFileIndex));
        verify(messageSource).getMessage(
                eq("msg.transactionsUnloadSuccessful"),
                eq((Object[]) null), eq((Locale) null));

        verify(service).getRefundTransactions(eq(dateFrom), eq(dateTo));
        verify(service, times(3)).writeXmlData(any(XMLStreamWriter.class),
                any(Refund.class), eq(formatter));

        assertTrue((Boolean) result.get(AjaxUtil.JSON_SUCCESS));
        assertEquals(xmlFileLink, result.get(AjaxUtil.JSON_DATA));
        assertEquals(messageGenerationSuccessfull,
                result.get(AjaxUtil.JSON_MESSAGE));
    }

    @Test
    public void testDownloadRefundTransactions() throws Exception {

        String csvFileType = TransactionsService.CSV_FILE_TYPE;
        String csvFileIndex = "0";
        String csvFilePath = System.getProperty("user.home")
                + "/transactions_test/" + csvFileType + "/"
                + TransactionsService.TRANSACTIONS_FILE_CSV + "."
                + csvFileIndex;

        String xmlFileType = TransactionsService.XML_FILE_TYPE;
        String xmlFileIndex = "0";
        String xmlFilePath = System.getProperty("user.home")
                + "/transactions_test/" + xmlFileType + "/"
                + TransactionsService.TRANSACTIONS_FILE_XML + "."
                + xmlFileIndex;

        HttpServletResponse response = new MockHttpServletResponse();

        // check csv
        when(service.getTransactionsFilePath(eq(csvFileType),
                eq(csvFileIndex))).thenReturn(csvFilePath);

        controller.downloadRefundTransactions(csvFileType, csvFileIndex,
                response);

        verify(service).getTransactionsFilePath(eq(csvFileType),
                eq(csvFileIndex));

        assertEquals("text/csv", response.getContentType());

        // check xml
        when(service.getTransactionsFilePath(eq(xmlFileType),
                eq(xmlFileIndex))).thenReturn(xmlFilePath);

        controller.downloadRefundTransactions(xmlFileType, xmlFileIndex,
                response);

        verify(service).getTransactionsFilePath(eq(xmlFileType),
                eq(xmlFileIndex));

        assertEquals("text/xml", response.getContentType());
    }

    @Test
    public void testGetRefundTransactionsXML() throws Exception {

        HttpServletResponse response = mock(HttpServletResponse.class);

        when(appSettingService.getApiTransactionsToken()).thenReturn("token");
        when(dateTimeService.createDefaultDateTime()).thenReturn(new DateTime());
        // Correct Date In Rigth Format
        String xmlResp = controller.getRefundTransactionsXML("201306", "201306", "token", response);
        verify(response, never()).sendError(anyInt(), anyString());
        assertNotNull(xmlResp);
        assertTrue(!xmlResp.equals(""));
        assertTrue(xmlResp.startsWith("<?xml version="));

        // Correct Empty Date
        xmlResp = controller.getRefundTransactionsXML("", "", "token", response);

        assertNotNull(xmlResp);
        assertTrue(!xmlResp.equals(""));
        assertTrue(xmlResp.startsWith("<?xml version="));

        verify(appSettingService, atLeastOnce()).getApiTransactionsToken();
        verify(response, atLeastOnce()).setContentType("application/xml; charset=UTF-8");
        verify(response, never()).sendError(anyInt(), anyString());

        // Wrong token
        xmlResp = controller.getRefundTransactionsXML("201306", "201306", "token1", response);
        verify(response).sendError(eq(403), anyString());
        assertEquals("", xmlResp);

        // Wrong date format
        xmlResp = controller.getRefundTransactionsXML("AAAA", "AAAAA", "token", response);
        verify(response, atLeastOnce()).sendError(eq(400), anyString());
        assertEquals("", xmlResp);
    }
    
    @Test(expected=OlekstraException.class)
    public void reexportTransactionsInvalidDateFindTest() throws OlekstraException{
    	controller.reexportTransactionsFind("12.31.2014", "11");
    }
    
    @Test(expected=OlekstraException.class)
    public void reexportTransactionsInvalidGroupFindTest() throws OlekstraException{
    	controller.reexportTransactionsFind("11.25.2014", "111");
    }
    
    @Test
    public void reexportTransactionsFindTest() throws OlekstraException{
    	controller.reexportTransactionsFind("25.11.2013", "11");
    	verify(dynamodbService, times(10)).getObjectsAllOrDie(eq(TransactionTotalInfo.class), any(String[].class), any(String[].class));
    }
    
    @Test
    public void reexportTransactionsResendTest() throws OlekstraException, JsonParseException, JsonMappingException, ParserConfigurationException, IOException, TransformerException, MessageSendException{
    	String dateFrom = "25112013";
    	
    	TransactionExportInfo te = new TransactionExportInfo();
    	List<TransactionExportInfo> tel = new ArrayList<TransactionExportInfo>();
    	tel.add(te);
    	
    	when(dynamodbService.getItemsByRangeKeyBegin(eq(TransactionExportInfo.class), anyString(), eq(dateFrom))).thenReturn(tel);
    	
    	ProcessingTransaction pe = new ProcessingTransaction(); 
    	List<ProcessingTransaction> pel = new ArrayList<ProcessingTransaction>();
    	pel.add(pe);
    	when(dynamodbService.getObjectsAllOrDie(eq(ProcessingTransaction.class), any(String[].class), any(String[].class))).thenReturn(pel);

    	IMessage completeMessage = new Message();
    	
    	when(xmlMessageService.createTransactionCompleteMessage(eq(pe))).thenReturn(completeMessage);
    	
    	controller.reexportTransactionsResend(dateFrom, "11");
    	
    	verify(sender).send(eq(completeMessage));
    }    
    
    
}
