package ru.olekstra.backoffice.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.backoffice.util.AjaxUtil;
import ru.olekstra.domain.User;
import ru.olekstra.service.UserService;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {

    private UserService service;
    private MessageSource messageSource;
    private UserController controller;

    @Before
    public void setup() {
        service = mock(UserService.class);
        messageSource = mock(MessageSource.class);
        controller = new UserController(service, messageSource);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void ViewUsers() throws IOException {

        User user1 = new User("user1@test.com");
        User user2 = new User("user2@test.com");
        User user3 = new User("test1@test.com");
        User user4 = new User("abcd@test.com");

        List<User> users = Arrays.asList(user1, user2, user3, user4);
        when(service.getAllUsers()).thenReturn(users);

        ModelAndView result = controller.viewUsers(new ModelMap());

        verify(service).getAllUsers();

        assertNotNull(result);
        // Пользователи должны быть в алфавитном порядке
        assertTrue(((List<User>) result.getModel().get("users")).get(0).equals(
                user4));
        assertTrue(((List<User>) result.getModel().get("users")).get(1).equals(
                user3));
        assertTrue(((List<User>) result.getModel().get("users")).get(2).equals(
                user1));
        assertTrue(((List<User>) result.getModel().get("users")).get(3).equals(
                user2));
        assertFalse(((List<User>) result.getModel().get("users"))
                .contains(new User("anotheruser@test.com")));
    }

    @Test
    public void addUser() {

        ModelAndView result = controller.addUser();
        assertEquals("userEdit", result.getViewName());
    }

    @Test
    public void editUser() {

        String id = "userId";
        User user = new User(id);

        when(service.getUser(eq(id))).thenReturn(user);

        ModelAndView result = controller.editUser(new ModelMap(), id);

        verify(service).getUser(eq(id));

        assertEquals("userEdit", result.getViewName());
        assertEquals(user, result.getModel().get("user"));
    }

    @Test
    public void saveUser() throws ItemSizeLimitExceededException {

        String messageSuccess = "success message";
        String id = "test@mail.com";
        boolean roleCardView = true;
        boolean roleCardGeneral = true;
        boolean roleCardBalance = true;
        boolean roleDrugstoreView = true;
        boolean roleDrugstoreEdit = true;
        boolean roleDrugstoreReports = true;
        boolean roleManufacturerReports = true;
        boolean roleHelpdesk = true;
        boolean roleTransactionsExport = true;
        boolean roleAdministration = true;
        boolean roleRecipeReceive = true;
        boolean roleRecipeParse = true;
        boolean roleRecipeVerify = true;
        boolean roleRecipeProblem = true;

        when(service.saveUser(any(User.class))).thenReturn(true);
        when(
                messageSource.getMessage(eq("msg.saved"), eq((Object[]) null),
                        eq((Locale) null))).thenReturn(messageSuccess);

        Map<String, Object> result = controller.saveUser(id, roleCardView,
                roleCardGeneral, roleCardBalance, roleDrugstoreView,
                roleDrugstoreEdit, roleDrugstoreReports,
                roleManufacturerReports, roleHelpdesk, roleTransactionsExport,
                roleAdministration, roleRecipeReceive, roleRecipeParse,
                roleRecipeVerify, roleRecipeProblem);

        verify(service).saveUser(any(User.class));
        verify(messageSource).getMessage(eq("msg.saved"), eq((Object[]) null),
                eq((Locale) null));

        assertEquals(true, result.get(AjaxUtil.JSON_SUCCESS));
        assertEquals(messageSuccess, result.get(AjaxUtil.JSON_DATA));
    }

    @Test
    public void saveUserErrors() throws ItemSizeLimitExceededException {

        String id = "test@mail.com";
        boolean roleCardView = true;
        boolean roleCardGeneral = true;
        boolean roleCardBalance = true;
        boolean roleDrugstoreView = true;
        boolean roleDrugstoreEdit = true;
        boolean roleDrugstoreReports = true;
        boolean roleManufacturerReports = true;
        boolean roleHelpdesk = true;
        boolean roleTransactionsExport = true;
        boolean roleAdministration = true;
        boolean roleRecipeReceive = true;
        boolean roleRecipeParse = true;
        boolean roleRecipeVerify = true;
        boolean roleRecipeProblem = true;

        String messageEmailIncorrect = "message email incorrect";
        when(
                messageSource.getMessage(eq("msg.userMailIncorrect"),
                        eq((Object[]) null), eq((Locale) null))).thenReturn(
                messageEmailIncorrect);

        Map<String, Object> result = controller.saveUser("any incorrect email",
                roleCardView, roleCardGeneral, roleCardBalance,
                roleDrugstoreView, roleDrugstoreEdit, roleDrugstoreReports,
                roleManufacturerReports, roleHelpdesk, roleTransactionsExport,
                roleAdministration, roleRecipeReceive, roleRecipeParse,
                roleRecipeVerify, roleRecipeProblem);

        verify(messageSource).getMessage(eq("msg.userMailIncorrect"),
                eq((Object[]) null), eq((Locale) null));

        assertEquals(false, result.get(AjaxUtil.JSON_SUCCESS));
        assertEquals(messageEmailIncorrect, result.get(AjaxUtil.JSON_MESSAGE));

        String messageFailure = "message failure";
        when(service.saveUser(any(User.class))).thenReturn(false);
        when(
                messageSource.getMessage(eq("msg.saveError"),
                        eq((Object[]) null), eq((Locale) null))).thenReturn(
                messageFailure);

        result = controller.saveUser(id, roleCardView, roleCardGeneral,
                roleCardBalance, roleDrugstoreView, roleDrugstoreEdit,
                roleDrugstoreReports, roleManufacturerReports, roleHelpdesk,
                roleTransactionsExport, roleAdministration, roleRecipeReceive,
                roleRecipeParse, roleRecipeVerify, roleRecipeProblem);

        verify(service).saveUser(any(User.class));
        verify(messageSource).getMessage(eq("msg.saveError"),
                eq((Object[]) null), eq((Locale) null));

        assertEquals(false, result.get(AjaxUtil.JSON_SUCCESS));
        assertEquals(messageFailure, result.get(AjaxUtil.JSON_MESSAGE));

        String exceptionMessage = "exception message";
        when(service.saveUser(any(User.class))).thenThrow(
                new ItemSizeLimitExceededException(exceptionMessage));

        result = controller.saveUser(id, roleCardView, roleCardGeneral,
                roleCardBalance, roleDrugstoreView, roleDrugstoreEdit,
                roleDrugstoreReports, roleManufacturerReports, roleHelpdesk,
                roleTransactionsExport, roleAdministration, roleRecipeReceive,
                roleRecipeVerify, roleRecipeVerify, roleRecipeProblem);

        verify(service, times(2)).saveUser(any(User.class));

        assertEquals(false, result.get(AjaxUtil.JSON_SUCCESS));
        assertEquals(exceptionMessage, result.get(AjaxUtil.JSON_MESSAGE));
    }

    @Test
    public void deleteUser() {

        String id = "userid";
        Map<String, Object> result = controller.deleteUser(id);
        assertEquals(id, result.get("data"));
    }
}
