package ru.olekstra.backoffice.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.ModelAndViewAssert.assertModelAttributeAvailable;
import static org.springframework.test.web.ModelAndViewAssert.assertViewName;

import java.security.Principal;

import javax.servlet.ServletContext;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.servlet.HandlerAdapter;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.SqsService;
import ru.olekstra.common.dao.DrugstoreDao;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.common.service.DateTimeService;
import ru.olekstra.domain.Ticket;
import ru.olekstra.service.CardService;
import ru.olekstra.service.DrugstoreService;
import ru.olekstra.service.TicketService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class HelpDeskControllerTest {
    @Autowired
    private static DateTimeService dateService;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private ServletContext servletContext;

    @Autowired
    private HelpDeskController controller;

    // @Autowired
    // private SqsService sqsService;

    @Autowired
    private static TicketService ticketService;

    @Autowired
    private HandlerAdapter handlerAdapter;

    private Principal principal;
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;

    @Before
    public void setUp() {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        principal = mock(Principal.class);
        handlerAdapter = applicationContext
                .getBean(AnnotationMethodHandlerAdapter.class);
    }

    @Test
    public void testHelpdeskShow() throws Exception {
        request.setRequestURI("/helpdesk");
        request.setMethod("GET");
        ModelAndView mav = handlerAdapter.handle(request, response,
                controller);

        assertViewName(mav, "helpdesk");
    }

    @Test
    public void testTicketsNew() throws Exception {
        request.setRequestURI("/helpdesk/new");
        request.setMethod("GET");
        ModelAndView mav = handlerAdapter.handle(request, response,
                controller);

        assertViewName(mav, "tickets");
        assertModelAttributeAvailable(mav, "ticketViews");
        assertModelAttributeAvailable(mav, "show");
        assertModelAttributeAvailable(mav, "periodFormatter");
    }

    @Test
    public void testShowTicketsPersonal() throws Exception {
        request.setRequestURI("/helpdesk/processing/personal");
        request.setUserPrincipal(principal);
        request.setMethod("GET");

        when(principal.getName()).thenReturn("user0");
        ModelAndView mav = handlerAdapter.handle(request, response,
                controller);

        assertViewName(mav, "tickets");
        assertModelAttributeAvailable(mav, "ticketViews");
        assertModelAttributeAvailable(mav, "show");
        assertModelAttributeAvailable(mav, "periodFormatter");
    }

    @Test
    public void testShowTicketsAll() throws Exception {
        request.setRequestURI("/helpdesk/processing/all");
        request.setUserPrincipal(principal);
        request.setMethod("GET");

        when(principal.getName()).thenReturn("user0");
        ModelAndView mav = handlerAdapter.handle(request, response,
                controller);

        assertViewName(mav, "tickets");
        assertModelAttributeAvailable(mav, "ticketViews");
        assertModelAttributeAvailable(mav, "show");
        assertModelAttributeAvailable(mav, "periodFormatter");
    }

    @Test
    public void testEditTicket() throws Exception {
        request.setRequestURI("/helpdesk/ticket");
        request.setParameter("ticketId", "ticketId");
        request.setParameter("period", "period");
        request.setParameter("type", "type");
        request.setParameter("message", "message");
        request.setUserPrincipal(principal);
        request.setMethod("GET");

        when(principal.getName()).thenReturn("user0");

        Ticket ticket = new Ticket();
        ticket.setReversal(false);

        when(ticketService.getTicket(any(String.class), any(String.class)))
                .thenReturn(ticket);

        ModelAndView mav = handlerAdapter.handle(request, response,
                controller);

        assertViewName(mav, "ticketView");
    }

    @Configuration
    static class TicketControllerTestConfig {

        @Bean
        public AppSettingsService settingService() {
            return mock(AppSettingsService.class);
        }

        @Bean
        public DrugstoreDao drugstoreDao() {
            return mock(DrugstoreDao.class);
        }

        @Bean
        public DateTimeService dateService() {
            DateTimeService dateService = mock(DateTimeService.class);
            when(dateService.createDateTimeWithZone(anyString())).thenReturn(DateTime.now());
            when(dateService.createDefaultDateTime()).thenReturn(DateTime.now());

            return dateService;
        }

        @Bean
        public ServletContext servletContext() {
            return new MockServletContext();
        }

        @Bean
        public HandlerAdapter handlerAdapter() {
            return new AnnotationMethodHandlerAdapter();
        }

        @Bean
        public SqsService sqsService() {
            return mock(SqsService.class);
        }

        @Bean
        public JmsTemplate jmsTemplate() {
            return mock(JmsTemplate.class);
        }

        @Bean
        public HelpDeskController controller() {
            ticketService = mock(TicketService.class);
            MessageSource messageSource = mock(MessageSource.class);
            AppSettingsService settingsService = mock(AppSettingsService.class);
            CardService cardService = mock(CardService.class);
            DrugstoreService drugstoreService = mock(DrugstoreService.class);
            DynamodbService dybamodbService = mock(DynamodbService.class);
            return new HelpDeskController(ticketService, settingsService,
                    cardService, drugstoreService,
                    messageSource, dateService, dybamodbService);
        }
    }
}
