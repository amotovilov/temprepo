package ru.olekstra.backoffice.controller;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.backoffice.util.AjaxUtil;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.common.service.PlanService;
import ru.olekstra.domain.DiscountPlan;
import ru.olekstra.domain.dto.DiscountPlanForm;
import ru.olekstra.exception.InvalidFormatDataException;

@RunWith(MockitoJUnitRunner.class)
public class PlanControllerTest {

    private PlanService service;
    private PlanController controller;
    private MessageSource messageSource;
    private AppSettingsService appSettingService;

    @Before
    public void setup() {
        service = mock(PlanService.class);
        messageSource = mock(MessageSource.class);
        appSettingService = mock(AppSettingsService.class);
        controller = new PlanController(service, messageSource, appSettingService);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testSavePlan() throws InvalidFormatDataException, IOException, ItemSizeLimitExceededException {

        String id = "id";
        String name = "name";
        Integer maxDiscount = 50;
        String description = "";
        String detailsLink = "http://link";
        String discounts = "question1,question2,question3";
        BigDecimal selfActivationBonus = BigDecimal.ONE;

        DiscountPlanForm form = new DiscountPlanForm(id, name, maxDiscount,
                description, detailsLink,
                discounts, true, selfActivationBonus);

        BindingResult bindingResult = new BeanPropertyBindingResult(
                new Boolean(false), "hasErrors");

        when(service.hasPlan(id)).thenReturn(false);
        when(
                service.savePlan(any(DiscountPlanForm.class),
                        (List<String>) anyObject()))
                .thenReturn(true);

        Map<String, Object> response = controller.saveDiscountPlan(form,
                bindingResult);

        assertTrue(((Boolean) response.get(AjaxUtil.JSON_SUCCESS))
                .booleanValue());

        verify(service).hasPlan(id);

    }

    @Test
    public void testSaveNewPlanWithExistingId()
            throws InvalidFormatDataException, IOException {

        String id = "id";
        String name = "name";
        Integer maxDiscount = 50;
        String description = "descr";
        String detailsLink = "link";
        String discounts = "question1,question2,question3";
        BigDecimal selfActivationBonus = BigDecimal.ONE;

        DiscountPlanForm form = new DiscountPlanForm(id, name, maxDiscount,
                description, detailsLink,
                discounts, true, selfActivationBonus);
        BindingResult bindingResult = new BeanPropertyBindingResult(
                new Boolean(false), "hasErrors");

        when(service.hasPlan(id)).thenReturn(true);

        Map<String, Object> response = controller.saveDiscountPlan(form,
                bindingResult);

        assertFalse(((Boolean) response.get(AjaxUtil.JSON_SUCCESS))
                .booleanValue());
        verify(service).hasPlan(id);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void ViewDiscountPlans() throws JsonGenerationException,
            JsonMappingException, IOException {

        DiscountPlan plan1 = new DiscountPlan("testPlan1", "PlanName", 0);
        DiscountPlan plan2 = new DiscountPlan("abcdPlan", "PlanName", 0);
        DiscountPlan plan3 = new DiscountPlan("testPlan2", "PlanName", 0);
        DiscountPlan plan4 = new DiscountPlan("urstPlan", "PlanName", 0);

        List<DiscountPlan> discountPlan = Arrays.asList(plan1, plan2, plan3,
                plan4);
        when(service.getAllPlans()).thenReturn(discountPlan);

        ModelAndView result = controller.discountPlans(new ModelMap());

        verify(service).getAllPlans();

        assertNotNull(result);
        // Пользователи должны быть в алфавитном порядке
        assertTrue(((List<DiscountPlan>) result.getModel().get("discountPlans"))
                .get(0).equals(plan2));
        assertTrue(((List<DiscountPlan>) result.getModel().get("discountPlans"))
                .get(1).equals(plan1));
        assertTrue(((List<DiscountPlan>) result.getModel().get("discountPlans"))
                .get(2).equals(plan3));
        assertTrue(((List<DiscountPlan>) result.getModel().get("discountPlans"))
                .get(3).equals(plan4));
    }
}
