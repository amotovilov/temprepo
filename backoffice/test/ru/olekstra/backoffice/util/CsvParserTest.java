package ru.olekstra.backoffice.util;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class CsvParserTest {

    @Test
    public void testParseSpecifiedNumberOfFields() {

        String value1 = "Hello;World";
        String value2 = "Hello;\"World\"";
        String value3 = "Hello;\"World; Universe\"";
        String value4 = "\"Hello\";\"World; \"\"Universe\"\"; Everything\"";

        String[] result1 = CsvParser.readFields(value1, CsvParser.SEMICOLON);
        String[] result2 = CsvParser.readFields(value2, CsvParser.SEMICOLON);
        String[] result3 = CsvParser.readFields(value3, CsvParser.SEMICOLON);
        String[] result4 = CsvParser.readFields(value4, CsvParser.SEMICOLON);

        // Во всех случаях должно получиться 2 поля
        assertEquals(2, result1.length);
        assertEquals(2, result2.length);
        assertEquals(2, result3.length);
        assertEquals(2, result4.length);
    }

    @Test
    public void testParseValues() {

        String[] data = new String[] {
                "Hello",
                "Hello;World",
                "Hello;World;",
                "Hello;World;;;",
                "Hello;World;;;!;;",
                "He;said;\"Good bye\"",
                "He;said;\"Good bye; baby\"",
                "Hello delimiter",
                "\"Say me \"\"Hello\"\" and go\";my;darling",
                "\"Hello\"",
                "\"\"",
                "\"\";\"\";\"\";;",
                "\"\"\"\"\"\"",
        };

        List<String[]> results = new ArrayList<String[]>();
        for (String value : data) {
            results.add(CsvParser.readFields(value, CsvParser.SEMICOLON));
        }

        // Проверка правильности интерпретации распарсенных полей
        assertArrayEquals(new String[] {"Hello"}, results.get(0));
        assertArrayEquals(new String[] {"Hello", "World"}, results.get(1));
        assertArrayEquals(new String[] {"Hello", "World", ""},
                results.get(2));
        assertArrayEquals(new String[] {"Hello", "World", "", "", ""},
                results.get(3));
        assertArrayEquals(new String[] {"Hello", "World", "", "", "!", "", ""},
                results.get(4));
        assertArrayEquals(new String[] {"He", "said", "Good bye"},
                results.get(5));
        assertArrayEquals(new String[] {"He", "said", "Good bye; baby"},
                results.get(6));
        assertArrayEquals(new String[] {"Hello delimiter"}, results.get(7));
        assertArrayEquals(new String[] {"Say me \"Hello\" and go", "my",
                "darling"}, results.get(8));
        assertArrayEquals(new String[] {"Hello"}, results.get(9));
        assertArrayEquals(new String[] {""}, results.get(10));
        assertArrayEquals(new String[] {"", "", "", "", ""}, results.get(11));
        assertArrayEquals(new String[] {"\"\""}, results.get(12));
    }
}
