package ru.olekstra.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;

import ru.olekstra.awsutils.DynamodbEntity;
import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.common.dao.CardOperationDao;
import ru.olekstra.common.helper.CardHelper;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.common.service.CardLogService;
import ru.olekstra.common.service.DateTimeService;
import ru.olekstra.common.service.SmsService;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.CardLog;
import ru.olekstra.domain.CardOperation;
import ru.olekstra.domain.dto.CardLogRecord;
import ru.olekstra.domain.dto.CardLogType;
import ru.olekstra.domain.dto.LogRecord;
import ru.olekstra.exception.NotFoundException;

@RunWith(MockitoJUnitRunner.class)
public class CardOperationServiceTest {

    private CardOperationService service;
    private DynamodbService dbService;
    private CardOperationDao operationDao;
    private SmsService smsService;
    private CardHelper cardHelper;
    private MessageSource messageSource;
    private SecurityService securityService;
    private AppSettingsService appSettingService;
    private CardLogService cardLogService;
    private DateTimeService dateService;

    @Before
    public void setup() {
        dbService = mock(DynamodbService.class);
        operationDao = mock(CardOperationDao.class);
        smsService = mock(SmsService.class);
        cardHelper = mock(CardHelper.class);
        messageSource = mock(MessageSource.class);
        securityService = mock(SecurityService.class);
        appSettingService = mock(AppSettingsService.class);
        cardLogService = mock(CardLogService.class);
        dateService = mock(DateTimeService.class);
        when(dateService.createDateTimeWithZone(anyString())).thenReturn(DateTime.now());
        when(dateService.createDefaultDateTime()).thenReturn(DateTime.now());

        service = new CardOperationService(dbService, operationDao, smsService,
                cardHelper, messageSource, securityService, cardLogService,
                dateService);
    }

    @Test
    public void testSaveTrasaction() throws JsonGenerationException,
            JsonMappingException, IOException, NotFoundException,
            ItemSizeLimitExceededException {

        DateTimeFormatter dateTimeFormatter = DateTimeFormat
                .forPattern(DynamodbEntity.DB_DATETIME_FORMAT);
        DateTime dateTime = DateTime.now();
        String clientPackName = "pack1";
        int quantity = 2;
        BigDecimal sumToPay = new BigDecimal("12.00");
        String cardNumber = "number";
        String drugstoreId = "dragstoreId";
        String phone = "799911112233";
        String transactionRangeKey = "uuid#row";
        String user = "currentUser";
        int rowNum = 0;
        BigDecimal newLimit = new BigDecimal(20);
        String note = "note";

        Card card = new Card(cardNumber);
        card.setTelephoneNumber(phone);

        when(messageSource.getMessage("label.helpdesk.ticket.recharge.message",
                new Object[] {
                        dateTimeFormatter.print(dateTime),
                        clientPackName, quantity,
                        sumToPay.toString()
                },
                null))
                .thenReturn(note);
        when(securityService.getCurrentUser()).thenReturn(user);

        service.saveBackupTransactionRecord(dateTime, drugstoreId, quantity,
                sumToPay, cardNumber, clientPackName,
                new BigDecimal(1), transactionRangeKey, rowNum, newLimit);

        verify(securityService).getCurrentUser();
    }

    @Test
    public void testSaveLimit() throws JsonGenerationException,
            JsonMappingException, IOException, NotFoundException,
            ItemSizeLimitExceededException {

        String cardNumber = "number";
        String drugstoreId = "dragstoreId";
        String phone = "799911112233";
        BigDecimal limit = new BigDecimal(20);
        BigDecimal discountSum = new BigDecimal(15);

        Card card = new Card(cardNumber);
        card.setLimitRemain(limit);
        card.setTelephoneNumber(phone);

        when(dbService.getObject(Card.class, cardNumber)).thenReturn(card);
        when(
                cardHelper.updateCardLimit(eq(cardNumber),
                        (BigDecimal) anyObject(), (BigDecimal) anyObject()))
                .thenReturn(true);
        BigDecimal newLimit = service.saveBackupLimitRecord(drugstoreId,
                cardNumber, discountSum);

        assertTrue(newLimit.equals(limit.add(discountSum)));

        verify(dbService).getObject(Card.class, cardNumber);
        verify(cardHelper).updateCardLimit(eq(cardNumber),
                (BigDecimal) anyObject(), (BigDecimal) anyObject());
    }

    @Test
    public void testSaveZeroBackupLimit() throws JsonGenerationException,
            JsonMappingException, IOException, NotFoundException,
            ItemSizeLimitExceededException {

        String cardNumber = "number";
        String drugstoreId = "dragstoreId";
        String phone = "799911112233";
        BigDecimal limit = new BigDecimal(20);
        BigDecimal discountSum = new BigDecimal("0.00");

        Card card = new Card(cardNumber);
        card.setLimitRemain(limit);
        card.setTelephoneNumber(phone);

        when(dbService.getObject(Card.class, cardNumber)).thenReturn(card);
        when(cardHelper.updateCardLimit(eq(cardNumber),
                (BigDecimal) anyObject(), (BigDecimal) anyObject()))
                .thenReturn(true);
        when(cardLogService.createLimitCardLog(any(String.class), any(String.class), any(String.class),
                any(DateTime.class)))
                .thenReturn(new CardLog());
        BigDecimal newLimit = service.saveBackupLimitRecord(drugstoreId,
                cardNumber, discountSum);

        assertTrue(newLimit.equals(limit.add(discountSum)));

        verify(cardLogService).createLimitCardLog(eq(cardNumber), eq(drugstoreId), any(String.class),
                any(DateTime.class));

        verify(dbService).getObject(Card.class, cardNumber);
        verify(cardHelper).updateCardLimit(eq(cardNumber),
                (BigDecimal) anyObject(), (BigDecimal) anyObject());
    }

    @Test
    public void testChangePlanRecord() throws JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException {

        String user = "user";
        String cardNumber = "777";
        String planId = "planId";
        String message = "test messsage";

        when(
                messageSource.getMessage(eq("msg.cardPlanChangeWithOutReason"),
                        eq(new Object[] {cardNumber}), (Locale) eq(null)))
                .thenReturn(message);
        when(securityService.getCurrentUser()).thenReturn(user);

        service.saveChangePlanRecord(cardNumber, planId);

        verify(securityService).getCurrentUser();
        verify(messageSource).getMessage(eq("msg.cardPlanChangeWithOutReason"),
                eq(new Object[] {planId}), (Locale) eq(null));
    }

    @Test
    public void testActivateRecord() throws JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException,
            RuntimeException, InterruptedException {

        String user = "user";
        String reason = "reason";
        String cardNumber1 = "777";
        String cardNumber2 = "888";
        String phoneNumber = "1234567";
        String holderName = "Name";
        String message = "test messsage";
        String discountPlan = "discountPlan";

        Card card1 = new Card(cardNumber1, false, holderName, phoneNumber);
        card1.setDiscountPlan(discountPlan);
        Card card2 = new Card(cardNumber2, false, holderName, phoneNumber);
        card2.setDiscountPlan(discountPlan);

        List<Card> cards = new ArrayList<Card>();
        cards.add(card1);
        cards.add(card2);

        when(
                messageSource.getMessage(eq("msg.service.activation"),
                        eq(new Object[] {holderName, phoneNumber, discountPlan,
                                reason}), (Locale) eq(null))).thenReturn(
                message);
        when(
                messageSource.getMessage(eq("msg.public.activation"),
                        eq(new Object[] {cardNumber1}), (Locale) eq(null)))
                .thenReturn(message);
        when(
                messageSource.getMessage(eq("msg.public.activation"),
                        eq(new Object[] {cardNumber2}), (Locale) eq(null)))
                .thenReturn(message);
        when(securityService.getCurrentUser()).thenReturn(user);

        service.saveCardActivateRecords(cards, reason);

        verify(securityService).getCurrentUser();
        verify(messageSource, times(2))
                .getMessage(
                        eq("msg.service.activation"),
                        eq(new Object[] {holderName, phoneNumber, discountPlan,
                                reason}), (Locale) eq(null));
        verify(messageSource).getMessage(eq("msg.public.activation"),
                eq(new Object[] {cardNumber1}), (Locale) eq(null));
        verify(messageSource).getMessage(eq("msg.public.activation"),
                eq(new Object[] {cardNumber2}), (Locale) eq(null));
    }

    @Test
    public void testChangeDateRecord() throws JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException,
            RuntimeException, InterruptedException {

        String cardNumber1 = "777";
        String cardNumber2 = "555";
        String cardNumber3 = "666";
        String reason = "reason";

        List<String> cardList = new ArrayList<String>();
        cardList.add(cardNumber1);
        cardList.add(cardNumber2);
        cardList.add(cardNumber3);

        String message = "test messsage";

        DateTime dateNow = new DateTime();
        DateTime dateEnd = DateTime.now().plusDays(5);

        String fromDate = dateNow.getDayOfMonth() + "."
                + dateNow.getMonthOfYear() + "." + dateNow.getYear()
                + " 00:00:01";
        String toDate = dateEnd.getDayOfMonth() + "."
                + dateEnd.getMonthOfYear() + "." + dateEnd.getYear()
                + " 23:59:59";

        when(
                messageSource.getMessage(eq("msg.cardDateChange"),
                        eq(new Object[] {fromDate, toDate}), (Locale) eq(null)))
                .thenReturn(message);
        when(cardLogService.createCardLog(any(String.class), eq(message),
                (String) isNull(), any(DateTime.class), eq(CardLogType.CHANGE_DATE), eq(false)))
                .thenReturn(new CardLog());
        service.saveChangeDateRecords(cardList, fromDate, toDate, reason);

        verify(messageSource, times(3)).getMessage(eq("msg.cardDateChange"),
                eq(new Object[] {fromDate, toDate}), (Locale) eq(null));
    }

    @Test
    public void testChangePlanRecords() throws JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException,
            RuntimeException, InterruptedException {

        String user = "user";
        List<String> cardNumbers = new ArrayList<String>();
        cardNumbers.add("7771");
        cardNumbers.add("7772");
        String planId = "planId";
        String message = "test messsage";
        String reason = "reason";

        when(
                messageSource.getMessage(eq("msg.cardPlanChange"),
                        any(Object[].class), (Locale) eq(null))).thenReturn(
                message);
        when(securityService.getCurrentUser()).thenReturn(user);
        when(cardLogService.createChangePlanCardLog(any(String.class), any(String.class),
                any(Boolean.class), any(String.class))).thenReturn(new CardLog());

        service.saveChangePlanRecords(cardNumbers, planId, reason);

        verify(securityService, times(1)).getCurrentUser();
        verify(messageSource, times(2)).getMessage(eq("msg.cardPlanChange"),
                any(Object[].class), (Locale) eq(null));
    }

    @Ignore
    @Test
    public void testGetOperationOnCard() throws JsonParseException,
            JsonMappingException, IOException {
        String currentId = "cardId";
        String drugstoreId = "drugstoreId";
        String status = "true";
        String period = "201204";
        String record = "{\"date\":1333416571789,\"drugstoreId\":\"drugstoreId\",\"note\":true}";

        List<LogRecord> records = new ArrayList<LogRecord>();
        records.add((LogRecord) DynamodbEntity.fromJson(record,
                LogRecord.class));

        List<String> attributesToGet = new ArrayList<String>();
        attributesToGet.add(CardOperation.FLD_NEXT_ID);
        attributesToGet.add(CardOperation.FLD_LOG_RECORD);

        // when(operationDao.getLogRecords(currentId,
        // period)).thenReturn(records);

        List<CardLogRecord> resultList = service.getLogRecordsOnCardStat(
                currentId, period);

        verify(operationDao).getLogRecords(currentId, period);

        assertNotNull(resultList);
        assertEquals(1, resultList.size());
        assertEquals(drugstoreId, ((CardLogRecord) resultList.get(0))
                .getDrugstoreName());
        assertEquals(status, ((CardLogRecord) resultList.get(0)).getNote());
    }

    @Ignore
    @Test
    public void testGetOperationsOnCard() throws JsonParseException,
            JsonMappingException, IOException {
        String currentId = "cardId";
        String id = "testId";
        String status = "true";
        String period = "201204";

        String record1 = "{\"date\":1333416571789,\"drugstoreId\":\"" + id
                + "\",\"note\":true}";
        String record2 = "{\"date\":1333416571789,\"drugstoreId\":\"" + id
                + "2\",\"note\":true}";
        String record3 = "{\"date\":1333416571789,\"drugstoreId\":\"" + id
                + "3\",\"note\":true}";

        List<LogRecord> records = new ArrayList<LogRecord>();
        records.add((LogRecord) DynamodbEntity.fromJson(record1,
                LogRecord.class));
        records.add((LogRecord) DynamodbEntity.fromJson(record2,
                LogRecord.class));
        records.add((LogRecord) DynamodbEntity.fromJson(record3,
                LogRecord.class));

        List<String> attributesToGet = new ArrayList<String>();
        attributesToGet.add(CardOperation.FLD_NEXT_ID);
        attributesToGet.add(CardOperation.FLD_LOG_RECORD);

        // when(operationDao.getLogRecords(eq(currentId),
        // eq(period))).thenReturn( records);

        List<CardLogRecord> resultList = service.getLogRecordsOnCardStat(
                currentId, period);

        verify(operationDao).getLogRecords(eq(currentId), eq(period));

        assertNotNull(resultList);
        assertEquals(3, resultList.size());

        assertEquals(id, ((CardLogRecord) resultList.get(0)).getDrugstoreName());
        assertEquals(status, ((CardLogRecord) resultList.get(0)).getNote());

        assertEquals(id + "2", ((CardLogRecord) resultList.get(1))
                .getDrugstoreName());
        assertEquals(status, ((CardLogRecord) resultList.get(1)).getNote());

        assertEquals(id + "3", ((CardLogRecord) resultList.get(2))
                .getDrugstoreName());
        assertEquals(status, ((CardLogRecord) resultList.get(2)).getNote());
    }

    @Test
    public void testChangeBatchRecord() throws JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException {

        String user = "user";
        String cardNumber = "777";
        String message = "test messsage";
        List<String> cardBatches = Arrays.asList("batch1", "batch2", "batch3");

        Card card = new Card();
        card.setNumber(cardNumber);
        card.setCardBatches(cardBatches);

        when(messageSource.getMessage(eq("msg.cardSingleBatchChange"),
                eq(new Object[] {StringUtils.join(
                        cardBatches.toArray(), ",")}),
                (Locale) eq(null))).thenReturn(message);
        when(securityService.getCurrentUser()).thenReturn(user);
        when(cardLogService.createCardLog(any(String.class), any(String.class), any(String.class),
                any(DateTime.class), eq(CardLogType.CHANGE_BATCH), eq(true)))
                .thenReturn(new CardLog());

        service.saveChangeBatchRecord(card);

        verify(securityService).getCurrentUser();
        verify(messageSource).getMessage(
                eq("msg.cardSingleBatchChange"),
                eq(new Object[] {StringUtils.join(card.getCardBatches()
                        .toArray(), ",")}), (Locale) eq(null));
    }

    @Test
    public void testChangeBatchRecords() throws JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException,
            RuntimeException, InterruptedException {

        String user = "user";
        String message = "test messsage";

        String cardNumber1 = "7771";
        String cardNumber2 = "7772";
        String reason = "reason";

        List<String> cardBatches = Arrays.asList("batch1", "batch2", "batch3");

        Card card1 = new Card();
        card1.setNumber(cardNumber1);
        card1.setCardBatches(cardBatches);

        Card card2 = new Card();
        card2.setNumber(cardNumber2);
        card2.setCardBatches(cardBatches);

        List<Card> cards = new ArrayList<Card>();
        cards.add(card1);
        cards.add(card2);

        when(messageSource.getMessage(eq("msg.cardBatchChange"),
                any(Object[].class), (Locale) eq(null))).thenReturn(
                message);
        when(securityService.getCurrentUser()).thenReturn(user);
        when(cardLogService.createCardLog(any(String.class), any(String.class), any(String.class),
                any(DateTime.class), eq(CardLogType.CHANGE_BATCH), eq(true)))
                .thenReturn(new CardLog());

        service.saveChangeBatchRecords(cards, reason);

        verify(securityService, times(1)).getCurrentUser();
        verify(messageSource, times(2)).getMessage(eq("msg.cardBatchChange"),
                any(Object[].class), (Locale) eq(null));
    }

    @Test
    public void testSaveUpdateLimitRecords() throws JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException,
            RuntimeException, InterruptedException {

        String messageForRecharge = "recharge message";
        String messageForUpdateAndSet = "update and set message";

        String messageServiceForRecharge = "recharge message service";
        String messageServiceForUpdateAndSet = "update and set message service";

        Map<String, BigDecimal> updatedLimits = new HashMap<String, BigDecimal>();
        BigDecimal newRechargeValue = new BigDecimal("15.00");
        BigDecimal limitValue = new BigDecimal("30.00");
        String cardNumber1 = "7771";
        String cardNumber2 = "7772";
        String reason = "reason";
        updatedLimits.put(cardNumber1, limitValue);
        updatedLimits.put(cardNumber2, limitValue);

        when(messageSource.getMessage(eq("msg.logrecord.limit"),
                eq(new Object[] {cardNumber1,
                        newRechargeValue.toString(),
                        limitValue.toString()}), eq((Locale) null)))
                .thenReturn(messageForRecharge);

        when(messageSource.getMessage(eq("msg.logrecord.limit"),
                eq(new Object[] {cardNumber2,
                        newRechargeValue.toString(),
                        limitValue.toString()}), eq((Locale) null)))
                .thenReturn(messageForRecharge);

        when(messageSource
                .getMessage(eq("msg.limitSet"), eq(new Object[] {
                        cardNumber1, limitValue.toString()}),
                        eq((Locale) null))).thenReturn(
                messageForUpdateAndSet);

        when(messageSource.getMessage(eq("msg.limitSet"), eq(new Object[] {
                cardNumber2, limitValue.toString()}),
                eq((Locale) null))).thenReturn(
                messageForUpdateAndSet);

        when(messageSource.getMessage(eq("msg.service.logrecord.limit"),
                eq(new Object[] {newRechargeValue.toString(),
                        limitValue.toString(), reason}),
                eq((Locale) null))).thenReturn(
                messageServiceForRecharge);

        when(messageSource.getMessage(eq("msg.service.limitSet"),
                eq(new Object[] {limitValue.toString(), reason}),
                eq((Locale) null))).thenReturn(
                messageServiceForUpdateAndSet);

        // логирование при обновлении баланса до заданного значения
        service.saveUpdateLimitRecords(updatedLimits, true,
                reason);

        verify(messageSource).getMessage(eq("msg.limitSet"),
                eq(new Object[] {cardNumber1, limitValue.toString()}),
                eq((Locale) null));

        verify(messageSource).getMessage(eq("msg.limitSet"),
                eq(new Object[] {cardNumber2, limitValue.toString()}),
                eq((Locale) null));

        verify(messageSource, times(2)).getMessage(eq("msg.service.limitSet"),
                eq(new Object[] {limitValue.toString(), reason}),
                eq((Locale) null));

        verify(cardLogService, times(2)).saveCardLog(
                anyListOf(CardLog.class));
    }

    @Test
    public void testSaveZeroRechargeLimitRecord() {

        String messageForRecharge = "recharge message";
        String messageForUpdateAndSet = "update and set message";

        String messageServiceForRecharge = "recharge message service";
        String messageServiceForUpdateAndSet = "update and set message service";

        Map<String, BigDecimal> updatedLimits = new HashMap<String, BigDecimal>();
        BigDecimal newRechargeValue = new BigDecimal("15.00");
        BigDecimal limitValue = new BigDecimal("30.00");
        String cardNumber1 = "7771";
        String cardNumber2 = "7772";
        String reason = "reason";
        updatedLimits.put(cardNumber1, limitValue);
        updatedLimits.put(cardNumber2, limitValue);
    }

    @Test
    public void testSaveChangeOwnerInfoRecord() throws JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException {

        String user = "user";
        String cardNumber = "777";
        String oldOwnerName = "test old owner name";
        String newOwnerName = "test new owner name";
        String oldOwnerPhone = "0987654321";
        String newOwnerPhone = "1234567890";
        String message = "message";

        when(
                messageSource.getMessage(eq("msg.cardOwnerInfoChange"),
                        eq(new Object[] {oldOwnerName, newOwnerName,
                                oldOwnerPhone, newOwnerPhone}),
                        eq((Locale) null))).thenReturn(message);
        when(securityService.getCurrentUser()).thenReturn(user);
        when(cardLogService.createCardLog(eq(cardNumber), any(String.class), (String) isNull(),
                any(DateTime.class), eq(CardLogType.CHANGE_OWNER_INFO), eq(true)))
                .thenReturn(new CardLog());

        service.saveChangeOwnerInfoRecord(cardNumber, oldOwnerName,
                newOwnerName, oldOwnerPhone, newOwnerPhone);

        verify(messageSource).getMessage(
                eq("msg.cardOwnerInfoChange"),
                eq(new Object[] {oldOwnerName, newOwnerName, oldOwnerPhone,
                        newOwnerPhone}), eq((Locale) null));
        verify(securityService).getCurrentUser();
        verify(cardLogService).saveCardLog(any(CardLog.class));
    }
}
