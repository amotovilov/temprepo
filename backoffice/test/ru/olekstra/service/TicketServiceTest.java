package ru.olekstra.service;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.w3c.dom.DOMException;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.SnsService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.awsutils.exception.OlekstraException;
import ru.olekstra.azure.model.Message;
import ru.olekstra.azure.service.QueueSender;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.common.service.DiscountService;
import ru.olekstra.common.service.XmlMessageService;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.Refund;
import ru.olekstra.domain.Ticket;
import ru.olekstra.domain.TicketAssigned;
import ru.olekstra.domain.TicketInfo;
import ru.olekstra.domain.TicketPost;
import ru.olekstra.domain.TicketUnassigned;
import ru.olekstra.domain.dto.PackDiscountData;
import ru.olekstra.domain.dto.VoucherResponseCard;
import ru.olekstra.domain.dto.VoucherResponseRow;

@RunWith(MockitoJUnitRunner.class)
public class TicketServiceTest {

    private DynamodbService dbService;
    private AppSettingsService settingsService;
    private CardOperationService operationService;
    private TicketService ticketService;
    private DiscountService discountService;
    private SnsService snsService;
    private MessageSource messageSource;
    private XmlMessageService xmlMessageService;
    private QueueSender sender;

    private final String FLD_EXECUTIVE = "Executive";

    @Before
    public void setup() throws ParserConfigurationException, TransformerException, JsonParseException,
            JsonMappingException, DOMException, IOException, OlekstraException {
        dbService = mock(DynamodbService.class);
        settingsService = mock(AppSettingsService.class);
        operationService = mock(CardOperationService.class);
        discountService = mock(DiscountService.class);
        xmlMessageService = mock(XmlMessageService.class);
        when(xmlMessageService.createTransactionCompleteMessage(any(ProcessingTransaction.class)))
                .thenReturn(new Message());
        snsService = mock(SnsService.class);
        messageSource = mock(MessageSource.class);
        sender = mock(QueueSender.class);

        ticketService = new TicketService(dbService, settingsService,
                operationService, discountService, snsService, messageSource, xmlMessageService, sender);
    }

    @Test
    public void testGetNewTickets() throws IOException {

        String cardNumber = "777";

        List<TicketUnassigned> items = new ArrayList<TicketUnassigned>(1);
        TicketUnassigned item1 = new TicketUnassigned();
        item1.setTicketId(UUID.fromString("00000000-0000-0000-0000-000000000000"));
        item1.setDate(DateTime.now());
        item1.setCardNumber("777");
        item1.setName("Ivanov");
        item1.setPhone("495-00-01");
        item1.setSubject("subj");
        item1.setVoucherRowId("00000000-0000-0000-0000-000000000000#1");

        TicketUnassigned item2 = new TicketUnassigned();
        item2.setTicketId(UUID.fromString("00000000-0000-0000-0000-000000000000"));
        item2.setDate(DateTime.now());
        item2.setCardNumber("777");
        item2.setName("Ivanov");
        item2.setPhone("495-00-01");
        item2.setSubject("subj");
        item2.setVoucherRowId("00000000-0000-0000-0000-000000000000#2");

        items.add(item1);
        items.add(item2);

        when(dbService.getAllObjects(eq(TicketUnassigned.class)))
                .thenReturn(items);

        List<TicketInfo> ticketList = ticketService.getNewTickets();

        assertEquals(2, ticketList.size());
        assertThat(ticketList.get(0), instanceOf(TicketUnassigned.class));

        assertTrue(ticketList.get(0).getCardNumber().equals(cardNumber));

    }

    @Test
    public void testGetAssignedTickets() throws RuntimeException, InterruptedException, IOException {

        String executive1 = "manager@pharmanet.ru";
        String executive2 = "boss@pharmanet.ru";

        List<TicketAssigned> itemsAll = new ArrayList<TicketAssigned>();
        List<TicketAssigned> itemsExecutive = new ArrayList<TicketAssigned>();

        TicketAssigned item1 = new TicketAssigned();
        item1.setTicketId(UUID.fromString("00000000-0000-0000-0000-000000000000"));
        item1.setExecutive(executive1);
        item1.setDate(DateTime.now());
        item1.setCardNumber("777");
        item1.setName("Ivanov");
        item1.setPhone("495-00-01");
        item1.setSubject("subj");
        item1.setVoucherRowId("00000000-0000-0000-0000-000000000000#1");

        TicketAssigned item2 = new TicketAssigned();
        item2.setTicketId(UUID.fromString("00000000-0000-0000-0000-000000000000"));
        item2.setExecutive(executive2);
        item2.setDate(DateTime.now());
        item2.setCardNumber("777");
        item2.setName("Ivanov");
        item2.setPhone("495-00-01");
        item2.setSubject("subj");
        item2.setVoucherRowId("00000000-0000-0000-0000-000000000000#2");

        itemsAll.add(item1);
        itemsAll.add(item2);
        itemsExecutive.add(item1);

        when(dbService
                .getAllObjects(eq(TicketAssigned.class)))
                .thenReturn(itemsAll);

        List<TicketInfo> ticketListAll = ticketService
                .getAssignedTickets(null);

        assertEquals(2, ticketListAll.size());

        List<TicketInfo> ticketList = ticketService
                .getAssignedTickets(executive1);

        assertEquals(1, ticketList.size());
        assertThat(ticketList.get(0), instanceOf(TicketAssigned.class));

    }

    @Test
    public void testGetAllPostsInTicket() throws InstantiationException, IllegalAccessException, IOException {

        String ticketId = "00000000-0000-0000-0000-000000000001";

        List<TicketPost> items = new ArrayList<TicketPost>();
        Map<String, Object> item1 = new HashMap<String, Object>();
        item1.put(TicketPost.FLD_TICKET_ID,
                UUID.fromString(ticketId));
        item1.put(TicketPost.FLD_DATE, "01.01.2013 10:00:00 +0400");
        item1.put(TicketPost.FLD_AUTHOR, "Ivanov");
        item1.put(TicketPost.FLD_POST, "blah blah blah");
        item1.put(TicketPost.FLD_SUBJECT, "subj");
        item1.put(TicketPost.FLD_VOUCHER_ROW_ID,
                "00000000-0000-0000-0000-000000000000#1");

        Map<String, Object> item2 = new HashMap<String, Object>();
        item2.put(TicketPost.FLD_TICKET_ID,
                UUID.fromString(ticketId));
        item2.put(TicketPost.FLD_DATE, "01.01.2013 13:00:00 +0400");
        item2.put(TicketPost.FLD_AUTHOR, "Ivanov");
        item2.put(TicketPost.FLD_POST, "ffffffuuuuuuuuuuuu");
        item2.put(TicketPost.FLD_SUBJECT, "subj");
        item2.put(TicketPost.FLD_VOUCHER_ROW_ID,
                "00000000-0000-0000-0000-000000000000#1");

        items.add(new TicketPost(item1));
        items.add(new TicketPost(item2));

        when(dbService.queryObjects(eq(TicketPost.class), eq(ticketId))).thenReturn(items);

        List<TicketPost> ticketList = ticketService.getTicketPosts(ticketId);

        assertEquals(2, ticketList.size());

    }

    @Test
    public void testAddPost() throws ItemSizeLimitExceededException {

        // TicketPost post = new TicketPost("777", "1234567890", "Tester",
        // "Test message", "Opened", DateTimeZone.UTC);
        TicketPost post = new TicketPost(UUID.randomUUID(), "Pushkin", "case",
                "post", "new", "00000000-0000-0000-0000-000000000000#1",
                DateTime.now());
        when(dbService.putObject(post)).thenReturn(true);

        boolean result = ticketService.addPost(post);
        assertTrue(result);
    }

    @Test
    public void testGetNewTicketsCount() {

        List<Map<String, Object>> results = new ArrayList<Map<String, Object>>(
                1);
        Map<String, Object> result = new HashMap<String, Object>();
        results.add(result);

        when(dbService
                .getAllItems(
                        TicketUnassigned.TABLE_NAME, null)).thenReturn(results);

        assertTrue(ticketService.getNewTicketsCount() > 0);
    }

    @Test
    public void testIsClosedStatus() {

        List<String> closedStatuses = Arrays.asList("Closed", "Finished");
        when(settingsService.getTicketStatusClosed())
                .thenReturn(closedStatuses);
        assertTrue(ticketService.isClosedStatus(closedStatuses.get(0)));
    }

    @Test
    public void testUpdateTicketReversal() {
        String cardId = "777";
        String transactionId = "00000000-0000-0000-0000-000000000000";
        Boolean value = true;

        Ticket ticket = new Ticket();
        ticket.setCardNumber(cardId);
        ticket.setTicketId(UUID.fromString(transactionId));
        ticket.setReversal(value);

        when(dbService.getObject(eq(Ticket.class), eq(cardId), eq(transactionId)))
                .thenReturn(ticket);

        assertTrue(ticketService.updateTicketReversal(cardId, transactionId,
                value));
    }

    @Test
    public void testGetReverseTransactionRowRefunds()
            throws JsonParseException,
            JsonMappingException, IOException {

        String discountId = "discountId";

        BigDecimal pack1Data1Percent = new BigDecimal("10");
        PackDiscountData pack1Data1 = new PackDiscountData(discountId);
        pack1Data1.setOriginalPercent(pack1Data1Percent);

        BigDecimal ppack1Data2Percent = new BigDecimal("10");
        PackDiscountData pack1Data2 = new PackDiscountData(discountId);
        pack1Data2.setOriginalPercent(ppack1Data2Percent);

        BigDecimal pack1Data3Percent = BigDecimal.ZERO;
        PackDiscountData pack1Data3 = new PackDiscountData(discountId);
        pack1Data3.setOriginalPercent(pack1Data3Percent);

        VoucherResponseRow row1 = new VoucherResponseRow("Product1", "barcode",
                new BigDecimal(100.0), 1, new BigDecimal(70.0), new BigDecimal(
                        30.0), new BigDecimal(30.0), new BigDecimal(15.0),
                new BigDecimal(100.0), new BigDecimal(30.0), "clientPackId1",
                "clientPackName1", 1);
        row1.setPackDiscount(new PackDiscountData[] {pack1Data1, pack1Data2,
                pack1Data3});
        List<VoucherResponseRow> rows = Arrays.asList(row1);

        VoucherResponseCard card = new VoucherResponseCard("777", "owner",
                "1234567890", true, "comment", "", "");

        UUID voucherId = UUID.randomUUID();
        ProcessingTransaction transaction = new ProcessingTransaction();
        transaction.setVoucherId(voucherId);
        transaction.setRows(rows);
        transaction.setCard(card);

        List<Refund> refunds = ticketService.getReverseTransactionRowRefunds(
                transaction, row1);

        assertEquals(2, refunds.size());

        assertEquals(-1, Integer.parseInt(refunds.get(0).getRow()));
        assertEquals(-1, Integer.parseInt(refunds.get(1).getRow()));
    }

    @Test
    public void testCreateReverseRefund() throws JsonGenerationException,
            JsonMappingException, IOException {

        String discountId = "discountId";
        UUID voucherId = UUID.randomUUID();
        String rowNumber = "1";
        String authCode = "100";
        String cardNumber = "777";
        DateTime period = new DateTime();
        DateTime dateTime = period;

        BigDecimal rowPrice = new BigDecimal("100.00");
        BigDecimal rowCount = new BigDecimal("1");
        BigDecimal rowSumWithoutDiscount = new BigDecimal("100.00");
        BigDecimal rowDiscountPercent = new BigDecimal("25");
        BigDecimal rowDiscountSum = new BigDecimal("75.00");
        BigDecimal rowPaidSum = new BigDecimal("75.00");
        BigDecimal rowDrugstoreRefund = new BigDecimal("25.00");
        BigDecimal rowCardLimitUsedSum = new BigDecimal("75.00");

        BigDecimal discountPercentOriginal = new BigDecimal("25");
        BigDecimal discountPercentUsed = new BigDecimal("25");
        BigDecimal discountRefundOriginalSum = new BigDecimal("25.00");
        BigDecimal discountRefundUsedSum = new BigDecimal("25.00");
        BigDecimal discountRefundSum = new BigDecimal("25.00");

        String clientPackId = "clientPackId";
        String clientPackName = "clientPackName";
        String packId = "packId";
        String packName = "packName";
        String manufacturer = "manufacturer";
        String drugstoreId = "drugstoreId";
        String drugstoreName = "drugstoreName";
        String currentVersion = "1";
        String barcode = "barcode";

        VoucherResponseCard card = new VoucherResponseCard(cardNumber, "owner",
                "1234567890", true, "comment", "", "");

        PackDiscountData pack = new PackDiscountData(discountId);
        pack.setDiscountId(discountId);
        pack.setOriginalPercent(discountPercentOriginal);
        pack.setOriginalSum(discountRefundOriginalSum);
        pack.setUsedPercent(discountPercentUsed);
        pack.setUsedSum(discountRefundUsedSum);
        pack.setRefundSum(discountRefundSum);

        VoucherResponseRow row = new VoucherResponseRow(packName, barcode,
                rowPrice, Integer.parseInt(rowCount.toString()), rowPaidSum,
                rowDiscountSum, rowDiscountPercent, rowDrugstoreRefund,
                rowSumWithoutDiscount, rowCardLimitUsedSum, clientPackId,
                clientPackName, 1);
        row.setPackDiscount(new PackDiscountData[] {pack});
        row.setId(packId);
        row.setManufacturer(manufacturer);

        List<VoucherResponseRow> rows = new ArrayList<VoucherResponseRow>();
        rows.add(row);

        ProcessingTransaction transaction = new ProcessingTransaction(
                voucherId, drugstoreId, card, drugstoreName,
                rowSumWithoutDiscount, rowDiscountSum, discountId,
                rowDiscountPercent.intValue(), currentVersion, rowPaidSum,
                new String[] {}, rows,
                DateTime.now().withZone(DateTimeZone.UTC));
        transaction.setDate(dateTime);
        transaction.setVoucherId(voucherId);
        transaction.setCard(card);
        transaction.setAuthCode(authCode);

        Refund result = ticketService.createReverseRefund(transaction, row,
                pack,
                period);

        assertEquals(Refund.dateFormatter.print(period), result.getPeriod());
        assertEquals(discountId, result.getDiscountId());
        assertEquals(voucherId.toString(), result.getVoucherId());
        assertEquals("-" + rowNumber, result.getRow());

        assertEquals(authCode, result.getAuthCode());
        assertEquals(cardNumber, result.getCardId());
        assertEquals(dateTime.minusMillis(dateTime.getMillisOfSecond())
                .getMillis(), result.getDateTime().getMillis());

        assertEquals(rowPrice, result.getRowPrice());
        assertEquals(rowCount.negate(), result.getRowCount());
        assertEquals(rowSumWithoutDiscount.negate(),
                result.getRowSumWithoutDiscount());
        assertEquals(rowDiscountPercent, result.getRowDiscountPercent());
        assertEquals(rowDiscountSum.negate(), result.getRowDiscountSum());
        assertEquals(rowPaidSum.negate(), result.getRowPaidSum());
        assertEquals(rowDrugstoreRefund.negate(),
                result.getRowDrugstoreRefund());

        assertEquals(discountPercentOriginal,
                result.getDiscountPercentOriginal());
        assertEquals(discountPercentUsed, result.getDiscountPercentUsed());
        assertEquals(discountRefundOriginalSum.negate(),
                result.getDiscountRefundOriginalSum());
        assertEquals(discountRefundUsedSum.negate(),
                result.getDiscountRefundUsedSum());
        assertEquals(discountRefundSum.negate(), result.getDiscountRefundSum());

        assertEquals(clientPackId, result.getClientPackId());
        assertEquals(clientPackName, result.getClientPackName());
        assertEquals(packId, result.getPackId());
        assertEquals(packName, result.getPackName());
        assertEquals(manufacturer, result.getManufacturer());
        assertEquals(drugstoreId, result.getDrugstoreId());
        assertEquals(drugstoreName, result.getDrugstoreName());
    }

    @Test
    public void testGetTicketByCardNumberAndCode() {

        String cardNumber = "777";
        String transactionCode = String.valueOf(System.currentTimeMillis());
        Ticket ticket = new Ticket();

        when(dbService.getObject(eq(Ticket.class), eq(cardNumber),
                eq(transactionCode))).thenReturn(ticket);

        Ticket result = ticketService.getTicket(cardNumber, transactionCode);

        verify(dbService).getObject(eq(Ticket.class), eq(cardNumber),
                eq(transactionCode));

        assertEquals(ticket, result);
    }

    @Test
    public void testTransactionValidation() throws JsonGenerationException,
            JsonMappingException, IOException {
        DateTime dateTime = DateTime.now();
        UUID voucherId = UUID
                .fromString("00000000-0000-0000-0000-000000000000");
        String voucherRowId = voucherId.toString()
                + ProcessingTransaction.getDelimeter() + "1";
        String drugstoreId = "id";

        String discountId = "discountId";

        BigDecimal pack1Data1Percent = new BigDecimal("10");
        PackDiscountData pack1Data1 = new PackDiscountData(discountId);
        pack1Data1.setOriginalPercent(pack1Data1Percent);

        BigDecimal ppack1Data2Percent = new BigDecimal("10");
        PackDiscountData pack1Data2 = new PackDiscountData(discountId);
        pack1Data2.setOriginalPercent(ppack1Data2Percent);

        BigDecimal pack1Data3Percent = BigDecimal.ZERO;
        PackDiscountData pack1Data3 = new PackDiscountData(discountId);
        pack1Data3.setOriginalPercent(pack1Data3Percent);

        VoucherResponseRow row1 = new VoucherResponseRow("Product1", "barcode",
                new BigDecimal(100.0), 1, new BigDecimal(70.0), new BigDecimal(
                        30.0), new BigDecimal(30.0), new BigDecimal(15.0),
                new BigDecimal(100.0), new BigDecimal(30.0), "clientPackId1",
                "clientPackName1", 1);
        row1.setPackDiscount(new PackDiscountData[] {pack1Data1, pack1Data2,
                pack1Data3});
        List<VoucherResponseRow> rows = Arrays.asList(row1);

        VoucherResponseCard card = new VoucherResponseCard("777", "owner",
                "1234567890", true, "comment", "", "");

        ProcessingTransaction transaction = new ProcessingTransaction();
        transaction.setVoucherId(voucherId);
        transaction.setRows(rows);
        transaction.setCard(card);

        when(dbService.getObject(eq(ProcessingTransaction.class),
                eq(drugstoreId), anyString())).thenReturn(transaction);

        // test normal behavior
        assertTrue(ticketService.isTransactionFound(dateTime, voucherRowId,
                drugstoreId));

        // test row not number
        assertFalse(ticketService.isTransactionFound(dateTime, "BlahBlah#Blah",
                drugstoreId));

        // test negative row
        assertFalse(ticketService.isTransactionFound(dateTime,
                voucherId.toString() + ProcessingTransaction.getDelimeter()
                        + "-27",
                drugstoreId));

        // test transaction not found
        when(dbService.getObject(eq(ProcessingTransaction.class),
                eq(drugstoreId), anyString())).thenReturn(null);
        assertFalse(ticketService.isTransactionFound(dateTime, voucherRowId,
                drugstoreId));

    }
}
