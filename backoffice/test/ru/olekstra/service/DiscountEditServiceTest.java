package ru.olekstra.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.backoffice.util.CsvParser;
import ru.olekstra.domain.Discount;
import ru.olekstra.domain.DiscountData;
import ru.olekstra.domain.DiscountLimit;
import ru.olekstra.domain.DiscountPlan;
import ru.olekstra.domain.DiscountReplace;
import ru.olekstra.helper.DiscountHelper;

import com.amazonaws.AmazonServiceException;

@RunWith(MockitoJUnitRunner.class)
public class DiscountEditServiceTest {

    private DiscountHelper helper;
    private DiscountEditService service;

    @Before
    public void setup() {
        helper = mock(DiscountHelper.class);
        service = new DiscountEditService(helper);
    }

    @Test
    public void testGetAll() throws IOException {

        String discountId = "testId";
        Discount discount = new Discount();
        discount.setId(discountId);

        String anotherDiscountId = "another testId";
        Discount anotherDiscount = new Discount();
        anotherDiscount.setId(anotherDiscountId);

        List<Discount> discounts = Arrays.asList(discount, anotherDiscount);

        when(helper.getAllDiscounts()).thenReturn(discounts);

        List<Discount> result = service.getAll();

        verify(helper).getAllDiscounts();

        assertEquals(discounts, result);
    }

    @Test
    public void testGetDiscount() {

        String discountId = "testId";
        Discount discount = new Discount();
        discount.setId(discountId);

        when(helper.getDiscount(eq(discountId))).thenReturn(discount);

        Discount result = service.getDiscount(discountId);

        verify(helper).getDiscount(eq(discountId));

        assertEquals(discount, result);
    }

    @Test
    public void testSaveDiscount() throws ItemSizeLimitExceededException {

        String id = "testId";
        String name = "name";
        boolean useCardLimit = true;
        boolean alwaysFullRefund = true;
        boolean newDiscount = true;
        int promoPercent = 5;
        int version = 1;
        int packCount = 0;

        when(helper.saveDiscount(any(Discount.class))).thenReturn(true);

        boolean result = service.saveDiscount(id, name, promoPercent, version,
                packCount, newDiscount, useCardLimit, alwaysFullRefund);

        assertTrue(result);
    }

    @Test
    public void testDeleteDiscount() {

        String discountId = "testId";
        service.deleteDiscount(discountId);
        verify(helper).deleteDiscount(eq(discountId));
    }

    @Test
    public void testGetRelatedDiscountPlans() throws JsonGenerationException,
            JsonMappingException, IOException {

        String planName1 = "planName1";
        String planName2 = "planName2";

        String discountUsed1 = "discountUsed1";
        String discountUsed2 = "discountUsed2";
        String notUsedDiscount = "freeDiscount";

        DiscountPlan discountPlan1 = new DiscountPlan("planId1", planName1, 50);
        discountPlan1.setDiscounts(Arrays.asList(discountUsed1));
        DiscountPlan discountPlan2 = new DiscountPlan("planId2", planName2, 50);
        discountPlan2.setDiscounts(Arrays.asList(discountUsed1, discountUsed2));

        when(helper.getAllDiscountPlans()).thenReturn(
                Arrays.asList(discountPlan1, discountPlan2));

        // all plans contain discount
        List<String> result = service.getRelatedDiscountPlans(discountUsed1);

        verify(helper).getAllDiscountPlans();

        assertTrue(result.contains(planName1));
        assertTrue(result.contains(planName2));

        // one plan contains discount
        result = service.getRelatedDiscountPlans(discountUsed2);

        verify(helper, times(2)).getAllDiscountPlans();

        assertFalse(result.contains(planName1));
        assertTrue(result.contains(planName2));

        // no plans contain discount
        result = service.getRelatedDiscountPlans(notUsedDiscount);

        verify(helper, times(3)).getAllDiscountPlans();

        assertFalse(result.contains(planName1));
        assertFalse(result.contains(planName2));
    }

    @Test
    public void testIsAlreadyExists() {

        String discountId = "testId";
        // discount is exists
        when(helper.getDiscount(eq(discountId))).thenReturn(new Discount());

        boolean result = service.isAlreadyExists(discountId);

        verify(helper).getDiscount(eq(discountId));
        assertTrue(result);

        // discount is not exists
        when(helper.getDiscount(any(String.class))).thenReturn(null);

        result = service.isAlreadyExists(discountId);

        verify(helper, times(2)).getDiscount(any(String.class));
        assertFalse(result);
    }

    @Test
    public void testSaveDiscountData() throws AmazonServiceException,
            IllegalArgumentException, InterruptedException,
            ItemSizeLimitExceededException {

        String discountId = "testId";
        Map<String, DiscountData> packsMap = new HashMap<String, DiscountData>();
        DiscountData pack = new DiscountData();
        pack.setPercent(30);
        pack.setLimitGroupId("group1");
        pack.setLimitGroupWeight(1);
        packsMap.put("ekt1", pack);
        pack = new DiscountData();
        pack.setPercent(35);
        pack.setLimitGroupId("group2");
        pack.setLimitGroupWeight(1);
        packsMap.put("ekt2", pack);
        pack = new DiscountData();
        pack.setPercent(40);
        pack.setLimitGroupId("group3");
        pack.setLimitGroupWeight(1);
        packsMap.put("ekt3", pack);
        int version = 1;

        when(helper.saveDiscountData(anyListOf(DiscountData.class)))
                .thenReturn(true);

        boolean result = service
                .saveDiscountData(discountId, packsMap, version);

        verify(helper).saveDiscountData(anyListOf(DiscountData.class));
        assertTrue(result);
    }

    @Test
    public void testSaveDiscountReplaces() throws AmazonServiceException,
            IllegalArgumentException, InterruptedException,
            ItemSizeLimitExceededException {

        String discountId = "testId";
        Map<String, String> packsEktSynonym = new HashMap<String, String>();
        int version = 1;

        when(helper.saveDiscountReplaces(anyListOf(DiscountReplace.class)))
                .thenReturn(true);

        boolean result = service.saveDiscountReplaces(discountId,
                packsEktSynonym, version);

        verify(helper).saveDiscountReplaces(anyListOf(DiscountReplace.class));
        assertTrue(result);
    }

    @Test
    public void testParsePacksCsv() throws IOException {

        String packEkt1 = "1234567890123";
        String packName1 = "Some Pack";
        String discount1 = "50";
        String packEkt2 = "1234567890124";
        String packName2 = "Some Other Pack";
        String discount2 = "30";
        String packDuplicate = "1234567890125";
        String duplicateLine = packDuplicate + CsvParser.TAB + "50" + "\n"
                + packDuplicate + CsvParser.TAB + "50" + "\n";
        String errorPack = "not a number";
        String errorLine = errorPack + "\n";

        String packsCsv = packEkt1 + CsvParser.TAB + discount1 + CsvParser.TAB + CsvParser.TAB + CsvParser.TAB
                + packName1 + "\n" + packEkt2 + CsvParser.TAB + discount2
                + CsvParser.TAB + CsvParser.TAB + CsvParser.TAB + packName2 + "\n" + duplicateLine + errorLine;

        Map<String, DiscountData> packsEkt = new HashMap<String, DiscountData>();
        Map<String, String> packsEktSynonym = new HashMap<String, String>();
        List<String> errorLines = new ArrayList<String>();
        List<String> duplicateLines = new ArrayList<String>();

        service.parsePacksCsv("id", 1, packsCsv, packsEkt, packsEktSynonym, errorLines,
                duplicateLines);

        assertTrue(packsEkt.containsKey(packEkt1));
        assertTrue(packsEkt.containsKey(packEkt2));
        assertTrue(packsEkt.containsKey(packDuplicate));
        assertFalse(packsEkt.containsKey(errorPack));

        assertEquals(packName1, packsEktSynonym.get(packEkt1));
        assertEquals(packName2, packsEktSynonym.get(packEkt2));

        assertTrue(duplicateLine.contains(packDuplicate));
        assertTrue(errorLines.get(0).contains(errorPack));
    }

    @Test
    public void testParseLimitsCsv() throws IOException {

        String limitsCsv = "13\tmonth\t1\t1\t1.5\t1.6\n" +
                "123\tquarter\t2\t2\t1.6\t1.7\n" +
                "12\thalfyear\t3\t3\t1.7\t1.8\n" +
                "122\tyear\t4\t4\t1.8\t1.9\n" +
                "122\tyear\t4\t4\t1.8\t1.9\n" +
                "103\tmonth\t5sss\t5\t1.9\t1.0\n";

        String packDuplicate = "122";
        String errorPack = "103";

        Map<String, DiscountLimit> limits = new HashMap<String, DiscountLimit>();
        List<String> errorLines = new ArrayList<String>();
        List<String> duplicateLines = new ArrayList<String>();

        service.parseLimitsCsv(limitsCsv, limits, errorLines, duplicateLines, "discount000", 1);

        assertTrue(limits.containsKey("13"));
        assertTrue(limits.containsKey("12"));
        assertTrue(limits.containsKey(packDuplicate));
        assertFalse(limits.containsKey(errorPack));

        assertTrue(duplicateLines.get(0).contains(packDuplicate));
        assertTrue(errorLines.get(0).contains(errorPack));
    }

    @Test
    public void testIsPackDataValid() {

        // empty parsed data
        String[] data1 = new String[] {};
        boolean result = service.isPackDataValid(data1);
        assertFalse(result);

        // incorrect data
        String[] data2 = new String[] {"not a number"};
        result = service.isPackDataValid(data2);
        assertFalse(result);

        // correct data
        String[] data3 = new String[] {"1234567890123", "56"};
        result = service.isPackDataValid(data3);
        assertTrue(result);
    }

    @Test
    public void testIsLimitValid() {

        // empty parsed data
        String[] data1 = new String[] {};
        boolean result = service.isLimitValid(data1);
        assertFalse(result);

        // incorrect data
        String[] data2 = new String[] {"abc", "month", "5sss", "5", "1.9", "1.0"};
        result = service.isLimitValid(data2);
        assertFalse(result);

        // incorrect data
        String[] data21 = new String[] {"abc", "month1", "5", "5", "1.9", "1.0"};
        result = service.isLimitValid(data21);
        assertFalse(result);

        // correct data
        String[] data3 = new String[] {"abc", "quarter", "2", "2", "1.6", "1.7"};
        result = service.isLimitValid(data3);
        assertTrue(result);
    }

}
