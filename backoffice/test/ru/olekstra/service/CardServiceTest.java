package ru.olekstra.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;
import org.dozer.Mapper;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;

import ru.olekstra.awsutils.BatchOperationResult;
import ru.olekstra.awsutils.ComplexKey;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.awsutils.exception.NotUpdatedException;
import ru.olekstra.backoffice.dto.BalanceOperationType;
import ru.olekstra.backoffice.dto.CardsLoadResult;
import ru.olekstra.common.helper.CardHelper;
import ru.olekstra.common.service.SmsService;
import ru.olekstra.domain.Card;
import ru.olekstra.exception.DuplicatePhoneException;
import ru.olekstra.exception.InvalidDateRangeException;
import ru.olekstra.exception.NotFoundException;

import com.amazonaws.AmazonServiceException;

@RunWith(MockitoJUnitRunner.class)
public class CardServiceTest {

    private InputStream cardsInputStream;
    private CardHelper helper;
    private SmsService smsService;
    private CardService service;
    private MessageSource messageSource;
    private Mapper mapper;

    @Before
    public void setup() {
        helper = mock(CardHelper.class);
        smsService = mock(SmsService.class);
        messageSource = mock(MessageSource.class);
        mapper = mock(Mapper.class);
        service = new CardService(helper, smsService, messageSource, mapper);

        String date1 = "21.11.2012";
        String date2 = "30.11.2012";
        String quotes = "\"";
        String quotesAndSemicolon = "\"" + ";" + "\"";
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 15; i++) {
            builder.append(quotes);
            builder.append("30000");
            if (i == 9)
                builder.append(2);
            else
                builder.append(i);
            builder.append(quotesAndSemicolon);
            builder.append("holder");
            builder.append(i);
            builder.append(quotesAndSemicolon);
            builder.append("");
            builder.append(quotesAndSemicolon);
            if (i == 7) {
                builder.append(date2);
                builder.append(quotesAndSemicolon);
                builder.append(date1);
            } else if (i == 5) {
                builder.append(" ");
                builder.append(quotesAndSemicolon);
                builder.append(date1);
            } else if (i == 4) {
                builder.append("");
                builder.append(quotesAndSemicolon);
                builder.append("");
            } else if (i == 3) {
                builder.append(date1);
            } else {
                builder.append(date1);
                builder.append(quotesAndSemicolon);
                builder.append(date2);
            }
            builder.append(quotes);
            builder.append(System.getProperty("line.separator"));
        }
        cardsInputStream = new ByteArrayInputStream(builder.toString()
                .getBytes());
    }

    @Test
    public void testLoadCardsWhenAllCardsAreNew()
            throws IllegalArgumentException, InvalidDateRangeException {
        int count = 3;
        String plan = "plan";
        String fromDate = "22.06.2012";
        String toDate = "22.06.2012";
        Map<String, String> numbers = new HashMap<String, String>();
        numbers.put("One", "111");
        numbers.put("Two", "222");
        numbers.put("Three", "333");

        when(helper.addNewCard(any(Card.class))).thenReturn(true);

        List<String> actual = service
                .createCards(plan, fromDate, toDate, numbers);

        assertEquals(count, actual.size());
    }

    @Test
    public void testLoadCardsWhenAllCardsAreExisting()
            throws IllegalArgumentException, InvalidDateRangeException {
        int count = 3;
        String plan = "plan";
        String fromDate = "22.06.2012";
        String toDate = "22.06.2012";
        Map<String, String> numbers = new HashMap<String, String>();
        numbers.put("One", "111");
        numbers.put("Two", "222");
        numbers.put("Three", "333");

        when(helper.addNewCard(any(Card.class))).thenReturn(false);

        List<String> actual = service
                .createCards(plan, fromDate, toDate, numbers);

        assertEquals(count - 3, actual.size());
    }

    @Test
    public void testUpdateCardBalanceTypeRecharge() throws RuntimeException,
            InterruptedException {

        String cardNumber1 = "7771";
        String cardNumber2 = "7772";

        Card card1 = new Card(cardNumber1);
        card1.setLimitRemain(new BigDecimal("10.00"));
        Card card2 = new Card(cardNumber2);
        card2.setLimitRemain(null);

        List<Card> cards = Arrays.asList(card1, card2);

        Map<String, Boolean> updatedCards = new HashMap<String, Boolean>();
        Map<String, BigDecimal> updatedLimits = new HashMap<String, BigDecimal>();
        List<String> sameLimitCards = new ArrayList<String>();
        Map<String, BigDecimal> moreLimitCards = new HashMap<String, BigDecimal>();

        List<String> limitFieldList = new ArrayList<String>(1);
        limitFieldList.add(Card.FLD_LIMIT_REMAIN);
        when(
                helper.updateCardWithNullConditions(any(Card.class),
                        eq(limitFieldList))).thenReturn(true);

        BigDecimal valueRecharge = new BigDecimal("15.00");
        when(
                helper.updateCardLimit(eq(cardNumber1), eq(card1
                        .getLimitRemain()), eq(card1.getLimitRemain().add(
                        valueRecharge)))).thenReturn(true);

        // пополнение баланса
        service.updateCardBalance(valueRecharge, cards, updatedCards,
                updatedLimits, sameLimitCards, moreLimitCards,
                BalanceOperationType.RECAHARGE);

        verify(helper).updateCardWithNullConditions(any(Card.class),
                eq(limitFieldList));
        verify(helper).updateCardLimit(eq(cardNumber1),
                eq(card1.getLimitRemain()),
                eq(card1.getLimitRemain().add(valueRecharge)));

        assertEquals(2, updatedCards.size());
        assertTrue(updatedCards.get(cardNumber1));
        assertTrue(updatedCards.get(cardNumber2));
        assertEquals(new BigDecimal("25.00"), updatedLimits.get(cardNumber1));
        assertEquals(new BigDecimal("15.00"), updatedLimits.get(cardNumber2));
        assertTrue(sameLimitCards.isEmpty());
        assertTrue(moreLimitCards.isEmpty());
    }

    @Test
    public void testUpdateCardBalanceTypeUpdate() throws RuntimeException,
            InterruptedException {

        String cardNumber1 = "7771";
        String cardNumber2 = "7772";
        String cardNumber3 = "7773";
        String cardNumber4 = "7774";

        Card card1 = new Card(cardNumber1);
        card1.setLimitRemain(new BigDecimal("10.00"));
        Card card2 = new Card(cardNumber2);
        card2.setLimitRemain(new BigDecimal("15.00"));
        Card card3 = new Card(cardNumber3);
        card3.setLimitRemain(new BigDecimal("20.00"));
        Card card4 = new Card(cardNumber4);
        card4.setLimitRemain(null);

        List<Card> cards = Arrays.asList(card1, card2, card3, card4);

        Map<String, Boolean> updatedCards = new HashMap<String, Boolean>();
        Map<String, BigDecimal> updatedLimits = new HashMap<String, BigDecimal>();
        List<String> sameLimitCards = new ArrayList<String>();
        Map<String, BigDecimal> moreLimitCards = new HashMap<String, BigDecimal>();

        List<String> limitFieldList = new ArrayList<String>(1);
        limitFieldList.add(Card.FLD_LIMIT_REMAIN);
        when(
                helper.updateCardWithNullConditions(any(Card.class),
                        eq(limitFieldList))).thenReturn(true);

        BigDecimal valueUpdate = new BigDecimal("15.00");
        when(
                helper.updateCardLimit(eq(cardNumber1), eq(card1
                        .getLimitRemain()), eq(valueUpdate))).thenReturn(true);

        // увеличение баланса до заданного значения
        service.updateCardBalance(valueUpdate, cards, updatedCards,
                updatedLimits, sameLimitCards, moreLimitCards,
                BalanceOperationType.UPTATE);

        verify(helper).updateCardWithNullConditions(any(Card.class),
                eq(limitFieldList));
        verify(helper).updateCardLimit(eq(cardNumber1),
                eq(card1.getLimitRemain()), eq(valueUpdate));

        assertEquals(2, updatedCards.size());
        assertTrue(updatedCards.get(cardNumber1));
        assertTrue(updatedCards.get(cardNumber4));
        assertEquals(new BigDecimal("15.00"), updatedLimits.get(cardNumber1));
        assertEquals(new BigDecimal("15.00"), updatedLimits.get(cardNumber4));
        assertTrue(!sameLimitCards.isEmpty());
        assertTrue(sameLimitCards.contains(cardNumber2));
        assertTrue(!moreLimitCards.isEmpty());
        assertEquals(new BigDecimal("20.00"), moreLimitCards.get(cardNumber3));
    }

    @Test
    public void testUpdateCardBalanceTypeSet() throws RuntimeException,
            InterruptedException {

        String cardNumber1 = "7771";
        String cardNumber2 = "7772";

        Card card1 = new Card(cardNumber1);
        card1.setLimitRemain(new BigDecimal("10.00"));
        Card card2 = new Card(cardNumber2);
        card2.setLimitRemain(null);

        List<Card> cards = Arrays.asList(card1, card2);

        Map<String, Boolean> updatedCards = new HashMap<String, Boolean>();
        Map<String, BigDecimal> updatedLimits = new HashMap<String, BigDecimal>();
        List<String> sameLimitCards = new ArrayList<String>();
        Map<String, BigDecimal> moreLimitCards = new HashMap<String, BigDecimal>();

        List<String> limitFieldList = new ArrayList<String>(1);
        limitFieldList.add(Card.FLD_LIMIT_REMAIN);
        when(
                helper.updateCardWithNullConditions(any(Card.class),
                        eq(limitFieldList))).thenReturn(true);

        BigDecimal valueSet = new BigDecimal("15.00");
        when(
                helper.updateCardLimit(eq(cardNumber1), eq(card1
                        .getLimitRemain()), eq(valueSet))).thenReturn(true);

        // установка баланса в заданное значение
        service.updateCardBalance(valueSet, cards, updatedCards, updatedLimits,
                sameLimitCards, moreLimitCards, BalanceOperationType.SET);

        verify(helper).updateCardWithNullConditions(any(Card.class),
                eq(limitFieldList));
        verify(helper).updateCardLimit(eq(cardNumber1),
                eq(card1.getLimitRemain()), eq(valueSet));

        assertEquals(2, updatedCards.size());
        assertTrue(updatedCards.get(cardNumber1));
        assertTrue(updatedCards.get(cardNumber2));
        assertEquals(new BigDecimal("15.00"), updatedLimits.get(cardNumber1));
        assertEquals(new BigDecimal("15.00"), updatedLimits.get(cardNumber2));
        assertTrue(sameLimitCards.isEmpty());
        assertTrue(moreLimitCards.isEmpty());
    }

    @Test
    public void testSendSmsAfterUpdateCardBalance()
            throws ClientProtocolException, IOException {

        String cardNumber1 = "7771";
        String cardNumForSms = smsService
                .getCardNumLastFourDigits(cardNumber1);
        String telephoneNumber = "000 000 0000";

        Card card1 = new Card(cardNumber1);
        card1.setLimitRemain(new BigDecimal("10.00"));
        card1.setTelephoneNumber(telephoneNumber);

        List<Card> cards = Arrays.asList(card1);

        BigDecimal value = new BigDecimal("15.00");
        Map<String, BigDecimal> updatedLimits = new HashMap<String, BigDecimal>();
        updatedLimits.put(cardNumber1, card1.getLimitRemain());

        String messageForRecharge = "recharge message";
        String messageForUpdateAndSet = "update and set message";

        List<Map<String, String>> recipients = new ArrayList<Map<String, String>>();
        Map<String, String> recipient = new HashMap<String, String>();
        recipient.put(telephoneNumber, messageForRecharge);

        recipients.add(recipient);
        when(
                messageSource.getMessage(
                        eq("msg.logrecord.limit"),
                        eq(new Object[] {cardNumForSms,
                                value.toString(),
                                card1.getLimitRemain().toString()}),
                        eq((Locale) null))).thenReturn(messageForRecharge);

        when(
                messageSource.getMessage(
                        eq("msg.limitSet"),
                        eq(new Object[] {cardNumForSms,
                                value.toString()}),
                        eq((Locale) null)))
                .thenReturn(messageForUpdateAndSet);

        // отправка смс увеличении баланса до заданного значения
        service.sendSmsAfterUpdateCardBalance(value, updatedLimits, cards,
                BalanceOperationType.RECAHARGE);

        verify(smsService).sendMultpleMessages(eq(recipients));

        verify(messageSource).getMessage(
                eq("msg.logrecord.limit"),
                eq(new Object[] {cardNumForSms, value.toString(),
                        card1.getLimitRemain().toString()}), eq((Locale) null));

        // отправка смс увеличении баланса до заданного значения
        updatedLimits.put(cardNumber1, value);
        service.sendSmsAfterUpdateCardBalance(value, updatedLimits, cards,
                BalanceOperationType.UPTATE);

        verify(smsService).sendMultpleMessages(eq(recipients));

        verify(messageSource).getMessage(eq("msg.limitSet"),
                eq(new Object[] {cardNumForSms, value.toString()}),
                eq((Locale) null));

        // отправка смс при установке баланса в заданное значение
        service.sendSmsAfterUpdateCardBalance(value, updatedLimits, cards,
                BalanceOperationType.SET);

        verify(smsService).sendMultpleMessages(eq(recipients));

        verify(messageSource, times(2)).getMessage(eq("msg.limitSet"),
                eq(new Object[] {cardNumForSms, value.toString()}),
                eq((Locale) null));
    }

    @Test
    public void testUpdateCardBalanceAlreadyExistsCard()
            throws NotFoundException, RuntimeException, InterruptedException {

        String rechargeSum = "50";
        String oldSum = "100";
        String resultSum = "150";

        String cardNumber1 = "777";
        String cardNumber2 = "888";

        Map<String, Boolean> updatedCards = new HashMap<String, Boolean>();
        Map<String, BigDecimal> updatedLimits = new HashMap<String, BigDecimal>();

        List<String> sameLimitCards = new ArrayList<String>();
        Map<String, BigDecimal> moreLimitCards = new HashMap<String, BigDecimal>();

        BigDecimal oldLimit = new BigDecimal(oldSum);
        BigDecimal recharge = new BigDecimal(rechargeSum);
        BigDecimal newLimit = new BigDecimal(resultSum);

        Card card1 = new Card(cardNumber1);
        card1.setLimitRemain(oldLimit);

        Card card2 = new Card(cardNumber2);
        card2.setLimitRemain(oldLimit);
        List<Card> cards = Arrays.asList(new Card[] {card1, card2});

        // test for correct new limit, that equals next:
        // newLimit = rechargeSum + oldSum,
        // for current test: 50 + 100 = 150
        // checking helper update method for right arguments
        when(
                helper.updateCardLimit(eq(card1.getNumber()), eq(card1
                        .getLimitRemain()), eq(oldLimit.add(recharge))))
                .thenReturn(true);

        when(
                helper.updateCardLimit(eq(card2.getNumber()), eq(card2
                        .getLimitRemain()), eq(oldLimit.add(recharge))))
                .thenReturn(true);

        service.updateCardBalance(recharge, cards, updatedCards, updatedLimits,
                sameLimitCards, moreLimitCards, BalanceOperationType.RECAHARGE);

        // helper update method should return true
        assertTrue(updatedCards.get(cardNumber1));
        assertTrue(updatedCards.get(cardNumber2));

        // updated values should contain newLimit
        assertTrue(updatedLimits.get(cardNumber1).equals(newLimit));
        assertTrue(updatedLimits.get(cardNumber2).equals(newLimit));
    }

    @Test
    public void testUpdateCardBalanceNewCard() throws NotFoundException,
            RuntimeException, InterruptedException, IOException {

        String rechargeSum = "50";
        String cardNumber = "777";
        Card card = new Card(cardNumber);
        List<String> cardNumbers = Arrays.asList(cardNumber);

        Map<String, Boolean> updatedCards = new HashMap<String, Boolean>();
        Map<String, BigDecimal> updatedLimits = new HashMap<String, BigDecimal>();
        List<String> sameLimitCards = new ArrayList<String>();
        Map<String, BigDecimal> moreLimitCards = new HashMap<String, BigDecimal>();
        BigDecimal recharge = new BigDecimal(rechargeSum);

        List<Card> cards = Arrays.asList(card);

        BatchOperationResult<Card> batchResult = new BatchOperationResult<Card>(
                cards, null);

        when(helper.getCardsInBatchMode(cardNumbers)).thenReturn(batchResult);

        // test for correct new limit, that equals recharge sum:
        // checking helper update method for right arguments
        when(
                helper.updateCardWithNullConditions(eq(card), eq(Arrays
                        .asList(Card.FLD_LIMIT_REMAIN)))).thenReturn(true);

        service.updateCardBalance(recharge, cards, updatedCards, updatedLimits,
                sameLimitCards, moreLimitCards, BalanceOperationType.RECAHARGE);

        // helper update method should return true
        assertTrue(updatedCards.get(cardNumber));

        // updated values should contain newLimit
        assertTrue(updatedLimits.get(cardNumber).equals(recharge));
    }

    @Test
    public void testSendSmsAfterUpdateCardPlan() {
        String message = "Test message";
        DateTime dateFrom = new DateTime();
        DateTime dateTo = new DateTime(System.currentTimeMillis() + 1000);
        Card card1 = new Card("7771", false, "Tester", "7000000001", dateFrom,
                dateTo);
        Card card2 = new Card("7772", false, "Tester", "7000000002", dateFrom,
                dateTo);
        List<Card> cards = Arrays.asList(new Card[] {card1, card2});

        boolean result = service.sendSmsAfterBatchCardUpdate(message, cards);
        assertTrue(result);
    }

    @Test
    public void testLoadCards() throws RuntimeException, InterruptedException, IOException {

        DateTime dateFrom = new DateTime();
        DateTime dateTo = new DateTime(System.currentTimeMillis() + 1000);
        String cardNumber1 = "7771";
        String cardNumber2 = "7772";

        Card card1 = new Card(cardNumber1, false, "Tester", "7000000001",
                dateFrom, dateTo);
        Card card2 = new Card(cardNumber2, false, "Tester", "7000000002",
                dateFrom, dateTo);

        List<String> cardNumbers = new ArrayList<String>();
        cardNumbers.add(cardNumber1);
        cardNumbers.add(cardNumber2);

        List<Card> cards = new ArrayList<Card>();
        cards.add(card1);
        cards.add(card2);

        when(helper.getCardsBatch(cardNumbers)).thenReturn(cards);

        CardsLoadResult result = service.loadCards(cardNumbers);

        verify(helper).getCardsBatch(cardNumbers);

        assertNotNull(result);
        assertEquals(cards.size(), result.getLoadedCards().size());
        assertEquals(cards, result.getLoadedCards());
        assertTrue(result.getDuplicateCards().isEmpty());
        assertTrue(result.getNotFoundCards().isEmpty());
    }

    @Test
    public void testLoadCardsWithDupleicatesAndNotFoundCards()
            throws RuntimeException, InterruptedException, IOException {

        DateTime dateFrom = new DateTime();
        DateTime dateTo = new DateTime(System.currentTimeMillis() + 10000);
        String cardNumber1 = "7771";
        String cardNumber2 = "7772";

        Card card1 = new Card(cardNumber1, false, "Tester", "7000000001",
                dateFrom, dateTo);
        Card card2 = new Card(cardNumber2, false, "Tester", "7000000002",
                dateFrom, dateTo);

        String duplicateNumber = "7771";
        String notFoundNumber = "7770";

        List<String> cardNumbers = new ArrayList<String>();
        cardNumbers.add(cardNumber1);
        cardNumbers.add(cardNumber2);
        cardNumbers.add(duplicateNumber);
        cardNumbers.add(duplicateNumber);
        cardNumbers.add(notFoundNumber);
        cardNumbers.add(duplicateNumber);
        cardNumbers.add(notFoundNumber);

        List<String> cardNumbersToLoad = new ArrayList<String>();
        cardNumbersToLoad.add(cardNumber1);
        cardNumbersToLoad.add(cardNumber2);
        cardNumbersToLoad.add(notFoundNumber);

        List<Card> cards = new ArrayList<Card>();
        cards.add(card1);
        cards.add(card2);

        when(helper.getCardsBatch(cardNumbersToLoad)).thenReturn(cards);

        CardsLoadResult result = service.loadCards(cardNumbers);

        verify(helper).getCardsBatch(cardNumbersToLoad);

        assertEquals(cards, result.getLoadedCards());
        assertFalse(result.getDuplicateCards().isEmpty());
        assertFalse(result.getNotFoundCards().isEmpty());

        assertEquals(1, result.getNotFoundCards().size());
        assertEquals(2, result.getDuplicateCards().size());
    }

    @Test
    public void testSkipAlreadySubscribed() {

        DateTime dateFrom = new DateTime();
        DateTime dateTo = new DateTime(System.currentTimeMillis() + 10000);
        String cardNumber1 = "7771";
        String cardNumber2 = "7772";

        Card card1 = new Card(cardNumber1, false, "Tester", "7000000001",
                dateFrom, dateTo);
        Card card2 = new Card(cardNumber2, false, "Tester", "7000000002",
                dateFrom, dateTo);

        String plan1 = "plan1";
        String plan2 = "plan2";
        card1.setDiscountPlan(plan1);
        card2.setDiscountPlan(plan2);

        List<Card> cards = new ArrayList<Card>();
        cards.add(card1);
        cards.add(card2);

        List<String> alreadySubscribed = new ArrayList<String>();
        service.skipAlreadySubscribed(cards, plan1, alreadySubscribed);
        assertTrue(alreadySubscribed.get(0).equals(cardNumber1));

        alreadySubscribed.clear();
        service.skipAlreadySubscribed(cards, plan2, alreadySubscribed);
        assertTrue(alreadySubscribed.get(0).equals(cardNumber2));
    }

    @Test
    public void testUpdateCardDiscountPlan() throws InterruptedException,
            AmazonServiceException, IllegalArgumentException,
            ItemSizeLimitExceededException {

        DateTime dateFrom = new DateTime();
        DateTime dateTo = new DateTime(System.currentTimeMillis() + 10000);
        String cardNumber1 = "7771";
        String cardNumber2 = "7772";
        String cardNumber3 = "7773";

        Card card1 = new Card(cardNumber1, false, "Tester", "7000000001",
                dateFrom, dateTo);
        Card card2 = new Card(cardNumber2, false, "Tester", "7000000002",
                dateFrom, dateTo);
        Card card3 = new Card(cardNumber3);

        String plan1 = "plan1";
        String plan2 = "plan2";
        card1.setDiscountPlan(plan1);
        card2.setDiscountPlan(plan2);

        List<Card> cards = new ArrayList<Card>();
        cards.add(card1);
        cards.add(card2);
        cards.add(card3);

        String newPlan = "new plan";

        when(helper.updateCards(cards)).thenReturn(new ArrayList<String>());

        Map<String, Boolean> updatedCards = new HashMap<String, Boolean>();
        Map<String, Boolean> activatedCards = new HashMap<String, Boolean>();
        List<String> notUpdatedCardList = new ArrayList<String>();

        service.updateCardDiscountPlan(newPlan, cards, updatedCards,
                activatedCards, notUpdatedCardList);

        verify(helper).updateCards(cards);

        assertEquals(3, updatedCards.keySet().size());
        assertEquals(3, activatedCards.keySet().size());

        assertTrue(updatedCards.get(cardNumber1));
        assertTrue(updatedCards.get(cardNumber2));
        assertTrue(updatedCards.get(cardNumber3));

        assertTrue(activatedCards.get(cardNumber1));
        assertTrue(activatedCards.get(cardNumber2));
        assertFalse(activatedCards.get(cardNumber3));
    }

    @Test
    public void testUpdateCardDate() throws InterruptedException,
            InvalidDateRangeException, AmazonServiceException,
            IllegalArgumentException, ItemSizeLimitExceededException {

        DateTime dateNow = new DateTime(DateTimeZone.UTC);
        DateTime dateEnd = DateTime.now(DateTimeZone.UTC).plusDays(5);
        String cardNumber1 = "7771";
        String cardNumber2 = "7772";
        String cardNumber3 = "7773";

        String fromDate = dateNow.getDayOfMonth() + "."
                + dateNow.getMonthOfYear() + "." + dateNow.getYear();
        String toDate = dateEnd.getDayOfMonth() + "."
                + dateEnd.getMonthOfYear() + "." + dateEnd.getYear();

        Card card1 = new Card(cardNumber1, false, "Tester", "7000000001",
                new DateTime(DateTimeZone.UTC), new DateTime(DateTimeZone.UTC));
        Card card2 = new Card(cardNumber2, false, "Tester", "7000000002",
                new DateTime(DateTimeZone.UTC), new DateTime(DateTimeZone.UTC));
        Card card3 = new Card(cardNumber3);

        String plan1 = "plan1";
        String plan2 = "plan2";
        card1.setDiscountPlan(plan1);
        card2.setDiscountPlan(plan2);

        List<Card> cards = new ArrayList<Card>();
        cards.add(card1);
        cards.add(card2);
        cards.add(card3);

        when(helper.updateCards(cards)).thenReturn(new ArrayList<String>());

        Map<String, Boolean> updatedCards = new HashMap<String, Boolean>();
        Map<String, Boolean> activatedCards = new HashMap<String, Boolean>();
        List<String> notUpdatedCardList = new ArrayList<String>();

        service.updateCardDate(fromDate, toDate, cards, updatedCards,
                activatedCards, notUpdatedCardList);

        verify(helper).updateCards(cards);

        assertEquals(3, updatedCards.keySet().size());
        assertEquals(3, activatedCards.keySet().size());

        assertTrue(updatedCards.get(cardNumber1));
        assertTrue(updatedCards.get(cardNumber2));
        assertTrue(updatedCards.get(cardNumber3));

        assertTrue(activatedCards.get(cardNumber1));
        assertTrue(activatedCards.get(cardNumber2));
        assertFalse(activatedCards.get(cardNumber3));
    }

    @Test
    public void testGetUpdatedActiveCards() {

        DateTime dateFrom = new DateTime();
        DateTime dateTo = new DateTime(System.currentTimeMillis() + 10000);
        String cardNumber1 = "7771";
        String cardNumber2 = "7772";
        String cardNumber3 = "7773";

        Card card1 = new Card(cardNumber1, false, "Tester", "7000000001",
                dateFrom, dateTo);
        Card card2 = new Card(cardNumber2, false, "Tester", "7000000002",
                dateFrom, dateTo);
        Card card3 = new Card(cardNumber3, false, "Tester", "7000000003",
                dateFrom, dateTo);

        List<Card> cards = new ArrayList<Card>();
        cards.add(card1);
        cards.add(card2);
        cards.add(card3);

        List<String> activeNumbers = new ArrayList<String>();
        activeNumbers.add(cardNumber3);

        List<Card> result = service.getUpdatedActiveCards(cards, activeNumbers);
        assertFalse(result.isEmpty());
        assertEquals(cardNumber3, result.get(0).getNumber());
    }

    @Test
    public void testPrepareCardNumbers() {

        String cardNumber1 = "7771";
        String cardNumber2 = "7772";

        List<String> cardNumbers = Arrays.asList(cardNumber1, cardNumber2,
                cardNumber2, "", cardNumber2);
        List<String> numbersToUpdate = new ArrayList<String>();
        List<String> duplicates = new ArrayList<String>();

        service.prepareCardNumbers(cardNumbers, numbersToUpdate, duplicates);

        assertEquals(2, numbersToUpdate.size());
        assertTrue(numbersToUpdate.contains(cardNumber1));
        assertTrue(numbersToUpdate.contains(cardNumber2));
        assertEquals(1, duplicates.size());
        assertTrue(duplicates.contains(cardNumber2));
    }

    @Test
    public void testLoadCardsByNumbers() throws RuntimeException,
            InterruptedException, IOException {

        String cardNumber1 = "7771";
        String cardNumber2 = "7772";

        Card card1 = new Card(cardNumber1);
        Card card2 = new Card(cardNumber2);

        List<String> cardNumbers = Arrays.asList(cardNumber1, cardNumber2);
        List<Card> cards = new ArrayList<Card>();
        List<String> notLoadedCards = new ArrayList<String>();

        // все карты найдены
        BatchOperationResult<Card> cardsResult = new BatchOperationResult<Card>();
        cardsResult.getSuccessList().add(card1);
        cardsResult.getSuccessList().add(card2);

        when(helper.getCardsInBatchMode(eq(cardNumbers))).thenReturn(
                cardsResult);

        service.loadCardsByNumbers(cardNumbers, cards, notLoadedCards);

        verify(helper).getCardsInBatchMode(eq(cardNumbers));
        assertEquals(cardNumbers.size(), cards.size());
        assertTrue(notLoadedCards.isEmpty());

        // часть карт не найдена
        cards = new ArrayList<Card>();
        notLoadedCards = new ArrayList<String>();
        cardsResult.getSuccessList().remove(card2);

        when(helper.getCardsInBatchMode(eq(cardNumbers))).thenReturn(
                cardsResult);
        service.loadCardsByNumbers(cardNumbers, cards, notLoadedCards);

        verify(helper, times(2)).getCardsInBatchMode(eq(cardNumbers));
        assertEquals(1, cards.size());
        assertEquals(1, notLoadedCards.size());
        assertTrue(notLoadedCards.contains(cardNumber2));

        // часть карт не загрузилась
        cards = new ArrayList<Card>();
        notLoadedCards = new ArrayList<String>();

        cardsResult.getUnSuccessList().add(new ComplexKey(cardNumber2, null));

        when(helper.getCardsInBatchMode(eq(cardNumbers))).thenReturn(
                cardsResult);
        service.loadCardsByNumbers(cardNumbers, cards, notLoadedCards);

        verify(helper, times(3)).getCardsInBatchMode(eq(cardNumbers));
        assertEquals(1, cards.size());
        assertEquals(1, notLoadedCards.size());
        assertTrue(notLoadedCards.contains(cardNumber2));
    }

    @Test
    public void testUpdateCardBalance() throws RuntimeException,
            InterruptedException {

        String cardNumber1 = "7771";
        String cardNumber2 = "7772";
        String cardNumber3 = "7773";
        String cardNumber4 = "7774";

        Card card1 = new Card(cardNumber1);
        card1.setLimitRemain(new BigDecimal("10.00"));
        Card card2 = new Card(cardNumber2);
        card2.setLimitRemain(new BigDecimal("15.00"));
        Card card3 = new Card(cardNumber3);
        card3.setLimitRemain(new BigDecimal("20.00"));
        Card card4 = new Card(cardNumber4);
        card4.setLimitRemain(null);

        List<Card> cards = Arrays.asList(card1, card2, card3, card4);

        Map<String, BigDecimal> values = new HashMap<String, BigDecimal>();
        values.put(cardNumber1, new BigDecimal("1.10"));
        values.put(cardNumber2, new BigDecimal("2.20"));
        values.put(cardNumber3, new BigDecimal("4.40"));
        values.put(cardNumber4, new BigDecimal("9.99"));

        Map<String, Boolean> updatedCards = new HashMap<String, Boolean>();
        Map<String, BigDecimal> updatedLimits = new HashMap<String, BigDecimal>();

        List<String> limitFieldList = new ArrayList<String>(1);
        limitFieldList.add(Card.FLD_LIMIT_REMAIN);
        when(helper.updateCardLimit(any(String.class), any(BigDecimal.class), any(BigDecimal.class)))
                .thenReturn(true);
        when(helper.updateCardWithNullConditions(any(Card.class),
                eq(limitFieldList))).thenReturn(true);

        // увеличение баланса до заданного значения
        service.updateCardBalance(values, cards, updatedCards,
                updatedLimits);

        verify(helper).updateCardWithNullConditions(card4, limitFieldList);
        verify(helper).updateCardLimit(eq(cardNumber1),
                eq(card1.getLimitRemain()), eq(new BigDecimal("11.10")));
        verify(helper).updateCardLimit(eq(cardNumber2),
                eq(card2.getLimitRemain()), eq(new BigDecimal("17.20")));
        verify(helper).updateCardLimit(eq(cardNumber3),
                eq(card3.getLimitRemain()), eq(new BigDecimal("24.40")));

        assertEquals(4, updatedCards.size());
        assertEquals(4, updatedLimits.size());
        assertTrue(updatedCards.get(cardNumber1));
        assertTrue(updatedCards.get(cardNumber2));
        assertTrue(updatedCards.get(cardNumber3));
        assertTrue(updatedCards.get(cardNumber4));
        assertEquals(new BigDecimal("11.10"), updatedLimits.get(cardNumber1));
        assertEquals(new BigDecimal("17.20"), updatedLimits.get(cardNumber2));
        assertEquals(new BigDecimal("24.40"), updatedLimits.get(cardNumber3));
        assertEquals(new BigDecimal("9.99"), updatedLimits.get(cardNumber4));
    }

    @Test
    public void testSendSmsAfterUpdateCardBalanceNew()
            throws ClientProtocolException, IOException {

        String cardNumber1 = "7771";
        String cardNumForSms = smsService
                .getCardNumLastFourDigits(cardNumber1);
        String telephoneNumber = "000 000 0000";

        Card card1 = new Card(cardNumber1);
        card1.setLimitRemain(new BigDecimal("10.00"));
        card1.setTelephoneNumber(telephoneNumber);

        List<Card> cards = Arrays.asList(card1);

        BigDecimal value = new BigDecimal("15.00");
        Map<String, BigDecimal> updatedLimits = new HashMap<String, BigDecimal>();
        updatedLimits.put(cardNumber1, card1.getLimitRemain());

        String message = "limit update message";

        List<Map<String, String>> recipients = new ArrayList<Map<String, String>>();
        Map<String, String> recipient = new HashMap<String, String>();
        recipient.put(telephoneNumber, message);

        recipients.add(recipient);
        when(
                messageSource.getMessage(
                        eq("msg.limitSet"),
                        eq(new Object[] {cardNumForSms,
                                value.toString()}),
                        eq((Locale) null)))
                .thenReturn(message);

        // отправка смс о пополнении баланса
        updatedLimits.put(cardNumber1, value);
        service.sendSmsAfterUpdateCardBalance(updatedLimits, cards);

        verify(smsService).sendMultpleMessages(eq(recipients));

        verify(messageSource).getMessage(eq("msg.limitSet"),
                eq(new Object[] {cardNumForSms, value.toString()}),
                eq((Locale) null));

    }

    @Test
    public void testCardsFileForBatchActivation() throws IOException {

        List<String> linesWithError = new ArrayList<String>();
        List<String> linesWithDuplicate = new ArrayList<String>();

        String testString = "222444\tIvan\t961-167-25-18\t25.11.2012\t30.11.2013"
                + '\n'
                + "222444\tIvan\t961-167-25-18\t25.11.2012\t30.11.2013"
                + '\n'
                + "222444\tIvan\t961-167-25-18\t25.11.2012\t30.11.2013\t25.11.2012\t30.11.2013";

        List<Card> cardList = service.getCardsFromCsvString(testString,
                linesWithError, linesWithDuplicate);

        assertEquals(1, cardList.size());

        assertEquals(1, linesWithDuplicate.size());
        assertEquals(1, linesWithError.size());

    }

    @Test
    public void testLoadCardList() throws RuntimeException,
            InterruptedException, IOException {
        List<Card> cardNumbers = new ArrayList<Card>();
        for (int i = 0; i < 15; i++) {
            Card card = new Card("2000" + i);
            card.setTelephoneNumber("7" + i % 4);
            cardNumbers.add(card);
        }
        List<String> notFoundCardsList = new ArrayList<String>();
        List<String> alreadyActivatedCardsList = new ArrayList<String>();

        List<String> duplicatePhones = new ArrayList<String>();
        List<String> duplicatePhonesInDb = new ArrayList<String>();

        BatchOperationResult<Card> cardsResult = new BatchOperationResult<Card>();
        for (int i = 0; i < 5; i++) {
            Card card = new Card("2000" + i);
            cardsResult.getSuccessList().add(card);
        }

        cardsResult.getSuccessList().get(0).setDiscountPlan("id1");
        cardsResult.getSuccessList().get(0).setHolderName("holder1");
        cardsResult.getSuccessList().get(1).setDiscountPlan("id2");
        cardsResult.getSuccessList().get(1).setHolderName("holder2");

        for (int i = 5; i < 10; i++) {
            ComplexKey cKey = new ComplexKey("2000" + i, null);
            cardsResult.getUnSuccessList().add(cKey);
        }

        when(helper.getCardsInBatchMode(anyListOf(String.class))).thenReturn(
                cardsResult);
        when(helper.isPhoneUsed("73")).thenReturn(true);

        List<Card> cards = service.loadCardList(cardNumbers, notFoundCardsList,
                alreadyActivatedCardsList, duplicatePhones,
                duplicatePhonesInDb, "all50");

        assertEquals(1, cards.size());
        assertEquals(cards.get(0).getNumber(), "20002");
        assertEquals(1, duplicatePhonesInDb.size());
        assertEquals(duplicatePhonesInDb.get(0), "20003");
        assertEquals(1, duplicatePhones.size());
        assertEquals(duplicatePhones.get(0), "20004");

        assertFalse(cards.get(0).isActive());

        // 20000, 20001
        assertEquals(2, alreadyActivatedCardsList.size());
        // 20005-20015
        assertEquals(10, notFoundCardsList.size());

        verify(helper).getCardsInBatchMode(anyListOf(String.class));
        verify(helper).isPhoneUsed("73");

    }

    @Test
    public void testGetSuccessfullyActivatedCardList() {
        List<Card> cardsForActivationList = new ArrayList<Card>();
        for (int i = 0; i < 10; i++) {
            Card card = new Card("2000" + i);
            cardsForActivationList.add(card);
        }

        List<String> unactivatedCardsList = new ArrayList<String>();
        for (int i = 3; i < 6; i++)
            unactivatedCardsList.add("2000" + i);

        List<Card> successfullyActivatedCardList = new ArrayList<Card>();

        List<String> cardList = service.getSuccessfullyActivatedCardList(
                cardsForActivationList, unactivatedCardsList,
                successfullyActivatedCardList);

        assertEquals(7, cardList.size());

        assertEquals(7, successfullyActivatedCardList.size());

    }

    @Test
    public void testUpdateCardOwner() throws DuplicatePhoneException,
            NotUpdatedException, ItemSizeLimitExceededException {
        String cardNumber = "777";
        String ownerName = "test user";
        String oldOwnerPhone = "1234567890";
        String newOwnerPhone = "0987654321";
        Card card = new Card(cardNumber, false, ownerName, oldOwnerPhone);

        when(helper.checkPhone(eq(newOwnerPhone))).thenReturn(null);
        when(helper.updateCard(eq(card))).thenReturn(true);

        service.updateCardOwner(card, oldOwnerPhone, newOwnerPhone);

        verify(helper).checkPhone(eq(newOwnerPhone));
        verify(helper).deleteTelephoneIndex(eq(oldOwnerPhone));
        verify(helper).updateCard(eq(card));
    }
}
