package ru.olekstra.service;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public final class CompletedTransactionDto {

    private List<Map<String, Object>> dataList;

    private static String CompletedTransaction_FLD_PERIOD = "Period";
    private static String CompletedTransaction_FLD_DATE_TIME = "DateTime";
    private static String CompletedTransaction_FLD_DRUGSTORE_ID = "DrugstoreId";
    private static String CompletedTransaction_FLD_MANUFACTURER = "Manufacturer";
    private static String CompletedTransaction_FLD_CARD_ID = "Card";
    private static String CompletedTransaction_FLD_PACK_COUNT = "Count";
    private static String CompletedTransaction_FLD_PACK_BARCODE = "Barcode";
    private static String CompletedTransaction_FLD_PACK_ID = "PackId";
    private static String CompletedTransaction_FLD_PACK_PRICE = "Price";
    private static String CompletedTransaction_FLD_SUM_WITHOUT_DISCOUNT = "SumWithoutDiscount";
    private static String CompletedTransaction_FLD_DISCOUNT_SUM = "DiscountSum";
    private static String CompletedTransaction_FLD_DISCOUNT_VALUE = "DiscountValue";
    private static String CompletedTransaction_FLD_REFUND_SUM = "Refund";

    public CompletedTransactionDto() {
        super();
        this.setDataList();
    }

    public List<Map<String, Object>> getDataList() {
        return dataList;
    }

    public void setDataList() {
        List<Map<String, Object>> list = new LinkedList<Map<String, Object>>();
        Map<String, Object> data = new HashMap<String, Object>();
        data.put(CompletedTransaction_FLD_PERIOD, new String("201208"));
        data.put(CompletedTransaction_FLD_DATE_TIME, "27.08.2012 11:00:01");
        data.put(CompletedTransaction_FLD_DRUGSTORE_ID, "drugstoreId1");
        data.put(CompletedTransaction_FLD_MANUFACTURER, new String("first"));
        data.put(CompletedTransaction_FLD_CARD_ID, new String("222"));
        data.put(CompletedTransaction_FLD_PACK_COUNT, "1");
        data.put(CompletedTransaction_FLD_PACK_BARCODE, "460001");
        data.put(CompletedTransaction_FLD_PACK_ID, "200001");
        data.put(CompletedTransaction_FLD_PACK_PRICE, "80.00");
        data.put(CompletedTransaction_FLD_SUM_WITHOUT_DISCOUNT, "80.00");
        data.put(CompletedTransaction_FLD_DISCOUNT_SUM, "40.00");
        data.put(CompletedTransaction_FLD_DISCOUNT_VALUE, "50.00");
        data.put(CompletedTransaction_FLD_REFUND_SUM, "40.00");
        list.add(data);
        data = new HashMap<String, Object>();
        data.put(CompletedTransaction_FLD_PERIOD, new String("201208"));
        data.put(CompletedTransaction_FLD_DATE_TIME, "31.08.2012 23:59:59");
        data.put(CompletedTransaction_FLD_DRUGSTORE_ID, "drugstoreId1");
        data.put(CompletedTransaction_FLD_MANUFACTURER, new String("first"));
        data.put(CompletedTransaction_FLD_CARD_ID, new String("222"));
        data.put(CompletedTransaction_FLD_PACK_COUNT, "1");
        data.put(CompletedTransaction_FLD_PACK_BARCODE, "460001");
        data.put(CompletedTransaction_FLD_PACK_ID, "200001");
        data.put(CompletedTransaction_FLD_PACK_PRICE, "80.00");
        data.put(CompletedTransaction_FLD_SUM_WITHOUT_DISCOUNT, "80.00");
        data.put(CompletedTransaction_FLD_DISCOUNT_SUM, "40.00");
        data.put(CompletedTransaction_FLD_DISCOUNT_VALUE, "50.00");
        data.put(CompletedTransaction_FLD_REFUND_SUM, "40.00");
        list.add(data);
        data = new HashMap<String, Object>();
        data.put(CompletedTransaction_FLD_PERIOD, "201208");
        data.put(CompletedTransaction_FLD_DATE_TIME, "07.08.2012 14:50:11");
        data.put(CompletedTransaction_FLD_DRUGSTORE_ID, "drugstoreId1");
        data.put(CompletedTransaction_FLD_MANUFACTURER, "first");
        data.put(CompletedTransaction_FLD_CARD_ID, "444");
        data.put(CompletedTransaction_FLD_PACK_COUNT, "2");
        data.put(CompletedTransaction_FLD_PACK_BARCODE, "460002");
        data.put(CompletedTransaction_FLD_PACK_ID, "200002");
        data.put(CompletedTransaction_FLD_PACK_PRICE, "12.35");
        data.put(CompletedTransaction_FLD_SUM_WITHOUT_DISCOUNT, "24.70");
        data.put(CompletedTransaction_FLD_DISCOUNT_SUM, "6.17");
        data.put(CompletedTransaction_FLD_DISCOUNT_VALUE, "49.5");
        data.put(CompletedTransaction_FLD_REFUND_SUM, "6.18");
        list.add(data);
        data = new HashMap<String, Object>();
        data.put(CompletedTransaction_FLD_PERIOD, "201208");
        data.put(CompletedTransaction_FLD_DATE_TIME, "01.08.2012 00:00:00");
        data.put(CompletedTransaction_FLD_DRUGSTORE_ID, "drugstoreId1");
        data.put(CompletedTransaction_FLD_MANUFACTURER, "second");
        data.put(CompletedTransaction_FLD_CARD_ID, "666");
        data.put(CompletedTransaction_FLD_PACK_COUNT, "1");
        data.put(CompletedTransaction_FLD_PACK_BARCODE, "460003");
        data.put(CompletedTransaction_FLD_PACK_ID, "200003");
        data.put(CompletedTransaction_FLD_PACK_PRICE, "119.99");
        data.put(CompletedTransaction_FLD_SUM_WITHOUT_DISCOUNT, "119.99");
        data.put(CompletedTransaction_FLD_DISCOUNT_SUM, "60.00");
        data.put(CompletedTransaction_FLD_DISCOUNT_VALUE, "50");
        data.put(CompletedTransaction_FLD_REFUND_SUM, "59.99");
        list.add(data);
        this.dataList = list;
    }

}
