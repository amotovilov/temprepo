package ru.olekstra.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import ru.olekstra.awsutils.DynamodbEntity;
import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.S3Service;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.awsutils.exception.ReporNotFoundException;
import ru.olekstra.awsutils.exception.S3ConnectionException;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.common.service.DateTimeService;
import ru.olekstra.domain.AuthCodeIndex;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.Recipe;
import ru.olekstra.domain.RecipeState;
import ru.olekstra.exception.NotSavedException;
import ru.olekstra.helper.RecipeHelper;
import ru.olekstra.service.dto.RecipeReportGroupDto;

import com.amazonaws.services.dynamodb.model.ProvisionedThroughputExceededException;

@RunWith(MockitoJUnitRunner.class)
public class RecipeServiceTest {

    private RecipeService service;
    private RecipeHelper helper;
    private S3Service s3Service;
    private AppSettingsService settings;
    private DynamodbService dynamoService;
    private ReportService reportService;
    private DateTimeService dateTimeService;

    @Before
    public void setup() {
        helper = mock(RecipeHelper.class);
        s3Service = mock(S3Service.class);
        settings = mock(AppSettingsService.class);
        dynamoService = mock(DynamodbService.class);
        reportService = mock(ReportService.class);
        dateTimeService = mock(DateTimeService.class);

        service = new RecipeService(helper, s3Service, settings, dynamoService,
                reportService, dateTimeService);
    }

    @Test
    public void testIsValidImageFormatString() {
        String fileName1 = "test.jpg";
        String fileName2 = "TEST.PNG";
        String fileName3 = "test.gif";
        String fileName4 = "test.txt";
        String fileName5 = "test.jpg.txt";
        String fileName6 = "";
        String fileName7 = null;

        assertTrue(service.isValidImageFormat(fileName1));
        assertTrue(service.isValidImageFormat(fileName2));

        assertFalse(service.isValidImageFormat(fileName3));
        assertFalse(service.isValidImageFormat(fileName4));
        assertFalse(service.isValidImageFormat(fileName5));
        assertFalse(service.isValidImageFormat(fileName6));
        assertFalse(service.isValidImageFormat(fileName7));
    }

    @Test
    public void testValidJpegImageFormatCheck() throws IOException {
        String fileName = "image.jpg";
        String jpgExt = "jpg";
        String jpegExt = "jpeg";
        byte[] fileData = new byte[] {(byte) 0xFF, (byte) 0xD8, (byte) 0xFF,
                (byte) 0xE0};
        MultipartFile fileJpg = new MockMultipartFile(fileName, fileData);
        boolean isValid = service.isValidImageFormat(fileJpg.getInputStream(),
                jpgExt);
        assertTrue(isValid);

        MultipartFile fileJpeg = new MockMultipartFile(fileName, fileData);
        isValid = service
                .isValidImageFormat(fileJpeg.getInputStream(), jpegExt);
        assertTrue(isValid);

    }

    public void testValidPngImageFormatCheck() throws IOException {
        String fileName = "image.png";
        String fileExt = "png";
        byte[] fileData = new byte[] {(byte) 0x89, (byte) 0x50, (byte) 0x4E,
                (byte) 0x47};
        MultipartFile file = new MockMultipartFile(fileName, fileData);
        boolean isValid = service.isValidImageFormat(file.getInputStream(),
                fileExt);
        assertTrue(isValid);
    }

    @Test
    public void testIllegalImageExtensionFormatCheck() throws IOException {
        String fileName = "image.dat";
        String fileExt = "dat";
        // "левое" имя файла, при этом дескриптор как у jpg
        byte[] fileData = new byte[] {(byte) 0xFF, (byte) 0xD8, (byte) 0xFF,
                (byte) 0xE0};
        MultipartFile file = new MockMultipartFile(fileName, fileData);
        boolean isValid = service.isValidImageFormat(file.getInputStream(),
                fileExt);
        assertFalse(isValid);
    }

    @Test
    public void testIllegalImageDescriptorFormatCheck() throws IOException {
        String fileName = "image.jpg";
        String fileExt = "jpg";
        // правильное расширение файла, при этом содержимое "левое"
        byte[] fileData = new byte[] {(byte) 0xD0, (byte) 0x18, (byte) 0x20,
                (byte) 0xF0};
        MultipartFile file = new MockMultipartFile(fileName, fileData);
        boolean isValid = service.isValidImageFormat(file.getInputStream(),
                fileExt);
        assertFalse(isValid);
    }

    @Test
    public void testSaveRecipeImage() {
        String filePath = "period/drugstore id";
        String bucket = "recipe.test.bucket";
        InputStream imageStream = new ByteArrayInputStream("test data"
                .getBytes());

        when(settings.getRecipeBucketName()).thenReturn(bucket);

        service.saveRecipeImage(filePath, imageStream);

        verify(settings).getRecipeBucketName();
    }

    @Test
    public void testGetImageExtension() {
        String fileName1 = "test.jpg";
        String fileName2 = "test.png";

        assertEquals(RecipeService.IMAGE_FILE_FORMAT_JPG, service
                .getImageExtension(fileName1));
        assertEquals(RecipeService.IMAGE_FILE_FORMAT_PNG, service
                .getImageExtension(fileName2));
    }

    @Test
    public void testGenarateImageName() {
        String uuid = UUID.randomUUID().toString();
        String fileExtension = "jpg";
        String referenceValue = uuid + "." + fileExtension;

        assertEquals(referenceValue, service.genarateImageName(uuid,
                fileExtension));
    }

    @Test
    public void testConvertRecipePeriod() {
        String periodToConvert = "02.2013";
        String expetedPeriod = "201302";

        assertEquals(expetedPeriod, service
                .convertRecipePeriod(periodToConvert));
    }

    @Test
    public void testSaveRecipe() throws ItemSizeLimitExceededException {
        String period = "201302";
        String uuid = UUID.randomUUID().toString();
        String state = "new";
        String drugstoreId = "test drugstore";
        String fileName = "test.jpg";
        String fileType = "jpg";
        Recipe recipe = new Recipe(period, uuid, state, drugstoreId, fileName,
                fileType, new DateTime());

        when(helper.addRecipe(eq(recipe))).thenReturn(true);

        boolean result = service.saveRecipe(recipe);

        verify(helper).addRecipe(eq(recipe));
        assertTrue(result);
    }

    @Test
    public void testUpdateRecipeStateWithoutUpdate()
            throws ItemSizeLimitExceededException, JsonGenerationException,
            JsonMappingException, IOException, NotSavedException {
        String period = "201302";
        String uuid = UUID.randomUUID().toString();
        String drugstoreId = "test drugstore";
        String oldState = Recipe.STATE_NEW;
        String newState = oldState;
        String user = "test user";
        String note = "test note";
        String fileName = "test.jpg";
        String fileType = "jpg";
        Recipe recipe = new Recipe(period, uuid, newState, drugstoreId,
                fileName, fileType, new DateTime());

        when(helper.addRecipeState(any(RecipeState.class))).thenReturn(true);
        when(helper.saveHistory(eq(period), eq(uuid), any(String.class)))
                .thenReturn(true);
        when(
                helper.updateRecipeState(eq(period), eq(uuid), eq(oldState),
                        eq(newState))).thenReturn(true);

        service.updateRecipeState(recipe, newState, note, user);

        verify(helper).addRecipeState(any(RecipeState.class));
        verify(helper).saveHistory(eq(period), eq(uuid), any(String.class));
        verify(helper, times(0)).updateRecipeState(eq(period), eq(uuid),
                eq(oldState), eq(newState));
    }

    @Test
    public void testUpdateRecipeState() throws ItemSizeLimitExceededException,
            JsonGenerationException, JsonMappingException, IOException,
            NotSavedException {
        String period = "201302";
        String uuid = UUID.randomUUID().toString();
        String drugstoreId = "test drugstore";
        String oldState = Recipe.STATE_NEW;
        String newState = Recipe.STATE_CHECKED;
        String user = "test user";
        String note = "test note";
        String fileName = "test.jpg";
        String fileType = "jpg";
        DateTime nowDateTime = DateTime.now();

        Recipe recipe = new Recipe(period, uuid, oldState, drugstoreId,
                fileName, fileType, new DateTime());
        RecipeState tmpRecipeState = new RecipeState(period, oldState, uuid,
                drugstoreId, "1044", nowDateTime);

        when(helper.getRecipeState(oldState, period, uuid)).thenReturn(
                tmpRecipeState);
        when(helper.addRecipeState(any(RecipeState.class))).thenReturn(true);
        when(helper.saveHistory(eq(period), eq(uuid), any(String.class)))
                .thenReturn(true);
        when(
                helper.updateRecipeState(eq(period), eq(uuid), eq(oldState),
                        eq(newState))).thenReturn(true);

        service.updateRecipeState(recipe, newState, note, user);

        verify(helper).getRecipeState(eq(oldState), eq(period), eq(uuid));
        verify(helper).addRecipeState(any(RecipeState.class));
        verify(helper).saveHistory(eq(period), eq(uuid), any(String.class));
        verify(helper).updateRecipeState(eq(period), eq(uuid), eq(oldState),
                eq(newState));
    }

    @Test
    public void testGetNewRecipeWithStateX() throws NotSavedException, IOException {

        String state = Recipe.STATE_NEW;
        // String operator1 = "operator1";
        String operator2 = "operator2";

        Map<String, Object> recipeStateMap = new HashMap<String, Object>();
        recipeStateMap.put("State", state);
        recipeStateMap.put("Period#UUID", "period#UUID");
        recipeStateMap.put("DrugstoreId", "drugstoreId");

        // второй вызов возвращает null для предотвращения зацикливания
        when(helper.getNextRecipeWithStateX(state, null)).thenReturn(
                recipeStateMap).thenReturn(null);

        when(helper.saveOperatorAndTime(recipeStateMap, operator2)).thenReturn(
                true);

        // полностью новый рецепт (оператор - пусто, время - пусто)
        RecipeState recipeStateItem3 = service.getNextRecipeWithStateX(state,
                operator2);
        assertNotNull(recipeStateItem3);
        verify(helper, times(1)).getNextRecipeWithStateX(state, null);

    }

    @Test
    public void testGetOutOfDateRecipeWithStateX() throws NotSavedException, IOException {

        String state = Recipe.STATE_NEW;
        String operator1 = "operator1";
        String operator2 = "operator2";

        Map<String, Object> recipeStateMap = new HashMap<String, Object>();
        recipeStateMap.put("State", state);
        recipeStateMap.put("Period#UUID", "period#UUID");
        recipeStateMap.put("DrugstoreId", "drugstoreId");
        recipeStateMap.put("ProcessingBy", operator1);
        recipeStateMap.put("ProcessingStarted", "01.01.2013 00:00:00 +0400");

        // второй вызов возвращает null для предотвращения зацикливания
        when(helper.getNextRecipeWithStateX(state, null)).thenReturn(
                recipeStateMap).thenReturn(null);

        when(helper.saveOperatorAndTime(recipeStateMap, operator2)).thenReturn(
                true);

        // чужой, просроченный
        RecipeState recipeStateItem1 = service.getNextRecipeWithStateX(state,
                operator2);
        assertNotNull(recipeStateItem1);
        verify(helper, times(1)).getNextRecipeWithStateX(state, null);

    }

    @Test
    public void testGetOutOfDateOwnRecipeWithStateX() throws NotSavedException, IOException {

        String state = Recipe.STATE_NEW;
        String operator1 = "operator1";

        Map<String, Object> recipeStateMap = new HashMap<String, Object>();
        recipeStateMap.put("State", state);
        recipeStateMap.put("Period#UUID", "period#UUID");
        recipeStateMap.put("DrugstoreId", "drugstoreId");
        recipeStateMap.put("ProcessingBy", operator1);
        recipeStateMap.put("ProcessingStarted", "01.01.2013 00:00:00 +0400");

        // второй вызов возвращает null для предотвращения зацикливания
        when(helper.getNextRecipeWithStateX(state, null)).thenReturn(
                recipeStateMap).thenReturn(null);

        when(helper.saveOperatorAndTime(recipeStateMap, operator1)).thenReturn(
                true);

        // свой, просроченный
        RecipeState recipeStateItem2 = service.getNextRecipeWithStateX(state,
                operator1);
        assertNotNull(recipeStateItem2);
        verify(helper, times(1)).getNextRecipeWithStateX(state, null);

    }

    @Test
    public void testGetOwnRecipeWithStateX() throws NotSavedException, IOException {

        String state = Recipe.STATE_NEW;
        String operator1 = "operator1";

        Map<String, Object> recipeStateMap = new HashMap<String, Object>();
        recipeStateMap.put("State", state);
        recipeStateMap.put("Period#UUID", "period#UUID");
        recipeStateMap.put("DrugstoreId", "drugstoreId");
        recipeStateMap.put("ProcessingBy", operator1);
        recipeStateMap.put("ProcessingStarted", DateTimeFormat.forPattern(
                DynamodbEntity.DB_DATETIME_FORMAT).print(
                DateTime.now().minusMinutes(1)));

        // второй вызов возвращает null для предотвращения зацикливания
        when(helper.getNextRecipeWithStateX(state, null)).thenReturn(
                recipeStateMap).thenReturn(null);

        when(helper.saveOperatorAndTime(recipeStateMap, operator1)).thenReturn(
                true);

        // свой, в работе (чтобы продолжить в случае аварийного завершения
        // работы оператора)
        RecipeState recipeStateItem2 = service.getNextRecipeWithStateX(state,
                operator1);
        assertNotNull(recipeStateItem2);
        verify(helper, times(1)).getNextRecipeWithStateX(state, null);

    }

    @Test
    public void testGetPairRecipeWithStateX() throws NotSavedException, IOException {
        String state = Recipe.STATE_NEW;
        String operator1 = "operator1";
        String operator2 = "operator2";

        Map<String, Object> recipeStateMap1 = new HashMap<String, Object>();
        recipeStateMap1.put("State", state);
        recipeStateMap1.put("Period#UUID", "period#UUID");
        recipeStateMap1.put("DrugstoreId", "drugstoreId");
        recipeStateMap1.put("ProcessingBy", operator1);
        recipeStateMap1.put("ProcessingStarted", DateTimeFormat.forPattern(
                DynamodbEntity.DB_DATETIME_FORMAT).print(
                DateTime.now().plusDays(1)));

        Map<String, Object> recipeStateMap2 = new HashMap<String, Object>();
        recipeStateMap2.put("State", state);
        recipeStateMap2.put("Period#UUID", "period#UUID");
        recipeStateMap2.put("DrugstoreId", "drugstoreId");
        recipeStateMap2.put("ProcessingBy", operator1);
        recipeStateMap2.put("ProcessingStarted", "01.01.2013 00:00:00 +0400");

        // в цикле сначала возвращаем "чужой в работе", затем
        // "чужой просроченный"
        when(helper.getNextRecipeWithStateX(state, null)).thenReturn(
                recipeStateMap1).thenReturn(recipeStateMap2).thenReturn(null);

        when(helper.saveOperatorAndTime(anyMap(), anyString()))
                .thenReturn(true);

        RecipeState recipeStateItem4 = service.getNextRecipeWithStateX(state,
                operator2);

        assertNotNull(recipeStateItem4);
        verify(helper, times(2)).getNextRecipeWithStateX(state, null);
    }

    @Test
    public void testGetEmptyRecipeWithStateX() throws NotSavedException, IOException {

        String state = Recipe.STATE_NEW;
        String operator = "operator1";

        Map<String, Object> recipeStateMap = new HashMap<String, Object>();

        // не найдено подходящих в БД
        when(helper.getNextRecipeWithStateX(state, null)).thenReturn(
                recipeStateMap);

        when(helper.saveOperatorAndTime(recipeStateMap, operator)).thenReturn(
                true);

        RecipeState recipeStateItem = service.getNextRecipeWithStateX(state,
                operator);

        assertNull(recipeStateItem);
        verify(helper, times(1)).getNextRecipeWithStateX(state, null);
    }

    @Test
    public void testGetTransaction() {

        String period = "201302";
        String drugstoreId = "drugstore";
        String authCode = "1000";
        String transactionPeriod = "20130201";
        DateTime transactionPeriodDateTime = DateTimeFormat.forPattern(
                AuthCodeIndex.DB_DATE_FORMAT).parseDateTime(transactionPeriod);
        UUID transactionUUID = UUID.randomUUID();

        ProcessingTransaction transaction = new ProcessingTransaction();
        transaction.setVoucherId(transactionUUID);
        String rangeKey = AuthCodeIndex.dbDateFormatter
                .print(transactionPeriodDateTime)
                + "#" + transactionUUID.toString();
        AuthCodeIndex dummyIndex = new AuthCodeIndex(
                AuthCodeIndex.dateMonthFormatter.parseDateTime(period),
                authCode, drugstoreId, rangeKey);

        when(
                dynamoService.getObject(AuthCodeIndex.class, AuthCodeIndex
                        .composeKey(period, authCode, drugstoreId),
                        AuthCodeIndex.prefix)).thenReturn(dummyIndex);
        when(
                dynamoService.getObject(ProcessingTransaction.class,
                        drugstoreId, rangeKey)).thenReturn(transaction);

        ProcessingTransaction actualTransaction = service.getTransaction(
                period, drugstoreId, authCode);

        assertEquals(transaction, actualTransaction);

        verify(dynamoService).getObject(AuthCodeIndex.class,
                AuthCodeIndex.composeKey(period, authCode, drugstoreId),
                AuthCodeIndex.prefix);
        verify(dynamoService).getObject(ProcessingTransaction.class,
                drugstoreId, rangeKey);
    }

    @Test
    public void testGetRecipe() {
        String period = "201302";
        String uuid = UUID.randomUUID().toString();
        String drugstoreId = "test drugstore";
        String oldState = Recipe.STATE_NEW;
        String fileName = "test.jpg";
        String fileType = "jpg";

        Recipe referenceRecipe = new Recipe(period, uuid, oldState,
                drugstoreId, fileName, fileType, new DateTime());

        when(helper.getRecipe(eq(period), eq(uuid)))
                .thenReturn(referenceRecipe);

        Recipe recipe = service.getRecipe(period, uuid);
        assertEquals(referenceRecipe, recipe);

        verify(helper).getRecipe(eq(period), eq(uuid));
    }

    @Test
    public void testSaveRecipeRejectReason() {
        String period = "201302";
        String uuid = UUID.randomUUID().toString();
        String rejectReason = "test reject reason";

        when(
                helper.saveRecipeRejectReason(eq(period), eq(uuid),
                        eq(rejectReason))).thenReturn(true);

        boolean result = service.saveRecipeRejectReason(period, uuid,
                rejectReason);

        verify(helper).saveRecipeRejectReason(eq(period), eq(uuid),
                eq(rejectReason));
        assertTrue(result);
    }

    @Test
    public void testGetActiveRecipiesForPeriod() throws ProvisionedThroughputExceededException, IOException {

        String period = "201302";
        List<RecipeState> list = new ArrayList<RecipeState>();
        list.add(new RecipeState());

        when(helper.getActiveRecipesForPeriod(period)).thenReturn(list);

        List<RecipeState> result = service.getActiveRecipesForPeriod(period);

        assertNotNull(result);
        assertEquals(list, result);
        assertEquals(list.size(), result.size());
    }

    @Test
    public void testGetActiveRecipiesForPeriodWithEmptyResult() throws ProvisionedThroughputExceededException,
            IOException {

        String period = "201302";
        List<RecipeState> list = new ArrayList<RecipeState>();

        when(helper.getActiveRecipesForPeriod(period)).thenReturn(null);

        List<RecipeState> result = service.getActiveRecipesForPeriod(period);

        assertNotNull(result);
        assertEquals(0, result.size());

        when(helper.getActiveRecipesForPeriod(period)).thenReturn(list);

        result = service.getActiveRecipesForPeriod(period);

        assertNotNull(result);
        assertEquals(0, result.size());
    }

    @Test
    public void testGetHtmlReportList() throws S3ConnectionException,
            ReporNotFoundException, IOException {
        String period = "201302";
        String reportLinks = "test";

        when(reportService.getHtmlReportList(eq(period),
                eq(ReportService.RECIPE_REPORT_TYPE))).thenReturn(reportLinks);

        String result = service.getHtmlReportList(period);

        assertEquals(reportLinks, result);
    }

    @Test
    public void testGenerateReport() throws RuntimeException, IOException {
        String period = "201302";
        String uuid1 = UUID.randomUUID().toString();
        String uuid2 = UUID.randomUUID().toString();
        String drugstoreId = "test drugstore";
        String fileName = "test.jpg";
        String fileType = "jpg";
        DateTime date = new DateTime();

        Recipe recipe1 = new Recipe(period, uuid1, Recipe.STATE_NEW,
                drugstoreId, fileName, fileType, date);
        Recipe recipe2 = new Recipe(period, uuid2, Recipe.STATE_PARSED,
                drugstoreId, fileName, fileType, date);
        List<Recipe> recipes = Arrays.asList(recipe1, recipe2);

        String s3FileName1 = uuid1 + "." + fileType;
        String s3FileName2 = uuid2 + "." + fileType;
        List<String> s3RecipeFiles = Arrays.asList(s3FileName1, s3FileName2);

        String filePath1 = period + "/" + drugstoreId + "/" + uuid1 + "."
                + fileType;
        String filePath2 = period + "/" + drugstoreId + "/" + uuid2 + "."
                + fileType;
        URL recipeUrl1 = new URL(
                "http://recipe.backoffice.olekstra.ru.s3.amazonaws.com/"
                        + filePath1);
        URL recipeUrl2 = new URL(
                "http://recipe.backoffice.olekstra.ru.s3.amazonaws.com/"
                        + filePath2);
        URL reportUrl = new URL(
                "http://recipe.backoffice.olekstra.ru.s3.amazonaws.com/"
                        + period + "/" + "reportList.html");

        when(
                s3Service.getChildsByParent(
                        eq(AppSettingsService.DEFAULT_RECIPE_REPORT_BUCKET),
                        eq(period))).thenReturn(s3RecipeFiles);
        when(helper.getRecipesForPeriod(eq(period), eq(s3RecipeFiles)))
                .thenReturn(recipes);
        when(
                s3Service.getUrl(
                        eq(AppSettingsService.DEFAULT_RECIPE_REPORT_BUCKET),
                        eq(filePath1))).thenReturn(recipeUrl1);
        when(
                s3Service.getUrl(
                        eq(AppSettingsService.DEFAULT_RECIPE_REPORT_BUCKET),
                        eq(filePath2))).thenReturn(recipeUrl2);
        when(
                reportService.saveReportToS3(eq(period), eq(drugstoreId),
                        eq(ReportService.RECIPE_REPORT_TYPE),
                        any(Object.class), eq((DateTimeFormatter) null)))
                .thenReturn(reportUrl);

        service.generateReport(period);

        verify(s3Service)
                .getChildsByParent(
                        eq(AppSettingsService.DEFAULT_RECIPE_REPORT_BUCKET),
                        eq(period));
        verify(helper).getRecipesForPeriod(eq(period), eq(s3RecipeFiles));
        verify(s3Service).getUrl(
                eq(AppSettingsService.DEFAULT_RECIPE_REPORT_BUCKET),
                eq(filePath1));
        verify(s3Service).getUrl(
                eq(AppSettingsService.DEFAULT_RECIPE_REPORT_BUCKET),
                eq(filePath2));
        verify(reportService).saveReportToS3(eq(period), eq(drugstoreId),
                eq(ReportService.RECIPE_REPORT_TYPE), any(Object.class),
                eq((DateTimeFormatter) null));
    }

    @Test
    public void testMakeReportRecipeGroup() throws MalformedURLException {
        String month = "MMMM yyyy";
        String period = "201302";
        String uuid1 = UUID.randomUUID().toString();
        String uuid2 = UUID.randomUUID().toString();
        String uuid3 = UUID.randomUUID().toString();
        String uuid4 = UUID.randomUUID().toString();
        String uuid5 = UUID.randomUUID().toString();
        String drugstoreId = "test drugstore";
        String fileName = "test.jpg";
        String fileType = "jpg";
        DateTime date = new DateTime();

        Recipe recipe1 = new Recipe(period, uuid1, Recipe.STATE_NEW,
                drugstoreId, fileName, fileType, date);
        Recipe recipe2 = new Recipe(period, uuid2, Recipe.STATE_PARSED,
                drugstoreId, fileName, fileType, date);
        Recipe recipe3 = new Recipe(period, uuid3, Recipe.STATE_CHECKED,
                drugstoreId, fileName, fileType, date);
        Recipe recipe4 = new Recipe(period, uuid4, Recipe.STATE_PROBLEM,
                drugstoreId, fileName, fileType, date);
        Recipe recipe5 = new Recipe(period, uuid5, Recipe.STATE_REJECTED,
                drugstoreId, fileName, fileType, date);
        List<Recipe> recipes = Arrays.asList(recipe1, recipe2, recipe3,
                recipe4, recipe5);

        String filePath1 = period + "/" + drugstoreId + "/" + uuid1 + "."
                + fileType;
        String filePath2 = period + "/" + drugstoreId + "/" + uuid2 + "."
                + fileType;
        String filePath3 = period + "/" + drugstoreId + "/" + uuid3 + "."
                + fileType;
        String filePath4 = period + "/" + drugstoreId + "/" + uuid4 + "."
                + fileType;
        String filePath5 = period + "/" + drugstoreId + "/" + uuid5 + "."
                + fileType;

        URL recipeUrl1 = new URL(
                "http://recipe.backoffice.olekstra.ru.s3.amazonaws.com/"
                        + filePath1);
        URL recipeUrl2 = new URL(
                "http://recipe.backoffice.olekstra.ru.s3.amazonaws.com/"
                        + filePath2);
        URL recipeUrl3 = new URL(
                "http://recipe.backoffice.olekstra.ru.s3.amazonaws.com/"
                        + filePath3);
        URL recipeUrl4 = new URL(
                "http://recipe.backoffice.olekstra.ru.s3.amazonaws.com/"
                        + filePath4);

        URL recipeUrl5 = new URL(
                "http://recipe.backoffice.olekstra.ru.s3.amazonaws.com/"
                        + filePath5);

        when(
                s3Service.getUrl(
                        eq(AppSettingsService.DEFAULT_RECIPE_REPORT_BUCKET),
                        eq(filePath1))).thenReturn(recipeUrl1);
        when(
                s3Service.getUrl(
                        eq(AppSettingsService.DEFAULT_RECIPE_REPORT_BUCKET),
                        eq(filePath2))).thenReturn(recipeUrl2);
        when(
                s3Service.getUrl(
                        eq(AppSettingsService.DEFAULT_RECIPE_REPORT_BUCKET),
                        eq(filePath3))).thenReturn(recipeUrl3);
        when(
                s3Service.getUrl(
                        eq(AppSettingsService.DEFAULT_RECIPE_REPORT_BUCKET),
                        eq(filePath4))).thenReturn(recipeUrl4);
        when(
                s3Service.getUrl(
                        eq(AppSettingsService.DEFAULT_RECIPE_REPORT_BUCKET),
                        eq(filePath5))).thenReturn(recipeUrl5);

        RecipeReportGroupDto result = service.makeReportRecipeGroup(period,
                month, drugstoreId, recipes);

        verify(s3Service).getUrl(
                eq(AppSettingsService.DEFAULT_RECIPE_REPORT_BUCKET),
                eq(filePath1));
        verify(s3Service).getUrl(
                eq(AppSettingsService.DEFAULT_RECIPE_REPORT_BUCKET),
                eq(filePath2));
        verify(s3Service).getUrl(
                eq(AppSettingsService.DEFAULT_RECIPE_REPORT_BUCKET),
                eq(filePath3));
        verify(s3Service).getUrl(
                eq(AppSettingsService.DEFAULT_RECIPE_REPORT_BUCKET),
                eq(filePath4));
        verify(s3Service).getUrl(
                eq(AppSettingsService.DEFAULT_RECIPE_REPORT_BUCKET),
                eq(filePath5));

        assertEquals(5, result.getTotal());
        assertEquals(1, result.getRefund());
        assertEquals(1, result.getRejected());
        assertEquals(3, result.getProcessing());

        assertEquals(month, result.getMonth());
        assertEquals(drugstoreId, result.getDrugstore());
        assertEquals(recipes.size(), result.getRecipes().size());
    }

    @Test
    public void testGetRecipesByDrugstore() {
        String period = "201302";
        String uuid1 = UUID.randomUUID().toString();
        String uuid2 = UUID.randomUUID().toString();
        String drugstoreId1 = "test drugstore1";
        String drugstoreId2 = "test drugstore2";
        String fileName = "test.jpg";
        String fileType = "jpg";
        DateTime date = new DateTime();

        Recipe recipe1 = new Recipe(period, uuid1, Recipe.STATE_NEW,
                drugstoreId1, fileName, fileType, date);
        Recipe recipe2 = new Recipe(period, uuid2, Recipe.STATE_PARSED,
                drugstoreId2, fileName, fileType, date);

        List<Recipe> recipes = Arrays.asList(recipe1, recipe2);

        Map<String, List<Recipe>> result = service
                .getRecipesByDrugstore(recipes);

        assertEquals(recipes.size(), result.size());
        assertEquals(recipe1, result.get(drugstoreId1).get(0));
        assertEquals(recipe2, result.get(drugstoreId2).get(0));
    }
}
