package ru.olekstra.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import ru.olekstra.awsutils.ComplexKey;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.backoffice.dto.ImportDto;
import ru.olekstra.backoffice.dto.PackImportDto;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.domain.Pack;
import ru.olekstra.exception.InvalidFormatDataException;
import ru.olekstra.helper.PackHelper;

import com.amazonaws.AmazonServiceException;

@RunWith(MockitoJUnitRunner.class)
public class PackServiceTest {

    private PackHelper packHelper;
    private AppSettingsService settingsService;
    private PackService service;

    @Before
    public void setup() {
        packHelper = mock(PackHelper.class);
        settingsService = mock(AppSettingsService.class);
        service = new PackService(packHelper, settingsService);
    }

    @SuppressWarnings({"unchecked"})
    @Test
    public void testSavePacks() throws InvalidFormatDataException, IOException,
            InstantiationException, IllegalAccessException,
            InterruptedException, AmazonServiceException,
            IllegalArgumentException, ItemSizeLimitExceededException {

        List<Pack> packs = new ArrayList<Pack>();
        packs.add(new Pack("id", "ekt", "barcode", "declNumber", "name",
                "manufacturer", "synonym", "trade name", "inn"));
        packs.add(new Pack("id", "ekt", "barcode", "declNumber", "name",
                "manufacturer", "synonym", "trade name", "inn"));

        when(settingsService.getMasterTablePackEntity()).thenReturn(Pack.class);

        String tableName = settingsService.getMasterTablePackEntity()
                .newInstance().getTableName();

        char delim = ',';
        short testingAction = 1;
        InputStream is = new ByteArrayInputStream(
                "co,nt,ent,mn,syn,tn,inn,ekt,decl".getBytes());
        MultipartFile file = new MockMultipartFile("fileName", is);
        ImportDto dto = new ImportDto(file, testingAction, false);
        dto.setDelim(delim);

        List<ComplexKey> result = new ArrayList<ComplexKey>();

        when(packHelper.checkTables(tableName)).thenReturn(true);
        when(packHelper.savePacks(any(List.class))).thenReturn(result);

        service.savePacks(settingsService.getMasterTablePackEntity(), dto);

        verify(packHelper).checkTables(tableName);
    }

    @Test(expected = InvalidFormatDataException.class)
    public void testSavePacksWhenWrongDataFormat()
            throws InvalidFormatDataException, IOException,
            InstantiationException, IllegalAccessException,
            InterruptedException, AmazonServiceException,
            IllegalArgumentException, ItemSizeLimitExceededException {

        when(settingsService.getMasterTablePackEntity()).thenReturn(Pack.class);

        String tableName = settingsService.getMasterTablePackEntity()
                .newInstance().getTableName();

        char delim = ',';
        short testingAction = 1;
        InputStream is = new ByteArrayInputStream("ent".getBytes());
        MultipartFile file = new MockMultipartFile("fileName", is);
        ImportDto dto = new ImportDto(file, testingAction, false);
        dto.setDelim(delim);

        when(settingsService.getMasterTablePackEntity()).thenReturn(Pack.class);
        when(packHelper.checkTables(tableName)).thenReturn(true);

        service.savePacks(settingsService.getMasterTablePackEntity(), dto);
    }

    @Test
    public void testPrepareSlaveTable() {

        String tableName = "tableName";
        when(settingsService.getPackSlaveTableName()).thenReturn(tableName);

        service.prepareSlaveTable();

        verify(settingsService).getPackSlaveTableName();
        verify(packHelper).prepareTables(tableName);
    }

    @SuppressWarnings({"unchecked"})
    @Test
    public void testSavePacksWithSkipLine() throws InvalidFormatDataException,
            IOException, InstantiationException, IllegalAccessException,
            InterruptedException, AmazonServiceException,
            IllegalArgumentException, ItemSizeLimitExceededException {

        when(settingsService.getMasterTablePackEntity()).thenReturn(Pack.class);

        String tableName = settingsService.getMasterTablePackEntity()
                .newInstance().getTableName();

        char delim = ';';
        InputStream is = new ByteArrayInputStream(
                ("Id;Ekt;Barcode;DeclNumber;Name;Manufacturer;Synonym;TradeName;Inn"
                        + "2000553900022;Клопиксол депо д/и р-р масл. 20% 1мл N10 Лундбек, Дания;;Лундбек;;Клопиксол депо;;2000553900022;"
                        + "\n\"2000553900022\";\"Клопиксол депо д/и р-р масл. 20% 1мл N10 Лундбек, Дания\";\"\";\"Лундбек\";\"\";\"Клопиксол депо\";\"\";\"2000553900022\";\"\"")
                        .getBytes());
        short testingAction = 1;
        MultipartFile file = new MockMultipartFile("fileName", is);
        ImportDto dto = new ImportDto(file, testingAction, false);
        dto.setDelim(delim);

        List<ComplexKey> result = new ArrayList<ComplexKey>();
        result.add(new ComplexKey("first", "first"));
        result.add(new ComplexKey("second", "second"));

        when(packHelper.checkTables(tableName)).thenReturn(true);
        when(packHelper.getMaxBatchSize()).thenReturn(25);
        when(packHelper.savePacks(any(List.class))).thenReturn(result);

        PackImportDto packImportResult = service.savePacks(settingsService
                .getMasterTablePackEntity(), dto);

        verify(packHelper).checkTables(tableName);
        assertEquals(2, packImportResult.getUnprocessedItemIdList().size());
    }

}
