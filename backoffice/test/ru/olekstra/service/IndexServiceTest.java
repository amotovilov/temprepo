package ru.olekstra.service;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import ru.olekstra.domain.IndexEntity;
import ru.olekstra.helper.IndexHelper;

@RunWith(MockitoJUnitRunner.class)
public class IndexServiceTest {

    private IndexHelper helper;
    private IndexService service;

    @Before
    public void setup() {
        helper = mock(IndexHelper.class);
        service = new IndexService(helper);
    }

    @Test
    public void testIfIndexExists() {

        String value = "cardNumber";
        String index = "telNumber";
        String prefix = "TC";

        Map<String, Object> attributes = new HashMap<String, Object>();
        attributes.put(IndexEntity.FLD_INDEX, index);
        attributes.put(IndexEntity.FLD_PREFIX, prefix);
        attributes.put(IndexEntity.FLD_VALUE, value);

        when(helper.getIndex(index, prefix)).thenReturn(attributes);

        boolean result = service.ifIndexExists(index, prefix);
        assertTrue(result);
    }

}
