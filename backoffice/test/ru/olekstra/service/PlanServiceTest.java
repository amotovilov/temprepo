package ru.olekstra.service;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.common.helper.PlanHelper;
import ru.olekstra.common.service.PlanService;
import ru.olekstra.domain.DiscountPlan;
import ru.olekstra.domain.dto.DiscountPlanForm;

@RunWith(MockitoJUnitRunner.class)
public class PlanServiceTest {

    private PlanHelper helper;
    private PlanService service;
    private InputStream discountDataInputStream;
    private InputStream discountDataInputStreamHalfSynonims;
    private List<String> questionsList;

    @Before
    public void setup() {
        helper = mock(PlanHelper.class);
        service = new PlanService(helper);

        String quotes = "\"";
        String quotesAndSemicolon = "\"" + ";" + "\"";
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 15; i++) {
            builder.append(quotes);
            builder.append("200000");
            builder.append(i);
            builder.append(quotesAndSemicolon);
            builder.append("20.00");
            builder.append(quotesAndSemicolon);
            builder.append("49");
            builder.append(quotesAndSemicolon);
            builder.append("39");
            builder.append(quotesAndSemicolon);
            builder.append("barcode");
            builder.append(i);
            builder.append(quotesAndSemicolon);
            builder.append("synonim");
            builder.append(i);
            builder.append(quotesAndSemicolon);
            builder.append("tradename");
            builder.append(i);
            builder.append(quotes);
            builder.append(System.getProperty("line.separator"));
        }
        discountDataInputStream = new ByteArrayInputStream(builder.toString()
                .getBytes());

        builder.setLength(0);
        for (int i = 0; i < 15; i++) {
            builder.append(quotes);
            builder.append("200000");
            builder.append(i);
            builder.append(quotesAndSemicolon);
            builder.append("20.00");
            builder.append(quotesAndSemicolon);
            builder.append("49");
            builder.append(quotesAndSemicolon);
            builder.append("39");
            builder.append(quotesAndSemicolon);
            builder.append("barcode");
            builder.append(i);
            builder.append(quotesAndSemicolon);
            if (i % 2 == 0) {
                builder.append("synonim");
                builder.append(i);
            }
            builder.append(quotesAndSemicolon);
            builder.append("tradename");
            builder.append(i);
            builder.append(quotes);
            builder.append(System.getProperty("line.separator"));
        }
        discountDataInputStreamHalfSynonims = new ByteArrayInputStream(builder
                .toString().getBytes());

        questionsList = new ArrayList<String>();
        questionsList.add("firstQuestion");
        questionsList.add("secondQuestion");
    }

    @Test
    public void testDeletePlanId() {

        String id = "id";

        service.deletePlan(id);

        verify(helper, times(10)).deletePlan(id);
    }

    @Test
    public void testSavePlan() throws JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException {

        DiscountPlanForm form = new DiscountPlanForm("id", "name", 45, "descr",
                "link", "1 2 3", true, BigDecimal.ONE);

        List<String> discounts = new ArrayList<String>();
        discounts.add("Test");
        discounts.add("Test2");
        discounts.add("Test3");

        when(helper.addPlan((DiscountPlan) anyObject())).thenReturn(true);

        boolean result = service.savePlan(form, discounts);
        assertTrue(result);
        verify(helper).addPlan((DiscountPlan) anyObject());
    }
}
