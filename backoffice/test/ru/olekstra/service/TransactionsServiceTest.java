package ru.olekstra.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import ru.olekstra.domain.Discount;
import ru.olekstra.domain.Refund;
import ru.olekstra.exception.InvalidDateRangeException;
import ru.olekstra.helper.DiscountHelper;
import ru.olekstra.helper.DrugstoreHelper;

import com.amazonaws.services.dynamodb.model.ProvisionedThroughputExceededException;

@RunWith(MockitoJUnitRunner.class)
public class TransactionsServiceTest {

    private DrugstoreHelper drugstoreHelper;
    private DiscountHelper discountHelper;
    private TransactionsService service;

    @Before
    public void setup() {
        drugstoreHelper = mock(DrugstoreHelper.class);
        discountHelper = mock(DiscountHelper.class);
        service = new TransactionsService(drugstoreHelper, discountHelper);
    }

    @Test
    public void testGetRefundTransactions()
            throws ProvisionedThroughputExceededException, JsonParseException,
            JsonMappingException, IOException, InvalidDateRangeException, InstantiationException,
            IllegalAccessException {

        String discountId1 = "discountId1";
        Discount discount1 = new Discount();
        discount1.setId(discountId1);

        String discountId2 = "discountId2";
        Discount discount2 = new Discount();
        discount2.setId(discountId2);

        List<Discount> discounts = Arrays.asList(discount1, discount2);

        String dateFrom = "02.2013";
        String dateTo = "03.2013";

        String period1 = "201302";
        String period2 = "201303";

        Refund refund1 = new Refund();
        refund1.setHashKey(
                DateTime.parse(period1, DateTimeFormat.forPattern("yyyyMM")),
                discountId1);
        Refund refund2 = new Refund();
        refund2.setHashKey(
                DateTime.parse(period2, DateTimeFormat.forPattern("yyyyMM")),
                discountId2);

        when(discountHelper.getAllDiscounts()).thenReturn(discounts);
        when(
                drugstoreHelper.getRefundTransactionsForPeriod(eq(period1),
                        eq(discountId1))).thenReturn(Arrays.asList(refund1));
        when(
                drugstoreHelper.getRefundTransactionsForPeriod(eq(period2),
                        eq(discountId2))).thenReturn(Arrays.asList(refund2));

        List<Refund> refunds = service.getRefundTransactions(dateFrom, dateTo);

        verify(discountHelper).getAllDiscounts();
        verify(drugstoreHelper).getRefundTransactionsForPeriod(eq(period1),
                eq(discountId1));
        verify(drugstoreHelper).getRefundTransactionsForPeriod(eq(period2),
                eq(discountId2));

        assertEquals(period1, refunds.get(0).getPeriod());
        assertEquals(discountId1, refunds.get(0).getDiscountId());

        assertEquals(period2, refunds.get(1).getPeriod());
        assertEquals(discountId2, refunds.get(1).getDiscountId());
    }
}
