package ru.olekstra.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.domain.Drugstore;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.helper.DrugstoreHelper;

import com.amazonaws.services.dynamodb.model.ProvisionedThroughputExceededException;

@RunWith(MockitoJUnitRunner.class)
public class DrugstoreServiceTest {

    private DrugstoreHelper helper;
    private DrugstoreService service;

    @Before
    public void setUp() throws Exception {

        helper = mock(DrugstoreHelper.class);
        service = new DrugstoreService(helper);
    }

    @Test
    public void testDrugstoreService() {

        assertTrue(service != null);
        assertTrue(DrugstoreService.class.isInstance(service));
    }

    @Test
    public void testFindDrugstores() throws IOException {

        List<Drugstore> drugstores = new ArrayList<Drugstore>();
        Drugstore drugstore = newDrugstore("testId", "testName",
                "testLocation", "timeZone", Boolean.TRUE);
        drugstore.setNonRefundPercent(20);
        drugstores.add(drugstore);
        String value = "valueToFind";

        when(helper.findDrugstores(value)).thenReturn(drugstores);

        List<Drugstore> result = service.findDrugstores(value);
        assertEquals(result.size(), drugstores.size());

        verify(helper).findDrugstores(value);
    }

    @Test
    public void testFindDrugstoreById() {

        Drugstore drugstore = newDrugstore("testId", "testName",
                "testLocation", "timeZone", Boolean.TRUE);
        drugstore.setNonRefundPercent(15);
        String drugstoreId = "testId";

        when(helper.getDrugstoreById(drugstoreId)).thenReturn(drugstore);

        Drugstore loadedDrugstore = service.findDrugstoreById(drugstoreId);
        assertEquals(drugstore, loadedDrugstore);

        verify(helper).getDrugstoreById(drugstoreId);
    }
    
    private Drugstore newDrugstore(String id, String name, String authCode, String timeZone, Boolean disabled){
        Drugstore drugstore = new Drugstore(id);
        drugstore.setName(name);
        drugstore.setAuthCode(authCode);
        drugstore.setTimeZone(timeZone);
        drugstore.setDisabled(disabled);
        return drugstore;
    }

    @Test
    public void testGetProcessingTransactions() throws JsonParseException,
            JsonMappingException, IOException,
            ProvisionedThroughputExceededException {

        String hashKey = "key";
        UUID uuid = UUID.randomUUID();
        int index = 1;
        List<ProcessingTransaction> transactions = new ArrayList<ProcessingTransaction>();
        ProcessingTransaction transaction = new ProcessingTransaction(
                new HashMap<String, Object>());
        transaction.setVoucherId(uuid);
        transactions.add(transaction);

        String dateTo = DateTime
                .now()
                .plusDays(1)
                .withZone(DateTimeZone.UTC)
                .toString(
                        DateTimeFormat
                                .forPattern(ProcessingTransaction.DB_DATE_FORMAT));

        String dateFrom = DateTime
                .now()
                .minusWeeks(index)
                .withZone(DateTimeZone.UTC)
                .toString(
                        DateTimeFormat
                                .forPattern(ProcessingTransaction.DB_DATE_FORMAT));

        when(
                helper.getDrugstoreProcessingTransactions(eq(hashKey),
                        eq(dateFrom), eq(dateTo))).thenReturn(transactions);

        List<ProcessingTransaction> result = service.getProcessingTransactions(
                hashKey, 1);

        verify(helper).getDrugstoreProcessingTransactions(eq(hashKey),
                eq(dateFrom), eq(dateTo));

        assertEquals(result.size(), transactions.size());
        assertEquals(result.get(0).getVoucherId(), uuid);
    }
    /*
     * @Test public void testGetResponseListFromDrugstoreList() {
     * 
     * List<Drugstore> drugstores = new ArrayList<Drugstore>(); Drugstore
     * drugstore = new Drugstore("testId", "testName", "testLocation",
     * "timeZone", Boolean.TRUE); drugstore.setNonRefundPercent(20);
     * drugstores.add(drugstore);
     * 
     * List<DrugstoreResponse> result = service
     * .getResponseListFromDrugstoreList(drugstores);
     * assertEquals(drugstores.size(), result.size()); }
     */
}
