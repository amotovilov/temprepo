package ru.olekstra.helper;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.domain.IndexEntity;

@RunWith(MockitoJUnitRunner.class)
public class IndexHelperTest {

    private IndexHelper helper;
    private DynamodbService service;

    @Before
    public void setup() {
        service = mock(DynamodbService.class);
        helper = new IndexHelper(service);
    }

    @Test
    public void testGetIndex() {
        String value = "cardNumber";
        String index = "telNumber";
        String prefix = "TC";

        Map<String, Object> attributes = new HashMap<String, Object>();
        attributes.put(IndexEntity.FLD_INDEX, index);
        attributes.put(IndexEntity.FLD_PREFIX, prefix);
        attributes.put(IndexEntity.FLD_VALUE, value);

        when(service.getAttributes(IndexEntity.TABLE_NAME, index, prefix, null))
                .thenReturn(attributes);

        Map<String, Object> result = helper.getIndex(index, prefix);
        assertEquals(attributes, result);
    }
}
