package ru.olekstra.helper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.common.dao.DrugstoreDao;
import ru.olekstra.domain.CompletedTransaction;
import ru.olekstra.domain.Drugstore;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.Refund;
import ru.olekstra.exception.NotSavedException;

import com.amazonaws.services.dynamodb.model.ProvisionedThroughputExceededException;

@RunWith(MockitoJUnitRunner.class)
public class DrugstoreHelperTest {

    private DrugstoreHelper helper;
    private DrugstoreDao dao;
    private DynamodbService service;
    private MessageSource messageSource;
    private String CompletedTransaction_FLD_CARD_ID = "Card";
    private String ProcessingTransaction_FLD_DRUGSTORE_ID = "DrugstoreId";

    @Before
    public void setUp() throws Exception {

        service = mock(DynamodbService.class);
        messageSource = mock(MessageSource.class);
        dao = mock(DrugstoreDao.class);
        helper = new DrugstoreHelper(service, messageSource, dao);
    }

    @Test
    public void testDrugstoreHelper() {

        assertTrue(helper != null);
        assertTrue(DrugstoreHelper.class.isInstance(helper));
    }

    @Test
    public void testGetAllDrugstores() throws IOException {

        List<Drugstore> list = new LinkedList<Drugstore>();
        list.add(new Drugstore());

        when(service.getAllObjects(Drugstore.class)).thenReturn(list);

        List<Drugstore> drugstores = helper.getAllDrugstores();

        assertEquals(list, drugstores);
    }

    @Test
    public void testGetAllDrugstoresIds() {

        List<Map<String, Object>> items = new ArrayList<Map<String, Object>>();
        Map<String, Object> drugstoreItems = new HashMap<String, Object>();
        drugstoreItems.put(Drugstore.FLD_ID, "testId0");
        drugstoreItems.put(Drugstore.FLD_ID, "testId1");

        items.add(drugstoreItems);

        when(
                service.getAllItems(Drugstore.TABLE_NAME, Arrays
                        .asList(Drugstore.FLD_ID))).thenReturn(items);

        List<String> drugstoreIds = helper.getAllDrugstoresIds();

        for (Map<String, Object> item : items) {
            assertTrue(drugstoreIds.contains(item.get(Drugstore.FLD_ID)));
        }

        verify(service).getAllItems(Drugstore.TABLE_NAME,
                Arrays.asList(Drugstore.FLD_ID));
    }

    private Drugstore newDrugstore(String id, String name, String authCode, String timeZone, Boolean disabled){
        Drugstore drugstore = new Drugstore(id);
        drugstore.setName(name);
        drugstore.setAuthCode(authCode);
        drugstore.setTimeZone(timeZone);
        drugstore.setDisabled(disabled);
        return drugstore;
    }
    
    @Test
    public void testFindDrugstores() throws IOException {

        String id = "id";
        String name = "name";
        String yourName = "yourname";
        String imya = "imya";
        String location = "location";
        String timeZone = "timezone";

        List<Drugstore> drugstoreList = new ArrayList<Drugstore>();
        Drugstore drugstore1 = newDrugstore(id + 1, name + 1, location + 1,
                timeZone + 1, Boolean.TRUE);
        Drugstore drugstore2 = newDrugstore(id + 2, imya + 2, location + 2,
                timeZone + 2, Boolean.TRUE);
        Drugstore drugstore3 = newDrugstore(id + 3, name + 3, location + 3,
                timeZone + 3, Boolean.TRUE);
        Drugstore drugstore4 = newDrugstore(id + 4, imya + 4, location + 4,
                timeZone + 4, Boolean.TRUE);
        Drugstore drugstore5 = newDrugstore(id + 5, yourName, location + 5,
                timeZone + 5, Boolean.TRUE);
        drugstoreList.add(drugstore1);
        drugstoreList.add(drugstore2);
        drugstoreList.add(drugstore3);
        drugstoreList.add(drugstore4);
        drugstoreList.add(drugstore5);

        String value = "name";

        when(dao.getDrugstoreNames()).thenReturn(
                drugstoreList);

        List<Drugstore> drugstores = helper.findDrugstores(value);

        assertEquals(3, drugstores.size());

    }

    @Test
    public void testGetDrugstoreById() {

        String drugstoreId = "testId";
        Drugstore drugstore = newDrugstore(drugstoreId, "testName",
                "testLocation", "timeZone", Boolean.TRUE);
        drugstore.setNonRefundPercent(20);

        when(service.getObject(Drugstore.class, drugstoreId)).thenReturn(
                drugstore);

        Drugstore loadedDrugstore = helper.getDrugstoreById(drugstoreId);

        assertEquals(drugstore, loadedDrugstore);
        verify(service).getObject(Drugstore.class, drugstoreId);
    }

    @Test
    public void testAdd() throws ItemSizeLimitExceededException {

        Drugstore drugstore = newDrugstore("testId", "testName",
                "testLocation", "timeZone", Boolean.TRUE);
        drugstore.setNonRefundPercent(50);

        when(service.putObject(drugstore)).thenReturn(true);

        boolean result = helper.add(drugstore);

        assertTrue(result);
        verify(service).putObject(drugstore);
    }

    @Test
    public void testSave() {

        Drugstore drugstore = newDrugstore("testId", "testName",
                "timeZone", "100", Boolean.TRUE);

        when(dao.concurrentUpdateDrugstore(any(Drugstore.class))).thenReturn(
                false);

        try {
            helper.save(drugstore);
        } catch (NotSavedException nse) {

            verify(dao, times(2)).concurrentUpdateDrugstore(drugstore);

        }
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testGetComplitedTransactions() throws JsonParseException,
            JsonMappingException, IOException,
            ProvisionedThroughputExceededException, InstantiationException, IllegalAccessException {

        Map<String, Object> attribute = new HashMap<String, Object>();
        List<CompletedTransaction> res = new ArrayList<CompletedTransaction>();

        String period = "201209";
        String cardNumber = "777";

        attribute.put(CompletedTransaction_FLD_CARD_ID, cardNumber);
        res.add(new CompletedTransaction(attribute));

        when(service.queryObjects(eq(CompletedTransaction.class), eq(period))).thenReturn(res);

        List<CompletedTransaction> results = helper.getCompletedTransactionsForPeriod(period);

        verify(service).queryObjects(eq(CompletedTransaction.class), eq(period));

        assertTrue(results.size() != 0);
        assertEquals(results.get(0).getCardNumber(), cardNumber);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testGetDrugstoreProcessingTransactions()
            throws JsonParseException, JsonMappingException, IOException,
            ProvisionedThroughputExceededException {

        List<Map<String, Object>> atributes = new ArrayList<Map<String, Object>>();
        Map<String, Object> attribute = new HashMap<String, Object>();

        String drugstoreId = "test drugstore id";
        attribute.put(ProcessingTransaction_FLD_DRUGSTORE_ID, drugstoreId);
        atributes.add(attribute);

        String dateTo = DateTime
                .now()
                .plusDays(1)
                .withZone(DateTimeZone.UTC)
                .toString(
                        DateTimeFormat
                                .forPattern(ProcessingTransaction.DB_DATE_FORMAT));

        String dateFrom = DateTime
                .now()
                .minusWeeks(1)
                .withZone(DateTimeZone.UTC)
                .toString(
                        DateTimeFormat
                                .forPattern(ProcessingTransaction.DB_DATE_FORMAT));

        when(
                service.getAttributesWithinTimePeriod(
                        eq(ProcessingTransaction.TABLE_NAME), eq(drugstoreId),
                        eq(dateFrom), eq(dateTo), any(List.class))).thenReturn(
                atributes);

        List<ProcessingTransaction> results = helper
                .getDrugstoreProcessingTransactions(drugstoreId, dateFrom,
                        dateTo);

        verify(service).getAttributesWithinTimePeriod(
                eq(ProcessingTransaction.TABLE_NAME), eq(drugstoreId),
                eq(dateFrom), eq(dateTo), any(List.class));

        assertTrue(results.size() != 0);
        assertEquals(results.get(0).getDrugstoreId(), drugstoreId);
    }

    @Test
    public void testGetAuthCode() {

        String drugstoreId = "testId";
        String authCode = "100";

        Map<String, Object> atributes = new HashMap<String, Object>();
        atributes.put(Drugstore.FLD_AUTH_CODE, authCode);

        when(
                service.getAttributes(eq(Drugstore.TABLE_NAME),
                        eq(drugstoreId), eq(Arrays
                                .asList(Drugstore.FLD_AUTH_CODE)))).thenReturn(
                atributes);

        String result = helper.getAuthCode(drugstoreId);

        assertNotNull(result);
        assertEquals(authCode, result);
    }

    @Test
    public void testGetRefundTransactionsForPeriod()
            throws ProvisionedThroughputExceededException, JsonParseException,
            JsonMappingException, IOException, InstantiationException, IllegalAccessException {

        String period = "201303";
        String discountId = "testId";

        String refundTransactionId = period + Refund.getDelimeter()
                + discountId;

        List<Refund> items = new ArrayList<Refund>();

        Map<String, Object> item1 = new HashMap<String, Object>();
        item1.put(new Refund().getHashKey(), refundTransactionId);

        Map<String, Object> item2 = new HashMap<String, Object>();
        item2.put(new Refund().getHashKey(), refundTransactionId);

        items.add(new Refund(item1));
        items.add(new Refund(item2));

        when(service.queryObjects(eq(Refund.class), eq(refundTransactionId))).thenReturn(items);

        List<Refund> result = helper.getRefundTransactionsForPeriod(period,
                discountId);

        verify(service).queryObjects(eq(Refund.class), eq(refundTransactionId));

        assertEquals(items.size(), result.size());
        assertEquals(period, result.get(0).getPeriod());
        assertEquals(period, result.get(1).getPeriod());
        assertEquals(discountId, result.get(0).getDiscountId());
        assertEquals(discountId, result.get(1).getDiscountId());
    }
}
