package ru.olekstra.helper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import ru.olekstra.awsutils.DynamodbEntity;
import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.common.helper.PlanHelper;
import ru.olekstra.domain.Discount;
import ru.olekstra.domain.DiscountPlan;

@RunWith(MockitoJUnitRunner.class)
public class PlanHelperTest {

    private DynamodbService dynamodb;
    private PlanHelper helper;

    @Before
    public void setup() {
        dynamodb = mock(DynamodbService.class);
        helper = new PlanHelper(dynamodb);
    }

    @Test
    public void testGetAllDiscountPlans() throws JsonGenerationException,
            JsonMappingException, IOException {

        String item1Id = "item1";
        String item1Name = "name1";
        String item1MaxDiscount = "50";
        List<String> discounts = new ArrayList<String>();
        discounts.add("discount1");
        discounts.add("discount2");
        String DiscountPlan_FLD_DISCOUNT_PLAN_ID = "Id";
        String DiscountPlan_FLD_DISCOUNT_PLAN_NAME = "Name";
        String DiscountPlan_FLD_MAX_DISCOUNT_PERCENT = "MaximumDiscountPercent";
        String DiscountPlan_FLD_DISCOUNTS = "Discounts";

        Map<String, Object> item1Attributes = new HashMap<String, Object>();
        item1Attributes.put(DiscountPlan_FLD_DISCOUNT_PLAN_ID, item1Id);
        item1Attributes.put(DiscountPlan_FLD_DISCOUNT_PLAN_NAME, item1Name);
        item1Attributes.put(DiscountPlan_FLD_MAX_DISCOUNT_PERCENT,
                item1MaxDiscount);
        item1Attributes.put(DiscountPlan_FLD_DISCOUNTS, DynamodbEntity
                .toJson(discounts));

        List<Map<String, Object>> allAttributes = new ArrayList<Map<String, Object>>();
        allAttributes.add(item1Attributes);

        when(dynamodb.getAllItems(DiscountPlan.TABLE_NAME, null)).thenReturn(
                allAttributes);

        List<DiscountPlan> resultList = helper.getAllDiscountPlans();

        assertEquals(allAttributes.size(), resultList.size());
        assertEquals(item1Id, resultList.get(0).getId());
        assertEquals(item1Name, resultList.get(0).getPlanName());
        assertEquals(new Integer(item1MaxDiscount), resultList.get(0)
                .getMaxDiscountPercent());
        assertEquals(discounts.size(), resultList.get(0).getDiscounts().size());
    }

    @Test
    public void testSavePlanItemsAllSaved() throws JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException {

        String item1PlanId = "plan1";
        String item1Name = "name1";
        Integer maxDiscountPercent1 = 50;

        String item2PlanId = "plan2";
        String item2Name = "name2";
        Integer maxDiscountPercent2 = 55;

        String discount1 = "discount1";
        String discount2 = "discount2";
        String discount3 = "discount3";
        String discount4 = "discount4";

        List<String> discounts1 = new ArrayList<String>();
        discounts1.add(discount1);
        discounts1.add(discount2);

        List<String> discounts2 = new ArrayList<String>();
        discounts2.add(discount3);
        discounts2.add(discount4);

        // Discount plan info
        DiscountPlan plan1 = new DiscountPlan(item1PlanId, item1Name,
                maxDiscountPercent1);
        plan1.setDiscounts(discounts1);

        DiscountPlan plan2 = new DiscountPlan(item2PlanId, item2Name,
                maxDiscountPercent2);
        plan2.setDiscounts(discounts2);

        when(dynamodb.putObject(plan1)).thenReturn(true);

        when(dynamodb.putObject(plan2)).thenReturn(true);

        boolean result1 = helper.addPlan(plan1);
        assertTrue(result1);

        boolean result2 = helper.addPlan(plan2);
        assertTrue(result2);

        verify(dynamodb).putObject(plan1);
        verify(dynamodb).putObject(plan2);

    }

    @Test
    public void getAllDiscountsTest() throws IOException {

        Discount discount1 = new Discount("1", "name", true, true, 1, 1, 10);
        Discount discount2 = new Discount("2", "name", true, true, 99, 1, 10);
        List<Discount> discounts = new ArrayList<Discount>();
        discounts.add(discount1);
        discounts.add(discount2);

        when(dynamodb.getAllObjects(Discount.class)).thenReturn(discounts);

        List<Discount> result = helper.getAllDiscounts();
        assertEquals(result.size(), discounts.size());
        assertEquals(result.get(0).getId(), discount1.getId());
        assertEquals(result.get(1).getId(), discount2.getId());
        verify(dynamodb).getAllObjects(Discount.class);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void hasPlanTest() {

        Map<String, Object> notNullDummyValues = new HashMap<String, Object>();
        notNullDummyValues.put("test", "test");

        when(
                dynamodb.getAttributes(eq(DiscountPlan.TABLE_NAME),
                        anyString(), (List<String>) anyObject())).thenReturn(
                notNullDummyValues);

        boolean result = helper.hasPlan("testId");

        assertTrue(result);
        verify(dynamodb).getAttributes(eq(DiscountPlan.TABLE_NAME),
                anyString(), (List<String>) anyObject());
    }
}
