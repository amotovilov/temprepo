package ru.olekstra.helper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import ru.olekstra.awsutils.BatchOperationResult;
import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.domain.Recipe;

@RunWith(MockitoJUnitRunner.class)
public class RecipeHelperTest {

    private RecipeHelper helper;
    private DynamodbService service;

    @Before
    public void setup() {
        service = mock(DynamodbService.class);
        helper = new RecipeHelper(service);
    }

    @Test
    public void testGetRecipesForPeriod() throws RuntimeException, IOException {
        String period = "201302";
        String uuid1 = UUID.randomUUID().toString();
        String uuid2 = UUID.randomUUID().toString();
        String drugstoreId = "test drugstore";
        String fileName = "test.jpg";
        String fileType = "jpg";
        DateTime date = new DateTime();

        Recipe recipe1 = new Recipe(period, uuid1, Recipe.STATE_NEW,
                drugstoreId, fileName, fileType, date);
        Recipe recipe2 = new Recipe(period, uuid2, Recipe.STATE_PARSED,
                drugstoreId, fileName, fileType, date);
        List<Recipe> referenceRecipes = Arrays.asList(recipe1, recipe2);
        BatchOperationResult<Recipe> result = new BatchOperationResult<Recipe>(
                referenceRecipes, null);

        String s3FileName1 = uuid1 + "." + fileType;
        String s3FileName2 = uuid2 + "." + fileType;
        List<String> s3RecipeFiles = Arrays.asList(s3FileName1, s3FileName2);

        when(
                service.getObjects(eq(Recipe.class), any(String[].class),
                        any(String[].class))).thenReturn(result);

        List<Recipe> recipes = helper
                .getRecipesForPeriod(period, s3RecipeFiles);

        verify(service).getObjects(eq(Recipe.class), any(String[].class),
                any(String[].class));

        assertNotNull(recipes);
        assertEquals(referenceRecipes, recipes);
    }
}
