package ru.olekstra.helper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import ru.olekstra.awsutils.BatchOperationResult;
import ru.olekstra.awsutils.ComplexKey;
import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.domain.Discount;
import ru.olekstra.domain.DiscountData;
import ru.olekstra.domain.DiscountPlan;
import ru.olekstra.domain.DiscountReplace;

import com.amazonaws.AmazonServiceException;

@RunWith(MockitoJUnitRunner.class)
public class DiscountHelperTest {

    private DynamodbService service;
    private DiscountHelper helper;

    @Before
    public void setup() {
        service = mock(DynamodbService.class);
        helper = new DiscountHelper(service);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Test
    public void testGetAllDiscounts() throws IOException {

        String discountId1 = "testId1";
        String discountId2 = "testId2";

        List<Map<String, Object>> items = new ArrayList<Map<String, Object>>();

        Map<String, Object> item1 = new HashMap<String, Object>();
        item1.put(Discount.FLD_DISCOUNT_ID, discountId1);

        Map<String, Object> item2 = new HashMap<String, Object>();
        item2.put(Discount.FLD_DISCOUNT_ID, discountId2);

        items.add(item1);
        items.add(item2);

        when(service.getAllItems(eq(Discount.TABLE_NAME), eq((List) null)))
                .thenReturn(items);

        List<Discount> result = helper.getAllDiscounts();

        verify(service).getAllItems(eq(Discount.TABLE_NAME), eq((List) null));

        assertEquals(items.size(), result.size());
        assertEquals(discountId1, result.get(0).getId());
        assertEquals(discountId2, result.get(1).getId());
    }

    @Test
    public void testGetDiscount() {

        String discountId = "testId";
        Discount discount = new Discount();
        discount.setId(discountId);

        when(service.getObject(eq(Discount.class), eq(discountId))).thenReturn(
                discount);

        Discount result = helper.getDiscount(discountId);

        verify(service).getObject(eq(Discount.class), eq(discountId));

        assertEquals(discountId, result.getId());
    }

    @Test
    public void testSaveDiscount() throws ItemSizeLimitExceededException {

        String discountId = "testId";
        Discount discount = new Discount();
        discount.setId(discountId);

        when(service.putObject(eq(discount))).thenReturn(true);

        boolean result = helper.saveDiscount(discount);

        verify(service).putObject(eq(discount));

        assertTrue(result);
    }

    @Test
    public void testDeleteDiscount() {

        String discountId = "testId";
        helper.deleteDiscount(discountId);
        verify(service).deleteItem(eq(Discount.TABLE_NAME), eq(discountId));
    }

    @Test
    public void testSaveDiscountData() throws AmazonServiceException,
            IllegalArgumentException, InterruptedException,
            ItemSizeLimitExceededException {

        DiscountData data1 = new DiscountData();
        DiscountData data2 = new DiscountData();
        DiscountData data3 = new DiscountData();

        List<DiscountData> discountPacks = Arrays.asList(data1, data2, data3);

        BatchOperationResult<DiscountData> operationResult = new BatchOperationResult<DiscountData>();
        operationResult.setUnSuccessList(new ArrayList<ComplexKey>());

        when(service.getMaxBatchSize()).thenReturn(25);
        when(service.putObjects(any(DiscountData[].class))).thenReturn(
                operationResult);

        boolean result = helper.saveDiscountData(discountPacks);

        verify(service, times(4)).getMaxBatchSize();
        verify(service).putObjects(any(DiscountData[].class));

        assertTrue(result);
    }

    @Test
    public void testSaveDiscountReplaces() throws AmazonServiceException,
            IllegalArgumentException, InterruptedException,
            ItemSizeLimitExceededException {

        DiscountReplace replace1 = new DiscountReplace("testId1", "synonym1",
                "packId1", "1");
        DiscountReplace replace2 = new DiscountReplace("testId2", "synonym2",
                "packId2", "1");
        DiscountReplace replace3 = new DiscountReplace("testId3", "synonym3",
                "packId3", "1");

        List<DiscountReplace> discountRepalces = Arrays.asList(replace1,
                replace2, replace3);

        BatchOperationResult<DiscountReplace> operationResult = new BatchOperationResult<DiscountReplace>();
        operationResult.setUnSuccessList(new ArrayList<ComplexKey>());

        when(service.getMaxBatchSize()).thenReturn(25);
        when(service.putObjects(any(DiscountReplace[].class))).thenReturn(
                operationResult);

        boolean result = helper.saveDiscountReplaces(discountRepalces);

        verify(service, times(4)).getMaxBatchSize();
        verify(service).putObjects(any(DiscountReplace[].class));

        assertTrue(result);
    }

    @Test
    public void testGetAllDiscountPlans() throws IOException {

        String planId1 = "testId1";
        DiscountPlan plan1 = new DiscountPlan();
        plan1.setId(planId1);

        String planId2 = "testId2";
        DiscountPlan plan2 = new DiscountPlan();
        plan2.setId(planId2);

        List<DiscountPlan> plans = Arrays.asList(plan1, plan2);

        when(service.getAllObjects(eq(DiscountPlan.class))).thenReturn(plans);

        List<DiscountPlan> result = helper.getAllDiscountPlans();

        verify(service).getAllObjects(eq(DiscountPlan.class));

        assertEquals(plans.size(), result.size());
        assertEquals(planId1, result.get(0).getId());
        assertEquals(planId2, result.get(1).getId());
    }
}
