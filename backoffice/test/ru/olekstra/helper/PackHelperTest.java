package ru.olekstra.helper;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import ru.olekstra.awsutils.ComplexKey;
import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.dynamodb.TableManagement;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.domain.Pack;
import ru.olekstra.domain.PackA;
import ru.olekstra.domain.PackB;

import com.amazonaws.AmazonServiceException;

@RunWith(MockitoJUnitRunner.class)
public class PackHelperTest {

    private DynamodbService dynamodb;
    private TableManagement tables;
    private PackHelper helper;
    private AppSettingsService settings;

    @Before
    public void setup() {
        dynamodb = mock(DynamodbService.class);
        tables = mock(TableManagement.class);
        settings = mock(AppSettingsService.class);
        helper = new PackHelper(dynamodb, tables, settings);
    }

    @Ignore
    @Test
    public void testSavePacks() throws InterruptedException,
            AmazonServiceException, IllegalArgumentException,
            ItemSizeLimitExceededException {

        List<Pack> packs = new ArrayList<Pack>();
        Pack packA = new PackA("id", "ekt", "barcode", "declNumber", "name",
                "manufacturer", "synonym", "trade name", "inn");
        Pack packB = new PackB("id", "ekt", "barcode", "declNumber", "name",
                "manufacturer", "synonym", "trade name", "inn");
        packs.add(packA);
        packs.add(packB);

        when(dynamodb.getMaxBatchSize()).thenReturn(25);

        when(dynamodb.putObjects(any(Pack[].class)).getUnSuccessList())
                .thenReturn(new ArrayList<ComplexKey>());
        List<ComplexKey> unprocessedList = helper.savePacks(packs);

        verify(dynamodb).putObjects(packs.toArray(new Pack[packs.size()]));

        assertEquals(true, unprocessedList.size() == packs.size());
    }

    @Ignore
    @Test
    public void testSavePacksWhenThereAreProblems()
            throws InterruptedException, AmazonServiceException,
            IllegalArgumentException, ItemSizeLimitExceededException {

        List<Pack> packs = new ArrayList<Pack>();
        List<Pack> otherPacks = new ArrayList<Pack>();
        Pack packA = new PackA("id", "ekt", "barcode", "declNumber", "name",
                "manufacturer", "synonym", "trade name", "inn");
        Pack packB = new PackB("id", "ekt", "barcode", "declNumber", "name",
                "manufacturer", "synonym", "trade name", "inn");
        packs.add(packA);
        packs.add(packB);
        otherPacks.add(packA);

        when(dynamodb.getMaxBatchSize()).thenReturn(25);

        List<ComplexKey> unprocessedList = helper.savePacks(otherPacks);

        verify(dynamodb).putObjects(
                otherPacks.toArray(new Pack[otherPacks.size()]));

        assertEquals(false, unprocessedList.size() == packs.size());
    }

    @Test
    public void testCheckTablesWhenTablesExist() {
        String tableName = "tableName";

        when(tables.isExist(tableName)).thenReturn(true);

        boolean actual = helper.checkTables(tableName);

        verify(tables).isExist(tableName);
        verify(tables, times(0)).create(tableName, Pack.FLD_ID);
        verify(tables).isExist(tableName);

        assertEquals(true, actual);
    }

    @Test
    public void testCheckTablesWhenTablesDoNotExist() {
        String tableName = "tableName";

        when(tables.isExist(tableName)).thenReturn(false);
        when(settings.getReadCapacity()).thenReturn(5L);
        when(settings.getWriteCapacity()).thenReturn(10L);
        when(
                tables.create(tableName, Pack.FLD_ID, Pack.FLD_EKT, settings
                        .getReadCapacity(), settings.getWriteCapacity()))
                .thenReturn(true);

        boolean actual = helper.checkTables(tableName);

        verify(tables).isExist(tableName);
        verify(tables).create(tableName, Pack.FLD_ID, Pack.FLD_EKT,
                settings.getReadCapacity(), settings.getWriteCapacity());

        assertEquals(true, actual);
    }

    @Test
    public void testCheckTablesWhenTablesDoNotExistAndThereAreProblems() {
        String tableName = "tableName";

        when(tables.isExist(tableName)).thenReturn(false);
        when(settings.getReadCapacity()).thenReturn(5L);
        when(settings.getWriteCapacity()).thenReturn(10L);
        when(
                tables.create(tableName, Pack.FLD_ID, Pack.FLD_EKT, settings
                        .getReadCapacity(), settings.getWriteCapacity()))
                .thenReturn(false);

        boolean actual = helper.checkTables(tableName);

        verify(tables).isExist(tableName);
        verify(tables).create(tableName, Pack.FLD_ID, Pack.FLD_EKT,
                settings.getReadCapacity(), settings.getWriteCapacity());

        assertEquals(false, actual);
    }

    @Test
    public void testGetItemCount() {
        String tableName = "tableName";
        long count = 1;

        when(tables.isExist(tableName)).thenReturn(true);
        when(tables.getItemCount(tableName)).thenReturn(count);

        long actual = helper.getItemCount(tableName);

        verify(tables).isExist(tableName);
        verify(tables).getItemCount(tableName);

        assertEquals(count, actual);
    }

    @Test
    public void testGetItemCountWhenTablesDoNotExist() {
        String tableName = "tableName";
        long count = -1;

        when(tables.isExist(tableName)).thenReturn(false);

        long actual = helper.getItemCount(tableName);

        verify(tables).isExist(tableName);
        verify(tables, times(0)).getItemCount(tableName);

        assertEquals(count, actual);
    }

    @Test
    public void testPrepareTables() {
        String tableName = "tableName";

        when(tables.isExist(tableName)).thenReturn(true);
        when(tables.delete(tableName)).thenReturn(true);
        when(settings.getReadCapacity()).thenReturn(5L);
        when(settings.getWriteCapacity()).thenReturn(10L);

        when(
                tables.create(tableName, Pack.FLD_ID, Pack.FLD_EKT, settings
                        .getReadCapacity(), settings.getWriteCapacity()))
                .thenReturn(true);

        boolean actual = helper.prepareTables(tableName);

        verify(tables).isExist(tableName);
        verify(tables).delete(tableName);
        verify(tables).create(tableName, Pack.FLD_ID, Pack.FLD_EKT,
                settings.getReadCapacity(), settings.getWriteCapacity());

        assertEquals(true, actual);
    }

    @Test
    public void testPrepareTablesWhenThereAreProblems() {
        String tableName = "tableName";

        when(tables.isExist(tableName)).thenReturn(true);
        when(tables.delete(tableName)).thenReturn(true);
        when(settings.getReadCapacity()).thenReturn(5L);
        when(settings.getWriteCapacity()).thenReturn(10L);
        when(
                tables.create(tableName, Pack.FLD_ID, Pack.FLD_EKT, settings
                        .getReadCapacity(), settings.getWriteCapacity()))
                .thenReturn(false);

        boolean actual = helper.prepareTables(tableName);

        verify(tables).isExist(tableName);
        verify(tables).delete(tableName);
        verify(tables).create(tableName, Pack.FLD_ID, Pack.FLD_EKT,
                settings.getReadCapacity(), settings.getWriteCapacity());

        assertEquals(false, actual);
    }
}
