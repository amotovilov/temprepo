package ru.olekstra.processing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.w3c.dom.DOMException;

import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.awsutils.exception.OlekstraException;
import ru.olekstra.azure.model.Message;
import ru.olekstra.azure.model.MessageSendException;
import ru.olekstra.azure.service.QueueSender;
import ru.olekstra.common.service.CardLogService;
import ru.olekstra.common.service.DateTimeService;
import ru.olekstra.common.service.DiscountService;
import ru.olekstra.common.service.XmlMessageService;
import ru.olekstra.domain.CardLog;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.Refund;
import ru.olekstra.domain.dto.ProcessingTransactionMessage;
import ru.olekstra.domain.dto.VoucherRequest;
import ru.olekstra.domain.dto.VoucherResponseCard;
import ru.olekstra.domain.dto.VoucherResponseRow;
import ru.olekstra.exception.NotFoundException;
import ru.olekstra.service.CardService;
import ru.olekstra.service.VoucherService;

@RunWith(MockitoJUnitRunner.class)
public class RefundProcessingTest {

    private String drugstoreId = "drugstoreId";
    private UUID uuid = UUID.randomUUID();
    private String nextAutchcode = "101";
    private String userAutchcode = "1017"; // see
                                           // VoucherService.calcUserAuthCode
    private List<VoucherResponseRow> rows = Arrays.asList(
            new VoucherResponseRow(), new VoucherResponseRow());
    private String cardNumber = "777";
    private VoucherResponseCard voucherResponseCard = new VoucherResponseCard(
            cardNumber, "",
            "", true, "", "", "");
    private BigDecimal discountSum = new BigDecimal("1.00");

    private VoucherRequest request;
    private ProcessingTransaction transaction;

    private VoucherService service;
    private RefundProcessing processing;
    private CardLogService cardLogService;
    private XmlMessageService xmlMessageService;
    private DiscountService discountService;
    private CardService cardService;
    private Mapper dozerBeanMapper;
    private DateTimeService dateService;
    private QueueSender sender;

    private String sampleCompleteTransactionXmlBody = "<transaction-complete " +
            "xmlns:obj=\"http://www.olekstra.ru/2014/objects\" xmlns=\"http://www.olekstra.ru/2014/messaging\">" +
            "<obj:transaction id=\"00000000-0000-0000-0000-000000000000\" period=\"yyyyMMM\">" +
            "</obj:transaction></transaction-complete>";

    @Before
    public void setUp() throws JsonGenerationException, JsonMappingException,
            IOException, ParserConfigurationException, TransformerException, DOMException, OlekstraException {

        request = new VoucherRequest();
        request.setDrugstoreId(drugstoreId);

        transaction = new ProcessingTransaction(DateTime.now());
        transaction.setVoucherId(uuid);
        transaction.setAuthCode(userAutchcode);
        transaction.setDrugstoreId(drugstoreId);
        transaction.setRows(rows);
        transaction.setCard(voucherResponseCard);
        transaction.setDiscountSum(discountSum);
        transaction.setComplete(false);
        transaction.setDate(DateTime.now());
        transaction.setRefundSum(BigDecimal.TEN);

        service = mock(VoucherService.class);
        cardLogService = mock(CardLogService.class);
        xmlMessageService = mock(XmlMessageService.class);
        when(xmlMessageService.createTransactionCompleteMessage(any(ProcessingTransaction.class)))
                .thenReturn(new Message());

        discountService = mock(DiscountService.class);
        cardService = mock(CardService.class);
        dozerBeanMapper = new DozerBeanMapper();
        dateService = mock(DateTimeService.class);
        when(dateService.createDateTimeWithZone(anyString())).thenReturn(DateTime.now());
        when(dateService.createDefaultDateTime()).thenReturn(DateTime.now());
        sender = mock(QueueSender.class);

        processing = new RefundProcessing(service, transaction,
                cardLogService, xmlMessageService, discountService, cardService, dozerBeanMapper,
                dateService, sender);
    }

    @Test
    public void testDoProcessing() throws JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException,
            InterruptedException, NotFoundException, InstantiationException,
            IllegalAccessException, RuntimeException, OlekstraException, ParserConfigurationException,
            TransformerException, MessageSendException {

        when(service.getNextDrugstoreCode(eq(drugstoreId))).thenReturn(nextAutchcode);
        when(service.calcUserAuthCode(eq(nextAutchcode))).thenReturn(userAutchcode);

        ProcessingTransaction result = processing.doProcessing(request);

        verify(service).getNextDrugstoreCode(eq(drugstoreId));
        verify(service).calcUserAuthCode(eq(nextAutchcode));

        // verify(service).generateRefundsAndRecords(eq(transaction),
        // anyListOf(Refund.class), anyMapOf(String.class, String.class),
        // any(DateTime.class));
        verify(service).updateBalance(eq(transaction));
        verify(service).saveRefunds(anyListOf(Refund.class));
        verify(service).sendAndSaveRefundRecords(eq(transaction), anyListOf(CardLog.class));
        verify(service).saveProcessingTransaction(eq(transaction));
        ProcessingTransactionMessage msg =
                new ProcessingTransactionMessage(transaction.getRangeKey(),
                        transaction.getDrugstoreId());
        verify(xmlMessageService).createTransactionCompleteMessage(eq(transaction));
        verify(service).saveRefundIndex(any(DateTime.class), eq(drugstoreId),
                eq(transaction.getRangeKey()), eq(userAutchcode));

        assertTrue(result.isComplete());
        assertEquals(transaction, result);
        assertEquals(drugstoreId, result.getDrugstoreId());
        assertEquals(uuid, result.getVoucherId());
        assertEquals(userAutchcode, result.getAuthCode());
        assertEquals(discountSum, result.getDiscountSum());
        assertEquals(voucherResponseCard, result.getCard());
        assertEquals(rows, result.getRows());
    }
}
