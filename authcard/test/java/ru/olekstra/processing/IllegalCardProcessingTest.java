package ru.olekstra.processing;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.UUID;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.EntityProxy;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.dto.VoucherRequest;
import ru.olekstra.service.VoucherService;

@RunWith(MockitoJUnitRunner.class)
public class IllegalCardProcessingTest {

    private VoucherService service;
    private IllegalCardProcessing processing;
    private Card card;

    @Before
    public void setUp() {
        service = mock(VoucherService.class);
        card = new Card("777");
        processing = new IllegalCardProcessing(service, card);
    }

    @Test
    public void testDoProcessing() throws JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException {

        VoucherRequest request = new VoucherRequest();
        String messageCardNotActive = "msg.cardnotactive";

        ProcessingTransaction transaction = new ProcessingTransaction(DateTime.now());
        transaction.setVoucherId(UUID.randomUUID());

        when(service.getErrorProcessingTransaction(eq(request),
                anyString(),
                eq(new Object[] {request.getCardBarcode()})))
                .thenReturn(transaction);

        when(service.saveProcessingTransaction(transaction)).thenReturn(true);

        ProcessingTransaction result = processing.doProcessing(request);

        verify(service).getErrorProcessingTransaction(eq(request),
                eq(messageCardNotActive),
                eq(new Object[] {request.getCardBarcode()}));

        assertEquals(transaction, result);
        assertEquals(UUID.fromString(EntityProxy.NULL_UUID),
                result.getVoucherId());
    }
}
