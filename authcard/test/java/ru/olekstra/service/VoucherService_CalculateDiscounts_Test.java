package ru.olekstra.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.common.service.CardLogService;
import ru.olekstra.common.service.DateTimeService;
import ru.olekstra.common.service.DiscountService;
import ru.olekstra.domain.Discount;
import ru.olekstra.domain.Drugstore;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.dto.PackDiscountData;
import ru.olekstra.domain.dto.VoucherResponseCard;
import ru.olekstra.domain.dto.VoucherResponseRow;
import ru.olekstra.exception.DataIntegrityException;

import com.google.inject.internal.Lists;

@RunWith(MockitoJUnitRunner.class)
public class VoucherService_CalculateDiscounts_Test {

    private DynamodbService dynamoService;
    private AppSettingsService settingsService;
    private CardOperationService operationService;
    private DiscountService discountService;
    private MessageSource messageSource;
    private VoucherService service;
    private CardLogService cardLogService;
    private DateTimeService dateService;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void setUp() {
        dynamoService = mock(DynamodbService.class);
        settingsService = mock(AppSettingsService.class);
        operationService = mock(CardOperationService.class);
        discountService = mock(DiscountService.class);
        messageSource = mock(MessageSource.class);
        cardLogService = mock(CardLogService.class);
        dateService = mock(DateTimeService.class);
        when(dateService.createDateTimeWithZone(anyString())).thenReturn(DateTime.now());
        when(dateService.createDefaultDateTime()).thenReturn(DateTime.now());

        service = new VoucherService(dynamoService, settingsService,
                operationService, discountService, messageSource, cardLogService,
                dateService);
    }

    @Test
    public void testCalculateDiscounts_CardLimitOk()
            throws JsonGenerationException, JsonMappingException, IOException, RuntimeException, InterruptedException,
            DataIntegrityException {

        // проверяем, что баланс карты учитывается
        // (и скидка не дается, если не хватает)

        List<VoucherResponseRow> rows = new ArrayList<VoucherResponseRow>();
        VoucherResponseRow row1;

        row1 = new VoucherResponseRow("rowName01", "001", new BigDecimal(100), 1, 1);
        row1.setClientPackId("clientPackId001");
        row1.setClientPackName("clientPackName001");
        row1.setId("ekt001");

        PackDiscountData data10 = new PackDiscountData();
        data10.setDiscountId("discount000");
        data10.setOriginalPercent(new BigDecimal(35)); // 35
        data10.setOriginalSum(new BigDecimal(35));

        PackDiscountData data11 = new PackDiscountData();
        data11.setDiscountId("discount001");
        data11.setOriginalPercent(new BigDecimal(25));
        data11.setOriginalSum(new BigDecimal(25));

        row1.setPackDiscount(new PackDiscountData[] {data10, data11});
        rows.add(row1);

        // transaction

        ProcessingTransaction transaction = new ProcessingTransaction(DateTime.now());
        transaction.setRows(rows);
        transaction.setDiscounts(Lists.newArrayList(new String[] {"discount000", "discount001"}));
        transaction.setDiscountPlanId("plan000");
        transaction.setDrugstoreNonRefundPercent(11);
        transaction.setMaxDiscountPercent(40);
        when(discountService.getDiscount("discount000"))
                .thenReturn(new Discount("discount000", "discount000", true, false, 35, 0, 0));
        when(discountService.getDiscount("discount001"))
                .thenReturn(new Discount("discount001", "discount001", true, false, 25, 0, 0));
        transaction.setCard(new VoucherResponseCard("0000000000000",
                "cardOwner", "000", true, "comment", "plan000", "plan000"));
        transaction.setDrugstoreId("drugstore000");
        Drugstore drugstore = new Drugstore("drugstore000");
        drugstore.setNonRefundPercent(5);
        when(dynamoService.getObject(Drugstore.class, "drugstore000")).thenReturn(drugstore);

        BigDecimal cardLimit = new BigDecimal(30);

        service.calculateDiscounts(transaction, cardLimit);

        // Проверяем баланс
        VoucherResponseRow row = transaction.getRows().get(0);
        assertEquals(25, row.getDiscountPercent().intValue());
        assertEquals(25, row.getCardLimitUsedSum().intValue());
        assertEquals(25, row.getDiscountSum().intValue()); // OLP-652
        assertEquals(25, transaction.getCardLimitUsedSum().intValue());
        assertEquals(5, transaction.getCardLimitDeficit().intValue()); // 35-30
        assertEquals(14, transaction.getRefundSum().intValue());
        assertEquals(0, data10.getUsedPercent().intValue());
        assertEquals(0, data10.getUsedSum().intValue());
        assertEquals(25, data11.getUsedPercent().intValue());
        assertEquals(25, data11.getUsedSum().intValue());
    }

    @Test
    public void testCalculateDiscounts_SumDiscounts()
            throws JsonGenerationException, JsonMappingException, IOException, RuntimeException, InterruptedException,
            DataIntegrityException {

        // провеяем что скидки успешно сложатся, если хватает баланса карты и не
        // превысили max процент

        List<VoucherResponseRow> rows = new ArrayList<VoucherResponseRow>();
        VoucherResponseRow row1;

        row1 = new VoucherResponseRow("rowName01", "001", new BigDecimal(100), 1, 1);
        row1.setClientPackId("clientPackId001");
        row1.setClientPackName("clientPackName001");
        row1.setId("ekt001");

        PackDiscountData data10 = new PackDiscountData();
        data10.setDiscountId("discount000");
        data10.setOriginalPercent(new BigDecimal(35));
        data10.setOriginalSum(new BigDecimal(35));

        PackDiscountData data11 = new PackDiscountData();
        data11.setDiscountId("discount001");
        data11.setOriginalPercent(new BigDecimal(25));
        data11.setOriginalSum(new BigDecimal(25));

        row1.setPackDiscount(new PackDiscountData[] {data10, data11});
        rows.add(row1);

        // transaction

        ProcessingTransaction transaction = new ProcessingTransaction(DateTime.now());
        transaction.setRows(rows);
        transaction.setDiscounts(Lists.newArrayList(new String[] {"discount000", "discount001"}));
        transaction.setDiscountPlanId("plan000");
        transaction.setDrugstoreNonRefundPercent(11);
        transaction.setMaxDiscountPercent(70);
        when(discountService.getDiscount("discount000"))
                .thenReturn(new Discount("discount000", "discount000", true, false, 35, 0, 0));
        when(discountService.getDiscount("discount001"))
                .thenReturn(new Discount("discount001", "discount001", true, false, 25, 0, 0));
        transaction.setCard(new VoucherResponseCard("0000000000000",
                "cardOwner", "000", true, "comment", "plan000", "plan000"));
        transaction.setDrugstoreId("drugstore000");
        Drugstore drugstore = new Drugstore("drugstore000");
        drugstore.setNonRefundPercent(5);
        when(dynamoService.getObject(Drugstore.class, "drugstore000")).thenReturn(drugstore);

        BigDecimal cardLimit = new BigDecimal(80);

        service.calculateDiscounts(transaction, cardLimit);

        // Проверяем баланс
        VoucherResponseRow row = transaction.getRows().get(0);
        assertEquals(60, row.getDiscountPercent().intValue()); // 25+35
        assertEquals(60, row.getCardLimitUsedSum().intValue());
        assertEquals(60, row.getDiscountSum().intValue());
        assertEquals(60, transaction.getCardLimitUsedSum().intValue());
        assertEquals(0, transaction.getCardLimitDeficit().intValue());
        assertEquals(35, data10.getUsedPercent().intValue());
        assertEquals(35, data10.getUsedSum().intValue());
        assertEquals(25, data11.getUsedPercent().intValue());
        assertEquals(25, data11.getUsedSum().intValue());
    }

    @Test
    public void testCalculateDiscounts_SumDiscountsUpToMax()
            throws JsonGenerationException, JsonMappingException, IOException, RuntimeException, InterruptedException,
            DataIntegrityException {

        // проверяем что скидки сложатся, но не превысят масимальный процент

        List<VoucherResponseRow> rows = new ArrayList<VoucherResponseRow>();
        VoucherResponseRow row1;

        row1 = new VoucherResponseRow("rowName01", "001", new BigDecimal(100), 1, 1);
        row1.setClientPackId("clientPackId001");
        row1.setClientPackName("clientPackName001");
        row1.setId("ekt001");

        PackDiscountData data10 = new PackDiscountData();
        data10.setDiscountId("discount000");
        data10.setOriginalPercent(new BigDecimal(35));
        data10.setOriginalSum(new BigDecimal(35));

        PackDiscountData data11 = new PackDiscountData();
        data11.setDiscountId("discount001");
        data11.setOriginalPercent(new BigDecimal(25));
        data11.setOriginalSum(new BigDecimal(25));

        row1.setPackDiscount(new PackDiscountData[] {data10, data11});
        rows.add(row1);

        // transaction

        ProcessingTransaction transaction = new ProcessingTransaction(DateTime.now());
        transaction.setRows(rows);
        transaction.setDiscounts(Lists.newArrayList(new String[] {"discount000", "discount001"}));
        transaction.setDiscountPlanId("plan000");
        transaction.setDrugstoreNonRefundPercent(11);
        transaction.setMaxDiscountPercent(50);
        when(discountService.getDiscount("discount000"))
                .thenReturn(new Discount("discount000", "discount000", true, false, 35, 0, 0));
        when(discountService.getDiscount("discount001"))
                .thenReturn(new Discount("discount001", "discount001", true, false, 25, 0, 0));
        transaction.setCard(new VoucherResponseCard("0000000000000",
                "cardOwner", "000", true, "comment", "plan000", "plan000"));
        transaction.setDrugstoreId("drugstore000");
        Drugstore drugstore = new Drugstore("drugstore000");
        drugstore.setNonRefundPercent(5);
        when(dynamoService.getObject(Drugstore.class, "drugstore000")).thenReturn(drugstore);

        BigDecimal cardLimit = new BigDecimal(80);

        service.calculateDiscounts(transaction, cardLimit);

        // Проверяем баланс
        VoucherResponseRow row = transaction.getRows().get(0);
        assertEquals(50, row.getDiscountPercent().intValue()); // 35+15 (max=50)
        assertEquals(50, row.getCardLimitUsedSum().intValue());
        assertEquals(50, row.getDiscountSum().intValue());
        assertEquals(50, transaction.getCardLimitUsedSum().intValue());
        assertEquals(0, transaction.getCardLimitDeficit().intValue());
        assertEquals(35, data10.getUsedPercent().intValue());
        assertEquals(35, data10.getUsedSum().intValue());
        assertEquals(15, data11.getUsedPercent().intValue());
        assertEquals(15, data11.getUsedSum().intValue());
    }

    @Test
    public void testCalculateDiscounts_CardBalanceUsedWhenNeededOnly()
            throws JsonGenerationException, JsonMappingException, IOException, RuntimeException, InterruptedException,
            DataIntegrityException {

        // проверяем что с карты не списывается только та часть, что по скидке
        // идет "с баланса карты"

        List<VoucherResponseRow> rows = new ArrayList<VoucherResponseRow>();
        VoucherResponseRow row1;

        row1 = new VoucherResponseRow("rowName01", "001", new BigDecimal(100), 1, 1);
        row1.setClientPackId("clientPackId001");
        row1.setClientPackName("clientPackName001");
        row1.setId("ekt001");

        PackDiscountData data10 = new PackDiscountData();
        data10.setDiscountId("discount000");
        data10.setOriginalPercent(new BigDecimal(35));
        data10.setOriginalSum(new BigDecimal(35));

        PackDiscountData data11 = new PackDiscountData();
        data11.setDiscountId("discount001");
        data11.setOriginalPercent(new BigDecimal(25));
        data11.setOriginalSum(new BigDecimal(25));

        row1.setPackDiscount(new PackDiscountData[] {data10, data11});
        rows.add(row1);

        // transaction

        ProcessingTransaction transaction = new ProcessingTransaction(DateTime.now());
        transaction.setRows(rows);
        transaction.setDiscounts(Lists.newArrayList(new String[] {"discount000", "discount001"}));
        transaction.setDiscountPlanId("plan000");
        transaction.setDrugstoreNonRefundPercent(11);
        transaction.setMaxDiscountPercent(80);
        when(discountService.getDiscount("discount000"))
                .thenReturn(new Discount("discount000", "discount000", true, false, 35, 0, 0));
        when(discountService.getDiscount("discount001"))
                .thenReturn(new Discount("discount001", "discount001", false, false, 25, 0, 0));
        transaction.setCard(new VoucherResponseCard("0000000000000",
                "cardOwner", "000", true, "comment", "plan000", "plan000"));
        transaction.setDrugstoreId("drugstore000");
        Drugstore drugstore = new Drugstore("drugstore000");
        drugstore.setNonRefundPercent(5);
        when(dynamoService.getObject(Drugstore.class, "drugstore000")).thenReturn(drugstore);

        BigDecimal cardLimit = new BigDecimal(80);

        service.calculateDiscounts(transaction, cardLimit);

        // Проверяем баланс
        VoucherResponseRow row = transaction.getRows().get(0);
        assertEquals(60, row.getDiscountPercent().intValue()); // 35+25
        assertEquals(35, row.getCardLimitUsedSum().intValue()); // 35 only!
        assertEquals(60, row.getDiscountSum().intValue());
        assertEquals(35, transaction.getCardLimitUsedSum().intValue());
        assertEquals(0, transaction.getCardLimitDeficit().intValue());
        assertEquals(35, data10.getUsedPercent().intValue());
        assertEquals(35, data10.getUsedSum().intValue());
        assertEquals(25, data11.getUsedPercent().intValue());
        assertEquals(25, data11.getUsedSum().intValue());
    }
}
