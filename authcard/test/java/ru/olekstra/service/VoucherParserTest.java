package ru.olekstra.service;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.azure.service.QueueSender;
import ru.olekstra.common.service.CardLogService;
import ru.olekstra.common.service.DateTimeService;
import ru.olekstra.common.service.DiscountService;
import ru.olekstra.common.service.XmlMessageService;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.Drugstore;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.dto.VoucherProduct;
import ru.olekstra.domain.dto.VoucherRequest;
import ru.olekstra.domain.dto.VoucherResponseCard;
import ru.olekstra.domain.dto.VoucherResponseRow;
import ru.olekstra.exception.NotFoundException;
import ru.olekstra.processing.ConfirmProcessing;
import ru.olekstra.processing.DiscountRecalculateProcessing;
import ru.olekstra.processing.IllegalCardProcessing;
import ru.olekstra.processing.Processing;
import ru.olekstra.processing.RefundProcessing;
import ru.olekstra.processing.UnknownDrugstoreProcessing;
import ru.olekstra.processing.VerificationConflictProcessing;

@RunWith(MockitoJUnitRunner.class)
public class VoucherParserTest {

    private VoucherService voucherService;
    private DiscountService discountService;
    private CardLogService cardLogService;
    private XmlMessageService xmlMessageService;
    private CardService cardService;
    private DynamodbService service;
    private Mapper dozerBeanMapper;
    private DateTimeService dateService;
    private QueueSender sender;

    private VoucherProduct[] products;

    private UUID uuid;
    private String drugstoreId;
    private String cardNumber;
    private String phone;

    @Before
    public void setup() {
        voucherService = mock(VoucherService.class);
        discountService = mock(DiscountService.class);
        cardLogService = mock(CardLogService.class);
        xmlMessageService = mock(XmlMessageService.class);
        cardService = mock(CardService.class);
        service = mock(DynamodbService.class);
        dozerBeanMapper = new DozerBeanMapper();
        sender = mock(QueueSender.class);
        dateService = mock(DateTimeService.class);
        when(dateService.createDateTimeWithZone(anyString())).thenReturn(DateTime.now());
        when(dateService.createDefaultDateTime()).thenReturn(DateTime.now());

        uuid = UUID.fromString("00000000-0000-0000-0000-000000000000");
        drugstoreId = "someDrugstoreId";
        cardNumber = "777000111";
        phone = "0123456789";

        products = new VoucherProduct[3];
        products[0] = new VoucherProduct("000", "", "", new BigDecimal("2.00"), 1, 0, "000", "pack0");
        products[1] = new VoucherProduct("111", "", "", new BigDecimal("3.00"), 2, 0, "111", "pack1");
        products[2] = new VoucherProduct("222", "", "", new BigDecimal("5.00"), 3, 0, "222", "pack2");

    }

    Card createActiveCard() {
        Card card = new Card();

        card.setNumber("700");
        card.setHolderName("Иванов");
        card.setDisabled(false);
        card.setTelephoneNumber("0123456789");
        card.setDiscountPlan("testDiscount");

        return card;
    }

    Card createDisabledCard() {
        Card card = createActiveCard();
        card.setDisabled(true);
        return card;
    }

    Card createNotActiveCard() {
        Card card = new Card();

        card.setNumber("700");
        card.setDisabled(false);

        return card;
    }

    @Test
    public void testValidRequestWithoutUUID() throws JsonParseException,
            JsonMappingException, IOException, NotFoundException {

        Drugstore drugstore = new Drugstore(drugstoreId);
        Card card = createActiveCard();

        ProcessingTransaction transaction = new ProcessingTransaction(DateTime.now());

        VoucherRequest request = new VoucherRequest();
        request.setDrugstoreId(drugstoreId);
        request.setCardBarcode(cardNumber);

        when(voucherService.getDrugstore(drugstoreId)).thenReturn(drugstore);
        when(cardService.getCard(cardNumber)).thenReturn(card);
        when(voucherService.createNewTransaction(drugstore, card, request))
                .thenReturn(transaction);

        VoucherParser parser = new VoucherParser(voucherService,
                discountService,
                cardLogService,
                xmlMessageService,
                cardService,
                service,
                dozerBeanMapper,
                dateService,
                sender);

        Processing processing = parser.parse(request);

        assertThat(processing, instanceOf(DiscountRecalculateProcessing.class));

        verify(voucherService, times(1)).createNewTransaction(drugstore, card,
                request);
        verify(voucherService, times(0)).getProcessingTransaction(
                any(VoucherRequest.class));

    }
    
    @Test
    public void testRequestExactFieldsChecking() throws JsonGenerationException, JsonMappingException, IOException, NotFoundException {
        VoucherRequest request = new VoucherRequest();
        request.setVoucherId(uuid);
        request.setDrugstoreId(drugstoreId);
        request.setCardBarcode(cardNumber);
        request.setProducts(products);
        request.setSumToPay(new BigDecimal("0.00"));
        request.setExactRefundSum(BigDecimal.ZERO);
        request.setExactSumToPay(BigDecimal.ZERO);
        request.setExactNonRefundSum(BigDecimal.ZERO);

        VoucherResponseCard card = new VoucherResponseCard(cardNumber,
                "ivanov", phone, true, "", "", "");

        VoucherResponseRow row1 = new VoucherResponseRow("", "000",
                new BigDecimal("2.00"), 1, 1);
        row1.setClientPackId("000");

        VoucherResponseRow row2 = new VoucherResponseRow("", "111",
                new BigDecimal("3.00"), 2, 2);
        row2.setClientPackId("111");

        VoucherResponseRow row3 = new VoucherResponseRow("", "222",
                new BigDecimal("5.00"), 3, 3);
        row3.setClientPackId("222");

        List<VoucherResponseRow> rowList = new ArrayList<VoucherResponseRow>();
        rowList.add(row1);
        rowList.add(row2);
        rowList.add(row3);

        ProcessingTransaction transaction = new ProcessingTransaction(uuid,
                drugstoreId, card, "drugstore0", new BigDecimal("0.00"),
                new BigDecimal("0.00"), "discountPlanId0", 50, "1",
                new BigDecimal("0.00"), new String[1], rowList,
                DateTime.now().withZone(DateTimeZone.UTC));

        when(voucherService.getProcessingTransaction(request)).thenReturn(
                transaction);

        VoucherParser parser = new VoucherParser(voucherService,
                discountService,
                cardLogService,
                xmlMessageService,
                cardService,
                service,
                dozerBeanMapper,
                dateService,
                sender);

        Processing processing = parser.parse(request);

        assertThat(processing, instanceOf(DiscountRecalculateProcessing.class));
    }
    
    @Test
    public void testTransactionExactFieldsChecking() throws JsonGenerationException, JsonMappingException, IOException, NotFoundException {
        VoucherRequest request = new VoucherRequest();
        request.setVoucherId(uuid);
        request.setDrugstoreId(drugstoreId);
        request.setCardBarcode(cardNumber);
        request.setProducts(products);
        request.setSumToPay(new BigDecimal("0.00"));

        VoucherResponseCard card = new VoucherResponseCard(cardNumber,
                "ivanov", phone, true, "", "", "");

        VoucherResponseRow row1 = new VoucherResponseRow("", "000",
                new BigDecimal("2.00"), 1, 1);
        row1.setClientPackId("000");

        VoucherResponseRow row2 = new VoucherResponseRow("", "111",
                new BigDecimal("3.00"), 2, 2);
        row2.setClientPackId("111");

        VoucherResponseRow row3 = new VoucherResponseRow("", "222",
                new BigDecimal("5.00"), 3, 3);
        row3.setClientPackId("222");

        List<VoucherResponseRow> rowList = new ArrayList<VoucherResponseRow>();
        rowList.add(row1);
        rowList.add(row2);
        rowList.add(row3);

        ProcessingTransaction transaction = new ProcessingTransaction(uuid,
                drugstoreId, card, "drugstore0", new BigDecimal("0.00"),
                new BigDecimal("0.00"), "discountPlanId0", 50, "1",
                new BigDecimal("0.00"), new String[1], rowList,
                DateTime.now().withZone(DateTimeZone.UTC));

        transaction.setExactRefundSum(BigDecimal.ZERO);
        transaction.setExactSumToPay(BigDecimal.ZERO);
        transaction.setExactNonRefundSum(BigDecimal.ZERO);
        
        when(voucherService.getProcessingTransaction(request)).thenReturn(
                transaction);

        VoucherParser parser = new VoucherParser(voucherService,
                discountService,
                cardLogService,
                xmlMessageService,
                cardService,
                service,
                dozerBeanMapper,
                dateService,
                sender);

        Processing processing = parser.parse(request);

        assertThat(processing, instanceOf(DiscountRecalculateProcessing.class));

    }
    
    @Test
    public void testExactFieldsEqualsChecking() throws JsonGenerationException, JsonMappingException, IOException, NotFoundException {
        VoucherRequest request = new VoucherRequest();
        request.setVoucherId(uuid);
        request.setDrugstoreId(drugstoreId);
        request.setCardBarcode(cardNumber);
        request.setProducts(products);
        request.setSumToPay(new BigDecimal("0.00"));
        request.setExactRefundSum(BigDecimal.ZERO);
        request.setExactSumToPay(BigDecimal.ZERO);
        request.setExactNonRefundSum(BigDecimal.ZERO);

        VoucherResponseCard card = new VoucherResponseCard(cardNumber,
                "ivanov", phone, true, "", "", "");

        VoucherResponseRow row1 = new VoucherResponseRow("", "000",
                new BigDecimal("2.00"), 1, 1);
        row1.setClientPackId("000");

        VoucherResponseRow row2 = new VoucherResponseRow("", "111",
                new BigDecimal("3.00"), 2, 2);
        row2.setClientPackId("111");

        VoucherResponseRow row3 = new VoucherResponseRow("", "222",
                new BigDecimal("5.00"), 3, 3);
        row3.setClientPackId("222");

        List<VoucherResponseRow> rowList = new ArrayList<VoucherResponseRow>();
        rowList.add(row1);
        rowList.add(row2);
        rowList.add(row3);

        ProcessingTransaction transaction = new ProcessingTransaction(uuid,
                drugstoreId, card, "drugstore0", new BigDecimal("0.00"),
                new BigDecimal("0.00"), "discountPlanId0", 50, "1",
                new BigDecimal("0.00"), new String[1], rowList,
                DateTime.now().withZone(DateTimeZone.UTC));

        transaction.setExactRefundSum(BigDecimal.ZERO);
        transaction.setExactSumToPay(BigDecimal.ZERO);
        transaction.setExactNonRefundSum(BigDecimal.ZERO);
        
        when(voucherService.getProcessingTransaction(request)).thenReturn(
                transaction);

        VoucherParser parser = new VoucherParser(voucherService,
                discountService,
                cardLogService,
                xmlMessageService,
                cardService,
                service,
                dozerBeanMapper,
                dateService,
                sender);

        Processing processing = parser.parse(request);

        assertThat(processing, instanceOf(RefundProcessing.class));

    }
    @Test
    public void testValidRequestWithoutUUIDAndWithoutCard()
            throws JsonParseException, JsonMappingException, IOException, NotFoundException {

        Drugstore drugstore = new Drugstore(drugstoreId);

        VoucherRequest request = new VoucherRequest();
        request.setDrugstoreId(drugstoreId);
        request.setCardBarcode(cardNumber);

        when(voucherService.getDrugstore(drugstoreId)).thenReturn(drugstore);
        when(cardService.getCard(cardNumber)).thenThrow(new NotFoundException(""));

        VoucherParser parser = new VoucherParser(voucherService,
                discountService,
                cardLogService,
                xmlMessageService,
                cardService,
                service,
                dozerBeanMapper,
                dateService,
                sender);

        Processing processing = parser.parse(request);

        assertThat(processing, instanceOf(IllegalCardProcessing.class));

        verify(voucherService, times(0)).getProcessingTransaction(
                any(VoucherRequest.class));

    }

    @Test
    public void testValidRequestWithoutUUIDAndWithoutDrugstore()
            throws JsonParseException, JsonMappingException, IOException, NotFoundException {

        Card card = createActiveCard();

        VoucherRequest request = new VoucherRequest();
        request.setDrugstoreId(drugstoreId);
        request.setCardBarcode(cardNumber);

        when(voucherService.getDrugstore(drugstoreId)).thenReturn(null);
        when(cardService.getCard(cardNumber)).thenReturn(card);

        VoucherParser parser = new VoucherParser(voucherService,
                discountService,
                cardLogService,
                xmlMessageService,
                cardService,
                service,
                dozerBeanMapper,
                dateService,
                sender);

        Processing processing = parser.parse(request);

        assertThat(processing, instanceOf(UnknownDrugstoreProcessing.class));

        verify(voucherService, times(0)).getProcessingTransaction(
                any(VoucherRequest.class));

    }

    @Test
    public void testRequestWithCardWithoutDiscountPlan()
            throws JsonParseException, JsonMappingException, IOException, NotFoundException {

        Drugstore drugstore = new Drugstore(drugstoreId);
        Card card = createNotActiveCard();

        VoucherRequest request = new VoucherRequest();
        request.setDrugstoreId(drugstoreId);
        request.setCardBarcode(cardNumber);

        when(voucherService.getDrugstore(drugstoreId)).thenReturn(drugstore);
        when(cardService.getCard(cardNumber)).thenReturn(card);
        when(voucherService.createNewTransaction(drugstore, card, request))
                .thenReturn(new ProcessingTransaction(DateTime.now()));

        VoucherParser parser = new VoucherParser(voucherService,
                discountService,
                cardLogService,
                xmlMessageService,
                cardService,
                service,
                dozerBeanMapper,
                dateService,
                sender);

        Processing processing = parser.parse(request);

        assertThat(processing, instanceOf(IllegalCardProcessing.class));

        verify(voucherService, times(0)).createNewTransaction(drugstore, card,
                request);
        verify(voucherService, times(0)).getProcessingTransaction(
                any(VoucherRequest.class));
        verify(voucherService, times(1)).getDrugstore(anyString());
        verify(cardService, times(1)).getCard(anyString());

    }

    @Test
    public void testRequestWithDisabledCardWithoutUUID()
            throws JsonParseException, JsonMappingException, IOException, NotFoundException {

        Drugstore drugstore = new Drugstore(drugstoreId);
        Card card = createDisabledCard();

        VoucherRequest request = new VoucherRequest();
        request.setDrugstoreId(drugstoreId);
        request.setCardBarcode(cardNumber);

        when(voucherService.getDrugstore(drugstoreId)).thenReturn(drugstore);
        when(cardService.getCard(cardNumber)).thenReturn(card);
        when(voucherService.createNewTransaction(drugstore, card, request))
                .thenReturn(new ProcessingTransaction(DateTime.now()));

        VoucherParser parser = new VoucherParser(voucherService,
                discountService,
                cardLogService,
                xmlMessageService,
                cardService,
                service,
                dozerBeanMapper,
                dateService,
                sender);

        Processing processing = parser.parse(request);

        assertThat(processing, instanceOf(IllegalCardProcessing.class));

        verify(voucherService, times(0)).createNewTransaction(drugstore, card,
                request);
        verify(voucherService, times(0)).getProcessingTransaction(
                any(VoucherRequest.class));
        verify(voucherService, times(1)).getDrugstore(anyString());
        verify(cardService, times(1)).getCard(anyString());

    }

    @Test
    public void testValidRequestWithAdditionPacks()
            throws JsonGenerationException, JsonMappingException, IOException, NotFoundException {

        VoucherRequest request = new VoucherRequest();
        request.setVoucherId(uuid);
        request.setDrugstoreId(drugstoreId);
        request.setCardBarcode(cardNumber);
        request.setProducts(products);
        request.setSumToPay(new BigDecimal("0.00"));

        VoucherResponseCard card = new VoucherResponseCard(cardNumber,
                "ivanov", phone, true, "", "", "");

        // в БД было 0 строк с товарами (пустой список)
        ProcessingTransaction transaction = new ProcessingTransaction(uuid,
                drugstoreId, card, "drugstore0", new BigDecimal("0.00"),
                new BigDecimal("0.00"), "discountPlanId0", 50, "1",
                new BigDecimal("0.00"), new String[1],
                new ArrayList<VoucherResponseRow>(), DateTime.now().withZone(DateTimeZone.UTC));

        when(voucherService.getProcessingTransaction(request)).thenReturn(
                transaction);

        VoucherParser parser = new VoucherParser(voucherService,
                discountService,
                cardLogService,
                xmlMessageService,
                cardService,
                service,
                dozerBeanMapper,
                dateService,
                sender);

        Processing processing = parser.parse(request);

        assertThat(processing, instanceOf(DiscountRecalculateProcessing.class));

        verify(voucherService, times(0)).getDrugstore(anyString());
        verify(cardService, times(0)).getCard(anyString());
    }

    @Test
    public void testRequestWithErrorUUID() throws JsonGenerationException,
            JsonMappingException, IOException, NotFoundException {

        Drugstore drugstore = new Drugstore(drugstoreId);

        Card card = createActiveCard();

        VoucherRequest request = new VoucherRequest();
        request.setVoucherId(uuid);
        request.setDrugstoreId(drugstoreId);
        request.setCardBarcode(cardNumber);
        request.setProducts(products);
        request.setSumToPay(new BigDecimal("0.00"));

        when(voucherService.getDrugstore(drugstoreId)).thenReturn(drugstore);
        when(cardService.getCard(cardNumber)).thenReturn(card);
        when(voucherService.getProcessingTransaction(request)).thenReturn(null);

        VoucherParser parser = new VoucherParser(voucherService,
                discountService,
                cardLogService,
                xmlMessageService,
                cardService,
                service,
                dozerBeanMapper,
                dateService,
                sender);

        Processing processing = parser.parse(request);

        assertThat(processing, instanceOf(DiscountRecalculateProcessing.class));

        verify(voucherService, times(1)).getDrugstore(anyString());
        verify(cardService, times(1)).getCard(anyString());
        verify(voucherService, times(1)).createNewTransaction(drugstore, card,
                request);
    }

    @Test
    public void testValidRequestWithDifferentPacks()
            throws JsonGenerationException, JsonMappingException, IOException, NotFoundException {

        VoucherRequest request = new VoucherRequest();
        request.setVoucherId(uuid);
        request.setDrugstoreId(drugstoreId);
        request.setCardBarcode(cardNumber);
        request.setProducts(products);
        request.setSumToPay(new BigDecimal("0.00"));

        VoucherResponseCard card = new VoucherResponseCard(cardNumber,
                "ivanov", phone, true, "", "", "");

        VoucherResponseRow row1 = new VoucherResponseRow("", "000",
                new BigDecimal("2.00"), 1, 1);
        row1.setClientPackId("000");
        List<VoucherResponseRow> rowList = new ArrayList<VoucherResponseRow>();
        rowList.add(row1);

        // в БД было 1 строка с товаром (то есть в реквесте добавили две)
        ProcessingTransaction transaction = new ProcessingTransaction(uuid,
                drugstoreId, card, "drugstore0", new BigDecimal("0.00"),
                new BigDecimal("0.00"), "discountPlanId0", 50, "1",
                new BigDecimal("0.00"), new String[1], rowList,
                DateTime.now().withZone(DateTimeZone.UTC));

        when(voucherService.getProcessingTransaction(request)).thenReturn(
                transaction);

        VoucherParser parser = new VoucherParser(voucherService,
                discountService,
                cardLogService,
                xmlMessageService,
                cardService,
                service,
                dozerBeanMapper,
                dateService,
                sender);

        Processing processing = parser.parse(request);

        assertThat(processing, instanceOf(DiscountRecalculateProcessing.class));

        verify(voucherService, times(0)).getDrugstore(anyString());
        verify(cardService, times(0)).getCard(anyString());
    }

    @Test
    public void testValidRequestWithEqualPacks()
            throws JsonGenerationException, JsonMappingException, IOException, NotFoundException {

        VoucherRequest request = new VoucherRequest();
        request.setVoucherId(uuid);
        request.setDrugstoreId(drugstoreId);
        request.setCardBarcode(cardNumber);
        request.setProducts(products);
        request.setSumToPay(new BigDecimal("0.00"));

        VoucherResponseCard card = new VoucherResponseCard(cardNumber,
                "ivanov", phone, true, "", "", "");

        VoucherResponseRow row1 = new VoucherResponseRow("", "000",
                new BigDecimal("2.00"), 1, 1);
        row1.setClientPackId("000");

        VoucherResponseRow row2 = new VoucherResponseRow("", "111",
                new BigDecimal("3.00"), 2, 2);
        row2.setClientPackId("111");

        VoucherResponseRow row3 = new VoucherResponseRow("", "222",
                new BigDecimal("5.00"), 3, 3);
        row3.setClientPackId("222");

        List<VoucherResponseRow> rowList = new ArrayList<VoucherResponseRow>();
        rowList.add(row1);
        rowList.add(row2);
        rowList.add(row3);

        ProcessingTransaction transaction = new ProcessingTransaction(uuid,
                drugstoreId, card, "drugstore0", new BigDecimal("0.00"),
                new BigDecimal("0.00"), "discountPlanId0", 50, "1",
                new BigDecimal("0.00"), new String[1], rowList,
                DateTime.now().withZone(DateTimeZone.UTC));

        when(voucherService.getProcessingTransaction(request)).thenReturn(
                transaction);

        VoucherParser parser = new VoucherParser(voucherService,
                discountService,
                cardLogService,
                xmlMessageService,
                cardService,
                service,
                dozerBeanMapper,
                dateService,
                sender);

        Processing processing = parser.parse(request);

        assertThat(processing, instanceOf(RefundProcessing.class));

        verify(voucherService, times(0)).getDrugstore(anyString());
        verify(cardService, times(0)).getCard(anyString());
    }

    @Test
    public void testValidRequestWithDifferentPackCount()
            throws JsonGenerationException, JsonMappingException, IOException, NotFoundException {

        VoucherRequest request = new VoucherRequest();
        request.setVoucherId(uuid);
        request.setDrugstoreId(drugstoreId);
        request.setCardBarcode(cardNumber);
        request.setProducts(products);
        request.setSumToPay(new BigDecimal("0.00"));

        VoucherResponseCard card = new VoucherResponseCard(cardNumber,
                "ivanov", phone, true, "", "", "");

        VoucherResponseRow row1 = new VoucherResponseRow("", "000",
                new BigDecimal("2.00"), 1, 1);
        row1.setClientPackId("000");

        VoucherResponseRow row2 = new VoucherResponseRow("", "111",
                new BigDecimal("3.00"), 1, 2);
        row2.setClientPackId("111");

        VoucherResponseRow row3 = new VoucherResponseRow("", "222",
                new BigDecimal("5.00"), 1, 3);
        row3.setClientPackId("222");

        List<VoucherResponseRow> rowList = new ArrayList<VoucherResponseRow>();
        rowList.add(row1);
        rowList.add(row2);
        rowList.add(row3);

        // в БД теже строки, что и в реквесте, но с другими количествами
        ProcessingTransaction transaction = new ProcessingTransaction(uuid,
                drugstoreId, card, "drugstore0", new BigDecimal("0.00"),
                new BigDecimal("0.00"), "discountPlanId0", 50, "1",
                new BigDecimal("0.00"), new String[1], rowList,
                DateTime.now().withZone(DateTimeZone.UTC));

        when(voucherService.getProcessingTransaction(request)).thenReturn(
                transaction);

        VoucherParser parser = new VoucherParser(voucherService,
                discountService,
                cardLogService,
                xmlMessageService,
                cardService,
                service,
                dozerBeanMapper,
                dateService,
                sender);

        Processing processing = parser.parse(request);

        assertThat(processing, instanceOf(DiscountRecalculateProcessing.class));

        verify(voucherService, times(0)).getDrugstore(anyString());
        verify(cardService, times(0)).getCard(anyString());
    }

    @Test
    public void testRequestWithDisabledCardValidStartTransaction()
            throws JsonGenerationException, JsonMappingException, IOException, NotFoundException {

        Drugstore drugstore = new Drugstore(drugstoreId);
        Card card = createDisabledCard();

        VoucherRequest request = new VoucherRequest();
        request.setVoucherId(uuid);
        request.setDrugstoreId(drugstoreId);
        request.setCardBarcode(cardNumber);
        request.setProducts(products);
        request.setSumToPay(new BigDecimal("0.00"));

        VoucherResponseCard responseCard = new VoucherResponseCard(cardNumber,
                "ivanov", phone, true, "", "", "");

        VoucherResponseRow row1 = new VoucherResponseRow("", "000",
                new BigDecimal("2.00"), 1, 1);
        row1.setClientPackId("000");

        VoucherResponseRow row2 = new VoucherResponseRow("", "111",
                new BigDecimal("3.00"), 2, 2);
        row2.setClientPackId("111");

        VoucherResponseRow row3 = new VoucherResponseRow("", "222",
                new BigDecimal("5.00"), 3, 3);
        row3.setClientPackId("222");

        List<VoucherResponseRow> rowList = new ArrayList<VoucherResponseRow>();
        rowList.add(row1);
        rowList.add(row2);
        rowList.add(row3);

        ProcessingTransaction transaction = new ProcessingTransaction(uuid,
                drugstoreId, responseCard, "drugstore0",
                new BigDecimal("0.00"), new BigDecimal("0.00"),
                "discountPlanId0", 50, "1", new BigDecimal("0.00"),
                new String[1], rowList, DateTime.now().withZone(DateTimeZone.UTC));

        when(voucherService.getProcessingTransaction(request)).thenReturn(
                transaction);
        when(voucherService.getDrugstore(drugstoreId)).thenReturn(drugstore);
        when(cardService.getCard(cardNumber)).thenReturn(card);
        when(voucherService.createNewTransaction(drugstore, card, request))
                .thenReturn(new ProcessingTransaction(DateTime.now()));

        VoucherParser parser = new VoucherParser(voucherService,
                discountService,
                cardLogService,
                xmlMessageService,
                cardService,
                service,
                dozerBeanMapper,
                dateService,
                sender);

        Processing processing = parser.parse(request);

        assertThat(processing, instanceOf(RefundProcessing.class));

        verify(voucherService, times(0)).getDrugstore(anyString());
        verify(cardService, times(0)).getCard(anyString());

    }

    /**
     * Проверяем работу расчета, если необходимо пересчитать чек, а карты в БД
     * нет
     * 
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     * @throws NotFoundException
     */
    @Test
    public void testRequestWithoutValidCard() throws JsonGenerationException,
            JsonMappingException, IOException, NotFoundException {

        Drugstore drugstore = new Drugstore(drugstoreId);

        VoucherRequest request = new VoucherRequest();
        request.setVoucherId(uuid);
        request.setDrugstoreId(drugstoreId);
        request.setCardBarcode(cardNumber);
        request.setProducts(products);
        request.setSumToPay(new BigDecimal("0.00"));

        VoucherResponseCard responseCard = new VoucherResponseCard(cardNumber,
                "ivanov", phone, false, "", "", "");

        VoucherResponseRow row1 = new VoucherResponseRow("", "000",
                new BigDecimal("2.00"), 1, 1);
        row1.setClientPackId("000");

        VoucherResponseRow row2 = new VoucherResponseRow("", "111",
                new BigDecimal("3.00"), 2, 2);
        row2.setClientPackId("111");

        VoucherResponseRow row3 = new VoucherResponseRow("", "222",
                new BigDecimal("5.00"), 3, 3);
        row3.setClientPackId("222");

        List<VoucherResponseRow> rowList = new ArrayList<VoucherResponseRow>();
        rowList.add(row1);
        rowList.add(row2);
        rowList.add(row3);

        ProcessingTransaction transaction = new ProcessingTransaction(uuid,
                drugstoreId, responseCard, "drugstore0",
                new BigDecimal("0.00"), new BigDecimal("0.00"),
                "discountPlanId0", 50, "1", new BigDecimal("0.00"),
                new String[1], rowList, DateTime.now().withZone(DateTimeZone.UTC));

        when(voucherService.getProcessingTransaction(request)).thenReturn(
                transaction);
        when(voucherService.getDrugstore(drugstoreId)).thenReturn(drugstore);
        when(cardService.getCard(cardNumber)).thenThrow(new NotFoundException(""));

        VoucherParser parser = new VoucherParser(voucherService,
                discountService,
                cardLogService,
                xmlMessageService,
                cardService,
                service,
                dozerBeanMapper,
                dateService,
                sender);

        Processing processing = parser.parse(request);

        assertThat(processing, instanceOf(IllegalCardProcessing.class));

        verify(voucherService, times(1)).getDrugstore(drugstoreId);
        verify(cardService, times(1)).getCard(cardNumber);
    }

    /**
     * Пришел "повторный" реквест на закрытие чека
     * 
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     * @throws NotFoundException
     */
    @Test
    public void testRequestForCompletedTransactionEqualsSumAndPacks()
            throws JsonGenerationException, JsonMappingException, IOException, NotFoundException {
        VoucherRequest request = new VoucherRequest();
        request.setVoucherId(uuid);
        request.setDrugstoreId(drugstoreId);
        request.setCardBarcode(cardNumber);
        request.setProducts(products);
        request.setSumToPay(new BigDecimal("10.00"));

        VoucherResponseCard card = new VoucherResponseCard(cardNumber,
                "ivanov", phone, true, "", "", "");

        VoucherResponseRow row1 = new VoucherResponseRow("", "000",
                new BigDecimal("2.00"), 1, 1);
        row1.setClientPackId("000");

        VoucherResponseRow row2 = new VoucherResponseRow("", "111",
                new BigDecimal("3.00"), 2, 2);
        row2.setClientPackId("111");

        VoucherResponseRow row3 = new VoucherResponseRow("", "222",
                new BigDecimal("5.00"), 3, 3);
        row3.setClientPackId("222");

        List<VoucherResponseRow> rowList = new ArrayList<VoucherResponseRow>();
        rowList.add(row1);
        rowList.add(row2);
        rowList.add(row3);

        ProcessingTransaction transaction = new ProcessingTransaction(uuid,
                drugstoreId, card, "drugstore0", new BigDecimal("0.00"),
                new BigDecimal("0.00"), "discountPlanId0", 50, "1",
                new BigDecimal("10.00"), new String[1], rowList,
                DateTime.now().withZone(DateTimeZone.UTC));
        transaction.setComplete(true);

        when(voucherService.getProcessingTransaction(request)).thenReturn(
                transaction);

        VoucherParser parser = new VoucherParser(voucherService,
                discountService,
                cardLogService,
                xmlMessageService,
                cardService,
                service,
                dozerBeanMapper,
                dateService,
                sender);

        Processing processing = parser.parse(request);

        assertThat(processing, instanceOf(ConfirmProcessing.class));

        verify(voucherService, times(0)).getDrugstore(anyString());
        verify(cardService, times(0)).getCard(anyString());
        verify(voucherService, times(0)).createNewTransaction(
                any(Drugstore.class), any(Card.class),
                any(VoucherRequest.class));

    }

    /**
     * Пришел реквест на изменение количества товаров для уже закрытого чека
     * 
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     * @throws NotFoundException
     */
    @Test
    public void testRequestForCompletedTransactionEqualsSumOnly()
            throws JsonGenerationException, JsonMappingException, IOException, NotFoundException {

        VoucherRequest request = new VoucherRequest();
        request.setVoucherId(uuid);
        request.setDrugstoreId(drugstoreId);
        request.setCardBarcode(cardNumber);
        request.setProducts(products);
        request.setSumToPay(new BigDecimal("10.00"));

        VoucherResponseCard card = new VoucherResponseCard(cardNumber,
                "ivanov", phone, true, "", "", "");

        VoucherResponseRow row1 = new VoucherResponseRow("", "000",
                new BigDecimal("2.00"), 1, 1);
        row1.setClientPackId("000");

        List<VoucherResponseRow> rowList = new ArrayList<VoucherResponseRow>();
        rowList.add(row1);

        ProcessingTransaction transaction = new ProcessingTransaction(uuid,
                drugstoreId, card, "drugstore0", new BigDecimal("0.00"),
                new BigDecimal("0.00"), "discountPlanId0", 50, "1",
                new BigDecimal("10.00"), new String[1], rowList,
                DateTime.now().withZone(DateTimeZone.UTC));
        transaction.setComplete(true);

        when(voucherService.getProcessingTransaction(request)).thenReturn(
                transaction);

        VoucherParser parser = new VoucherParser(voucherService,
                discountService,
                cardLogService,
                xmlMessageService,
                cardService,
                service,
                dozerBeanMapper,
                dateService,
                sender);

        Processing processing = parser.parse(request);

        assertThat(processing, instanceOf(VerificationConflictProcessing.class));

        verify(voucherService, times(0)).getDrugstore(anyString());
        verify(cardService, times(0)).getCard(anyString());
        verify(voucherService, times(0)).createNewTransaction(
                any(Drugstore.class), any(Card.class),
                any(VoucherRequest.class));
    }

}
