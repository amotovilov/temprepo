package ru.olekstra.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.BeanUtils;
import org.springframework.context.MessageSource;

import ru.olekstra.awsutils.BatchOperationResult;
import ru.olekstra.awsutils.ComplexKey;
import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.BatchGetException;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.common.service.CardLogService;
import ru.olekstra.common.service.DateTimeService;
import ru.olekstra.common.service.DiscountService;
import ru.olekstra.domain.AuthCodeIndex;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.CardLog;
import ru.olekstra.domain.Discount;
import ru.olekstra.domain.DiscountData;
import ru.olekstra.domain.DiscountLimit;
import ru.olekstra.domain.Drugstore;
import ru.olekstra.domain.DrugstorePackIndex;
import ru.olekstra.domain.Pack;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.Refund;
import ru.olekstra.domain.dto.DiscountLimitUsedDetail;
import ru.olekstra.domain.dto.DiscountLimitUsedTotal;
import ru.olekstra.domain.dto.PackDiscountData;
import ru.olekstra.domain.dto.PackDiscountLimitData;
import ru.olekstra.domain.dto.VoucherProduct;
import ru.olekstra.domain.dto.VoucherRequest;
import ru.olekstra.domain.dto.VoucherResponseCard;
import ru.olekstra.domain.dto.VoucherResponseRow;
import ru.olekstra.exception.DataIntegrityException;
import ru.olekstra.exception.NotFoundException;

import com.google.inject.internal.Lists;

@RunWith(MockitoJUnitRunner.class)
public class VoucherServiceTest {

    private DynamodbService dynamoService;
    private AppSettingsService settingsService;
    private CardOperationService operationService;
    private DiscountService discountService;
    private MessageSource messageSource;
    private VoucherService service;
    private CardLogService cardLogService;
    private DateTimeService dateService;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void setUp() {
        dynamoService = mock(DynamodbService.class);
        settingsService = mock(AppSettingsService.class);
        operationService = mock(CardOperationService.class);
        discountService = mock(DiscountService.class);
        messageSource = mock(MessageSource.class);
        cardLogService = mock(CardLogService.class);
        dateService = mock(DateTimeService.class);
        when(dateService.createDateTimeWithZone(anyString())).thenReturn(DateTime.now());
        when(dateService.createDefaultDateTime()).thenReturn(DateTime.now());

        service = new VoucherService(dynamoService, settingsService,
                operationService, discountService, messageSource, cardLogService,
                dateService);
    }

    @Test
    public void testGetProcessingTransaction() throws JsonGenerationException, JsonMappingException, IOException {

        UUID uuid = UUID.randomUUID();
        String drugstoreId = "drugstoreId";

        VoucherRequest request = new VoucherRequest();
        request.setVoucherId(uuid);
        request.setDrugstoreId(drugstoreId);

        DateTime date = DateTime.now(DateTimeZone.UTC);
        String todayKey = ProcessingTransaction.buildRangeKey(date, uuid);

        ProcessingTransaction transaction = new ProcessingTransaction(DateTime.now());
        transaction.setVoucherId(uuid);
        transaction.setDrugstoreId(drugstoreId);

        // today's transaction is found
        when(dateService.createUTCDate()).thenReturn(date);

        when(dynamoService.getObject(eq(ProcessingTransaction.class), eq(drugstoreId), eq(todayKey)))
                .thenReturn(transaction);

        ProcessingTransaction result = service.getProcessingTransaction(request);

        verify(dynamoService, times(1)).getObject(eq(ProcessingTransaction.class), eq(drugstoreId), eq(todayKey));

        assertEquals(transaction, result);

        // today's transaction is not found, but yesterday's is found
        when(dynamoService.getObject(eq(ProcessingTransaction.class), eq(drugstoreId), eq(todayKey))).thenReturn(null);

        date = date.minusDays(1);
        String yesterdayKey = ProcessingTransaction.buildRangeKey(date, uuid);
        when(dynamoService.getObject(eq(ProcessingTransaction.class), eq(drugstoreId), eq(yesterdayKey)))
                .thenReturn(transaction);

        result = service.getProcessingTransaction(request);

        verify(dynamoService, times(2)).getObject(eq(ProcessingTransaction.class), eq(drugstoreId), eq(todayKey));
        verify(dynamoService, times(1)).getObject(eq(ProcessingTransaction.class), eq(drugstoreId), eq(yesterdayKey));

        assertEquals(transaction, result);

        // no transactions are found
        when(dynamoService.getObject(eq(ProcessingTransaction.class), eq(drugstoreId), eq(yesterdayKey)))
                .thenReturn(null);

        result = service.getProcessingTransaction(request);

        verify(dynamoService, times(3)).getObject(eq(ProcessingTransaction.class), eq(drugstoreId), eq(todayKey));
        verify(dynamoService, times(2)).getObject(eq(ProcessingTransaction.class), eq(drugstoreId), eq(yesterdayKey));

        assertNull(result);
    }

    @Test
    public void testGetErrorProcessingTransaction()
            throws JsonGenerationException, JsonMappingException, IOException {

        UUID uuid = UUID.randomUUID();
        String drugstoreId = "drugstoreId";
        String cardNumber = "777";

        VoucherRequest request = new VoucherRequest();
        request.setVoucherId(uuid);
        request.setDrugstoreId(drugstoreId);
        request.setCardBarcode(cardNumber);

        String warning = "warning";
        String warningMessage = "some warning";

        when(messageSource.getMessage(eq(warning), eq((Object[]) null), eq((Locale) null))).thenReturn(warningMessage);
        when(dateService.createUTCDate()).thenReturn(DateTime.now().withZone(DateTimeZone.UTC));

        ProcessingTransaction transaction = service.getErrorProcessingTransaction(request, warning, null);

        verify(messageSource).getMessage(eq(warning), eq((Object[]) null), eq((Locale) null));

        assertNotNull(transaction);
        assertEquals(drugstoreId, transaction.getDrugstoreId());
        assertEquals(cardNumber, transaction.getCard().getNumber());
        assertEquals(0, transaction.getRows().size());
        assertNotSame(uuid, transaction.getVoucherId());
        assertNotNull(transaction.getDate());
    }

    @Test
    public void testGetNextDrugstoreCode() {

        String drugstoreId = "drugstoreId";
        Map<String, Object> attributes = new HashMap<String, Object>();

        String authCode = null;
        String newAuthCode = Drugstore.START_AUTH_CODE;

        // new auth code
        attributes.put(Drugstore.FLD_AUTH_CODE, authCode);

        when(
                dynamoService.getAttributes(eq(Drugstore.TABLE_NAME),
                        eq(drugstoreId), eq(Arrays
                                .asList(Drugstore.FLD_AUTH_CODE)))).thenReturn(
                attributes);
        when(
                dynamoService.updateAttributeWithCondition(
                        eq(Drugstore.TABLE_NAME), eq(drugstoreId),
                        eq(Drugstore.FLD_AUTH_CODE), eq(authCode),
                        eq(newAuthCode))).thenReturn(true);

        String result = service.getNextDrugstoreCode(drugstoreId);

        verify(dynamoService).getAttributes(eq(Drugstore.TABLE_NAME),
                eq(drugstoreId), eq(Arrays.asList(Drugstore.FLD_AUTH_CODE)));
        verify(dynamoService).updateAttributeWithCondition(
                eq(Drugstore.TABLE_NAME), eq(drugstoreId),
                eq(Drugstore.FLD_AUTH_CODE), eq(authCode), eq(newAuthCode));

        assertEquals(newAuthCode, result);

        // auth code is already set
        authCode = Drugstore.START_AUTH_CODE;
        newAuthCode = String.valueOf((Integer
                .parseInt(Drugstore.START_AUTH_CODE) + 1));

        attributes.put(Drugstore.FLD_AUTH_CODE, authCode);

        when(
                dynamoService.getAttributes(eq(Drugstore.TABLE_NAME),
                        eq(drugstoreId), eq(Arrays
                                .asList(Drugstore.FLD_AUTH_CODE)))).thenReturn(
                attributes);
        when(
                dynamoService.updateAttributeWithCondition(
                        eq(Drugstore.TABLE_NAME), eq(drugstoreId),
                        eq(Drugstore.FLD_AUTH_CODE), eq(authCode),
                        eq(newAuthCode))).thenReturn(true);

        result = service.getNextDrugstoreCode(drugstoreId);

        verify(dynamoService, times(2)).getAttributes(eq(Drugstore.TABLE_NAME),
                eq(drugstoreId), eq(Arrays.asList(Drugstore.FLD_AUTH_CODE)));
        verify(dynamoService).updateAttributeWithCondition(
                eq(Drugstore.TABLE_NAME), eq(drugstoreId),
                eq(Drugstore.FLD_AUTH_CODE), eq(authCode), eq(newAuthCode));

        assertEquals(newAuthCode, result);

        // max auth code is already set
        authCode = Drugstore.MAX_AUTH_CODE;
        newAuthCode = Drugstore.START_AUTH_CODE;

        attributes.put(Drugstore.FLD_AUTH_CODE, authCode);

        when(
                dynamoService.getAttributes(eq(Drugstore.TABLE_NAME),
                        eq(drugstoreId), eq(Arrays
                                .asList(Drugstore.FLD_AUTH_CODE)))).thenReturn(
                attributes);
        when(
                dynamoService.updateAttributeWithCondition(
                        eq(Drugstore.TABLE_NAME), eq(drugstoreId),
                        eq(Drugstore.FLD_AUTH_CODE), eq(authCode),
                        eq(newAuthCode))).thenReturn(true);

        result = service.getNextDrugstoreCode(drugstoreId);

        verify(dynamoService, times(3)).getAttributes(eq(Drugstore.TABLE_NAME),
                eq(drugstoreId), eq(Arrays.asList(Drugstore.FLD_AUTH_CODE)));
        verify(dynamoService).updateAttributeWithCondition(
                eq(Drugstore.TABLE_NAME), eq(drugstoreId),
                eq(Drugstore.FLD_AUTH_CODE), eq(authCode), eq(newAuthCode));

        assertEquals(newAuthCode, result);

        // parallel auth code change
        authCode = "101";
        newAuthCode = "102";
        String freshAuthCode = "103";

        Map<String, Object> freshAttributes = new HashMap<String, Object>();
        attributes.put(Drugstore.FLD_AUTH_CODE, authCode);
        freshAttributes.put(Drugstore.FLD_AUTH_CODE, newAuthCode);

        when(
                dynamoService.getAttributes(eq(Drugstore.TABLE_NAME),
                        eq(drugstoreId), eq(Arrays
                                .asList(Drugstore.FLD_AUTH_CODE)))).thenReturn(
                attributes).thenReturn(freshAttributes);
        when(
                dynamoService.updateAttributeWithCondition(
                        eq(Drugstore.TABLE_NAME), eq(drugstoreId),
                        eq(Drugstore.FLD_AUTH_CODE), eq(authCode),
                        eq(newAuthCode))).thenReturn(false);
        when(
                dynamoService.updateAttributeWithCondition(
                        eq(Drugstore.TABLE_NAME), eq(drugstoreId),
                        eq(Drugstore.FLD_AUTH_CODE), eq(newAuthCode),
                        eq(freshAuthCode))).thenReturn(true);

        result = service.getNextDrugstoreCode(drugstoreId);

        verify(dynamoService, times(5)).getAttributes(eq(Drugstore.TABLE_NAME),
                eq(drugstoreId), eq(Arrays.asList(Drugstore.FLD_AUTH_CODE)));
        verify(dynamoService).updateAttributeWithCondition(
                eq(Drugstore.TABLE_NAME), eq(drugstoreId),
                eq(Drugstore.FLD_AUTH_CODE), eq(authCode), eq(newAuthCode));
        verify(dynamoService)
                .updateAttributeWithCondition(eq(Drugstore.TABLE_NAME),
                        eq(drugstoreId), eq(Drugstore.FLD_AUTH_CODE),
                        eq(newAuthCode), eq(freshAuthCode));

        assertEquals(freshAuthCode, result);
    }

    @Test
    public void testGetDrugstoreLastAuthcode() {

        String drugstoreId = "drugstoreId";
        String authCode = "100";

        Map<String, Object> attributes = new HashMap<String, Object>();
        attributes.put(Drugstore.FLD_AUTH_CODE, authCode);

        when(
                dynamoService.getAttributes(eq(Drugstore.TABLE_NAME),
                        eq(drugstoreId), eq(Arrays
                                .asList(Drugstore.FLD_AUTH_CODE)))).thenReturn(
                attributes);

        String result = service.getDrugstoreLastAuthcode(drugstoreId);

        verify(dynamoService).getAttributes(eq(Drugstore.TABLE_NAME),
                eq(drugstoreId), eq(Arrays.asList(Drugstore.FLD_AUTH_CODE)));

        assertEquals(authCode, result);
    }

    @Test
    public void testSetRecords() throws JsonGenerationException,
            JsonMappingException, IOException {

        String number = "777";
        VoucherResponseCard card = new VoucherResponseCard(number, "owner",
                "1234567890", true, "comment", "planId", "planName");

        String discountId1 = "discountId1";
        String discountId2 = "discountId2";

        BigDecimal pack1Data1Percent = new BigDecimal("10");
        PackDiscountData pack1Data1 = new PackDiscountData(discountId1);
        pack1Data1.setOriginalPercent(pack1Data1Percent);

        BigDecimal ppack1Data2Percent = new BigDecimal("10");
        PackDiscountData pack1Data2 = new PackDiscountData(discountId2);
        pack1Data2.setOriginalPercent(ppack1Data2Percent);

        BigDecimal pack2Data1Percent = new BigDecimal("10");
        PackDiscountData pack2Data1 = new PackDiscountData(discountId1);
        pack2Data1.setOriginalPercent(pack2Data1Percent);

        BigDecimal pack2Data2Percent = BigDecimal.ZERO;
        PackDiscountData pack2Data2 = new PackDiscountData(discountId1);
        pack2Data2.setOriginalPercent(pack2Data2Percent);

        VoucherResponseRow row1 = new VoucherResponseRow("Product1", "barcode",
                new BigDecimal(100.0), 1, new BigDecimal(70.0), new BigDecimal(
                        30.0), new BigDecimal(30.0), new BigDecimal(15.0),
                new BigDecimal(100.0), new BigDecimal(30.0), "clientPackId1",
                "clientPackName1", 1);
        row1.setPackDiscount(new PackDiscountData[] {pack1Data1, pack1Data2});

        VoucherResponseRow row2 = new VoucherResponseRow("Product2",
                "barcode1", new BigDecimal(1000.0), 1, new BigDecimal(600.0),
                new BigDecimal(400.0), new BigDecimal(40),
                new BigDecimal(250.0), new BigDecimal(600.0), new BigDecimal(
                        400.0), "clientPackId2", "clientPackName2", 2);
        row2.setPackDiscount(new PackDiscountData[] {pack2Data1, pack2Data2});

        VoucherResponseRow row3 = new VoucherResponseRow("Product2",
                "barcode1", new BigDecimal(1000.0), 1, new BigDecimal(600.0),
                new BigDecimal(400.0), new BigDecimal(40),
                new BigDecimal(250.0), new BigDecimal(600.0), new BigDecimal(
                        400.0), "clientPackId2", "clientPackName2", 3);
        row3.setPackDiscount(new PackDiscountData[] {pack2Data1, pack2Data2});

        List<VoucherResponseRow> rows = new ArrayList<VoucherResponseRow>();
        rows.add(row1);
        rows.add(row2);
        rows.add(row3);

        UUID voucherId = UUID.randomUUID();

        ProcessingTransaction transaction = new ProcessingTransaction(DateTime.now());
        transaction.setVoucherId(voucherId);
        transaction.setCard(card);
        transaction.setRows(rows);

        String transactionNote = "transactionnote";
        String duplicateNote = "duplicatenote";

        when(messageSource.getMessage(eq("msg.transactionnote"),
                any(Object[].class),
                eq((Locale) null))).thenReturn(transactionNote);

        when(messageSource.getMessage(eq("msg.transactionnoteWithDuplicates"),
                any(Object[].class),
                eq((Locale) null))).thenReturn(duplicateNote);

        CardLog cardLogTr = new CardLog();
        cardLogTr.setNote(transactionNote);

        CardLog cardLogDp = new CardLog();
        cardLogDp.setNote(duplicateNote);

        when(cardLogService.createTransactionCardLog(eq(transaction), eq(transactionNote), any(Integer.class)))
                .thenReturn(cardLogTr);
        when(cardLogService.createTransactionCardLog(eq(transaction), eq(duplicateNote), any(Integer.class)))
                .thenReturn(cardLogDp);

        List<CardLog> recordList = service.setRecords(transaction);

        verify(messageSource, times(1)).getMessage(eq("msg.transactionnote"),
                any(Object[].class), eq((Locale) null));

        verify(messageSource, times(2)).getMessage(
                eq("msg.transactionnoteWithDuplicates"),
                any(Object[].class), eq((Locale) null));

        assertEquals(3, recordList.size());

        assertNotNull(recordList.get(0).getNote());
        assertNotNull(recordList.get(1).getNote());
        assertNotNull(recordList.get(2).getNote());

    }

    @Test
    public void testSetRefunds() throws JsonGenerationException,
            JsonMappingException, IOException {

        String number = "777";
        VoucherResponseCard card = new VoucherResponseCard(number, "owner",
                "1234567890", true, "comment", "planId", "planName");

        String discountId1 = "discountId1";
        String discountId2 = "discountId2";

        BigDecimal pack1Data1Percent = new BigDecimal("10");
        PackDiscountData pack1Data1 = new PackDiscountData(discountId1);
        pack1Data1.setOriginalPercent(pack1Data1Percent);

        BigDecimal ppack1Data2Percent = new BigDecimal("10");
        PackDiscountData pack1Data2 = new PackDiscountData(discountId2);
        pack1Data2.setOriginalPercent(ppack1Data2Percent);

        BigDecimal pack2Data1Percent = new BigDecimal("10");
        PackDiscountData pack2Data1 = new PackDiscountData(discountId1);
        pack2Data1.setOriginalPercent(pack2Data1Percent);

        BigDecimal pack2Data2Percent = BigDecimal.ZERO;
        PackDiscountData pack2Data2 = new PackDiscountData(discountId1);
        pack2Data2.setOriginalPercent(pack2Data2Percent);

        VoucherResponseRow row1 = new VoucherResponseRow("Product1", "barcode",
                new BigDecimal(100.0), 1, new BigDecimal(70.0), new BigDecimal(
                        30.0), new BigDecimal(30.0), new BigDecimal(15.0),
                new BigDecimal(100.0), new BigDecimal(30.0), "clientPackId1",
                "clientPackName1", 1);
        row1.setPackDiscount(new PackDiscountData[] {pack1Data1, pack1Data2});

        VoucherResponseRow row2 = new VoucherResponseRow("Product2",
                "barcode1", new BigDecimal(1000.0), 1, new BigDecimal(600.0),
                new BigDecimal(400.0), new BigDecimal(40),
                new BigDecimal(250.0), new BigDecimal(600.0), new BigDecimal(
                        400.0), "clientPackId2", "clientPackName2", 2);
        row2.setPackDiscount(new PackDiscountData[] {pack2Data1, pack2Data2});

        VoucherResponseRow row3 = new VoucherResponseRow("Product2",
                "barcode1", new BigDecimal(1000.0), 1, new BigDecimal(600.0),
                new BigDecimal(400.0), new BigDecimal(40),
                new BigDecimal(250.0), new BigDecimal(600.0), new BigDecimal(
                        400.0), "clientPackId2", "clientPackName2", 3);
        row3.setPackDiscount(new PackDiscountData[] {pack2Data1, pack2Data2});

        List<VoucherResponseRow> rows = new ArrayList<VoucherResponseRow>();
        rows.add(row1);
        rows.add(row2);
        rows.add(row3);

        UUID voucherId = UUID.randomUUID();

        ProcessingTransaction transaction = new ProcessingTransaction(DateTime.now());
        transaction.setVoucherId(voucherId);
        transaction.setCard(card);
        transaction.setRows(rows);

        DateTime period = DateTime.now().withZone(DateTimeZone.UTC);

        List<Refund> refundList = service.setRefunds(transaction, period);

        assertEquals(4, refundList.size());

        // проверяем номера позиций в refunds
        assertEquals(1, Integer.parseInt(refundList.get(0).getRow()));
        assertEquals(1, Integer.parseInt(refundList.get(1).getRow()));
        assertEquals(2, Integer.parseInt(refundList.get(2).getRow()));
        assertEquals(3, Integer.parseInt(refundList.get(3).getRow()));

    }

    @Test
    public void testCreateNewRefund() throws JsonGenerationException,
            JsonMappingException, IOException {

        String discountId = "discountId";
        UUID voucherId = UUID.randomUUID();
        String rowNumber = "1";
        String authCode = "100";
        String cardNumber = "777";
        DateTime period = new DateTime();
        DateTime dateTime = period;

        BigDecimal rowPrice = new BigDecimal("100.00");
        BigDecimal rowCount = new BigDecimal("1");
        BigDecimal rowSumWithoutDiscount = new BigDecimal("100.00");
        BigDecimal rowDiscountPercent = new BigDecimal("25");
        BigDecimal rowDiscountSum = new BigDecimal("75.00");
        BigDecimal rowPaidSum = new BigDecimal("75.00");
        BigDecimal rowDrugstoreRefund = new BigDecimal("25.00");
        BigDecimal rowCardLimitUsedSum = new BigDecimal("75.00");

        BigDecimal discountPercentOriginal = new BigDecimal("25");
        BigDecimal discountPercentUsed = new BigDecimal("25");
        BigDecimal discountRefundOriginalSum = new BigDecimal("25.00");
        BigDecimal discountRefundUsedSum = new BigDecimal("25.00");
        BigDecimal discountRefundSum = new BigDecimal("25.00");

        String clientPackId = "clientPackId";
        String clientPackName = "clientPackName";
        String packId = "packId";
        String packName = "packName";
        String manufacturer = "manufacturer";
        String drugstoreId = "drugstoreId";
        String drugstoreName = "drugstoreName";
        String currentVersion = "1";
        String barcode = "barcode";

        VoucherResponseCard card = new VoucherResponseCard(cardNumber, "owner",
                "1234567890", true, "comment", "", "");

        PackDiscountData pack = new PackDiscountData(discountId);
        pack.setDiscountId(discountId);
        pack.setOriginalPercent(discountPercentOriginal);
        pack.setOriginalSum(discountRefundOriginalSum);
        pack.setUsedPercent(discountPercentUsed);
        pack.setUsedSum(discountRefundUsedSum);
        pack.setRefundSum(discountRefundSum);

        VoucherResponseRow row = new VoucherResponseRow(packName, barcode,
                rowPrice, Integer.parseInt(rowCount.toString()), rowPaidSum,
                rowDiscountSum, rowDiscountPercent, rowDrugstoreRefund,
                rowSumWithoutDiscount, rowCardLimitUsedSum, clientPackId,
                clientPackName, 1);
        row.setPackDiscount(new PackDiscountData[] {pack});
        row.setId(packId);
        row.setManufacturer(manufacturer);

        List<VoucherResponseRow> rows = new ArrayList<VoucherResponseRow>();
        rows.add(row);

        ProcessingTransaction transaction = new ProcessingTransaction(
                voucherId, drugstoreId, card, drugstoreName,
                rowSumWithoutDiscount, rowDiscountSum, discountId,
                rowDiscountPercent.intValue(), currentVersion, rowPaidSum,
                new String[] {}, rows, DateTime.now().withZone(DateTimeZone.UTC));
        transaction.setDate(dateTime);
        transaction.setVoucherId(voucherId);
        transaction.setCard(card);
        transaction.setAuthCode(authCode);

        Refund result = service.createNewRefund(transaction, row, pack, period,
                Integer.parseInt(rowNumber));

        assertEquals(Refund.dateFormatter.print(period), result.getPeriod());
        assertEquals(discountId, result.getDiscountId());
        assertEquals(voucherId.toString(), result.getVoucherId());
        assertEquals(rowNumber, result.getRow());

        assertEquals(authCode, result.getAuthCode());
        assertEquals(cardNumber, result.getCardId());
        assertEquals(dateTime.minusMillis(dateTime.getMillisOfSecond())
                .getMillis(), result.getDateTime().getMillis());

        assertEquals(rowPrice, result.getRowPrice());
        assertEquals(rowCount, result.getRowCount());
        assertEquals(rowSumWithoutDiscount, result.getRowSumWithoutDiscount());
        assertEquals(rowDiscountPercent, result.getRowDiscountPercent());
        assertEquals(rowDiscountSum, result.getRowDiscountSum());
        assertEquals(rowPaidSum, result.getRowPaidSum());
        assertEquals(rowDrugstoreRefund, result.getRowDrugstoreRefund());

        assertEquals(discountPercentOriginal, result
                .getDiscountPercentOriginal());
        assertEquals(discountPercentUsed, result.getDiscountPercentUsed());
        assertEquals(discountRefundOriginalSum, result
                .getDiscountRefundOriginalSum());
        assertEquals(discountRefundUsedSum, result.getDiscountRefundUsedSum());
        assertEquals(discountRefundSum, result.getDiscountRefundSum());

        assertEquals(clientPackId, result.getClientPackId());
        assertEquals(clientPackName, result.getClientPackName());
        assertEquals(packId, result.getPackId());
        assertEquals(packName, result.getPackName());
        assertEquals(manufacturer, result.getManufacturer());
        assertEquals(drugstoreId, result.getDrugstoreId());
        assertEquals(drugstoreName, result.getDrugstoreName());
    }

    @Test
    public void testUpdateBalance() throws JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException,
            NotFoundException {

        BigDecimal discountSum = new BigDecimal("25.00");
        BigDecimal cardBalance = new BigDecimal("100.00");
        BigDecimal cardBalanceUsedtSum = new BigDecimal("15.00");
        BigDecimal updatedBalance = cardBalance.subtract(cardBalanceUsedtSum);

        String cardNumber = "777";
        Card card = new Card(cardNumber);
        VoucherResponseCard voucherCard = new VoucherResponseCard(cardNumber,
                "owner", "1234567890", true, "comment", "", "");

        String drugstoreId = "drugstoreId";
        ProcessingTransaction transaction = new ProcessingTransaction(DateTime.now().withZone(DateTimeZone.UTC));
        transaction.setDrugstoreId(drugstoreId);
        transaction.setCard(voucherCard);
        transaction.setDiscountSum(discountSum);
        transaction.setCardLimitUsedSum(cardBalanceUsedtSum);

        when(dynamoService.getObject(eq(Card.class), eq(cardNumber)))
                .thenReturn(card);

        when(operationService.saveLimit(eq(drugstoreId), eq(card), eq(discountSum)))
                .thenReturn(updatedBalance);

        service.updateBalance(transaction);

        verify(dynamoService).getObject(eq(Card.class), eq(cardNumber));
        verify(operationService).saveLimit(eq(drugstoreId), eq(card), eq(cardBalanceUsedtSum));
    }

    @Test
    public void testSaveRefundIndex() throws ItemSizeLimitExceededException {

        DateTime period = new DateTime();
        String authcode = "101";
        String drugstoreId = "drugstoreId";
        UUID voucherId = UUID.randomUUID();
        String processingTransactionRangeKey = "201201#" + voucherId;

        dynamoService.putObjectOrDie(any(AuthCodeIndex.class));

        service.saveRefundIndex(period, drugstoreId, processingTransactionRangeKey, authcode);

        verify(dynamoService, times(2)).putObjectOrDie(any(AuthCodeIndex.class));
    }

    @Test
    public void testGetDiscountData() throws RuntimeException,
            InterruptedException, IOException {

        List<String> discountIds = Arrays.asList("discountId1", "discountId2");
        List<String> packIds = Arrays.asList("ekt1", "ekt2");
        List<DiscountData> discountData = Arrays.asList(new DiscountData(),
                new DiscountData());

        BatchOperationResult<DiscountData> operationResult = new BatchOperationResult<DiscountData>(
                discountData, null);

        when(
                dynamoService.getObjects(eq(DiscountData.class), eq(discountIds
                        .toArray(new String[discountIds.size()])), eq(packIds
                        .toArray(new String[packIds.size()])))).thenReturn(
                operationResult);

        List<DiscountData> result = service.getDiscountData(discountIds,
                packIds);

        verify(dynamoService).getObjects(eq(DiscountData.class),
                eq(discountIds.toArray(new String[discountIds.size()])),
                eq(packIds.toArray(new String[packIds.size()])));

        assertEquals(discountData, result);
    }

    @Test
    public void testCompareAndFillRows() throws JsonGenerationException,
            JsonMappingException, IOException {
        VoucherRequest request = new VoucherRequest();
        VoucherProduct[] products = new VoucherProduct[] {
                new VoucherProduct("0031", "ekt01", "10", new BigDecimal(10l),
                        10, 2014, "packId01", "packName01"),
                new VoucherProduct("0032", "ekt02", "11", new BigDecimal(11l),
                        11, 2014, "packId02", "packName02"),
                new VoucherProduct("0033", "ekt03", "12", new BigDecimal(12l),
                        12, 2014, "packId03", "packName03"),
                new VoucherProduct("0034", "ekt04", "13", new BigDecimal(13l),
                        13, 2014, "packId04", "packName04")
        };
        request.setProducts(products);

        ProcessingTransaction transaction = new ProcessingTransaction(DateTime.now());
        List<VoucherResponseRow> rows = new ArrayList<VoucherResponseRow>();

        VoucherResponseRow row0, row1, row2, row3, rowExp0, rowExp1, rowExp2, rowExp3;

        // существующая строка, не изменилась
        row0 = new VoucherResponseRow("rowName01", "00301",
                new BigDecimal(10l), 10, 1);
        row0.setClientPackId("packId01");
        row0.setClientPackName("packName01");
        row0.setDiscountPercent(ProcessingTransaction.FORMATTED_ZERO);
        row0.setDiscountSum(ProcessingTransaction.FORMATTED_ZERO);
        row0.setPackDiscount(null);
        row0.setSumToPay(ProcessingTransaction.FORMATTED_ZERO);
        row0.setCardLimitUsedSum(ProcessingTransaction.FORMATTED_ZERO);
        row0.setRefundSum(ProcessingTransaction.FORMATTED_ZERO);
        rows.add(row0);
        rowExp0 = new VoucherResponseRow();
        BeanUtils.copyProperties(row0, rowExp0);

        // Проверяем изменение количества (раньше было 100, будет 11)
        row1 = new VoucherResponseRow("rowName02", "00302",
                new BigDecimal(11l), 100, 2);
        row1.setClientPackId("packId02");
        row1.setClientPackName("packName02");
        row1.setDiscountPercent(BigDecimal.ONE);
        row1.setDiscountSum(BigDecimal.ONE);
        row1.setPackDiscount(new PackDiscountData[1]);
        row1.setSumToPay(BigDecimal.ONE);
        row1.setCardLimitUsedSum(BigDecimal.ONE);
        row1.setRefundSum(BigDecimal.ONE);
        rows.add(row1);
        rowExp1 = new VoucherResponseRow();
        BeanUtils.copyProperties(row1, rowExp1);
        rowExp1.setQuantity(11); // на входе у нас 11 будет вместо 100

        // Проверяем изменение цены (раньше было 120, будет 12)
        row2 = new VoucherResponseRow("rowName03", "00303",
                new BigDecimal(120l), 12, 3);
        row2.setClientPackId("packId03");
        row2.setClientPackName("packName03");
        row2.setDiscountPercent(ProcessingTransaction.FORMATTED_ZERO);
        row2.setDiscountSum(ProcessingTransaction.FORMATTED_ZERO);
        row2.setPackDiscount(null);
        row2.setSumToPay(ProcessingTransaction.FORMATTED_ZERO);
        row2.setCardLimitUsedSum(ProcessingTransaction.FORMATTED_ZERO);
        row2.setRefundSum(ProcessingTransaction.FORMATTED_ZERO);
        rows.add(row2);
        rowExp2 = new VoucherResponseRow();
        BeanUtils.copyProperties(row2, rowExp2);
        rowExp2.setPrice(new BigDecimal(12l)); // на входе у нас 12 а не 120

        // строка будет добавлена (её не было в транзакции)
        rowExp3 = new VoucherResponseRow("rowName04", "00304",
                new BigDecimal(13l), 13, 4);
        rowExp3.setClientPackId("packId04");
        rowExp3.setClientPackName("packName04");
        rowExp3.setDiscountPercent(ProcessingTransaction.FORMATTED_ZERO);
        rowExp3.setDiscountSum(ProcessingTransaction.FORMATTED_ZERO);
        rowExp3.setPackDiscount(null);
        rowExp3.setSumToPay(ProcessingTransaction.FORMATTED_ZERO);
        rowExp3.setCardLimitUsedSum(ProcessingTransaction.FORMATTED_ZERO);
        rowExp3.setRefundSum(ProcessingTransaction.FORMATTED_ZERO);

        transaction.setRows(rows);

        List<VoucherResponseRow> newRows = service.compareAndFillRows(
                transaction, request);
        List<VoucherResponseRow> transactionResultRowList = new ArrayList<VoucherResponseRow>();
        transactionResultRowList.add(rowExp0);
        transactionResultRowList.add(rowExp1);
        transactionResultRowList.add(rowExp2);
        transactionResultRowList.add(rowExp3);

        assertEquals(transactionResultRowList.size(), transaction.getRows().size());
        int i = 0;
        assertEquals(transaction.getRows().size(), transactionResultRowList.size());
        for (VoucherResponseRow row : transaction.getRows()) {
            assertEquals(row.getClientPackId(), transactionResultRowList.get(i).getClientPackId());
            assertEquals(row.getClientPackName(), transactionResultRowList.get(i).getClientPackName());
            assertEquals(row.getQuantity(), transactionResultRowList.get(i).getQuantity());
            assertEquals(row.getPrice(), transactionResultRowList.get(i).getPrice());
            assertEquals(row.getDiscountPercent().intValue(), 0);
            assertEquals(row.getDiscountSum().intValue(), 0);
            assertNull(row.getPackDiscount());
            assertEquals(row.getSumToPay().intValue(), 0);
            assertEquals(row.getCardLimitUsedSum().intValue(), 0);
            assertEquals(row.getRefundSum().intValue(), 0);
            i++;
        }

        List<VoucherResponseRow> newRowsExpectedList = new ArrayList<VoucherResponseRow>();
        newRowsExpectedList.add(rowExp3);

        assertEquals(newRows.size(), newRowsExpectedList.size());
        i = 0;
        for (VoucherResponseRow row : newRows) {
            assertEquals(row.getClientPackId(), newRowsExpectedList.get(i)
                    .getClientPackId());
            assertEquals(row.getClientPackName(), newRowsExpectedList.get(i)
                    .getClientPackName());
            assertEquals(row.getQuantity(), newRowsExpectedList.get(i)
                    .getQuantity());
            assertEquals(row.getPrice(), newRowsExpectedList.get(i).getPrice());
            i++;
        }
    }

    @Test
    public void testGetEktArray() {
        Map<String, String> ektMap = new HashMap<String, String>();
        ektMap.put("key1", "val1");
        ektMap.put("key2", "val2");
        ektMap.put("key3", "val2");
        ektMap.put("key4", "val3");
        ektMap.put("key5", "val4");
        ektMap.put("key6", "val4");
        String[] ektArray = service.getEktArray(ektMap);

        assertEquals(4, ektArray.length);

        boolean hasDuplicates = false;
        int i = 0;
        for (String ekt : ektArray) {
            for (int j = i + 1; j < ektArray.length; j++)
                if (ektArray[j].equalsIgnoreCase(ekt)) {
                    hasDuplicates = true;
                    continue;
                }
            if (hasDuplicates)
                continue;
            else
                i++;
        }

        assertFalse(hasDuplicates);

    }

    @Test
    public void testGetPacks() throws JsonGenerationException,
            JsonMappingException, IOException, BatchGetException {
        List<VoucherResponseRow> rows = new ArrayList<VoucherResponseRow>();
        VoucherResponseRow row0, row1, row2, row3, row4;

        row0 = new VoucherResponseRow("rowName00", "000", new BigDecimal(10l),
                10, 5);
        row0.setClientPackId("clientPackId000");
        row0.setClientPackName("clientPackName000");
        row0.setDiscountPercent(ProcessingTransaction.FORMATTED_ZERO);
        row0.setDiscountSum(ProcessingTransaction.FORMATTED_ZERO);
        row0.setPackDiscount(null);
        row0.setSumToPay(ProcessingTransaction.FORMATTED_ZERO);
        row0.setCardLimitUsedSum(ProcessingTransaction.FORMATTED_ZERO);
        row0.setRefundSum(ProcessingTransaction.FORMATTED_ZERO);
        rows.add(row0);

        row1 = new VoucherResponseRow("rowName01", "001", new BigDecimal(10l),
                10, 1);
        row1.setClientPackId("clientPackId001");
        row1.setClientPackName("clientPackName001");
        row1.setDiscountPercent(ProcessingTransaction.FORMATTED_ZERO);
        row1.setDiscountSum(ProcessingTransaction.FORMATTED_ZERO);
        row1.setPackDiscount(null);
        row1.setSumToPay(ProcessingTransaction.FORMATTED_ZERO);
        row1.setCardLimitUsedSum(ProcessingTransaction.FORMATTED_ZERO);
        row1.setRefundSum(ProcessingTransaction.FORMATTED_ZERO);
        rows.add(row1);

        row2 = new VoucherResponseRow("rowName02", "002", new BigDecimal(20l),
                20, 2);
        row2.setClientPackId("clientPackId002");
        row2.setClientPackName("clientPackName002");
        row2.setDiscountPercent(ProcessingTransaction.FORMATTED_ZERO);
        row2.setDiscountSum(ProcessingTransaction.FORMATTED_ZERO);
        row2.setPackDiscount(null);
        row2.setSumToPay(ProcessingTransaction.FORMATTED_ZERO);
        row2.setCardLimitUsedSum(ProcessingTransaction.FORMATTED_ZERO);
        row2.setRefundSum(ProcessingTransaction.FORMATTED_ZERO);
        rows.add(row2);

        row3 = new VoucherResponseRow("rowName03", "003", new BigDecimal(30l),
                30, 3);
        row3.setClientPackId("clientPackId003");
        row3.setClientPackName("clientPackName003");
        row3.setDiscountPercent(ProcessingTransaction.FORMATTED_ZERO);
        row3.setDiscountSum(ProcessingTransaction.FORMATTED_ZERO);
        row3.setPackDiscount(null);
        row3.setSumToPay(ProcessingTransaction.FORMATTED_ZERO);
        row3.setCardLimitUsedSum(ProcessingTransaction.FORMATTED_ZERO);
        row3.setRefundSum(ProcessingTransaction.FORMATTED_ZERO);
        rows.add(row3);

        row4 = new VoucherResponseRow("rowName04", "004", new BigDecimal(30l),
                30, 4);
        row4.setClientPackId("clientPackId004");
        row4.setClientPackName("clientPackName004");
        row4.setDiscountPercent(ProcessingTransaction.FORMATTED_ZERO);
        row4.setDiscountSum(ProcessingTransaction.FORMATTED_ZERO);
        row4.setPackDiscount(null);
        row4.setSumToPay(ProcessingTransaction.FORMATTED_ZERO);
        row4.setCardLimitUsedSum(ProcessingTransaction.FORMATTED_ZERO);
        row4.setRefundSum(ProcessingTransaction.FORMATTED_ZERO);
        rows.add(row4);

        ProcessingTransaction transaction = new ProcessingTransaction(DateTime.now());
        transaction.setRows(rows);

        List<VoucherProduct> products = new ArrayList<VoucherProduct>();
        VoucherProduct product0 = new VoucherProduct("barcode000", "ekt000",
                "number000",
                BigDecimal.ONE, 10, 2014, "clientPackId000",
                "clientPackName000");
        VoucherProduct product1 = new VoucherProduct("barcode001", null,
                "number001",
                BigDecimal.ONE, 10, 2014, "clientPackId001",
                "clientPackName001");
        VoucherProduct product2 = new VoucherProduct("barcode002",
                "ektIncorrect", "number002",
                BigDecimal.ONE, 10, 2014, "clientPackId002",
                "clientPackName002");
        VoucherProduct product3 = new VoucherProduct("barcode003", null,
                "number003",
                BigDecimal.ONE, 10, 2014, "clientPackId003",
                "clientPackName003");
        VoucherProduct product4 = new VoucherProduct("barcode004", null,
                "number004",
                BigDecimal.ONE, 10, 2014, "clientPackId004",
                "clientPackName004");
        products.add(product0);
        products.add(product1);
        products.add(product2);
        products.add(product3);
        products.add(product4);

        String[] productIndexIdKeyList = new String[] {
                "drugstore000#clientPackId000",
                "drugstore000#clientPackId001",
                "drugstore000#clientPackId002",
                "drugstore000#clientPackId003",
                "drugstore000#clientPackId004"
        };

        String[] productIndexNameKeyList = new String[] {
                "drugstore000#clientPackName000",
                "drugstore000#clientPackName001",
                "drugstore000#clientPackName002",
                "drugstore000#clientPackName003",
                "drugstore000#clientPackName004"
        };

        VoucherRequest request = new VoucherRequest();
        request.setDrugstoreId("drugstore000");
        request.setProducts(products.toArray(new VoucherProduct[4]));

        String[] packIds = new String[] {"clientPackId000", "clientPackId001",
                "clientPackId002", "clientPackId003", "clientPackId004"};
        String[] rangeArray = new String[packIds.length];
        for (int i = 0; i < rangeArray.length; i++) {
            rangeArray[i] = DrugstorePackIndex.DRUGSTORE_PACK;
        }

        List<DrugstorePackIndex> indexesById = new ArrayList<DrugstorePackIndex>();
        DrugstorePackIndex index0 = new DrugstorePackIndex("drugstore000",
                "clientPackId000", "ekt000");
        DrugstorePackIndex index1 = new DrugstorePackIndex("drugstore000",
                "clientPackId001", "ekt001");
        DrugstorePackIndex index2 = new DrugstorePackIndex("drugstore000",
                "clientPackId002", "ektIncorrect");
        indexesById.add(index0);
        indexesById.add(index1);
        indexesById.add(index2);

        List<DrugstorePackIndex> indexesByName = new ArrayList<DrugstorePackIndex>();

        DrugstorePackIndex index3 = new DrugstorePackIndex("drugstore000",
                "clientPackName004", "ekt004");
        indexesByName.add(index3);

        when(dynamoService
                .getObjectsAllOrDie(eq(DrugstorePackIndex.class),
                        eq(productIndexIdKeyList),
                        eq(rangeArray))).thenReturn(indexesById);

        when(dynamoService
                .getObjectsAllOrDie(eq(DrugstorePackIndex.class),
                        eq(productIndexNameKeyList),
                        eq(rangeArray))).thenReturn(indexesByName);

        String[] ektArray = new String[] {"ekt000", "ektIncorrect", "ekt001", "ekt004",
                AppSettingsService.DEFAULT_PACK_EKT};
        List<Pack> packs = new ArrayList<Pack>();
        Pack pack0 = new Pack("ekt000", "ekt000", "000", "000", "pack000",
                "manufacturer000", "synonym000", "tradeName000", "inn000");
        Pack pack1 = new Pack("ekt001", "ekt001", "001", "001", "pack001",
                "manufacturer001", "synonym001", "tradeName001", "inn001");
        Pack packDefault = new Pack(AppSettingsService.DEFAULT_PACK_EKT,
                AppSettingsService.DEFAULT_PACK_EKT, "002", "002",
                "defaultPack",
                "manufacturerDefault", "synonymDefault", "tradeNameDefault",
                "innDefault");
        Pack pack3 = new Pack("ekt003", "ekt003", "003", "003", "pack003",
                "manufacturer003", "synonym003", "tradeName003", "inn003");
        Pack pack4 = new Pack("ekt004", "ekt004", "004", "004", "pack004",
                "manufacturer004", "synonym004", "tradeName004", "inn004");
        packs.add(pack0);
        packs.add(pack1);
        packs.add(pack3);
        packs.add(pack4);
        List<ComplexKey> incorrectPackKeys = new ArrayList<ComplexKey>();
        incorrectPackKeys.add(new ComplexKey("ektIncorrect", "ektIncorrect"));
        incorrectPackKeys.add(new ComplexKey(null, null));

        when(dynamoService.getObjects(
                settingsService.getMasterTablePackEntity(),
                ektArray,
                ektArray)).thenReturn(new BatchOperationResult<Pack>(packs, null));
        when(dynamoService.getObject(
                settingsService.getMasterTablePackEntity(),
                AppSettingsService.DEFAULT_PACK_EKT,
                AppSettingsService.DEFAULT_PACK_EKT)).thenReturn(packDefault);

        when(settingsService.getDefaultPackEkt()).thenReturn(
                AppSettingsService.DEFAULT_PACK_EKT);

        service.getPacks(rows, request, null);

        for (VoucherResponseRow row : rows) {
            assertNotNull(row.getBarcode());
            assertNotNull(row.getName());
            assertNotNull(row.getId());
            assertNotNull(row.getInn());
            assertNotNull(row.getManufacturer());
        }

        assertEquals(rows.get(0).getId(), "ekt000");
        assertEquals(rows.get(1).getId(), "ekt001");
        assertEquals(rows.get(2).getId(), AppSettingsService.DEFAULT_PACK_EKT);
        assertEquals(rows.get(3).getId(), AppSettingsService.DEFAULT_PACK_EKT);
        assertEquals(rows.get(4).getId(), "ekt004");
    }

    @Test
    public void testCalculateDiscounts()
            throws JsonGenerationException, JsonMappingException, IOException, RuntimeException, InterruptedException,
            DataIntegrityException {
        List<VoucherResponseRow> rows = new ArrayList<VoucherResponseRow>();
        VoucherResponseRow row0, row1, row2, row3, row4;

        List<DiscountLimit> limits = new ArrayList<DiscountLimit>();
        limits.add(new DiscountLimit("discount000", "packLimitGroup000", 0,
                "year", 1000, 1000l, new BigDecimal(1000), new BigDecimal(3000)));
        limits.add(new DiscountLimit("discount000", "*", 0, "year", 1200,
                1200l, new BigDecimal(1200), new BigDecimal(3000)));
        limits.add(new DiscountLimit("discount001", "packLimitGroup001", 0,
                "halfyear", 1000, 1000l, new BigDecimal(700), new BigDecimal(
                        2000)));
        limits.add(new DiscountLimit("discount000", "packLimitGroup300", 0,
                "halfyear", 70, 70l, new BigDecimal(100), new BigDecimal(100)));

        // row0: частичное суммирование баланса
        row0 = new VoucherResponseRow("rowName00", "000", new BigDecimal(10l),
                10, 1);
        row0.setClientPackId("clientPackId000");
        row0.setClientPackName("clientPackName000");
        row0.setId("ekt000");
        row0.setDiscountPercent(ProcessingTransaction.FORMATTED_ZERO);
        row0.setDiscountSum(ProcessingTransaction.FORMATTED_ZERO);
        row0.setPackDiscount(null);
        row0.setSumToPay(ProcessingTransaction.FORMATTED_ZERO);
        row0.setCardLimitUsedSum(ProcessingTransaction.FORMATTED_ZERO);
        row0.setRefundSum(ProcessingTransaction.FORMATTED_ZERO);
        row0.setPrice(new BigDecimal(100));
        row0.setQuantity(1);

        PackDiscountData data00 = new PackDiscountData();
        data00.setDiscountId("discount000");
        data00.setOriginalPercent(new BigDecimal(30));
        data00.setOriginalSum(new BigDecimal(100));

        PackDiscountData data01 = new PackDiscountData();
        data01.setDiscountId("discount000");
        data01.setOriginalPercent(new BigDecimal(15));
        data01.setOriginalSum(new BigDecimal(100));

        row0.setPackDiscount(new PackDiscountData[] {data00, data01});
        rows.add(row0);

        // row1: Расчет использованого баланса карты
        row1 = new VoucherResponseRow("rowName01", "001", new BigDecimal(10l),
                10, 1);
        row1.setClientPackId("clientPackId001");
        row1.setClientPackName("clientPackName001");
        row1.setId("ekt001");
        row1.setDiscountPercent(ProcessingTransaction.FORMATTED_ZERO);
        row1.setDiscountSum(ProcessingTransaction.FORMATTED_ZERO);
        row1.setPackDiscount(null);
        row1.setSumToPay(ProcessingTransaction.FORMATTED_ZERO);
        row1.setCardLimitUsedSum(ProcessingTransaction.FORMATTED_ZERO);
        row1.setRefundSum(ProcessingTransaction.FORMATTED_ZERO);
        row1.setPrice(new BigDecimal(100));
        row1.setQuantity(1);

        PackDiscountData data10 = new PackDiscountData();
        data10.setDiscountId("discount001");
        data10.setOriginalPercent(new BigDecimal(31));
        data10.setOriginalSum(new BigDecimal(100));

        PackDiscountData data11 = new PackDiscountData();
        data11.setDiscountId("discount001");
        data11.setOriginalPercent(new BigDecimal(30));
        data11.setOriginalSum(new BigDecimal(100));

        row1.setPackDiscount(new PackDiscountData[] {data10, data11});
        rows.add(row1);

        // row2: использование лимитов
        row2 = new VoucherResponseRow("rowName02", "002", new BigDecimal(20l),
                20, 2);
        row2.setClientPackId("clientPackId002");
        row2.setClientPackName("clientPackName002");
        row2.setId("ekt002");
        row2.setDiscountPercent(ProcessingTransaction.FORMATTED_ZERO);
        row2.setDiscountSum(ProcessingTransaction.FORMATTED_ZERO);
        row2.setPackDiscount(null);
        row2.setSumToPay(ProcessingTransaction.FORMATTED_ZERO);
        row2.setCardLimitUsedSum(ProcessingTransaction.FORMATTED_ZERO);
        row2.setRefundSum(ProcessingTransaction.FORMATTED_ZERO);
        row2.setPrice(new BigDecimal(100));
        row2.setQuantity(1);

        PackDiscountData data20 = new PackDiscountData(
                "discount000", new BigDecimal(10), new BigDecimal(100),
                new BigDecimal(10),
                BigDecimal.ZERO, BigDecimal.ZERO);

        PackDiscountLimitData limitData200 = new PackDiscountLimitData();
        limitData200.setLimitGroupId("packLimitGroup000");
        limitData200.setLimitGroupWeight(10);

        PackDiscountLimitData limitData201 = new PackDiscountLimitData();
        limitData201.setLimitGroupId("*");
        limitData201.setLimitGroupWeight(20);

        data20.setPackDiscountLimitData(new PackDiscountLimitData[] {
                limitData200, limitData201});

        row2.setPackDiscount(new PackDiscountData[] {data20});

        DiscountLimitUsedTotal limitUsedTotal20 = new DiscountLimitUsedTotal();
        limitUsedTotal20.setDiscountId("discount000");
        limitUsedTotal20.setCardNumber("0000000000000");
        limitUsedTotal20.setRow(0);
        limitUsedTotal20.setPeriod("201310");
        limitUsedTotal20.setLimitGroupId("packLimitGroup000");
        limitUsedTotal20.setDiscountSum(new BigDecimal(30));
        limitUsedTotal20.setSumBeforeDiscount(new BigDecimal(80));
        limitUsedTotal20.setAveragePackCount(250l);
        limitUsedTotal20.setPackCount(65);

        DiscountLimitUsedTotal limitUsedTotal21 = new DiscountLimitUsedTotal();
        limitUsedTotal21.setDiscountId("discount000");
        limitUsedTotal21.setCardNumber("0000000000000");
        limitUsedTotal21.setRow(0);
        limitUsedTotal21.setPeriod("201310");
        limitUsedTotal21.setLimitGroupId("*");
        limitUsedTotal21.setDiscountSum(new BigDecimal(10));
        limitUsedTotal21.setSumBeforeDiscount(new BigDecimal(10));
        limitUsedTotal21.setAveragePackCount(10l);
        limitUsedTotal21.setPackCount(10);

        List<DiscountLimitUsedTotal> limitUsedList = new ArrayList<DiscountLimitUsedTotal>();
        limitUsedList.add(limitUsedTotal20);
        limitUsedList.add(limitUsedTotal21);

        rows.add(row2);

        // row3: переполнение лимитов

        row3 = new VoucherResponseRow("rowName03", "003", new BigDecimal(20l),
                20, 3);
        row3.setClientPackId("clientPackId002");
        row3.setClientPackName("clientPackName002");
        row3.setId("ekt002");
        row3.setDiscountPercent(ProcessingTransaction.FORMATTED_ZERO);
        row3.setDiscountSum(ProcessingTransaction.FORMATTED_ZERO);
        row3.setPackDiscount(null);
        row3.setSumToPay(ProcessingTransaction.FORMATTED_ZERO);
        row3.setCardLimitUsedSum(ProcessingTransaction.FORMATTED_ZERO);
        row3.setRefundSum(ProcessingTransaction.FORMATTED_ZERO);
        row3.setPrice(new BigDecimal(100));
        row3.setQuantity(1);

        DiscountLimitUsedTotal limitUsedTotal30 = new DiscountLimitUsedTotal();
        limitUsedTotal30.setDiscountId("discount000");
        limitUsedTotal30.setCardNumber("0000000000000");
        limitUsedTotal30.setRow(0);
        limitUsedTotal30.setPeriod("201310");
        limitUsedTotal30.setLimitGroupId("packLimitGroup300");
        limitUsedTotal30.setDiscountSum(new BigDecimal(30));
        limitUsedTotal30.setSumBeforeDiscount(new BigDecimal(80));
        limitUsedTotal30.setAveragePackCount(1000l);
        limitUsedTotal30.setPackCount(65);
        limitUsedList.add(limitUsedTotal30);

        DiscountLimitUsedTotal limitUsedTotal31 = new DiscountLimitUsedTotal();
        limitUsedTotal31.setDiscountId("discount000");
        limitUsedTotal31.setCardNumber("0000000000000");
        limitUsedTotal31.setRow(0);
        limitUsedTotal31.setPeriod("201310");
        limitUsedTotal31.setLimitGroupId("*");
        limitUsedTotal31.setDiscountSum(new BigDecimal(30));
        limitUsedTotal31.setSumBeforeDiscount(new BigDecimal(80));
        limitUsedTotal31.setAveragePackCount(250l);
        limitUsedTotal31.setPackCount(65);
        limitUsedList.add(limitUsedTotal31);

        PackDiscountData data30 = new PackDiscountData(
                "discount000", new BigDecimal(10), new BigDecimal(100),
                new BigDecimal(10),
                BigDecimal.ZERO, BigDecimal.ZERO);

        PackDiscountLimitData limitData300 = new PackDiscountLimitData();
        limitData300.setLimitGroupId("packLimitGroup300");
        limitData300.setLimitGroupWeight(10);

        PackDiscountLimitData limitData301 = new PackDiscountLimitData();
        limitData301.setLimitGroupId("*");
        limitData301.setLimitGroupWeight(10);

        data30.setPackDiscountLimitData(new PackDiscountLimitData[] {
                limitData300, limitData301});

        row3.setPackDiscount(new PackDiscountData[] {data30});

        rows.add(row3);

        // row4: списание баланса
        row4 = new VoucherResponseRow("rowName04", "004", new BigDecimal(10l),
                10, 4);
        row4.setClientPackId("clientPackId001");
        row4.setClientPackName("clientPackName001");
        row4.setId("ekt004");
        row4.setDiscountPercent(ProcessingTransaction.FORMATTED_ZERO);
        row4.setDiscountSum(ProcessingTransaction.FORMATTED_ZERO);
        row4.setPackDiscount(null);
        row4.setSumToPay(ProcessingTransaction.FORMATTED_ZERO);
        row4.setCardLimitUsedSum(ProcessingTransaction.FORMATTED_ZERO);
        row4.setRefundSum(ProcessingTransaction.FORMATTED_ZERO);
        row4.setPrice(new BigDecimal(10));
        row4.setQuantity(1);

        PackDiscountData data40 = new PackDiscountData();
        data40.setDiscountId("discount001");
        data40.setOriginalPercent(new BigDecimal(30));
        data40.setOriginalSum(new BigDecimal(100));

        PackDiscountData data41 = new PackDiscountData();
        data41.setDiscountId("discount001");
        data41.setOriginalPercent(new BigDecimal(30));
        data41.setOriginalSum(new BigDecimal(10));

        row4.setPackDiscount(new PackDiscountData[] {data40, data41});
        rows.add(row4);

        // transaction

        ProcessingTransaction transaction = new ProcessingTransaction(DateTime.now());
        transaction.setRows(rows);
        transaction.setDiscounts(Lists.newArrayList(new String[] {
                "discount000", "discount001"}));
        transaction.setDiscountPlanId("plan000");
        transaction.setDrugstoreNonRefundPercent(11);
        transaction.setMaxDiscountPercent(40);
        when(discountService.getDiscount("discount000"))
                .thenReturn(
                        new Discount("discount000", "discount000", false,
                                false, 40, 0, 100));
        when(discountService.getDiscount("discount001"))
                .thenReturn(
                        new Discount("discount001", "discount001", true, false,
                                40, 0, 25));
        transaction.setCard(new VoucherResponseCard("0000000000000",
                "cardOwner", "000", true, "comment", "plan000", "plan000"));
        transaction.setDrugstoreId("drugstore000");
        transaction.setDiscountLimits(limits);
        // transaction.setDiscountLimitUsed(limitUsedList);
        transaction.setDiscountLimitUsedPreviousTotal(limitUsedList);
        Drugstore drugstore = new Drugstore("drugstore000");
        drugstore.setNonRefundPercent(5);
        when(dynamoService.getObject(Drugstore.class, "drugstore000"))
                .thenReturn(drugstore);

        BigDecimal cardLimit = new BigDecimal(30);

        service.calculateDiscounts(transaction, cardLimit);

        // Проверяем частичное суммирование скидок
        VoucherResponseRow row = transaction.getRows().get(0);
        assertEquals(new BigDecimal(40), row.getDiscountPercent());
        assertEquals(new BigDecimal(30),
                row.getPackDiscount()[0].getUsedPercent());
        assertEquals(row.getDiscountSum(), row.getPackDiscount()[0]
                .getUsedSum()
                .add(row.getPackDiscount()[1].getUsedSum()));
        assertEquals(row.getDiscountPercent(), row.getPackDiscount()[0]
                .getUsedPercent()
                .add(row.getPackDiscount()[1].getUsedPercent()));
        assertEquals(new BigDecimal(10),
                row.getPackDiscount()[1].getUsedPercent());

        // Проверяем дефицит баланса
        // assertEquals(0, transaction.getRows().get(1).getCardLimitUsedSum()
        // .intValue());
        // assertEquals(0, transaction.getRows().get(1).getDiscountSum()
        // .intValue()); // OLP-652
        // assertEquals(4, transaction.getCardLimitUsedSum().intValue());
        // assertEquals(10, transaction.getCardLimitDeficit().intValue());

        // Проверяем списание лимитов
        /*
         * Collection<DiscountLimitUsedRecord> detailLimits =
         * Collections2.filter( transaction.getDiscountLimitUsed(), new
         * Predicate<DiscountLimitUsedRecord>() {
         * 
         * @Override public boolean apply(DiscountLimitUsedRecord limit) {
         * return limit.getType().equals(DiscountLimitUsed.TYPE_DETAIL); } });
         */
        assertEquals(2, transaction.getDiscountLimitUsedCurrentDetail().size());
        DiscountLimitUsedDetail usedLimit = ((DiscountLimitUsedDetail) transaction.getDiscountLimitUsedCurrentDetail()
                .toArray()[0]);

        assertEquals(usedLimit.getSumBeforeDiscount(),
                (transaction.getRows().get(usedLimit.getRow() - 1).getSumWithoutDiscount()));

        // Проверяем достижение лимитов
        assertEquals(0, transaction.getRows().get(3).getDiscountPercent()
                .intValue());
        assertEquals(0, transaction.getRows().get(3).getDiscountSum()
                .intValue());
        assertEquals(1, transaction.getRows().get(3)
                .getDiscountLimitReachedWarnings().length);
        assertEquals(0, transaction.getRows().get(3).getRefundSum().intValue());

        // проверка разрядностей
        assertEquals(2, transaction.getCardLimitDeficit().scale());
        assertEquals(2, transaction.getCardLimitUsedSum().scale());
        assertEquals(2, transaction.getDiscountSum().scale());
        assertEquals(2, transaction.getSumToPay().scale());
        assertEquals(2, transaction.getSumWithoutDiscount().scale());
    }

    @Test
    public void sendRowWarningsToSmsTest()
            throws JsonGenerationException, JsonMappingException, IOException {
        ProcessingTransaction transaction = new ProcessingTransaction(DateTime.now().withZone(DateTimeZone.UTC));
        transaction.setRows(new ArrayList<VoucherResponseRow>());
        VoucherResponseRow row0 = new VoucherResponseRow();
        String[] warnings = new String[] {"warning0", "warning1"};
        VoucherResponseCard card = new VoucherResponseCard("000", "owner", "phoneNumber", true, "", "", "planId");
        transaction.setCard(card);
        row0.setDiscountLimitReachedWarnings(warnings);
        transaction.getRows().add(row0);
        warnings = new String[] {"warning1", "warning2"};
        VoucherResponseRow row1 = new VoucherResponseRow();
        row1.setDiscountLimitReachedWarnings(warnings);
        transaction.getRows().add(row1);
        service.sendRowWarningsToSms(transaction);
        verify(operationService).sendSmsNotification(card.getPhone(), "warning0");
        verify(operationService).sendSmsNotification(card.getPhone(), "warning1");
        verify(operationService).sendSmsNotification(card.getPhone(), "warning2");
    }

    @Test
    public void checkExactFieldsTestOk() throws JsonGenerationException, JsonMappingException, IOException {
        ProcessingTransaction transaction = new ProcessingTransaction(DateTime.now());
        transaction.setRows(new ArrayList<VoucherResponseRow>());
        VoucherResponseRow row0 = new VoucherResponseRow();
        row0.setExactSumToPay(new BigDecimal("19.79"));
        row0.setExactRefundSum(new BigDecimal("9.25"));
        row0.setExactNonRefundSum(new BigDecimal("3.96"));
        row0.setDiscountSum(new BigDecimal("13.20"));
        row0.setSumToPay(new BigDecimal("19.80"));
        row0.setSumWithoutDiscount(new BigDecimal("33.00"));
        row0.setRefundSum(new BigDecimal("9.24"));

        row0 = spy(row0);

        transaction.getRows().add(row0);

        service.checkExactFields(transaction);

        verify(row0, never()).setExactNonRefundSum(any(BigDecimal.class));
    }

    @Test
    public void checkExactFieldsTestNonRefundSumGreater() throws JsonGenerationException, JsonMappingException,
            IOException {
        ProcessingTransaction transaction = new ProcessingTransaction(DateTime.now());
        transaction.setRows(new ArrayList<VoucherResponseRow>());
        VoucherResponseRow row0 = new VoucherResponseRow();
        row0.setExactSumToPay(new BigDecimal("19.79"));
        row0.setExactRefundSum(new BigDecimal("9.25"));
        row0.setExactNonRefundSum(new BigDecimal("4.96"));
        row0.setDiscountSum(new BigDecimal("13.20"));
        row0.setSumToPay(new BigDecimal("19.80"));
        row0.setSumWithoutDiscount(new BigDecimal("33.00"));
        row0.setRefundSum(new BigDecimal("9.24"));

        row0 = spy(row0);

        transaction.getRows().add(row0);

        service.checkExactFields(transaction);

        verify(row0).setExactNonRefundSum(any(BigDecimal.class));
    }

    @Test
    public void checkExactFieldsTestSumToPayGreater() throws JsonGenerationException, JsonMappingException, IOException {
        ProcessingTransaction transaction = new ProcessingTransaction(DateTime.now());
        transaction.setRows(new ArrayList<VoucherResponseRow>());
        VoucherResponseRow row0 = new VoucherResponseRow();
        row0.setExactSumToPay(new BigDecimal("21.79"));
        row0.setExactRefundSum(new BigDecimal("9.25"));
        row0.setExactNonRefundSum(new BigDecimal("3.96"));
        row0.setDiscountSum(new BigDecimal("13.20"));
        row0.setSumToPay(new BigDecimal("19.80"));
        row0.setSumWithoutDiscount(new BigDecimal("33.00"));
        row0.setRefundSum(new BigDecimal("9.24"));

        row0 = spy(row0);

        transaction.getRows().add(row0);

        service.checkExactFields(transaction);

        verify(row0).setExactNonRefundSum(any(BigDecimal.class));
    }

    @Test
    public void checkExactFieldsTestBigSumToPay() throws JsonGenerationException, JsonMappingException, IOException {
        ProcessingTransaction transaction = new ProcessingTransaction(DateTime.now());
        transaction.setRows(new ArrayList<VoucherResponseRow>());
        VoucherResponseRow row0 = new VoucherResponseRow();
        row0.setExactSumToPay(new BigDecimal("19.79"));
        row0.setExactRefundSum(new BigDecimal("9.25"));
        row0.setExactNonRefundSum(new BigDecimal("3.96"));
        row0.setDiscountSum(new BigDecimal("13.20"));
        row0.setSumToPay(new BigDecimal("190.80"));
        row0.setSumWithoutDiscount(new BigDecimal("33.00"));
        row0.setRefundSum(new BigDecimal("9.24"));

        row0 = spy(row0);

        transaction.getRows().add(row0);

        service.checkExactFields(transaction);

        verify(row0).setExactNonRefundSum(any(BigDecimal.class));
    }

    @Test
    public void checkExactFieldsTestBigRefundSum() throws JsonGenerationException, JsonMappingException, IOException {
        ProcessingTransaction transaction = new ProcessingTransaction(DateTime.now());
        transaction.setRows(new ArrayList<VoucherResponseRow>());
        VoucherResponseRow row0 = new VoucherResponseRow();
        row0.setExactSumToPay(new BigDecimal("19.79"));
        row0.setExactRefundSum(new BigDecimal("9.25"));
        row0.setExactNonRefundSum(new BigDecimal("4.96"));
        row0.setDiscountSum(new BigDecimal("13.20"));
        row0.setSumToPay(new BigDecimal("19.80"));
        row0.setSumWithoutDiscount(new BigDecimal("33.00"));
        row0.setRefundSum(new BigDecimal("9.24"));

        transaction.getRows().add(row0);

        service.checkExactFields(transaction);

        assertEquals(new BigDecimal("3.96"), row0.getExactNonRefundSum());
        assertEquals(new BigDecimal("9.24"), row0.getExactRefundSum());
        assertEquals(new BigDecimal("19.80"), row0.getExactSumToPay());
    }

    @Test
    public void checkExactFieldsExactValuesTest() throws JsonGenerationException, JsonMappingException, IOException {
        ProcessingTransaction transaction = new ProcessingTransaction(DateTime.now());
        transaction.setRows(new ArrayList<VoucherResponseRow>());
        VoucherResponseRow row0 = new VoucherResponseRow();
        row0.setExactSumToPay(new BigDecimal("19.79"));
        row0.setExactRefundSum(new BigDecimal("90.25"));
        row0.setExactNonRefundSum(new BigDecimal("3.96"));
        row0.setDiscountSum(new BigDecimal("13.20"));
        row0.setSumToPay(new BigDecimal("19.80"));
        row0.setSumWithoutDiscount(new BigDecimal("33.00"));
        row0.setRefundSum(new BigDecimal("9.24"));

        row0 = spy(row0);

        transaction.getRows().add(row0);

        service.checkExactFields(transaction);

        verify(row0).setExactNonRefundSum(any(BigDecimal.class));
    }

}
