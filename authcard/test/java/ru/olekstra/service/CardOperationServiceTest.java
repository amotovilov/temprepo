package ru.olekstra.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.common.service.CardLogService;
import ru.olekstra.common.service.DateTimeService;
import ru.olekstra.common.service.SmsService;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.CardLog;
import ru.olekstra.exception.NotFoundException;

@RunWith(MockitoJUnitRunner.class)
public class CardOperationServiceTest {

    private CardOperationService service;
    private SmsService smsService;
    private DynamodbService dbService;
    private CardService cardService;
    private MessageSource messageSource;
    private CardLogService cardLogService;
    private DateTimeService dateService;

    @Before
    public void setup() {
        smsService = mock(SmsService.class);
        dbService = mock(DynamodbService.class);
        cardService = mock(CardService.class);
        messageSource = mock(MessageSource.class);
        cardLogService = mock(CardLogService.class);
        dateService = mock(DateTimeService.class);
        when(dateService.createDateTimeWithZone(anyString())).thenReturn(DateTime.now());
        when(dateService.createDefaultDateTime()).thenReturn(DateTime.now());

        service = new CardOperationService(smsService, dbService,
                cardService, messageSource, cardLogService, dateService);
    }

    @Test
    public void testSaveLimit() throws JsonGenerationException,
            JsonMappingException, IOException, NotFoundException,
            ItemSizeLimitExceededException {

        String cardNumber = "number";
        String drugstoreId = "dragstoreId";
        String phone = "799911112233";
        BigDecimal limit = new BigDecimal(20).setScale(2, RoundingMode.HALF_UP);
        BigDecimal discountSum = new BigDecimal(15);
        String timeZone = "Europe/Moscow";

        Card card = new Card(cardNumber);
        card.setLimitRemain(limit);
        card.setTelephoneNumber(phone);

        when(messageSource.getMessage("msg.cardlimitnote", new Object[] {discountSum}, null))
                .thenReturn("limit change: " + discountSum);
        when(cardService.updateCardLimit(eq(cardNumber), (BigDecimal) anyObject(), (BigDecimal) anyObject()))
                .thenReturn(true);

        BigDecimal newLimit = service.saveLimit(drugstoreId, card, discountSum);

        assertTrue(newLimit.equals(limit.subtract(discountSum)));

        verify(messageSource).getMessage("msg.cardlimitnote", new Object[] {discountSum.toString()}, null);

        verify(cardLogService).saveCardLog(any(CardLog.class));
    }

    @Test
    public void testSaveLimitWithOverLimits() throws JsonGenerationException,
            JsonMappingException, IOException, NotFoundException,
            ItemSizeLimitExceededException {

        String cardNumber = "number";
        String drugstoreId = "dragstoreId";
        String phone = "799911112233";
        BigDecimal limit = new BigDecimal(15).setScale(2, RoundingMode.HALF_UP);
        BigDecimal discountSum = new BigDecimal(20).setScale(2, RoundingMode.HALF_UP);
        String timeZone = "Europe/Moscow";

        Card card = new Card(cardNumber);
        card.setLimitRemain(limit);
        card.setTelephoneNumber(phone);

        when(
                messageSource.getMessage("msg.cardlimitnote",
                        new Object[] {discountSum}, null))
                .thenReturn("limit change: " + discountSum);
        when(
                cardService.updateCardLimit(eq(cardNumber),
                        (BigDecimal) anyObject(), (BigDecimal) anyObject()))
                .thenReturn(true);

        BigDecimal newLimit = service.saveLimit(drugstoreId, card, discountSum);

        assertTrue(newLimit.equals(limit.subtract(discountSum)));

        verify(messageSource).getMessage("msg.cardlimitnote", new Object[] {discountSum.toString()}, null);

        verify(cardLogService).saveCardLog(any(CardLog.class));
    }

    @Test
    public void testSaveLimitWithRetry() throws JsonGenerationException,
            JsonMappingException, IOException, NotFoundException,
            ItemSizeLimitExceededException {

        String cardNumber = "number";
        String drugstoreId = "dragstoreId";
        String phone = "799911112233";
        BigDecimal limit = new BigDecimal(20);
        BigDecimal discountSum = new BigDecimal(15);
        String timeZone = "Europe/Moscow";

        Card card = new Card(cardNumber);
        card.setLimitRemain(limit);
        card.setTelephoneNumber(phone);

        when(dbService.getObject(Card.class, cardNumber)).thenReturn(card);
        when(messageSource.getMessage("msg.cardlimitnote", new Object[] {discountSum}, null))
                .thenReturn("limit change: " + discountSum);
        when(cardService.updateCardLimit(eq(cardNumber), (BigDecimal) anyObject(), (BigDecimal) anyObject()))
                .thenReturn(false);

        BigDecimal newLimit = service.saveLimit(drugstoreId, card, discountSum);

        assertTrue(newLimit.equals(limit.subtract(discountSum)));

        verify(dbService).getObject(Card.class, cardNumber);

        verify(messageSource).getMessage("msg.cardlimitnote", new Object[] {discountSum.toString()}, null);

        verify(cardLogService).saveCardLog(any(CardLog.class));
    }

    @Test
    public void testSaveLimitWithOverLimitsWithRetry()
            throws JsonGenerationException,
            JsonMappingException, IOException, NotFoundException,
            ItemSizeLimitExceededException {

        String cardNumber = "number";
        String drugstoreId = "dragstoreId";
        String phone = "799911112233";
        BigDecimal limit = new BigDecimal(15);
        BigDecimal discountSum = new BigDecimal(20);
        String timeZone = "Europe/Moscow";

        Card card = new Card(cardNumber);
        card.setLimitRemain(limit);
        card.setTelephoneNumber(phone);

        when(dbService.getObject(Card.class, cardNumber)).thenReturn(card);
        when(messageSource.getMessage("msg.cardlimitnote", new Object[] {discountSum}, null))
                .thenReturn("limit change: " + discountSum);
        when(cardService.updateCardLimit(eq(cardNumber), (BigDecimal) anyObject(), (BigDecimal) anyObject()))
                .thenReturn(false);

        BigDecimal newLimit = service.saveLimit(drugstoreId, card, discountSum);

        assertTrue(newLimit.equals(limit.subtract(discountSum)));

        verify(dbService).getObject(Card.class, cardNumber);

        verify(messageSource).getMessage("msg.cardlimitnote", new Object[] {discountSum.toString()}, null);

        verify(cardLogService).saveCardLog(any(CardLog.class));
    }

    @Test
    public void testSendSmsNotification() throws ClientProtocolException,
            IOException {

        String phone = "799911112233";
        String notification = "test notitfcation";

        when(smsService.send(phone, notification)).thenReturn(false);

        boolean result = service.sendSmsNotification(phone,
                notification);

        assertFalse(result);

        when(smsService.send(phone, notification)).thenReturn(true);

        result = service.sendSmsNotification(phone,
                "test notitfcation");

        assertTrue(result);
    }

    @Test
    public void testSendSmsNotifications() throws ClientProtocolException,
            IOException {

        String phoneNumber = "799911112233";
        List<String> notifications = new ArrayList<String>();

        notifications.add("notification 1");
        notifications.add("notification 2");

        boolean result = service.sendSmsNotifications("", notifications);

        assertFalse(result);

        result = service.sendSmsNotifications(phoneNumber, notifications);

        assertTrue(result);
    }

}
