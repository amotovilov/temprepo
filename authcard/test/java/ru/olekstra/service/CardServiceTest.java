package ru.olekstra.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.domain.Card;
import ru.olekstra.exception.NotFoundException;

@RunWith(MockitoJUnitRunner.class)
public class CardServiceTest {

    private DynamodbService dynamodb;
    private CardService service;

    @Before
    public void setup() {
        dynamodb = mock(DynamodbService.class);
        service = new CardService(dynamodb);
    }

    @Test
    public void testGetCard() throws NotFoundException {

        String number = "number";
        Card card = new Card(number);
        when(dynamodb.getObject(Card.class, number)).thenReturn(card);

        Card actual = service.getCard(number);

        verify(dynamodb).getObject(Card.class, number);
        assertEquals(number, actual.getNumber());
    }
}
