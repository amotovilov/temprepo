package ru.olekstra.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.UUID;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import ru.olekstra.authcard.dto.ConfirmRequest;
import ru.olekstra.authcard.dto.ConfirmResponse;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.awsutils.exception.NotUpdatedException;
import ru.olekstra.awsutils.exception.OlekstraException;
import ru.olekstra.azure.model.MessageSendException;
import ru.olekstra.azure.service.QueueSender;
import ru.olekstra.common.service.CardLogService;
import ru.olekstra.common.service.DateTimeService;
import ru.olekstra.common.service.DiscountService;
import ru.olekstra.common.service.XmlMessageService;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.dto.VoucherProduct;
import ru.olekstra.domain.dto.VoucherRequest;
import ru.olekstra.exception.DataIntegrityException;
import ru.olekstra.exception.NotFoundException;
import ru.olekstra.exception.UnconsistentCheckDataException;
import ru.olekstra.processing.Processing;
import ru.olekstra.processing.RefundProcessing;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.util.json.JSONException;

@RunWith(MockitoJUnitRunner.class)
public class RefundServiceTest {

    private ConfirmRequest request;
    private VoucherRequest voucherRequest;
    private ProcessingTransaction transaction;

    private ConfirmRequest confirmRequest;

    private String drigstoreId;
    private UUID voucherId;
    private UUID confirmationToken;

    private Processing refundProcessing;

    private VoucherService voucherService;
    private CardLogService cardLogService;
    private XmlMessageService xmlMessageService;
    private CardService cardService;
    private DiscountService discountService;
    private Mapper dozerBeanMapper;
    private DateTimeService dateService;

    private RefundService refundService;
    private QueueSender sender;

    @Before
    public void setup() throws JsonGenerationException, JsonMappingException, IOException {

        drigstoreId = "did";
        voucherId = UUID.randomUUID();
        confirmationToken = UUID.randomUUID();

        confirmRequest = new ConfirmRequest(drigstoreId, voucherId, confirmationToken);

        voucherRequest = new VoucherRequest(confirmRequest.getVoucherId(), confirmRequest.getDrugstoreId(),
                "", new VoucherProduct[] {}, BigDecimal.ZERO);

        transaction = new ProcessingTransaction(DateTime.now());
        transaction.setVoucherId(voucherId);
        transaction.setConfirmationToken(confirmationToken);

        voucherService = mock(VoucherService.class);
        cardLogService = mock(CardLogService.class);
        xmlMessageService = mock(XmlMessageService.class);
        cardService = mock(CardService.class);
        discountService = mock(DiscountService.class);
        dozerBeanMapper = mock(Mapper.class);
        dateService = mock(DateTimeService.class);
        sender =  mock(QueueSender.class);

        refundService = spy(new RefundService(voucherService, cardLogService, xmlMessageService, cardService,
                discountService, dozerBeanMapper, dateService, sender));

        refundProcessing = mock(RefundProcessing.class);

    }

    @Test
    public void testGetConfirmResponseSuccess() throws JsonGenerationException, JsonMappingException,
            AmazonServiceException,
            IllegalArgumentException, NotUpdatedException, IOException, InterruptedException,
            ItemSizeLimitExceededException, NotFoundException, OlekstraException, InstantiationException,
            IllegalAccessException, RuntimeException, DataIntegrityException, UnconsistentCheckDataException,
            JSONException, MessageSendException, ParserConfigurationException, TransformerException {

        ProcessingTransaction refundedTransaction = new ProcessingTransaction(DateTime.now());
        refundedTransaction.setVoucherId(voucherId);
        refundedTransaction.setAuthCode("1234");

        when(refundService.newRefundProcessing(any(VoucherService.class), any(ProcessingTransaction.class),
                any(CardLogService.class), any(XmlMessageService.class), any(DiscountService.class),
                any(CardService.class),
                any(DozerBeanMapper.class), any(DateTimeService.class), any(QueueSender.class)))
                .thenReturn(refundProcessing);
        when(refundProcessing.doProcessing(any(VoucherRequest.class)))
                .thenReturn(refundedTransaction);

        ConfirmResponse confirmResponse = refundService.getConfirmResponse(confirmRequest, voucherRequest, transaction);

        assertEquals("1234", confirmResponse.getAuthorizationCode());
        assertEquals(transaction.getVoucherId().toString(), confirmResponse.getVoucherId().toString());

        verify(refundService, times(1))
                .newRefundProcessing(any(VoucherService.class), any(ProcessingTransaction.class),
                        any(CardLogService.class), any(XmlMessageService.class), any(DiscountService.class),
                        any(CardService.class), any(DozerBeanMapper.class), any(DateTimeService.class), any(QueueSender.class));

    }

    @Test(expected = UnconsistentCheckDataException.class)
    public void testGetConfirmResponseMismatch() throws JsonGenerationException, JsonMappingException,
            AmazonServiceException, IllegalArgumentException, NotUpdatedException,
            IOException, InterruptedException, ItemSizeLimitExceededException, NotFoundException, OlekstraException,
            InstantiationException, IllegalAccessException, RuntimeException, DataIntegrityException, JSONException,
            UnconsistentCheckDataException, MessageSendException, ParserConfigurationException, TransformerException {

        transaction.setConfirmationToken(UUID.randomUUID());

        refundService.getConfirmResponse(confirmRequest, voucherRequest, transaction);

    }

}
