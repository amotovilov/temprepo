package ru.olekstra.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.mutable.MutableObject;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.common.service.CardLogService;
import ru.olekstra.common.service.DateTimeService;
import ru.olekstra.common.service.DiscountService;
import ru.olekstra.common.service.LimitPeriod;
import ru.olekstra.domain.DiscountLimit;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.dto.DiscountLimitUsedDetail;
import ru.olekstra.domain.dto.DiscountLimitUsedTotal;
import ru.olekstra.domain.dto.PackDiscountData;
import ru.olekstra.domain.dto.PackDiscountLimitData;
import ru.olekstra.domain.dto.VoucherResponseCard;
import ru.olekstra.exception.DataIntegrityException;

import com.amazonaws.util.json.JSONException;

@RunWith(MockitoJUnitRunner.class)
public class LimitsTest {

    private DynamodbService dynamoService;
    private AppSettingsService settingsService;
    private CardOperationService operationService;
    private DiscountService discountService;
    private MessageSource messageSource;
    private VoucherService service;
    private CardLogService cardLogService;
    private DateTimeService dateService;

    private UUID transactionUUID;
    private String cardNum;
    private String discountId;
    private int version;
    private DateTime transactionDateTime;
    private String period;
    private String periodDate;
    private String limitGroupIdDiklo;
    private String limitGroupIdAllPurpose;

    private ProcessingTransaction transaction;
    private VoucherResponseCard card;

    private DiscountLimit diklofenacDiscountLimit;
    private DiscountLimit oneStarDiscountLimit;

    private List<DiscountLimitUsedDetail> detailUsedLimitList;

    private PackDiscountData data;

    private MutableObject cardBalanceCompensationFromLimit;

    private int rowQuantity;
    private BigDecimal discountUsedPercent;
    private BigDecimal rowSumWithoutDiscount;
    private int rowNumber;
    private BigDecimal transactionDiscountSum;
    private BigDecimal cardLimit;

    @Before
    public void setUp() throws JSONException, JsonGenerationException, JsonMappingException, IOException {
        dynamoService = mock(DynamodbService.class);
        settingsService = mock(AppSettingsService.class);
        operationService = mock(CardOperationService.class);
        discountService = mock(DiscountService.class);
        messageSource = mock(MessageSource.class);
        cardLogService = mock(CardLogService.class);
        dateService = mock(DateTimeService.class);
        when(dateService.createDateTimeWithZone(anyString())).thenReturn(DateTime.now());
        when(dateService.createDefaultDateTime()).thenReturn(DateTime.now());

        service = new VoucherService(dynamoService, settingsService,
                operationService, discountService, messageSource, cardLogService,
                dateService);

        transactionUUID = UUID.randomUUID();
        cardNum = "12345";
        discountId = "UNO";
        version = 1;
        transactionDateTime = DateTime.now();
        period = LimitPeriod.MONTH.getPeriod();
        periodDate = LimitPeriod.MONTH.getCurrentPeriod(transactionDateTime);

        limitGroupIdDiklo = "DiklofenacLimit";
        limitGroupIdAllPurpose = "*";

        transaction = new ProcessingTransaction(DateTime.now().withZone(DateTimeZone.UTC));
        transaction.setStartTime(transactionDateTime);
        transaction.setVoucherId(transactionUUID);

        card = new VoucherResponseCard();
        card.setNumber(cardNum);
        transaction.setCard(card);

        diklofenacDiscountLimit = new DiscountLimit(discountId, limitGroupIdDiklo, version, period,
                10, 20L, new BigDecimal(500), new BigDecimal(1000));
        oneStarDiscountLimit = new DiscountLimit(discountId, limitGroupIdAllPurpose, version, period,
                50, 100L, new BigDecimal(8000), new BigDecimal(22000));
        List<DiscountLimit> discountLimitList = Arrays
                .asList(new DiscountLimit[] {diklofenacDiscountLimit, oneStarDiscountLimit});
        transaction.setDiscountLimits(discountLimitList);

        detailUsedLimitList = new ArrayList<DiscountLimitUsedDetail>();

        PackDiscountLimitData pdld1 = new PackDiscountLimitData(limitGroupIdDiklo, 1);
        PackDiscountLimitData pdld2 = new PackDiscountLimitData(limitGroupIdAllPurpose, null);
        data = new PackDiscountData(discountId, new BigDecimal(50), new BigDecimal(60),
                new BigDecimal(50), new BigDecimal(60), new BigDecimal(60));
        data.setPackDiscountLimitData(new PackDiscountLimitData[] {pdld1, pdld2});

        cardBalanceCompensationFromLimit = new MutableObject(new BigDecimal("0"));

        rowQuantity = 1;
        discountUsedPercent = new BigDecimal(50);
        rowSumWithoutDiscount = new BigDecimal(120);
        rowNumber = 1;
        transactionDiscountSum = BigDecimal.ZERO;
        cardLimit = new BigDecimal(4000);

    }

    @Test
    public void testBalanceUseCardLimit() throws JsonGenerationException, JsonMappingException, IOException,
            DataIntegrityException {

        DiscountLimitUsedTotal dlu1 = new DiscountLimitUsedTotal(periodDate, cardNum,
                transactionUUID, 0, limitGroupIdDiklo, discountId,
                4, 8L, new BigDecimal(475), new BigDecimal(600));
        DiscountLimitUsedTotal dlu2 = new DiscountLimitUsedTotal(periodDate, cardNum,
                transactionUUID, 0, limitGroupIdAllPurpose, discountId,
                30, 80L, new BigDecimal(4000), new BigDecimal(1800));
        List<DiscountLimitUsedTotal> discountLimitUsedTotalList = Arrays
                .asList(new DiscountLimitUsedTotal[] {dlu1, dlu2});
        transaction.setDiscountLimitUsedPreviousTotal(discountLimitUsedTotalList);

        Boolean result = service.calculatePackDiscountLimits(data,
                detailUsedLimitList, transaction, rowQuantity, discountUsedPercent,
                rowSumWithoutDiscount, rowNumber, transactionDiscountSum, cardLimit,
                true, cardBalanceCompensationFromLimit, null);

        assertFalse(result);
        assertEquals("35.00", cardBalanceCompensationFromLimit.toString());
        assertEquals(2, detailUsedLimitList.size());
        for (DiscountLimitUsedDetail dlud : detailUsedLimitList) {
            assertEquals("25.00", dlud.getDiscountSum().toString());
        }
    }

    @Test
    public void testBalanceDontUseCardLimit() throws JsonParseException, JsonMappingException, IOException,
            DataIntegrityException {

        DiscountLimitUsedTotal dlu1 = new DiscountLimitUsedTotal(periodDate, cardNum,
                transactionUUID, 0, limitGroupIdDiklo, discountId,
                2, 4L, new BigDecimal(75), new BigDecimal(150));
        DiscountLimitUsedTotal dlu2 = new DiscountLimitUsedTotal(periodDate, cardNum,
                transactionUUID, 0, limitGroupIdAllPurpose, discountId,
                30, 80L, new BigDecimal(400), new BigDecimal(180));
        List<DiscountLimitUsedTotal> discountLimitUsedPrevTotalList = Arrays
                .asList(new DiscountLimitUsedTotal[] {dlu1, dlu2});
        transaction.setDiscountLimitUsedPreviousTotal(discountLimitUsedPrevTotalList);

        Boolean result = service.calculatePackDiscountLimits(data,
                detailUsedLimitList, transaction, rowQuantity, discountUsedPercent,
                rowSumWithoutDiscount, rowNumber, transactionDiscountSum, cardLimit,
                true, cardBalanceCompensationFromLimit, null);

        assertFalse(result);
        assertEquals("0", cardBalanceCompensationFromLimit.toString());
        assertEquals(2, detailUsedLimitList.size());
        for (DiscountLimitUsedDetail dlur : detailUsedLimitList) {
            assertEquals("60.00", dlur.getDiscountSum().toString());
        }
    }

    @Test(expected = DataIntegrityException.class)
    public void testEmptyDiscountData() throws JsonGenerationException, JsonMappingException,
            IOException, DataIntegrityException {

        DiscountLimitUsedTotal dlu1 = new DiscountLimitUsedTotal(periodDate, cardNum,
                transactionUUID, 0, limitGroupIdDiklo, discountId, 0, 0L, BigDecimal.ZERO, BigDecimal.ZERO);
        DiscountLimitUsedTotal dlu2 = new DiscountLimitUsedTotal(periodDate, cardNum,
                transactionUUID, 0, limitGroupIdAllPurpose, discountId, 0, 0L, BigDecimal.ZERO, BigDecimal.ZERO);
        List<DiscountLimitUsedTotal> dluptList = Arrays.asList(new DiscountLimitUsedTotal[] {dlu1, dlu2});
        transaction.setDiscountLimitUsedPreviousTotal(dluptList);

        // нет данных по "общему" лимиту
        List<DiscountLimit> discountLimitList = Arrays.asList(new DiscountLimit[] {diklofenacDiscountLimit});
        transaction.setDiscountLimits(discountLimitList);

        Boolean result = service.calculatePackDiscountLimits(data,
                detailUsedLimitList, transaction, rowQuantity, discountUsedPercent,
                rowSumWithoutDiscount, rowNumber, transactionDiscountSum, cardLimit,
                true, cardBalanceCompensationFromLimit, null);

    }

}
