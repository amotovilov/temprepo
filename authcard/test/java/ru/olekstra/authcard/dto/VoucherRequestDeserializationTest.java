package ru.olekstra.authcard.dto;

import java.io.StringReader;
import java.math.BigDecimal;

import javax.xml.transform.stream.StreamSource;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import ru.olekstra.domain.dto.VoucherRequest;

public class VoucherRequestDeserializationTest extends Assert {
    final String drugstorePackIdArb = "2002123412311";
    final String drugstorePackIdDic = "2002123412334";

    final String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n"
            + "<olk:voucherRequest xmlns:olk=\"http://www.olekstra.ru/schema/cards\">\r\n"
            + "   <olk:cardBarcode>card000</olk:cardBarcode>\r\n"
            + "   <olk:drugstoreId>drugstore000</olk:drugstoreId>\r\n"
            + "   <olk:product>\r\n"
            + "      <olk:clientPackId>2002123412311</olk:clientPackId>\r\n"
            + "      <olk:clientPackName>\u0410\u0440\u0431\u0438\u0434\u043E\u043B</olk:clientPackName>\r\n"
            + "      <olk:expireYear>2015</olk:expireYear>\r\n"
            + "      <olk:price>110.00</olk:price>\r\n"
            + "      <olk:quantity>1</olk:quantity>\r\n"
            + "   </olk:product>\r\n"
            + "   <olk:product>\r\n"
            + "      <olk:clientPackId>2002123412334</olk:clientPackId>\r\n"
            + "      <olk:clientPackName>\u0414\u0438\u043A\u043B\u043E\u0444\u0435\u043D\u0430\u043A-10</olk:clientPackName>\r\n"
            + "      <olk:expireYear>2015</olk:expireYear>\r\n"
            + "      <olk:price>120.00</olk:price>\r\n"
            + "      <olk:quantity>1</olk:quantity>\r\n"
            + "   </olk:product>\r\n"
            + "   <olk:sumToPay>0</olk:sumToPay>\r\n"
            + "   <olk:voucherId>06573e13-f19d-48ac-b6c8-457aef63f523</olk:voucherId>\r\n"
            + "</olk:voucherRequest>";

    @Test
    public void xmlMarshalTest() {
        Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
        jaxb2Marshaller.setClassesToBeBound(VoucherRequest.class);

        final StringReader in = new StringReader(xml);
        VoucherRequest request1 = (VoucherRequest) jaxb2Marshaller.unmarshal(new StreamSource(in));

        assertEquals("drugstore000", request1.getDrugstoreId());
        assertEquals("card000", request1.getCardBarcode());

        assertEquals(new BigDecimal("110.00"), request1.getProducts()[0].getPrice());
        assertEquals(1, request1.getProducts()[0].getQuantity());
        assertEquals(2015, request1.getProducts()[0].getExpireYear());
        assertEquals(drugstorePackIdArb, request1.getProducts()[0].getClientPackId());
        assertEquals("Арбидол", request1.getProducts()[0].getClientPackName());

        assertEquals(new BigDecimal("120.00"), request1.getProducts()[1].getPrice());
        assertEquals(1, request1.getProducts()[1].getQuantity());
        assertEquals(2015, request1.getProducts()[1].getExpireYear());
        assertEquals(drugstorePackIdDic, request1.getProducts()[1].getClientPackId());
        assertEquals("Диклофенак-10", request1.getProducts()[1].getClientPackName());
    }

}
