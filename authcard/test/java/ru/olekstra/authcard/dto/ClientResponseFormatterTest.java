package ru.olekstra.authcard.dto;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.dto.VoucherResponseCard;
import ru.olekstra.domain.dto.VoucherResponsePackReplace;
import ru.olekstra.domain.dto.VoucherResponseRow;

@RunWith(MockitoJUnitRunner.class)
public class ClientResponseFormatterTest {

    private ProcessingTransaction response;

    @Before
    public void before() throws JsonGenerationException, JsonMappingException, IOException {

        String cardNumber = "3456";
        String cardOwner = "user";
        String phone = "0123456789";
        String discountId = "discountId";
        String discountPlan = "discountPlan";
        String comment = "comment";
        String drugstoreId = "drugstoreId";
        String drugstoreName = "drugstoreName";
        UUID voucherId = UUID.fromString("e58052c3-29d5-4d72-8058-93dbc611e5b3");
        UUID confirmationToken = UUID.fromString("a2dfadd7-c2d7-404e-9bd8-9c5b738c6140");

        VoucherResponseCard card = new VoucherResponseCard(cardNumber,
                cardOwner, phone, true, comment, discountId, discountPlan);
        card.setChecks(new String[] {"q1", "q2"});

        VoucherResponseRow row1 = new VoucherResponseRow("name1", "barcode1",
                new BigDecimal("60.00"), 1, new BigDecimal("30.00"),
                new BigDecimal("30.00"), new BigDecimal("50"), new BigDecimal(
                        "30.00"), new BigDecimal("60.00"), new BigDecimal(
                        "30.00"), "clientPackId1", "clientPackName1", 1);
        row1.setId("ekt1");

        VoucherResponseRow row2 = new VoucherResponseRow("name2", "barcode2",
                new BigDecimal("60.00"), 2, new BigDecimal("120.00"),
                new BigDecimal("0.00"), new BigDecimal("0"), new BigDecimal(
                        "0.00"), new BigDecimal("120.00"), new BigDecimal("0"),
                "clientPackId2", "clientPackName2", 2);
        row2.setId("ekt2");

        VoucherResponsePackReplace replacement = new VoucherResponsePackReplace("id", "replaceBarCode", "tradeName");
        row2.setReplacePacks(new VoucherResponsePackReplace[] {replacement});

        List<VoucherResponseRow> rows = Arrays.asList(row1, row2);

        response = new ProcessingTransaction(voucherId,
                drugstoreId, card, drugstoreName, new BigDecimal("100.00"),
                new BigDecimal("30.00"), discountId, 20, "5", new BigDecimal(
                        "150.00"), new String[] {"warning"}, rows, DateTime.now().withZone(DateTimeZone.UTC));
        response.setConfirmationToken(confirmationToken);
    }

    @Test
    public void testToJSONWithNewProcessingTransaction() throws Exception {

        response.setAuthCode("101");
        response.setComplete(true);

        String referenceParsedResponse = "{"
                + "\"voucherId\":\"e58052c3-29d5-4d72-8058-93dbc611e5b3\","
                + "\"drugstoreId\":\"drugstoreId\","
                + "\"confirmationToken\":\"a2dfadd7-c2d7-404e-9bd8-9c5b738c6140\","

                + "\"card\":{" + "\"number\":\"3456\"," + "\"owner\":\"user\","
                + "\"active\":true," + "\"comment\":\"comment\","
                + "\"planId\":\"discountId\","
                + "\"planName\":\"discountPlan\"," + "\"checks\":[" + "\"q1\","
                + "\"q2\"" + "]" + "}," +

                "\"sumWithoutDiscount\":\"100.00\","
                + "\"discountSum\":\"30.00\"," + "\"sumToPay\":\"150.00\","
                + "\"warnings\":[" + "\"warning\"" + "]," +

                "\"rows\":[" + "{\"name\":\"name1\"," + "\"id\":\"ekt1\","
                + "\"clientPackId\":\"clientPackId1\"," + "\"quantity\":1,"
                + "\"price\":\"60.00\"," + "\"sumToPay\":\"30.00\","
                + "\"discountSum\":\"30.00\"," + "\"discountPercent\":50,"
                + "\"refundSum\":\"30.00\","
                + "\"sumWithoutDiscount\":\"60.00\"," + "\"replacePacks\":[],\"warnings\":[]},"
                +

                "{\"name\":\"name2\"," + "\"id\":\"ekt2\","
                + "\"clientPackId\":\"clientPackId2\"," + "\"quantity\":2,"
                + "\"price\":\"60.00\"," + "\"sumToPay\":\"120.00\","
                + "\"discountSum\":\"0.00\"," + "\"discountPercent\":0,"
                + "\"refundSum\":\"0.00\","
                + "\"sumWithoutDiscount\":\"120.00\"," + "\"replacePacks\":[" +

                "{\"packId\":\"id\"," + "\"barcode\":\"replaceBarCode\","
                + "\"tradeName\":\"tradeName\"}" + "],\"warnings\":[]}]," +

                "\"complete\":true," + "\"authorizationCode\":\"101\"}";

        String resultJSONResponse = ClientResponseFormatter.toJSON(response);
        assertEquals(referenceParsedResponse, resultJSONResponse);
    }

    @Test
    public void testToJSONWithOutRows() throws Exception {

        response = new ProcessingTransaction(DateTime.now().withZone(DateTimeZone.UTC));

        response.setAuthCode("101");
        response.setComplete(false);

        String referenceParsedResponse = "{" + "\"voucherId\":null,"
                + "\"drugstoreId\":null,"
                + "\"confirmationToken\":\"\","

                + "\"sumWithoutDiscount\":\"0.00\","
                + "\"discountSum\":\"0.00\"," + "\"sumToPay\":\"0.00\","
                + "\"warnings\":[]," + "\"rows\":[]," + "\"complete\":false,"
                + "\"authorizationCode\":\"101\"}";

        String resultJSONResponse = ClientResponseFormatter.toJSON(response);
        assertEquals(referenceParsedResponse, resultJSONResponse);
    }

    @Test
    public void testToXMLWithNewProcessingTransaction() throws Exception {

        response.setAuthCode("101");
        response.setComplete(true);

        String referenceResultXMLResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><voucherResponse>" +
                "<voucherId>e58052c3-29d5-4d72-8058-93dbc611e5b3</voucherId>" +
                "<drugstoreId>drugstoreId</drugstoreId>" +
                "<confirmationToken>a2dfadd7-c2d7-404e-9bd8-9c5b738c6140</confirmationToken>" +
                "<card><number>3456</number>" +
                "<owner>user</owner><active>true</active><comment>comment</comment>" +
                "<planId>discountId</planId><planName>discountPlan</planName><check>q1</check>" +
                "<check>q2</check></card>" +
                "<sumWithoutDiscount>100.00</sumWithoutDiscount><discountSum>30.00</discountSum>" +
                "<sumToPay>150.00</sumToPay><warning>warning</warning>" +
                "<row><name>name1</name><id>ekt1</id><clientPackId>clientPackId1</clientPackId>" +
                "<quantity>1</quantity><price>60.00</price><sumToPay>30.00</sumToPay>" +
                "<discountSum>30.00</discountSum><discountPercent>50</discountPercent>" +
                "<refundSum>30.00</refundSum><sumWithoutDiscount>60.00</sumWithoutDiscount></row>" +
                "<row><name>name2</name><id>ekt2</id><clientPackId>clientPackId2</clientPackId>" +
                "<quantity>2</quantity><price>60.00</price><sumToPay>120.00</sumToPay>" +
                "<discountSum>0.00</discountSum><discountPercent>0</discountPercent>" +
                "<refundSum>0.00</refundSum><sumWithoutDiscount>120.00</sumWithoutDiscount>" +
                "<replacePack><packId>id</packId><barcode>replaceBarCode</barcode>" +
                "<tradeName>tradeName</tradeName></replacePack></row><complete>true</complete>" +
                "<authorizationCode>101</authorizationCode></voucherResponse>";

        String resultXMLResponse = ClientResponseFormatter.toXML(response);

        assertEquals(referenceResultXMLResponse, resultXMLResponse);

    }

}
