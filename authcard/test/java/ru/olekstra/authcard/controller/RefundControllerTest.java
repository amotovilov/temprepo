package ru.olekstra.authcard.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.UUID;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import ru.olekstra.authcard.dto.ConfirmRequest;
import ru.olekstra.authcard.dto.ConfirmResponse;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.dto.VoucherRequest;
import ru.olekstra.exception.UnconsistentCheckDataException;
import ru.olekstra.service.RefundService;
import ru.olekstra.service.VoucherService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@WebAppConfiguration
public class RefundControllerTest {

    @Autowired
    private VoucherService voucherService;

    @Autowired
    private RefundService refundService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    private ConfirmRequest confirmRequest;
    private ObjectMapper mapper;
    private String requestBodyJson;
    private String requestBodyXml;
    private String responseBodyXML;

    private String drigstoreId;
    private UUID voucherId;
    private UUID confirmationToken;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        drigstoreId = "did";
        voucherId = UUID.randomUUID();
        confirmationToken = UUID.randomUUID();

        confirmRequest = new ConfirmRequest(drigstoreId, voucherId, confirmationToken);

        mapper = new ObjectMapper();
        requestBodyJson = mapper.writeValueAsString(confirmRequest);

        requestBodyXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                "<confirmRequest>" +
                "<drugstoreId>" + drigstoreId + "</drugstoreId>" +
                "<voucherId>" + voucherId.toString() + "</voucherId>" +
                "<confirmationToken>" + confirmationToken.toString() + "</confirmationToken>" +
                "</confirmRequest>";

        responseBodyXML = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
                "<confirmResponse>" +
                "<authorizationCode>1234</authorizationCode>" +
                "<voucherId>" + voucherId.toString() + "</voucherId>" +
                "</confirmResponse>";

    }

    @Test
    public void testConfirmRefundWithNullTransaction() throws Exception {

        // транзакция не нашлась
        when(voucherService.getProcessingTransaction(any(VoucherRequest.class))).thenReturn(null);

        // ожидаем 409
        mockMvc.perform(post("/confirm.json")
                .content(requestBodyJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict());
    }

    @Test
    public void testConfirmRefundWithWrongDrugstoreId() throws JsonGenerationException,
            JsonMappingException, IOException, Exception {

        // код аптеки в реквесте и взятой транзакции не совпадает
        ProcessingTransaction transaction = new ProcessingTransaction(DateTime.now().withZone(DateTimeZone.UTC));
        transaction.setDrugstoreId("dead");

        when(voucherService.getProcessingTransaction(any(VoucherRequest.class))).thenReturn(transaction);

        // ожидаем 400
        mockMvc.perform(post("/confirm.json")
                .content(requestBodyJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testConfirmRefundWithException() throws Exception {

        // успешный запрос и ответ
        ProcessingTransaction transaction = new ProcessingTransaction(DateTime.now().withZone(DateTimeZone.UTC));
        transaction.setDrugstoreId(drigstoreId);

        when(voucherService.getProcessingTransaction(any(VoucherRequest.class))).thenReturn(transaction);
        when(refundService.getConfirmResponse(any(ConfirmRequest.class), any(VoucherRequest.class), eq(transaction)))
                .thenThrow(new UnconsistentCheckDataException("ConfirmationToken mismatch"));

        // ожидаем 409
        mockMvc.perform(post("/confirm.json")
                .content(requestBodyJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isConflict());

        verify(refundService, times(1))
                .getConfirmResponse(any(ConfirmRequest.class), any(VoucherRequest.class), eq(transaction));

    }

    @Test
    public void testConfirmRefundWithSuccess() throws Exception {

        // успешный запрос и ответ
        ProcessingTransaction transaction = new ProcessingTransaction(DateTime.now().withZone(DateTimeZone.UTC));
        transaction.setDrugstoreId(drigstoreId);

        ConfirmResponse confirmResponse = new ConfirmResponse(confirmRequest.getVoucherId(), "1234");

        when(voucherService.getProcessingTransaction(any(VoucherRequest.class))).thenReturn(transaction);
        when(refundService.getConfirmResponse(any(ConfirmRequest.class), any(VoucherRequest.class), eq(transaction)))
                .thenReturn(confirmResponse);

        // ожидаем 200
        mockMvc.perform(post("/confirm.json")
                .content(requestBodyJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("voucherId").value(confirmRequest.getVoucherId().toString()))
                .andExpect(jsonPath("authorizationCode").value("1234"));

        verify(refundService, times(1))
                .getConfirmResponse(any(ConfirmRequest.class), any(VoucherRequest.class), eq(transaction));

    }

    @Test
    public void testXmlConfirmRefundWithSuccess() throws Exception {

        // успешный запрос и ответ
        ProcessingTransaction transaction = new ProcessingTransaction(DateTime.now().withZone(DateTimeZone.UTC));
        transaction.setDrugstoreId(drigstoreId);

        ConfirmResponse confirmResponse = new ConfirmResponse(confirmRequest.getVoucherId(), "1234");

        when(voucherService.getProcessingTransaction(any(VoucherRequest.class))).thenReturn(transaction);
        when(refundService.getConfirmResponse(any(ConfirmRequest.class), any(VoucherRequest.class), eq(transaction)))
                .thenReturn(confirmResponse);

        // ожидаем 200
        mockMvc.perform(post("/confirm.xml")
                .content(requestBodyXml.getBytes())
                .contentType(MediaType.APPLICATION_XML))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/xml"))
                .andExpect(content().xml(responseBodyXML));

        verify(refundService, times(1))
                .getConfirmResponse(any(ConfirmRequest.class), any(VoucherRequest.class), eq(transaction));

    }

    @Configuration
    @EnableWebMvc
    static class RefundControllerTestConfig {

        @Bean
        public VoucherService voucherService() {
            return mock(VoucherService.class);
        }

        @Bean
        public RefundService refundService() {
            return mock(RefundService.class);
        }

        @Bean
        public RefundController refundController() {
            return new RefundController();
        }

    }
}
