package ru.olekstra.authcard.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import net.sf.ehcache.CacheManager;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.dozer.Mapper;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.awsutils.exception.NotUpdatedException;
import ru.olekstra.awsutils.exception.OlekstraException;
import ru.olekstra.azure.model.MessageSendException;
import ru.olekstra.common.dao.AppSettingsDao;
import ru.olekstra.common.dao.DiscountDao;
import ru.olekstra.common.helper.PlanHelper;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.common.service.CardLogService;
import ru.olekstra.common.service.DateTimeService;
import ru.olekstra.common.service.DiscountService;
import ru.olekstra.common.service.LimitPeriod;
import ru.olekstra.common.service.PlanService;
import ru.olekstra.common.service.XmlMessageService;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.Discount;
import ru.olekstra.domain.DiscountData;
import ru.olekstra.domain.DiscountLimit;
import ru.olekstra.domain.DiscountLimitUsed;
import ru.olekstra.domain.DiscountPlan;
import ru.olekstra.domain.DiscountReplace;
import ru.olekstra.domain.Drugstore;
import ru.olekstra.domain.DrugstorePackIndex;
import ru.olekstra.domain.EntityProxy;
import ru.olekstra.domain.Pack;
import ru.olekstra.domain.PackA;
import ru.olekstra.domain.dto.VoucherProduct;
import ru.olekstra.domain.dto.VoucherRequest;
import ru.olekstra.exception.DataIntegrityException;
import ru.olekstra.exception.NotFoundException;
import ru.olekstra.exception.UnconsistentCheckDataException;
import ru.olekstra.exception.UnknownPlanTypeException;
import ru.olekstra.service.CardOperationService;
import ru.olekstra.service.CardService;
import ru.olekstra.service.DrugstoreService;
import ru.olekstra.service.VoucherService;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AwsTestConfig.class})
public class CardControllerIntegrationTest {

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private DynamodbService dynamodbService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private CacheManager cacheManager;

    @Autowired
    private AppSettingsService settingService;

    @Autowired
    private DateTimeService dateService;

    @Autowired
    private XmlMessageService xmlMessageService;

    @Autowired
    Mapper mapper;

    private CardOperationService cardOperationService;
    private VoucherService voucherService;
    private DiscountService discountService;
    private CardController cardController;

    private Card card;
    private Card card_;

    static final String drugstoreId = "TEST DARYHANA";
    static final String drugstoreName = "Дарыхана на проспекте Абая";

    static final String drugstorePackIdArb = "2002123412311";
    static final String drugstorePackIdArb2 = "2002123412322";
    static final String drugstorePackIdVit = "2002123412333";
    static final String drugstorePackIdDic = "2002123412334";
    static final String drugstorePackIdPir = "2002123412335";
    static final String drugstorePackIdAsp = "2002123412336";
    static final String drugstorePackIdBro = "2002123412337";
    static final String drugstorePackIdGlo = "2002123412338";
    static final String drugstorePackIdIod = "2002123412339";
    static final String drugstorePackNameIod = "testIod";
    static final String drugstorePackIdKetanov = "2002123412340";
    static final String drugstorePackIdKetorol = "2002123412341";
    static final String drugstorePackIdNiz = "2002123412342";

    static final String limitArbId = "ArbidolLimit";
    static final String limitVitId = "VitaminLimit";
    static final String limitDicId = "DiclofinacLimit";
    static final String limitPirId = "PiracetamLimit";
    static final String limitAspId = "AspirinLimit";
    static final String limitBroId = "BromhexinLimit";
    static final String limitGloId = "GlovesLimit";
    static final String limitIodId = "IodLimit";
    static final String limitAllId = (String) DiscountLimit.getAllPurposeLimitId().toArray()[0];

    static final String tarifId = "TEST_TARIF";
    static final String discountIdUno = "UNO";
    static final String discountIdDos = "DOS";
    static final String discountIdTre = "TRE";

    static final String cardNumber = "7770000000177";
    static final String cardNumber_ = "7770000000178";

    private DiscountLimitUsed newDiscountLimitUsed(String period, String cardNum, UUID voucherId, Integer row,
            String limitGroupId, String discountId, Integer packCount, Long averagePackCount,
            BigDecimal discountSum, BigDecimal sumBeforeDiscount) {
        String hashKey = null;
        String rangeKey = null;
        if (voucherId == null
                || voucherId.toString().equals(EntityProxy.NULL_UUID)) {
            hashKey = DiscountLimitUsed.buildKey(period, cardNum, DiscountLimitUsed.TYPE_TOTAL);
            rangeKey = DiscountLimitUsed.buildKey(limitGroupId, discountId,
                    EntityProxy.NULL_UUID, "0");
        } else {
            hashKey = DiscountLimitUsed.buildKey(period, cardNum, DiscountLimitUsed.TYPE_DETAIL);
            rangeKey = DiscountLimitUsed.buildKey(limitGroupId, discountId,
                    voucherId.toString(),
                    String.valueOf(row));
        }
        DiscountLimitUsed dlu = new DiscountLimitUsed(hashKey, rangeKey,
                packCount, averagePackCount, discountSum, sumBeforeDiscount);
        return dlu;
    }

    @Before
    public void before() throws JsonGenerationException, JsonMappingException, IOException,
            ItemSizeLimitExceededException, NotUpdatedException {

        assertNotNull(dynamodbService);
        assertNotNull(messageSource);
        assertNotNull(cacheManager);
        assertNotNull(settingService);
        assertNotNull(dateService);

        CardService cardService = new CardService(dynamodbService);
        DrugstoreService drugstoreService = new DrugstoreService(
                dynamodbService);
        cardOperationService = Mockito.mock(CardOperationService.class);

        AppSettingsDao appSettingsDao = new AppSettingsDao(dynamodbService);
        AppSettingsService appSettingsService = new AppSettingsService(
                appSettingsDao);
        DiscountDao discountDao = new DiscountDao(dynamodbService);
        PlanHelper planHelper = new PlanHelper(dynamodbService);
        PlanService planService = new PlanService(planHelper);
        discountService = new DiscountService(discountDao, dynamodbService,
                planService, appSettingsService, mapper, messageSource, dateService);
        CardLogService cardLogService = new CardLogService(dynamodbService, messageSource);
        voucherService = new VoucherService(dynamodbService, appSettingsService,
                cardOperationService, discountService, messageSource, cardLogService, dateService);
        cardController = new CardController(cardService, drugstoreService,
                cardOperationService, voucherService, messageSource,
                discountService, cardLogService, xmlMessageService,
                dynamodbService, null);

        // Тестовая модель данных

        // аптека
        Drugstore drugstore = new Drugstore(drugstoreId, drugstoreName, "Asia/Omsk", null, false);
        drugstore.setNonRefundPercent(12);

        dynamodbService.putObjectOrDie(drugstore);

        // товары
        String ekt1 = "TEST001";
        String ekt2 = "TEST002";
        String ekt3 = "TEST003";
        String ekt4 = "TEST004";
        String ekt5 = "TEST005";
        String ekt6 = "TEST006";
        String ekt7 = "TEST007";
        String ekt8 = "TEST008";
        String ekt9 = "TEST009";
        String ekt10 = "TEST010";
        String ekt11 = "TEST011";
        String ekt12 = "TEST012";

        Pack packArb10 = new PackA(ekt1, ekt1, "", "", "Арбидол-10", "Россия", "", "Арбидол", "Арбидол");
        Pack packArb20 = new PackA(ekt2, ekt2, "", "", "Арбидол-20", "Россия", "", "Арбидол", "Арбидол");
        Pack packVit = new PackA(ekt3, ekt3, "", "", "Поливитамины", "Венгрия", "", "Витамин", "Витамин");
        Pack packDiс = new PackA(ekt4, ekt4, "", "", "Диклофенак-100", "Индия", "", "Диклофенак", "Диклофенак");
        Pack packPir = new PackA(ekt5, ekt5, "", "", "Ноотропил", "Бельгия", "", "Пирацетам", "Пирацетам");
        Pack packAsp = new PackA(ekt6, ekt6, "", "", "Аспирин Бонжур", "Франция", "", "Аспирин",
                "Аспирин");
        Pack packBro = new PackA(ekt7, ekt7, "", "", "Бромгексин Берлин-Хеми", "Германия", "", "Бромгексин",
                "Бромгексин");
        Pack packGlo = new PackA(ekt8, ekt8, "", "", "Перчатки", "Китай", "", "Перчатки", "Перчатки");
        Pack packIod = new PackA(ekt9, ekt9, "", "", "Йод", "Россия", "", "Йод", "Йод");
        Pack packKetanov = new PackA(ekt10, ekt10, "", "", "Кетанов", "Россия", "Кеторол", "Кеторолака", "Кеторолака");
        Pack packKetorol = new PackA(ekt11, ekt11, "", "", "Кеторол", "Россия", "", "Кеторолака", "Кеторолака");
        Pack packNiz = new PackA(ekt12, ekt12, "", "", "Найз", "Россия", "Кеторол", "Найз", "Найз");

        List<? extends Pack> packList = Arrays.asList(packArb10, packArb20, packVit, packDiс, packPir, packAsp,
                packBro, packGlo, packIod, packKetanov, packKetorol, packNiz);

        dynamodbService.putObjectsAllOrDie(packList);

        // для Кетанова будет синоним (в каждом дисконте)
        DiscountReplace discountReplaceKetorolUno = new DiscountReplace(discountIdUno, "Кеторол", ekt11, "1");
        discountReplaceKetorolUno.setTradeName("Кеторол");
        DiscountReplace discountReplaceKetorolDos = new DiscountReplace(discountIdDos, "Кеторол", ekt11, "1");
        discountReplaceKetorolDos.setTradeName("Кеторол");
        DiscountReplace discountReplaceKetorolTre = new DiscountReplace(discountIdTre, "Кеторол", ekt11, "1");
        discountReplaceKetorolTre.setTradeName("Кеторол");
        List<DiscountReplace> discountReplaceList = Arrays.asList(discountReplaceKetorolUno, discountReplaceKetorolDos,
                discountReplaceKetorolTre);

        dynamodbService.putObjectsAllOrDie(discountReplaceList);

        // индексная таблица
        DrugstorePackIndex indx1 = new DrugstorePackIndex(drugstoreId, drugstorePackIdArb, ekt1);
        DrugstorePackIndex indx2 = new DrugstorePackIndex(drugstoreId, drugstorePackIdArb2, ekt2);
        DrugstorePackIndex indx3 = new DrugstorePackIndex(drugstoreId, drugstorePackIdVit, ekt3);
        DrugstorePackIndex indx4 = new DrugstorePackIndex(drugstoreId, drugstorePackIdDic, ekt4);
        DrugstorePackIndex indx5 = new DrugstorePackIndex(drugstoreId, drugstorePackIdPir, ekt5);
        DrugstorePackIndex indx6 = new DrugstorePackIndex(drugstoreId, drugstorePackIdAsp, ekt6);
        DrugstorePackIndex indx7 = new DrugstorePackIndex(drugstoreId, drugstorePackIdBro, ekt7);
        DrugstorePackIndex indx8 = new DrugstorePackIndex(drugstoreId, drugstorePackIdGlo, ekt8);
        DrugstorePackIndex indx9 = new DrugstorePackIndex(drugstoreId, drugstorePackIdIod, ekt9);
        DrugstorePackIndex indx10 = new DrugstorePackIndex(drugstoreId, drugstorePackIdKetanov, ekt10);
        DrugstorePackIndex indx11 = new DrugstorePackIndex(drugstoreId, drugstorePackIdKetorol, ekt11);
        DrugstorePackIndex indx12 = new DrugstorePackIndex(drugstoreId, drugstorePackIdNiz, ekt12);
        DrugstorePackIndex indx13 = new DrugstorePackIndex(drugstoreId, drugstorePackNameIod, ekt9);
        List<DrugstorePackIndex> indexList = Arrays.asList(indx1, indx2, indx3, indx4, indx5, indx6, indx7, indx8,
                indx9, indx10, indx11, indx12, indx13);

        dynamodbService.putObjectsAllOrDie(indexList);

        // дисконты
        int version1 = 1;
        int packCount = 5;
        int maxDiscountPercent = 75;
        List<String> discountIdList = Arrays.asList(discountIdUno, discountIdDos, discountIdTre);

        DiscountPlan discountPlan = new DiscountPlan(tarifId, tarifId, maxDiscountPercent, "для интеграц. тестов", "");
        discountPlan.setDiscounts(discountIdList);

        dynamodbService.putObjectOrDie(discountPlan);

        // первый дисконт с лимитами по скидкам
        Discount discountUno = new Discount(discountIdUno, "тестовый первый", false, false, null, version1, packCount);
        // второй дисконт без лимитов по скидкам, но с лимитом по карте
        Discount discountDos = new Discount(discountIdDos, "тестовый второй", true, false, null, version1, packCount);
        // третий дисконт c лимитами по скидкам и лимитом по карте
        // (в этом случае нехватка discountSum в лимите по скидке может быть
        // компенсирована из лимита по карте)
        Discount discountTre = new Discount(discountIdTre, "тестовый третий", true, false, null, version1, packCount);
        List<Discount> discountList = Arrays.asList(discountUno, discountDos, discountTre);

        dynamodbService.putObjectsAllOrDie(discountList);

        // лимиты
        DiscountLimit limitArb = new DiscountLimit(discountIdUno, limitArbId, version1, LimitPeriod.MONTH.getPeriod(),
                10, 100L, new BigDecimal(750), new BigDecimal(5000));
        DiscountLimit limitVit = new DiscountLimit(discountIdUno, limitVitId, version1, LimitPeriod.MONTH.getPeriod(),
                10, 100L, new BigDecimal(1500), BigDecimal.ZERO);
        DiscountLimit limitDic = new DiscountLimit(discountIdUno, limitDicId, version1, LimitPeriod.MONTH.getPeriod(),
                10, 100L, new BigDecimal(1000), BigDecimal.ZERO);
        DiscountLimit limitPir = new DiscountLimit(discountIdUno, limitPirId, version1, LimitPeriod.MONTH.getPeriod(),
                2, 5L, BigDecimal.ZERO, BigDecimal.ZERO);
        DiscountLimit limitAsp = new DiscountLimit(discountIdTre, limitAspId, version1, LimitPeriod.WEEK.getPeriod(),
                6, 12L, new BigDecimal(350), BigDecimal.ZERO);
        DiscountLimit limitBro = new DiscountLimit(discountIdTre, limitBroId, version1, LimitPeriod.WEEK.getPeriod(),
                10, 50L, new BigDecimal(1000), BigDecimal.ZERO);
        DiscountLimit limitGlo = new DiscountLimit(discountIdTre, limitGloId, version1, LimitPeriod.WEEK.getPeriod(),
                5, 10L, new BigDecimal(100), BigDecimal.ZERO);
        DiscountLimit limitIod = new DiscountLimit(discountIdTre, limitIodId, version1, LimitPeriod.WEEK.getPeriod(),
                5, 5L, new BigDecimal(100), new BigDecimal(200));
        DiscountLimit limitAllUno = new DiscountLimit(discountIdUno, limitAllId, version1,
                LimitPeriod.YEAR.getPeriod(),
                0, 0L, BigDecimal.ZERO, new BigDecimal(8000));
        DiscountLimit limitAllTre = new DiscountLimit(discountIdTre, limitAllId, version1,
                LimitPeriod.YEAR.getPeriod(),
                0, 0L, new BigDecimal(3000), new BigDecimal(8000));
        List<DiscountLimit> dlList = Arrays.asList(limitArb, limitVit, limitDic, limitPir, limitAsp, limitBro,
                limitGlo, limitIod, limitAllUno, limitAllTre);

        dynamodbService.putObjectsAllOrDie(dlList);

        DiscountData ddArbidol = new DiscountData(discountIdUno, ekt1, version1, 40, limitArbId, 1);
        DiscountData ddVitamin = new DiscountData(discountIdUno, ekt3, version1, 40, limitVitId, 1);
        DiscountData ddDiclofinac = new DiscountData(discountIdUno, ekt4, version1, 40, limitDicId, 1);
        DiscountData ddPiracetam = new DiscountData(discountIdUno, ekt5, version1, 40, limitPirId, 1);
        DiscountData ddPiracetamUnlimit = new DiscountData(discountIdDos, ekt5, version1, 40, null, null);
        DiscountData ddAspirin = new DiscountData(discountIdTre, ekt6, version1, 40, limitAspId, 2);
        DiscountData ddBromhexin = new DiscountData(discountIdTre, ekt7, version1, 40, limitBroId, 1);
        DiscountData ddAspirinUnlim = new DiscountData(discountIdDos, ekt6, version1, 50, null, null);
        DiscountData ddBromhexinUnlim = new DiscountData(discountIdDos, ekt7, version1, 50, null, null);
        DiscountData ddGloves = new DiscountData(discountIdTre, ekt8, version1, 50, limitGloId, 2);
        DiscountData ddIod = new DiscountData(discountIdTre, ekt9, version1, 50, limitIodId, 1);
        List<DiscountData> ddList = Arrays.asList(ddArbidol, ddVitamin, ddDiclofinac, ddPiracetam, ddPiracetamUnlimit,
                ddAspirin, ddBromhexin, ddAspirinUnlim, ddBromhexinUnlim, ddGloves, ddIod);

        dynamodbService.putObjectsAllOrDie(ddList);

        // карта
        card = new Card(cardNumber, "000", false);
        card.setDiscountPlan(tarifId);
        card.setHolderName("Интегратор");
        card.setTelephoneNumber("0000");
        card.setLimitRemain(new BigDecimal(1000));

        card_ = new Card(cardNumber_, "000", false);
        card_.setDiscountPlan(tarifId);
        card_.setHolderName("Интегратор_");
        card_.setTelephoneNumber("1111");
        card_.setLimitRemain(new BigDecimal(1000));

        List<Card> cardList = Arrays.asList(card, card_);

        dynamodbService.putObjectsAllOrDie(cardList);

    }

    @Test
    public void discountCacheTest() throws RuntimeException,
            InterruptedException, IOException {
        // Cache cache = cacheManager.getCache(DiscountDao.CACHE_NAME);
        List<Discount> discountList = discountService.getDiscounts(Arrays
                .asList("num5", "num4"));
        assertNotNull(discountList);
        assertEquals(2, discountList.size());
        // В кеше должна быть 1 запись
        // assertNotNull(cache);
        // assertEquals(1, cache.getSize());
    }

    HttpServletResponse response = mock(HttpServletResponse.class);

    private String getResponseBody(VoucherRequest request) throws IOException, AmazonServiceException,
            IllegalArgumentException, NotUpdatedException, InstantiationException, IllegalAccessException,
            InterruptedException, ItemSizeLimitExceededException, NotFoundException, OlekstraException,
            RuntimeException, JSONException, DataIntegrityException, MessageSendException,
            ParserConfigurationException, TransformerException {
        OutputStream stream = new ByteArrayOutputStream();
        PrintWriter writer = new PrintWriter(stream);
        when(response.getWriter()).thenReturn(writer);
        cardController.getCheckSumAsJson(request, response);
        String responseBody = stream.toString();
        return responseBody;

    }

    @Test
    public void cardVerificationTest() throws JsonParseException,
            JsonMappingException, IOException, NotFoundException,
            UnconsistentCheckDataException, ItemSizeLimitExceededException,
            UnknownPlanTypeException, InterruptedException, JSONException,
            OlekstraException, InstantiationException, IllegalAccessException,
            RuntimeException, DataIntegrityException, MessageSendException, ParserConfigurationException,
            TransformerException {
        VoucherRequest request = new VoucherRequest();
        request.setDrugstoreId(drugstoreId);
        // "левая" карта
        request.setCardBarcode("000111");
        request.setProducts(new VoucherProduct[] {});

        String responseBody = getResponseBody(request);

        // Из сериализованного объекта создаем объект JSONObject
        // Интересно, что классы org.json входят в состав AWS Utils, так что нам
        // нет необходимости помещать в класспасс что-то дополнительное
        JSONObject jsonObject = new JSONObject(responseBody);

        // "Простые" объекты из JSONObject вынимаются методом getString(keyName)
        assertEquals(EntityProxy.NULL_UUID, jsonObject.getString("voucherId"));

        // "Списковые" (массивы) - getJSONArray(keyName)
        assertEquals(0, jsonObject.getJSONArray("rows").length());
        String msg = messageSource.getMessage("msg.thiscardnotfound", new Object[] {request.getCardBarcode()}, null);
        assertEquals(msg, jsonObject.getJSONArray("warnings").get(0));

        // Вложенные объекты - getJSONObject(keyName)
        assertFalse(jsonObject.getJSONObject("card").getBoolean("active"));

    }

    @Test
    public void packReplaceTest() throws AmazonServiceException, IllegalArgumentException, NotUpdatedException,
            IOException, InstantiationException, IllegalAccessException, InterruptedException,
            ItemSizeLimitExceededException, NotFoundException, OlekstraException, RuntimeException, JSONException,
            DataIntegrityException, MessageSendException, ParserConfigurationException, TransformerException {

        // первый запрос - проверка карты
        VoucherRequest request1 = new VoucherRequest();
        request1.setDrugstoreId(drugstoreId);
        request1.setCardBarcode(cardNumber);
        request1.setProducts(new VoucherProduct[] {});
        String responseBody1 = getResponseBody(request1);

        JSONObject jsonObject1 = new JSONObject(responseBody1);

        // второй запрос с товарами для расчета скидок

        VoucherRequest request2 = new VoucherRequest();
        request2.setDrugstoreId(drugstoreId);
        request2.setCardBarcode(cardNumber);
        request2.setVoucherId(UUID.fromString(jsonObject1.getString("voucherId")));
        VoucherProduct product1 = new VoucherProduct(null, null, null,
                new BigDecimal("65.00"), 1, 2015, drugstorePackIdKetanov, "Кетанов 10 таб.");
        VoucherProduct product2 = new VoucherProduct(null, null, null,
                new BigDecimal("75.00"), 1, 2015, drugstorePackIdNiz, "Найз 20 таб.");
        request2.setProducts(new VoucherProduct[] {product1, product2});

        String responseBody2 = getResponseBody(request2);

        JSONObject jsonObject2 = new JSONObject(responseBody2);
        JSONObject isonObjectRow = (JSONObject) jsonObject2.getJSONArray("rows").get(0);
        assertEquals(1, isonObjectRow.getJSONArray("replacePacks").length());
    }

    @Test
    public void recalculateTest() throws JsonParseException,
            JsonMappingException, IOException, NotFoundException,
            UnconsistentCheckDataException, ItemSizeLimitExceededException,
            UnknownPlanTypeException, InterruptedException, JSONException,
            OlekstraException, InstantiationException, IllegalAccessException,
            RuntimeException, DataIntegrityException, MessageSendException, ParserConfigurationException,
            TransformerException {

        // первый запрос - проверка карты
        VoucherRequest request1 = new VoucherRequest();
        request1.setDrugstoreId(drugstoreId);
        request1.setCardBarcode(cardNumber);
        request1.setProducts(new VoucherProduct[] {});
        String responseBody1 = getResponseBody(request1);

        System.out.println(responseBody1);

        JSONObject jsonObject1 = new JSONObject(responseBody1);

        // второй запрос с товарами для расчета скидок

        VoucherRequest request2 = new VoucherRequest();
        request2.setDrugstoreId(drugstoreId);
        request2.setCardBarcode(cardNumber);
        request2.setVoucherId(UUID.fromString(jsonObject1
                .getString("voucherId")));
        VoucherProduct product1 = new VoucherProduct(null, null, null,
                new BigDecimal("110.00"), 1, 2015, drugstorePackIdArb, "Арбидол");
        VoucherProduct product2 = new VoucherProduct(null, null, null,
                new BigDecimal("120.00"), 1, 2015, drugstorePackIdDic, "Диклофенак-10");
        VoucherProduct product3 = new VoucherProduct(null, null, null,
                new BigDecimal("25.50"), 1, 2015, drugstorePackIdVit, "Витапачка");
        VoucherProduct product4 = new VoucherProduct(null, null, null,
                new BigDecimal("150"), 1, 2015, drugstorePackIdPir, "Пирацетам");

        request2.setProducts(new VoucherProduct[] {product1, product2});

        String responseBody2 = getResponseBody(request2);

        JSONObject jsonObject2 = new JSONObject(responseBody2);
        assertTrue(jsonObject2.getJSONObject("card").getBoolean("active"));
        assertFalse(jsonObject2.getString("voucherId").equalsIgnoreCase(EntityProxy.NULL_UUID));
        assertEquals(2, jsonObject2.getJSONArray("rows").length());

        // третий запрос - c другими товарами
        VoucherRequest request3 = new VoucherRequest();
        request3.setDrugstoreId(drugstoreId);
        request3.setCardBarcode(cardNumber);
        request3.setProducts(new VoucherProduct[] {product3, product4});
        request3.setVoucherId(UUID.fromString(jsonObject2.getString("voucherId")));

        String responseBody3 = getResponseBody(request3);

        JSONObject jsonObject3 = new JSONObject(responseBody3);
        assertTrue(jsonObject3.getJSONObject("card").getBoolean("active"));
        assertFalse(jsonObject3.getString("voucherId").equalsIgnoreCase(EntityProxy.NULL_UUID));
        assertEquals(2, jsonObject3.getJSONArray("rows").length());
    }

    @Test
    public void recalculateExactFieldsTest()
            throws AmazonServiceException, IllegalArgumentException, NotUpdatedException, InstantiationException,
            IllegalAccessException,
            IOException, InterruptedException, ItemSizeLimitExceededException, NotFoundException, OlekstraException,
            RuntimeException,
            JSONException, DataIntegrityException, MessageSendException, ParserConfigurationException,
            TransformerException {
        VoucherRequest request4 = new VoucherRequest();
        request4.setDrugstoreId(drugstoreId);
        request4.setCardBarcode(cardNumber);
        request4.setExactRefundSum(new BigDecimal("20.10"));
        request4.setExactNonRefundSum(new BigDecimal("30.11"));
        request4.setExactSumToPay(new BigDecimal("40.32"));
        VoucherProduct product41 = new VoucherProduct(null, null, null,
                new BigDecimal("30.33"), 1, 2015, drugstorePackIdArb, "Арбидол");
        product41.setExactRefundSum(new BigDecimal("10.10"));
        product41.setExactNonRefundSum(new BigDecimal("10.11"));
        product41.setExactSumToPay(new BigDecimal("10.12"));
        VoucherProduct product42 = new VoucherProduct(null, null, null,
                new BigDecimal("60.00"), 1, 2015, drugstorePackIdNiz, "Найз 20 таб.");
        product42.setExactRefundSum(new BigDecimal("10.00"));
        product42.setExactNonRefundSum(new BigDecimal("20.00"));
        product42.setExactSumToPay(new BigDecimal("30.00"));
        request4.setProducts(new VoucherProduct[] {product41, product42});
        String responseBody4 = getResponseBody(request4);
        JSONObject jsonObject4 = new JSONObject(responseBody4);

        JSONArray rows = jsonObject4.getJSONArray("rows");

        assertEquals("8.49", rows.getJSONObject(0).getString("exactRefundSum"));
        assertEquals("3.64", rows.getJSONObject(0).getString("exactNonRefundSum"));
        assertEquals("18.2", rows.getJSONObject(0).getString("exactSumToPay"));

        assertEquals("8.49", jsonObject4.getString("exactRefundSum"));
        assertEquals("3.64", jsonObject4.getString("exactNonRefundSum"));
        assertEquals("78.20", jsonObject4.getString("exactSumToPay"));

    }

    @Test
    public void recalculateExactFieldsCheckTest()
            throws AmazonServiceException, IllegalArgumentException, NotUpdatedException, InstantiationException,
            IllegalAccessException,
            IOException, InterruptedException, ItemSizeLimitExceededException, NotFoundException, OlekstraException,
            RuntimeException,
            JSONException, DataIntegrityException, MessageSendException, ParserConfigurationException,
            TransformerException {
        // на балансе карты всего 10.
        // карта
        card = new Card(cardNumber, "000", false);
        card.setDiscountPlan(tarifId);
        card.setHolderName("Интегратор");
        card.setTelephoneNumber("0000");
        card.setLimitRemain(new BigDecimal("2.00"));

        dynamodbService.putObjectOrDie(card);

        // проверка exact-полей

        VoucherRequest request2 = new VoucherRequest();
        request2.setDrugstoreId(drugstoreId);
        request2.setCardBarcode(cardNumber);
        request2.setExactNonRefundSum(BigDecimal.ZERO);
        request2.setExactRefundSum(BigDecimal.ZERO);
        request2.setExactSumToPay(BigDecimal.ZERO);
        VoucherProduct product1 = new VoucherProduct(null, null, null,
                new BigDecimal("33.00"), 1, 2015, drugstorePackIdGlo, "Перчатки хирург.");
        product1.setExactRefundSum(new BigDecimal("1.0"));
        product1.setExactNonRefundSum(new BigDecimal("2.0"));
        product1.setExactSumToPay(new BigDecimal("30.0"));
        VoucherProduct product2 = new VoucherProduct(null, null, null,
                new BigDecimal("33.00"), 1, 2015, drugstorePackIdDic, "Диклофенак-10");
        product2.setExactRefundSum(new BigDecimal("9.25"));
        product2.setExactNonRefundSum(new BigDecimal("3.96"));
        product2.setExactSumToPay(new BigDecimal("19.79"));

        request2.setProducts(new VoucherProduct[] {product1, product2});

        String responseBody2 = getResponseBody(request2);

        JSONObject jsonObject2 = new JSONObject(responseBody2);

        // для первой строки должен сработать перерасчет exact-полей
        JSONObject row0 = jsonObject2.getJSONArray("rows").getJSONObject(0);
        assertEquals(new BigDecimal(row0.getString("refundSum")),
                new BigDecimal(row0.getString("exactRefundSum")).setScale(2));
        assertEquals(
                new BigDecimal(row0.getString("discountSum")).subtract(new BigDecimal(row0.getString("refundSum"))),
                new BigDecimal(row0.getString("exactNonRefundSum")).setScale(2));
        assertEquals(new BigDecimal(row0.getString("sumToPay")).setScale(2),
                new BigDecimal(row0.getString("exactSumToPay")).setScale(2));

        // а для второй - нет
        JSONObject row1 = jsonObject2.getJSONArray("rows").getJSONObject(1);
        assertFalse(new BigDecimal(row1.getString("refundSum")).setScale(2)
                .equals(new BigDecimal(row1.getString("exactRefundSum")).setScale(2)));
        assertFalse(new BigDecimal(row1.getString("sumToPay")).setScale(2)
                .equals(new BigDecimal(row1.getString("exactSumToPay")).setScale(2)));
    }

    @Test
    public void recalculateExactFieldsEmptyTest()
            throws AmazonServiceException, IllegalArgumentException, NotUpdatedException, InstantiationException,
            IllegalAccessException,
            IOException, InterruptedException, ItemSizeLimitExceededException, NotFoundException, OlekstraException,
            RuntimeException,
            JSONException, DataIntegrityException, MessageSendException, ParserConfigurationException,
            TransformerException {
        // на балансе карты всего 10.
        // карта
        card = new Card(cardNumber, "000", false);
        card.setDiscountPlan(tarifId);
        card.setHolderName("Интегратор");
        card.setTelephoneNumber("0000");
        card.setLimitRemain(new BigDecimal("2.00"));

        dynamodbService.putObjectOrDie(card);

        // проверка exact-полей

        VoucherRequest request2 = new VoucherRequest();
        request2.setDrugstoreId(drugstoreId);
        request2.setCardBarcode(cardNumber);
        VoucherProduct product1 = new VoucherProduct(null, null, null,
                new BigDecimal("30.00"), 1, 2015, drugstorePackIdGlo, "Перчатки хирург.");

        request2.setProducts(new VoucherProduct[] {product1});

        String responseBody2 = getResponseBody(request2);

        JSONObject jsonObject2 = new JSONObject(responseBody2);

        JSONObject row0 = jsonObject2.getJSONArray("rows").getJSONObject(0);
        assertFalse(row0.has("exactRefundSum"));
        assertFalse(row0.has("exactNonRefundSum"));
        assertFalse(row0.has("exactSumToPay"));

        assertFalse(jsonObject2.has("exactRefundSum"));
        assertFalse(jsonObject2.has("exactNonRefundSum"));
        assertFalse(jsonObject2.has("exactSumToPay"));
    }

    @Test
    public void recalculateWithDiscountLimitReachedTest() throws JsonParseException, JsonMappingException, IOException,
            NotFoundException, UnconsistentCheckDataException, ItemSizeLimitExceededException,
            UnknownPlanTypeException, InterruptedException, JSONException, OlekstraException, InstantiationException,
            IllegalAccessException, RuntimeException, DataIntegrityException, MessageSendException,
            ParserConfigurationException, TransformerException {

        // Used лимиты на дисконты (здесь "близко к пределу" - packCount)
        String period = LimitPeriod.MONTH.getCurrentPeriod(DateTime.now());
        DiscountLimitUsed dluArb = newDiscountLimitUsed(period, cardNumber, null, 0, limitArbId, discountIdUno,
                9, 9L, new BigDecimal("550.00"), new BigDecimal("550.00"));

        dynamodbService.putObjectOrDie(dluArb);

        // первый запрос - проверка карты
        VoucherRequest request1 = new VoucherRequest();
        request1.setDrugstoreId(drugstoreId);
        request1.setCardBarcode(cardNumber);
        request1.setProducts(new VoucherProduct[] {});
        String responseBody1 = getResponseBody(request1);

        JSONObject jsonObject1 = new JSONObject(responseBody1);

        // второй запрос с товарами для расчета скидок

        VoucherRequest request2 = new VoucherRequest();
        request2.setDrugstoreId(drugstoreId);
        request2.setCardBarcode(cardNumber);
        request2.setVoucherId(UUID.fromString(jsonObject1.getString("voucherId")));
        VoucherProduct product1 = new VoucherProduct(null, null, null,
                new BigDecimal("110.00"), 11, 2015, drugstorePackIdArb, "Арбидол");
        VoucherProduct product2 = new VoucherProduct(null, null, null,
                new BigDecimal("120.00"), 1, 2015, drugstorePackIdDic, "Диклофенак-10");

        request2.setProducts(new VoucherProduct[] {product1, product2});

        String responseBody2 = getResponseBody(request2);

        JSONObject jsonObject2 = new JSONObject(responseBody2);
        assertTrue(jsonObject2.getJSONObject("card").getBoolean("active"));
        assertFalse(jsonObject2.getString("voucherId").equalsIgnoreCase(EntityProxy.NULL_UUID));
        assertEquals(2, jsonObject2.getJSONArray("rows").length());

        JSONArray warnings = jsonObject2.getJSONArray("rows").getJSONObject(0)
                .getJSONArray("warnings");
        assertEquals(1, warnings.length());
        String msgPeriod = messageSource.getMessage("msg.limitPeriodMonth", new Object[] {}, null);
        String msg = messageSource.getMessage("msg.discountLimitReached", new Object[] {msgPeriod, limitArbId}, null);
        assertEquals(msg, warnings.getString(0));

    }

    @Test
    public void recalculateWithCardLimitCompensationTest() throws ItemSizeLimitExceededException, JsonParseException,
            JsonMappingException, IOException, NotFoundException, UnconsistentCheckDataException,
            UnknownPlanTypeException, InterruptedException, JSONException, OlekstraException, InstantiationException,
            IllegalAccessException, RuntimeException, DataIntegrityException, MessageSendException,
            ParserConfigurationException, TransformerException {

        // Used лимиты на дисконты - уже покупали Аспирин
        // (здесь "близко к пределу" - discountSum, для Аспирина должно не
        // хватить 20 и они будут взяты из лимита по карте)
        String periodWeek = LimitPeriod.WEEK.getCurrentPeriod(DateTime.now());
        String periodYear = LimitPeriod.YEAR.getCurrentPeriod(DateTime.now());
        DiscountLimitUsed dluAsp = newDiscountLimitUsed(periodWeek, cardNumber, null, 0, limitAspId, discountIdTre,
                5, 10L, new BigDecimal("330.00"), new BigDecimal("550.00"));
        DiscountLimitUsed dluAll = newDiscountLimitUsed(periodYear, cardNumber, null, 0, limitAllId, discountIdTre,
                18, 40L, new BigDecimal("1250.00"), new BigDecimal("5050.00"));

        dynamodbService.putObjectOrDie(dluAsp);
        dynamodbService.putObjectOrDie(dluAll);

        // первый запрос - проверка карты
        VoucherRequest request1 = new VoucherRequest();
        request1.setDrugstoreId(drugstoreId);
        request1.setCardBarcode(cardNumber);
        request1.setProducts(new VoucherProduct[] {});
        String responseBody1 = getResponseBody(request1);

        JSONObject jsonObject1 = new JSONObject(responseBody1);

        // второй запрос с товарами для расчета скидок

        VoucherRequest request2 = new VoucherRequest();
        request2.setDrugstoreId(drugstoreId);
        request2.setCardBarcode(cardNumber);
        request2.setVoucherId(UUID.fromString(jsonObject1.getString("voucherId")));
        VoucherProduct product1 = new VoucherProduct(null, null, null,
                new BigDecimal("220.00"), 1, 2015, drugstorePackIdAsp, "Аспирин франц.");
        VoucherProduct product2 = new VoucherProduct(null, null, null,
                new BigDecimal("250.00"), 1, 2015, drugstorePackIdBro, "Бромгекс.Берл.Х");

        request2.setProducts(new VoucherProduct[] {product1, product2});

        String responseBody2 = getResponseBody(request2);

        JSONObject jsonObject2 = new JSONObject(responseBody2);
        assertTrue(jsonObject2.getJSONObject("card").getBoolean("active"));
        assertFalse(jsonObject2.getString("voucherId").equalsIgnoreCase(EntityProxy.NULL_UUID));
        assertEquals(2, jsonObject2.getJSONArray("rows").length());

        // варнингов о нехватке Аспирина нет
        JSONArray warnings = jsonObject2.getJSONArray("rows").getJSONObject(0)
                .getJSONArray("warnings");
        assertEquals(0, warnings.length());

    }

    @Test
    public void recalculateWithCardLimitReachedTest() throws ItemSizeLimitExceededException, JSONException,
            JsonParseException, JsonMappingException, IOException, NotFoundException, UnconsistentCheckDataException,
            UnknownPlanTypeException, InterruptedException, OlekstraException, InstantiationException,
            IllegalAccessException, RuntimeException, DataIntegrityException, MessageSendException,
            ParserConfigurationException, TransformerException {

        // на балансе карты всего 10.
        // карта
        card = new Card(cardNumber, "000", false);
        card.setDiscountPlan(tarifId);
        card.setHolderName("Интегратор");
        card.setTelephoneNumber("0000");
        card.setLimitRemain(BigDecimal.TEN);

        dynamodbService.putObjectOrDie(card);

        // Used лимиты на дисконты - уже покупали Аспирин
        // (здесь "близко к пределу" - discountSum, для Аспирина должно не
        // хватить 20, но лимит по карте тоже исчерпан)
        String periodWeek = LimitPeriod.WEEK.getCurrentPeriod(DateTime.now());
        DiscountLimitUsed dluAsp = newDiscountLimitUsed(periodWeek, cardNumber, null, 0, limitAspId, discountIdTre,
                5, 10L, new BigDecimal("330.00"), new BigDecimal("550.00"));

        dynamodbService.putObjectOrDie(dluAsp);

        // первый запрос - проверка карты
        VoucherRequest request1 = new VoucherRequest();
        request1.setDrugstoreId(drugstoreId);
        request1.setCardBarcode(cardNumber);
        request1.setProducts(new VoucherProduct[] {});
        String responseBody1 = getResponseBody(request1);

        JSONObject jsonObject1 = new JSONObject(responseBody1);

        // второй запрос с товарами для расчета скидок

        VoucherRequest request2 = new VoucherRequest();
        request2.setDrugstoreId(drugstoreId);
        request2.setCardBarcode(cardNumber);
        request2.setVoucherId(UUID.fromString(jsonObject1.getString("voucherId")));
        VoucherProduct product1 = new VoucherProduct(null, null, null,
                new BigDecimal("220.00"), 1, 2015, drugstorePackIdAsp, "Аспирин франц.");

        request2.setProducts(new VoucherProduct[] {product1});

        String responseBody2 = getResponseBody(request2);

        JSONObject jsonObject2 = new JSONObject(responseBody2);
        assertTrue(jsonObject2.getJSONObject("card").getBoolean("active"));
        assertFalse(jsonObject2.getString("voucherId").equalsIgnoreCase(EntityProxy.NULL_UUID));
        assertEquals(1, jsonObject2.getJSONArray("rows").length());

        // варнинг об исчерпании лимита на дисконт для Аспирина
        JSONArray discountLimitWarnings = jsonObject2.getJSONArray("rows").getJSONObject(0)
                .getJSONArray("warnings");
        assertEquals(1, discountLimitWarnings.length());

        // варнинг о нехватке баланса по карте
        assertEquals(1, jsonObject2.getJSONArray("warnings").length());

    }

    @Test
    public void recalculateWithDiscountLimitAndCompensation2PackTest() throws ItemSizeLimitExceededException,
            JsonParseException,
            JsonMappingException, IOException, NotFoundException, UnconsistentCheckDataException,
            UnknownPlanTypeException, InterruptedException, JSONException, OlekstraException, InstantiationException,
            IllegalAccessException, RuntimeException, DataIntegrityException, MessageSendException,
            ParserConfigurationException, TransformerException {

        // на балансе карты всего 10.
        // карта
        card = new Card(cardNumber, "000", false);
        card.setDiscountPlan(tarifId);
        card.setHolderName("Интегратор");
        card.setTelephoneNumber("0000");
        card.setLimitRemain(new BigDecimal("2.00"));

        dynamodbService.putObjectOrDie(card);

        // Used лимиты на дисконты - уже покупали Йод
        // лимита по карте впритык
        String periodWeek = LimitPeriod.WEEK.getCurrentPeriod(DateTime.now());
        String periodYear = LimitPeriod.YEAR.getCurrentPeriod(DateTime.now());
        DiscountLimitUsed dluIod = newDiscountLimitUsed(periodWeek, cardNumber, null, 0, limitIodId, discountIdTre,
                2, 2L, new BigDecimal("20.00"), new BigDecimal("40.00"));
        DiscountLimitUsed dluAll = newDiscountLimitUsed(periodYear, cardNumber, null, 0, limitAllId, discountIdTre,
                997, 997L, new BigDecimal("2972.00"), new BigDecimal("6900.00"));

        dynamodbService.putObjectOrDie(dluIod);
        dynamodbService.putObjectOrDie(dluAll);

        // первый запрос - проверка карты
        VoucherRequest request1 = new VoucherRequest();
        request1.setDrugstoreId(drugstoreId);
        request1.setCardBarcode(cardNumber);
        request1.setProducts(new VoucherProduct[] {});
        String responseBody1 = getResponseBody(request1);

        JSONObject jsonObject1 = new JSONObject(responseBody1);

        // второй запрос с товарами для расчета скидок

        VoucherRequest request2 = new VoucherRequest();
        request2.setDrugstoreId(drugstoreId);
        request2.setCardBarcode(cardNumber);
        request2.setVoucherId(UUID.fromString(jsonObject1.getString("voucherId")));
        VoucherProduct product1 = new VoucherProduct(null, null, null,
                new BigDecimal("30.00"), 1, 2015, drugstorePackIdGlo, "Перчатки хирург.");
        VoucherProduct product2 = new VoucherProduct(null, null, null,
                new BigDecimal("20.00"), 1, 2015, drugstorePackIdIod, "Йода р-р 10 мл");

        request2.setProducts(new VoucherProduct[] {product1, product2});

        String responseBody2 = getResponseBody(request2);

        JSONObject jsonObject2 = new JSONObject(responseBody2);

        assertTrue(jsonObject2.getJSONObject("card").getBoolean("active"));
        assertFalse(jsonObject2.getString("voucherId").equalsIgnoreCase(EntityProxy.NULL_UUID));
        assertEquals(2, jsonObject2.getJSONArray("rows").length());

        // варнингов о нехватке Йода не должно быть
        JSONArray warnings = jsonObject2.getJSONArray("rows").getJSONObject(1)
                .getJSONArray("warnings");
        assertEquals(0, warnings.length());

    }

    @Test
    public void getPacksByClientPackNamesTest()
            throws ItemSizeLimitExceededException, AmazonServiceException, IllegalArgumentException,
            NotUpdatedException, InstantiationException, IllegalAccessException,
            IOException, InterruptedException, NotFoundException, OlekstraException, RuntimeException,
            JSONException, DataIntegrityException, MessageSendException, ParserConfigurationException,
            TransformerException {
        // на балансе карты всего 10.
        // карта
        card = new Card(cardNumber, "000", false);
        card.setDiscountPlan(tarifId);
        card.setHolderName("Интегратор");
        card.setTelephoneNumber("0000");
        card.setLimitRemain(new BigDecimal("2.00"));

        dynamodbService.putObjectOrDie(card);

        // первый запрос - проверка карты
        VoucherRequest request1 = new VoucherRequest();
        request1.setDrugstoreId(drugstoreId);
        request1.setCardBarcode(cardNumber);
        request1.setProducts(new VoucherProduct[] {});
        // портим clientPackId в запросе чтобы сработал поиск по индексу с
        // clientPackName
        VoucherProduct product1 = new VoucherProduct(null, null, null,
                new BigDecimal("30.00"), 1, 2015, drugstorePackIdIod + "bad", drugstorePackNameIod);
        request1.setProducts(new VoucherProduct[] {product1});
        String responseBody1 = getResponseBody(request1);
        JSONObject response = new JSONObject(responseBody1);
        JSONArray rows = (JSONArray) response.get("rows");
        assertNotNull(((JSONObject) rows.get(0)).get("name"));

    }

    @Test
    public void changePacksCountRecalculateTest() throws JSONException, AmazonServiceException,
            IllegalArgumentException, NotUpdatedException, IOException, InstantiationException, IllegalAccessException,
            InterruptedException, ItemSizeLimitExceededException, NotFoundException, OlekstraException,
            RuntimeException, DataIntegrityException, MessageSendException, ParserConfigurationException,
            TransformerException {

        // первый запрос - проверка карты
        VoucherRequest request1 = new VoucherRequest();
        request1.setDrugstoreId(drugstoreId);
        request1.setCardBarcode(cardNumber);
        request1.setProducts(new VoucherProduct[] {});
        String responseBody1 = getResponseBody(request1);

        JSONObject jsonObject1 = new JSONObject(responseBody1);

        String token1 = jsonObject1.getString("confirmationToken");

        // второй запрос с товаром (с лимитами) для расчета скидок

        VoucherRequest request2 = new VoucherRequest();
        request2.setDrugstoreId(drugstoreId);
        request2.setCardBarcode(cardNumber);
        request2.setVoucherId(UUID.fromString(jsonObject1.getString("voucherId")));
        VoucherProduct product1 = new VoucherProduct(null, null, null,
                new BigDecimal("110.00"), 2, 2015, drugstorePackIdArb, "Арбидол");

        request2.setProducts(new VoucherProduct[] {product1});

        String responseBody2 = getResponseBody(request2);

        JSONObject jsonObject2 = new JSONObject(responseBody2);

        String token2 = jsonObject2.getString("confirmationToken");

        assertTrue(jsonObject2.getJSONObject("card").getBoolean("active"));
        assertFalse(jsonObject2.getString("voucherId").equalsIgnoreCase(EntityProxy.NULL_UUID));
        assertEquals(1, jsonObject2.getJSONArray("rows").length());
        assertNotSame(token1, token2);

        // третий запрос - изменили количество
        // и номер карты!

        product1.setQuantity(3);
        VoucherRequest request3 = new VoucherRequest();
        request3.setDrugstoreId(drugstoreId);
        request3.setCardBarcode(cardNumber_);
        request3.setProducts(new VoucherProduct[] {product1});
        request3.setVoucherId(UUID.fromString(jsonObject2.getString("voucherId")));

        String responseBody3 = getResponseBody(request3);

        JSONObject jsonObject3 = new JSONObject(responseBody3);

        String token3 = jsonObject3.getString("confirmationToken");

        assertTrue(jsonObject3.getJSONObject("card").getBoolean("active"));
        assertFalse(jsonObject3.getString("voucherId").equalsIgnoreCase(EntityProxy.NULL_UUID));
        assertEquals(1, jsonObject3.getJSONArray("rows").length());
        assertNotSame(token2, token3);

    }
}
