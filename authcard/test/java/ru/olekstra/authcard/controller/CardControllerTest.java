package ru.olekstra.authcard.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.math.BigDecimal;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.mock.web.MockHttpServletRequest;

import ru.olekstra.authcard.dto.CardResponse;
import ru.olekstra.awsutils.DynamodbEntity;
import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.azure.service.QueueSender;
import ru.olekstra.common.service.CardLogService;
import ru.olekstra.common.service.DiscountService;
import ru.olekstra.common.service.XmlMessageService;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.Drugstore;
import ru.olekstra.domain.dto.VoucherRequest;
import ru.olekstra.exception.NotFoundException;
import ru.olekstra.exception.UnconsistentCheckDataException;
import ru.olekstra.service.CardOperationService;
import ru.olekstra.service.CardService;
import ru.olekstra.service.DrugstoreService;
import ru.olekstra.service.VoucherService;

@RunWith(MockitoJUnitRunner.class)
public class CardControllerTest {

    private CardService cardService;
    private CardOperationService requestService;
    private DrugstoreService drugstoreService;
    private CardController controller;
    private VoucherService voucherService;
    private MessageSource messageSource;
    private DiscountService discountService;
    private CardLogService cardLogService;
    private XmlMessageService xmlMessageService;
    private DynamodbService service;
    private QueueSender sender;

    @Before
    public void setup() {
        cardService = mock(CardService.class);
        requestService = mock(CardOperationService.class);
        drugstoreService = mock(DrugstoreService.class);
        voucherService = mock(VoucherService.class);
        discountService = mock(DiscountService.class);
        cardLogService = mock(CardLogService.class);
        xmlMessageService = mock(XmlMessageService.class);
        service = mock(DynamodbService.class);
        sender = mock(QueueSender.class);

        controller = new CardController(cardService, drugstoreService,
                requestService, voucherService, messageSource,
                discountService, cardLogService, xmlMessageService,
                service, sender);
    }

    @Test
    public void testGetCardStatusAsJson() throws JsonGenerationException,
            JsonMappingException, IOException, NotFoundException,
            NoSuchMessageException, UnconsistentCheckDataException,
            ItemSizeLimitExceededException {

        String cardNumber = "number";
        String drugstoreId = "drugstoreId";
        // String cardHolder = "holder";
        String drugstoreName = "Drugstore";
        String status = "Не активна";
        String planId = "discountPlanId";
        String planName = "discount Name";

        Drugstore drugstore = new Drugstore(drugstoreId);
        drugstore.setName(drugstoreName);

        Card card = new Card(cardNumber);
        // card.setHolderName(cardHolder);
        card.setDiscountPlan(planId);

        when(drugstoreService.getDrugstore(drugstoreId)).thenReturn(drugstore);
        when(cardService.getCard(cardNumber)).thenReturn(card);
        when(cardService.getDiscountPlanName(planId)).thenReturn(planName);

        CardResponse actual = controller.getCardStatusAsJson(cardNumber,
                drugstoreId);

        verify(drugstoreService).getDrugstore(drugstoreId);
        verify(cardService).getCard(cardNumber);
        verify(requestService).saveRequestRecord(drugstoreId, cardNumber,
                status, drugstoreName, drugstore.getTimeZone());

        assertNotNull(actual);
        assertEquals(cardNumber, actual.getNumber());
        assertEquals(status, actual.getStatus());
        // assertEquals(cardHolder, actual.getOwnerName());
        assertEquals(planName, actual.getDiscountPlanName());
    }

    @Test
    public void testGetCardStatusAsXml() throws JsonGenerationException,
            JsonMappingException, IOException, NotFoundException,
            NoSuchMessageException, UnconsistentCheckDataException,
            ItemSizeLimitExceededException {

        String cardNumber = "number";
        String drugstoreId = "drugstoreId";
        String cardHolder = "holder";
        String drugstoreName = "Drugstore";
        String status = "Не активна";

        Drugstore drugstore = new Drugstore(drugstoreId);
        drugstore.setName(drugstoreName);

        Card card = new Card(cardNumber);
        card.setHolderName(cardHolder);

        when(drugstoreService.getDrugstore(drugstoreId)).thenReturn(drugstore);
        when(cardService.getCard(cardNumber)).thenReturn(card);

        CardResponse actual = controller.getCardStatusAsXml(cardNumber, drugstoreId);

        verify(drugstoreService).getDrugstore(drugstoreId);
        verify(cardService).getCard(cardNumber);
        verify(requestService).saveRequestRecord(drugstoreId, cardNumber,
                status, drugstoreName, drugstore.getTimeZone());

        assertNotNull(actual);
        assertEquals(cardNumber, actual.getNumber());
        assertEquals(status, actual.getStatus());
        assertEquals(cardHolder, actual.getOwnerName());
    }

    @Test
    public void testRawJsonFromClientRequest() throws JsonParseException,
            JsonMappingException, IOException {
        String expected1 = "{\"voucherId\":\"85f882be-d21e-4c60-860c-0e1546be3ebb\",\"drugstoreId\":\"drugstore\",\"cardBarcode\":\"cardBarcode\",\"products\":[{\"ekt\":\"123\",\"barcode\":\"barcode1\",\"decNumber\":\"POCC RU-something\",\"price\":15.0,\"quantity\":1,\"expireYear\":2012,\"clientPackId\":\"123454321\",\"clientPackName\":\"Client pack name\"},{\"ekt\":\"456\",\"barcode\":\"barcode2\",\"decNumber\":\"POCC RU-something\",\"price\":15.0,\"quantity\":1,\"expireYear\":2012,\"clientPackId\":\"123454321\",\"clientPackName\":\"Client pack name\"}],\"sumToPay\":30.0}";

        VoucherRequest result = (VoucherRequest) DynamodbEntity.fromJson(
                expected1, VoucherRequest.class);

        assertEquals("85f882be-d21e-4c60-860c-0e1546be3ebb", result
                .getVoucherId().toString());
        assertEquals("drugstore", result.getDrugstoreId());
        assertEquals("cardBarcode", result.getCardBarcode());
        assertEquals("30.0", result.getSumToPay(), new BigDecimal("30.0"));
        assertEquals(2, result.getProducts().length);

        assertEquals(result.getProducts()[0].getEkt(), "123");
        assertEquals(result.getProducts()[0].getBarcode(), "barcode1");
        assertEquals(result.getProducts()[0].getDecNumber(),
                "POCC RU-something");
        assertEquals(result.getProducts()[0].getPrice(), new BigDecimal("15.0"));
        assertEquals(result.getProducts()[0].getQuantity(), 1);
        assertEquals(result.getProducts()[0].getExpireYear(), 2012);
        assertEquals(result.getProducts()[0].getClientPackId(), "123454321");
        assertEquals(result.getProducts()[0].getClientPackName(),
                "Client pack name");

        assertEquals(result.getProducts()[1].getEkt(), "456");
        assertEquals(result.getProducts()[1].getBarcode(), "barcode2");
        assertEquals(result.getProducts()[1].getDecNumber(),
                "POCC RU-something");
        assertEquals(result.getProducts()[1].getPrice(), new BigDecimal("15.0"));
        assertEquals(result.getProducts()[1].getQuantity(), 1);
        assertEquals(result.getProducts()[1].getExpireYear(), 2012);
        assertEquals(result.getProducts()[1].getClientPackId(), "123454321");
        assertEquals(result.getProducts()[0].getClientPackName(),
                "Client pack name");
    }

    @Test
    public void testSpringJsonConverter() throws IOException {

        String inputJson = "{\"voucherId\":\"5ec659bb-4ab7-4395-b75a-7efc0111b16d\",\"drugstoreId\":\"drugstore\",\"cardBarcode\":\"12345\",\"products\":[{\"ekt\":\"123\",\"barcode\":\"barcode1\",\"decNumber\":\"POCC RU-something\",\"price\":15.0,\"quantity\":1,\"expireYear\":2012,\"clientPackId\":\"123454321\",\"clientPackName\":\"Client pack name\"},{\"ekt\":\"456\",\"barcode\":\"barcode2\",\"decNumber\":\"POCC RU-something\",\"price\":15.0,\"quantity\":1,\"expireYear\":2012,\"clientPackId\":\"123454321\",\"clientPackName\":\"Client pack name\"}],\"sumToPay\":30.0}";

        VoucherRequest expectedObject = (VoucherRequest) DynamodbEntity
                .fromJson(inputJson, VoucherRequest.class);

        MappingJacksonHttpMessageConverter converter = new MappingJacksonHttpMessageConverter();
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setMethod("POST");
        request.setContentType("application/json");
        request.setContent(inputJson.getBytes());

        ServletServerHttpRequest message = new ServletServerHttpRequest(request);
        VoucherRequest actualObject = (VoucherRequest) converter.read(
                VoucherRequest.class, message);

        assertEquals(expectedObject.getCardBarcode(), actualObject
                .getCardBarcode());
        assertEquals(expectedObject.getDrugstoreId(), actualObject
                .getDrugstoreId());
        assertEquals(expectedObject.getSumToPay(), actualObject.getSumToPay());
        assertEquals(expectedObject.getVoucherId(), actualObject.getVoucherId());
        assertEquals(expectedObject.getProducts().length, actualObject
                .getProducts().length);

        assertEquals(expectedObject.getProducts()[0].getEkt(), actualObject
                .getProducts()[0].getEkt());
        assertEquals(expectedObject.getProducts()[0].getBarcode(), actualObject
                .getProducts()[0].getBarcode());
        assertEquals(expectedObject.getProducts()[0].getDecNumber(),
                actualObject.getProducts()[0].getDecNumber());
        assertEquals(expectedObject.getProducts()[0].getPrice(), actualObject
                .getProducts()[0].getPrice());
        assertEquals(expectedObject.getProducts()[0].getQuantity(),
                actualObject.getProducts()[0].getQuantity());
        assertEquals(expectedObject.getProducts()[0].getExpireYear(),
                actualObject.getProducts()[0].getExpireYear());
        assertEquals(expectedObject.getProducts()[0].getClientPackId(),
                actualObject.getProducts()[0].getClientPackId());
        assertEquals(expectedObject.getProducts()[0].getClientPackName(),
                actualObject.getProducts()[0].getClientPackName());

        assertEquals(expectedObject.getProducts()[1].getEkt(), actualObject
                .getProducts()[1].getEkt());
        assertEquals(expectedObject.getProducts()[1].getBarcode(), actualObject
                .getProducts()[1].getBarcode());
        assertEquals(expectedObject.getProducts()[1].getDecNumber(),
                actualObject.getProducts()[1].getDecNumber());
        assertEquals(expectedObject.getProducts()[1].getPrice(), actualObject
                .getProducts()[1].getPrice());
        assertEquals(expectedObject.getProducts()[1].getQuantity(),
                actualObject.getProducts()[1].getQuantity());
        assertEquals(expectedObject.getProducts()[1].getExpireYear(),
                actualObject.getProducts()[1].getExpireYear());
        assertEquals(expectedObject.getProducts()[1].getClientPackId(),
                actualObject.getProducts()[1].getClientPackId());
        assertEquals(expectedObject.getProducts()[1].getClientPackName(),
                actualObject.getProducts()[1].getClientPackName());
    }
}
