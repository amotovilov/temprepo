package ru.olekstra.authcard.controller;

import static org.mockito.Mockito.mock;

import java.util.Collections;

import org.dozer.DozerBeanMapper;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.io.ClassPathResource;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.common.dao.AppSettingsDao;
import ru.olekstra.common.dao.DrugstoreDao;
import ru.olekstra.common.helper.XmlMessageHelper;
import ru.olekstra.common.service.AppSettingsService;
import ru.olekstra.common.service.DateTimeService;
import ru.olekstra.common.service.XmlMessageService;

@Configuration
public class AwsTestConfig {

    @Bean
    public DynamodbService dynamodbService() {
        return new DynamodbService();
    }

    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setFallbackToSystemLocale(true);
        messageSource.setBasenames(new String[] {"messages"});
        messageSource.setUseCodeAsDefaultMessage(true);
        // messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

    @Bean
    public EhCacheManagerFactoryBean cacheManager() {
        EhCacheManagerFactoryBean cacheManager = new EhCacheManagerFactoryBean();
        cacheManager.setConfigLocation(new ClassPathResource("ru/olekstra/common/ehcache.xml"));
        return cacheManager;
    }

    @Bean(name = "org.dozer.Mapper")
    public DozerBeanMapper mapper() {
        DozerBeanMapper result = new org.dozer.DozerBeanMapper();
        result.setMappingFiles(Collections.singletonList("ru/olekstra/common/mappings.xml"));
        return result;
    }

    @Bean
    public AppSettingsService settingService() {
        return new AppSettingsService(new AppSettingsDao(dynamodbService()));
    }

    @Bean
    public DrugstoreDao drugstoreDao() {
        return mock(DrugstoreDao.class);
    }

    @Bean
    public DateTimeService dateService() {
        return new DateTimeService();
    }
    
    @Bean
    public XmlMessageService xmlMessageService() {
        return new XmlMessageService();
    }

    @Bean
    public XmlMessageHelper helper() {
        return new XmlMessageHelper();
    }
}
