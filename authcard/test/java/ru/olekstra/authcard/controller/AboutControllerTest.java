package ru.olekstra.authcard.controller;

import static org.springframework.test.web.ModelAndViewAssert.assertViewName;

import javax.servlet.ServletContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.servlet.HandlerAdapter;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter;

import ru.olekstra.awsutils.dynamodb.TableManagement;
import ru.olekstra.common.service.AboutService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class AboutControllerTest {

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private ServletContext servletContext;

    @Autowired
    private AboutController aboutController;

    @Autowired
    private AboutService aboutService;

    @Autowired
    private TableManagement tableService;

    @Autowired
    private HandlerAdapter handlerAdapter;

    private MockHttpServletRequest request;
    private MockHttpServletResponse response;

    @Before
    public void setUp() {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        handlerAdapter = applicationContext.getBean(AnnotationMethodHandlerAdapter.class);
    }

    @Test
    public void testIndex() throws Exception {
        request.setRequestURI("/about");
        ModelAndView mav = handlerAdapter.handle(request, response, aboutController);
        assertViewName(mav, "about");
    }

    @Configuration
    static class AboutControllerTestConfig {

        @Bean
        public ServletContext servletContext() {
            return new MockServletContext();
        }

        @Bean
        public TableManagement tableService() {
            return Mockito.mock(TableManagement.class);
        }

        @Bean
        public AboutService aboutService() {
            return Mockito.mock(AboutService.class);
        }

        @Bean
        public HandlerAdapter handlerAdapter() {
            return new AnnotationMethodHandlerAdapter();
        }

        @Bean
        public AboutController aboutController() {
            return new AboutController();
        }

    }
}
