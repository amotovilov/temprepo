package ru.olekstra.service;

import java.io.IOException;
import java.math.BigDecimal;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.dozer.Mapper;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.azure.service.QueueSender;
import ru.olekstra.common.service.CardLogService;
import ru.olekstra.common.service.DateTimeService;
import ru.olekstra.common.service.DiscountService;
import ru.olekstra.common.service.XmlMessageService;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.Drugstore;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.dto.VoucherProduct;
import ru.olekstra.domain.dto.VoucherRequest;
import ru.olekstra.domain.dto.VoucherResponseRow;
import ru.olekstra.exception.NotFoundException;
import ru.olekstra.processing.ConfirmProcessing;
import ru.olekstra.processing.DiscountRecalculateProcessing;
import ru.olekstra.processing.DummyProcessing;
import ru.olekstra.processing.IllegalCardProcessing;
import ru.olekstra.processing.Processing;
import ru.olekstra.processing.RefundProcessing;
import ru.olekstra.processing.UnknownDrugstoreProcessing;
import ru.olekstra.processing.VerificationConflictProcessing;

public class VoucherParser {

    private VoucherService voucherService;
    private DiscountService discountService;
    private CardLogService cardLogService;
    private XmlMessageService xmlMessageService;
    private CardService cardService;
    private DynamodbService service;
    private Mapper dozerBeanMapper;
    private DateTimeService dateService;
    private QueueSender sender;

    public VoucherParser(VoucherService voucherService,
            DiscountService discountService,
            CardLogService cardLogService,
            XmlMessageService xmlMessageService,
            CardService cardService,
            DynamodbService service,
            Mapper dozerBeanMapper,
            DateTimeService dateService, QueueSender sender) {
        this.voucherService = voucherService;
        this.discountService = discountService;
        this.cardLogService = cardLogService;
        this.xmlMessageService = xmlMessageService;
        this.cardService = cardService;
        this.service = service;
        this.dozerBeanMapper = dozerBeanMapper;
        this.dateService = dateService;
        this.sender = sender;
    }

    public Processing parse(VoucherRequest request) throws JsonParseException,
            JsonMappingException, IOException {

        Drugstore drugstore = null;
        Card card = null;
        ProcessingTransaction transaction = null;

        boolean needRecalculate = false;
        boolean needCreateNewTransaction = false;

        // проверяем наличие UUID в реквесте
        if (request.getVoucherId() != null) {
            // пытаемся взять транзакцию с этим UUID, временно "поверив" в
            // правильность переданных drugstoreId и cardBarcode
            transaction = voucherService.getProcessingTransaction(request);
            if (transaction != null) {
                // проверяем совпадение карты в реквесте и транзакции
                // и то, что она активна
                String requestCardNum = request.getCardBarcode();
                String transactionCardNum = transaction.getCard().getNumber();
                BigDecimal requestExactRefundSum = request.getExactRefundSum();
                BigDecimal requestExactSumToPay = request.getExactSumToPay();
                BigDecimal requestExactNonRefundSum = request.getExactNonRefundSum();
                BigDecimal transactionExactRefundSum = transaction.getExactRefundSum();
                BigDecimal transactionExactSumToPay = transaction.getExactSumToPay();
                BigDecimal transactionExactNonRefundSum = transaction.getExactNonRefundSum();
                boolean exactEquals = true;
                if (requestExactRefundSum != null && transactionExactRefundSum != null) {
                    if (!requestExactRefundSum.equals(transactionExactRefundSum)) {
                        exactEquals = false;
                    }
                } else if (requestExactRefundSum == null ^ transactionExactRefundSum == null) {
                    exactEquals = false;
                }
                if (requestExactSumToPay != null && transactionExactSumToPay != null) {
                    if (!requestExactSumToPay.equals(transactionExactSumToPay)) {
                        exactEquals = false;
                    }
                } else if (requestExactSumToPay == null ^ transactionExactSumToPay == null) {
                    exactEquals = false;
                }
                if (requestExactNonRefundSum != null && transactionExactNonRefundSum != null) {
                    if (!requestExactNonRefundSum.equals(transactionExactNonRefundSum)) {
                        exactEquals = false;
                    }
                } else if (requestExactNonRefundSum == null ^ transactionExactNonRefundSum == null) {
                    exactEquals = false;
                }

                if (requestCardNum.equals(transactionCardNum)
                        && transaction.getCard().isActive()) {
                    // проверяем, не закрыт ли уже чек
                    if (!transaction.isComplete()) {
                        // проверяем совпадают ли товары в реквесте и транзакции
                        // а также совпадают ли exact-поля в реквесте и транзакции
                        if (!isGoodsEquals(request, transaction) || !exactEquals)
                            needRecalculate = true;
                    } else {
                        if (request.getSumToPay().compareTo(
                                transaction.getSumToPay()) != 0)
                            // если в чеке и реквесте разные суммы, сообщаем
                            // кассиру об ошибке
                            return new VerificationConflictProcessing(
                                    voucherService);
                        else
                        // иначе проверим совпадение товаров в чеке и реквесте
                        if (isGoodsEquals(request, transaction))
                            return new ConfirmProcessing(transaction);
                        else
                            return new VerificationConflictProcessing(
                                    voucherService);
                    }
                } else
                    needCreateNewTransaction = true;
            } else
                needCreateNewTransaction = true;
        } else
            needCreateNewTransaction = true;

        if (needCreateNewTransaction) {
            // Проверяем, существует ли переданные аптека и карта
            drugstore = voucherService.getDrugstore(request.getDrugstoreId());
            if (drugstore != null && !drugstore.getDisabled()) {
                try {
                    card = cardService.getCard(request.getCardBarcode());
                } catch (NotFoundException nfe) {
                    return new IllegalCardProcessing(voucherService, card);
                }
                if (card.isActive()) {
                    // начинаем новую транзакцию
                    transaction = voucherService.createNewTransaction(drugstore, card, request);
                    needRecalculate = true;
                } else {
                    return new IllegalCardProcessing(voucherService, card);
                }
            } else
                return new UnknownDrugstoreProcessing(voucherService);
        }

        if (needRecalculate)
            return new DiscountRecalculateProcessing(voucherService,
                    transaction, discountService, card, cardLogService, service);
        else if (request.getSumToPay().compareTo(transaction.getSumToPay()) != 0)
            // если суммы в реквесте и транзакции не равны, просто возвращаем
            // транзакцию
            return new DummyProcessing(voucherService, transaction);
        else
            // иначе закрываем транзакцию
            return new RefundProcessing(voucherService, transaction,
                    cardLogService, xmlMessageService, discountService, cardService, 
                    dozerBeanMapper, dateService, sender);
    }

    private boolean isGoodsEquals(VoucherRequest request,
            ProcessingTransaction transaction) throws JsonParseException,
            JsonMappingException, IOException {
        // определяем только сам факт равенства или неравенства реквеста и
        // транзакции. что именно пересчитывать и какие скидки дополнительно
        // брать из БД, будем определять позже в DiscountRecalculateProcessing

        if (request.getProducts() == null && transaction.getRows() == null)
            return true;

        if ((request.getProducts() != null && transaction.getRows() != null))
            if (request.getProducts().length != transaction.getRows().size())
                return false;
            else {
                for (VoucherProduct rPack : request.getProducts()) {
                    boolean notFound = true;
                    for (VoucherResponseRow tPack : transaction.getRows()) {
                        if (tPack.getClientPackId().equals(
                                rPack.getClientPackId())
                                && tPack.getPrice().equals(rPack.getPrice())
                                && tPack.getQuantity() == rPack.getQuantity()) {
                            notFound = false;
                            break;
                        }
                    }
                    if (notFound) {
                        return false;
                    }
                }
                return true;
            }
        else
            return false;
    }
}
