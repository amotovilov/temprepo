package ru.olekstra.service;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.olekstra.authcard.dto.ConfirmRequest;
import ru.olekstra.authcard.dto.ConfirmResponse;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.awsutils.exception.NotUpdatedException;
import ru.olekstra.awsutils.exception.OlekstraException;
import ru.olekstra.azure.model.MessageSendException;
import ru.olekstra.azure.service.QueueSender;
import ru.olekstra.common.service.CardLogService;
import ru.olekstra.common.service.DateTimeService;
import ru.olekstra.common.service.DiscountService;
import ru.olekstra.common.service.XmlMessageService;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.dto.VoucherRequest;
import ru.olekstra.exception.DataIntegrityException;
import ru.olekstra.exception.NotFoundException;
import ru.olekstra.exception.UnconsistentCheckDataException;
import ru.olekstra.processing.Processing;
import ru.olekstra.processing.RefundProcessing;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.util.json.JSONException;

@Service
public class RefundService {

    private VoucherService voucherService;
    private CardLogService cardLogService;
    private XmlMessageService xmlMessageService;
    private CardService cardService;
    private DiscountService discountService;
    private Mapper dozerBeanMapper;
    private DateTimeService dateService;
    private QueueSender sender;

    @Autowired
    public RefundService(VoucherService voucherService,
            CardLogService cardLogService, XmlMessageService xmlMessageService, CardService cardService,
            DiscountService discountService, Mapper dozerBeanMapper, DateTimeService dateService,
            QueueSender sender) {
        this.voucherService = voucherService;
        this.cardLogService = cardLogService;
        this.xmlMessageService = xmlMessageService;
        this.cardService = cardService;
        this.discountService = discountService;
        this.dozerBeanMapper = dozerBeanMapper;
        this.dateService = dateService;
        this.sender = sender;
    }

    // to make Processing object visible in tests
    protected Processing newRefundProcessing(VoucherService voucherService, ProcessingTransaction transaction,
            CardLogService cardLogService, XmlMessageService xmlMessageService, DiscountService discountService,
            CardService cardService, Mapper dozerBeanMapper, DateTimeService dateService, QueueSender sender) {

        return new RefundProcessing(voucherService, transaction, cardLogService, xmlMessageService,
                discountService, cardService, dozerBeanMapper, dateService, sender);
    }

    public ConfirmResponse getConfirmResponse(ConfirmRequest request, VoucherRequest voucherRequest,
            ProcessingTransaction transaction) throws UnconsistentCheckDataException, JsonGenerationException,
            JsonMappingException, AmazonServiceException, IllegalArgumentException, NotUpdatedException, IOException,
            InterruptedException, ItemSizeLimitExceededException, NotFoundException, OlekstraException,
            InstantiationException, IllegalAccessException, RuntimeException, DataIntegrityException, JSONException, MessageSendException, ParserConfigurationException, TransformerException {

        if (transaction.getAuthCode() != null)
            return new ConfirmResponse(transaction.getVoucherId(), transaction.getAuthCode());
        else if (transaction.getConfirmationToken() != null
                &&
                !transaction.getConfirmationToken().equals(request.getConfirmationToken()))
            throw new UnconsistentCheckDataException("ConfirmationToken mismatch");

        Processing processing = this.newRefundProcessing(voucherService, transaction,
                cardLogService, xmlMessageService, discountService, cardService,
                dozerBeanMapper, dateService, sender);

        ProcessingTransaction refundedTransaction = processing.doProcessing(voucherRequest);
        return new ConfirmResponse(refundedTransaction.getVoucherId(), refundedTransaction.getAuthCode());
    }

}
