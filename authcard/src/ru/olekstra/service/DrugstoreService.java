package ru.olekstra.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.domain.Drugstore;
import ru.olekstra.exception.NotFoundException;

@Service
public class DrugstoreService {

    private DynamodbService service;

    @Autowired
    public DrugstoreService(DynamodbService service) {
        this.service = service;
    }

    /**
     * Get a drugstore by specific number.
     * 
     * @param id drugstore number
     * @return found drugstore
     * @throws NotFoundException
     */
    public Drugstore getDrugstore(String id) throws NotFoundException {

        Drugstore drugstore = service.getObject(Drugstore.class, id);
        if (drugstore == null) {
            throw new NotFoundException("Drugstore not found by id " + id);
        }
        return drugstore;
    }
}
