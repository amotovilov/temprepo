package ru.olekstra.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.common.service.CardLogService;
import ru.olekstra.common.service.DateTimeService;
import ru.olekstra.common.service.SmsService;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.CardLog;
import ru.olekstra.domain.dto.CardLogType;
import ru.olekstra.exception.NotFoundException;

import com.amazonaws.AmazonServiceException;

@Service
public class CardOperationService {

    private SmsService smsService;
    private DynamodbService dbService;
    private CardService cardService;
    private MessageSource messageSource;
    private CardLogService cardLogService;
    private DateTimeService dateService;

    private static final Logger LOGGER = Logger
            .getLogger(CardOperationService.class);

    @Autowired
    public CardOperationService(SmsService smsService,
            DynamodbService dynamoService, CardService cardService,
            MessageSource messageSource, CardLogService cardLogService,
            DateTimeService dateService) {

        this.smsService = smsService;
        this.dbService = dynamoService;
        this.cardService = cardService;
        this.messageSource = messageSource;
        this.cardLogService = cardLogService;
        this.dateService = dateService;
    }

    public void saveRequestRecord(String drugstoreId, String cardNumber,
            String status, String drugstoreName, String timeZone)
            throws JsonGenerationException, JsonMappingException, IOException,
            ItemSizeLimitExceededException {

        CardLog log = cardLogService.createCardLog(cardNumber, drugstoreName + ": " + status,
                dateService.createDateTimeWithZone(timeZone),
                CardLogType.REQUEST, false);
        cardLogService.saveCardLog(log);
    }

    public void saveTransactionRecords(String cardNumber,
            List<CardLog> recordList) throws JsonGenerationException,
            JsonMappingException, IOException, ItemSizeLimitExceededException,
            AmazonServiceException, IllegalArgumentException, InterruptedException {

        cardLogService.saveCardLog(recordList);
    }

    public BigDecimal saveLimit(String drugstoreId, Card card, BigDecimal discountSum)
            throws JsonGenerationException, JsonMappingException, IOException,
            ItemSizeLimitExceededException, NotFoundException {

        if (card == null) {
            LOGGER.debug("Card is null. Drugstore id#" + drugstoreId);
            return null;
        }

        BigDecimal limit = card.getLimitRemain() == null ? new BigDecimal(0) : card.getLimitRemain();
        BigDecimal newLimit = limit.subtract(discountSum).setScale(2, RoundingMode.HALF_UP);
        boolean success = cardService.updateCardLimit(card.getNumber(), limit, newLimit);

        if (!success) {
            LOGGER.debug("Reload card to update limit value on card #" + card.getNumber());

            card = dbService.getObject(Card.class, card.getNumber());
            limit = card.getLimitRemain() == null ? new BigDecimal(0) : card.getLimitRemain();
            newLimit = limit.subtract(discountSum);
            success = cardService.updateCardLimit(card.getNumber(), limit, newLimit);

            if (!success) {
                LOGGER.debug("Failure to update limit value on card #" + card.getNumber());
            }
        }

        CardLog cardLimitLog = cardLogService.createCardLog(
                card.getNumber(), messageSource.getMessage("msg.cardlimitnote",
                        new Object[] {discountSum.toString()}, null),
                dateService.createDrugstoreDate(drugstoreId), CardLogType.LIMIT,
                false);

        cardLogService.saveCardLog(cardLimitLog);

        return newLimit;
    }

    public boolean sendSmsNotification(String phoneNumber, String notification) {

        if (phoneNumber != null && !phoneNumber.isEmpty()) {
            try {
                return smsService.send(phoneNumber, notification);
            } catch (ClientProtocolException e) {
                LOGGER.debug(e.getLocalizedMessage(), e);
            } catch (IOException e) {
                LOGGER.debug(e.getLocalizedMessage(), e);
            }
        }
        return false;
    }

    public boolean sendSmsNotifications(String phoneNumber,
            List<String> notifications) {

        List<Map<String, String>> recipients = new ArrayList<Map<String, String>>(
                notifications.size());

        for (String value : notifications) {
            Map<String, String> recipient = new HashMap<String, String>();
            recipient.put(phoneNumber, value);
            recipients.add(recipient);
        }

        if (phoneNumber != null && !phoneNumber.isEmpty()) {
            try {
                smsService.sendMultpleMessages(recipients);
                return true;
            } catch (ClientProtocolException e) {
                LOGGER.debug(e.getLocalizedMessage(), e);
            } catch (IOException e) {
                LOGGER.debug(e.getLocalizedMessage(), e);
            }
        }
        return false;
    }
}