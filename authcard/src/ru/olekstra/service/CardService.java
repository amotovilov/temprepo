package ru.olekstra.service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.common.annotation.CardAudit;
import ru.olekstra.common.annotation.CardNumber;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.DiscountPlan;
import ru.olekstra.exception.NotFoundException;

@Service
public class CardService {

    private DynamodbService service;

    public CardService() {

    }

    @Autowired
    public CardService(DynamodbService service) {
        this.service = service;
    }

    /**
     * Get a card by specific number.
     * 
     * @param number card number
     * @return found card
     * @throws NotFoundException
     */
    public Card getCard(String number) throws NotFoundException {

        Card card = service.getObject(Card.class, number);
        if (card == null) {
            throw new NotFoundException("Card not found by number " + number);
        }
        return card;
    }

    public String getDiscountPlanName(String id) {
        DiscountPlan dp = service.getObject(DiscountPlan.class, id);
        if (dp == null) {
            return null;
        }
        return dp.getPlanName();
    }

    @CardAudit
    public boolean updateCardLimit(@CardNumber String cardNumber, BigDecimal expectedValue,
            BigDecimal newValue) {

        return service.updateAttributeWithCondition(Card.TABLE_NAME,
                cardNumber, Card.FLD_LIMIT_REMAIN, expectedValue.toString(),
                newValue.toString());
    }
}
