package ru.olekstra.processing;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;

import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.awsutils.exception.NotUpdatedException;
import ru.olekstra.awsutils.exception.OlekstraException;
import ru.olekstra.azure.model.MessageSendException;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.dto.VoucherRequest;
import ru.olekstra.exception.DataIntegrityException;
import ru.olekstra.exception.NotFoundException;

import com.amazonaws.AmazonServiceException;

public interface Processing {

    ProcessingTransaction doProcessing(VoucherRequest request)
            throws JsonGenerationException, JsonMappingException, IOException,
            AmazonServiceException, IllegalArgumentException,
            InterruptedException, ItemSizeLimitExceededException,
            NotUpdatedException, NotFoundException, OlekstraException,
            InstantiationException, IllegalAccessException, RuntimeException, DataIntegrityException,
            MessageSendException, ParserConfigurationException, TransformerException;

}
