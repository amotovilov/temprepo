package ru.olekstra.processing;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import ru.olekstra.awsutils.DynamodbEntity;
import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.awsutils.exception.OlekstraException;
import ru.olekstra.common.service.CardLogService;
import ru.olekstra.common.service.DiscountService;
import ru.olekstra.domain.AuthCodeIndex;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.CardLog;
import ru.olekstra.domain.DiscountLimitUsed;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.dto.DiscountLimitUsedDetail;
import ru.olekstra.domain.dto.DiscountLimitUsedTotal;
import ru.olekstra.domain.dto.VoucherRequest;
import ru.olekstra.domain.dto.VoucherResponseRow;
import ru.olekstra.exception.DataIntegrityException;
import ru.olekstra.service.VoucherService;

public class DiscountRecalculateProcessing implements Processing {

    private VoucherService voucherService;
    private ProcessingTransaction transaction;
    private DiscountService discountService;
    private Card card;
    private CardLogService cardLogService;
    private DynamodbService service;

    public DiscountRecalculateProcessing(VoucherService voucherService,
            ProcessingTransaction transaction, DiscountService service,
            Card card, CardLogService cardLogService,
            DynamodbService dynamodbService) {
        this.voucherService = voucherService;
        this.transaction = transaction;
        this.discountService = service;
        this.card = card;
        this.cardLogService = cardLogService;
        this.service = dynamodbService;
    }

    @Override
    public ProcessingTransaction doProcessing(
            VoucherRequest request) throws JsonParseException,
            JsonMappingException, IOException, RuntimeException,
            InterruptedException, OlekstraException,
            ItemSizeLimitExceededException, InstantiationException, IllegalAccessException, DataIntegrityException {

        boolean processExactFields = false;

        if (request.getProducts().length > 0) {
            // сравним строки transaction и request.
            List<VoucherResponseRow> newRows = voucherService.compareAndFillRows(transaction, request);

            System.out.println(" ");
            System.out.println("================== NEW ROWS ================");
            System.out.println("========= AFTER compareAndFillRows =========");
            if (newRows != null) {
                for (VoucherResponseRow row : newRows)
                    System.out.println("ClientId:  " + row.getClientPackId() + "  Id: " + row.getId());
            }

            // загрузить данные о товарах для новых строк
            voucherService.getPacks(newRows, request, transaction.getFormularId());
            System.out.println("=========== AFTER getPacks =================");
            if (newRows != null) {
                for (VoucherResponseRow row : newRows)
                    System.out.println("ClientId:  " + row.getClientPackId() + "  Id: " + row.getId());
            }

            // загрузить данные о дисконтах и лимитах (если они есть)
            transaction = discountService.getDiscountsAndLimits(transaction);

            // считываем использованные лимиты
            List<DiscountLimitUsed> usedLimitList = discountService.getUsedLimits(
                    transaction.getCard().getNumber(),
                    transaction.getStartTime(),
                    transaction.getDiscountLimits(),
                    transaction.getDiscountLimitUsedPreviousTotal());

            // объединим выбранные UsedLimits с уже загруженными, если они были
            List<DiscountLimitUsedTotal> dlutList = discountService.unionPreviousLimitUsedTotal(
                    transaction.getCard().getNumber(),
                    transaction.getStartTime(),
                    transaction.getDiscountLimits(),
                    usedLimitList,
                    transaction.getDiscountLimitUsedPreviousTotal());

            transaction.setDiscountLimitUsedPreviousTotal(dlutList);
            // все посчитанные UsedLimit-ы необходимо обнулить, на случай, если
            // они остались с предыдущего расчета
            transaction.setDiscountLimitUsedCurrentDetail(new ArrayList<DiscountLimitUsedDetail>());
            transaction.setDiscountLimitUsedNewTotal(new ArrayList<DiscountLimitUsedTotal>());

            // лимит по карте
            BigDecimal cardLimit = voucherService.getCardLimit(transaction.getCard().getNumber());

            // расчет
            voucherService.calculateDiscounts(transaction, cardLimit);

            if (request.getExactNonRefundSum() != null
                    || request.getExactRefundSum() != null
                    || request.getExactSumToPay() != null) {
                voucherService.checkExactFields(transaction);
                processExactFields = true;
            }

            // OLP-747. После каждого пересчета обновляем CalculationId
            transaction.setConfirmationToken(UUID.randomUUID());

            // загрузить синонимы для товаров, у которых не оказалось скидок
            discountService.getDiscountReplaces(transaction);

            // проверяем наличие достаточного количества средств на указанные в
            // чеке товары. при нехватке средств отправляем уведомление
            // (cardLimit теперь нужен только в СМС-сообщении)
            if (transaction.getWarnings() != null) {
                transaction.getWarnings().clear();
            }
            voucherService.checkCardLimitIsEnough(transaction, cardLimit);

            // Отправляем в СМС все предупреждения по строкам, если они есть
            voucherService.sendRowWarningsToSms(transaction);

        } else {
            // очистка transaction от возможно старых строк, сброс данных
            // об использовании лимитов
            transaction.setRows(null);
            transaction.setDiscountLimitUsedPreviousTotal(new ArrayList<DiscountLimitUsedTotal>());
            transaction.setDiscountLimitUsedCurrentDetail(new ArrayList<DiscountLimitUsedDetail>());
            transaction.setDiscountLimitUsedNewTotal(new ArrayList<DiscountLimitUsedTotal>());
            transaction.setConfirmationToken(null);
        }

        // если ещё не записывали CardLog (то есть транзакция только что
        // создана)
        boolean saveCardLog = false;
        CardLog cardLog = null;
        if (transaction.getCardLogId() == null) {
            cardLog = cardLogService.createNewTransactionTypeCardLog(transaction, card);
            saveCardLog = cardLogService.saveCardLog(cardLog);
        }
        if (saveCardLog)
            transaction.setCardLogId(cardLog.getCardLogId());

        // заполняем exact-поля значениями переданными в запросе
        if (processExactFields) {
            voucherService.sumExactFields(transaction);
        }
        service.putObjectOrDie(transaction);

        AuthCodeIndex index = new AuthCodeIndex(transaction.getDate(), transaction.getAuthCode(),
                transaction.getDrugstoreId(), transaction.getRangeKey());

        service.putObjectOrDie(index);

        String transactionJsonString = DynamodbEntity.toJson(transaction);

        System.out.println("");
        System.out.println("=================================================");
        System.out.println(transactionJsonString);
        System.out.println("=================================================");
        System.out.println("");
        return transaction;
    }
}
