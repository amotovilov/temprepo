package ru.olekstra.processing;

import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.dto.VoucherRequest;

public class ConfirmProcessing implements Processing {

    private ProcessingTransaction transaction;

    public ConfirmProcessing(ProcessingTransaction transaction) {
        this.transaction = transaction;
    }

    @Override
    public ProcessingTransaction doProcessing(VoucherRequest request) {
        return transaction;
    }

}
