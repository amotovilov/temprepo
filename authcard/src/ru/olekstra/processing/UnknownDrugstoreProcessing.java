package ru.olekstra.processing;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;

import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.dto.VoucherRequest;
import ru.olekstra.service.VoucherService;

public class UnknownDrugstoreProcessing implements Processing {

    private VoucherService voucherService;

    public UnknownDrugstoreProcessing(VoucherService voucherService) {
        this.voucherService = voucherService;
    }

    @Override
    public ProcessingTransaction doProcessing(VoucherRequest request)
            throws JsonGenerationException, JsonMappingException, IOException {
        return voucherService.getErrorProcessingTransaction(request,
                "msg.drugstoreNotFound.install", null);
    }

}
