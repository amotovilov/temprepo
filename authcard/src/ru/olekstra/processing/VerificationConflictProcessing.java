package ru.olekstra.processing;

import java.io.IOException;
import java.util.UUID;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;

import ru.olekstra.domain.EntityProxy;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.dto.VoucherRequest;
import ru.olekstra.service.VoucherService;

public class VerificationConflictProcessing implements Processing {

    private VoucherService voucherService;

    public VerificationConflictProcessing(VoucherService voucherService) {
        this.voucherService = voucherService;
    }

    @Override
    public ProcessingTransaction doProcessing(VoucherRequest request)
            throws JsonGenerationException, JsonMappingException, IOException {

        ProcessingTransaction transaction = voucherService
                .getErrorProcessingTransaction(request, "msg.checkiscompleted",
                        new Object[] {request.getCardBarcode()});

        transaction.setVoucherId(UUID.fromString(EntityProxy.NULL_UUID));
        return transaction;
    }

}
