package ru.olekstra.processing;

import java.io.IOException;
import java.util.UUID;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;

import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.EntityProxy;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.dto.VoucherRequest;
import ru.olekstra.service.VoucherService;

public class IllegalCardProcessing implements Processing {

    private VoucherService voucherService;
    private Card card;

    public IllegalCardProcessing(VoucherService voucherService, Card card) {
        super();
        this.voucherService = voucherService;
        this.card = card;
    }

    @Override
    public ProcessingTransaction doProcessing(VoucherRequest request)
            throws JsonGenerationException, JsonMappingException, IOException {

        String message = "msg.cardunknownproblem";
        if (card == null)
            message = "msg.thiscardnotfound";
        else {
            if (!card.isActive())
                message = "msg.cardnotactive";
            if (card.getDisabled())
                message = "msg.cardisdisabled";
        }

        ProcessingTransaction transaction = voucherService
                .getErrorProcessingTransaction(request, message,
                        new Object[] {request.getCardBarcode()});
        try {
            voucherService.saveProcessingTransaction(transaction);
        } catch (ItemSizeLimitExceededException e) {
            e.printStackTrace();
        }

        transaction.setVoucherId(UUID.fromString(EntityProxy.NULL_UUID));
        return transaction;
    }

}
