package ru.olekstra.processing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.dozer.Mapper;
import org.joda.time.DateTime;

import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.awsutils.exception.OlekstraException;
import ru.olekstra.azure.model.IMessage;
import ru.olekstra.azure.model.MessageSendException;
import ru.olekstra.azure.service.QueueSender;
import ru.olekstra.common.helper.DozerHelper;
import ru.olekstra.common.service.CardLogService;
import ru.olekstra.common.service.DateTimeService;
import ru.olekstra.common.service.DiscountService;
import ru.olekstra.common.service.XmlMessageService;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.CardLog;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.Refund;
import ru.olekstra.domain.dto.DiscountLimitRemaining;
import ru.olekstra.domain.dto.DiscountLimitUsedDto;
import ru.olekstra.domain.dto.DiscountLimitUsedTotal;
import ru.olekstra.domain.dto.VoucherRequest;
import ru.olekstra.exception.NotFoundException;
import ru.olekstra.service.CardService;
import ru.olekstra.service.VoucherService;

public class RefundProcessing implements Processing {

    private VoucherService voucherService;
    private ProcessingTransaction transaction;
    private CardLogService cardLogService;
    private XmlMessageService xmlMessageService;
    private CardService cardService;
    private DiscountService discountService;
    private Mapper dozerBeanMapper;
    private DateTimeService dateService;
    private QueueSender sender;

    public RefundProcessing(VoucherService voucherService,
            ProcessingTransaction transaction,
            CardLogService cardLogService,
            XmlMessageService xmlMessageService,
            DiscountService discountService,
            CardService cardService,
            Mapper dozerBeanMapper,
            DateTimeService dateService,
            QueueSender sender) {
        this.voucherService = voucherService;
        this.transaction = transaction;
        this.cardLogService = cardLogService;
        this.xmlMessageService = xmlMessageService;
        this.discountService = discountService;
        this.cardService = cardService;
        this.dozerBeanMapper = dozerBeanMapper;
        this.dateService = dateService;
        this.sender = sender;
    }

    @Override
    public ProcessingTransaction doProcessing(VoucherRequest request)
            throws JsonGenerationException, JsonMappingException, IOException,
            InterruptedException, ItemSizeLimitExceededException,
            NotFoundException, InstantiationException, IllegalAccessException,
            RuntimeException, OlekstraException, MessageSendException, ParserConfigurationException,
            TransformerException {

        // закрываем чек
        String drugstoreAuthCode = voucherService.getNextDrugstoreCode(request.getDrugstoreId());
        transaction.setAuthCode(voucherService.calcUserAuthCode(drugstoreAuthCode));
        transaction.setComplete(true);

        // Подготавливаем данные по позициям
        DateTime period = dateService.createDrugstoreDate(transaction.getDrugstoreId());
        transaction.setCompleteTime(period);
        List<CardLog> cardRecordsList = voucherService.setRecords(transaction);
        List<Refund> refunds = voucherService.setRefunds(transaction, period);

        // сохраняем данные по завершенной транзакции
        voucherService.saveRefunds(refunds);
        // сохраняем лог записи и отправляем оповещение
        voucherService.sendAndSaveRefundRecords(transaction, cardRecordsList);

        // обновляем баланс
        voucherService.updateBalance(transaction);
        // сохраняем и закрываем транзакцию
        voucherService.saveProcessingTransaction(transaction);
        // сохраняем данные по использованию лимитов
        List<DiscountLimitUsedDto> limitList = new ArrayList<DiscountLimitUsedDto>();
        if (transaction.getDiscountLimitUsedCurrentDetail() != null) {
            limitList.addAll(transaction.getDiscountLimitUsedCurrentDetail());
        }
        if (transaction.getDiscountLimitUsedNewTotal() != null) {
            limitList.addAll(transaction.getDiscountLimitUsedNewTotal());
        }
        discountService.updateTotalAndSave(limitList);

        IMessage completeMessage = xmlMessageService.createTransactionCompleteMessage(transaction);
        completeMessage.setLabel(XmlMessageService.TRANSACTION_COMPLETE);
        sender.send(completeMessage);

        // сохраняем CardLog
        CardLog cardLog = cardLogService.createCompletedTransactionCardLog(transaction);
        cardLogService.saveCardLog(cardLog);

        // сохраняем остаточные лимиты
        Card card = cardService.getCard(transaction.getCard().getNumber());

        List<DiscountLimitUsedTotal> totalLimits =
                DozerHelper.map(dozerBeanMapper, transaction.getDiscountLimitUsedNewTotal(), DiscountLimitUsedTotal.class);
        List<DiscountLimitRemaining> limits = discountService.
                calculateRemainingLimits(transaction.getDiscountLimits(), totalLimits,
                        transaction.getCard().getNumber());

        if (limits != null) {
            for (DiscountLimitRemaining limit : limits) {
                CardLog limitCardLock = cardLogService.createLimitRemainCardLog(card.getNumber(), limit,
                        dateService.createDrugstoreDate(transaction.getDrugstoreId()));
                cardLogService.saveCardLog(limitCardLock);
            }
        }

        // сохраняем индексные значения
        voucherService.saveRefundIndex(period,
                transaction.getDrugstoreId(), transaction.getRangeKey(),
                transaction.getAuthCode());

        return transaction;
    }

}
