package ru.olekstra.authcard.dto;

import java.util.UUID;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ConfirmRequest {

    private String drugstoreId;
    private UUID voucherId;
    private UUID confirmationToken;

    public ConfirmRequest() {
    }

    public ConfirmRequest(String drugstoreId, UUID voucherId, UUID confirmationToken) {
        this.drugstoreId = drugstoreId;
        this.voucherId = voucherId;
        this.confirmationToken = confirmationToken;
    }

    @XmlElement
    public String getDrugstoreId() {
        return drugstoreId;
    }

    public void setDrugstoreId(String drugstoreId) {
        this.drugstoreId = drugstoreId;
    }

    @XmlElement
    public UUID getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(UUID voucherId) {
        this.voucherId = voucherId;
    }

    @XmlElement
    public UUID getConfirmationToken() {
        return confirmationToken;
    }

    public void setConfirmationToken(UUID confirmationToken) {
        this.confirmationToken = confirmationToken;
    }

}
