package ru.olekstra.authcard.dto;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import javax.xml.bind.DatatypeConverter;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.dto.VoucherResponseCard;
import ru.olekstra.domain.dto.VoucherResponsePackReplace;
import ru.olekstra.domain.dto.VoucherResponseRow;

import com.amazonaws.util.XMLWriter;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONWriter;

public class ClientResponseFormatter {

    public static final String toJSON(ProcessingTransaction transaction)
            throws JSONException, JsonParseException, JsonMappingException,
            IOException {
        Writer writer = new StringWriter();
        JSONWriter jsonWriter = new JSONWriter(writer);

        jsonWriter.object().key("voucherId").value(transaction.getVoucherId())
                .key("drugstoreId").value(transaction.getDrugstoreId());
        if (transaction.getConfirmationToken() != null)
            jsonWriter.key("confirmationToken").value(transaction.getConfirmationToken().toString());
        else
            jsonWriter.key("confirmationToken").value("");

        VoucherResponseCard card = transaction.getCard();
        if (card != null) {
            // конвертируем VoucherResponseCard
            jsonWriter.key("card").object().key("number").value(
                    card.getNumber()).key("owner").value(card.getOwner()).key(
                    "active").value(card.isActive()).key("comment").value(
                    card.getComment()).key("planId").value(card.getPlanId())
                    .key("planName").value(card.getPlanName());

            // конвертируем список checks
            jsonWriter.key("checks").array();
            if (card.getChecks() != null && card.getChecks().length > 0) {

                for (String value : card.getChecks()) {
                    jsonWriter.value(value);
                }
            }
            jsonWriter.endArray(); // checks

            jsonWriter.endObject(); // card
        }

        // продолжаем конвертировать транзакцию
        jsonWriter.key("sumWithoutDiscount").value(
                transaction.getSumWithoutDiscount().toString()).key(
                "discountSum").value(transaction.getDiscountSum().toString())
                .key("sumToPay").value(transaction.getSumToPay().toString());

        // конвертируем список warnings
        jsonWriter.key("warnings").array();
        if (transaction.getWarnings() != null
                && transaction.getWarnings().size() > 0) {

            for (String value : transaction.getWarnings()) {
                jsonWriter.value(value);
            }
        }
        jsonWriter.endArray();

        // конвертируем список VoucherResponseRow
        jsonWriter.key("rows").array();
        if (transaction.getRows() != null && transaction.getRows().size() > 0) {

            for (VoucherResponseRow row : transaction.getRows()) {
                jsonWriter.object().key("name").value(row.getName()).key("id")
                        .value(row.getId()).key("clientPackId").value(
                                row.getClientPackId()).key("quantity").value(
                                row.getQuantity()).key("price").value(
                                row.getPrice().toString()).key("sumToPay")
                        .value(row.getSumToPay().toString()).key("discountSum")
                        .value(row.getDiscountSum().toString()).key(
                                "discountPercent").value(
                                row.getDiscountPercent()).key("refundSum")
                        .value(row.getRefundSum().toString()).key(
                                "sumWithoutDiscount").value(
                                row.getSumWithoutDiscount().toString());
                if (row.getExactRefundSum() != null) {
                    jsonWriter.key("exactRefundSum").value(row.getExactRefundSum());
                }
                if (row.getExactNonRefundSum() != null) {
                    jsonWriter.key("exactNonRefundSum").value(row.getExactNonRefundSum());
                }
                if (row.getExactSumToPay() != null) {
                    jsonWriter.key("exactSumToPay").value(row.getExactSumToPay());
                }
                // конвертируем массив VoucherResponsePackReplace
                jsonWriter.key("replacePacks").array();
                if (row.getReplacePacks() != null
                        && row.getReplacePacks().length > 0) {

                    for (VoucherResponsePackReplace replacePack : row
                            .getReplacePacks()) {
                        jsonWriter.object().key("packId").value(
                                replacePack.getPackId()).key("barcode").value(
                                replacePack.getBarcode()).key("tradeName")
                                .value(replacePack.getTradeName());
                        jsonWriter.endObject();
                    }
                }
                jsonWriter.endArray();

                // конвертируем массив Warnings
                jsonWriter.key("warnings").array();
                if (row.getDiscountLimitReachedWarnings() != null
                        && row.getDiscountLimitReachedWarnings().length > 0) {

                    for (String w : row.getDiscountLimitReachedWarnings()) {
                        jsonWriter.value(w);
                    }
                }
                jsonWriter.endArray();

                jsonWriter.endObject();
            }
        }
        jsonWriter.endArray();

        // продолжаем конвертировать транзакцию
        jsonWriter.key("complete").value(transaction.isComplete()).key(
                "authorizationCode").value(transaction.getAuthCode());

        if (transaction.getExactRefundSum() != null) {
            jsonWriter.key("exactRefundSum").value(transaction.getExactRefundSum().toString());
        }
        if (transaction.getExactNonRefundSum() != null) {
            jsonWriter.key("exactNonRefundSum").value(transaction.getExactNonRefundSum().toString());
        }
        if (transaction.getExactSumToPay() != null) {
            jsonWriter.key("exactSumToPay").value(transaction.getExactSumToPay().toString());
        }
        jsonWriter.endObject();

        return writer.toString();
    }

    public static final String toXML(ProcessingTransaction transaction) throws JsonParseException,
            JsonMappingException, IOException {
        Writer writer = new StringWriter();
        XMLWriter xmlWriter = new XMLWriter(writer);

        xmlWriter.startElement("voucherResponse");
        xmlWriter.startElement("voucherId").value(transaction.getVoucherId().toString()).endElement();
        xmlWriter.startElement("drugstoreId").value(transaction.getDrugstoreId()).endElement();

        xmlWriter.startElement("confirmationToken");
        if (transaction.getConfirmationToken() != null)
            xmlWriter.value(transaction.getConfirmationToken().toString());
        else
            xmlWriter.value("");
        xmlWriter.endElement();

        VoucherResponseCard card = transaction.getCard();
        if (card != null) {
            xmlWriter.startElement("card");
            xmlWriter.startElement("number").value(card.getNumber()).endElement();
            xmlWriter.startElement("owner").value(card.getOwner()).endElement();
            xmlWriter.startElement("active").value(DatatypeConverter.printBoolean(card.isActive())).endElement();
            xmlWriter.startElement("comment").value(card.getComment()).endElement();
            xmlWriter.startElement("planId").value(card.getPlanId()).endElement();
            xmlWriter.startElement("planName").value(card.getPlanName()).endElement();

            if (card.getChecks() != null && card.getChecks().length > 0)
                for (String value : card.getChecks())
                    xmlWriter.startElement("check").value(value).endElement();

            xmlWriter.endElement(); // card
        }

        xmlWriter.startElement("sumWithoutDiscount")
                .value(DatatypeConverter.printDecimal(transaction.getSumWithoutDiscount()))
                .endElement();
        xmlWriter.startElement("discountSum")
                .value(DatatypeConverter.printDecimal(transaction.getDiscountSum()))
                .endElement();
        xmlWriter.startElement("sumToPay")
                .value(DatatypeConverter.printDecimal(transaction.getSumToPay()))
                .endElement();

        // конвертируем список warnings
        if (transaction.getWarnings() != null && transaction.getWarnings().size() > 0)
            for (String value : transaction.getWarnings())
                xmlWriter.startElement("warning")
                        .value(value)
                        .endElement();

        // конвертируем список VoucherResponseRow
        if (transaction.getRows() != null && transaction.getRows().size() > 0) {
            for (VoucherResponseRow row : transaction.getRows()) {
                xmlWriter.startElement("row");

                xmlWriter.startElement("name").value(row.getName()).endElement();
                xmlWriter.startElement("id").value(row.getId()).endElement();
                xmlWriter.startElement("clientPackId").value(row.getClientPackId()).endElement();
                xmlWriter.startElement("quantity").value(row.getQuantity()).endElement();
                xmlWriter.startElement("price").value(DatatypeConverter.printDecimal(row.getPrice())).endElement();
                xmlWriter.startElement("sumToPay")
                        .value(DatatypeConverter.printDecimal(row.getSumToPay()))
                        .endElement();
                xmlWriter.startElement("discountSum")
                        .value(DatatypeConverter.printDecimal(row.getDiscountSum()))
                        .endElement();
                xmlWriter.startElement("discountPercent")
                        .value(DatatypeConverter.printDecimal(row.getDiscountPercent()))
                        .endElement();
                xmlWriter.startElement("refundSum")
                        .value(DatatypeConverter.printDecimal(row.getRefundSum()))
                        .endElement();
                xmlWriter.startElement("sumWithoutDiscount")
                        .value(DatatypeConverter.printDecimal(row.getSumWithoutDiscount()))
                        .endElement();

                // конвертируем массив VoucherResponsePackReplace
                if (row.getReplacePacks() != null && row.getReplacePacks().length > 0) {

                    for (VoucherResponsePackReplace replacePack : row.getReplacePacks()) {
                        xmlWriter.startElement("replacePack");

                        xmlWriter.startElement("packId").value(replacePack.getPackId()).endElement();
                        xmlWriter.startElement("barcode").value(replacePack.getBarcode()).endElement();
                        xmlWriter.startElement("tradeName").value(replacePack.getTradeName()).endElement();

                        xmlWriter.endElement(); // replacePacks
                    }
                }

                // конвертируем массив Warnings
                if (row.getDiscountLimitReachedWarnings() != null && row.getDiscountLimitReachedWarnings().length > 0)
                    for (String w : row.getDiscountLimitReachedWarnings())
                        xmlWriter.startElement("warning").value(w).endElement();

                xmlWriter.endElement(); // rows
            }
        }

        // продолжаем конвертировать транзакцию
        xmlWriter.startElement("complete").value(transaction.isComplete()).endElement();
        xmlWriter.startElement("authorizationCode").value(transaction.getAuthCode()).endElement();

        xmlWriter.endElement(); // transaction
        return writer.toString();
    }

}
