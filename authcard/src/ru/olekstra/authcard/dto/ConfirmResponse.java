package ru.olekstra.authcard.dto;

import java.util.UUID;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ConfirmResponse {

    private UUID voucherId;
    private String authorizationCode;

    public ConfirmResponse() {
    }

    public ConfirmResponse(UUID voucherId, String authorizationCode) {
        this.voucherId = voucherId;
        this.authorizationCode = authorizationCode;
    }

    @XmlElement
    public UUID getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(UUID voucherId) {
        this.voucherId = voucherId;
    }

    @XmlElement
    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

}
