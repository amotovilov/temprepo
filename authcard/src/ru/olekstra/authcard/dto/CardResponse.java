package ru.olekstra.authcard.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author I.Zerin
 * 
 */
@XmlRootElement
public class CardResponse {

    private String number;
    private String ownerName;
    private String telephoneNumber;
    private String status;
    private String discountPlanName;

    /**
     * @param number
     * @param ownerName
     * @param telephoneNumber
     * @param status
     * @param discountPlanName
     */
    public CardResponse(String number, String owner, String telephoneNumber,
            String status, String discountPlanName) {
        this.number = number;
        this.ownerName = owner;
        this.telephoneNumber = telephoneNumber;
        this.status = status;
        this.discountPlanName = discountPlanName;
    }

    public CardResponse() {
        this.number = "";
        this.ownerName = "";
        this.telephoneNumber = "";
        this.status = "";
        this.discountPlanName = "";
    }

    @XmlElement
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @XmlElement
    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    @XmlElement
    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    @XmlElement
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @XmlElement
    public String getDiscountPlanName() {
        return discountPlanName;
    }

    public void setDiscountPlanName(String discountPlanName) {
        this.discountPlanName = discountPlanName;
    }
}
