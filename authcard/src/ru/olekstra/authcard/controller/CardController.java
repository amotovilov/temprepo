package ru.olekstra.authcard.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ru.olekstra.authcard.dto.CardResponse;
import ru.olekstra.authcard.dto.ClientResponseFormatter;
import ru.olekstra.awsutils.DynamodbService;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.awsutils.exception.NotUpdatedException;
import ru.olekstra.awsutils.exception.OlekstraException;
import ru.olekstra.azure.model.MessageSendException;
import ru.olekstra.azure.model.MessageSendException;
import ru.olekstra.azure.service.QueueSender;
import ru.olekstra.common.service.CardLogService;
import ru.olekstra.common.service.DateTimeService;
import ru.olekstra.common.service.DiscountService;
import ru.olekstra.common.service.XmlMessageService;
import ru.olekstra.domain.Card;
import ru.olekstra.domain.Drugstore;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.dto.VoucherProduct;
import ru.olekstra.domain.dto.VoucherRequest;
import ru.olekstra.exception.DataIntegrityException;
import ru.olekstra.exception.NotFoundException;
import ru.olekstra.processing.Processing;
import ru.olekstra.service.CardOperationService;
import ru.olekstra.service.CardService;
import ru.olekstra.service.DrugstoreService;
import ru.olekstra.service.VoucherParser;
import ru.olekstra.service.VoucherService;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.util.json.JSONException;

@Controller
public class CardController {

    private CardService cardService;
    private DrugstoreService drugstoreService;
    private CardOperationService requestService;
    private VoucherService voucherService;
    private DiscountService discountService;
    private CardLogService cardLogService;
    private XmlMessageService xmlMessageService;
    private DynamodbService service;
    private QueueSender sender;

    @Autowired
    public MessageSource messageSource;

    @Autowired
    Mapper dozerBeanMapper;

    @Autowired
    DateTimeService dateService;

    public static final String STATUS_JSON = "/status.json";
    public static final String STATUS_XML = "/status.xml";
    public static final String CHECK = "/check";
    public static final String CHECK_JSON = "/check.json";
    public static final String CHECK_XML = "/check.xml";

    public static final String CARD_STATUS_ACTIVE = "Активна";
    public static final String CARD_STATUS_NOT_ACTIVE = "Не активна";

    public CardController() {

    }

    @Autowired
    public CardController(CardService cardService,
            DrugstoreService drugstoreService,
            CardOperationService requestService,
            VoucherService voucherService,
            MessageSource messageSource,
            DiscountService discountService,
            CardLogService cardLogService,
            XmlMessageService xmlMessageService,
            DynamodbService service, QueueSender sender) {

        this.cardService = cardService;
        this.drugstoreService = drugstoreService;
        this.requestService = requestService;
        this.voucherService = voucherService;
        this.messageSource = messageSource;
        this.discountService = discountService;
        this.cardLogService = cardLogService;
        this.xmlMessageService = xmlMessageService;
        this.service = service;
        this.sender = sender;
    }

    @RequestMapping(value = STATUS_JSON, method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public CardResponse getCardStatusAsJson(
            @RequestParam("cardNumber") String cardNumber,
            @RequestParam("drugstoreId") String drugstoreId) {

        return getCardResponse(cardNumber, drugstoreId);
    }

    @RequestMapping(value = STATUS_XML, method = RequestMethod.GET, produces = "application/xml")
    @ResponseBody
    public CardResponse getCardStatusAsXml(
            @RequestParam("cardNumber") String cardNumber,
            @RequestParam("drugstoreId") String drugstoreId) {

        return getCardResponse(cardNumber, drugstoreId);
    }

    private CardResponse getCardResponse(String cardNumber, String drugstoreId) {
        CardResponse cardResponse = null;
        Card card = null;
        try {
            Drugstore drugstore = drugstoreService.getDrugstore(drugstoreId);
            card = cardService.getCard(cardNumber);

            String status = "";
            if (card.isActive()) {
                status = CARD_STATUS_ACTIVE;
            } else
                status = CARD_STATUS_NOT_ACTIVE;

            requestService.saveRequestRecord(drugstoreId, cardNumber, status,
                    drugstore.getName(), drugstore.getTimeZone());

            cardResponse = new CardResponse(card.getNumber(), card
                    .getHolderName(), card.getTelephoneNumber(), status,
                    cardService.getDiscountPlanName(card.getDiscountPlanId()));

            /*
             * requestService.saveRequest(drugstoreId, cardNumber, card
             * .getCurrentStatus(), drugstore.getName());
             * 
             * cardResponse = new CardResponse(card.getNumber(), card
             * .getHolderName(), card.getTelephoneNumber(), card
             * .getCurrentStatus(), cardService.getDiscountPlanName(card
             * .getDiscountPlanId()));
             */
        } catch (NotFoundException e) {
            cardResponse = new CardResponse();
            e.printStackTrace();
        } catch (Exception rest) {
            cardResponse = new CardResponse();
            rest.printStackTrace();
        }
        return cardResponse;
    }

    private ProcessingTransaction getCheckSum(VoucherRequest request)
            throws JsonParseException, JsonMappingException, IOException,
            AmazonServiceException, IllegalArgumentException, NotUpdatedException, InstantiationException,
            IllegalAccessException, InterruptedException, ItemSizeLimitExceededException, NotFoundException,
            OlekstraException, RuntimeException, DataIntegrityException, MessageSendException, ParserConfigurationException, TransformerException {
        System.out.println("");
        System.out.println("====== Request ======");
        if (request.getVoucherId() != null)
            System.out.println("Voucher: " + request.getVoucherId().toString());
        System.out.println("Drugstore: " + request.getDrugstoreId());
        if (request.getProducts() != null && request.getProducts().length > 0)
            for (VoucherProduct vp : request.getProducts()) {
                System.out.println("Barcode: " + vp.getBarcode() + ";  Ekt: "
                        + vp.getEkt() + ";  Id: " + vp.getClientPackId()
                        + ";  Name: " + vp.getClientPackName() + ";  Q: "
                        + vp.getQuantity() + ";  Price: "
                        + vp.getPrice().toString());
            }
        if (request.getSumToPay() != null)
            System.out.println("Sum to Pay: "
                    + request.getSumToPay().toString());

        VoucherParser voucherParser = new VoucherParser(voucherService,
                discountService,
                cardLogService,
                xmlMessageService,
                cardService,
                service,
                dozerBeanMapper,
                dateService,
                sender);

        Processing processing = voucherParser.parse(request);
        return processing.doProcessing(request);
    }

    @RequestMapping(value = {CHECK, CHECK_JSON}, method = RequestMethod.POST)
    public void getCheckSumAsJson(
            @RequestBody VoucherRequest request, HttpServletResponse response)
            throws JsonParseException, JsonMappingException, AmazonServiceException, IllegalArgumentException,
            NotUpdatedException, InstantiationException, IllegalAccessException, IOException, InterruptedException,
            ItemSizeLimitExceededException, NotFoundException, OlekstraException, RuntimeException, JSONException,
            DataIntegrityException, MessageSendException, ParserConfigurationException, TransformerException {

        ProcessingTransaction processingTransaction = this.getCheckSum(request);

        String responseBody = ClientResponseFormatter.toJSON(processingTransaction);

        response.setHeader("Content-Type", "application/json;charset=UTF-8");
        response.getWriter().write(responseBody);
        response.getWriter().flush();
    }

    @RequestMapping(value = CHECK_XML, method = RequestMethod.POST,
            consumes = "application/xml")
    @ResponseBody
    public void getCheckSumAsXml(
            @RequestBody VoucherRequest request, HttpServletResponse response)
            throws JsonParseException, JsonMappingException, AmazonServiceException, IllegalArgumentException,
            NotUpdatedException, InstantiationException, IllegalAccessException, IOException, InterruptedException,
            ItemSizeLimitExceededException, NotFoundException, OlekstraException, RuntimeException,
            DataIntegrityException, MessageSendException, ParserConfigurationException, TransformerException {

        ProcessingTransaction processingTransaction = this.getCheckSum(request);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/xml;charset=utf-8");

        String responseBody = ClientResponseFormatter.toXML(processingTransaction);

        response.setHeader("Content-Type", "application/xml;charset=UTF-8");
        response.getWriter().append(responseBody);
    }

}
