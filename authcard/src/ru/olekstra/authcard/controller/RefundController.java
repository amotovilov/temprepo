package ru.olekstra.authcard.controller;

import java.io.IOException;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ru.olekstra.authcard.dto.ConfirmRequest;
import ru.olekstra.authcard.dto.ConfirmResponse;
import ru.olekstra.awsutils.exception.ItemSizeLimitExceededException;
import ru.olekstra.awsutils.exception.NotUpdatedException;
import ru.olekstra.awsutils.exception.OlekstraException;
import ru.olekstra.azure.model.MessageSendException;
import ru.olekstra.domain.ProcessingTransaction;
import ru.olekstra.domain.dto.VoucherProduct;
import ru.olekstra.domain.dto.VoucherRequest;
import ru.olekstra.exception.DataIntegrityException;
import ru.olekstra.exception.NotFoundException;
import ru.olekstra.exception.UnconsistentCheckDataException;
import ru.olekstra.service.RefundService;
import ru.olekstra.service.VoucherService;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.util.json.JSONException;

@Controller
public class RefundController {

    public static final String CONFIRM = "/confirm";
    public static final String CONFIRM_JSON = "/confirm.json";
    public static final String CONFIRM_XML = "/confirm.xml";

    @Autowired
    private VoucherService voucherService;

    @Autowired
    private RefundService refundService;

    public RefundController() {
    }

    @Autowired
    public RefundController(VoucherService voucherService, RefundService refundService) {
        this.voucherService = voucherService;
        this.refundService = refundService;
    }

    @RequestMapping(value = {CONFIRM, CONFIRM_JSON}, method = RequestMethod.POST)
    @ResponseBody
    public ConfirmResponse confirmRefundAsJson(@RequestBody ConfirmRequest confirmRequest,
            HttpServletResponse response)
            throws JsonGenerationException, JsonMappingException, AmazonServiceException, IllegalArgumentException,
            NotUpdatedException, UnconsistentCheckDataException, IOException, InterruptedException,
            ItemSizeLimitExceededException, NotFoundException, OlekstraException, InstantiationException,
            IllegalAccessException, RuntimeException, DataIntegrityException, JSONException, MessageSendException, ParserConfigurationException, TransformerException {

        ConfirmResponse confirmResponse = null;

        VoucherRequest voucherRequest = this.getVoucherRequest(confirmRequest);
        ProcessingTransaction transaction = voucherService.getProcessingTransaction(voucherRequest);

        if (transaction != null) {
            if (confirmRequest.getDrugstoreId().equals(transaction.getDrugstoreId()))
                try {
                    confirmResponse = refundService.getConfirmResponse(confirmRequest, voucherRequest, transaction);
                } catch (UnconsistentCheckDataException ucde) {
                    response.setStatus(response.SC_CONFLICT);
                }
            else
                response.setStatus(response.SC_BAD_REQUEST);
        } else {
            response.setStatus(response.SC_CONFLICT);
        }

        return confirmResponse;
    }

    @RequestMapping(value = {CONFIRM, CONFIRM_JSON}, method = RequestMethod.POST,
            consumes = "application/xml", produces = "application/xml")
    @ResponseBody
    public ConfirmResponse confirmRefundAsXml(@RequestBody ConfirmRequest confirmRequest,
            HttpServletResponse response) throws JsonGenerationException, JsonMappingException, AmazonServiceException,
            IllegalArgumentException, NotUpdatedException, IOException, InterruptedException,
            ItemSizeLimitExceededException, NotFoundException, OlekstraException, InstantiationException,
            IllegalAccessException, RuntimeException, DataIntegrityException, JSONException, MessageSendException, ParserConfigurationException, TransformerException {

        return this.getConfirmResponse(confirmRequest, response);

    }

    private ConfirmResponse getConfirmResponse(ConfirmRequest confirmRequest, HttpServletResponse response)
            throws JsonGenerationException, JsonMappingException, AmazonServiceException, IllegalArgumentException,
            NotUpdatedException, IOException, InterruptedException, ItemSizeLimitExceededException, NotFoundException,
            OlekstraException, InstantiationException, IllegalAccessException, RuntimeException,
            DataIntegrityException, JSONException, MessageSendException, ParserConfigurationException, TransformerException {

        ConfirmResponse confirmResponse = null;

        VoucherRequest voucherRequest = this.getVoucherRequest(confirmRequest);
        ProcessingTransaction transaction = voucherService.getProcessingTransaction(voucherRequest);

        if (transaction != null) {
            if (confirmRequest.getDrugstoreId().equals(transaction.getDrugstoreId()))
                try {
                    confirmResponse = refundService.getConfirmResponse(confirmRequest, voucherRequest, transaction);
                } catch (UnconsistentCheckDataException ucde) {
                    response.setStatus(response.SC_CONFLICT);
                }
            else
                response.setStatus(response.SC_BAD_REQUEST);
        } else {
            response.setStatus(response.SC_CONFLICT);
        }

        return confirmResponse;
    }

    private VoucherRequest getVoucherRequest(ConfirmRequest confirmRequest) {
        return new VoucherRequest(confirmRequest.getVoucherId(),
                confirmRequest.getDrugstoreId(),
                "",
                new VoucherProduct[] {},
                BigDecimal.ZERO);
    }

}
