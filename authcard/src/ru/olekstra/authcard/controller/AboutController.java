package ru.olekstra.authcard.controller;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import ru.olekstra.common.service.AboutService;

@Controller
public class AboutController {
    public AboutController() {

    }

    @Autowired
    private AboutService aboutService;

    // @Autowired
    CardController cardController;

    @RequestMapping("/about")
    public ModelAndView showAboutInfo(Locale locale) throws IOException,
            RuntimeException, InterruptedException {

        ModelAndView mav = new ModelAndView("about");
        LinkedHashMap<String, String> result = aboutService.getBuildStat(locale);
        mav.addObject("stats", result);
        return mav;
    }
}
