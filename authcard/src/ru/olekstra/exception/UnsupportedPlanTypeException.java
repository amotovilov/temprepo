package ru.olekstra.exception;

public class UnsupportedPlanTypeException extends Exception {

    public UnsupportedPlanTypeException(String message) {
        super(message);
    }
}
