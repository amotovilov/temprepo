package ru.olekstra.exception;

public class UnconsistentCheckDataException extends Exception {

    public UnconsistentCheckDataException(String message) {
        super(message);
    }

}
