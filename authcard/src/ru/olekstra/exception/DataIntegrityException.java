package ru.olekstra.exception;

public class DataIntegrityException extends Exception {

    private static final long serialVersionUID = -337692729686347351L;

    public DataIntegrityException(String msg) {
        super(msg);
    }

}
