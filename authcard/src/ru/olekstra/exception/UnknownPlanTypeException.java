package ru.olekstra.exception;

public class UnknownPlanTypeException extends Exception {

    public UnknownPlanTypeException(String message) {
        super(message);
    }
}
